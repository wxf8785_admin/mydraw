#pragma once
#include "MyMemento.h"
#include "Graph.h"
#include "MyDrawView.h"

class MyCommand
{
public:
	MyCommand(void);
	~MyCommand(void);
	virtual void Execute()=0;
	virtual void UnExecute()=0;
	virtual void ReExecute()=0;
protected:
	CMyDrawView* pView;
};

class CutCommand:public MyCommand
{
public:
	CutCommand();
	CutCommand(CMyDrawView* pView);
	~CutCommand(){}
	void Execute();
	void UnExecute();
	void ReExecute();
private:
	MyMemento*  CreateMemento(CArray<CGraph*,CGraph*>& cutGraphs);
	MyMemento* myMemento;
};

class DeleteCommand:public MyCommand
{
public:
	DeleteCommand();
	DeleteCommand(CMyDrawView* pView);
	~DeleteCommand(){}
	void Execute();
	void UnExecute();
	void ReExecute();
private:
	MyMemento* CreateMemento(CArray<CGraph*,CGraph*>& deleteGraphs);
	MyMemento* myMemento;
};

class NewCommand:public MyCommand
{
public:
	NewCommand();
	NewCommand(CMyDrawView* pView);
	~NewCommand(){}
	void Execute();
	void UnExecute();
	void ReExecute();
private:
	MyMemento* CreateMemento(CGraph* newGraphs);//CArray只能用引用作为函数的参数
	MyMemento* myMemento;
};

class PasteCommand:public MyCommand
{
public:
	PasteCommand();
	PasteCommand(CMyDrawView* pView);
	~PasteCommand(){}
	void Execute();
	void UnExecute();
	void ReExecute();
private:
	MyMemento* CreateMemento(CArray<CGraph*, CGraph*>& pasteGraphs);
	MyMemento* myMemento;
};

class MoveCommand:public MyCommand
{
public:
	MoveCommand();
	MoveCommand(CMyDrawView* pView);
	~MoveCommand(){}
	void Execute();
	void UnExecute();
	void ReExecute();
private:
	MyMemento* CreateMemento(CGraph* moveGraph,CRect _OriRect);
	MyMemento* myMemento;
};

