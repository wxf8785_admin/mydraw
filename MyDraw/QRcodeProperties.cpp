
#include "stdafx.h"
#include "MyDraw.h"
#include "QR_Encode.h"
#include "QRcodeProperties.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CQRcodeProperties


CQRcodeProperties::CQRcodeProperties(CWnd* pParent /*=NULL*/)
	: CDialog(CQRcodeProperties::IDD, pParent)
	, nLevel(1)
	, nVersion(0)
	, bAutoExtent(TRUE)
	, nMaskingNo(0)
	, m_fQRRotateAngle(0)
{


	m_bInitControl = FALSE;
}


void CQRcodeProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CQRcodeProperties)
	DDX_Control(pDX, IDC_COMBOLEVEL, m_comboLevel);
	DDX_Control(pDX, IDC_COMBOVERSION, m_comboVersion);
	DDX_Control(pDX, IDC_STATICVERSION, m_staticVersion);
	DDX_Control(pDX, IDC_CHECKAUTOEXTENT, m_checkAutoExtent);
	DDX_Control(pDX, IDC_COMBOMASKINGNO, m_comboMaskingNo);
	DDX_Control(pDX, IDC_STATICMASKINGNO, m_staticMaskingNo);
	DDX_Control(pDX, IDC_EDITMODULESIZE, m_editModuleSize);
	DDX_Control(pDX, IDC_SPINMODULESIZE, m_spinModuleSize);
	DDX_Control(pDX, IDC_STATICBMPSIZE, m_staticBmpSize);
	//}}AFX_DATA_MAP
	DDX_CBIndex(pDX, IDC_COMBOLEVEL, nLevel);
	DDX_CBIndex(pDX, IDC_COMBOVERSION, nVersion);
	DDX_Check(pDX, IDC_CHECKAUTOEXTENT, bAutoExtent);
	DDX_CBIndex(pDX, IDC_COMBOMASKINGNO, nMaskingNo);
	DDX_Text(pDX, IDC_EDIT1, m_fQRRotateAngle);
	DDV_MinMaxFloat(pDX, m_fQRRotateAngle, 0, 360);
}


BEGIN_MESSAGE_MAP(CQRcodeProperties,CDialog)
	//{{AFX_MSG_MAP(CQRcodeProperties)

	ON_CBN_SELCHANGE(IDC_COMBOLEVEL, OnSelChangeLevel)
	ON_CBN_SELCHANGE(IDC_COMBOVERSION, OnSelChangeVersion)
	ON_BN_CLICKED(IDC_CHECKAUTOEXTENT, OnAutoExtent)
	ON_CBN_SELCHANGE(IDC_COMBOMASKINGNO, OnSelChangeMaskNo)
	ON_EN_CHANGE(IDC_EDITMODULESIZE, OnChangeDrawSize)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CQRcodeProperties 




/////////////////////////////////////////////////////////////////////////////
// CQRcodeProperties

BOOL CQRcodeProperties::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (! m_bInitControl)
	{
		//m_comboLevel.SetCurSel(1);

		//m_comboVersion.SetCurSel(0);
		m_staticVersion.SetWindowText("");
		//m_checkAutoExtent.SetCheck(1);
		m_checkAutoExtent.EnableWindow(FALSE);

		//m_comboMaskingNo.SetCurSel(0);
		m_staticMaskingNo.SetWindowText("");

		m_editModuleSize.SetWindowText("4");
		m_spinModuleSize.SetRange(1, 20);
		m_staticBmpSize.SetWindowText("");


		m_bInitControl = TRUE;
	}

	return TRUE;  
}


/////////////////////////////////////////////////////////////////////////////





/////////////////////////////////////////////////////////////////////////////

void CQRcodeProperties::OnSelChangeLevel() 
{
	if (m_bInitControl)
	{

	}
}

void CQRcodeProperties::OnSelChangeVersion() 
{
	if (m_bInitControl)
	{

	}
}

void CQRcodeProperties::OnAutoExtent() 
{
	if (m_bInitControl)
	{

	}
}

void CQRcodeProperties::OnSelChangeMaskNo() 
{
	if (m_bInitControl)
	{

	}
}

void CQRcodeProperties::OnChangeDrawSize() 
{
	if (m_bInitControl)
	{

	}
}




/////////////////////////////////////////////////////////////////////////////

