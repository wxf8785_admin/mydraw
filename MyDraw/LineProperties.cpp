// LineProperties.cpp : implementation file
//

#include "stdafx.h"
#include "MyDraw.h"
#include "LineProperties.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLineProperties dialog


CLineProperties::CLineProperties(CWnd* pParent /*=NULL*/)
	: CDialog(CLineProperties::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLineProperties)
	m_LineWidth = _T("");
	m_LineStyle = _T("");
	//}}AFX_DATA_INIT
}


void CLineProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLineProperties)
	DDX_Control(pDX, IDC_BUTTON_COLOR, m_BtnColor);
	DDX_CBString(pDX, IDC_LINEWIDTH, m_LineWidth);
	DDX_CBString(pDX, IDC_LINESTYLE, m_LineStyle);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLineProperties, CDialog)
	//{{AFX_MSG_MAP(CLineProperties)
	ON_BN_CLICKED(IDC_BUTTON_COLOR, OnButtonColor)
	//}}AFX_MSG_MAP
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLineProperties message handlers


BOOL CLineProperties::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CString str_RGB;
	str_RGB.Format("%d:%d:%d",GetRValue(m_LineColor),
		GetGValue(m_LineColor),GetBValue(m_LineColor));
	this->m_BtnColor.SetWindowText(str_RGB);
	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子  
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLineProperties::OnButtonColor() 
{
	// TODO: Add your control notification handler code here
	CColorDialog dlg(m_LineColor); 
	if (dlg.DoModal()==IDOK)
	{
		m_LineColor=dlg.GetColor();
		CString str_RGB;
		str_RGB.Format("%d:%d:%d",GetRValue(m_LineColor),
			GetGValue(m_LineColor),GetBValue(m_LineColor));
		this->m_BtnColor.SetWindowText(str_RGB);
	}
}


HBRUSH CLineProperties::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
