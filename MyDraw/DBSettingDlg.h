#if !defined(AFX_DBSETTINGDLG_H__D434C9C6_6CD1_488C_9C3E_1B12ADED15EE__INCLUDED_)
#define AFX_DBSETTINGDLG_H__D434C9C6_6CD1_488C_9C3E_1B12ADED15EE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DBSettingDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDBSettingDlg dialog

class CDBSettingDlg : public CDialog
{
// Construction
public:
	CDBSettingDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDBSettingDlg)
	enum { IDD = IDD_DIALOG_SETTING_DB };
	int		m_OtherCharValueCor;
	int		m_FieldNumConstCord;
	int     m_FieldNumConstSep;
	CString	m_nCharcterStringCord;
	CString	m_nCharcterStringSep;
	//}}AFX_DATA

	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDBSettingDlg)
	protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	bool CDBSettingDlg::isUseFieldSymbol();
	int CDBSettingDlg::GetRecordFieldSymbol();
	int CDBSettingDlg::GetSeprateNum();
	int CDBSettingDlg::GetSeprateFieldSymbol();



	void CDBSettingDlg::SetUseFieldSymbol(bool isUseFieldSymbol);
	void CDBSettingDlg::SetRecordFieldSymbol(int n);
	void CDBSettingDlg::SetSeprateNum(int n);
	void CDBSettingDlg::SetSeprateFieldSymbol(int n);

protected:

	// Generated message map functions
	//{{AFX_MSG(CDBSettingDlg)
	afx_msg void OnRadioConstField();
	afx_msg void OnRadioEnterRecord();
	afx_msg void OnRadioOtherRecord();
	afx_msg void OnRadioEnterSeprate();
	afx_msg void OnRadioOtherSeprate();
	afx_msg void OnDeltaposSpinCharterOtherSord(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposSpinFieldNumSep(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRadioUseSeprate();
	afx_msg void OnDeltaposSpinOtherCharSep(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DBSETTINGDLG_H__D434C9C6_6CD1_488C_9C3E_1B12ADED15EE__INCLUDED_)
