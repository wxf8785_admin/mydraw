#include "StdAfx.h"
#include "MyCommand.h"
#include "Singleton.h"
#include "MyDrawView.h"


MyCommand::MyCommand(void)
{
}

MyCommand::~MyCommand(void)
{
}

//cut
CutCommand::CutCommand()
{
}
CutCommand::CutCommand(CMyDrawView *pView)
{
	this->pView = pView;
}

void CutCommand::Execute()
{
	CArray<CGraph*,CGraph*> cutGraph;
	Sing->pasteLsit.RemoveAll();
	POSITION pos = pView->m_ObjectList.GetHeadPosition();
	while (pos != NULL)
	{
		POSITION temp = pos;
		CGraph* pHitItem = (CGraph*)pView->m_ObjectList.GetNext(pos);
		if (pHitItem != NULL && pHitItem->bIsSelected)
		{
			CGraph* graph = pHitItem;
			pView->m_ObjectList.RemoveAt(temp);//从容器中删除选中图元
			cutGraph.Add(graph);
			Sing->pasteLsit.AddTail(graph);
			myMemento = CreateMemento(cutGraph);
		}
	}

}

void CutCommand::UnExecute()
{
	//CMyDrawDoc* pDoc=GetDocument();
	CArray<CGraph*,CGraph*> cutGraph;//使用容器可以实现同时剪切多个图元，这里没有应用
	CGraph* graph;
	//CRect r1;
	CutMemento * cutMemento = dynamic_cast<CutMemento *>(myMemento);
	if (cutMemento == NULL)
		return;
	cutMemento->GetGraph(cutGraph);
	for(int i=0;i<cutGraph.GetSize();i++)
	{
		graph=cutGraph.GetAt(i);//执行粘贴操作后，从备忘录中取出的图元为什么坐标改为粘贴时重置的坐标？
		//r1=graph->GetRect();
		pView->m_ObjectList.AddTail(graph);
	}
}

void CutCommand::ReExecute()
{
	//CMyDrawDoc* pDoc=GetDocument();
	CArray<CGraph*,CGraph*> cutGraph;
	CGraph* graphmemento=NULL;
	CGraph* graph=NULL;
	CRect r1,r2;
	CutMemento * cutMemento = dynamic_cast<CutMemento *>(myMemento);
	if (cutMemento == NULL)
		return;
	cutMemento->GetGraph(cutGraph);
	for(int i=0;i<(cutGraph.GetSize());i++)
	{
		//r1=(CGraph*)cutGraph.GetAt(i)->GetRect();
		graphmemento=(CGraph*)cutGraph.GetAt(i);
		POSITION pos, temp;
		for (pos = pView->m_ObjectList.GetHeadPosition(); pos != NULL;)
		{
			temp = pos;
			graph = (CGraph*)pView->m_ObjectList.GetNext(pos);
			if (graph == graphmemento)
			{
				pView->m_ObjectList.RemoveAt(temp);
				break;
			}
		}
	}
}

MyMemento* CutCommand::CreateMemento(CArray<CGraph*,CGraph*>& cutGraphs)
{
	//myMemento=new CutMemento(cutGraphs);
	return new CutMemento(cutGraphs);
	//return NULL;

}
//delete
DeleteCommand::DeleteCommand()
{
}

DeleteCommand::DeleteCommand(CMyDrawView* pView)
{
	this->pView = pView;
}

void DeleteCommand::Execute()
{
	CArray<CGraph*,CGraph*> deleteGraph;

	POSITION pos = pView->m_ObjectList.GetHeadPosition();
	while (pos != NULL)
	{
		POSITION temp = pos;
		CGraph* pHitItem = (CGraph*)pView->m_ObjectList.GetNext(pos);
		if (pHitItem != NULL && pHitItem->bIsSelected)
		{
			CGraph* graph = pHitItem;
			pView->m_ObjectList.RemoveAt(temp);//从容器中删除选中图元
			deleteGraph.Add(graph);
			myMemento = CreateMemento(deleteGraph);
		}
	}
}

void DeleteCommand::UnExecute()
{
	CArray<CGraph*,CGraph*> deleteGraph;
	CGraph* gp;
	DeleteMemento* deleteMemento=dynamic_cast<DeleteMemento*>(myMemento);
	if (deleteMemento == NULL)
		return;
	deleteMemento->GetGraph(deleteGraph);
	for(int i=0;i<deleteGraph.GetSize();i++)
	{
		gp=deleteGraph.GetAt(i);
		pView->m_ObjectList.AddTail(gp);
	}
}

void DeleteCommand::ReExecute()
{
	CArray<CGraph*,CGraph*> deleteGraph;
	CGraph* gp=NULL;
	CGraph* graph=NULL;
	//CRect r1,r2;
	DeleteMemento* deleteMemento=dynamic_cast<DeleteMemento*>(myMemento);
	if (deleteMemento == NULL)
		return;
	deleteMemento->GetGraph(deleteGraph);
	for(int i=0;i<(deleteGraph.GetSize());i++)
	{
		graph=(CGraph*)deleteGraph.GetAt(i);
		POSITION pos, temp;
		for (pos = pView->m_ObjectList.GetHeadPosition(); pos != NULL;)
		{
			temp = pos;
			graph = (CGraph*)pView->m_ObjectList.GetNext(pos);
			if (graph == graph)
			{
				pView->m_ObjectList.RemoveAt(temp);
				break;
			}
		}
	}
}

MyMemento* DeleteCommand::CreateMemento(CArray<CGraph*,CGraph*> &deleteGraphs)
{
	return new DeleteMemento(deleteGraphs);
}

//new
NewCommand::NewCommand()
{
	myMemento=NULL;
	//graph=NULL;
	
}
NewCommand::NewCommand(CMyDrawView *pView)
{
	this->pView = pView;
	myMemento=NULL;
	//graph=NULL;
	
}

void NewCommand::Execute()
{
	//int w;
	CGraph* graph;
	if(Sing->pGraph!=NULL)
	{
		graph=Sing->pGraph;
		pView->m_ObjectList.AddTail(graph);
		Sing->pGraph=NULL;
		myMemento=CreateMemento(graph);
	}
	else
		return;
}

void NewCommand::UnExecute()
{
	NewMemento* newMemento=dynamic_cast<NewMemento*>(myMemento);
	if (newMemento == NULL)
		return;
	CGraph* gp=NULL;
	CGraph* graph=NULL;
	//CRect r1,r2;
	graph=newMemento->GetGraph();
	//r1=graph->GetRect();
	POSITION pos, temp;
	for (pos = pView->m_ObjectList.GetHeadPosition(); pos != NULL;)
	{
		temp = pos;
		gp = (CGraph*)pView->m_ObjectList.GetNext(pos);
		if (gp == graph)
		{
			pView->m_ObjectList.RemoveAt(temp);
			break;
		}
	}
}

void NewCommand::ReExecute()
{
	CGraph* graph;
	NewMemento* newMemento=dynamic_cast<NewMemento*>(myMemento);
	if (newMemento == NULL)
		return;
	graph=newMemento->GetGraph();
	pView->m_ObjectList.AddTail(graph);
	
}

MyMemento* NewCommand::CreateMemento(CGraph *newGraphs)
{
	return new NewMemento(newGraphs);
}

//paste
PasteCommand::PasteCommand()
{
	myMemento=NULL;
	//graph=NULL;
}

PasteCommand::PasteCommand(CMyDrawView *pView)
{
	this->pView = pView;
	myMemento=NULL;
	//graph=NULL;
}

void PasteCommand::Execute()
{
	CArray<CGraph*, CGraph*> pasteGraph;

	POSITION pos = Sing->pasteLsit.GetHeadPosition();
	while (pos != NULL)
	{
		CGraph* graph1 = (CGraph*)Sing->pasteLsit.GetNext(pos);

		CGraph* graph = graph1->Clone();
		CRect ct;//确定粘贴的位置
		int x1, y1;
		ct = graph1->GetRect();
		OffsetRect(ct, graph->endX - graph->startX, graph->endY - graph->startY);//偏移
		graph->SetRect(ct);

		pView->m_ObjectList.AddTail(graph);

		pasteGraph.Add(graph);
		myMemento = CreateMemento(pasteGraph);

		if (pView->m_selectLsit.Find(graph) == NULL)
			pView->m_selectLsit.AddTail(graph);
		graph->bIsSelected = true;
	}


	//解决多次粘贴位置重叠的问题
	Sing->pasteLsit.RemoveAll();
	POSITION posSel = pView->m_selectLsit.GetHeadPosition();
	CObject* pHitItem;
	while (posSel != NULL)
	{
		pHitItem = (CObject*)pView->m_selectLsit.GetNext(posSel);
		Sing->pasteLsit.AddTail(pHitItem);
	}	

}




void PasteCommand::UnExecute()
{
	CArray<CGraph*, CGraph*> pasteGraph;
	CGraph* gp=NULL;
	CGraph* graph = NULL;
	
	//CRect r1,r2;
	PasteMemento* pasteMemento=dynamic_cast<PasteMemento*>(myMemento);
	if (pasteMemento == NULL)
		return;
	pasteMemento->GetGraph(pasteGraph);
	for (int i = 0; i < (pasteGraph.GetSize()); i++)
	{
		graph = (CGraph*)pasteGraph.GetAt(i);
		POSITION pos, temp;
		for (pos = pView->m_ObjectList.GetHeadPosition(); pos != NULL;)
		{
			temp = pos;
			gp = (CGraph*)pView->m_ObjectList.GetNext(pos);
			if (gp == graph)
			{
				pView->m_ObjectList.RemoveAt(temp);
				break;
			}
		}
	}
}


void PasteCommand::ReExecute()
{
	CArray<CGraph*, CGraph*> pasteGraph;
	CGraph* graph=NULL;
	PasteMemento* pasteMemento=dynamic_cast<PasteMemento*>(myMemento);
	if (pasteMemento == NULL)
		return;
	pasteMemento->GetGraph(pasteGraph);
	for (int i = 0; i < (pasteGraph.GetSize()); i++)
	{
		graph = (CGraph*)pasteGraph.GetAt(i);
		pView->m_ObjectList.AddTail(graph);
	}
}


MyMemento* PasteCommand::CreateMemento(CArray<CGraph*, CGraph*> &pasteGraphs)
{
	return new PasteMemento(pasteGraphs);
}


//move
MoveCommand::MoveCommand()
{
	myMemento=NULL;
}

MoveCommand::MoveCommand(CMyDrawView *pView)
{
	this->pView = pView;
	myMemento=NULL;

}

void MoveCommand::Execute()
{
	CGraph* graph = Sing->pGraph;
	CRect rect = graph->_OriRect;
	myMemento = CreateMemento(graph, rect);
}

void MoveCommand::UnExecute()
{
	//CRect r3,r4;
	CGraph* graph=NULL;
	CRect rect;
	MoveMemento* moveMemento=dynamic_cast<MoveMemento*>(myMemento);
	graph=moveMemento->GetGraph(/*graph*/);//得到备忘图元
	rect=moveMemento->GetRect();//得到备忘的图元索引号
	//r3=graph->GetRect();
	//Sing->reTracker=graph->GetRect();//备忘图元的坐标
	Sing->reOri = graph->tracker.m_rect;//当前主容器中相应图元的坐标
	//r4=Sing->reOri;
	graph->SetRect(rect);//将主容器中的相应图元的坐标改为备忘录中，备忘图元的坐标
	moveMemento->SetRect(Sing->reOri);//更改备忘图元的坐标
	Sing->reOri=0;
}

void MoveCommand::ReExecute()
{
	//CRect r3,r4;
	CGraph* graph=NULL;
	CRect rect;
	MoveMemento* moveMemento=dynamic_cast<MoveMemento*>(myMemento);
	graph=moveMemento->GetGraph(/*graph*/);//得到备忘图元
	rect=moveMemento->GetRect();//得到备忘的图元索引号
	//r3=graph->GetRect();
	//Sing->reTracker=graph->GetRect();//备忘图元的坐标
	Sing->reOri=graph->GetRect();//当前主容器中相应图元的坐标
	//r4=Sing->reOri;
	graph->SetRect(rect);//将主容器中的相应图元的坐标改为备忘录中，备忘图元的坐标
	moveMemento->SetRect(Sing->reOri);//更改备忘图元的坐标
	Sing->reOri=0;
}

MyMemento* MoveCommand::CreateMemento(CGraph *moveGraph, CRect _OriRect)
{
	return new MoveMemento(moveGraph,_OriRect);
}

