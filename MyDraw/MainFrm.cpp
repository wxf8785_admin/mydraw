﻿// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "MyDraw.h"
#include "MyDrawDoc.h"
#include "MainFrm.h"
#include "MyDrawView.h"
#include "SplashWnd.h"  
#include "PrintDlg.h"
#include "MyProgress.h"
#include "thread_lock.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWndEx)

const int  iMaxUserToolbars = 10;
const UINT uiFirstUserToolBarId = AFX_IDW_CONTROLBAR_FIRST + 40;
const UINT uiLastUserToolBarId = uiFirstUserToolBarId + iMaxUserToolbars - 1;


CMap<int, int, int, int> m_mapFontSize;
CMap<int, int, CString, CString> m_mapFontHeight;

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWndEx)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_VIEW_CUSTOMIZE, &CMainFrame::OnViewCustomize)
	ON_REGISTERED_MESSAGE(AFX_WM_CREATETOOLBAR, &CMainFrame::OnToolbarCreateNew)
	ON_COMMAND_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnApplicationLook)
	ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_WINDOWS_7, &CMainFrame::OnUpdateApplicationLook)
	ON_COMMAND(ID_MENUITEM_PRINT, OnMenuitemPrint)
	ON_COMMAND(ID_MENUITEM_LINE, OnMenuitemLine)
	ON_COMMAND(ID_MENUITEM_ELLIPSE, OnMenuitemEllipse)
	ON_COMMAND(ID_MENUITEM_BAR, OnMenuitemBar)
	ON_COMMAND(ID_MENUITEM_ARC, OnMenuitemArc)
	ON_COMMAND(ID_MENUITEM_RECT, OnMenuitemRect)
	ON_COMMAND(ID_MENUITEM_RECT2, OnMenuitemRect2)
	ON_COMMAND(ID_MENUITEM_TEXT, OnMenuitemText)
	ON_COMMAND(ID_MENUITEM_PRINTSET, OnMenuitemPrintset)
	ON_COMMAND(ID_MENUITEM_FIRMWARESET, OnMenuitemFirmwareset)
	ON_COMMAND(ID_MENUITEM_HARDWARESET, OnMenuitemHardwareset)
	ON_COMMAND(ID_MENUITEM_LINKBOXSET, OnMenuitemLinkboxset)
	ON_COMMAND(ID_MENUITEM_PRENTCODEERRORSET, OnMenuitemPrentcodeerrorset)
	ON_WM_TIMER()
	ON_COMMAND(ID_MENUITEM_SETTING_DB, OnMenuitemSettingDb)
	ON_COMMAND(ID_MENUITEM_IMPORT_DB, OnMenuitemImportDb)
	ON_UPDATE_COMMAND_UI(ID_MENUITEM_IMPORT_DB, OnUpdateMenuitemImportDb)
	ON_COMMAND_RANGE(ID_MENUITEM_ZOOM_10, ID_MENUITEM_ZOOM_500, &CMainFrame::OnMenuitemZoom)
	ON_UPDATE_COMMAND_UI_RANGE(ID_MENUITEM_ZOOM_10, ID_MENUITEM_ZOOM_500, &CMainFrame::OnUpdateMenuitemZoom)
	ON_MESSAGE(WM_USER_MSG, OnDealRecvUdpMsg)
	ON_COMMAND(ID_MENUITEM_PIC, OnMenuitemPic)
	ON_WM_SETTINGCHANGE()
	ON_COMMAND(ID_COM_ZOOM_EDIT, OnClickComboBox)
	ON_CBN_SELCHANGE(ID_COM_ZOOM_EDIT, OnSelChangeClick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator	
	ID_INDICATOR_DBPATH, 
	ID_INDICATOR_VIEW,	
	ID_INDICATOR_XXX,
	ID_INDICATOR_PAGE,
	ID_INDICATOR_DPI,
	ID_INDICATOR_TIME,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
};
int getSMJETIndexByIP(char* sIP);
CThreadLock  threadLock;                            //声明CThreadLock类型的全局变量
/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	int hr = BCLicenseMe("Mem: Zhishen Xingyuan Technology Co., Ltd, CN-46-3204", eLicKindDeveloper, 1, "BD71F019347A8CEA7D9C13710AECB41E", eLicProd2D);

	//theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_OFF_2003);
	theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_WIN_XP); 
	//theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_OFF_2007_AQUA);
	theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_100;

	pPrintDlg = new CPrintDlg();
	pPrintSetDlg = new CPrintSetDlg();
	pLinkBoxOperDlg = new CLinkBoxOperDlg();
	pHardWareSetDlg = new CHardWareSetDlg();
	pFirmWareOperDlg = new CFirmWareOperDlg();
	pDBSettingDlg = new CDBSettingDlg();

	byte m_nAckNum = 0;
	for (int i = 0; i < 6; i++)
	{
		m_nPrintIndex[i] = 1;		//打印头编号
		m_nPaperIndex[i] = 1;		//打印页编号
		m_nPackageIndex[i] = 0;	//本数据包编号
		m_nPrinterConut[i] = 1;
		SpeedTime[i] = 0;
		nReSendCnt[i] = 0;
	}

	isPrint = false;
	m_strPath = GetWorkDir();
	m_nPaperLenth = ::GetPrivateProfileInt(_T("PrintSet"), _T("PAPER_LENTH"), 297, m_strPath) ;// -4
	sDBPathName = _T("未加载数据库");
	m_nVersion = 2;
}

CMainFrame::~CMainFrame()
{
	if (pPrintDlg)
	{
		delete pPrintDlg;
		pPrintDlg = NULL;
	}

	if (pPrintSetDlg)
	{
		delete pPrintSetDlg;
		pPrintSetDlg = NULL;
	}

	if (pLinkBoxOperDlg)
	{
		delete pLinkBoxOperDlg;
		pLinkBoxOperDlg = NULL;
	}

	if (pHardWareSetDlg)
	{
		delete pHardWareSetDlg;
		pHardWareSetDlg = NULL;
	}

	if (pFirmWareOperDlg)
	{
		delete pFirmWareOperDlg;
		pFirmWareOperDlg = NULL;
	}

	if (pDBSettingDlg)
	{
		delete pDBSettingDlg;
		pDBSettingDlg = NULL;
	}

	OnUdpClose(0);
	OnUdpClose(1);
	OnUdpClose(2);
	OnUdpClose(3);
	OnUdpClose(4);
	OnUdpClose(5);
}




//DWORD WINAPI UdpRecvThread(LPVOID lpParam)
UINT _stdcall UdpRecvThread(LPVOID lpParam)
{
	CMainFrame *pFrm = (CMainFrame*)lpParam;

	WORD wVersionRequested;
	WSADATA wsaData;
	int err;

	wVersionRequested = MAKEWORD(1, 1);

	err = WSAStartup(wVersionRequested, &wsaData);
	if (err != 0) {
		printf("Init socket dll error!");
		return 0;
	}


	if (LOBYTE(wsaData.wVersion) != 1 ||
		HIBYTE(wsaData.wVersion) != 1) {
		WSACleanup();
		return 0;
	}

	SOCKET sockSrv = socket(AF_INET, SOCK_DGRAM, 0);
	if (SOCKET_ERROR == sockSrv)
	{
		printf("Create Socket Error!");
		return 0;
	}
	SOCKADDR_IN addrSrv;
	addrSrv.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	addrSrv.sin_family = AF_INET;
	addrSrv.sin_port = htons(8090);

	BOOL bDisable = TRUE;
	setsockopt(sockSrv, IPPROTO_TCP, TCP_NODELAY, (char*)&bDisable, sizeof(bDisable));

	int nRecvBuf = 32 * 1024;//设置为32K
	setsockopt(sockSrv, SOL_SOCKET, SO_RCVBUF, (const char*)&nRecvBuf, sizeof(int));
	//发送缓冲区
	int nSendBuf = 32 * 1024;//设置为32K
	setsockopt(sockSrv, SOL_SOCKET, SO_SNDBUF, (const char*)&nSendBuf, sizeof(int));


	bind(sockSrv, (SOCKADDR*)&addrSrv, sizeof(SOCKADDR));

	SOCKADDR_IN addrClient;
	int len = sizeof(SOCKADDR);
	char recvBuf[1024];

	bool nFlag = true;
	char ip[50];

	while (nFlag)
	{
		recvfrom(sockSrv, (char*)recvBuf, sizeof(recvBuf), 0, (SOCKADDR*)&addrClient, &len);
		//pFrm->SendMessage(WM_USER_MSG,(WPARAM)recvBuf,sizeof(recvBuf));

		strcpy(ip, (const char*)inet_ntoa(addrClient.sin_addr));
		SocketRecvParam* pSocketRecvParam = new SocketRecvParam;
		memset(pSocketRecvParam->buff, 0, sizeof(pSocketRecvParam->buff));
		memset(pSocketRecvParam->sIP, 0, sizeof(pSocketRecvParam->sIP));
		memcpy(pSocketRecvParam->buff, recvBuf, sizeof(recvBuf));
		memcpy(pSocketRecvParam->sIP, ip, 50);
		pSocketRecvParam->nLenth = sizeof(recvBuf);

		pFrm->SendMessage(WM_USER_MSG, (WPARAM)pSocketRecvParam, sizeof(SocketRecvParam));

		//printf("%s\n", recvBuf);
	}

	closesocket(sockSrv);
	WSACleanup();


	return 1;

}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;


	BOOL bNameValid;
	m_hAccel = LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));
	if (!m_wndMenuBar.Create(this))
	{
		TRACE0("未能创建菜单栏\n");
		return -1;      // 未能创建
	}

	m_wndMenuBar.SetPaneStyle(m_wndMenuBar.GetPaneStyle() | CBRS_SIZE_DYNAMIC | CBRS_TOOLTIPS | CBRS_FLYBY);

	// 防止菜单栏在激活时获得焦点
	CMFCPopupMenu::SetForceMenuFocus(FALSE);

	if (!m_myToolBar2.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC, CRect(1, 1, 1, 1), IDR_TOOLBAR1) ||
		!m_myToolBar2.LoadToolBar(IDR_TOOLBAR1))
	{
		TRACE0("未能创建工具栏n");
		return -1; // 未能创建 
	}


	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC, CRect(1, 1, 1, 1), IDR_MAINFRAME_256) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME_256))
	{
		TRACE0("未能创建工具栏\n");
		return -1;      // 未能创建
	}


	if (!m_myToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC, CRect(1, 1, 1, 1), IDR_MYTOOLBAR) ||
		!m_myToolBar.LoadToolBar(IDR_MYTOOLBAR))
	{
		TRACE0("未能创建预览工具栏n");
		return -1; // 未能创建 
	}


	m_myToolBar2.SetWindowText(_T("对齐"));
	m_wndToolBar.SetWindowText(_T("标准工具栏"));
	m_myToolBar.SetWindowText(_T("预览"));

	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);
	//m_wndToolBar.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);

	// 允许用户定义的工具栏操作: 
	InitUserToolbars(NULL, uiFirstUserToolBarId, uiLastUserToolBarId);

	if (!m_wndStatusBar.Create(this))
	{
		TRACE0("未能创建状态栏\n");
		return -1;      // 未能创建
	}
	m_wndStatusBar.SetIndicators(indicators, sizeof(indicators) / sizeof(UINT));

	m_wndStatusBar.SetPaneInfo(0, ID_SEPARATOR, SBPS_NORMAL, 50);
	m_wndStatusBar.SetPaneInfo(1, ID_INDICATOR_DBPATH, SBPS_NORMAL, 600);
	m_wndStatusBar.SetPaneInfo(2, ID_INDICATOR_VIEW, SBPS_NORMAL, 100);
	m_wndStatusBar.SetPaneInfo(3, ID_INDICATOR_XXX, SBPS_NORMAL, 200);
	m_wndStatusBar.SetPaneInfo(4, ID_INDICATOR_PAGE, SBPS_NORMAL, 100);
	m_wndStatusBar.SetPaneInfo(5, ID_INDICATOR_DPI, SBPS_NORMAL, 200);
	m_wndStatusBar.SetPaneInfo(6, ID_INDICATOR_TIME, SBPS_NORMAL, 200);
	m_wndStatusBar.SetPaneInfo(7, ID_INDICATOR_CAPS, SBPS_NORMAL, 50);
	m_wndStatusBar.SetPaneInfo(8, ID_INDICATOR_NUM, SBPS_NORMAL, 50);

	//插入组合框  
	m_wndToolBar.InsertButton(CMFCToolBarComboBoxButton(ID_COM_ZOOM_EDIT, 0, CBS_DROPDOWNLIST,60), 23);
	//设置组合框属性  
	m_comboZoom = (CMFCToolBarComboBoxButton*)m_wndToolBar.GetButton(23);
	m_comboZoom->EnableWindow(TRUE);
	m_comboZoom->SetCenterVert();
	m_comboZoom->SetDropDownHeight(200);//设置下拉列表的高度  
	m_comboZoom->SetFlatMode();
	//添加内容  
	m_comboZoom->AddItem(_T("10%"));
	m_comboZoom->AddItem(_T("20%"));
	m_comboZoom->AddItem(_T("50%"));
	m_comboZoom->AddItem(_T("100%"));
	m_comboZoom->AddItem(_T("200%"));
	m_comboZoom->AddItem(_T("300%"));
	m_comboZoom->AddItem(_T("500%"));
	m_comboZoom->SelectItem(3);

	// TODO:  如果您不希望工具栏和菜单栏可停靠，请删除这五行
	AddPane(&m_wndMenuBar);
	EnableDocking(CBRS_ALIGN_ANY);
	EnableAutoHidePanes(CBRS_ALIGN_ANY);
	SetMenuBarState(AFX_MBS_VISIBLE);

	//m_wndMenuBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	m_myToolBar.EnableDocking(CBRS_ALIGN_ANY);
	m_myToolBar2.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	//DockPane(&m_wndMenuBar);
	//DockPane(&m_wndToolBar);
	//DockPane(&m_myToolBar);
	DockPane(&m_myToolBar2);
	DockPaneLeftOf(&m_myToolBar, &m_myToolBar2);//主要是这里
	DockPaneLeftOf(&m_wndToolBar, &m_myToolBar);//主要是这里



	// 启用 Visual Studio 2005 样式停靠窗口行为
	CDockingManager::SetDockingMode(DT_SMART);
	// 启用 Visual Studio 2005 样式停靠窗口自动隐藏行为
	EnableAutoHidePanes(CBRS_ALIGN_ANY);

	// 加载菜单项图像(不在任何标准工具栏上): 
	CMFCToolBar::AddToolBarForImageCollection(IDR_MENU_IMAGES, theApp.m_bHiColorIcons ? IDB_MENU_IMAGES_24 : 0);

	// 创建停靠窗口
	if (!CreateDockingWindows())
	{
		TRACE0("未能创建停靠窗口\n");
		return -1;
	}

	//m_wndFileView.EnableDocking(CBRS_ALIGN_ANY);
	//m_wndClassView.EnableDocking(CBRS_ALIGN_ANY);
	//DockPane(&m_wndFileView);
	//CDockablePane* pTabbedBar = NULL;
	//m_wndClassView.AttachToTabWnd(&m_wndFileView, DM_SHOW, TRUE, &pTabbedBar);
	//m_wndOutput.EnableDocking(CBRS_ALIGN_ANY);
	//DockPane(&m_wndOutput);
	m_wndProperties.EnableDocking(CBRS_ALIGN_ANY);
	DockPane(&m_wndProperties);

	// 基于持久值设置视觉管理器和样式
	OnApplicationLook(theApp.m_nAppLook);

	// 启用工具栏和停靠窗口菜单替换
	EnablePaneMenu(TRUE, ID_VIEW_CUSTOMIZE, strCustomize, ID_VIEW_TOOLBAR);

	// 启用快速(按住 Alt 拖动)工具栏自定义
	CMFCToolBar::EnableQuickCustomization();

	if (CMFCToolBar::GetUserImages() == NULL)
	{
		// 加载用户定义的工具栏图像
		if (m_UserImages.Load(_T(".\\UserImages.bmp")))
		{
			CMFCToolBar::SetUserImages(&m_UserImages);
		}
	}

	// 启用菜单个性化(最近使用的命令)
	// TODO:  定义您自己的基本命令，确保每个下拉菜单至少有一个基本命令。
	CList<UINT, UINT> lstBasicCommands;

	lstBasicCommands.AddTail(ID_FILE_NEW);
	lstBasicCommands.AddTail(ID_FILE_OPEN);
	lstBasicCommands.AddTail(ID_FILE_SAVE);
	lstBasicCommands.AddTail(ID_FILE_PRINT);
	lstBasicCommands.AddTail(ID_APP_EXIT);
	lstBasicCommands.AddTail(ID_EDIT_CUT);
	lstBasicCommands.AddTail(ID_EDIT_PASTE);
	lstBasicCommands.AddTail(ID_EDIT_UNDO);
	lstBasicCommands.AddTail(ID_APP_ABOUT);
	lstBasicCommands.AddTail(ID_VIEW_STATUS_BAR);
	lstBasicCommands.AddTail(ID_VIEW_TOOLBAR);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2003);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_VS_2005);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLUE);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_SILVER);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLACK);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_AQUA);
	lstBasicCommands.AddTail(ID_VIEW_APPLOOK_WINDOWS_7);
	lstBasicCommands.AddTail(ID_SORTING_SORTALPHABETIC);
	lstBasicCommands.AddTail(ID_SORTING_SORTBYTYPE);
	lstBasicCommands.AddTail(ID_SORTING_SORTBYACCESS);
	lstBasicCommands.AddTail(ID_SORTING_GROUPBYTYPE);

	int index = 0;
	RECT rect;
	rect.left = 248;
	rect.top = 1;
	rect.right = 400;
	rect.bottom = 100;
	// 创建并显示控件 

	if (!m_barText.Create(ES_MULTILINE | WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER, rect,
		&m_myToolBar, ID_BARTEXT))
	{
		return FALSE;
	}
	m_barText.SetWindowTextA("1");
	m_barText.EnableWindow(FALSE);

	m_barText.ShowWindow(SW_SHOW);

	//CMFCToolBar::SetBasicCommands(lstBasicCommands);

	pPrintDlg->Create(IDD_PRINT_DLG, GetDesktopWindow());
	pPrintDlg->ShowWindow(SW_HIDE);


	pPrintSetDlg->Create(IDD_DIALOG_PRINTSET, this);
	pPrintSetDlg->ShowWindow(SW_HIDE);


	pLinkBoxOperDlg->Create(IDD_DIALOG_LINKBOXOPER, this);
	pLinkBoxOperDlg->ShowWindow(SW_HIDE);

	pHardWareSetDlg->Create(IDD_DIALOG_HARDWARESET, this);
	pHardWareSetDlg->ShowWindow(SW_HIDE);

	pFirmWareOperDlg->Create(IDD_DIALOG_FIRMWAREOPER, this);
	pFirmWareOperDlg->ShowWindow(SW_HIDE);

	pDBSettingDlg->Create(IDD_DIALOG_SETTING_DB, this);
	pDBSettingDlg->ShowWindow(SW_HIDE);


	for (int i = 0; i < 6; i++)
	{
		if (i == 0)
			OnInitUdpSock("192.168.1.100", 8090, 0);
		if (i == 1)
			OnInitUdpSock("192.168.1.101", 8090, 1);
		if (i == 2)
			OnInitUdpSock("192.168.1.102", 8090, 2);
		if (i == 3)
			OnInitUdpSock("192.168.1.103", 8090, 3);
		if (i == 4)
			OnInitUdpSock("192.168.1.104", 8090, 4);
		if (i == 5)
			OnInitUdpSock("192.168.1.105", 8090, 5);
	}

	HANDLE hTcpThread = (HANDLE)_beginthreadex(NULL,0,UdpRecvThread,this,0,NULL);

	SetTimer(1, 2000, NULL);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CFrameWndEx::PreCreateWindow(cs))
		return FALSE;
	// TODO:  在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return TRUE;
}

BOOL CMainFrame::CreateDockingWindows()
{
	BOOL bNameValid;
	//// 创建类视图
	//CString strClassView;
	//bNameValid = strClassView.LoadString(IDS_CLASS_VIEW);
	//ASSERT(bNameValid);
	//if (!m_wndClassView.Create(strClassView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_CLASSVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT | CBRS_FLOAT_MULTI))
	//{
	//	TRACE0("未能创建“类视图”窗口\n");
	//	return FALSE; // 未能创建
	//}

	//// 创建文件视图
	//CString strFileView;
	//bNameValid = strFileView.LoadString(IDS_FILE_VIEW);
	//ASSERT(bNameValid);
	//if (!m_wndFileView.Create(strFileView, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_FILEVIEW, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_LEFT| CBRS_FLOAT_MULTI))
	//{
	//	TRACE0("未能创建“文件视图”窗口\n");
	//	return FALSE; // 未能创建
	//}

	//// 创建输出窗口
	//CString strOutputWnd;
	//bNameValid = strOutputWnd.LoadString(IDS_OUTPUT_WND);
	//ASSERT(bNameValid);
	//if (!m_wndOutput.Create(strOutputWnd, this, CRect(0, 0, 100, 100), TRUE, ID_VIEW_OUTPUTWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_BOTTOM | CBRS_FLOAT_MULTI))
	//{
	//	TRACE0("未能创建输出窗口\n");
	//	return FALSE; // 未能创建
	//}

	// 创建属性窗口
	CString strPropertiesWnd;
	bNameValid = strPropertiesWnd.LoadString(IDS_PROPERTIES_WND);
	ASSERT(bNameValid);
	if (!m_wndProperties.Create(strPropertiesWnd, this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_PROPERTIESWND, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CBRS_RIGHT | CBRS_FLOAT_MULTI))
	{
		TRACE0("未能创建“属性”窗口\n");
		return FALSE; // 未能创建
	}
	SetDockingWindowIcons(theApp.m_bHiColorIcons);
	return TRUE;
}

void CMainFrame::SetDockingWindowIcons(BOOL bHiColorIcons)
{
	//HICON hFileViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_FILE_VIEW_HC : IDI_FILE_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	//m_wndFileView.SetIcon(hFileViewIcon, FALSE);

	//HICON hClassViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_CLASS_VIEW_HC : IDI_CLASS_VIEW), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	//m_wndClassView.SetIcon(hClassViewIcon, FALSE);

	//HICON hOutputBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_OUTPUT_WND_HC : IDI_OUTPUT_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	//m_wndOutput.SetIcon(hOutputBarIcon, FALSE);

	HICON hPropertiesBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), MAKEINTRESOURCE(bHiColorIcons ? IDI_PROPERTIES_WND_HC : IDI_PROPERTIES_WND), IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
	m_wndProperties.SetIcon(hPropertiesBarIcon, FALSE);

}

// CMainFrame 诊断
#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWndEx::Dump(dc);
}
#endif //_DEBUG



// CMainFrame 消息处理程序
void CMainFrame::ShowRulers(BOOL bShow)
{
	m_Rulers.ShowRulers(bShow);
}

void CMainFrame::UpdateRulersInfo(stRULER_INFO stRulerInfo)
{
	m_Rulers.UpdateRulersInfo(stRulerInfo);
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	if (!m_Rulers.CreateRulers(this, pContext)) {
		TRACE("Error creation of rulers\n");
		return CFrameWnd::OnCreateClient(lpcs, pContext);
	}
	return TRUE;
}


void CMainFrame::OnViewCustomize()
{
	CMFCToolBarsCustomizeDialog* pDlgCust = new CMFCToolBarsCustomizeDialog(this, TRUE /* 扫描菜单*/);
	pDlgCust->EnableUserDefinedToolbars();
	pDlgCust->Create();
}

LRESULT CMainFrame::OnToolbarCreateNew(WPARAM wp, LPARAM lp)
{
	LRESULT lres = CFrameWndEx::OnToolbarCreateNew(wp, lp);
	if (lres == 0)
	{
		return 0;
	}

	CMFCToolBar* pUserToolbar = (CMFCToolBar*)lres;
	ASSERT_VALID(pUserToolbar);

	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);

	pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
	return lres;
}

int _nUpSMJET;
void CMainFrame::SetSMJET(int nSmjet)
{
	_nUpSMJET = nSmjet;
}

CArray<FirmData, FirmData> _firmDataArray;

void CMainFrame::ClearFirmDataArray()
{
	_firmDataArray.RemoveAll();
}


void CMainFrame::SetFirmDataArray(FirmData tempArray)
{
	FirmData fd;
	fd.code = 6;
	fd.pack_no = tempArray.pack_no;
	fd.packtatal = tempArray.packtatal;
	fd.reverse = tempArray.reverse;
	fd.total_len = tempArray.total_len;
	fd.total_byte = tempArray.total_byte;
	memcpy(fd.bData, tempArray.bData, 1000);
	_firmDataArray.Add(fd);
}


LRESULT CMainFrame::OnDealRecvUdpMsg(WPARAM wParam, LPARAM lParam)
{

	SocketRecvParam* pSocketRecvParam = (SocketRecvParam*)wParam;
	char pData[1024];
	int nLen = pSocketRecvParam->nLenth;
	memcpy(pData, pSocketRecvParam->buff, nLen);

	int CurSMJETIndex = getSMJETIndexByIP(pSocketRecvParam->sIP) - 1;

	if (CurSMJETIndex < 0)
	{
		return 0;
	}


	byte nTmp1 = pData[0];
	byte nTmp2 = pData[1];
	if ((pData[0] == 0x01) && (pData[1] == 0x24))
	{
		OnDealRecvInkBoxParam(pData, nLen, CurSMJETIndex);//墨盒参数
	}
	else if ((pData[0] == 0x0E))
	{
		OnDealRecvKeepAlive(pData, nLen, CurSMJETIndex);//心跳握返回帧
	}
	else if ((pData[0] == 0x11))
	{
		OnDealRecvVersion(pData, nLen, CurSMJETIndex);//版本号
	}
	else if (pData[0] == 0x0F)
	{
		CString sTmp = "";
		int nSet = (pData[1] & 0xF);
		sTmp.Format("设置SMJET%d为SMJET%d成功，请重启设备", CurSMJETIndex + 1, nSet + 1);
		AfxMessageBox(sTmp);
	}
	else if (pData[0] == 0x0D)
	{
		if (!isPrint)
			return 0;
		OnDealRecvDataFeedBack(pData, nLen, CurSMJETIndex);//数据帧返回帧
	}
	else if (pData[0] == 0x0C)
	{
		m_nPackageIndex[CurSMJETIndex] = 1;

		m_nPackageDataLenth = 1000;
		isPrint = true;

		if (!hasData[CurSMJETIndex])
		{
			return  0;
		}
		SendPrintData(m_nPrintIndex[CurSMJETIndex], m_nPaperIndex[CurSMJETIndex], m_nPackageIndex[CurSMJETIndex], m_nPaperLenth, m_nPackageDataLenth, CurSMJETIndex, 10);
	}
	else if (pData[0] == 0x10)
	{
		OnDealRecvFirmWareFeedBack(pData, nLen, CurSMJETIndex);//固件升级返回帧
	}

	else if (pData[0] == 0x0B)
	{
		AfxMessageBox(_T("写入墨盒参数成功！"));
	}
	else if (pData[0] == 18)
	{
		AfxMessageBox(_T("清洗墨盒成功！"));
	}


	delete pSocketRecvParam;
	pSocketRecvParam = NULL;

	return 0;
}


int getSMJETIndexByIP(char* sIP)
{
	CString Tmp = "";
	Tmp.Format("%s", sIP);

	if (Tmp == "192.168.1.100")
	{
		return 1;
	}
	else if (Tmp == "192.168.1.101")
	{
		return 2;
	}
	else if (Tmp == "192.168.1.102")
	{
		return 3;
	}
	else if (Tmp == "192.168.1.103")
	{
		return 4;
	}
	else if (Tmp == "192.168.1.104")
	{
		return 5;
	}
	else if (Tmp == "192.168.1.105")
	{
		return 6;
	}

	return 0;

}

////墨盒参数
void CMainFrame::OnDealRecvInkBoxParam(char* pBuff, int nLen, int nSMJETIndex)
{
	if (pBuff == NULL || nLen == 0)
		return;
	if ((pBuff[0] == 0x01) && (pBuff[1] == 0x24))
	{
		INKBOXPARAM* pINKBOXPARAM = (INKBOXPARAM*)pBuff;

		/*
		for(int i=0;i<4;i++)
		{
		pINKBOXPARAM->m_nBadPint[i] = (int)pLinkBoxOperDlg->GetInkBoxParam(i,1);
		//		pINKBOXPARAM->m_nBadPint[i] = pHardWareSetDlg->GetInkBoxParam(i,2);
		pINKBOXPARAM->m_nInkBoxTemp[i] = (int)(pLinkBoxOperDlg->GetInkBoxParam(i,3)*10);
		pINKBOXPARAM->m_nInkBoxVol[i] = (int)(pLinkBoxOperDlg->GetInkBoxParam(i,4)*10);
		pINKBOXPARAM->m_nIsWarm[i] = (int)pLinkBoxOperDlg->GetInkBoxParam(i,5);
		pINKBOXPARAM->m_nIsWash[i] = (int)pLinkBoxOperDlg->GetInkBoxParam(i,6);
		pINKBOXPARAM->m_nRenew[i] = (int)pLinkBoxOperDlg->GetInkBoxParam(i,7);
		pINKBOXPARAM->m_nInkBoxRow[i] = (int)pLinkBoxOperDlg->GetInkBoxParam(i,8);
		}
		*/
		pLinkBoxOperDlg->m_nSMJETIndex_H.SetCurSel(nSMJETIndex - 1);
		for (int i = 0; i < 4; i++)
		{
			CString sText = "";
			sText.Format("%.1f", pINKBOXPARAM->m_nPulseTime[i] / 10.0f);
			pLinkBoxOperDlg->m_InkBoxParamList.SetItemText(i, 1, sText);

			sText.Format("%.1f", pINKBOXPARAM->m_nInkBoxTemp[i] / 10.0f);
			pLinkBoxOperDlg->m_InkBoxParamList.SetItemText(i, 3, sText);

			sText.Format("%.1f", pINKBOXPARAM->m_nInkBoxVol[i] / 10.0f);
			pLinkBoxOperDlg->m_InkBoxParamList.SetItemText(i, 4, sText);


			if (pINKBOXPARAM->m_nIsWarm[i] >= 0 || pINKBOXPARAM->m_nIsWarm[i] <= 1)
			{
				char* p[] = { "是", "否" };
				sText.Format("%s", p[pINKBOXPARAM->m_nIsWarm[i]]);
				pLinkBoxOperDlg->m_InkBoxParamList.SetItemText(i, 5, sText);
			}


			if (pINKBOXPARAM->m_nIsWash[i] >= 0 || pINKBOXPARAM->m_nIsWash[i] <= 3)
			{
				char* p[] = { "不清洗", "轻度", "中度", "重度" };
				sText.Format("%s", p[pINKBOXPARAM->m_nIsWash[i]]);
				pLinkBoxOperDlg->m_InkBoxParamList.SetItemText(i, 6, sText);
			}

			if (pINKBOXPARAM->m_nRenew[i] >= 0 || pINKBOXPARAM->m_nRenew[i] <= 1)
			{
				char* p[] = { "是", "否" };
				sText.Format("%s", p[pINKBOXPARAM->m_nRenew[i]]);
				pLinkBoxOperDlg->m_InkBoxParamList.SetItemText(i, 7, sText);
			}

			if (pINKBOXPARAM->m_nInkBoxRow[i] >= 0 || pINKBOXPARAM->m_nInkBoxRow[i] <= 1)
			{
				char* p[] = { "是", "否" };
				sText.Format("%s", p[pINKBOXPARAM->m_nInkBoxRow[i]]);
				pLinkBoxOperDlg->m_InkBoxParamList.SetItemText(i, 8, sText);
			}
		}

	}
}



void CMainFrame::OnDealRecvKeepAlive(char* pBuff, int nLen, int nIndex)
{
	if (pBuff == NULL || nLen == 0)
		return;

	int nConnectStatus = 0;
	int SMJETINDEX = nIndex;//第一个
	if (SMJETINDEX < 0)
	{
		SMJETINDEX = 0;
	}
	if (SMJETINDEX > 5)
	{
		SMJETINDEX = 5;
	}
	if (pBuff[0] == 0x0E)
	{
		int data = 0;
		for (int i = 0; i < 10; i++)
			data = pBuff[i] & 0xFF;
		//if(pPrintDlg)
		{
			pPrintDlg->nConnectStatus[SMJETINDEX] = pBuff[1];
			pPrintDlg->nPrintStatus[SMJETINDEX] = pBuff[2];
			pPrintDlg->nInkBoxStatus[SMJETINDEX][0] = GetBitValue(pBuff[3], 0);
			pPrintDlg->nInkBoxStatus[SMJETINDEX][1] = GetBitValue(pBuff[3], 1);
			pPrintDlg->nInkBoxStatus[SMJETINDEX][2] = GetBitValue(pBuff[3], 2);
			pPrintDlg->nInkBoxStatus[SMJETINDEX][3] = GetBitValue(pBuff[3], 3);
			pPrintDlg->nTransSpeed[SMJETINDEX] = (bytesToInt((byte*)pBuff, 4, 2)) / 10.0f;

			int PrintedNo = (bytesToInt((byte*)pBuff, 8, 4));

			if (PrintedNo > 0 )
				pPrintDlg->PrintedNo[SMJETINDEX] = PrintedNo;

			//if (SMJETINDEX == 0)
				pPrintDlg->UpdataCtrlData(SMJETINDEX);
		}
	}
}



void CMainFrame::OnDealRecvVersion(char* pBuff, int nLen, int nIndex)
{
	if (pBuff == NULL || nLen == 0)
		return;

	int nConnectStatus = 0;
	int SMJETINDEX = nIndex;//第一个
	if (SMJETINDEX < 0)
	{
		SMJETINDEX = 0;
	}
	if (SMJETINDEX > 5)
	{
		SMJETINDEX = 5;
	}
	if (pBuff[0] == 0x11)
	{
		CString strData(pBuff+2);
		//if(pPrintDlg)
		{
			CString str = "";
			str.Format("%d号机:  %s", SMJETINDEX + 1, strData);
			pFirmWareOperDlg->UpdataCtrlData(SMJETINDEX, str);
		}
	}
}


void CMainFrame::OnDealRecvDataFeedBack(char* pBuff, int nLen, int nSMJETIndex)
{
	if (pBuff == NULL || nLen == 0)
		return;

	if (!isPrint)
	{
		return;
	}

	if (pBuff[0] == 0x0D)
	{
		byte nAck = pBuff[1];

		if (nAck == 1)	//成功
		{
			KillTimer(10 + nSMJETIndex);
			nReSendCnt[nSMJETIndex] = 0;
			m_nAckNum = nAck;

			map<info_head, PRINTDATA*>::iterator iter;
			for (int i = 0; i < 10; i++)
			{
				int printer_index = (short)bytesToInt((byte*)pBuff, 4 + 8 * i, 2);      //打印头编号
				int this_pack_index = (short)bytesToInt((byte*)pBuff, 6 + 8 * i, 2);     //本数据包编号
				int page_index = (int)bytesToInt((byte*)pBuff, 8 + 8 * i, 4);         //打印页编号
				info_head ite;
				ite.printer_index = printer_index;
				ite.page_index = page_index;
				ite.this_pack_index = this_pack_index;

				//CThreadLockHandle lockHandle(&threadLock);                        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁
				iter = m_PRINTDATA_info_head[nSMJETIndex].find(ite);
				if (iter != m_PRINTDATA_info_head[nSMJETIndex].end())
				{
					PRINTDATA*  pPRINTDATA = iter->second;
					if (pPRINTDATA != NULL)
					{
						delete pPRINTDATA;
						pPRINTDATA = NULL;
					}
					m_PRINTDATA_info_head[nSMJETIndex].erase(iter);
				}
			}
			int nCount = 0;
			if (m_PRINTDATA_info_head[nSMJETIndex].size() > 0)
			{
				//CThreadLockHandle lockHandle(&threadLock);                        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁
				for (iter = m_PRINTDATA_info_head[nSMJETIndex].begin(); iter != m_PRINTDATA_info_head[nSMJETIndex].end(); iter++)//未返回的包号，重发
				{
					if (!isPrint)
						return;

					PRINTDATA*  pPRINTDATA = iter->second;
					nCount++;
					if (nCount == 10)
					{
						pPRINTDATA->bNeedAnswer = 0x01;
					}
					else if (nCount > 10)
					{
						break;
					}
					else
					{
						pPRINTDATA->bNeedAnswer = 0;
					}
					OnUdpSendData((char*)pPRINTDATA, 20 + pPRINTDATA->nCurDataLen, nSMJETIndex);
				}
			}

			if (nCount < 10)
			{
				m_nPackageIndex[nSMJETIndex] += 1;
				if (!isPrint)
					return;
				SendPrintData(m_nPrintIndex[nSMJETIndex], m_nPaperIndex[nSMJETIndex], m_nPackageIndex[nSMJETIndex], m_nPaperLenth, m_nPackageDataLenth, nSMJETIndex, 10 - nCount);
			}
			else
			{
				if (!isPrint)
					return;
				SetTimer(10 + nSMJETIndex, 5, NULL);//防止下位机没收到，重发
			}
		}
		else if (nAck == 2)//满了，等会再发
		{
			KillTimer(10 + nSMJETIndex);
			nReSendCnt[nSMJETIndex] = 0;
			m_nAckNum = nAck;
			if (!isPrint)
				return;		
			SetTimer(nSMJETIndex + 2, 10, NULL);
		}
	}
}



void CMainFrame::OnDealRecvFirmWareFeedBack(char* pBuff, int nLen, int nSMJETIndex)
{
	if (pBuff == NULL || nLen == 0)
		return;

	if (pBuff[0] == 0x10)
	{
		byte nAck = pBuff[1];

		if (nAck == 1)	//成功
		{
			short nData = (short)bytesToInt((byte*)pBuff, 2, 2);
			short nSize = _firmDataArray.GetSize();
			if (nData < nSize)
			{
				FirmData fd = _firmDataArray.GetAt(nData);
				OnUdpSendData((char*)(&fd), 1012, _nUpSMJET);
			}
			else if (nData == nSize)
			{
				AfxMessageBox(_T("升级成功"));
			}
		}
		else if (nAck == 2)//重发
		{
			short nData = (short)bytesToInt((byte*)pBuff, 2, 2);
			FirmData fd = _firmDataArray.GetAt(nData - 1);
			OnUdpSendData((char*)(&fd), 1012, _nUpSMJET);
		}
	}
}


void CMainFrame::OnApplicationLook(UINT id)
{
	CWaitCursor wait;

	theApp.m_nAppLook = id;

	switch (theApp.m_nAppLook)
	{
	case ID_VIEW_APPLOOK_WIN_2000:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManager));
		break;

	case ID_VIEW_APPLOOK_OFF_XP:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
		break;

	case ID_VIEW_APPLOOK_WIN_XP:
		CMFCVisualManagerWindows::m_b3DTabsXPTheme = TRUE;
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
		break;

	case ID_VIEW_APPLOOK_OFF_2003:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2003));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	case ID_VIEW_APPLOOK_VS_2005:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2005));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	case ID_VIEW_APPLOOK_VS_2008:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2008));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	case ID_VIEW_APPLOOK_WINDOWS_7:
		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows7));
		CDockingManager::SetDockingMode(DT_SMART);
		break;

	default:
		switch (theApp.m_nAppLook)
		{
		case ID_VIEW_APPLOOK_OFF_2007_BLUE:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_LunaBlue);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_BLACK:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_SILVER:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
			break;

		case ID_VIEW_APPLOOK_OFF_2007_AQUA:
			CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Aqua);
			break;
		}

		CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
		CDockingManager::SetDockingMode(DT_SMART);
	}

	//m_wndOutput.UpdateFonts();
	RedrawWindow(NULL, NULL, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_FRAME | RDW_ERASE);

	theApp.WriteInt(_T("ApplicationLook"), theApp.m_nAppLook);
}

void CMainFrame::OnUpdateApplicationLook(CCmdUI* pCmdUI)
{
	pCmdUI->SetRadio(theApp.m_nAppLook == pCmdUI->m_nID);
}


// CMainFrame类的PreTranslateMessage函数加入：
BOOL CMainFrame::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if (::TranslateAccelerator(GetSafeHwnd(), m_hAccel, pMsg))
		return   TRUE;
	return CFrameWndEx::PreTranslateMessage(pMsg);
}

BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext)
{
	// 基类将执行真正的工作
	if (!CFrameWndEx::LoadFrame(nIDResource, dwDefaultStyle, pParentWnd, pContext))
	{
		return FALSE;
	}
	// 为所有用户工具栏启用自定义按钮
	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);

	for (int i = 0; i < iMaxUserToolbars; i++)
	{
		CMFCToolBar* pUserToolbar = GetUserToolBarByIndex(i);
		if (pUserToolbar != NULL)
		{
			pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
		}
	}

	m_myToolBar2.RemoveButton(5);
	m_myToolBar2.AdjustSizeImmediate();//根据按钮大小调整工具栏
	m_myToolBar2.AdjustLayout();//根据按钮大小调整工具栏


	m_wndToolBar.EnableTextLabels(TRUE);
	m_myToolBar.EnableTextLabels(TRUE);
	m_myToolBar2.EnableTextLabels(TRUE);


	return TRUE;
}


void CMainFrame::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CFrameWndEx::OnSettingChange(uFlags, lpszSection);
	//m_wndOutput.UpdateFonts();
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
int rTimes = 0;
void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == 1)
	{
		if (pPrintDlg->IsWindowVisible())
		{
			rTimes++;
			int r = rTimes % 5;  //10秒刷新一次，防止掉线后不刷新状态
			if (r == 0)
			{
				rTimes = 0;
				pPrintDlg->DefaultCtrlData();
			}

			for (int i = 1; i <= pHardWareSetDlg->getSelectSMJET(); i++)
			{
				int nSMJETIndex = pHardWareSetDlg->getSMJETRealIndex(i) - 1;
				SendKeepAlive(nSMJETIndex);
			}

		}
		//获取当前DPI
		else
		{
			int i = pPrintSetDlg->GetPPIX();
			int j = pPrintSetDlg->GetPPIY();

			int y[2] = { 600, 300 };
			int x[6] = { 600, 300, 200, 150, 100, 75 };

			CString strDPI;
			strDPI.Format(_T("DPI = %d * %d"), y[i], x[j]);
			m_wndStatusBar.SetPaneText(5, strDPI);
			pPrintDlg->RefreshCtrlDPI(strDPI);
		}

		CString strTime;
		// 获取系统当前时间，并保存到curTime   
		CTime curTime = CTime::GetCurrentTime();
		// 格式化curTime，将字符串保存到strTime   
		strTime = curTime.Format(_T("%Y-%m-%d %H:%M:%S"));
		// 在状态栏的时间窗格中显示系统时间字符串   
		m_wndStatusBar.SetPaneText(6, strTime);

		CString str;
		str.Format("页长:%dmm   ", (pPrintSetDlg)->GetPaperLenth());//格式化文本
		m_wndStatusBar.SetPaneText(4, str);

		m_wndStatusBar.SetPaneText(1, sDBPathName);
	}
	if (nIDEvent >= 2 && nIDEvent <= 7)//延时发送
	{
		KillTimer(nIDEvent);
		if (!isPrint)
			return;

		int nSMJETIndex = nIDEvent - 2;

		////从包头开始
		//m_nPackageIndex[nSMJETIndex] = 1;
		////发数据
		//SendPrintData(m_nPrintIndex[nSMJETIndex], m_nPaperIndex[nSMJETIndex], m_nPackageIndex[nSMJETIndex], m_nPaperLenth, m_nPackageDataLenth, nSMJETIndex, 10);

		int nCount = 0;
		if (m_PRINTDATA_info_head[nSMJETIndex].size() > 0)
		{
			//CThreadLockHandle lockHandle(&threadLock);                        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁
			map<info_head, PRINTDATA*>::iterator iter;
			for (iter = m_PRINTDATA_info_head[nSMJETIndex].begin(); iter != m_PRINTDATA_info_head[nSMJETIndex].end(); iter++)//未返回的包号，重发
			{
				if (!isPrint)
					return;

				PRINTDATA*  pPRINTDATA = iter->second;
				nCount++;
				if (nCount == 10)
				{
					pPRINTDATA->bNeedAnswer = 0x01;
				}
				else if (nCount > 10)
				{
					break;
				}
				else
				{
					pPRINTDATA->bNeedAnswer = 0;
				}
				OnUdpSendData((char*)pPRINTDATA, 20 + pPRINTDATA->nCurDataLen, nSMJETIndex);
			}
		}

		if (nCount < 10)
		{
			m_nPackageIndex[nSMJETIndex] += 1;
			if (!isPrint)
				return;
			SendPrintData(m_nPrintIndex[nSMJETIndex], m_nPaperIndex[nSMJETIndex], m_nPackageIndex[nSMJETIndex], m_nPaperLenth, m_nPackageDataLenth, nSMJETIndex, 10 - nCount);
		}
		else
		{
			if (!isPrint)
				return;
			SetTimer(10 + nSMJETIndex, 5, NULL);//防止下位机没收到，重发
		}
	}

	if (nIDEvent >= 10 && nIDEvent <= 15)//定时器，防止丢包，重发
	{
		KillTimer(nIDEvent);
		if (!isPrint)
		{
			return;
		}

		int nSMJETIndex = nIDEvent - 10;
		if (nReSendCnt[nSMJETIndex] > 20)
		{		
			nReSendCnt[nSMJETIndex] = 0;

			CMyDrawView *pView = (CMyDrawView *)GetActiveView();
			if (m_nPaperIndex[nSMJETIndex] == pView->nAllPaperNo && m_nPrinterConut[nSMJETIndex] >= pHardWareSetDlg->GetPrinterNo(nSMJETIndex + 1))
				return;
								
			CString strTemp;
			strTemp.Format(_T("%d 网络异常，请检查网络后按确定，进行续传打印!"), nSMJETIndex + 100);
			if (AfxMessageBox(strTemp, MB_OKCANCEL) == IDOK)
			{
				SetTimer(nIDEvent, 1000, NULL);		
			}
			else
			{
				return;
			}
		}

		//发数据
		SendPrintData(m_nPrintIndex[nSMJETIndex], m_nPaperIndex[nSMJETIndex], m_nPackageIndex[nSMJETIndex], m_nPaperLenth, m_nPackageDataLenth, nSMJETIndex, 1);
		nReSendCnt[nSMJETIndex]++;
	}


	if (nIDEvent >= 20 && nIDEvent <= 25)//定时器，换页等待
	{
		KillTimer(nIDEvent);
		if (!isPrint)
		{
			return;
		}

		int nSMJETIndex = nIDEvent - 20;
		//发数据
		SendPrintData(m_nPrintIndex[nSMJETIndex], m_nPaperIndex[nSMJETIndex], m_nPackageIndex[nSMJETIndex], m_nPaperLenth, m_nPackageDataLenth, nSMJETIndex, 1);
	}

	CFrameWnd::OnTimer(nIDEvent);
}



void CMainFrame::OnMenuitemLine()
{
	// TODO: Add your command handler code here
	CMyDrawView *pView = (CMyDrawView *)GetActiveView();
	pView->OnButtonLine();
}

void CMainFrame::OnMenuitemEllipse()
{
	// TODO: Add your command handler code here
	CMyDrawView *pView = (CMyDrawView *)GetActiveView();
	pView->OnButtonEllipse();
}

void CMainFrame::OnMenuitemBar()
{
	// TODO: Add your command handler code here
	CMyDrawView *pView = (CMyDrawView *)GetActiveView();
	pView->OnButtonTBar();
}

void CMainFrame::OnMenuitemArc()
{
	CMyDrawView *pView = (CMyDrawView *)GetActiveView();
	pView->OnButtonArc();
}

void CMainFrame::OnMenuitemRect()
{
	CMyDrawView *pView = (CMyDrawView *)GetActiveView();
	pView->OnButtonRectangle();
}

void CMainFrame::OnMenuitemRect2()
{
	CMyDrawView *pView = (CMyDrawView *)GetActiveView();
	pView->OnButtonRectangle2();
}

void CMainFrame::OnMenuitemText()
{
	CMyDrawView *pView = (CMyDrawView *)GetActiveView();
	pView->OnButtonText();
}
void CMainFrame::OnMenuitemPic()
{
	CMyDrawView *pView = (CMyDrawView *)GetActiveView();
	pView->OnButtonPic();
}


void CMainFrame::OnMenuitemPrint()
{
	if (pPrintDlg)
	{
		pPrintDlg->ShowWindow(SW_SHOW);
		pPrintDlg->SetFocus();
	}
}
void CMainFrame::OnMenuitemPrintset()
{
	if (pPrintSetDlg)
	{
		pPrintSetDlg->ShowWindow(SW_SHOW);
	}
}

void CMainFrame::OnMenuitemFirmwareset()
{
	if (pFirmWareOperDlg)
	{
		pFirmWareOperDlg->ShowWindow(SW_SHOW);
	}
}

void CMainFrame::OnMenuitemHardwareset()
{
	if (pHardWareSetDlg)
	{
		pHardWareSetDlg->ShowWindow(SW_SHOW);
	}
}

void CMainFrame::OnMenuitemLinkboxset()
{
	if (pLinkBoxOperDlg)
	{
		pLinkBoxOperDlg->ShowWindow(SW_SHOW);
	}
}

void CMainFrame::OnMenuitemPrentcodeerrorset()
{
	// TODO: Add your command handler code here

}
void CMainFrame::OnInitUdpSock(CString sIP, int nPort, int SMJETIndex)
{
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;


	wVersionRequested = MAKEWORD(1, 1);

	err = WSAStartup(wVersionRequested, &wsaData);
	if (err != 0) {
		return;
	}

	if (LOBYTE(wsaData.wVersion) != 1 ||
		HIBYTE(wsaData.wVersion) != 1) {
		WSACleanup();
		return;
	}

	//	SOCKET 
	sockClient[SMJETIndex] = socket(AF_INET, SOCK_DGRAM, 0);
	if (SOCKET_ERROR == sockClient[SMJETIndex])
	{
		printf("Create Socket Error!");
		return;
	}

	//	SOCKADDR_IN addrSrv;
	addrSrv[SMJETIndex].sin_addr.S_un.S_addr = inet_addr(sIP);
	addrSrv[SMJETIndex].sin_family = AF_INET;
	addrSrv[SMJETIndex].sin_port = htons(nPort);


	BOOL bDisable = TRUE;
	setsockopt(sockClient[SMJETIndex], IPPROTO_TCP, TCP_NODELAY, (char*)&bDisable, sizeof(bDisable));

	int nRecvBuf = 32 * 1024;//设置为32K
	setsockopt(sockClient[SMJETIndex], SOL_SOCKET, SO_RCVBUF, (const char*)&nRecvBuf, sizeof(int));
	//发送缓冲区
	int nSendBuf = 32 * 1024;//设置为32K
	setsockopt(sockClient[SMJETIndex], SOL_SOCKET, SO_SNDBUF, (const char*)&nSendBuf, sizeof(int));

}

void CMainFrame::OnEndTest()
{
	for (int i = 0; i < 6; i++)
	{
		KillTimer(10 + i);
	}

	CMyDrawView *pView = (CMyDrawView *)GetActiveView();

	pView->isGetData = false;
	pView->Invalidate(FALSE);
	isPrint = false;

}


void CMainFrame::OnUdpSendData(char* pBuff, int nLen, int nSMJETIndex)
{
	//	int nLen = sizeof(pBuff);
	for (int i = 0; i < nLen; i++)
		byte bData = pBuff[i] & 0xFF;

	sendto(sockClient[nSMJETIndex], (char*)pBuff, nLen, 0,
		(SOCKADDR*)&(addrSrv[nSMJETIndex]), sizeof(SOCKADDR));

}

void CMainFrame::OnUdpClose(int nIndex)
{
	closesocket(sockClient[nIndex]);
	WSACleanup();
}


/////发送心跳
void CMainFrame::SendKeepAlive(int nSMJETIndex)
{
	byte m_KeepAlice[2] = { 0 };
	m_KeepAlice[0] = 4;
	if (isPrint)
	{
		m_KeepAlice[1] = 1;
	}
	else
	{
		m_KeepAlice[1] = 2;
	}

	OnUdpSendData((char*)m_KeepAlice, 2, nSMJETIndex);
}

void CMainFrame::SendReadInkBoxParam(int nSMJETIndex)
{
	char pBuff[] = { 13, 0 };
	OnUdpSendData(pBuff, 2, nSMJETIndex);
}
void CMainFrame::SendWriteInkBoxParam(int nSMJETIndex)
{
	/*
	struct INKBOXPARAM{
	byte nCode;
	byte nLen;
	byte m_nPulseTime[4];//脉冲时间
	short m_nInkBoxTemp[4];//温度
	short m_nInkBoxVol[4];//电压
	byte m_nIsWarm[4];//是否加热
	byte m_nIsWash[4];//清洗
	byte m_nRenew[4];//换墨盒
	byte m_nInkBoxRow[4];//单列还是双列
	};
	*/
	INKBOXPARAM* pINKBOXPARAM = new INKBOXPARAM;
	pINKBOXPARAM->nCode = 0x01;
	pINKBOXPARAM->nLen = 36;

	for (int i = 0; i < 4; i++)
	{
		pINKBOXPARAM->m_nPulseTime[i] = (int)(pLinkBoxOperDlg->GetInkBoxParam(i, 1) * 10 + 0.5);
		//		pINKBOXPARAM->m_nBadPint[i] = pHardWareSetDlg->GetInkBoxParam(i,2);
		pINKBOXPARAM->m_nInkBoxTemp[i] = (int)(pLinkBoxOperDlg->GetInkBoxParam(i, 3) * 10 + 0.5);
		pINKBOXPARAM->m_nInkBoxVol[i] = (int)(pLinkBoxOperDlg->GetInkBoxParam(i, 4) * 10 + 0.5);
		pINKBOXPARAM->m_nIsWarm[i] = (int)pLinkBoxOperDlg->GetInkBoxParam(i, 5);
		pINKBOXPARAM->m_nIsWash[i] = (int)pLinkBoxOperDlg->GetInkBoxParam(i, 6);
		pINKBOXPARAM->m_nRenew[i] = (int)pLinkBoxOperDlg->GetInkBoxParam(i, 7);
		pINKBOXPARAM->m_nInkBoxRow[i] = (int)pLinkBoxOperDlg->GetInkBoxParam(i, 8);
	}

	OnUdpSendData((char*)pINKBOXPARAM, 38, nSMJETIndex);

	delete pINKBOXPARAM;
	pINKBOXPARAM = NULL;

}


void CMainFrame::SendWashBoxParam(int nSMJETIndex)
{
	byte  buff[6];
	buff[0] = 8;
	buff[1] = 0;
	for (int i = 0; i < 4; i++)
	{
		buff[2+i] = (int)pLinkBoxOperDlg->GetInkBoxParam(i, 6);;
	}

	OnUdpSendData((char*)buff, 6, nSMJETIndex);

}

void CMainFrame::SendSetSMJETIndex(int OrgSMJETIndex, int SetSMJETIndex)
{
	byte  buff[2];
	buff[0] = 5;
	buff[1] = (byte)SetSMJETIndex;
	OnUdpSendData((char*)buff, 2, OrgSMJETIndex);
}

void CMainFrame::SendStartData(int nSMJETIndex)
{
	//	short nCode;				//标识码
	//	byte  bPPIX;				//X分辨率
	//	byte  bPPIY;				//Y分辨率
	//	short nPageLen;				//页长
	//	bool  bSimulateEysTrig;		//模拟电眼
	//	byte  LenthSimulateEysTrig;	//模拟电眼距离
	//	byte  bInkBoxSelect;		//墨盒选择
	//	byte  bInkBoxRow;			//左列，右列，双列打印
	//	short nDistancePenToEys[4];	//墨盒到电眼距离
	//	byte  bInkBoxDirect;		//墨盒方向
	//	byte  bEncodeerDistance;	//编码器单个脉冲对应距离


	CMyDrawView *pView = (CMyDrawView *)GetActiveView();

	STARTSTRUCT *pSTARTSTRUCT = new STARTSTRUCT;
	pSTARTSTRUCT->nCode = 0x02;
	pSTARTSTRUCT->nLen = 26;
	pSTARTSTRUCT->bPPIX = pPrintSetDlg->GetPPIY();
	pSTARTSTRUCT->bPPIY = pPrintSetDlg->GetPPIX();
	m_nPaperLenth = pPrintSetDlg->GetPaperLenth();
	pSTARTSTRUCT->nPageLen = (int)(((int)(m_nPaperLenth * 600 / 25.4 + 0.5) + 4) / 8) * 8;
	if (pView->DPIY > 0)//水平300dpi,
	{
		int x[6] = { 600, 300, 200, 150, 100, 75 };
		float X = x[pView->DPIY];
		pSTARTSTRUCT->nPageLen = (int)(((int)(m_nPaperLenth * X / 25.4 + 0.5) + 4) / 8) * 8;
	}
	pSTARTSTRUCT->bSimulateEysTrig = pPrintSetDlg->isSimEye();
	pSTARTSTRUCT->bReserve = int(pPrintSetDlg->m_dHeibiao * 10 + 0.5);
	pSTARTSTRUCT->LenthSimulateEysTrig = pPrintSetDlg->getSimeyeDistance();
	pSTARTSTRUCT->bInkBoxSelect = pHardWareSetDlg->GetSelectPrinter(nSMJETIndex + 1);
	pSTARTSTRUCT->bInkBoxRow[0] = 0x02;
	pSTARTSTRUCT->bInkBoxRow[1] = 0x02;
	pSTARTSTRUCT->bInkBoxRow[2] = 0x02;
	pSTARTSTRUCT->bInkBoxRow[3] = 0x02;

	pSTARTSTRUCT->nDistancePenToEys[0] = (int)(pHardWareSetDlg->GetInkBoxToEyeDis(nSMJETIndex, 0) * 10 + 0.5);
	pSTARTSTRUCT->nDistancePenToEys[1] = (int)(pHardWareSetDlg->GetInkBoxToEyeDis(nSMJETIndex, 1) * 10 + 0.5);
	pSTARTSTRUCT->nDistancePenToEys[2] = (int)(pHardWareSetDlg->GetInkBoxToEyeDis(nSMJETIndex, 2) * 10 + 0.5);
	pSTARTSTRUCT->nDistancePenToEys[3] = (int)(pHardWareSetDlg->GetInkBoxToEyeDis(nSMJETIndex, 3) * 10 + 0.5);

	pSTARTSTRUCT->bInkBoxDirect = pHardWareSetDlg->GetInkBoxDirect(nSMJETIndex);

	pSTARTSTRUCT->bEncodeerDistance = (int)(pHardWareSetDlg->GetEncoderSpeed() * 10000);

	OnUdpSendData((char*)pSTARTSTRUCT, 28, nSMJETIndex);

	delete pSTARTSTRUCT;
	pSTARTSTRUCT = NULL;

}



void CMainFrame::SendPrintData(int nPrinterIndex, int nPaperIndex, int bPackageIndex, int nPaperLenth, int nDataPackageSize, int nSMJETIndex, int nSendCount)
{
	/*
	struct PRINTDATA{
	//    short nCode;
	byte nCode;
	byte nLen;
	short nPrinterIndex;
	short nPaperIndex;
	short nPaperPackageNo;
	short  bCurPackageIndex;//
	int nPaperTotalData;//一页纸对应点阵数
	//   int  nCurOffset;
	short  nCurDataLen;//一个数据包点阵数
	byte  bData[1000];
	};
	*/

	int nCount = 0;
	CMyDrawView *pView = (CMyDrawView *)GetActiveView();

	int nCol = (int)(((int)(m_nPaperLenth * 600 / 25.4 + 0.5) + 4) / 8) * 8;
	if (pView->DPIY > 0)  //水平300dpi
	{
		int x[6] = { 600, 300, 200, 150, 100, 75 };
		float X = x[pView->DPIY];

		nCol = (int)(((int)(m_nPaperLenth * X / 25.4 + 0.5) + 4) / 8) * 8;
	}
	int  nTotalPackageSize = nCol * 40;
	if (pView->DPIX > 0)  //垂直300dpi
	{
		nTotalPackageSize = nCol * 20;
	}

	int nPackageNo = 0;
	if (nTotalPackageSize%nDataPackageSize == 0)
	{
		nPackageNo = nTotalPackageSize / nDataPackageSize;
	}
	else
	{
		nPackageNo = nTotalPackageSize / nDataPackageSize + 1;
	}

	nextPackage:
	if (!isPrint)
	{
		return;
	}
	if (bPackageIndex > nPackageNo)
	{
		CString sText = "";
		if (m_nPrinterConut[nSMJETIndex] < pHardWareSetDlg->GetPrinterNo(nSMJETIndex + 1))
		{
			m_nPrinterConut[nSMJETIndex]++;
			m_nPrintIndex[nSMJETIndex] = pHardWareSetDlg->GetPrinterIndex(nSMJETIndex + 1, m_nPrinterConut[nSMJETIndex]);
			nPrinterIndex = m_nPrintIndex[nSMJETIndex];
		}
		else
		{		
			//获取有数据的arm
			int o = 0;
			for (; o < 6; o++)
			{
				if (hasData[o]) break;
			}
			//下一张纸
			{
				nPaperIndex++;
				if (nPaperIndex > (pView->nAllPaperNo))
				{
					sText.Format("%d--%d--%d--%d", nPaperIndex, bPackageIndex, nPackageNo, nPrinterIndex);
					KillTimer(10 + nSMJETIndex);
					nReSendCnt[nSMJETIndex] = 0;
					//AfxMessageBox(_T("传输完成---") + sText, MB_OK | MB_SYSTEMMODAL);

					if ((m_nPaperIndex[0] == pView->nAllPaperNo || m_nPaperIndex[0] == 1) && (m_nPrinterConut[0] >= pHardWareSetDlg->GetPrinterNo(0 + 1)))
						if ((m_nPaperIndex[1] == pView->nAllPaperNo || m_nPaperIndex[1] == 1) && (m_nPrinterConut[1] >= pHardWareSetDlg->GetPrinterNo(1 + 1)))
							if ((m_nPaperIndex[2] == pView->nAllPaperNo || m_nPaperIndex[2] == 1) && (m_nPrinterConut[2] >= pHardWareSetDlg->GetPrinterNo(2 + 1)))
								if ((m_nPaperIndex[3] == pView->nAllPaperNo || m_nPaperIndex[3] == 1) && (m_nPrinterConut[3] >= pHardWareSetDlg->GetPrinterNo(3 + 1)))
									if ((m_nPaperIndex[4] == pView->nAllPaperNo || m_nPaperIndex[4] == 1) && (m_nPrinterConut[4] >= pHardWareSetDlg->GetPrinterNo(4 + 1)))
										if ((m_nPaperIndex[5] == pView->nAllPaperNo || m_nPaperIndex[5] == 1) && (m_nPrinterConut[5] >= pHardWareSetDlg->GetPrinterNo(5 + 1)))
										{
											//AfxMessageBox(_T("传输完成---") + sText, MB_OK | MB_SYSTEMMODAL);

											int result = AfxMessageBox(_T("数据库已经打印完，是否重新打印？"), MB_YESNO | MB_SYSTEMMODAL);
											switch (result)
											{
											case IDYES:
												for (int i = 0; i < 6; i++)
												{
													KillTimer(10 + i);
												}
												pView->m_NextPageParam.clear();
												pPrintDlg->SetFocus();
												for (int i = 0; i < pView->m_nSMJET; i++)
												{
													m_nPaperIndex[i] = 1;

													//for (int j = 0; j < 4; j++)
													//	pView->SetRepPoint(i, j, pHardWareSetDlg->GetBadPointNum(i, j));

													pView->Preparedata(m_nPaperIndex[i], i, true);
													pView->NextPage(m_nPaperIndex[i] + 1, i);
													//显示正在打印第几页
													pPrintDlg->RefreshCtrlPageData(m_nPaperIndex[i]);

													SendPrintData(m_nPrintIndex[i], m_nPaperIndex[i], m_nPackageIndex[i], m_nPaperLenth, m_nPackageDataLenth, i, 10);

												}
												break;
											case IDNO:

												pView->isGetData = false;
												pView->Invalidate(FALSE);
												break;
											}
										}


					//int result = AfxMessageBox(_T("清空下位机缓存？"), MB_YESNO | MB_SYSTEMMODAL);
					//switch (result)
					//{
					//case IDYES:
					//	isPrint = false;
					//	pPrintDlg->OnButtonCancelPrint();
					//	break;
					//case IDNO:
					//	break;
					//}
					return;
				}
				
				//换页
				pView->isGetData = true;
				{
					NextPageParam it;
					it.nPageIndex = nPaperIndex;         //打印头编号
					it.nSMJET = nSMJETIndex;              //打印页编号
					
					map<NextPageParam, int>::iterator iter = pView->m_NextPageParam.find(it);
					if (iter != pView->m_NextPageParam.end())
					{	
						nReSendCnt[nSMJETIndex] = 0;
						memcpy(pView->bPrintData[nSMJETIndex], pView->bPrintDataNext[nSMJETIndex], sizeof(pView->bPrintDataNext[nSMJETIndex]));

						{
							//CThreadLockHandle lockHandle(&threadLock);        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁
							//从map里面删掉，不然会越来越大
							pView->m_NextPageParam.erase(iter);
						}
						

						pView->NextPage(nPaperIndex + 1, nSMJETIndex);					
					}
					else//如果没有缓冲完，则等待
					{				
						if (!isPrint)
							return;
						{
							SetTimer(20 + nSMJETIndex, 8, NULL);
							return;
						}						
					}

					//计算每10页的速度
					if ((nPaperIndex - 3) % 5 == 0)
					{
						if (SpeedTime[nSMJETIndex] == 0)
						{
							SpeedTime[nSMJETIndex] = GetTickCount();
						}
						else
						{
							DWORD Speed = GetTickCount() - SpeedTime[nSMJETIndex];
							if (nSMJETIndex == o)
							{
								pPrintDlg->RefreshCtrlSpeedData(Speed * 2);
							}
							SpeedTime[nSMJETIndex] = GetTickCount();
						}
					}

					if (nSMJETIndex == o)
					{
						//显示正在打印第几页
						pPrintDlg->RefreshCtrlPageData(nPaperIndex);
					}
				}
			}

			m_nPrintIndex[nSMJETIndex] = 1;
			nPrinterIndex = m_nPrintIndex[nSMJETIndex];
			m_nPrinterConut[nSMJETIndex] = 1;

		}
		m_nPaperIndex[nSMJETIndex] = nPaperIndex;
		m_nPackageIndex[nSMJETIndex] = bPackageIndex = 1;
	}

	if (nPaperIndex > (pView->nAllPaperNo))
	{
		if (m_nPrinterConut[nSMJETIndex] >= pHardWareSetDlg->GetPrinterNo(nSMJETIndex + 1))
		{
			KillTimer(10 + nSMJETIndex);
			nReSendCnt[nSMJETIndex] = 0;
			CString sText = "";
			sText.Format("%d--%d--%d--%d", nPaperIndex, bPackageIndex, nPackageNo, nPrinterIndex);
			//AfxMessageBox(_T("传输完成---") + sText, MB_OK | MB_SYSTEMMODAL);
			if ((m_nPaperIndex[0] == pView->nAllPaperNo || m_nPaperIndex[0] == 1) && (m_nPrinterConut[0] >= pHardWareSetDlg->GetPrinterNo(0 + 1)))
				if ((m_nPaperIndex[1] == pView->nAllPaperNo || m_nPaperIndex[1] == 1) && (m_nPrinterConut[1] >= pHardWareSetDlg->GetPrinterNo(1 + 1)))
					if ((m_nPaperIndex[2] == pView->nAllPaperNo || m_nPaperIndex[2] == 1) && (m_nPrinterConut[2] >= pHardWareSetDlg->GetPrinterNo(2 + 1)))
						if ((m_nPaperIndex[3] == pView->nAllPaperNo || m_nPaperIndex[3] == 1) && (m_nPrinterConut[3] >= pHardWareSetDlg->GetPrinterNo(3 + 1)))
							if ((m_nPaperIndex[4] == pView->nAllPaperNo || m_nPaperIndex[4] == 1) && (m_nPrinterConut[4] >= pHardWareSetDlg->GetPrinterNo(4 + 1)))
								if ((m_nPaperIndex[5] == pView->nAllPaperNo || m_nPaperIndex[5] == 1) && (m_nPrinterConut[5] >= pHardWareSetDlg->GetPrinterNo(5 + 1)))
								{
									//AfxMessageBox(_T("传输完成---") + sText, MB_OK | MB_SYSTEMMODAL);

									int result = AfxMessageBox(_T("数据库已经打印完，是否重新打印？"), MB_YESNO | MB_SYSTEMMODAL);
									switch (result)
									{
									case IDYES:
										for (int i = 0; i < 6; i++)
										{
											KillTimer(10 + i);
										}
										pView->m_NextPageParam.clear();
										pPrintDlg->SetFocus();
										for (int i = 0; i < pView->m_nSMJET; i++)
										{
											m_nPaperIndex[i] = 1;

											//for (int j = 0; j < 4; j++)
											//	pView->SetRepPoint(i, j, pHardWareSetDlg->GetBadPointNum(i, j));

											pView->Preparedata(m_nPaperIndex[i], i, true);
											pView->NextPage(m_nPaperIndex[i] + 1, i);
											//显示正在打印第几页
											pPrintDlg->RefreshCtrlPageData(m_nPaperIndex[i]);

											SendPrintData(m_nPrintIndex[i], m_nPaperIndex[i], m_nPackageIndex[i], m_nPaperLenth, m_nPackageDataLenth, i, 10);

										}
										break;
									case IDNO:

										pView->isGetData = false;
										pView->Invalidate(FALSE);
										break;
									}
								}

			//int result = AfxMessageBox(_T("清空下位机缓存？"), MB_YESNO | MB_SYSTEMMODAL);
			//switch (result)
			//{
			//case IDYES:
			//	isPrint = false;
			//	pPrintDlg->OnButtonCancelPrint();
			//	break;
			//case IDNO:
			//	break;
			//}
			return;
		}
	}

	PRINTDATA* pPRINTDATA = new PRINTDATA;

	pPRINTDATA->nCode = 0x03;
	pPRINTDATA->bNeedAnswer = 0;	//是否需要回应
	if (nSendCount == 1)
	{
		pPRINTDATA->bNeedAnswer = 0x01;	//重发，需要回应
	}
	pPRINTDATA->nPrinterIndex = nPrinterIndex;
	pPRINTDATA->nPaperIndex = m_nPaperIndex[nSMJETIndex];

	pPRINTDATA->nPaperPackageNo = nPackageNo;
	pPRINTDATA->bCurPackageIndex = bPackageIndex;
	pPRINTDATA->nPaperTotalData = nTotalPackageSize;

	pPRINTDATA->nCurFill = 0;         //填充，暂时不用
	pPRINTDATA->nCurDataLen = nDataPackageSize;

	int nOffset = (bPackageIndex - 1)*nDataPackageSize;
	bool  isHaveData = false;//判断包里面是否有有效数据，没有有效数据则不发

	if (pView->DPIX > 0)//300dpi,一列有效点20个
	{
		for (int i = 0; i < pPRINTDATA->nCurDataLen; i++)
		{
			int m = (i + nOffset) / 20;
			int n = (i + nOffset) % 20;

			byte bTmp = pView->bPrintData[nSMJETIndex][m][n + (nPrinterIndex - 1) * 20];
			if (bTmp != 0x00)
				isHaveData = true;
			pPRINTDATA->bData[i] = bTmp;
		}
	}
	else
	{
		for (int i = 0; i < pPRINTDATA->nCurDataLen; i++)
		{
			int m = (i + nOffset) / 40;
			int n = (i + nOffset) % 40;

			byte bTmp = pView->bPrintData[nSMJETIndex][m][n + (nPrinterIndex - 1) * 40];
			if (bTmp != 0x00)
				isHaveData = true;
			pPRINTDATA->bData[i] = bTmp;
		}
	}

	if (bPackageIndex + 1 > nPackageNo) //每次发完一页
	{
		nCount++;
		pPRINTDATA->bNeedAnswer = 0x01;
		OnUdpSendData((char*)pPRINTDATA, 20 + pPRINTDATA->nCurDataLen, nSMJETIndex);

		info_head it;
		it.printer_index = pPRINTDATA->nPrinterIndex;         //打印头编号
		it.page_index = pPRINTDATA->nPaperIndex;              //打印页编号
		it.this_pack_index = pPRINTDATA->bCurPackageIndex;    //本数据包编号
		{
			//CThreadLockHandle lockHandle(&threadLock);        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁
			map<info_head, PRINTDATA*>::iterator iter = m_PRINTDATA_info_head[nSMJETIndex].find(it);
			if (iter == m_PRINTDATA_info_head[nSMJETIndex].end())
			{
				m_PRINTDATA_info_head[nSMJETIndex].insert(make_pair(it, pPRINTDATA));
			}
			else
			{
				if (pPRINTDATA != NULL)
				{
					delete pPRINTDATA;
					pPRINTDATA = NULL;
				}
			}
		}

		if (!isPrint)
			return;
		SetTimer(10 + nSMJETIndex, 5, NULL);//防止下位机没收到，重发
		return;	
	}
	else if (isHaveData || bPackageIndex + 5 > nPackageNo)//有数据，或者一页最后5个包  必发
	{
		nCount++;
		if (nCount == nSendCount)
		{
			pPRINTDATA->bNeedAnswer = 0x01;
		}
		OnUdpSendData((char*)pPRINTDATA, 20 + pPRINTDATA->nCurDataLen, nSMJETIndex);

		info_head it;
		it.printer_index = pPRINTDATA->nPrinterIndex;         //打印头编号
		it.page_index = pPRINTDATA->nPaperIndex;              //打印页编号
		it.this_pack_index = pPRINTDATA->bCurPackageIndex;    //本数据包编号
		{
			//CThreadLockHandle lockHandle(&threadLock);        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁
			map<info_head, PRINTDATA*>::iterator iter = m_PRINTDATA_info_head[nSMJETIndex].find(it);
			if (iter == m_PRINTDATA_info_head[nSMJETIndex].end())
			{
				m_PRINTDATA_info_head[nSMJETIndex].insert(make_pair(it, pPRINTDATA));
			}
			else
			{
				if (pPRINTDATA != NULL)
				{
					delete pPRINTDATA;
					pPRINTDATA = NULL;
				}
			}
		}

		if (nCount == nSendCount)
		{
			if (!isPrint)
				return;
			SetTimer(10 + nSMJETIndex, 5, NULL);//防止下位机没收到，重发
			return;
		}
		else
		{
			if (!isPrint)
				return;
			bPackageIndex++;
			m_nPackageIndex[nSMJETIndex] += 1;
			goto nextPackage;
		}
	}
	else  //如果包里面没有有效数据直接跳到下一包
	{
		delete pPRINTDATA;
		pPRINTDATA = NULL;
		{
			if (!isPrint)
				return;
			bPackageIndex++;
			m_nPackageIndex[nSMJETIndex] += 1;
			goto nextPackage;
		}
	}
}




void CMainFrame::CancelPrint()
{

}

////读墨盒参数
void CMainFrame::RequstInkBoxParam(int nSMJETIndex)
{
	byte bBuff[2] = { 0 };
	bBuff[0] = 0x0C;
	OnUdpSendData((char*)bBuff, 2, nSMJETIndex);
}

//写墨盒参数
void CMainFrame::WriteInboxParam(int nSMJETIndex)
{

}




//index 从0开始
int CMainFrame::GetBitValue(int nValue, int index)
{
	if (index > (8 * sizeof(nValue) - 1))
		return 0;
	nValue = nValue >> index;
	return nValue % 2;
}

//低位在前，高位在后
int CMainFrame::bytesToInt(byte* src, int offset, int nLen)
{
	int value = 0;
	if (nLen == 4)
	{
		value = (int)(((src[offset + 0] & 0xFF) << 0)
			| ((src[offset + 1] & 0xFF) << 8)
			| ((src[offset + 2] & 0xFF) << 16)
			| ((src[offset + 3] & 0xFF) << 24));
	}
	else if (nLen == 2)
	{
		int N1 = (src[offset] & 0xFF);
		int N2 = (src[offset + 1]) & 0xFF;

		value = (N1) | (N2 << 8);

		value = (int)(((src[offset] & 0xFF) << 0)
			| ((src[offset + 1] & 0xFF) << 8));
	}


	return value;
}


void CMainFrame::OnMenuitemSettingDb()
{
	// TODO: Add your command handler code here
	pDBSettingDlg->ShowWindow(SW_SHOW);
}


#include <fstream>
FILETYPE GetTextFileType(const CString & strFileName)
{
	FILETYPE fileType = ANSI;
	ifstream file;
	file.open(strFileName, std::ios_base::in);

	bool bUnicodeFile = false;
	if (file.good())
	{
		char szFlag[3] = { 0 };
		file.read(szFlag, sizeof(char) * 3);
		if ((unsigned char)szFlag[0] == 0xFF
			&& (unsigned char)szFlag[1] == 0xFE)
		{
			fileType = UNICODE;
		}
		else if ((unsigned char)szFlag[0] == 0xEF
			&& (unsigned char)szFlag[1] == 0xBB
			&& (unsigned char)szFlag[2] == 0xBF)
		{
			fileType = UTF8;
		}
	}

	file.close();

	return fileType;
}




string UTF8ToGB(const char* str)
{
	string result;
	WCHAR *strSrc;
	LPSTR szRes;

	//获得临时变量的大小
	int i = MultiByteToWideChar(CP_UTF8, 0, str, -1, NULL, 0);
	strSrc = new WCHAR[i + 1];
	MultiByteToWideChar(CP_UTF8, 0, str, -1, strSrc, i);

	//获得临时变量的大小
	i = WideCharToMultiByte(CP_ACP, 0, strSrc, -1, NULL, 0, NULL, NULL);
	szRes = new CHAR[i + 1];
	WideCharToMultiByte(CP_ACP, 0, strSrc, -1, szRes, i, NULL, NULL);

	result = szRes;
	delete[]strSrc;
	delete[]szRes;

	return result;
}



void CMainFrame::OnMenuitemImportDb()
{
	// TODO: Add your command handler code here
	CFileDialog Filedlg(TRUE, NULL, NULL,
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |
		OFN_ALLOWMULTISELECT | OFN_EXPLORER,
		"Data Files (*.txt)|*.txt|All files(*.*)|*.*||",
		NULL);

	char pBuf[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, pBuf);
	CString sPath = "";
	sPath.Format("%s", pBuf);

	Filedlg.m_ofn.lpstrTitle = "导入数据库文件";//改变标题
	Filedlg.m_ofn.lpstrInitialDir = sPath;//设定打开初始目录
	//Filedlg.m_ofn.lStructSize = structsize;
	Filedlg.m_ofn.nMaxFile = MAX_PATH;
	CString strFilePath = "";
	if (Filedlg.DoModal() == IDOK)
	{
		strFilePath = Filedlg.GetPathName();
		sDBPathName = strFilePath;
		ReadPrintData(strFilePath);
	}

}



//DWORD WINAPI SetPrintDataThread(LPVOID lpParam)
UINT _stdcall SetPrintDataThreadFun(LPVOID lpParam)
{
	CMainFrame*pFrm = (CMainFrame*)AfxGetApp()->m_pMainWnd;//获得主窗口指标

	if (pFrm == NULL)
		return 1;


	if (pFrm->pPrintDlg)
	{
		CMyDrawView* MyView = (CMyDrawView*)lpParam;
		if (MyView == NULL)
			return 1;

		FILETYPE fileType = GetTextFileType(pFrm->sDBPathName);

		FILE * pFile;
		pFile = fopen(pFrm->sDBPathName, "r");
		fseek(pFile, 0, SEEK_END);//指针移到文件结尾
		int len = ftell(pFile);//获取文件大小
		rewind(pFile);//指针移到文件头

		if (UTF8 == fileType)
		{
			fseek(pFile, sizeof(char) * 3, 0);
		}
		else if (UNICODE == fileType)
		{
			fseek(pFile, sizeof(char) * 2, 0);
		}
		int nOffset = 0;
		char bBuff = 0;
		wchar_t szBuf = 0;
		///记录号是否使用分隔符
		bool isUseSep = pFrm->pDBSettingDlg->isUseFieldSymbol();
		//////记录号分隔符assic
		int nRecordAssic = pFrm->pDBSettingDlg->GetRecordFieldSymbol();
		////记录号固定字段数
		int nSepNum = pFrm->pDBSettingDlg->GetSeprateNum();
		/////字段分段符assic//
		int nSepAssic = pFrm->pDBSettingDlg->GetSeprateFieldSymbol();

		int nSepCnt = 0;//字段计数
		int nRecordCnt = 0;
		CString sData = "";
		CString sText = "";
		bool bFlag = false;

		CMyProgress m_MyProgress;//用于处理进度条的类

		m_MyProgress.CreateProgressDlg(MyView->nAllPaperNo);
		m_MyProgress.SetCaption(_T("进度条"));
		m_MyProgress.SetStatus(_T("读取数据中......"));


		pFrm->pPrintDlg->CreateRecordList(MyView->nMaxColNum);
		//CString sTmp = "";
		//sTmp.Format("%d", nRecordCnt+1);
		//pFrm->pPrintDlg->m_RecordNumList.InsertItem(nRecordCnt, sTmp);


		while (nOffset < len) //
		{
			bFlag = false;
			if (UNICODE == fileType)
			{
				memset(&szBuf, 0, 1);
				fread(&szBuf, sizeof(wchar_t), 1, pFile);
				sText=szBuf;
				if (szBuf == nSepAssic)
				{
					////得到一个段，存入list中
					//pFrm->pPrintDlg->m_RecordNumList.SetItemText(nRecordCnt, nSepCnt + 1, sData);
					pFrm->pPrintDlg->sPrintData[(nRecordCnt)* MyView->nMaxColNum + nSepCnt + 1] = sData;
					nSepCnt++;
					sData = "";
					bFlag = true;
				}
				if (szBuf == nRecordAssic)//记录号分隔符
				{
					if (isUseSep)//使用分段符
					{
						if (!bFlag && sData.GetLength() > 0)
						{
								pFrm->pPrintDlg->sPrintData[(nRecordCnt)* MyView->nMaxColNum + nSepCnt + 1] = sData;
						}

						nSepCnt = 0;
						//////下一个字段就是一行
						nRecordCnt++;
						sData = "";
						bFlag = true;

						if (nRecordCnt % 10000 == 0)
						{
							if (!m_MyProgress.DoProgress(nRecordCnt))
							{
								m_MyProgress.DestroyWindow();
								return 0;
							}
						}
						//sTmp.Format("%d", nRecordCnt + 1);
						//pFrm->pPrintDlg->m_RecordNumList.InsertItem(nRecordCnt, sTmp);
					}
				}

				if (!bFlag)
				{
					sData += sText;
				}
				if (!isUseSep)//不使用分段符(固定段数)
				{
					if (nSepCnt == nSepNum)//
					{
						nSepCnt = 0;
						//////下一个字段就是一行
						nRecordCnt++;

						if (nRecordCnt % 1000 == 0)
						{
							if (!m_MyProgress.DoProgress(nRecordCnt* MyView->nMaxColNum))
							{
								m_MyProgress.DestroyWindow();
								return 0;
							}
						}
						//sTmp.Format("%d", nRecordCnt + 1);
						//pFrm->pPrintDlg->m_RecordNumList.InsertItem(nRecordCnt, sTmp);
					}
				}
				nOffset++;
				nOffset++;
			}
			else
			{
				memset(&bBuff, 0, 1);
				fread(&bBuff, sizeof(char), 1, pFile);
				sText.Format("%c", bBuff);
				if (bBuff == nSepAssic)
				{
					////得到一个段，存入list中
					//pFrm->pPrintDlg->m_RecordNumList.SetItemText(nRecordCnt, nSepCnt + 1, sData);
					if (UTF8 == fileType)
						pFrm->pPrintDlg->sPrintData[(nRecordCnt)* MyView->nMaxColNum + nSepCnt + 1] = UTF8ToGB(sData.GetBuffer(0)).c_str();
					else
						pFrm->pPrintDlg->sPrintData[(nRecordCnt)* MyView->nMaxColNum + nSepCnt + 1] = sData;
					nSepCnt++;
					sData = "";
					bFlag = true;
				}
				if (bBuff == nRecordAssic)//记录号分隔符
				{
					if (isUseSep)//使用分段符
					{
						if (!bFlag && sData.GetLength() > 0)
						{
							//pFrm->pPrintDlg->m_RecordNumList.SetItemText(nRecordCnt, nSepCnt + 1, sData);
							if (UTF8 == fileType)
								pFrm->pPrintDlg->sPrintData[(nRecordCnt)* MyView->nMaxColNum + nSepCnt + 1] = UTF8ToGB(sData.GetBuffer(0)).c_str();
							else
								pFrm->pPrintDlg->sPrintData[(nRecordCnt)* MyView->nMaxColNum + nSepCnt + 1] = sData;

						}

						nSepCnt = 0;
						//////下一个字段就是一行
						nRecordCnt++;
						sData = "";
						bFlag = true;

						if (nRecordCnt % 10000 == 0)
						{
							if (!m_MyProgress.DoProgress(nRecordCnt))
							{
								m_MyProgress.DestroyWindow();
								return 0;
							}
						}

						//sTmp.Format("%d", nRecordCnt + 1);
						//pFrm->pPrintDlg->m_RecordNumList.InsertItem(nRecordCnt, sTmp);
					}
				}

				if (!bFlag)
				{
					sData += sText;
				}
				if (!isUseSep)//不使用分段符(固定段数)
				{
					if (nSepCnt == nSepNum)//
					{
						nSepCnt = 0;
						//////下一个字段就是一行
						nRecordCnt++;

						if (nRecordCnt % 1000 == 0)
						{
							if (!m_MyProgress.DoProgress(nRecordCnt* MyView->nMaxColNum))
							{
								m_MyProgress.DestroyWindow();
								return 0;
							}
						}
						//sTmp.Format("%d", nRecordCnt + 1);
						//pFrm->pPrintDlg->m_RecordNumList.InsertItem(nRecordCnt, sTmp);
					}
				}
				nOffset++;
			}

		}//while

		fclose(pFile); /* 关闭文件 */

		if (sData.GetLength() > 1)
		{
			//pFrm->pPrintDlg->m_RecordNumList.SetItemText(nRecordCnt, nSepCnt + 1, sData);
			if (UTF8 == fileType)
				pFrm->pPrintDlg->sPrintData[(nRecordCnt)* MyView->nMaxColNum + nSepCnt + 1] = UTF8ToGB(sData.GetBuffer(0)).c_str();
			else
				pFrm->pPrintDlg->sPrintData[(nRecordCnt)* MyView->nMaxColNum + nSepCnt + 1] = sData;
		}


		MyView->nAllPaperNo = nRecordCnt;
		pFrm->pPrintDlg->nMaxColNum = MyView->nMaxColNum;
		pFrm->pPrintDlg->RefreshCtrlAllPage(MyView->nAllPaperNo);
		pFrm->pPrintDlg->AddRows(MyView->nAllPaperNo);

		m_MyProgress.DestroyWindow();
	}

	return 0;
}

void CMainFrame::ReadPrintData(CString sPathName)
{
	FILE * pFile;
	pFile = fopen(sPathName, "r");
	fseek(pFile, 0, SEEK_END);//指针移到文件结尾
	int len = ftell(pFile);//获取文件大小
	rewind(pFile);//指针移到文件头

	int nOffset = 0;
	char bBuff = 0;
	///记录号是否使用分隔符
	bool isUseSep = pDBSettingDlg->isUseFieldSymbol();
	//////记录号分隔符assic
	int nRecordAssic = pDBSettingDlg->GetRecordFieldSymbol();
	////记录号固定字段数
	int nSepNum = pDBSettingDlg->GetSeprateNum();
	/////字段分段符assic//
	int nSepAssic = pDBSettingDlg->GetSeprateFieldSymbol();
	int nSepCnt = 0;//字段计数
	int nRecordCnt = 0;
	CString sData = "";
	CString sText = "";
	bool bFlag = false;

	CMyDrawView *pView = (CMyDrawView *)GetActiveView();
	pView->nMaxColNum = 0;
	m_PrintDataArray.RemoveAll();
	SPRINTDATAITEM *pSPRINTDATAITEM = NULL;

	if (!isUseSep)//不使用分段符(固定段数)
	{
		pView->nMaxColNum = nSepNum;
	}
	else
	{
		while (nOffset < 10000 && nOffset < len) //
		{
			bFlag = false;
			memset(&bBuff, 0, 1);
			fread(&bBuff, sizeof(char), 1, pFile);

			sText.Format("%c", bBuff);
			if (bBuff == nSepAssic)
			{
				////得到一个段，存入list中
				pSPRINTDATAITEM = new SPRINTDATAITEM;
				pSPRINTDATAITEM->nRow = nRecordCnt + 1;
				pSPRINTDATAITEM->nCol = nSepCnt + 1;
				pSPRINTDATAITEM->sData = sData;
				m_PrintDataArray.Add(pSPRINTDATAITEM);
				nSepCnt++;
				sData = "";
				bFlag = true;
			}
			else if (bBuff == nRecordAssic)//记录号分隔符
			{
				if (isUseSep)//使用分段符
				{
					if (!bFlag && sData.GetLength() > 0)
					{
						pSPRINTDATAITEM = new SPRINTDATAITEM;
						pSPRINTDATAITEM->nRow = nRecordCnt + 1;
						pSPRINTDATAITEM->nCol = nSepCnt + 1;
						pSPRINTDATAITEM->sData = sData;
						m_PrintDataArray.Add(pSPRINTDATAITEM);
					}
					nSepCnt = 0;
					//////下一个字段就是一行
					nRecordCnt++;
					sData = "";
					bFlag = true;

					break;
				}
			}

			if (!bFlag)
			{
				sData += sText;
			}
			nOffset++;

		}//while


		SPRINTDATAITEM*	m_pSPRINTDATAITEM = NULL;
		int nSize = m_PrintDataArray.GetSize();
		for (int i = 0; i < m_PrintDataArray.GetSize(); i++)
		{
			m_pSPRINTDATAITEM = m_PrintDataArray.GetAt(i);
			if (pView->nMaxColNum < m_pSPRINTDATAITEM->nCol)
				pView->nMaxColNum = m_pSPRINTDATAITEM->nCol;

			delete m_pSPRINTDATAITEM;
			m_pSPRINTDATAITEM = NULL;
		}
	}

	//读取行数
	rewind(pFile);//指针移到文件头
	int nAllPaperNo = 0;
	char line[256] = { 0 };
	while (fgets(line, 255, pFile))
	{
		nAllPaperNo++;
	}

	fclose(pFile); /* 关闭文件 */

	pView->nAllPaperNo = nAllPaperNo;

	if (pPrintDlg)
	{
		int i = pPrintSetDlg->GetPPIX();
		int j = pPrintSetDlg->GetPPIY();

		int y[2] = { 600, 300 };
		int x[6] = { 600, 300, 200, 150, 100, 75 };

		CString strDPI;
		strDPI.Format(_T("DPI = %d * %d"), y[i], x[j]);
		pPrintDlg->RESOLUTION.SetWindowText(strDPI);
	}

	pPrintDlg->ShowWindow(SW_SHOW);
	pPrintDlg->RefreshCtrlAllPage(pView->nAllPaperNo);

	//更新属性中的字段数
	CMFCPropertyGridProperty * pProp11;
	pProp11 = m_wndProperties.m_wndPropList.GetProperty(3)->GetSubItem(0);
	CString itemname = pProp11->GetName();
	if (itemname == "字段")
	{
		pProp11->RemoveAllOptions();
		pProp11->AddOption(_T(" "));

		for (int i = 1; i <= pView->nMaxColNum; i++)
		{
			CString tmp;
			tmp.Format(_T("字段%d"), i);
			pProp11->AddOption(tmp);
		}
		pProp11->AllowEdit(FALSE);
	}

	//HANDLE hSetPrintDataThread = ::CreateThread(NULL,0,SetPrintDataThread,pView,0,NULL);


	HANDLE hSetPrintDataThread = (HANDLE)_beginthreadex(NULL, 0, SetPrintDataThreadFun, pView, 0, NULL);


}



void CMainFrame::OnUpdateMenuitemImportDb(CCmdUI* pCmdUI)
{
	// TODO: Add your command update UI handler code here

}




void CMainFrame::OnMenuitemZoom(UINT id)
{
	CWaitCursor wait;
	theApp.m_nAppZOOM = id;

	CMyDrawView *pView = (CMyDrawView *)GetActiveView();

	switch (theApp.m_nAppZOOM)
	{
	case ID_MENUITEM_ZOOM_10:
		pView->Zoom = 0.1f;
		break;

	case ID_MENUITEM_ZOOM_20:
		pView->Zoom = 0.2f;
		break;

	case ID_MENUITEM_ZOOM_50:
		pView->Zoom = 0.5f;
		break;

	case ID_MENUITEM_ZOOM_100:
		pView->Zoom = 1.0f;
		break;

	case ID_MENUITEM_ZOOM_200:
		pView->Zoom = 2.0f;
		break;

	case ID_MENUITEM_ZOOM_300:
		pView->Zoom = 3.0f;
		break;

	case ID_MENUITEM_ZOOM_500:
		pView->Zoom = 5.0f;
		break;

	default:
		pView->Zoom = 1.0f;
		break;

	}

	pView->ClearSelection(true);
	pView->Invalidate();
	pView->UpdateWindow();
	if (pView->Zoom > 2.0)
		pView->SetScrollSizes(MM_TEXT, CSize(6000 * 2, 1500 * 2));
	else
		pView->SetScrollSizes(MM_TEXT, CSize(6000 * pView->Zoom, 1500 * pView->Zoom));


	CString str;
	str.Format("view: %d%%", int(pView->Zoom * 100));//格式化文本
	m_wndStatusBar.SetPaneText(2, str);

	ToolBarComboBoxButtonSetText();

}

void CMainFrame::OnUpdateMenuitemZoom(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(theApp.m_nAppZOOM == pCmdUI->m_nID);
}

CString CMainFrame::GetWorkDir()
{
	CString path;
	GetModuleFileName(NULL, path.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
	path.ReleaseBuffer();
	int pos = path.ReverseFind('\\');
	path = path.Left(pos);
	path = path + "\\SetParam.ini";
	return path;
}

void CMainFrame::ToolBarComboBoxButtonSetText()
{

	//CMFCToolBarComboBoxButton* pSrcCombo = CMFCToolBarComboBoxButton::GetByCmd(ID_COM_ZOOM_EDIT, TRUE);
	//int index = pSrcCombo->GetCurSel();
	//CString str = pSrcCombo->GetItem(index);

	switch (theApp.m_nAppZOOM)
	{
	case ID_MENUITEM_ZOOM_10:
		m_comboZoom->SetText("10%");
		break;

	case ID_MENUITEM_ZOOM_20:
		m_comboZoom->SetText("20%");
		break;

	case ID_MENUITEM_ZOOM_50:
		m_comboZoom->SetText("50%");
		break;

	case ID_MENUITEM_ZOOM_100:
		m_comboZoom->SetText("100%");
		break;

	case ID_MENUITEM_ZOOM_200:
		m_comboZoom->SetText("200%");
		break;

	case ID_MENUITEM_ZOOM_300:
		m_comboZoom->SetText("300%");
		break;

	case ID_MENUITEM_ZOOM_500:
		m_comboZoom->SetText("500%");
		break;

	default:
		m_comboZoom->SetText("100%");
		break;

	}

}


void CMainFrame::OnSelChangeClick()
{
	CMFCToolBarComboBoxButton* pSrcCombo = CMFCToolBarComboBoxButton::GetByCmd(ID_COM_ZOOM_EDIT, TRUE);
	int index = pSrcCombo->GetCurSel();
	CString str = pSrcCombo->GetItem(index);
	CMyDrawView *pView = (CMyDrawView *)GetActiveView();

	switch (index)
	{
	case 0:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_10;
		pView->Zoom = 0.1f;
		break;

	case 1:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_20;
		pView->Zoom = 0.2f;
		break;

	case 2:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_50;
		pView->Zoom = 0.5f;
		break;

	case 3:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_100;
		pView->Zoom = 1.0f;
		break;

	case 4:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_200;
		pView->Zoom = 2.0f;
		break;

	case 5:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_300;
		pView->Zoom = 3.0f;
		break;

	case 6:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_500;
		pView->Zoom = 5.0f;
		break;

	default:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_10;
		pView->Zoom = 1.0f;
		break;
	}

	pView->ClearSelection(true);
	pView->Invalidate();
	pView->UpdateWindow();
	if (pView->Zoom > 2.0)
		pView->SetScrollSizes(MM_TEXT, CSize(6000 * 2, 1500 * 2));
	else
		pView->SetScrollSizes(MM_TEXT, CSize(6000 * pView->Zoom, 1500 * pView->Zoom));



	//显示百分比
	CMainFrame*pFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;//获得主窗口指标
	CMFCStatusBar*pStatus = &pFrame->m_wndStatusBar;//获得主窗口的状态栏指针
	if (pStatus)
	{
		CString str;
		str.Format("view: %d%%", int(pView->Zoom * 100));//格式化文本
		pStatus->SetPaneText(2, str);
	}


}
void CMainFrame::OnClickComboBox()
{
}


