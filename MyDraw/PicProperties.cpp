// PicProperties.cpp : 实现文件
//

#include "stdafx.h"
#include "MyDraw.h"
#include "MainFrm.h"
#include "MyDrawView.h"
#include "PicProperties.h"


// CPicProperties 对话框

IMPLEMENT_DYNAMIC(CPicProperties, CDialog)

CPicProperties::CPicProperties(CWnd* pParent /*=NULL*/)
	: CDialog(CPicProperties::IDD, pParent)
	, pPic(NULL)
	, m_fPicWidth(0)
	, m_fPicHeight(0)
	, m_fPicLeft(0)
	, m_fPicTop(0)
	,m_nPicRotateAngle(0)
	, m_PicDirectory(_T(""))
	, m_bFixScale(FALSE)
{

}

CPicProperties::~CPicProperties()
{
}

void CPicProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_PIC, m_PicPropertyTab);
	DDX_Text(pDX, IDC_EDIT_PIC_WIDTH, m_fPicWidth);
	DDX_Text(pDX, IDC_EDIT_PIC_HEIGH, m_fPicHeight);
	DDX_Text(pDX, IDC_EDIT_PIC_LEFT, m_fPicLeft);
	DDX_Text(pDX, IDC_EDIT_PIC_TOP, m_fPicTop);
	DDX_Text(pDX, IDC_EDIT_PIC_NAME, m_PicStaticName);
	DDX_Text(pDX, IDC_EDIT_DYNAMIC_PIC, m_PicDirectory);
	DDX_Control(pDX, IDC_COMBO_PIC_FIELDNUM, m_PicFieldNumCmb);
	DDX_Control(pDX, IDC_RADIO_STATIC_PIC, m_PicStaticRadio);
	DDX_Control(pDX, IDC_RADIO_DYNAMIC_PIC, m_PicDynamicRadio);
	DDX_Check(pDX, IDC_CHECK_FIX_SCALE, m_bFixScale);
	DDX_Control(pDX, IDC_CHECK_FIX_SCALE, m_checkFixScale);
}


BEGIN_MESSAGE_MAP(CPicProperties, CDialog)
	ON_BN_CLICKED(IDC_RADIO_STATIC_PIC, &CPicProperties::OnBnClickedRadioStaticPic)
	ON_BN_CLICKED(IDC_RADIO_DYNAMIC_PIC, &CPicProperties::OnBnClickedRadioDynamicPic)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_PIC_PATH, &CPicProperties::OnBnClickedButtonSelectPicPath)
	ON_BN_CLICKED(IDC_BUTTON_SELECT_BYNAMIC_PIC, &CPicProperties::OnBnClickedButtonSelectBynamicPic)
	ON_BN_CLICKED(IDC_RADIO_PIC_RORATE_0, &CPicProperties::OnBnClickedRadioPicRorate0)
	ON_BN_CLICKED(IDC_RADIO_PIC_RORATE_90, &CPicProperties::OnBnClickedRadioPicRorate90)
	ON_BN_CLICKED(IDC_RADIO_PIC_RORATE_180, &CPicProperties::OnBnClickedRadioPicRorate180)
	ON_BN_CLICKED(IDC_RADIO_PIC_RORATE_270, &CPicProperties::OnBnClickedRadioPicRorate270)
	ON_BN_CLICKED(IDC_RADIOIDC_RADIO_PIC_MIRROR, &CPicProperties::OnBnClickedRadioidcRadioPicMirror)
	ON_CBN_SELCHANGE(IDC_COMBO_PIC_FIELDNUM, &CPicProperties::OnCbnSelchangeComboPicFieldnum)
	ON_BN_CLICKED(IDYES, &CPicProperties::OnBnClickedYes)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_PIC, &CPicProperties::OnTcnSelchangeTabPic)
	ON_BN_CLICKED(IDOK, &CPicProperties::OnBnClickedOk)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CPicProperties 消息处理程序


BOOL CPicProperties::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化

	m_PicPropertyTab.InsertItem(0,"位置",0);
	m_PicPropertyTab.InsertItem(1,"内容",1);
	m_PicPropertyTab.InsertItem(2,"外形",2);
	m_PicPropertyTab.SetCurSel(1);

	if (isStaticPic)//静态图片
	{
		m_PicStaticRadio.SetCheck(1);
		m_PicDynamicRadio.SetCheck(0);
		
		GetDlgItem(IDC_BUTTON_SELECT_PIC_PATH)->EnableWindow(true);

		GetDlgItem(IDC_EDIT_DYNAMIC_PIC)->EnableWindow(false);
		GetDlgItem(IDC_BUTTON_SELECT_BYNAMIC_PIC)->EnableWindow(false);
		GetDlgItem(IDC_COMBO_PIC_FIELDNUM)->EnableWindow(false);
	}
	else
	{
		m_PicStaticRadio.SetCheck(0);
		m_PicDynamicRadio.SetCheck(1);
		GetDlgItem(IDC_BUTTON_SELECT_PIC_PATH)->EnableWindow(false);
		
		GetDlgItem(IDC_BUTTON_SELECT_BYNAMIC_PIC)->EnableWindow(true);
		GetDlgItem(IDC_COMBO_PIC_FIELDNUM)->EnableWindow(true);
	}

	GetDlgItem(IDC_EDIT_PIC_NAME)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_DYNAMIC_PIC)->EnableWindow(false);

	GetDlgItem(IDC_RADIO_PIC_RORATE_0)->EnableWindow(true);
	GetDlgItem(IDC_RADIO_PIC_RORATE_90)->EnableWindow(true);
	GetDlgItem(IDC_RADIO_PIC_RORATE_180)->EnableWindow(true);
	GetDlgItem(IDC_RADIO_PIC_RORATE_270)->EnableWindow(true);
	GetDlgItem(IDC_RADIOIDC_RADIO_PIC_MIRROR)->EnableWindow(false);



	OnInitSqlFieldNumCmb(nMaxFieldNum, m_PicFieldNum);

	SetCtrlVisible(1);
	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子  
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CPicProperties::OnBnClickedRadioStaticPic()
{
	// TODO: 在此添加控件通知处理程序代码
	isStaticPic = true;
	m_PicStaticRadio.SetCheck(1);
	m_PicDynamicRadio.SetCheck(0);
//	GetDlgItem(IDC_EDIT_PIC_NAME)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_SELECT_PIC_PATH)->EnableWindow(true);

//	GetDlgItem(IDC_EDIT_DYNAMIC_PIC)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_SELECT_BYNAMIC_PIC)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_PIC_FIELDNUM)->EnableWindow(false);
}

void CPicProperties::OnBnClickedRadioDynamicPic()
{
	// TODO: 在此添加控件通知处理程序代码
	isStaticPic = false;
	m_PicDynamicRadio.SetCheck(1);
//	GetDlgItem(IDC_EDIT_PIC_NAME)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_SELECT_PIC_PATH)->EnableWindow(false);

//	GetDlgItem(IDC_EDIT_DYNAMIC_PIC)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_SELECT_BYNAMIC_PIC)->EnableWindow(true);
	GetDlgItem(IDC_COMBO_PIC_FIELDNUM)->EnableWindow(true);
}

void CPicProperties::OnBnClickedButtonSelectPicPath()
{
	// TODO: 在此添加控件通知处理程序代码
	CFileDialog Filedlg(TRUE,NULL,NULL,
		OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT |
		OFN_ALLOWMULTISELECT|OFN_EXPLORER, 
		"Data Files (*.bmp)|*.bmp|All files(*.*)|*.*||",
		NULL); 

	char pBuf[MAX_PATH];
	GetCurrentDirectory(MAX_PATH,pBuf);
	CString sPath = "";
	sPath.Format("%s",pBuf);

	Filedlg.m_ofn.lpstrTitle = "导入位图";//改变标题

	Filedlg.m_ofn.lpstrInitialDir = sPath;//设定打开初始目录

	//	Filedlg.m_ofn.lStructSize = structsize;

	Filedlg.m_ofn.nMaxFile = MAX_PATH;

	CString strFilePath = "";
	CString strFileName = "";
	if(Filedlg.DoModal() == IDOK)
	{
		m_PicStaticPathName = Filedlg.GetPathName();
		//strFileName = Filedlg.GetFileName();
		m_PicStaticName = Filedlg.GetFileName();
	//	ReadPrintData(strFilePath);

		CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
		CMyDrawView *pView = (CMyDrawView *)pView1;

		//加载图片  
		CBitmap m_BkGndBmp;
		CString sFilePathName = m_PicStaticPathName;
		HBITMAP hBitmap = (HBITMAP)::LoadImage(AfxGetInstanceHandle(), sFilePathName, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);
		if (!hBitmap)
		{
			return;
		}

		m_BkGndBmp.Attach(hBitmap);

		//获取图片大小  
		BITMAP bm;
		m_BkGndBmp.GetBitmap(&bm);

		m_fPicWidth = int(bm.bmWidth / pView->ZoomInX_Pic) / pView->ScaleX;
		m_fPicHeight = int(bm.bmHeight / pView->ZoomInY_Pic) / pView->ScaleY;


	} 
	UpdateData(false);



}

void CPicProperties::OnBnClickedButtonSelectBynamicPic()
{
	// TODO: 在此添加控件通知处理程序代码
	// TODO: Add your control notification handler code here  
	TCHAR pszPath[MAX_PATH];  
	BROWSEINFO bi;   
	bi.hwndOwner      = this->GetSafeHwnd();  
	bi.pidlRoot       = NULL;  
	bi.pszDisplayName = NULL;   
	bi.lpszTitle      = TEXT("请选择文件夹");   
	bi.ulFlags        = BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT;  
	bi.lpfn           = NULL;   
	bi.lParam         = 0;  
	bi.iImage         = 0;   

	LPITEMIDLIST pidl = SHBrowseForFolder(&bi);  
	if (pidl == NULL)  
	{  
		return;  
	}  

	if (SHGetPathFromIDList(pidl, pszPath))  
	{  
		//AfxMessageBox(pszPath); 
		m_PicDirectory = pszPath;
		UpdateData(false);
	} 
}

void CPicProperties::OnInitSqlFieldNumCmb(int nNum,int nIndex)
{
	if(nNum<1)
		return;
	if(nIndex>nNum)
		nIndex =nNum;
	CString sText = "";
	for(int i=0;i<nNum;i++)
	{
		sText.Format("字段%d",i+1);
		m_PicFieldNumCmb.AddString(sText);
	
	}

	if(nIndex<1)
		nIndex = 1;
	m_PicFieldNumCmb.SetCurSel(nIndex-1);
	m_PicFieldNum = nIndex;

}

void CPicProperties::SetSqlFieldNumParam(int nMxFdNum,int nFieldCmbIndex)
{
	nMaxFieldNum=nMxFdNum;
	m_PicFieldNum=nFieldCmbIndex;

}

void CPicProperties::OnBnClickedRadioPicRorate0()
{
	// TODO: 在此添加控件通知处理程序代码
	m_nPicRotateAngle = 0;
}

void CPicProperties::OnBnClickedRadioPicRorate90()
{
	// TODO: 在此添加控件通知处理程序代码
	m_nPicRotateAngle = 90;
}

void CPicProperties::OnBnClickedRadioPicRorate180()
{
	// TODO: 在此添加控件通知处理程序代码
	m_nPicRotateAngle = 180;
}

void CPicProperties::OnBnClickedRadioPicRorate270()
{
	// TODO: 在此添加控件通知处理程序代码
	m_nPicRotateAngle = 270;
}

void CPicProperties::OnBnClickedRadioidcRadioPicMirror()
{
	// TODO: 在此添加控件通知处理程序代码
}

void CPicProperties::OnCbnSelchangeComboPicFieldnum()
{
	// TODO: 在此添加控件通知处理程序代码
	int nIndex = m_PicFieldNumCmb.GetCurSel();

	if (nIndex == CB_ERR)
	{
		m_PicFieldNum = 0;
		return;
	}
	m_PicFieldNum = nIndex+1;

}




void CPicProperties::OnBnClickedYes()
{
	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);

	if (pPic == NULL)
		return;

	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;

	pPic->startX = ((int)(m_fPicLeft*pView->ScaleX / pView->Zoom + 0.005) * 1000) / 1000.0;
	pPic->startY = ((int)(m_fPicTop*pView->ScaleY / pView->Zoom + 0.005) * 1000) / 1000.0;
	//pPic->nWidth = m_fPicWidth;
	//pPic->nHeight = m_fPicHeight;
	pPic->isStaticPic = isStaticPic;
	pPic->m_PicStaticName = m_PicStaticName;
	pPic->m_PicStaticPathName = m_PicStaticPathName;
	pPic->m_PicDirectory = m_PicDirectory;
	pPic->m_PicFieldNum = m_PicFieldNum;
	pPic->nPicRotateAngle = m_nPicRotateAngle;
	pPic->m_isFixScale = m_bFixScale;

	pPic->bIsSelected = false;

	pView->Invalidate(FALSE);
}



void CPicProperties::SetCtrlVisible(int nView)
{
	if (nView == 0)
	{
		////位置与大小
		GetDlgItem(IDC_GROUP_PIC_POS)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_PIC_WIDH)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_PIC_WIDTH)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_PIC_HEIGHT)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_PIC_HEIGH)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_PIC_LEFT)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_PIC_LEFT)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_PIC_TOP)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_PIC_TOP)->ShowWindow(true);

		//////内容
		GetDlgItem(IDC_GROUP_PIC_NAME)->ShowWindow(false);
		GetDlgItem(IDC_GROUP_STATIC_PIC)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_STATIC_PIC)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_PIC_NAME)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_PIC_NAME)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_SELECT_PIC_PATH)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_Dynamic_PIC)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_DYNAMIC_PIC)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_DYNAMIC_PIC)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_SELECT_BYNAMIC_PIC)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_PIC_FIELDNUM)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_PIC_FIELDNUM)->ShowWindow(false);


		GetDlgItem(IDC_CHECK_FIX_SCALE)->ShowWindow(false);


		////外形
		GetDlgItem(IDC_GROUP_PIC_APPERANCE)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_PIC_RORATE_0)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_PIC_RORATE_90)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_PIC_RORATE_180)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_PIC_RORATE_270)->ShowWindow(false);
		GetDlgItem(IDC_RADIOIDC_RADIO_PIC_MIRROR)->ShowWindow(false);
		
	}
	else if (nView == 1)
	{
		////位置与大小
		GetDlgItem(IDC_GROUP_PIC_POS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_PIC_WIDH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PIC_WIDTH)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_PIC_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PIC_HEIGH)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_PIC_LEFT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PIC_LEFT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_PIC_TOP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PIC_TOP)->ShowWindow(false);

		//////内容
		GetDlgItem(IDC_GROUP_PIC_NAME)->ShowWindow(true);
		GetDlgItem(IDC_GROUP_STATIC_PIC)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_STATIC_PIC)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_PIC_NAME)->ShowWindow(true);

		GetDlgItem(IDC_EDIT_PIC_NAME)->ShowWindow(true);
		GetDlgItem(IDC_BUTTON_SELECT_PIC_PATH)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_Dynamic_PIC)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_DYNAMIC_PIC)->ShowWindow(true);

		GetDlgItem(IDC_EDIT_DYNAMIC_PIC)->ShowWindow(true);
		GetDlgItem(IDC_BUTTON_SELECT_BYNAMIC_PIC)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_PIC_FIELDNUM)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_PIC_FIELDNUM)->ShowWindow(true);


		GetDlgItem(IDC_CHECK_FIX_SCALE)->ShowWindow(true);


		////外形
		GetDlgItem(IDC_GROUP_PIC_APPERANCE)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_PIC_RORATE_0)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_PIC_RORATE_90)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_PIC_RORATE_180)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_PIC_RORATE_270)->ShowWindow(false);
		GetDlgItem(IDC_RADIOIDC_RADIO_PIC_MIRROR)->ShowWindow(false);

	}
	else if (nView == 2)
	{
		////位置与大小
		GetDlgItem(IDC_GROUP_PIC_POS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_PIC_WIDH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PIC_WIDTH)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_PIC_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PIC_HEIGH)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_PIC_LEFT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PIC_LEFT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_PIC_TOP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PIC_TOP)->ShowWindow(false);

		//////内容
		GetDlgItem(IDC_GROUP_PIC_NAME)->ShowWindow(false);
		GetDlgItem(IDC_GROUP_STATIC_PIC)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_STATIC_PIC)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_PIC_NAME)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_PIC_NAME)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_SELECT_PIC_PATH)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_Dynamic_PIC)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_DYNAMIC_PIC)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_DYNAMIC_PIC)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_SELECT_BYNAMIC_PIC)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_PIC_FIELDNUM)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_PIC_FIELDNUM)->ShowWindow(false);


		GetDlgItem(IDC_CHECK_FIX_SCALE)->ShowWindow(false);


		////外形
		GetDlgItem(IDC_GROUP_PIC_APPERANCE)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_PIC_RORATE_0)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_PIC_RORATE_90)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_PIC_RORATE_180)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_PIC_RORATE_270)->ShowWindow(true);
		GetDlgItem(IDC_RADIOIDC_RADIO_PIC_MIRROR)->ShowWindow(true);

	}


}

void CPicProperties::OnTcnSelchangeTabPic(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO:  在此添加控件通知处理程序代码
	int nIndex = m_PicPropertyTab.GetCurSel();
	SetCtrlVisible(nIndex);

	*pResult = 0;
}



void CPicProperties::OnBnClickedOk()
{
	// TODO:  在此添加控件通知处理程序代码
	CDialog::OnOK();
}


HBRUSH CPicProperties::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
