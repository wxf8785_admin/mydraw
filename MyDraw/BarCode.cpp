// BarCode.cpp: implementation of the CBarCode class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MyDraw.h"
#include "MyDrawView.h"
#include "MainFrm.h"
#include "BarCode.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CBarCode,CObject,1)


void CBarCode::Serialize(CArchive & ar)
{
	

	CObject::Serialize(ar);
    if(ar.IsLoading())
    {
		CString lfFaceName;

		ar>>startX>>startY>>endX>>endY>>MyText>>Direction>>
			LinePen.lopnWidth.x>>LinePen.lopnColor>>LinePen.lopnStyle>>nPenWidth>>
			nCodeType>>m_nBarPropertyType>>
			(CString)chCodeString>>
			nBitMapLenth>>nBitMapHeight>>nRotateAngle>>nFrameType>>m_nFieldIndex>>
			nTextAlineType>>m_nFrameType>>m_nTop>>m_nBottom>>m_nRotateTyp>>nTextGroupType>>nTextGroupDis>>nTextBarDis>>isDefaultTextBarDis>>
			TextColor>>
			TextFont.lfCharSet>>TextFont.lfClipPrecision>>TextFont.lfEscapement>>lfFaceName>>TextFont.lfHeight>>TextFont.lfItalic>>TextFont.lfOrientation>>TextFont.lfOutPrecision>>TextFont.lfPitchAndFamily>>TextFont.lfQuality>>TextFont.lfStrikeOut>>TextFont.lfUnderline>>TextFont.lfWeight>>TextFont.lfWidth>>
			sInsetParam>>sFieldTypeString>>sFieldTypeDis>>m_strFieldDes>>m_strRecordVal>>m_strInitVal>>
			nCounterStart>>nCounterEnd>>nCounterStep>>m_nSqlTypeFieldIndex>>
			sSqlData>>sBarDateTypeString>>
			nQRLevel>>nQRVersion>>bQRAutoExtent>>nQRMaskingNo>>fQRRotateAngle>>nStartPos>>nCounts>>m_nTimeFormatIndex;

		strcpy(TextFont.lfFaceName, lfFaceName);
		setCodeString(MyText);
    }
	else
	{
		MyText = chCodeString;

		ar<<startX<<startY<<endX<<endY<<MyText<<Direction<<
			LinePen.lopnWidth.x<<LinePen.lopnColor<<LinePen.lopnStyle<<nPenWidth<<
			nCodeType<<m_nBarPropertyType<<
			(CString)chCodeString<<
			nBitMapLenth<<nBitMapHeight<<nRotateAngle<<nFrameType<<m_nFieldIndex<<
			nTextAlineType<<m_nFrameType<<m_nTop<<m_nBottom<<m_nRotateTyp<<nTextGroupType<<nTextGroupDis<<nTextBarDis<<isDefaultTextBarDis<<
			TextColor<<
			TextFont.lfCharSet<<TextFont.lfClipPrecision<<TextFont.lfEscapement<<(CString)TextFont.lfFaceName<<TextFont.lfHeight<<TextFont.lfItalic<<TextFont.lfOrientation<<TextFont.lfOutPrecision<<TextFont.lfPitchAndFamily<<TextFont.lfQuality<<TextFont.lfStrikeOut<<TextFont.lfUnderline<<TextFont.lfWeight<<TextFont.lfWidth<<
			sInsetParam<<sFieldTypeString<<sFieldTypeDis<<m_strFieldDes<<m_strRecordVal<<m_strInitVal<<
			nCounterStart<<nCounterEnd<<nCounterStep<<m_nSqlTypeFieldIndex<<
			sSqlData<<sBarDateTypeString<<
			nQRLevel<<nQRVersion<<bQRAutoExtent<<nQRMaskingNo<<fQRRotateAngle<<nStartPos<<nCounts<<m_nTimeFormatIndex;

    }

	
}




CGraph* CBarCode::Clone()
{
	CBarCode* p = new CBarCode();

	p->startX = this->startX + 10;
	p->startY = this->startY + 10;
	p->endX = this->endX + 10;
	p->endY = this->endY + 10;

	p->MyText = this->MyText;
	p->Direction = this->Direction;
	p->LinePen = this->LinePen;
	p->nPenWidth = this->nPenWidth;
	p->nCodeType = this->nCodeType;
	p->m_nBarPropertyType = this->m_nBarPropertyType;
	p->setCodeString(this->GetCodeString());
	p->nBitMapLenth = this->nBitMapLenth;
	p->nBitMapHeight = this->nBitMapHeight;
	p->nRotateAngle = this->nRotateAngle;
	p->nFrameType = this->nFrameType;
	p->m_nFieldIndex = this->m_nFieldIndex;
	p->nTextAlineType = this->nTextAlineType;
	p->m_nFrameType = this->m_nFrameType;
	p->m_nTop = this->m_nTop;
	p->m_nBottom = this->m_nBottom;
	p->m_nRotateTyp = this->m_nRotateTyp;
	p->nTextGroupType = this->nTextGroupType;
	p->nTextGroupDis = this->nTextGroupDis;
	p->nTextBarDis = this->nTextBarDis;
	p->isDefaultTextBarDis = this->isDefaultTextBarDis;
	p->TextColor = this->TextColor;
	p->TextFont = this->TextFont;
	p->sInsetParam = this->sInsetParam;
	p->sFieldTypeString = this->sFieldTypeString;
	p->sFieldTypeDis = this->sFieldTypeDis;
	p->m_strFieldDes = this->m_strFieldDes;
	p->m_strRecordVal = this->m_strRecordVal;
	p->m_strInitVal = this->m_strInitVal;
	p->nCounterStart = this->nCounterStart;
	p->nCounterEnd = this->nCounterEnd;
	p->nCounterStep = this->nCounterStep;
	p->m_nSqlTypeFieldIndex = this->m_nSqlTypeFieldIndex;
	p->sSqlData = this->sSqlData;
	p->sBarDateTypeString = this->sBarDateTypeString;
	p->nQRLevel = this->nQRLevel;
	p->nQRVersion = this->nQRVersion;
	p->bQRAutoExtent = this->bQRAutoExtent;
	p->nQRMaskingNo = this->nQRMaskingNo;
	p->fQRRotateAngle = this->fQRRotateAngle;
	p->nStartPos = this->nStartPos;
	p->nCounts = this->nCounts;
	p->m_nTimeFormatIndex = this->m_nTimeFormatIndex;


	p->_OriRect = this->_OriRect;
	p->tracker = this->tracker;
	p->bIsSelected = this->bIsSelected;
	p->bIsLocked = this->bIsLocked;

	return p;
}



void   CBarCode::InitPropList()
{

	CMFCPropertyGridProperty * pProp;
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMyDrawView *pView = (CMyDrawView *)pFrame->GetActiveView();

	pFrame->m_wndProperties.graphs.Add(this);
	//pFrame->m_wndProperties.m_wndObjectCombo.SetCurSel(nGraphics);
	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(0);
	//读取数据
	CString itemname;
	itemname = pProp->GetName(); //获取名称
	COleVariant itemvalue;
	itemvalue = pProp->GetValue();//获取值
	//写入数据
	CString m_startX;
	m_startX.Format(_T("%.2f"), startX / pView->ScaleX);
	pProp->SetValue((_variant_t)(m_startX));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(1);
	CString m_startY;
	m_startY.Format(_T("%.2f"), startY / pView->ScaleY);
	pProp->SetValue((_variant_t)(m_startY));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(0);
	CString m_BarWidth;
	m_BarWidth.Format(_T("%.2f"), (endX - startX) / pView->ScaleX * pView->Zoom);
	pProp->SetValue((_variant_t)(m_BarWidth));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(1);
	CString m_BarHeight;
	m_BarHeight.Format(_T("%.2f"), (endY - startY) / pView->ScaleY * pView->Zoom);
	pProp->SetValue((_variant_t)(m_BarHeight));


}