// TBarCode11.cpp: implementation of the CTBarCode11 class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MyDraw.h"
#include "MyDrawView.h"
#include "MainFrm.h"
#include "TBarCode11.h"
#include "CMFCPropertyGridFontPropertyEx.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CTBarCode11, CObject, 1)


CTBarCode11::CTBarCode11()
{
	tracker.m_nStyle = CRectTracker::resizeInside |
		CRectTracker::dottedLine;
	tracker.m_rect.SetRect(0, 0, 0, 0);
	tracker.m_nHandleSize = 8;

	startX = 1;
	startY = 1;
	endX = 121;
	endY = 51;

	nCodeType = 15;
	nBitMapLenth = 120;
	nBitMapHeight = 50;
	nPenWidth = 1;
	m_nFieldIndex = 1;

	TextFont.lfCharSet = DEFAULT_CHARSET;
	TextFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	TextFont.lfEscapement = 0;
	strcpy(TextFont.lfFaceName, "Arial");
	TextFont.lfHeight = -12;
	TextFont.lfItalic = false;
	TextFont.lfOrientation = 0;
	TextFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
	TextFont.lfPitchAndFamily = FF_SWISS;
	TextFont.lfQuality = DEFAULT_QUALITY;
	TextFont.lfStrikeOut = false;
	TextFont.lfUnderline = false;
	TextFont.lfWeight = 400;
	TextFont.lfWidth = 0;
	TextColor = RGB(0, 0, 0);

	nTextAlineType = 0;//文本对齐方式
	m_nFrameType = 0;
	m_QietZoonUnit = 0;
	m_nTop = 12;     //上边框宽度值（pixel）
	m_nBottom = 12;  ////下边框宽度值（pixel）
	m_nLeft = 12;    //上边框宽度值（pixel）
	m_nRight = 12;   ////下边框宽度值（pixel）
	m_nRotateTyp = 0;
	nTextGroupType = 0;//分组类型
	nTextGroupDis = 2;//字符分组间距
	nTextBarDis = 0;//文本间距

	isDefaultTextBarDis = TRUE;
	sFieldTypeString = "1,2,3,4,5";

	m_nBarPropertyType = 0;
	m_nSqlTypeFieldIndex = 0;

	////计数器
	nCounterStart = 0;
	nCounterEnd = 1000;
	nCounterStep = 1;

	//二维码设置参数
	nQRLevel = 1;
	nQRVersion = 0;
	bQRAutoExtent = 1;
	nQRMaskingNo = 0;
	fQRRotateAngle = 0;


	nGraphics = 8;


	nStartPos = 0;
	nCounts = 0;
	m_nTimeFormatIndex = 0;


	//修边
	bBarWidthReduction = false;
	nBarWidthReduction = 1;

	//条码的文本是否显示，是否显示在上面
	bReadable = TRUE;
	bAbove = FALSE;
	m_bMirror = FALSE;


	ERRCODE eCode = S_OK;
	eCode = BCAlloc(&m_pBarCode);
	if (eCode != S_OK)
	{
		return;
	}

	CClientDC dc(AfxGetApp()->GetMainWnd());

	m_bAspectRatio = true;                        // Retain aspect ratio (picture size)
	m_bResRatio = true;                           // Same horizontal and vertical resolution
	m_nHeight = 30000;                            // 30mm picture height
	m_nWidth = 50000;                             // 50mm picture width
	m_nHorzRes = dc.GetDeviceCaps(LOGPIXELSX);    // ~96 dpi horizontal resolution
	m_nVertRes = dc.GetDeviceCaps(LOGPIXELSY);    // ~96 dpi vertical resolution
	m_nUnit = MILLIMETERS;                        // Default unit: mm
	m_dAspectRatio = (float)m_nWidth / m_nHeight;   // Reckon aspect ratio: width / height

	SetData(_T("123456789"));                         // Dummy text
	BCSetMustFit(m_pBarCode, FALSE);              // no check for too low resolution
	BCSetBCType(m_pBarCode, eBC_Code128C);  //eBC_Code128       // symbology = Code 128 (bar code type)//eBC_9OF3
	BCSetCDMethod(m_pBarCode, eCDStandard);       // check digit method


	LOGFONTA* pFont;
	pFont = BCGetLogFont(m_pBarCode);             // get pointer to font struct

	// set 8 pt font (formula is used to get correct screen representation)
	if (dc)
		pFont->lfHeight = -MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);

	_tcscpy(pFont->lfFaceName, _T("Arial"));
	pFont->lfWeight = 400;
	BCSetLogFont(m_pBarCode, pFont);

	TextFont.lfHeight = pFont->lfHeight;
	TextFont.lfWeight = pFont->lfWeight;

}

CTBarCode11::~CTBarCode11()
{

	if (m_pBarCode)
	{
		BCFree(m_pBarCode);
		m_pBarCode = NULL;
	}

}


void CTBarCode11::Serialize(CArchive & ar)
{
	DWORD eBarCType;  // Variable to convert enum to DWORD
	DWORD eCDMethod;  // Variable to convert enum to DWORD
	DWORD dwVersion;  // Version of bcs-file
	CString sID;    // ID of bcs-file
	DWORD   eCCType;

	CObject::Serialize(ar);
	if (ar.IsLoading())
	{

		CString lfFaceName;

		ar >> startX >> startY >> endX >> endY >> Direction >>
			LinePen.lopnWidth.x >> LinePen.lopnColor >> LinePen.lopnStyle >> nPenWidth >>
			nCodeType >> m_nBarPropertyType >>
			m_sData >>
			nBitMapLenth >> nBitMapHeight >> nRotateAngle >> nFrameType >> m_nFieldIndex >>
			nTextAlineType >> m_nFrameType >> m_QietZoonUnit >> m_nLeft >> m_nTop >> m_nRight >> m_nBottom >> m_nRotateTyp >> nTextGroupType >> nTextGroupDis >> nTextBarDis >> isDefaultTextBarDis >>
			TextColor >>
			TextFont.lfCharSet >> TextFont.lfClipPrecision >> TextFont.lfEscapement >> lfFaceName >> TextFont.lfHeight >> TextFont.lfItalic >> TextFont.lfOrientation >> TextFont.lfOutPrecision >> TextFont.lfPitchAndFamily >> TextFont.lfQuality >> TextFont.lfStrikeOut >> TextFont.lfUnderline >> TextFont.lfWeight >> TextFont.lfWidth >>
			sInsetParam >> sFieldTypeString >> sFieldTypeDis >> m_strFieldDes >> m_strRecordVal >> m_strInitVal >>
			nCounterStart >> nCounterEnd >> nCounterStep >> m_nSqlTypeFieldIndex >>
			sSqlData >> sBarDateTypeString >>
			nQRLevel >> nQRVersion >> bQRAutoExtent >> nQRMaskingNo >> fQRRotateAngle >> nStartPos >> nCounts >> m_nTimeFormatIndex >>
			bBarWidthReduction >> nBarWidthReduction >>
			bReadable >> bAbove >> m_bMirror;

		strcpy(TextFont.lfFaceName, lfFaceName);

		ERRCODE eCode;

		if (m_pBarCode)
		{
			BCFree(m_pBarCode);
			m_pBarCode = NULL;
		}
		eCode = BCAlloc(&m_pBarCode);
		ASSERT(eCode == S_OK);

		ar >> sID;
		ar >> dwVersion;

		if (sID.Compare(BCS_ID) || (dwVersion != BCS_VERSION))
		{
			// Not a BCS-file or wrong version.
		}
		else
		{
			int n1, n2;
			BOOL b1, b2;
			DWORD col;
			CString csz1, csz2, csz3;

			ar >> eBarCType;
			ar >> eCDMethod;

			BCSetBCType(m_pBarCode, (e_BarCType)eBarCType);
			BCSetCDMethod(m_pBarCode, (e_CDMethod)eCDMethod);

			ar >> n1; BCSetRotation(m_pBarCode, (e_Degree)n1);
			ar >> n1; BCSetBkMode(m_pBarCode, n1);
			ar >> b1;
			ar >> b2; BCSetPrintText(m_pBarCode, b1, b2);
			ar >> b1; BCSetMirror(m_pBarCode, b1);
			ar >> n1; BCSetTextDist(m_pBarCode, n1);

			ar >> csz1; BCSetModWidth(m_pBarCode, (LPCTSTR)csz1);
			ar >> csz1; BCSetRatio(m_pBarCode, (LPCTSTR)csz1);
			ar >> csz1; BCSetFormat(m_pBarCode, (LPCTSTR)csz1);
			ar >> n1; BCSetBearerBarWidth(m_pBarCode, n1);
			ar >> n1; BCSetBearerBarType(m_pBarCode, (e_BearerBar)n1);
			ar >> n1; BCSetNotchHeight(m_pBarCode, n1);
			ar >> b1; BCSetTranslateEsc(m_pBarCode, b1);

			// PDF417 specific information
			ar >> n1; BCSet_PDF417_Rows(m_pBarCode, n1);
			ar >> n1; BCSet_PDF417_Columns(m_pBarCode, n1);
			ar >> n1; BCSet_PDF417_ECLevel(m_pBarCode, n1);
			ar >> n1; BCSet_PDF417_RowHeight(m_pBarCode, n1);

			// MaxiCode specific information
			ar >> n1; BCSet_Maxi_Mode(m_pBarCode, n1);
			ar >> n1; BCSet_Maxi_UnderCut(m_pBarCode, n1);
			ar >> b1;
			ar >> csz1; BCSet_Maxi_UsePreamble(m_pBarCode, b1, (LPCTSTR)csz1);
			ar >> n1;
			ar >> n2; BCSet_Maxi_Append(m_pBarCode, n1, n2);
			ar >> csz1;
			ar >> csz2;
			ar >> csz3; BCSet_Maxi_SCM(m_pBarCode, csz1, csz2, csz3);


			ar >> col; BCSetColorBC(m_pBarCode, (COLORREF)col);
			ar >> col; BCSetColorFont(m_pBarCode, col);
			ar >> col; BCSetColorBk(m_pBarCode, (COLORREF)col);

			ar.Read(BCGetLogFont(m_pBarCode), sizeof(LOGFONTA));

			ar >> m_bAspectRatio;
			ar >> m_bResRatio;
			ar >> m_nHeight;
			ar >> m_nWidth;
			ar >> m_nHorzRes;
			ar >> m_nVertRes;
			ar >> m_nUnit;

			m_dAspectRatio = (float)m_nWidth / m_nHeight;

			// Barcode text has to initialized:
			if (!m_sData.GetLength())
				m_sData = "123456789";

			SetData(m_sData);

			ar >> eCCType; BCSet2DCompositeComponent(m_pBarCode, (e_CCType)eCCType);
		}

	}
	else
	{
		ar << startX << startY << endX << endY << Direction <<
			LinePen.lopnWidth.x << LinePen.lopnColor << LinePen.lopnStyle << nPenWidth <<
			nCodeType << m_nBarPropertyType <<
			m_sData <<
			nBitMapLenth << nBitMapHeight << nRotateAngle << nFrameType << m_nFieldIndex <<
			nTextAlineType << m_nFrameType << m_QietZoonUnit << m_nLeft << m_nTop << m_nRight << m_nBottom << m_nRotateTyp << nTextGroupType << nTextGroupDis << nTextBarDis << isDefaultTextBarDis <<
			TextColor <<
			TextFont.lfCharSet << TextFont.lfClipPrecision << TextFont.lfEscapement << (CString)TextFont.lfFaceName << TextFont.lfHeight << TextFont.lfItalic << TextFont.lfOrientation << TextFont.lfOutPrecision << TextFont.lfPitchAndFamily << TextFont.lfQuality << TextFont.lfStrikeOut << TextFont.lfUnderline << TextFont.lfWeight << TextFont.lfWidth <<
			sInsetParam << sFieldTypeString << sFieldTypeDis << m_strFieldDes << m_strRecordVal << m_strInitVal <<
			nCounterStart << nCounterEnd << nCounterStep << m_nSqlTypeFieldIndex <<
			sSqlData << sBarDateTypeString <<
			nQRLevel << nQRVersion << bQRAutoExtent << nQRMaskingNo << fQRRotateAngle << nStartPos << nCounts << m_nTimeFormatIndex <<
			bBarWidthReduction << nBarWidthReduction <<
			bReadable << bAbove << m_bMirror;

		sID = BCS_ID;
		dwVersion = BCS_VERSION;

		ar << sID;
		ar << dwVersion;

		eBarCType = (DWORD)BCGetBCType(m_pBarCode);
		eCDMethod = (DWORD)BCGetCDMethod(m_pBarCode);

		ar << eBarCType;
		ar << eCDMethod;
		ar << (int)BCGetRotation(m_pBarCode);
		ar << BCGetBkMode(m_pBarCode);
		ar << BCGetPrintText(m_pBarCode);
		ar << BCGetTextAbove(m_pBarCode);
		ar << BCGetMirror(m_pBarCode);
		ar << BCGetTextDist(m_pBarCode);

		ar << (CString)BCGetModWidth(m_pBarCode);
		ar << (CString)BCGetRatio(m_pBarCode);
		ar << (CString)BCGetFormat(m_pBarCode);
		ar << BCGetBearerBarWidth(m_pBarCode);
		ar << BCGetBearerBarType(m_pBarCode);
		ar << BCGetNotchHeight(m_pBarCode);
		ar << BCGetTranslateEsc(m_pBarCode);

		// PDF417 specific information
		ar << BCGet_PDF417_Rows(m_pBarCode);
		ar << BCGet_PDF417_Columns(m_pBarCode);
		ar << BCGet_PDF417_ECLevel(m_pBarCode);
		ar << BCGet_PDF417_RowHeight(m_pBarCode);

		// MaxiCode specific information
		ar << BCGet_Maxi_Mode(m_pBarCode);
		ar << BCGet_Maxi_UnderCut(m_pBarCode);
		ar << BCGet_Maxi_UsePreamble(m_pBarCode);
		ar << (CString)BCGet_Maxi_PreambleDate(m_pBarCode);
		ar << BCGet_Maxi_AppendSum(m_pBarCode);
		ar << BCGet_Maxi_AppendIndex(m_pBarCode);
		ar << (CString)BCGet_Maxi_SCMServClass(m_pBarCode);
		ar << (CString)BCGet_Maxi_SCMCountryCode(m_pBarCode);
		ar << (CString)BCGet_Maxi_SCMPostalCode(m_pBarCode);

		ar << BCGetColorBC(m_pBarCode);
		ar << (DWORD)BCGetColorFont(m_pBarCode);
		ar << BCGetColorBk(m_pBarCode);

		ar.Write(BCGetLogFont(m_pBarCode), sizeof(LOGFONTA));

		ar << m_bAspectRatio;
		ar << m_bResRatio;
		ar << m_nHeight;
		ar << m_nWidth;
		ar << m_nHorzRes;
		ar << m_nVertRes;
		ar << m_nUnit;

		eCCType = (DWORD)BCGet2DCompositeComponent(m_pBarCode);

		ar << eCCType;


	}

}




CGraph* CTBarCode11::Clone()
{
	CTBarCode11* p = new CTBarCode11();

	p->startX = this->startX + 10;
	p->startY = this->startY + 10;
	p->endX = this->endX + 10;
	p->endY = this->endY + 10;


	p->nCodeType = this->nCodeType;
	p->m_sData = this->m_sData;

	p->Direction = this->Direction;
	p->LinePen = this->LinePen;
	p->nPenWidth = this->nPenWidth;
	p->m_nBarPropertyType = this->m_nBarPropertyType;
	p->nBitMapLenth = this->nBitMapLenth;
	p->nBitMapHeight = this->nBitMapHeight;
	p->nRotateAngle = this->nRotateAngle;
	p->nFrameType = this->nFrameType;
	p->m_nFieldIndex = this->m_nFieldIndex;
	p->nTextAlineType = this->nTextAlineType;
	p->m_nFrameType = this->m_nFrameType;
	p->m_QietZoonUnit = this->m_QietZoonUnit;
	p->m_nTop = this->m_nTop;
	p->m_nLeft = this->m_nLeft;
	p->m_nBottom = this->m_nBottom;
	p->m_nRight = this->m_nRight;
	p->m_nRotateTyp = this->m_nRotateTyp;
	p->nTextGroupType = this->nTextGroupType;
	p->nTextGroupDis = this->nTextGroupDis;
	p->nTextBarDis = this->nTextBarDis;
	p->isDefaultTextBarDis = this->isDefaultTextBarDis;
	p->TextColor = this->TextColor;
	p->TextFont = this->TextFont;
	p->sInsetParam = this->sInsetParam;
	p->sFieldTypeString = this->sFieldTypeString;
	p->sFieldTypeDis = this->sFieldTypeDis;
	p->m_strFieldDes = this->m_strFieldDes;
	p->m_strRecordVal = this->m_strRecordVal;
	p->m_strInitVal = this->m_strInitVal;
	p->nCounterStart = this->nCounterStart;
	p->nCounterEnd = this->nCounterEnd;
	p->nCounterStep = this->nCounterStep;
	p->m_nSqlTypeFieldIndex = this->m_nSqlTypeFieldIndex;
	p->sSqlData = this->sSqlData;
	p->sBarDateTypeString = this->sBarDateTypeString;
	p->nQRLevel = this->nQRLevel;
	p->nQRVersion = this->nQRVersion;
	p->bQRAutoExtent = this->bQRAutoExtent;
	p->nQRMaskingNo = this->nQRMaskingNo;
	p->fQRRotateAngle = this->fQRRotateAngle;
	p->nStartPos = this->nStartPos;
	p->nCounts = this->nCounts;
	p->m_nTimeFormatIndex = this->m_nTimeFormatIndex;
	p->bBarWidthReduction = this->bBarWidthReduction;
	p->nBarWidthReduction = this->nBarWidthReduction;

	DWORD eBarCType;  // Variable to convert enum to DWORD
	DWORD eCDMethod;  // Variable to convert enum to DWORD
	DWORD dwVersion;  // Version of bcs-file
	CString sID;    // ID of bcs-file
	DWORD   eCCType;


	eBarCType = (DWORD)BCGetBCType(this->m_pBarCode);
	BCSetBCType(p->m_pBarCode, (e_BarCType)eBarCType);

	eCDMethod = (DWORD)BCGetCDMethod(this->m_pBarCode);
	BCSetCDMethod(p->m_pBarCode, (e_CDMethod)eCDMethod);

	int n1 = (int)BCGetRotation(this->m_pBarCode);
	BCSetRotation(p->m_pBarCode, (e_Degree)n1);
	BCSetBkMode(p->m_pBarCode, BCGetBkMode(this->m_pBarCode));
	BCSetPrintText(p->m_pBarCode, BCGetPrintText(this->m_pBarCode), BCGetTextAbove(this->m_pBarCode));
	BCSetTextDist(p->m_pBarCode, BCGetTextDist(this->m_pBarCode));


	BCSetModWidth(p->m_pBarCode, (LPCTSTR)(CString)BCGetModWidth(this->m_pBarCode));
	BCSetRatio(p->m_pBarCode, (LPCTSTR)(CString)BCGetRatio(this->m_pBarCode));
	BCSetFormat(p->m_pBarCode, (LPCTSTR)(CString)BCGetFormat(this->m_pBarCode));

	BCSetBearerBarWidth(p->m_pBarCode, BCGetBearerBarWidth(this->m_pBarCode));
	BCSetBearerBarType(p->m_pBarCode, BCGetBearerBarType(this->m_pBarCode));
	BCSetNotchHeight(p->m_pBarCode, BCGetNotchHeight(this->m_pBarCode));
	BCSetTranslateEsc(p->m_pBarCode, BCGetTranslateEsc(this->m_pBarCode));

	// PDF417 specific information
	BCSet_PDF417_Rows(p->m_pBarCode, BCGet_PDF417_Rows(this->m_pBarCode));
	BCSet_PDF417_Columns(p->m_pBarCode, BCGet_PDF417_Columns(this->m_pBarCode));
	BCSet_PDF417_ECLevel(p->m_pBarCode, BCGet_PDF417_ECLevel(this->m_pBarCode));
	BCSet_PDF417_RowHeight(p->m_pBarCode, BCGet_PDF417_RowHeight(this->m_pBarCode));

	// MaxiCode specific information
	BCSet_Maxi_Mode(p->m_pBarCode, BCGet_Maxi_Mode(this->m_pBarCode));
	BCSet_Maxi_UnderCut(p->m_pBarCode, BCGet_Maxi_UnderCut(this->m_pBarCode));
	BCSet_Maxi_UsePreamble(p->m_pBarCode, BCGet_Maxi_UsePreamble(this->m_pBarCode), (LPCTSTR)(CString)BCGet_Maxi_PreambleDate(this->m_pBarCode));
	BCSet_Maxi_Append(p->m_pBarCode, BCGet_Maxi_AppendSum(this->m_pBarCode), BCGet_Maxi_AppendIndex(this->m_pBarCode));
	BCSet_Maxi_SCM(p->m_pBarCode, (CString)BCGet_Maxi_SCMServClass(this->m_pBarCode), (CString)BCGet_Maxi_SCMCountryCode(this->m_pBarCode), (CString)BCGet_Maxi_SCMPostalCode(this->m_pBarCode));

	BCSetColorBC(p->m_pBarCode, (COLORREF)BCGetColorBC(this->m_pBarCode));
	BCSetColorFont(p->m_pBarCode, (DWORD)BCGetColorFont(this->m_pBarCode));
	BCSetColorBk(p->m_pBarCode, (COLORREF)BCGetColorBk(this->m_pBarCode));
	BCSetLogFont(p->m_pBarCode, BCGetLogFont(this->m_pBarCode));


	p->m_bAspectRatio = this->m_bAspectRatio;
	p->m_bResRatio = this->m_bResRatio;
	p->m_nHeight = this->m_nHeight;
	p->m_nWidth = this->m_nWidth;
	p->m_nHorzRes = this->m_nHorzRes;
	p->m_nVertRes = this->m_nVertRes;
	p->m_nUnit = this->m_nUnit;

	p->m_dAspectRatio = (float)m_nWidth / m_nHeight;

	BCSet2DCompositeComponent(m_pBarCode, (e_CCType)(DWORD)BCGet2DCompositeComponent(this->m_pBarCode));


	p->bReadable = bReadable;
	p->bAbove = bAbove;
	p->m_bMirror = m_bMirror;

	p->_OriRect = this->_OriRect;
	p->tracker = this->tracker;
	p->bIsSelected = this->bIsSelected;
	p->bIsLocked = this->bIsLocked;

	return p;
}


void  CTBarCode11::InitPropList()
{

	CMFCPropertyGridProperty * pProp;
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMyDrawView *pView = (CMyDrawView *)pFrame->GetActiveView();

	pFrame->m_wndProperties.graphs.Add(this);
	//pFrame->m_wndProperties.m_wndObjectCombo.SetCurSel(nGraphics);
	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(0);
	//读取数据
	CString itemname;
	itemname = pProp->GetName(); //获取名称
	COleVariant itemvalue;
	itemvalue = pProp->GetValue();//获取值
	//写入数据
	CString m_startX;
	m_startX.Format(_T("%.2f"), startX / pView->ScaleX);
	pProp->SetValue((_variant_t)(m_startX));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(1);
	CString m_startY;
	m_startY.Format(_T("%.2f"), startY / pView->ScaleY);
	pProp->SetValue((_variant_t)(m_startY));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(0);
	CString m_BarWidth;
	m_BarWidth.Format(_T("%.2f"), (endX - startX) / pView->ScaleX * pView->Zoom);
	pProp->SetValue((_variant_t)(m_BarWidth));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(1);
	CString m_BarHeight;
	m_BarHeight.Format(_T("%.2f"), (endY - startY) / pView->ScaleY * pView->Zoom);
	pProp->SetValue((_variant_t)(m_BarHeight));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(2)->GetSubItem(0);
	CMFCPropertyGridFontPropertyEx * pProp2 = (CMFCPropertyGridFontPropertyEx*)pProp;

	if (pProp2 != NULL)
	{
		pProp2->SetFont(this->TextFont);
		pProp2->SetColor(this->TextColor);
		pProp2->SetName("字体");
	}

	//字段
	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(3)->GetSubItem(0);
	int nCount = 0;

	if (this->m_nBarPropertyType == 0 && this->sInsetParam.GetLength() > 0)
	{
		for (int i = 0; i <= pView->nMaxColNum; i++)
		{
			CString tmp;
			tmp.Format(_T("字段%d"), i);
			if (this->sInsetParam.Find(tmp) >= 0)
				nCount = i;
		}
	}
	else if (this->m_nBarPropertyType == 2)
		nCount = this->m_nSqlTypeFieldIndex;
	if (nCount > 0)
	{
		CString tmp;
		tmp.Format(_T("字段%d"), nCount);
		pProp->SetValue((_variant_t)(tmp));
	}
	else
		pProp->SetValue((_variant_t)(_T(" ")));



}