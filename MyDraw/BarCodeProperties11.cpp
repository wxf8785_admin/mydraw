// BarCodeProperties.cpp : implementation file
//

#include "stdafx.h"
#include "MyDraw.h"
#include "MainFrm.h"
#include "MyDrawView.h"
#include "BarCodeProperties11.h"
#include "QRcodeProperties.h"
#include "QuietZoon.h"

#include "Text.h"
#include "BarCode.h"
#include "Pic.h"
#include "TBarCode11.h"
#include <math.h>
#include <vector>
#include "Graph.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBarCodeProperties11 dialog

extern CMap<int, int, int, int> m_mapFontSize;
extern CMap<int, int, CString, CString> m_mapFontHeight;
CBarCodeProperties11::CBarCodeProperties11(CWnd* pParent /*=NULL*/)
	: CDialog(CBarCodeProperties11::IDD, pParent)
	, m_nTextAndBarDis(0)
	, bReadable(TRUE)
	, bAbove(FALSE)
	, m_bMirror(FALSE)
{
	//{{AFX_DATA_INIT(CBarCodeProperties11)
	pTBarCode11 = NULL;
	m_EditText = _T("");
	m_nStartNum = 1;
	m_nStep = 1;
	m_nEndNum = 1000;
	m_PosX = 0.0f;
	m_PosY = 0.0f;
	m_BarPenSize = 1;
	m_nFieldIndex = 1;
	m_nFieldDistance = 1;
	m_BarWidth = 0.0f;
	m_BarHeight = 0.0f;
	m_sArbGroup = _T("1,2,3,4");
	m_FixGroup = _T("0");
	m_nGroupDis=1;
	//}}AFX_DATA_INIT

	nMaxFieldNum=0;
	nFieldIndex=0;

	m_nBarTypeIndex =14;
	m_nTextAndBarDis =0;
	isDefaultTextBarDis = TRUE;

	sInsertParam = "";

	nBarPropertyType=0;
	m_nSqlTypeFieldIndex = 0;


	//二维码设置参数
	nQRLevel = 1;
	nQRVersion =  -1;
	bQRAutoExtent = 1;
	nQRMaskingNo =  0;
	fQRRotateAngle = 0;

	m_timeFormat = "YY-MM-DD HH:MM:SS";
	m_startPos = 0;
	m_counts = 0;
	nTimeFormatIndex = 0;

	//是否修边
	isUseDeburring = false;
	nBarWidthReduction = 1;
}


void CBarCodeProperties11::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBarCodeProperties11)
	DDX_Control(pDX, IDC_CHECK_DEFAULT_TEXTDIS, m_DefaultTxtDis);
	DDX_Control(pDX, IDC_COMBO_ALINE, m_AlineType);
	DDX_Control(pDX, IDC_COMBO_FIELD_DATABASE, m_DBFieldCmb);
	DDX_Control(pDX, IDC_COMBO_FRAME, m_FrameCmb);
	DDX_Control(pDX, IDC_COMBO_ROTATE_ANGLE, m_RotateAngle);
	DDX_Control(pDX, IDC_COMBO_BAR_TYPE, m_BarTypeCmb);
	DDX_Control(pDX, IDC_TAB_BAR, m_BarProptyTab);
	DDX_Control(pDX, IDC_COMBO_FIELD_INDEX, m_nBarFieldNumCmb);
	DDX_Control(pDX, IDC_RADIO_DATE, m_RadioDate);
	DDX_Control(pDX, IDC_RADIO_CONTER, m_RadioCounter);
	DDX_Control(pDX, IDC_RADIO_CONTENT, m_RadoiText);
	DDX_Control(pDX, IDC_RADIO_DATABASE, m_RadoiDataBase);
	DDX_Control(pDX, IDC_RADIO_FIX_GROUP, m_FixGroupRadio);
	DDX_Control(pDX, IDC_RADIO_ARB_GROUP, m_ArbGroupRadio);
	DDX_Text(pDX, IDC_EDIT_TEXT, m_EditText);
	DDX_Text(pDX, IDC_EDIT_START, m_nStartNum);
	DDV_MinMaxLong(pDX, m_nStartNum, 1, 999999999);
	DDX_Text(pDX, IDC_EDIT_STEP, m_nStep);
	DDV_MinMaxLong(pDX, m_nStep, 1, 2147483647);
	DDX_Text(pDX, IDC_EDIT_END, m_nEndNum);
	DDV_MinMaxLong(pDX, m_nEndNum, 1, 999999999);
	DDX_Text(pDX, IDC_EDIT_POS_X, m_PosX);
	DDX_Text(pDX, IDC_EDIT_POS_Y, m_PosY);
	DDX_Text(pDX, IDC_EDIT_PENSIZE, m_BarPenSize);
	DDV_MinMaxInt(pDX, m_BarPenSize, 0, 15);
	DDX_Text(pDX, IDC_EDIT_DISTANCE, m_nFieldDistance);
	DDV_MinMaxInt(pDX, m_nFieldDistance, 1, 10000);
	DDX_Text(pDX, IDC_EDIT_BAR_WIDTH, m_BarWidth);
	DDV_MinMaxFloat(pDX, m_BarWidth, 0.f, 99999.f);
	DDX_Text(pDX, IDC_EDIT_BAR_HEIGHT, m_BarHeight);
	DDV_MinMaxFloat(pDX, m_BarHeight, 0.f, 9999.f);
	DDX_Text(pDX, IDC_EDIT_ARB_GROUP, m_sArbGroup);
	DDX_Text(pDX, IDC_EDIT_FIX_GROUP, m_FixGroup);
	//	DDX_Text(pDX, IDC_EDIT_GROUP_DIS, m_GroupDis);
	DDX_Text(pDX, IDC_EDIT_GROUP_DIS, m_nGroupDis);
	DDV_MinMaxInt(pDX, m_nGroupDis, 0, 999999);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_EDIT_TEXT_BAR_DIS, m_nTextAndBarDis);
	DDV_MinMaxInt(pDX, m_nTextAndBarDis, 0, 1000);
	DDX_Control(pDX, IDC_COMBO_TIME_FORMAT, m_timeFormatCmb);
	DDX_Control(pDX, IDC_COMBO_BAR_PENSIZE, m_PENSIZEcmb);
	DDX_Text(pDX, IDC_EDIT_STARTPOS, m_startPos);
	DDX_Text(pDX, IDC_EDIT_COUNTS, m_counts);

	DDX_Control(pDX, IDC_LIST_FONT_TYPE, m_listFontType);
	DDX_Control(pDX, IDC_LIST_FONT_STYLE, m_listFontStyle);
	DDX_Control(pDX, IDC_LIST_FONT_SIZE, m_listFontSize);
	DDX_Control(pDX, IDC_STATIC_FONT_TMP, m_staticFontTmp);
	DDX_Text(pDX, IDC_EDIT_FONT_TYPE2, m_strFontType);
	DDX_Text(pDX, IDC_EDIT_FONT_STYLE, m_strFontStyle);
	DDX_Text(pDX, IDC_EDIT_FONT_SIZE, m_strFontSize);
	DDX_Control(pDX, IDC_CHECK_FONT_UNDERLINE2, m_underLine);
	DDX_Control(pDX, IDC_CHECK_FONT_STRIKEOUT2, m_strikeOut);

	DDX_Control(pDX, IDC_CHECK_DEBURRING, m_deburring);
	DDX_Control(pDX, IDC_COMBO_BAR_BURRING, m_BURRINGcmb);

	DDX_Check(pDX, IDC_CHECK_Readable, bReadable);
	DDX_Check(pDX, IDC_CHECK_Above, bAbove);
	DDX_Check(pDX, IDC_CHECK_Mirror, m_bMirror);
}

//afx_msg void OnSelchangeComboFrameType();
//afx_msg void OnSelchangeComboRotateType();
//afx_msg void OnSelchangeComboAlineType();


BEGIN_MESSAGE_MAP(CBarCodeProperties11, CDialog)
	//{{AFX_MSG_MAP(CBarCodeProperties11)
	ON_BN_CLICKED(IDC_RADIO_CONTENT, OnRadioContent)
	ON_BN_CLICKED(IDC_RADIO_CONTER, OnRadioConter)
	ON_BN_CLICKED(IDC_RADIO_DATE, OnRadioDate)
	ON_CBN_SELCHANGE(IDC_COMBO_FIELD_INDEX, OnSelchangeComboFieldIndex)
	ON_CBN_SELCHANGE(IDC_COMBO_BAR_TYPE, OnSelchangeComboBarType)
	ON_CBN_SELCHANGE(IDC_COMBO_FRAME, OnSelchangeComboFrameType)
	ON_CBN_SELCHANGE(IDC_COMBO_ROTATE_ANGLE, OnSelchangeComboRotateType)
	ON_CBN_SELCHANGE(IDC_COMBO_ALINE, OnSelchangeComboAlineType)
	ON_BN_CLICKED(IDC_BUTTO_BAR_FONT, OnButtoBarFont)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_BAR, OnSelchangeTabBar)
	ON_BN_CLICKED(IDC_RADIO_DATABASE, OnRadioDatabase)
	ON_BN_CLICKED(IDC_RADIO_ARB_GROUP, OnRadioArbGroup)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CHECK_DEFAULT_TEXTDIS, &CBarCodeProperties11::OnBnClickedCheckDefaultTextdis)
	ON_BN_CLICKED(IDC_BUTTON_INSERT, &CBarCodeProperties11::OnBnClickedButtonInsert)
	ON_BN_CLICKED(IDC_RADIO_FIX_GROUP, &CBarCodeProperties11::OnBnClickedRadioFixGroup)
	ON_CBN_SELCHANGE(IDC_COMBO_FIELD_DATABASE, &CBarCodeProperties11::OnCbnSelchangeComboFieldDatabase)
	ON_BN_CLICKED(IDC_BUTTON_QRcodeProperties, &CBarCodeProperties11::OnBnClickedButtonQrcodeproperties)
	ON_BN_CLICKED(IDOK, &CBarCodeProperties11::OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_COMBO_BAR_PENSIZE, &CBarCodeProperties11::OnCbnSelchangeComboBarPensize)
	ON_CBN_SELCHANGE(IDC_COMBO_TIME_FORMAT, &CBarCodeProperties11::OnCbnSelchangeComboTimeFormat)
	ON_BN_CLICKED(IDYES, &CBarCodeProperties11::OnBnClickedYes)
	ON_LBN_SELCHANGE(IDC_LIST_FONT_TYPE, &CBarCodeProperties11::OnLbnSelChangeFontType)
	ON_LBN_SELCHANGE(IDC_LIST_FONT_STYLE, &CBarCodeProperties11::OnLbnSelChangeFontStyle)
	ON_LBN_SELCHANGE(IDC_LIST_FONT_SIZE, &CBarCodeProperties11::OnLbnSelChangeFontSize)
	ON_BN_CLICKED(IDC_CHECK_FONT_UNDERLINE2, &CBarCodeProperties11::OnBnClickedCheckUnderLine)
	ON_BN_CLICKED(IDC_CHECK_FONT_STRIKEOUT2, &CBarCodeProperties11::OnBnClickedCheckStikeOut)
	ON_BN_CLICKED(IDC_CHECK_DEBURRING, &CBarCodeProperties11::OnBnClickedCheckDeburring)
	ON_BN_CLICKED(IDC_BUTTON_quietZoon, &CBarCodeProperties11::OnBnClickedButtonquietzoon)
	ON_CBN_SELCHANGE(IDC_COMBO_BAR_BURRING, &CBarCodeProperties11::OnCbnSelchangeComboBarBurring)
	ON_EN_CHANGE(IDC_EDIT_FONT_SIZE, &CBarCodeProperties11::OnEnChangeEditTxtFontSize)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBarCodeProperties11 message handlers

BOOL CBarCodeProperties11::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	m_BarProptyTab.InsertItem(0,"位置",0);
	m_BarProptyTab.InsertItem(1,"内容",1);
	m_BarProptyTab.InsertItem(2,"条码",2);
	m_BarProptyTab.InsertItem(3,"外形",3);
	m_BarProptyTab.InsertItem(4,"字体",4);

	m_BarProptyTab.SetCurSel(1);

	m_BarTypeCmb.AddString("EAN 8");
	m_BarTypeCmb.AddString("EAN 13");
	m_BarTypeCmb.AddString("EAN 128");
	//m_BarTypeCmb.AddString("Code 25");
	m_BarTypeCmb.AddString("Code 39");
	m_BarTypeCmb.AddString("Code 128");
	m_BarTypeCmb.AddString("Codabar 2 Widths");
	m_BarTypeCmb.AddString("PDF417");
	m_BarTypeCmb.AddString("QRCode");
	m_BarTypeCmb.AddString("MaxiCode");
	m_BarTypeCmb.AddString("DataMatrix");
	//m_BarTypeCmb.AddString("PIATS");
	//m_BarTypeCmb.AddString("PIATSDRUG");
	m_BarTypeCmb.AddString("Code 93");
	m_BarTypeCmb.AddString("UPC12");
	m_BarTypeCmb.AddString("Code 128A");
	m_BarTypeCmb.AddString("Code 128B");
	m_BarTypeCmb.AddString("Code 128C");
	m_BarTypeCmb.AddString("UPC - E");
	m_BarTypeCmb.AddString("ITF - 14");
	m_BarTypeCmb.AddString("Code 2of5 Interleaved");

	m_BarTypeCmb.SetCurSel(m_nBarTypeIndex);


	///选择角度
	m_RotateAngle.AddString("0°");
	m_RotateAngle.AddString("90°");
	m_RotateAngle.AddString("180°");
	m_RotateAngle.AddString("270°");
	m_RotateAngle.SetCurSel(m_nRotateTypeIndex);

	m_FrameCmb.AddString("无边框");
	m_FrameCmb.AddString("上边框");
	m_FrameCmb.AddString("下边框");
	m_FrameCmb.AddString("上下边框");
	m_FrameCmb.SetCurSel(m_nFrameTypeIndex);

	m_AlineType.AddString("默认");
	m_AlineType.AddString("左对齐");
	m_AlineType.AddString("右对齐");
	m_AlineType.AddString("居中对齐");
	m_AlineType.SetCurSel(m_nAlineTypeIndex);
	

	m_RadoiText.SetCheck(true);

	if (isDefaultTextBarDis)
	{
		m_DefaultTxtDis.SetCheck(1);
		GetDlgItem(IDC_EDIT_TEXT_BAR_DIS)->EnableWindow(false);
	}
	else
	{
		m_DefaultTxtDis.SetCheck(0);
		GetDlgItem(IDC_EDIT_TEXT_BAR_DIS)->EnableWindow(true);
	}

	if (nTextSpreadType == 1)//任意分组
	{
		m_ArbGroupRadio.SetCheck(1);
		m_FixGroupRadio.SetCheck(0);
		GetDlgItem(IDC_EDIT_FIX_GROUP)->EnableWindow(false);
		GetDlgItem(IDC_EDIT_ARB_GROUP)->EnableWindow(true);
	}
	else		//固定分组
	{
		m_ArbGroupRadio.SetCheck(0);
		m_FixGroupRadio.SetCheck(1);
		GetDlgItem(IDC_EDIT_FIX_GROUP)->EnableWindow(true);
		GetDlgItem(IDC_EDIT_ARB_GROUP)->EnableWindow(false);
		
	}


	OnIninFieldNumCom(nMaxFieldNum,nFieldIndex,m_nSqlTypeFieldIndex);

	if (nBarPropertyType ==0)
	{
		OnRadioContent();
	}
	else if (nBarPropertyType ==1)
	{
		OnRadioConter();
	}
	else if (nBarPropertyType ==2)
	{
		OnRadioDatabase();
	}
	else if (nBarPropertyType ==3)
	{
		OnRadioDate();
	}
	
	for (int i = 4; i < 50; i++)
	{
		CString str;
		str.Format("%.7f", 0.254*i / 6);
		m_PENSIZEcmb.AddString(str);
	}

	for (int i = 0; i < 4; i++)
	{
		CString str;
		str.Format("%d", i + 1);
		m_BURRINGcmb.AddString(str);
	}
	m_BURRINGcmb.SetCurSel(nBarWidthReduction-1);

	int BarPenSizeIndex = int (m_BarPenSize * 6 + 0.5) - 4;
	m_PENSIZEcmb.SetCurSel(BarPenSizeIndex);

	if (isUseDeburring)
	{
		m_deburring.SetCheck(1);
		GetDlgItem(IDC_COMBO_BAR_BURRING)->EnableWindow(true);
	}
	else
	{
		m_deburring.SetCheck(0);
		GetDlgItem(IDC_COMBO_BAR_BURRING)->EnableWindow(false);
	}

	SetCtrlVisible(1);
	OnInitTimeFormat(nTimeFormatIndex);

	OnInitFontCtrl();

	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子   
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CBarCodeProperties11::OnRadioContent() 
{
	// TODO: Add your control notification handler code here
	nBarPropertyType = 0;
	m_RadoiText.SetCheck(true);
	m_RadioCounter.SetCheck(false);
	m_RadioDate.SetCheck(false);
	m_RadoiDataBase.SetCheck(false);


	GetDlgItem(IDC_EDIT_TEXT)->EnableWindow(true);
	GetDlgItem(IDC_COMBO_FIELD_INDEX)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_INSERT)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_DISTANCE)->EnableWindow(true);

	GetDlgItem(IDC_STATIC_1)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_START)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_END)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_STEP)->EnableWindow(false);

	GetDlgItem(IDC_COMBO_FIELD_DATABASE)->EnableWindow(false);

	GetDlgItem(IDC_GROUP_Text_Field)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_COUNTS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_COUNTS)->EnableWindow(false);

	GetDlgItem(IDC_COMBO_TIME_FORMAT)->EnableWindow(false);
}

void CBarCodeProperties11::OnRadioConter() 
{
	// TODO: Add your control notification handler code here
	nBarPropertyType = 1;
	m_RadoiText.SetCheck(false);
	m_RadioCounter.SetCheck(true);
	m_RadioDate.SetCheck(false);
	m_RadoiDataBase.SetCheck(false);

	GetDlgItem(IDC_EDIT_TEXT)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_FIELD_INDEX)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_INSERT)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_DISTANCE)->EnableWindow(false);

	GetDlgItem(IDC_STATIC_1)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_START)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_END)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_STEP)->EnableWindow(true);

	GetDlgItem(IDC_COMBO_FIELD_DATABASE)->EnableWindow(false);

	GetDlgItem(IDC_GROUP_Text_Field)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_COUNTS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_COUNTS)->EnableWindow(false);

	GetDlgItem(IDC_COMBO_TIME_FORMAT)->EnableWindow(false);

}

void CBarCodeProperties11::OnRadioDatabase() 
{
	// TODO: Add your control notification handler code here
	nBarPropertyType = 2;
	m_RadoiText.SetCheck(false);
	m_RadioCounter.SetCheck(false);
	m_RadioDate.SetCheck(false);
	m_RadoiDataBase.SetCheck(true);


	GetDlgItem(IDC_EDIT_TEXT)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_FIELD_INDEX)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_INSERT)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_DISTANCE)->EnableWindow(false);

	GetDlgItem(IDC_STATIC_1)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_START)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_END)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_STEP)->EnableWindow(false);

	GetDlgItem(IDC_COMBO_FIELD_DATABASE)->EnableWindow(true);

	GetDlgItem(IDC_GROUP_Text_Field)->EnableWindow(true);
	GetDlgItem(IDC_STATIC_STARTPOS)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_STARTPOS)->EnableWindow(true);
	GetDlgItem(IDC_STATIC_COUNTS)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_COUNTS)->EnableWindow(true);

	GetDlgItem(IDC_COMBO_TIME_FORMAT)->EnableWindow(false);
}

void CBarCodeProperties11::OnRadioDate() 
{
	// TODO: Add your control notification handler code here
	nBarPropertyType = 3;
	m_RadoiText.SetCheck(false);
	m_RadioCounter.SetCheck(false);
	m_RadioDate.SetCheck(true);
	m_RadoiDataBase.SetCheck(false);

	GetDlgItem(IDC_EDIT_TEXT)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_FIELD_INDEX)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_INSERT)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_DISTANCE)->EnableWindow(false);

	
	GetDlgItem(IDC_STATIC_1)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_START)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_END)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_STEP)->EnableWindow(false);


	GetDlgItem(IDC_COMBO_FIELD_DATABASE)->EnableWindow(false);

	GetDlgItem(IDC_GROUP_Text_Field)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_COUNTS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_COUNTS)->EnableWindow(false);

	GetDlgItem(IDC_COMBO_TIME_FORMAT)->EnableWindow(true);

}

void CBarCodeProperties11::GetInitParam(int nMxFdNum,int nFieldCmbIndex)
{
	nMaxFieldNum=nMxFdNum;
	nFieldIndex=nFieldCmbIndex;
}


void CBarCodeProperties11::OnIninFieldNumCom(int nNum,int nIndex,int SqlFieldIndex)
{
	if(nNum<1)
		return;
	if(nIndex>nNum)
		nIndex =nNum;

	//数据库字段使用次数统计    //黑色的是固定数据，绿色的是一个字段，蓝色的是出现两次相通字段，红色是出现三次以上都是红色，
	map<int, int>m_nCountsUse;

	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;


	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;
	CObject* pObject = NULL;
	POSITION pos = pView->m_ObjectList.GetHeadPosition();
	while (pos != NULL)
	{
		pObject = (CObject*)pView->m_ObjectList.GetNext(pos);

		if (pObject->IsKindOf(RUNTIME_CLASS(CPic)))
		{

		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pObject;
			if (pText != NULL)
			{
				if (pText->nConstValueType == 0 && pText->sInsetSqlParam.GetLength() > 0)
				{
					for (int i = 0; i <= pView->nMaxColNum; i++)
					{
						CString tmp;
						tmp.Format(_T("字段%d"), i);
						if (pText->sInsetSqlParam.Find(tmp) >= 0)
							m_nCountsUse[i]++;
					}
				}
				else if (pText->nConstValueType == 2)
				{
					int nCount = pText->m_nFieldIndex;
					m_nCountsUse[nCount]++;
				}
			}

		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{

		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pObject;
			if (pTBarCode11 != NULL)
			{
				if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->sInsetParam.GetLength() > 0)
				{
					for (int i = 0; i <= pView->nMaxColNum; i++)
					{
						CString tmp;
						tmp.Format(_T("字段%d"), i);
						if (pTBarCode11->sInsetParam.Find(tmp) >= 0)
							m_nCountsUse[i]++;
					}

				}
				else if (pTBarCode11->m_nBarPropertyType == 2)
				{
					int nCount = pTBarCode11->m_nSqlTypeFieldIndex;
					m_nCountsUse[nCount]++;
				}
			}
		}
	}

	CString sText = "";
	for(int i=0;i<nNum;i++)
	{
		sText.Format("字段%d",i+1);
		int nIndex = m_nBarFieldNumCmb.AddString(sText);

		COLORREF pColor = 0;
		if (m_nCountsUse[i + 1] == 1)
			pColor = 1;
		if (m_nCountsUse[i + 1] == 2)
			pColor = 2;
		if (m_nCountsUse[i + 1] >= 3)
			pColor = 3;
		if (pColor > 0)
			m_nBarFieldNumCmb.SetColor(nIndex, pColor);

		nIndex = m_DBFieldCmb.AddString(sText);
		if (pColor > 0)
			m_DBFieldCmb.SetColor(nIndex, pColor);
	}

	if(nIndex<1)
		nIndex = 1;
	if (SqlFieldIndex<1)
	{
		SqlFieldIndex = 1;
	}
	m_nSqlTypeFieldIndex = SqlFieldIndex;
	m_nBarFieldNumCmb.SetCurSel(nIndex-1);
	m_DBFieldCmb.SetCurSel(SqlFieldIndex-1);

}

void CBarCodeProperties11::OnSelchangeComboFrameType()
{
	CString strName = "";
	int nIndex = m_FrameCmb.GetCurSel();

	if (nIndex == CB_ERR)
	{
		m_nFrameTypeIndex = 0;
		return;
	}
	m_nFrameTypeIndex = nIndex;
}
void CBarCodeProperties11::OnSelchangeComboRotateType()
{

	CString strName = "";
	int nIndex = m_RotateAngle.GetCurSel();

	if (nIndex == CB_ERR)
	{
		m_nRotateTypeIndex = 0;
		return;
	}
	m_nRotateTypeIndex = nIndex;

}
void CBarCodeProperties11::OnSelchangeComboAlineType()
{
	CString strName = "";
	int nIndex = m_AlineType.GetCurSel();

	if (nIndex == CB_ERR)
	{
		m_nAlineTypeIndex = 0;
		return;
	}
	m_nAlineTypeIndex = nIndex;

}
void CBarCodeProperties11::OnSelchangeComboBarType()
{
	CString strName = "";
	int nIndex = m_BarTypeCmb.GetCurSel();

	if(nIndex == 7)
		//二维码
		GetDlgItem(IDC_BUTTON_QRcodeProperties)->ShowWindow(true);
	else
		GetDlgItem(IDC_BUTTON_QRcodeProperties)->ShowWindow(false);


	if (nIndex == CB_ERR)
	{
		m_nBarTypeIndex = 14;
		return;
	}
	m_nBarTypeIndex = nIndex;

}

void CBarCodeProperties11::OnSelchangeComboFieldIndex() 
{
	// TODO: Add your control notification handler code here
//	nFieldIndex
	CString strName = "";
	int nIndex = m_nBarFieldNumCmb.GetCurSel();
	
	if (nIndex == CB_ERR)
	{
		nFieldIndex = 0;
		return;
	}
	nFieldIndex = nIndex;
}

void CBarCodeProperties11::OnButtoBarFont() 
{
	// TODO: Add your control notification handler code here
	CFontDialog dlg(&TextFont);
	if(dlg.DoModal()==IDOK)
	{
		dlg.GetCurrentFont(&TextFont);
		TextColor=dlg.m_cf.rgbColors;
	}
}
void CBarCodeProperties11::SetCtrlVisible(int nView)
{
	if(nView==0)
	{
		////位置
		GetDlgItem(IDC_STATIC_POS)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_WIDTH)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_BAR_WIDTH)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_UNILT)->ShowWindow(true);


		GetDlgItem(IDC_STATIC_HEIGHT)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_BAR_HEIGHT)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_POS_X2_UNILT)->ShowWindow(true);


		GetDlgItem(IDC_STATIC_POS_X)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_POS_X)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_POS_X_UNILT)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_TOP)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_POS_Y)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_POS_Y_UNILT)->ShowWindow(true);

		//内容
		GetDlgItem(IDC_STATIC_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TEXT)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_FIELD_INDEX)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_INSERT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_DISTANCE)->ShowWindow(false);

		GetDlgItem(IDC_RADIO_DATABASE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FILED_NAME)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_FIELD_DATABASE)->ShowWindow(false);


		GetDlgItem(IDC_STATIC_LINE_DIS)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_CONTER)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_1)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_START)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_2)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_END)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_STEP)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_STEP)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_DATE)->ShowWindow(false);

		//条码
		GetDlgItem(IDC_GROUP_BARCODE_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_BAR_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_BAR_TYPE)->ShowWindow(false);
	
		////外形		
		GetDlgItem(IDC_GROUP_COMM)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_ROTATE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_ROTATE_ANGLE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FRAME)->ShowWindow(false);		
		GetDlgItem(IDC_COMBO_FRAME)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_quietZoon)->ShowWindow(false);
		GetDlgItem(IDC_GROUP_BARTEXT_SETTING)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_ALINE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_ALINE)->ShowWindow(false);


		GetDlgItem(IDC_RADIO_FIX_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FIX_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_DIS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_GROUP_DIS)->ShowWindow(false);
		
	
		GetDlgItem(IDC_RADIO_ARB_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_ARB_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TEXT_DIS)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_DEFAULT_TEXTDIS)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_MIN_PENSIZE)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_CPEN_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PENSIZE)->ShowWindow(false); 
		GetDlgItem(IDC_COMBO_BAR_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TEXT_BAR_DIS)->ShowWindow(false);
		

		/////字体和颜色
		GetDlgItem(IDC_GROUP_BARTEXT_CFONT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT)->ShowWindow(false);
		GetDlgItem(IDC_BUTTO_BAR_FONT)->ShowWindow(false);

		GetDlgItem(IDC_LIST_FONT_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT_TMP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_TYPE2)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_UNDERLINE2)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_STRIKEOUT2)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_EFFECT2)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_TEMP2)->ShowWindow(false);


		//二维码
		GetDlgItem(IDC_BUTTON_QRcodeProperties)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_Text_Field)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_COUNTS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_COUNTS)->ShowWindow(false);

		GetDlgItem(IDC_COMBO_TIME_FORMAT)->ShowWindow(false);

		//修边
		GetDlgItem(IDC_GROUP_TXT_DEBURRING)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_DEBURRING)->ShowWindow(false);

		GetDlgItem(IDC_COMBO_BAR_BURRING)->ShowWindow(false);



		GetDlgItem(IDC_CHECK_Readable)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_Above)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_Mirror)->ShowWindow(false);

	}
	else if(nView==1)
	{

		////位置
		GetDlgItem(IDC_STATIC_POS)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_BAR_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_UNILT)->ShowWindow(false);


		GetDlgItem(IDC_STATIC_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_BAR_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_X2_UNILT)->ShowWindow(false);


		GetDlgItem(IDC_STATIC_POS_X)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_POS_X)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_X_UNILT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TOP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_POS_Y)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_Y_UNILT)->ShowWindow(false);


		///内容
		GetDlgItem(IDC_STATIC_CONTENT)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_CONTENT)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TEXT)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_FIELD_INDEX)->ShowWindow(true);
		GetDlgItem(IDC_BUTTON_INSERT)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_DISTANCE)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_LINE_DIS)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_CONTER)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_1)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_START)->ShowWindow(true);

		GetDlgItem(IDC_RADIO_DATABASE)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_FILED_NAME)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_FIELD_DATABASE)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_2)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_END)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_STEP)->ShowWindow(true);

		GetDlgItem(IDC_EDIT_STEP)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_DATE)->ShowWindow(true);

		//条码
		GetDlgItem(IDC_GROUP_BARCODE_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_BAR_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_BAR_TYPE)->ShowWindow(false);

		////外形		
		GetDlgItem(IDC_GROUP_COMM)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_ROTATE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_ROTATE_ANGLE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FRAME)->ShowWindow(false);		
		GetDlgItem(IDC_COMBO_FRAME)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_quietZoon)->ShowWindow(false);
		GetDlgItem(IDC_GROUP_BARTEXT_SETTING)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_ALINE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_ALINE)->ShowWindow(false);


		GetDlgItem(IDC_RADIO_FIX_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FIX_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_DIS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_GROUP_DIS)->ShowWindow(false);


		GetDlgItem(IDC_RADIO_ARB_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_ARB_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TEXT_DIS)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_DEFAULT_TEXTDIS)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_MIN_PENSIZE)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_CPEN_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_BAR_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TEXT_BAR_DIS)->ShowWindow(false);


		/////字体和颜色
		GetDlgItem(IDC_GROUP_BARTEXT_CFONT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT)->ShowWindow(false);
		GetDlgItem(IDC_BUTTO_BAR_FONT)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT_TMP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_TYPE2)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_UNDERLINE2)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_STRIKEOUT2)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_EFFECT2)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_TEMP2)->ShowWindow(false);

		
		//二维码
		GetDlgItem(IDC_BUTTON_QRcodeProperties)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_Text_Field)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_STARTPOS)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_STARTPOS)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_COUNTS)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_COUNTS)->ShowWindow(true);

		GetDlgItem(IDC_COMBO_TIME_FORMAT)->ShowWindow(true);


		//修边
		GetDlgItem(IDC_GROUP_TXT_DEBURRING)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_DEBURRING)->ShowWindow(false);

		GetDlgItem(IDC_COMBO_BAR_BURRING)->ShowWindow(false);

		GetDlgItem(IDC_CHECK_Readable)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_Above)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_Mirror)->ShowWindow(false);

	}
	else if(nView==2)
	{
			////位置
		GetDlgItem(IDC_STATIC_POS)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_BAR_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_UNILT)->ShowWindow(false);


		GetDlgItem(IDC_STATIC_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_BAR_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_X2_UNILT)->ShowWindow(false);


		GetDlgItem(IDC_STATIC_POS_X)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_POS_X)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_X_UNILT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TOP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_POS_Y)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_Y_UNILT)->ShowWindow(false);


		///内容
		GetDlgItem(IDC_STATIC_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TEXT)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_FIELD_INDEX)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_INSERT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_DISTANCE)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_LINE_DIS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_CPEN_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_BAR_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_CONTER)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_1)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_START)->ShowWindow(false);

		GetDlgItem(IDC_RADIO_DATABASE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FILED_NAME)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_FIELD_DATABASE)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_2)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_END)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_STEP)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_STEP)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_DATE)->ShowWindow(false);

		//条码
		GetDlgItem(IDC_GROUP_BARCODE_TYPE)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_BAR_TYPE)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_BAR_TYPE)->ShowWindow(true);

		////外形		
		GetDlgItem(IDC_GROUP_COMM)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_ROTATE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_ROTATE_ANGLE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FRAME)->ShowWindow(false);		
		GetDlgItem(IDC_COMBO_FRAME)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_quietZoon)->ShowWindow(false);
		GetDlgItem(IDC_GROUP_BARTEXT_SETTING)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_ALINE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_ALINE)->ShowWindow(false);


		GetDlgItem(IDC_RADIO_FIX_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FIX_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_DIS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_GROUP_DIS)->ShowWindow(false);


		GetDlgItem(IDC_RADIO_ARB_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_ARB_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TEXT_DIS)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_DEFAULT_TEXTDIS)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_MIN_PENSIZE)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_CPEN_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_BAR_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TEXT_BAR_DIS)->ShowWindow(false);


		/////字体和颜色
		GetDlgItem(IDC_GROUP_BARTEXT_CFONT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT)->ShowWindow(false);
		GetDlgItem(IDC_BUTTO_BAR_FONT)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT_TMP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_TYPE2)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_UNDERLINE2)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_STRIKEOUT2)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_EFFECT2)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_TEMP2)->ShowWindow(false);

		//二维码
		int nIndex = m_BarTypeCmb.GetCurSel();
		if(nIndex == 7)
			GetDlgItem(IDC_BUTTON_QRcodeProperties)->ShowWindow(true);
		else
			GetDlgItem(IDC_BUTTON_QRcodeProperties)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_Text_Field)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_COUNTS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_COUNTS)->ShowWindow(false);

		GetDlgItem(IDC_COMBO_TIME_FORMAT)->ShowWindow(false);

		//修边
		GetDlgItem(IDC_GROUP_TXT_DEBURRING)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_DEBURRING)->ShowWindow(false);

		GetDlgItem(IDC_COMBO_BAR_BURRING)->ShowWindow(false);

		GetDlgItem(IDC_CHECK_Readable)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_Above)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_Mirror)->ShowWindow(false);
		
	}
	else if(nView==3)
	{
		////位置
		GetDlgItem(IDC_STATIC_POS)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_BAR_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_UNILT)->ShowWindow(false);


		GetDlgItem(IDC_STATIC_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_BAR_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_X2_UNILT)->ShowWindow(false);


		GetDlgItem(IDC_STATIC_POS_X)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_POS_X)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_X_UNILT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TOP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_POS_Y)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_Y_UNILT)->ShowWindow(false);


		///内容
		GetDlgItem(IDC_STATIC_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TEXT)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_FIELD_INDEX)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_INSERT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_DISTANCE)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_LINE_DIS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_CPEN_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_BAR_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_CONTER)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_1)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_START)->ShowWindow(false);

		GetDlgItem(IDC_RADIO_DATABASE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FILED_NAME)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_FIELD_DATABASE)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_2)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_END)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_STEP)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_STEP)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_DATE)->ShowWindow(false);

		///条码
		GetDlgItem(IDC_GROUP_BARCODE_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_BAR_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_BAR_TYPE)->ShowWindow(false);


		////外形
		GetDlgItem(IDC_GROUP_COMM)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_ROTATE)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_ROTATE_ANGLE)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_FRAME)->ShowWindow(true);		
		GetDlgItem(IDC_COMBO_FRAME)->ShowWindow(true);
		GetDlgItem(IDC_BUTTON_quietZoon)->ShowWindow(true);
		GetDlgItem(IDC_GROUP_BARTEXT_SETTING)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_ALINE)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_ALINE)->ShowWindow(true);


		GetDlgItem(IDC_RADIO_FIX_GROUP)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_FIX_GROUP)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_GROUP_DIS)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_GROUP_DIS)->ShowWindow(true);
		
	
		GetDlgItem(IDC_RADIO_ARB_GROUP)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_ARB_GROUP)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_TEXT_DIS)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_DEFAULT_TEXTDIS)->ShowWindow(true);

		GetDlgItem(IDC_GROUP_MIN_PENSIZE)->ShowWindow(false);

		//GetDlgItem(IDC_STATIC_CPEN_WIDTH)->ShowWindow(true);
		//GetDlgItem(IDC_EDIT_PENSIZE)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_BAR_PENSIZE)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_TEXT_BAR_DIS)->ShowWindow(true);

		/////字体和颜色
		GetDlgItem(IDC_GROUP_BARTEXT_CFONT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT)->ShowWindow(false);
		GetDlgItem(IDC_BUTTO_BAR_FONT)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT_TMP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_TYPE2)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_UNDERLINE2)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_STRIKEOUT2)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_EFFECT2)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_TEMP2)->ShowWindow(false);

		//二维码
		GetDlgItem(IDC_BUTTON_QRcodeProperties)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_Text_Field)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_COUNTS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_COUNTS)->ShowWindow(false);

		GetDlgItem(IDC_COMBO_TIME_FORMAT)->ShowWindow(false);

		//修边
		GetDlgItem(IDC_GROUP_TXT_DEBURRING)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_DEBURRING)->ShowWindow(true);

		GetDlgItem(IDC_COMBO_BAR_BURRING)->ShowWindow(true);


		GetDlgItem(IDC_CHECK_Readable)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_Above)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_Mirror)->ShowWindow(true);
	}
	else if(nView==4)
	{
				////位置
		GetDlgItem(IDC_STATIC_POS)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_BAR_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_UNILT)->ShowWindow(false);


		GetDlgItem(IDC_STATIC_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_BAR_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_X2_UNILT)->ShowWindow(false);


		GetDlgItem(IDC_STATIC_POS_X)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_POS_X)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_X_UNILT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TOP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_POS_Y)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POS_Y_UNILT)->ShowWindow(false);


		///内容
		GetDlgItem(IDC_STATIC_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TEXT)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_FIELD_INDEX)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_INSERT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_DISTANCE)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_LINE_DIS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_CPEN_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_BAR_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_CONTER)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_1)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_START)->ShowWindow(false);

		GetDlgItem(IDC_RADIO_DATABASE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FILED_NAME)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_FIELD_DATABASE)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_2)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_END)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_STEP)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_STEP)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_DATE)->ShowWindow(false);

		///条码
		
		GetDlgItem(IDC_GROUP_BARCODE_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_BAR_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_BAR_TYPE)->ShowWindow(false);


		////外形
		GetDlgItem(IDC_GROUP_COMM)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_ROTATE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_ROTATE_ANGLE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FRAME)->ShowWindow(false);		
		GetDlgItem(IDC_COMBO_FRAME)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_quietZoon)->ShowWindow(false);
		GetDlgItem(IDC_GROUP_BARTEXT_SETTING)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_ALINE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_ALINE)->ShowWindow(false);


		GetDlgItem(IDC_RADIO_FIX_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FIX_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_DIS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_GROUP_DIS)->ShowWindow(false);
		
	
		GetDlgItem(IDC_RADIO_ARB_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_ARB_GROUP)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TEXT_DIS)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_DEFAULT_TEXTDIS)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_MIN_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_CPEN_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_BAR_PENSIZE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TEXT_BAR_DIS)->ShowWindow(false);

		/////字体和颜色	
		GetDlgItem(IDC_GROUP_BARTEXT_CFONT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT)->ShowWindow(false);
		GetDlgItem(IDC_BUTTO_BAR_FONT)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_TYPE)->ShowWindow(true);
		GetDlgItem(IDC_LIST_FONT_STYLE)->ShowWindow(true);
		GetDlgItem(IDC_LIST_FONT_SIZE)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_FONT_TMP)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_FONT_TYPE2)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_FONT_STYLE)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_FONT_SIZE)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_FONT_UNDERLINE2)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_FONT_STRIKEOUT2)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_GROUP_EFFECT2)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_GROUP_TEMP2)->ShowWindow(true);
		//二维码
		GetDlgItem(IDC_BUTTON_QRcodeProperties)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_Text_Field)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_COUNTS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_COUNTS)->ShowWindow(false);

		GetDlgItem(IDC_COMBO_TIME_FORMAT)->ShowWindow(false);

		//修边
		GetDlgItem(IDC_GROUP_TXT_DEBURRING)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_DEBURRING)->ShowWindow(false);

		GetDlgItem(IDC_COMBO_BAR_BURRING)->ShowWindow(false);

		GetDlgItem(IDC_CHECK_Readable)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_Above)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_Mirror)->ShowWindow(false);
	}
}
void CBarCodeProperties11::OnSelchangeTabBar(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	int nIndex = m_BarProptyTab.GetCurSel();
	SetCtrlVisible(nIndex);
	*pResult = 0;
}



void CBarCodeProperties11::OnBnClickedCheckDefaultTextdis()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_DefaultTxtDis.GetCheck())
	{
		isDefaultTextBarDis = true;
		GetDlgItem(IDC_EDIT_TEXT_BAR_DIS)->EnableWindow(false);
	}
	else
	{
		isDefaultTextBarDis = false;
		m_nTextAndBarDis=0;
		GetDlgItem(IDC_EDIT_TEXT_BAR_DIS)->EnableWindow(true);
	}
	UpdateData(false);
}

void CBarCodeProperties11::OnBnClickedButtonInsert()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();

	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_TEXT);
 	ASSERT(pEdit);
	CString str;
	pEdit->GetWindowText(str);
	int nPos;

	pEdit->GetSel(nPos, nPos); 

 	pEdit->SetSel(-1,0);//选中光标的当前位置
	int nIndex = m_nBarFieldNumCmb.GetCurSel()+1;
	if (nIndex>0)
	{
		CString sData = "";
		sData.Format("[字段%d]",nIndex);
		pEdit->ReplaceSel(sData);//
		CString sTmp="";
		sTmp.Format("%d_[字段%d]_%d_#*",nPos,nIndex,nIndex);
		if (str.Find("[字段") >= 0) //如果插入多个字段
			sInsertParam += (sTmp);
		else
			sInsertParam = (sTmp);
	}
	
 
}

void CBarCodeProperties11::OnBnClickedRadioFixGroup()
{
	// TODO: 在此添加控件通知处理程序代码
	nTextSpreadType = 0;
	m_ArbGroupRadio.SetCheck(0);
	m_FixGroupRadio.SetCheck(1);
	GetDlgItem(IDC_EDIT_FIX_GROUP)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_ARB_GROUP)->EnableWindow(false);
}


void CBarCodeProperties11::OnRadioArbGroup() 
{
	// TODO: Add your control notification handler code here
	nTextSpreadType= 1;
	m_ArbGroupRadio.SetCheck(1);
	m_FixGroupRadio.SetCheck(0);
	GetDlgItem(IDC_EDIT_FIX_GROUP)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_ARB_GROUP)->EnableWindow(true);
}
void CBarCodeProperties11::OnCbnSelchangeComboFieldDatabase()
{
	// TODO: 在此添加控件通知处理程序代码
	CString strName = "";
	int nIndex = m_DBFieldCmb.GetCurSel();

	if (nIndex == CB_ERR)
	{
		m_nSqlTypeFieldIndex = 0;
		return;
	}
	m_nSqlTypeFieldIndex = nIndex+1;
}

int CBarCodeProperties11::getInsetParam(CString sData)
{




	
	return 0;
}


void CBarCodeProperties11::OnBnClickedButtonQrcodeproperties()
{
	// TODO: 在此添加控件通知处理程序代码

	CQRcodeProperties dlg;

	//二维码设置参数
	dlg.nLevel = nQRLevel;
	dlg.nVersion = nQRVersion ;
	dlg.bAutoExtent =  bQRAutoExtent;
	dlg.nMaskingNo = nQRMaskingNo;
	dlg.m_fQRRotateAngle = fQRRotateAngle;


		//二维码设置参数
	if (dlg.DoModal()==IDOK)
	{
		nQRLevel = dlg.nLevel;
		nQRVersion =  dlg.nVersion - 1;
		bQRAutoExtent = dlg.bAutoExtent;
		nQRMaskingNo =  dlg.nMaskingNo;
		fQRRotateAngle = dlg.m_fQRRotateAngle;
	}

}

void CBarCodeProperties11::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	OnOK();
}


void CBarCodeProperties11::OnCbnSelchangeComboBarPensize()
{
	// TODO:  在此添加控件通知处理程序代码
	CString selStr;
	if (m_PENSIZEcmb)
	{
		m_PENSIZEcmb.GetLBText(m_PENSIZEcmb.GetCurSel(), selStr);

		double pensize = atof(selStr);

		m_BarPenSize = pensize / 0.254000;

		UpdateData(false);
	}

}

void CBarCodeProperties11::OnCbnSelchangeComboBarBurring()
{
	// TODO:  在此添加控件通知处理程序代码
	int nIndex = m_BURRINGcmb.GetCurSel();
	if (nIndex == CB_ERR)
	{
		nBarWidthReduction = 0;
		return;
	}
	nBarWidthReduction = nIndex + 1;
}
void CBarCodeProperties11::OnCbnSelchangeComboTimeFormat()
{
	int nIndex = m_timeFormatCmb.GetCurSel();
	if (nIndex == CB_ERR)
	{
		nTimeFormatIndex = 0;
		return;
	}
	nTimeFormatIndex = nIndex + 1;
}
void CBarCodeProperties11::OnInitTimeFormat(int nIndex)
{
	if (m_timeFormatCmb.GetCount() == 0)
	{
		m_timeFormatCmb.AddString("H:M:S");
		m_timeFormatCmb.AddString("HH:MM:SS");
		m_timeFormatCmb.AddString("YY-M-D");
		m_timeFormatCmb.AddString("YY-MM-DD");
		m_timeFormatCmb.AddString("YYYY-M-D");
		m_timeFormatCmb.AddString("YYYY-MM-DD");
		m_timeFormatCmb.AddString("DD-MM-YY");
		m_timeFormatCmb.AddString("MM-DD-YY");
		m_timeFormatCmb.AddString("MM-DD-YYYY");
		m_timeFormatCmb.AddString("YYYY.MM.DD");
		m_timeFormatCmb.AddString("DD.MM.YYYY");
		m_timeFormatCmb.AddString("MM.DD.YYYY");
		m_timeFormatCmb.AddString("YYYYMMDDHHMMSS");
		m_timeFormatCmb.AddString("YYYY-MM-DD HH:MM:SS");
	}
	if (nIndex<1)
	{
		nIndex = 13;
	}
	nTimeFormatIndex = nIndex;
	m_timeFormatCmb.SetCurSel(nIndex - 1);
}
void CBarCodeProperties11::OnInitFontCtrl()
{
	OnInitFontType();
	OnInitFontStyle();
	OnInitFontSize();
	int nIdex;
	CString strSelText;
	if (m_mapFont.Lookup(_T(TextFont.lfFaceName), nIdex))
	{
		m_listFontType.SetCurSel(nIdex);
		m_strFontType = TextFont.lfFaceName;
		CEdit* pFontType;
		pFontType = (CEdit*)GetDlgItem(IDC_EDIT_FONT_TYPE2);
		//赋值
		pFontType->SetWindowText(_T(m_strFontType));
	}
	if (TextFont.lfWeight == 700 && TextFont.lfItalic == TRUE)
	{
		strSelText = "粗体 斜体";
		if (m_mapFontStyle.Lookup(_T(strSelText), nIdex))
		{
			m_listFontStyle.SetCurSel(nIdex);
		}
	}
	else if (TextFont.lfWeight == 700 && TextFont.lfItalic == FALSE)
	{
		strSelText = "粗体";
		if (m_mapFontStyle.Lookup(_T(strSelText), nIdex))
		{
			m_listFontStyle.SetCurSel(nIdex);
		}
	}
	else if (TextFont.lfWeight == 400 && TextFont.lfItalic == TRUE)
	{
		strSelText = "斜体";
		if (m_mapFontStyle.Lookup(_T(strSelText), nIdex))
		{
			m_listFontStyle.SetCurSel(nIdex);
		}
	}
	else if (TextFont.lfWeight == 400 && TextFont.lfItalic == FALSE)
	{
		strSelText = "常规";
		if (m_mapFontStyle.Lookup(_T(strSelText), nIdex))
		{
			m_listFontStyle.SetCurSel(nIdex);
		}
	}
	m_strFontStyle = strSelText;
	CEdit* pFontStyle;
	pFontStyle = (CEdit*)GetDlgItem(IDC_EDIT_FONT_STYLE);
	//赋值
	pFontStyle->SetWindowText(_T(m_strFontStyle));

	if (m_mapFontSize.Lookup(TextFont.lfHeight, nIdex))
	{
		m_listFontSize.SetCurSel(nIdex);
		CString strTemp;
		if (m_mapFontHeight.Lookup(TextFont.lfHeight, strTemp))
		{
			m_strFontSize = strTemp;
		}
		CEdit* pFontSize;
		pFontSize = (CEdit*)GetDlgItem(IDC_EDIT_FONT_SIZE);
		//赋值
		pFontSize->SetWindowText(_T(m_strFontSize));
	}
	else
	{
		CString strTemp;
		if (m_mapFontHeight.Lookup(TextFont.lfHeight, strTemp))
		{
			m_strFontSize = strTemp;
		}
		else
		{
			float FontSize = fabs(TextFont.lfHeight*72.0 / GetDeviceCaps(::GetDC(NULL), LOGPIXELSY));
			strTemp.Format(_T("%.1f"), FontSize);

			m_strFontSize = strTemp;
		}
		CEdit* pFontSize;
		pFontSize = (CEdit*)GetDlgItem(IDC_EDIT_FONT_SIZE);
		//赋值
		pFontSize->SetWindowText(_T(m_strFontSize));
	}
	if (TextFont.lfUnderline == TRUE)
	{
		m_underLine.SetCheck(1);
	}
	else
	{
		m_underLine.SetCheck(0);
	}
	if (TextFont.lfStrikeOut == TRUE)
	{
		m_strikeOut.SetCheck(1);
	}
	else
	{
		m_strikeOut.SetCheck(0);
	}
	//CFont newFont;
	newFont.DeleteObject();
	newFont.CreateFontIndirect(&TextFont);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
}



int CALLBACK EnumFontFamProc11(LPENUMLOGFONT lpelf, LPNEWTEXTMETRIC lpntm, DWORD nFontType, long lparam)
{
	CBarCodeProperties11 *pBarCodeProperties11 = (CBarCodeProperties11*)lparam;//窗口句柄
	if (pBarCodeProperties11->m_font != (lpelf->elfLogFont.lfFaceName))
	{
		pBarCodeProperties11->m_font = (lpelf->elfLogFont.lfFaceName);
		int nIndex = pBarCodeProperties11->m_listFontType.AddString(pBarCodeProperties11->m_font);
		pBarCodeProperties11->m_mapFont.SetAt(pBarCodeProperties11->m_font, nIndex);
	}
	return 1;
}


void CBarCodeProperties11::OnInitFontType()
{

	LOGFONT lf;
	lf.lfCharSet = DEFAULT_CHARSET;
	strcpy(lf.lfFaceName, "");
	CClientDC dc(this);
	m_font = ""; //定义的全局变量
	::EnumFontFamiliesEx((HDC)dc, &lf,
		(FONTENUMPROC)EnumFontFamProc11, (LPARAM)this, 0);

}



void CBarCodeProperties11::OnInitFontStyle()
{
	int nIndex;
	nIndex = m_listFontStyle.AddString("常规");
	m_mapFontStyle.SetAt("常规", nIndex);
	nIndex = m_listFontStyle.AddString("斜体");
	m_mapFontStyle.SetAt("斜体", nIndex);
	nIndex = m_listFontStyle.AddString("粗体");
	m_mapFontStyle.SetAt("粗体", nIndex);
	nIndex = m_listFontStyle.AddString("粗体 斜体");
	m_mapFontStyle.SetAt("粗体 斜体", nIndex);
}

void CBarCodeProperties11::OnInitFontSize()
{
	int nIndex = 0;


	nIndex = m_listFontSize.InsertString(nIndex, "5");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "6");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "7");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "8");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "9");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "10");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "11");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "12");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "14");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "16");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "18");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "20");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "22");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "24");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "26");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "28");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "36");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "48");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "72");
	
}

void CBarCodeProperties11::OnLbnSelChangeFontType()
{
	//CFont newFont;
	newFont.DeleteObject();
	//newFont.CreatePointFont(160, "楷体");
	//m_staticFontTmp.SetFont(&newFont);

	int nSel = m_listFontType.GetCurSel();
	CString strSelText;
	m_listFontType.GetText(nSel, strSelText);
	strcpy(TextFont.lfFaceName, strSelText);

	//lf.lfHeight = -26;    //修改字体大小
	//lf.lfItalic = TRUE;        //倾斜
	//lf.lfWeight = 400;   //修改字体的粗细
	newFont.CreateFontIndirect(&TextFont);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
	m_strFontType = strSelText;
	CEdit* pBoxOne;
	pBoxOne = (CEdit*)GetDlgItem(IDC_EDIT_FONT_TYPE);
	//赋值
	pBoxOne->SetWindowText(_T(strSelText));
}
void CBarCodeProperties11::OnLbnSelChangeFontStyle()
{
	//CFont newFont;
	newFont.DeleteObject();
	int nSel = m_listFontStyle.GetCurSel();
	CString strSelText;
	m_listFontStyle.GetText(nSel, strSelText);
	if (strSelText == "粗体 斜体")
	{
		TextFont.lfWeight = 700;
		TextFont.lfItalic = TRUE;
	}
	else if (strSelText == "粗体")
	{
		TextFont.lfWeight = 700;
		TextFont.lfItalic = FALSE;
	}
	else if (strSelText == "斜体")
	{
		TextFont.lfWeight = 400;
		TextFont.lfItalic = TRUE;
	}
	else
	{
		TextFont.lfWeight = 400;
		TextFont.lfItalic = FALSE;
	}
	newFont.CreateFontIndirect(&TextFont);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
	m_strFontStyle = strSelText;
	CEdit* pBoxOne;
	pBoxOne = (CEdit*)GetDlgItem(IDC_EDIT_FONT_STYLE);
	//赋值
	pBoxOne->SetWindowText(_T(strSelText));
}
void CBarCodeProperties11::OnLbnSelChangeFontSize()
{
	//CFont newFont;
	newFont.DeleteObject();
	int nSel = m_listFontSize.GetCurSel();
	CString strSelText;
	m_listFontSize.GetText(nSel, strSelText);
	{
		int nFontSize = atof(strSelText) * 10;
		TextFont.lfHeight = -(int)(fabs(nFontSize*GetDeviceCaps(::GetDC(NULL), LOGPIXELSY) / 72.) / 10 + 0.5);
		newFont.CreateFontIndirect(&TextFont);//创建一个新的字体
		m_staticFontTmp.SetFont(&newFont);
		m_strFontSize = strSelText;


		m_mapFontSize.SetAt(TextFont.lfHeight, nSel);
	}

	//newFont.CreateFontIndirect(&TextFont);//创建一个新的字体
	//m_staticFontTmp.SetFont(&newFont);
	//m_strFontSize = strSelText;
	CEdit* pBoxOne;
	pBoxOne = (CEdit*)GetDlgItem(IDC_EDIT_FONT_SIZE);
	//赋值
	pBoxOne->SetWindowText(_T(strSelText));
}

void CBarCodeProperties11::OnBnClickedCheckStikeOut()
{
	//CFont newFont;
	newFont.DeleteObject();
	if (m_strikeOut.GetCheck())
	{
		TextFont.lfStrikeOut = TRUE;
	}
	else
	{
		TextFont.lfStrikeOut = FALSE;
	}
	newFont.CreateFontIndirect(&TextFont);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
}
void CBarCodeProperties11::OnBnClickedCheckUnderLine()
{
	//CFont newFont;
	newFont.DeleteObject();
	if (m_underLine.GetCheck())
	{
		TextFont.lfUnderline = TRUE;
	}
	else
	{
		TextFont.lfUnderline = FALSE;
	}
	newFont.CreateFontIndirect(&TextFont);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
}

void CBarCodeProperties11::OnEnChangeEditTxtFontSize()
{
	//CFont newFont;
	newFont.DeleteObject();
	HDC hDC;
	hDC = ::GetDC(NULL);
	CString str;
	GetDlgItem(IDC_EDIT_FONT_SIZE)->GetWindowTextA(str);

	int nIndex = m_listFontSize.FindStringExact(0, str);
	if (nIndex>0)
	{
		int nFontSize = atof(str) * 10;
		TextFont.lfHeight = -(int)(fabs(nFontSize*GetDeviceCaps(hDC, LOGPIXELSY) / 72.) / 10 + 0.5);
		newFont.CreateFontIndirect(&TextFont);//创建一个新的字体
		m_staticFontTmp.SetFont(&newFont);
		m_strFontSize = str;
		return;
	}
	int nFontSize = atof(str) * 10;
	TextFont.lfHeight = -(int)(fabs(nFontSize*GetDeviceCaps(hDC, LOGPIXELSY) / 72.) / 10 + 0.5);
	newFont.CreateFontIndirect(&TextFont);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
	m_strFontSize = str;

	m_mapFontHeight.SetAt(TextFont.lfHeight, m_strFontSize);

	::ReleaseDC(NULL, hDC);
}



void CBarCodeProperties11::OnBnClickedCheckDeburring()
{

	if (m_deburring.GetCheck())
	{
		isUseDeburring = true;
		GetDlgItem(IDC_COMBO_BAR_BURRING)->EnableWindow(true);
	}
	else
	{
		isUseDeburring = false;
		GetDlgItem(IDC_COMBO_BAR_BURRING)->EnableWindow(false);
	}

}


void CBarCodeProperties11::OnBnClickedYes()
{
	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);

	if (pTBarCode11 == NULL)
		return;

	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;


	//删除字段后的处理
	CStringArray strArray;
	SplitString(sInsertParam, '*', strArray);
	CString sInsertParamTemp = "";
	for (int n = 0; n <= nMaxFieldNum; n++)
	{
		CString sTmp = "";
		sTmp.Format("[字段%d]", n);
		if (m_EditText.Find(sTmp) != -1)
		{
			for (int m = 0; m < strArray.GetCount(); m++)
			{
				if (strArray[m].Find(sTmp) != -1)
				{
					sInsertParamTemp += strArray[m] + "*";
				}
			}
		}
	}
	sInsertParam = sInsertParamTemp;


	CString Tmp = m_EditText;
	for (int n = 0; n <= nMaxFieldNum; n++)
	{
		CString sTmp = "";
		sTmp.Format("[字段%d]", n);
		Tmp.Replace(sTmp, "");
	}

	pTBarCode11->SetData(Tmp);

	pTBarCode11->m_nBarPropertyType = nBarPropertyType;//类型，固定，计数器，数据库，时间
	pTBarCode11->sInsetParam = sInsertParam;
	pTBarCode11->startX = ((int)(m_PosX*pView->ScaleX / pView->Zoom + 0.005) * 1000) / 1000.0;
	pTBarCode11->startY = ((int)(m_PosY*pView->ScaleY / pView->Zoom + 0.005) * 1000) / 1000.0;

	pTBarCode11->nBitMapLenth = (int(m_BarWidth / pView->Zoom *pView->ScaleX));
	pTBarCode11->nBitMapHeight = m_BarHeight / pView->Zoom *pView->ScaleY;

	pTBarCode11->endX = pTBarCode11->startX + pTBarCode11->nBitMapLenth;
	pTBarCode11->endY = pTBarCode11->startY + pTBarCode11->nBitMapHeight;

	//if (pTBarCode11->GetCodeType() == 8)//二维码
	//	pTBarCode11->nBitMapHeight = m_BarHeight / pView->Zoom*pView->ScaleY;
	//else if (pTBarCode11->GetCodeType() == 9)//二维码
	//	pTBarCode11->nBitMapHeight = m_BarHeight / pView->Zoom*pView->ScaleY;
	//else if (pTBarCode11->GetCodeType() == 10)//二维码
	//	pTBarCode11->nBitMapHeight = m_BarHeight / pView->Zoom*pView->ScaleY;

	pTBarCode11->nPenWidth = m_BarPenSize;

	pTBarCode11->SetCodeType(m_nBarTypeIndex + 1);

	///////计数器
	pTBarCode11->nCounterStart = m_nStartNum;
	pTBarCode11->nCounterEnd = m_nEndNum;
	pTBarCode11->nCounterStep = m_nStep;

	/////数据库
	pTBarCode11->m_nSqlTypeFieldIndex = m_nSqlTypeFieldIndex;

	pTBarCode11->m_nFrameType = m_nFrameTypeIndex;

	pTBarCode11->m_QietZoonUnit = m_QietZoonUnit;
	pTBarCode11->m_nTop = m_nTop;  //上边框宽度值（pixel）
	pTBarCode11->m_nBottom = m_nBottom; ////下边框宽度值（pixel）
	pTBarCode11->m_nLeft = m_nLeft;  //上边框宽度值（pixel）
	pTBarCode11->m_nRight = m_nRight; ////下边框宽度值（pixel）

	pTBarCode11->m_nRotateTyp = m_nRotateTypeIndex;
	pTBarCode11->nTextAlineType = m_nAlineTypeIndex;

	pTBarCode11->isDefaultTextBarDis = isDefaultTextBarDis;
	pTBarCode11->nTextBarDis = m_nTextAndBarDis;
	if (pTBarCode11->isDefaultTextBarDis)
	{
		pTBarCode11->nTextBarDis = 0;
	}
	pTBarCode11->nTextGroupDis = m_nGroupDis;

	pTBarCode11->TextFont = TextFont;
	pTBarCode11->TextColor = TextColor;

	pTBarCode11->sFieldTypeString = m_sArbGroup;
	pTBarCode11->sFieldTypeDis = m_FixGroup;//分组间距

	pTBarCode11->nTextGroupType = nTextSpreadType;

	pTBarCode11->nStartPos = m_startPos;
	pTBarCode11->nCounts = m_counts;

	if (pTBarCode11->TextFont.lfHeight<0)
	{
		pTBarCode11->TextFont.lfHeight = -(int)(pTBarCode11->TextFont.lfHeight * pView->iLOGPIXELSY / 96);
	}
	TextFont.lfHeight;
	TextFont.lfWidth;
	TextFont.lfWeight;

	pTBarCode11->nQRLevel = nQRLevel;
	pTBarCode11->nQRVersion = nQRVersion;
	pTBarCode11->bQRAutoExtent = bQRAutoExtent;
	pTBarCode11->nQRMaskingNo = nQRMaskingNo;
	pTBarCode11->fQRRotateAngle = fQRRotateAngle;
	pTBarCode11->m_nTimeFormatIndex = nTimeFormatIndex;

	pTBarCode11->bReadable = bReadable;
	pTBarCode11->bAbove = bAbove;
	pTBarCode11->m_bMirror = m_bMirror;

	if (pTBarCode11->m_nBarPropertyType == 1)
	{
		CString strCountTmp = pView->GetCounterText(pTBarCode11->nCounterStart, pTBarCode11->nCounterEnd, pTBarCode11->nCounterStep, 1);
		pTBarCode11->SetData(strCountTmp);
		//if (strCountTmp.GetLength() > 0)
		//{
		//	pTBarCode11->nCounterStart = _ttol(_T(strCountTmp));
		//}
	}
	else if (pTBarCode11->m_nBarPropertyType == 2)
	{

		CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(0, pTBarCode11->m_nSqlTypeFieldIndex);
		if (pTBarCode11->nStartPos >= 0 && pTBarCode11->nStartPos<sPrint.GetLength() && pTBarCode11->nCounts>0 && pTBarCode11->nCounts <= sPrint.GetLength())
		{
			sPrint = sPrint.Mid(pTBarCode11->nStartPos, pTBarCode11->nCounts);
		}
		pTBarCode11->SetData(sPrint);
	}
	else if (pTBarCode11->m_nBarPropertyType == 3)
	{
		CString strTime = pView->GetNowTime(pTBarCode11->m_nTimeFormatIndex);
		strTime.Replace(':', '-');
		pTBarCode11->SetData(strTime);
	}

	pTBarCode11->bIsSelected = false;

	pTBarCode11->m_nFieldIndex = nFieldIndex;


	if (pTBarCode11->GetCodeType() == 1)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_EAN8);  //
	}
	else if (pTBarCode11->GetCodeType() == 2)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_EAN13);
	}
	else if (pTBarCode11->GetCodeType() == 3)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_EAN128);
	}
	else if (pTBarCode11->GetCodeType() == 4)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_3OF9);
	}
	else if (pTBarCode11->GetCodeType() == 5)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_Code128);
	}
	else if (pTBarCode11->GetCodeType() == 6)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_CodaBar2);
	}
	else if (pTBarCode11->GetCodeType() == 7)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_PDF417);
	}
	else if (pTBarCode11->GetCodeType() == 8)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_QRCode);
	}
	else if (pTBarCode11->GetCodeType() == 9)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_MAXICODE);
	}
	else if (pTBarCode11->GetCodeType() == 10)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_DataMatrix);
	}
	else if (pTBarCode11->GetCodeType() == 11)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_9OF3);
	}
	else if (pTBarCode11->GetCodeType() == 12)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_UPC12);
	}
	else if (pTBarCode11->GetCodeType() == 13)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_Code128A);
	}
	else if (pTBarCode11->GetCodeType() == 14)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_Code128B);
	}
	else if (pTBarCode11->GetCodeType() == 15)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_Code128C);
	}
	else if (pTBarCode11->GetCodeType() == 16)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_UPCE);
	}
	else if (pTBarCode11->GetCodeType() == 17)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_ITF14);
	}
	else if (pTBarCode11->GetCodeType() == 18)
	{
		BCSetBCType(pTBarCode11->GetBarCode(), eBC_2OF5IL);
	}

	BCSetRotation(pTBarCode11->GetBarCode(), (e_Degree)m_nRotateTypeIndex);

	pTBarCode11->bBarWidthReduction = isUseDeburring;
	pTBarCode11->nBarWidthReduction = nBarWidthReduction;

	pView->Invalidate(FALSE);
}


void CBarCodeProperties11::OnBnClickedButtonquietzoon()
{
	// TODO:  在此添加控件通知处理程序代码

	CQuietZoon dlg;
	dlg.m_nTop = m_nTop ;   
	dlg.m_nBottom = m_nBottom ;   
	dlg.m_nLeft = m_nLeft;  
	dlg.m_nRight = m_nRight;   
	dlg.m_QietZoonUnit= m_QietZoonUnit;

	if (dlg.DoModal() == IDOK)
	{
		m_QietZoonUnit = dlg.m_QietZoonUnit;
		m_nTop = dlg.m_nTop;          
		m_nBottom = dlg.m_nBottom;  
		m_nLeft = dlg.m_nLeft;
		m_nRight = dlg.m_nRight;
	}

}


HBRUSH CBarCodeProperties11::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
