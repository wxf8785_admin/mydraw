#if !defined(AFX_CUSTOMCOMBOX_H__2C12B551_3373_41BB_97B0_0BBA6D626982__INCLUDED_)
#define AFX_CUSTOMCOMBOX_H__2C12B551_3373_41BB_97B0_0BBA6D626982__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CustomComBox.h : header file
//



#define  CMBMAXITEM		200
/////////////////////////////////////////////////////////////////////////////
// CCustomComBox window

class CCustomComBox : public CComboBox
{
// Construction
public:
	CCustomComBox();

// Attributes
public:

	int nColorFalg[CMBMAXITEM];
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCustomComBox)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCustomComBox();
	void SetColor(int nRow,int nColor);
	void ClearColor();
	// Generated message map functions
protected:
	//{{AFX_MSG(CCustomComBox)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CUSTOMCOMBOX_H__2C12B551_3373_41BB_97B0_0BBA6D626982__INCLUDED_)
