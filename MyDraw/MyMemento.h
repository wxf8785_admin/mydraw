#pragma once
#include "Graph.h"
class MyMemento
{
public:
	virtual ~MyMemento(void);
protected:
	MyMemento(void);
	CArray<CGraph*,CGraph*> graphs;//存放备忘的内容，这里指的是备忘的图元
};
//cut
class CutMemento:public MyMemento
{
public:
	~CutMemento(){}
private:
	friend class CutCommand;
	CutMemento(){}
	CutMemento(CArray<CGraph*,CGraph*>& _graphs);
	void GetGraph(CArray<CGraph*,CGraph*>& _graphs);
private:
	CArray<CGraph*,CGraph*> graphs;//存放备忘的内容，这里指的是备忘的图元指针
};
//delete
class DeleteMemento:public MyMemento
{
public:
	~DeleteMemento(){}
private:
	friend class DeleteCommand;
	DeleteMemento();
	DeleteMemento(CArray<CGraph*,CGraph*>& _graphs);
	void GetGraph(CArray<CGraph*,CGraph*>& _graphs);
private:
	CArray<CGraph*,CGraph*> graphs;
};
//new
class NewMemento:public MyMemento
{
public:
	~NewMemento(){}
private:
	friend class NewCommand;
	NewMemento();
	NewMemento(CGraph* _graphs);
	CGraph* GetGraph();
private:
	CGraph* graphs;
};

//paste
class PasteMemento:public MyMemento
{

public:
	~PasteMemento(){}
private:
	friend class PasteCommand;
	PasteMemento();
	PasteMemento(CArray<CGraph*, CGraph*>& _graphs);
	void GetGraph(CArray<CGraph*, CGraph*>& _graphs);
private:
	CArray<CGraph*, CGraph*> graphs;

};
//move
class MoveMemento:public MyMemento
{
public:
	~MoveMemento(){}
private:
	friend class MoveCommand;//若不声明为友元类，不能通过指针访问该备忘录中私有的内容
	MoveMemento(CGraph* _graphs,CRect _OriRect);
	CGraph* GetGraph();
	CRect GetRect();
	void SetRect(CRect _OriRect);
private:
	CGraph* graphs;
	CRect OriRect;
};
