#pragma once
#include "afxwin.h"


// CQuietZoon 对话框

class CQuietZoon : public CDialogEx
{
	DECLARE_DYNAMIC(CQuietZoon)

public:
	CQuietZoon(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CQuietZoon();

// 对话框数据
	enum { IDD = IDD_DIALOG_quietZOON };

protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_comboxUnit;
	int m_nTop;
	int m_nBottom;
	virtual BOOL OnInitDialog();
	int m_nRight;
	int m_nLeft;
	int m_QietZoonUnit;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
