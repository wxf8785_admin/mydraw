// EllipseProperties.cpp : implementation file
//

#include "stdafx.h"
#include "MyDraw.h"
#include "EllipseProperties.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEllipseProperties dialog

CEllipseProperties::CEllipseProperties(CWnd* pParent /*=NULL*/)
	: CDialog(CEllipseProperties::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEllipseProperties)
	m_LineWidth = _T("");
	m_LineStyle = _T("");
	m_HatchStyle = _T("");
	m_BrushStyle = _T("");
	m_fTxtWidth = 0;
	m_fTxtHeight = 0;
	m_fTxtLeftPos = 0;
	m_fTxtTopPos = 0;
	bOnlyLine = false;
	//}}AFX_DATA_INIT
}

void CEllipseProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEllipseProperties)
	DDX_Control(pDX, IDC_BUTTON_BRUSHCOLOR, m_BtnBrushColor);
	DDX_Control(pDX, IDC_BUTTON_COLOR, m_BtnColor);
	DDX_CBString(pDX, IDC_LINEWIDTH, m_LineWidth);
	DDX_CBString(pDX, IDC_LINESTYLE, m_LineStyle);
	DDX_CBString(pDX, IDC_HATCHSTYLE, m_HatchStyle);
	DDX_CBString(pDX, IDC_BRUSHSTYLE, m_BrushStyle);
	DDX_Control(pDX, IDC_TAB_TEXT, m_tab);
	DDX_Text(pDX, IDC_EDIT_WIDTH, m_fTxtWidth);
	DDX_Text(pDX, IDC_EDIT_HEIGTH, m_fTxtHeight);
	DDX_Text(pDX, IDC_EDIT_LEFT, m_fTxtLeftPos);
	DDX_Text(pDX, IDC_EDIT_TOP, m_fTxtTopPos);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CEllipseProperties, CDialog)
	//{{AFX_MSG_MAP(CEllipseProperties)
	ON_BN_CLICKED(IDC_BUTTON_COLOR, OnButtonColor)
	ON_BN_CLICKED(IDC_BUTTON_BRUSHCOLOR, OnButtonBrushcolor)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CEllipseProperties::OnBnClickedOk)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_TEXT, &CEllipseProperties::OnTcnSelchangeTabText)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEllipseProperties message handlers

void CEllipseProperties::OnButtonColor() 
{
	// TODO: Add your control notification handler code here
	CColorDialog dlg(m_LineColor); 
	if (dlg.DoModal()==IDOK)
	{
		m_LineColor=dlg.GetColor();
		CString str_RGB;
		str_RGB.Format("%d:%d:%d",GetRValue(m_LineColor),
			GetGValue(m_LineColor),GetBValue(m_LineColor));
		this->m_BtnColor.SetWindowText(str_RGB);
	}
}

void CEllipseProperties::OnButtonBrushcolor() 
{
	// TODO: Add your control notification handler code here
	CColorDialog dlg(m_BrushColor); 
	if (dlg.DoModal()==IDOK)
	{
		m_BrushColor=dlg.GetColor();
		CString str_RGB;
		str_RGB.Format("%d:%d:%d",GetRValue(m_BrushColor),
			GetGValue(m_BrushColor),GetBValue(m_BrushColor));
		this->m_BtnBrushColor.SetWindowText(str_RGB);
	}
}

BOOL CEllipseProperties::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_tab.InsertItem(0, "图形", 0);
	m_tab.InsertItem(1, "尺寸与位置", 1);
	m_tab.SetCurSel(0);
	SetCtrlVisible(0);

	CString str_RGB;
	str_RGB.Format("%d:%d:%d",GetRValue(m_LineColor),
		GetGValue(m_LineColor),GetBValue(m_LineColor));
	this->m_BtnColor.SetWindowText(str_RGB);
	
	str_RGB.Format("%d:%d:%d",GetRValue(m_BrushColor),
		GetGValue(m_BrushColor),GetBValue(m_BrushColor));
	this->m_BtnBrushColor.SetWindowText(str_RGB);

	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子  

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CEllipseProperties::OnBnClickedOk()
{
	// TODO:  在此添加控件通知处理程序代码
	CDialog::OnOK();
}


void CEllipseProperties::OnTcnSelchangeTabText(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO:  在此添加控件通知处理程序代码
	int nIndex = m_tab.GetCurSel();
	SetCtrlVisible(nIndex);
	*pResult = 0;
}
void CEllipseProperties::SetCtrlVisible(int nView)
{
	if (nView == 0)
	{
		GetDlgItem(IDC_STATIC_LINE)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_LINEWIDTH)->ShowWindow(true);
		GetDlgItem(IDC_LINEWIDTH)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_WZ)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_LEFT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_LEFT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TOP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TOP)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_LEFT2)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_POSWIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_HEIGTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_HEIGTH)->ShowWindow(false);


		if (bOnlyLine)
		{

			GetDlgItem(IDC_STATIC_BRUSHSTYLE)->ShowWindow(false);
			GetDlgItem(IDC_STATIC_BRUSHCO)->ShowWindow(false);
			GetDlgItem(IDC_BRUSHSTYLE)->ShowWindow(false);
			GetDlgItem(IDC_BUTTON_BRUSHCOLOR)->ShowWindow(false);



			GetDlgItem(IDC_STATIC_LINETYPE)->ShowWindow(true);
			GetDlgItem(IDC_LINESTYLE)->ShowWindow(true);

		
		}
		else
		{
			GetDlgItem(IDC_STATIC_LINETYPE)->ShowWindow(true);
			GetDlgItem(IDC_LINESTYLE)->ShowWindow(true);


			GetDlgItem(IDC_STATIC_BRUSHSTYLE)->ShowWindow(true);
			GetDlgItem(IDC_STATIC_BRUSHCO)->ShowWindow(false);
			GetDlgItem(IDC_BRUSHSTYLE)->ShowWindow(true);
			GetDlgItem(IDC_BUTTON_BRUSHCOLOR)->ShowWindow(false);
		}
	}
	else
	{
		GetDlgItem(IDC_STATIC_LINE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_LINEWIDTH)->ShowWindow(false);
		GetDlgItem(IDC_LINEWIDTH)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_WZ)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_LEFT)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_LEFT)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_TOP)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TOP)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_LEFT2)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_POSWIDTH)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_WIDTH)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_HEIGTH)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_HEIGTH)->ShowWindow(true);


		if (bOnlyLine)
		{

			GetDlgItem(IDC_STATIC_BRUSHSTYLE)->ShowWindow(false);
			GetDlgItem(IDC_STATIC_BRUSHCO)->ShowWindow(false);
			GetDlgItem(IDC_BRUSHSTYLE)->ShowWindow(false);
			GetDlgItem(IDC_BUTTON_BRUSHCOLOR)->ShowWindow(false);



			GetDlgItem(IDC_STATIC_LINETYPE)->ShowWindow(false);
			GetDlgItem(IDC_LINESTYLE)->ShowWindow(false);

		}
		else
		{
			GetDlgItem(IDC_STATIC_BRUSHSTYLE)->ShowWindow(false);
			GetDlgItem(IDC_STATIC_BRUSHCO)->ShowWindow(false);
			GetDlgItem(IDC_BRUSHSTYLE)->ShowWindow(false);
			GetDlgItem(IDC_BUTTON_BRUSHCOLOR)->ShowWindow(false);


			GetDlgItem(IDC_STATIC_LINETYPE)->ShowWindow(false);
			GetDlgItem(IDC_LINESTYLE)->ShowWindow(false);

		}
		
	}
}




HBRUSH CEllipseProperties::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
