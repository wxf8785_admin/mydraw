#pragma once


// CElementArray 对话框

class CElementArray : public CDialog
{
	DECLARE_DYNAMIC(CElementArray)

public:
	CElementArray(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CElementArray();

// 对话框数据m_ChangeFieldNumStartCmb
	enum { IDD = IDD_DIALOG_ARRAY };

protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:

	int m_Horizontal;
	float m_fHorizontalDis;
	int m_Verital;
	float m_fVerticalDis;
	CComboBox m_ConstFieldNumCmb;
	CComboBox m_ChangeFieldNumStartCmb;
	CComboBox m_ChangeFieldNumEndCmb;
	CButton m_RadioConstFieldNum;
	CButton m_RadioChangeFieldNum;
	int nMaxFieldNum;

	int nType;
	int ConstFieldIndex;
	int m_ChangeFieldStart;
	int m_ChangeFieldEnd;
	int m_FX;

	void CElementArray::SetMaxFieldNum(int nMax);
	void CElementArray::SetCmbString();
	virtual BOOL OnInitDialog();

	afx_msg void OnCbnSelchangeComboConstFieldnum();
	afx_msg void OnCbnSelchangeComboChangeFieldStart();
	afx_msg void OnCbnSelchangeComboChangeFieldEnd();
	afx_msg void OnBnClickedRadioConstFieldnum();
	afx_msg void OnBnClickedRadioChangeFieldnum();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
