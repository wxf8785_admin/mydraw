// Text.h: interface for the CText class.
//
//////////////////////////////////////////////////////////////////////

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CText :public CGraph
{
    DECLARE_SERIAL(CText)
public:
	//int startX;
	//int startY;
	//int endX;
	//int endY;

	int BkMode;
	CString MyText;
	LOGFONT MyFont;
	COLORREF MyColor;

	int m_nFieldIndex;
	int m_iTxtFieldDis;
	int m_nTimeFormatIndex;

	CString m_strFieldDes;
	CString m_strRecordVal;
	CString m_strInitVal;

	CString     sInsetSqlParam;
	CString     m_sTxtFieldType;

	int nConstValueType;
	int nTxtRotateType;//旋转类型
	int nTxtRotateAngle;//旋转角度
	bool isMirrorImage;//是否为镜像；
	int  nTxtInlineType;//对齐方式
	bool isTxtUseStyleCtrl;//是否使用文本格式控制

	bool isUseAnyStyle;//任意模式
	bool isAntiDistrot;//防失真
	bool isUseDeburring;//是否修边
	int nDeburring_x;
	int nDeburring_y;

	int nStartPos;
	int nCounts;

	long m_lTxtCounterStartNum;
	long m_lTxtCounterEndNum;
	long m_lTxtCounterStep;

	bool isDelete0;

	CText();
	virtual ~CText();
	void Serialize(CArchive & ar);
	virtual CGraph* Clone();
	virtual void  InitPropList();
};