#include "afxwin.h"
#include "TBarCode11.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BarCodeProperties.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBarCodeProperties11 dialog

class CBarCodeProperties11 : public CDialog
{
	// Construction
public:
	CBarCodeProperties11(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
	//{{AFX_DATA(CBarCodeProperties11)
	enum { IDD = IDD_DIALOG_BARCODE };
	CTBarCode11 *pTBarCode11;
	CButton	m_DefaultTxtDis;
	CComboBox	m_AlineType;
	CCustomComBox	m_DBFieldCmb;
	CComboBox	m_FrameCmb;
	CComboBox	m_RotateAngle;
	CComboBox	m_BarTypeCmb;
	CTabCtrl	m_BarProptyTab;
	CCustomComBox	m_nBarFieldNumCmb;
	CButton	m_RadioDate;
	CButton	m_RadioCounter;
	CButton	m_RadoiText;
	CButton m_RadoiDataBase;
	CButton m_FixGroupRadio;
	CButton m_ArbGroupRadio;
	CString	m_EditText;
	long	m_nStartNum;
	long	m_nStep;
	long	m_nEndNum;
	float	m_PosX;
	float	m_PosY;
	float	m_BarPenSize;
	int		m_nFieldIndex;//是哪个字段
	int		m_nFieldDistance;
	float	m_BarWidth;
	float	m_BarHeight;
	CString	m_sArbGroup;
	CString	m_FixGroup;
	//	CString	m_GroupDis;
	int	m_nGroupDis;
	int m_nTextAndBarDis;//文本和条形码的距离
	//}}AFX_DATA
	CString m_timeFormat;
	int m_startPos;
	int m_counts;
	CComboBox m_timeFormatCmb;

	bool isUseDeburring;//是否修边
	int nBarWidthReduction;//修边级别

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBarCodeProperties11)
protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
	LOGFONT		TextFont;
	COLORREF	TextColor;

	int nMaxFieldNum;
	int nFieldIndex;
	int nTimeFormatIndex;

	int m_nBarTypeIndex;
	int m_nFrameTypeIndex;
	int         m_QietZoonUnit;//QietZoonUnit的单位
	int         m_nTop;        //QietZoonUnit上面的距离
	int         m_nLeft;       //QietZoonUnit左边面的距离
	int         m_nBottom;     //QietZoonUnit下面的距离
	int         m_nRight;      //QietZoonUnit右面的距离
	int m_nRotateTypeIndex;
	int m_nAlineTypeIndex;
	BOOL isDefaultTextBarDis;

	CString sInsertParam;

	int nTextSpreadType;//文字分组方式，0固定分组，1任意分组

	int nBarPropertyType;
	int m_nSqlTypeFieldIndex;


	//二维码设置参数
	int nQRLevel;
	int nQRVersion;
	BOOL bQRAutoExtent;
	int nQRMaskingNo;
	float fQRRotateAngle;
public:
	CListBox m_listFontType;
	CListBox m_listFontStyle;
	CListBox m_listFontSize;
	CStatic m_staticFontTmp;
	CString m_strFontType;
	CString m_strFontStyle;
	CString m_strFontSize;
	CMap<CString, LPCSTR, int, int> m_mapFont;
	CMap<CString, LPCSTR, int, int> m_mapFontStyle;
	CButton	m_underLine;
	CButton m_strikeOut;
	CButton m_deburring;
	CFont newFont;
	void CBarCodeProperties11::OnInitFontCtrl();
	CString m_font; //定义的全局变量
	void CBarCodeProperties11::OnInitFontType();
	void CBarCodeProperties11::OnInitFontStyle();
	void CBarCodeProperties11::OnInitFontSize();

	void CBarCodeProperties11::OnIninFieldNumCom(int nNum, int nIndex, int SqlFieldIndex);
	void CBarCodeProperties11::GetInitParam(int nMxFdNum, int nFieldCmbIndex);
	void CBarCodeProperties11::SetCtrlVisible(int nView);

	int CBarCodeProperties11::getInsetParam(CString sData);

	void CBarCodeProperties11::OnInitTimeFormat(int nIndex);
	// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBarCodeProperties11)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioContent();
	afx_msg void OnRadioConter();
	afx_msg void OnRadioDate();
	afx_msg void OnSelchangeComboFieldIndex();
	afx_msg void OnButtoBarFont();
	afx_msg void OnSelchangeTabBar(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRadioDatabase();
	afx_msg void OnRadioArbGroup();

	afx_msg void OnSelchangeComboBarType();

	afx_msg void OnSelchangeComboFrameType();
	afx_msg void OnSelchangeComboRotateType();
	afx_msg void OnSelchangeComboAlineType();

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnBnClickedCheckDefaultTextdis();
	afx_msg void OnBnClickedButtonInsert();
	afx_msg void OnBnClickedRadioFixGroup();

	afx_msg void OnCbnSelchangeComboFieldDatabase();
	afx_msg void OnBnClickedButtonQrcodeproperties();
	afx_msg void OnBnClickedOk();
	CComboBox m_PENSIZEcmb;
	afx_msg void OnCbnSelchangeComboBarPensize();
	afx_msg void OnCbnSelchangeComboTimeFormat();
	afx_msg void OnBnClickedYes();
	afx_msg void OnLbnSelChangeFontType();
	afx_msg void OnLbnSelChangeFontStyle();
	afx_msg void OnLbnSelChangeFontSize();
	afx_msg void OnBnClickedCheckUnderLine();
	afx_msg void OnBnClickedCheckStikeOut();
	afx_msg void OnBnClickedCheckDeburring();
	afx_msg void OnBnClickedButtonquietzoon();
	CComboBox m_BURRINGcmb;
	afx_msg void OnCbnSelchangeComboBarBurring();

	BOOL bReadable;
	BOOL bAbove;
	BOOL m_bMirror;

	afx_msg void OnEnChangeEditTxtFontSize();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

