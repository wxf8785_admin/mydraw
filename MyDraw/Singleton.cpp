#include "StdAfx.h"
#include "Singleton.h"
#include "MainFrm.h"
Singleton* Singleton::_instance = 0;

Singleton::Singleton(void)
{
	pGraph = NULL;
	//ѡ��
	m_rectTracker.m_nStyle=CRectTracker::resizeInside|CRectTracker::dottedLine;

	reOri=0;
}

Singleton::~Singleton(void)
{
	if (_instance!= NULL)
	{
		delete _instance;
		_instance = NULL;
	}
}

Singleton* Singleton::instance()
{
	if(_instance==0)
	{
		_instance=new Singleton();
	}
	return _instance;
}
