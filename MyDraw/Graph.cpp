// Graph.cpp : 实现文件
//

#include "StdAfx.h"
#include "MyDrawView.h"
#include "MainFrm.h"
#include "Graph.h"


//IMPLEMENT_SERIAL(CGraph,CObject,1)


CGraph::CGraph(void)
{
	nGraphics=0;
	startX = 41;
	startY = 41;
	endX = 140;
	endY = 140;
	bIsSelected = false;
	bIsLocked = false;
}

void CGraph::Serialize(CArchive& ar)
{
}

CGraph::~CGraph(void)
{
}

UINT CGraph::GetType()
{
	return nGraphics;
}
CRect CGraph::GetRect()
{

	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMyDrawView *pView = (CMyDrawView *)pFrame->GetActiveView();

	CPoint m_ptFrom = CPoint(pView->CoordZoomOut(startX, 0), pView->CoordZoomOut(startY, 1));
	CPoint m_ptTo = CPoint(pView->CoordZoomOut(endX, 0), pView->CoordZoomOut(endY, 1));

	CRect rect(m_ptFrom, m_ptTo);
	rect.NormalizeRect();//标准化，符合正常视觉
	return rect;
}



void CGraph::SetRect(CRect re)
{

	tracker.m_rect.SetRect(re.TopLeft().x, re.TopLeft().y, re.BottomRight().x, re.BottomRight().y);


	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMyDrawView *pView = (CMyDrawView *)pFrame->GetActiveView();
	this->startX = pView->CoordZoomOrgi(this->tracker.m_rect.left, 0);
	this->startY = pView->CoordZoomOrgi(this->tracker.m_rect.top, 1);
	this->endX = pView->CoordZoomOrgi(this->tracker.m_rect.right, 0);
	this->endY = pView->CoordZoomOrgi(this->tracker.m_rect.bottom, 1);

	//this->startX = re.TopLeft().x;
	//this->startY = re.TopLeft().y;
	//this->endX = re.BottomRight().x;
	//this->endY = re.BottomRight().y;
}


//


CGraph* CGraph::operator =(CGraph& p)
{
	this->nGraphics = p.nGraphics;
	this->startX = p.startX;
	this->startY = p.startY;
	this->endX = p.endX;
	this->endY = p.endY;

	this->tracker = p.tracker;

	//
	return &p;
}

