#include "afxwin.h"
#if !defined(AFX_FIRMWAREOPERDLG_H__C36D3148_849A_47CA_954D_BAF67FE4C80D__INCLUDED_)
#define AFX_FIRMWAREOPERDLG_H__C36D3148_849A_47CA_954D_BAF67FE4C80D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FirmWareOperDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFirmWareOperDlg dialog

class CFirmWareOperDlg : public CDialog
{
	// Construction
public:
	CFirmWareOperDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
	//{{AFX_DATA(CFirmWareOperDlg)
	enum { IDD = IDD_DIALOG_FIRMWAREOPER };
	// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFirmWareOperDlg)
protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFirmWareOperDlg)
	// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_nSMJETSelectCmb;
	CComboBox m_nSMJETSetIndexCmb;
	CTabCtrl	m_FixSetTab;
	CString	m_strFilePath;
	afx_msg void OnBnClickedButtonSetSmjet();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeTabFixSet(NMHDR* pNMHDR, LRESULT* pResult);
	void CFirmWareOperDlg::SetCtrlVisible(int nFlag);
	afx_msg void OnBnClickedButtonSelfile();
	CComboBox m_nSMJETSelectCmb2;
	afx_msg void OnBnClickedButtonStruct();
	afx_msg void OnBnClickedButtonversion();
	void CFirmWareOperDlg::UpdataCtrlData(int SMJET, CString str);
	CStatic VERSION_NUM;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIRMWAREOPERDLG_H__C36D3148_849A_47CA_954D_BAF67FE4C80D__INCLUDED_)
