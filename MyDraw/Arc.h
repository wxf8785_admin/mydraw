// Arc.h: interface for the CArc class.
//
//////////////////////////////////////////////////////////////////////

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CArc :public CGraph
{
	DECLARE_SERIAL(CArc)
public:
	//int startX;
	//int startY;
	//int endX;
	//int endY;
	int Direction;

	LOGPEN LinePen;
	CArc();
	virtual ~CArc();
	void Serialize(CArchive & ar);
	virtual CGraph* Clone();
	virtual void  InitPropList();

};
