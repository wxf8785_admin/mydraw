#include "afxcmn.h"
#include "afxwin.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TextProperties.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTextProperties dialog

class CTextProperties : public CDialog
{
// Construction
public:
	
	CTextProperties(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTextProperties)
	enum { IDD = IDD_DIALOG_TEXT };
	CText* pText;
	CString	m_Text;
	BOOL	m_BkMode;
	CTabCtrl m_TxtTab;
	float m_fTxtWidth;
	float m_fTxtHeight;
	float m_fTxtLeftPos;
	float m_fTxtTopPos;

	CButton	m_TxtConstRadio;
	CButton	m_TxtCounterRadio;
	CButton	m_TxtSQLFieldRadio;
	CButton	m_TxtDateRadio;

	CCustomComBox m_InsertSqlFieldCmb;
	int m_TxtLine;
	long m_lTxtCounterStartNum;
	long m_lTxtCounterEndNum;
	long m_lTxtCounterStep;
	CCustomComBox m_TxtContentSqlCmb;

	float m_fTxtRorateAngle;
	CComboBox m_TxtInlineTypeCmb;
	int m_iTxtFieldDis;
	CString m_sTxtFieldType;
	CButton m_TxtEstendStyleAnyChk;
	CButton m_TxtAntiDistrotChk;
	CButton m_TxtDeburringChk;
	CButton m_TxtImageChk;
	CButton m_isUseTxtFormChk;

	int m_EdtTxtFieldDis;

	int m_startPos;
	int m_counts;

	bool isDelete0;

	CComboBox m_timeFormatCmb;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTextProperties)
	protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

public:
	COLORREF m_TextColor;
	LOGFONT m_Font;


	int nMaxFieldNum;
	int nFieldIndex;
	int nTimeFormatIndex;
	CString sInsertParam;

	int nConstValueType;
	int nTxtRotateType;//旋转类型
	int nTxtRotateAngle;//旋转角度
	bool isMirrorImage;//是否为镜像；
	int  nTxtInlineType;//对齐方式
	bool isTxtUseStyleCtrl;//是否使用文本格式控制

	bool isUseAnyStyle;//任意模式
	bool isAntiDistrot;//防失真
	bool isUseDeburring;//是否修边
	int nDeburring_x;
	int nDeburring_y;

public:
	CListBox m_listFontType;
	CListBox m_listFontStyle;
	CListBox m_listFontSize;
	CStatic m_staticFontTmp;
	CString m_strFontType;
	CString m_strFontStyle;
	CString m_strFontSize;
	CMap<CString, LPCSTR, int, int> m_mapFont;
	CMap<CString, LPCSTR, int, int> m_mapFontStyle;
	CButton	m_underLine;
	CButton m_strikeOut;
	CButton m_isDelete0;

	CFont newFont;
	void CTextProperties::OnInitFontCtrl();
	CString m_font; //定义的全局变量
	void CTextProperties::OnInitFontType();
	void CTextProperties::OnInitFontStyle();
	void CTextProperties::OnInitFontSize();
public:
	void CTextProperties::SetCtrlVisible(int nView);
	void CTextProperties::SetRadioContentPropty();

	void CTextProperties::SetSqlFieldNumParam(int nMxFdNum,int nFieldCmbIndex);
	void CTextProperties::OnInitSqlFieldNumCmb(int nNum,int nIndex);
	void CTextProperties::OnInitCtrlData();
	void CTextProperties::OnInitTimeFormat(int nIndex);
protected:

	// Generated message map functions
	//{{AFX_MSG(CTextProperties)
	afx_msg void OnButtonFont();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:

	virtual BOOL OnInitDialog();
	afx_msg void OnTcnSelchangeTabText(NMHDR *pNMHDR, LRESULT *pResult);


	afx_msg void OnBnClickedRadioTxtConst();
	afx_msg void OnBnClickedRadioTxtCounter();
	afx_msg void OnBnClickedRadioTxtSqlField();
	afx_msg void OnBnClickedRadioTxtDate();

	afx_msg void OnBnClickedRadioRorate0();
	afx_msg void OnBnClickedRadioRorate90();
	afx_msg void OnBnClickedRadioRorate180();
	afx_msg void OnBnClickedRadioRorate270();
	afx_msg void OnBnClickedRadioRorateAny();
	afx_msg void OnBnClickedCheckTxtImage();
	afx_msg void OnCbnSelchangeComboTxtInlineType();
	afx_msg void OnCbnSelchangeComboTxtField();
	afx_msg void OnBnClickedCheckTxtForm();
	afx_msg void OnBnClickedCheckTxtAnyStyle();
	afx_msg void OnBnClickedCheckTxtAntidistrot();
	afx_msg void OnBnClickedCheckDeburring();

	afx_msg void OnBnClickedButtonInsertSqlField();
	
	
	
	afx_msg void OnEnChangeEditTxtRorateAny();
	
	afx_msg void OnBnClickedOk();

	afx_msg void OnCbnSelchangeComboTimeFormat();
	afx_msg void OnBnClickedYes();
	afx_msg void OnLbnSelChangeFontType();
	afx_msg void OnLbnSelChangeFontStyle();
	afx_msg void OnLbnSelChangeFontSize();
	afx_msg void OnBnClickedCheckUnderLine();
	afx_msg void OnBnClickedCheckStikeOut();

	afx_msg void OnEnChangeEditTxtFontSize();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedCheckisdelete0();

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
