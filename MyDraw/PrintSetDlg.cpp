// PrintSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MyDraw.h"
#include "PrintSetDlg.h"
#include "MyDrawView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintSetDlg dialog


CPrintSetDlg::CPrintSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrintSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPrintSetDlg)

	CMainFrame* pMain = (CMainFrame*)AfxGetMainWnd();
	m_strParamPath = pMain->GetWorkDir();
	m_nPaperLenth = ::GetPrivateProfileInt(_T("PrintSet"), _T("PAPER_LENTH"), 297, m_strParamPath);
	m_dHeibiao = ::GetPrivateProfileInt(_T("PrintSet"), _T("Heibiao_LENTH"), 41, m_strParamPath);
	m_dHeibiao = (int(m_dHeibiao + 0.5)) / 10.;
	m_nSimuPrintSpeed = 0;
	m_nSimlEyeLenth = 355;
	m_nTimesTrig = 0;
	//}}AFX_DATA_INIT
}


void CPrintSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintSetDlg)
	DDX_Control(pDX, IDC_CHECK_SIM_EYETRIG, m_SimeyeTrig);
	DDX_Control(pDX, IDC_COMBO_HIGHSPEED_PPIY, m_nHeigh_PPIY);
	DDX_Control(pDX, IDC_COMBO_HIGHSPEED_PPIX, m_nHeigh_PPIX);
	DDX_Control(pDX, IDC_COMBO_NORMAL_PPIX, m_nNorMal_PPIX);
	DDX_Control(pDX, IDC_COMBO_NORMAL_PPIY, m_nNorMal_PPIY);
	DDX_Text(pDX, IDC_EDIT_PAPER_LENTH, m_nPaperLenth);
	DDV_MinMaxInt(pDX, m_nPaperLenth, 1, 65535);
	DDX_Text(pDX, IDC_EDIT_SIMUATE_PRINT_SPEED, m_nSimuPrintSpeed);
	DDV_MinMaxInt(pDX, m_nSimuPrintSpeed, 0, 30000);
	DDX_Text(pDX, IDC_EDIT_SIMULATE_EYETRIG_LENTH, m_nSimlEyeLenth);
	DDV_MinMaxInt(pDX, m_nSimlEyeLenth, 0, 65535);
	DDX_Text(pDX, IDC_EDIT_TIMES_TRIG, m_nTimesTrig);
	DDV_MinMaxInt(pDX, m_nTimesTrig, 0, 65535);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_EDIT_PAPER_LENTH2, m_dHeibiao);
}


BEGIN_MESSAGE_MAP(CPrintSetDlg, CDialog)
	//{{AFX_MSG_MAP(CPrintSetDlg)
	ON_BN_CLICKED(IDC_CHECK_INK_LIMI, OnCheckInkLimi)
	ON_BN_CLICKED(IDC_RADIO_PPI_MODE_NORMAL, OnRadioPpiModeNormal)
	ON_BN_CLICKED(IDC_RADIO_PPI_MODE_HIGHSPEED, OnRadioPpiModeHighspeed)
	ON_BN_CLICKED(IDC_CHECK_SIM_EYETRIG, OnCheckSimEyetrig)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(IDC_COMBO_NORMAL_PPIX, &CPrintSetDlg::OnCbnSelchangeComboNormalPpix)
	ON_CBN_SELCHANGE(IDC_COMBO_HIGHSPEED_PPIX, &CPrintSetDlg::OnCbnSelchangeComboHighspeedPpix)
	ON_CBN_SELCHANGE(IDC_COMBO_NORMAL_PPIY, &CPrintSetDlg::OnCbnSelchangeComboNormalPpiy)
	ON_CBN_SELCHANGE(IDC_COMBO_HIGHSPEED_PPIY, &CPrintSetDlg::OnCbnSelchangeComboHighspeedPpiy)
	ON_BN_CLICKED(IDOK, &CPrintSetDlg::OnBnClickedOk)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintSetDlg message handlers
BOOL CPrintSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here
	m_nNorMal_PPIX.AddString(_T("600"));
	m_nNorMal_PPIX.AddString(_T("300"));
	//m_nNorMal_PPIX.SetCurSel(0);

	m_nNorMal_PPIY.AddString(_T("600"));
	m_nNorMal_PPIY.AddString(_T("300"));
	m_nNorMal_PPIY.AddString(_T("200"));
	m_nNorMal_PPIY.AddString(_T("150"));
	m_nNorMal_PPIY.AddString(_T("100"));
	m_nNorMal_PPIY.AddString(_T("75"));
	m_nNorMal_PPIY.SetCurSel(0);

	m_nHeigh_PPIX.AddString(_T("300"));
	//m_nHeigh_PPIX.SetCurSel(0);

	m_nHeigh_PPIY.AddString(_T("600"));
	m_nHeigh_PPIY.AddString(_T("300"));
	m_nHeigh_PPIY.AddString(_T("200"));
	m_nHeigh_PPIY.AddString(_T("150"));
	m_nHeigh_PPIY.AddString(_T("100"));
	m_nHeigh_PPIY.AddString(_T("75"));
	//m_nHeigh_PPIY.SetCurSel(0);

	//CButton* pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
	//pBtn->SetCheck(1);
	ReadInkBoxParamIni();

	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子  
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void CPrintSetDlg::OnCheckInkLimi() 
{
	// TODO: Add your control notification handler code here
	
}

void CPrintSetDlg::OnRadioPpiModeNormal() 
{
	// TODO: Add your control notification handler code here


	GetDlgItem(IDC_COMBO_NORMAL_PPIX)->EnableWindow(true);
	GetDlgItem(IDC_COMBO_NORMAL_PPIY)->EnableWindow(true);

	GetDlgItem(IDC_COMBO_HIGHSPEED_PPIX)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_HIGHSPEED_PPIY)->EnableWindow(false);

	int nIndex = 0;
	int nIndey = 0;
	CButton* pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
	if (pBtn->GetCheck())
	{
		nIndex = m_nNorMal_PPIX.GetCurSel();//当前选中的行。
		nIndey = m_nNorMal_PPIY.GetCurSel();//当前选中的行。
	}
	else
	{
		nIndex = m_nHeigh_PPIX.GetCurSel() + 1;
		nIndey = m_nHeigh_PPIY.GetCurSel();
	}



	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;
	if (pView)
	{
		pView->DPIX = nIndex;
		pView->DPIY = nIndey;
	}

}


void CPrintSetDlg::OnRadioPpiModeHighspeed()
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_COMBO_NORMAL_PPIX)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_NORMAL_PPIY)->EnableWindow(false);

	GetDlgItem(IDC_COMBO_HIGHSPEED_PPIX)->EnableWindow(true);
	GetDlgItem(IDC_COMBO_HIGHSPEED_PPIY)->EnableWindow(true);

	int nIndex = 0;
	int nIndey = 0;
	CButton* pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
	if (pBtn->GetCheck())
	{
		nIndex = m_nNorMal_PPIX.GetCurSel();//当前选中的行。
		nIndey = m_nNorMal_PPIY.GetCurSel();//当前选中的行。
	}
	else
	{
		nIndex = m_nHeigh_PPIX.GetCurSel() + 1;
		nIndey = m_nHeigh_PPIY.GetCurSel();
	}



	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;
	if (pView)
	{
		pView->DPIX = nIndex;
		pView->DPIY = nIndey;
	}

}



void CPrintSetDlg::OnCheckSimEyetrig() 
{
	// TODO: Add your control notification handler code here
	CButton* pBtn = (CButton*)GetDlgItem(IDC_CHECK_SIM_EYETRIG);
	int state = pBtn->GetCheck();
	if(state == 1)
	{
		GetDlgItem(IDC_EDIT_SIMULATE_EYETRIG_LENTH)->EnableWindow(true);
		GetDlgItem(IDC_EDIT_TIMES_TRIG)->EnableWindow(true);
	}
	else
	{
		GetDlgItem(IDC_EDIT_SIMULATE_EYETRIG_LENTH)->EnableWindow(false);
		GetDlgItem(IDC_EDIT_TIMES_TRIG)->EnableWindow(false);
	}
}

////分辨率X
int CPrintSetDlg::GetPPIX()
{
	int nIndex = 0;
	CButton* pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
	if(pBtn->GetCheck())
		nIndex = m_nNorMal_PPIX.GetCurSel();//当前选中的行。
	else
		nIndex = m_nHeigh_PPIX.GetCurSel() + 1;


	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;
	if (pView)
	{
		pView->DPIX = nIndex;
	}

	return nIndex;
}
////分辨率Y
int CPrintSetDlg::GetPPIY()
{
	int nIndex = 0;
	CButton* pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
	if(pBtn->GetCheck())
		nIndex = m_nNorMal_PPIY.GetCurSel();//当前选中的行。
	else
		nIndex = m_nHeigh_PPIY.GetCurSel();

	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;
	if (pView)
	{
		pView->DPIY = nIndex;
	}
	return nIndex;
}


/////是否模拟电眼
bool CPrintSetDlg::isSimEye()
{
	CButton* pBtn = (CButton*)GetDlgItem(IDC_CHECK_SIM_EYETRIG);
	int state = pBtn->GetCheck();
	if(state == 1)
		return 1;
	else
		return 0;
	return 0;
}
////
int CPrintSetDlg::getSimeyeDistance()
{
	CButton* pBtn = (CButton*)GetDlgItem(IDC_CHECK_SIM_EYETRIG);
	int state = pBtn->GetCheck();
	if(state == 1)
	{
		return m_nSimlEyeLenth;	
	}
	else
		return 0;

	return 0;
}

int CPrintSetDlg::GetPaperLenth()
{
	UpdateData(true);
	return m_nPaperLenth ;//-4
}


void CPrintSetDlg::OnCbnSelchangeComboNormalPpix()
{
	// TODO:  在此添加控件通知处理程序代码

	int nIndex = 0;
	CButton* pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
	if (pBtn->GetCheck())
		nIndex = m_nNorMal_PPIX.GetCurSel();//当前选中的行。
	else
		nIndex = m_nHeigh_PPIX.GetCurSel() + 1;


	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;

	pView->DPIX = nIndex;

}


void CPrintSetDlg::OnCbnSelchangeComboHighspeedPpix()
{
	// TODO:  在此添加控件通知处理程序代码
	int nIndex = 0;
	CButton* pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
	if (pBtn->GetCheck())
		nIndex = m_nNorMal_PPIX.GetCurSel();//当前选中的行。
	else
		nIndex = m_nHeigh_PPIX.GetCurSel() + 1;


	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;

	pView->DPIX = nIndex;
}


void CPrintSetDlg::OnCbnSelchangeComboNormalPpiy()
{
	// TODO:  在此添加控件通知处理程序代码
	int nIndex = 0;
	CButton* pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
	if (pBtn->GetCheck())
		nIndex = m_nNorMal_PPIY.GetCurSel();//当前选中的行。
	else
		nIndex = m_nHeigh_PPIY.GetCurSel();

	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;

	pView->DPIY = nIndex;
}


void CPrintSetDlg::OnCbnSelchangeComboHighspeedPpiy()
{
	// TODO:  在此添加控件通知处理程序代码

	int nIndex = 0;
	CButton* pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
	if (pBtn->GetCheck())
		nIndex = m_nNorMal_PPIY.GetCurSel();//当前选中的行。
	else
		nIndex = m_nHeigh_PPIY.GetCurSel();

	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;

	pView->DPIY = nIndex;
}


void CPrintSetDlg::OnBnClickedOk()
{
	UpdateData(true);
	
	int nIndex = 0;//水平方向的dpi，0:600,1:300,2:200,3:150,4:100,5:75
	CButton* pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
	if (pBtn->GetCheck())
		nIndex = m_nNorMal_PPIY.GetCurSel();//当前选中的行。
	else
		nIndex = m_nHeigh_PPIY.GetCurSel();
	if (nIndex == 0)
	{
		if (m_nPaperLenth > 1000)
		{
			AfxMessageBox(_T("水平600分辨率，页长不能大于1000mm"));
			return;
		}
	}
	else if (nIndex == 1)
	{
		if (m_nPaperLenth > 2000)
		{
			AfxMessageBox(_T("水平300分辨率，页长不能大于2000mm"));
			return;
		}
	}
	else if (nIndex == 2)
	{
		if (m_nPaperLenth > 3000)
		{
			AfxMessageBox(_T("水平200分辨率，页长不能大于3000mm"));
			return;
		}
	}
	else if (nIndex == 3)
	{
		if (m_nPaperLenth > 4000)
		{
			AfxMessageBox(_T("水平150分辨率，页长不能大于4000mm"));
			return;
		}
	}
	else if (nIndex == 4)
	{
		if (m_nPaperLenth > 6000)
		{
			AfxMessageBox(_T("水平100分辨率，页长不能大于6000mm"));
			return;
		}
	}
	else if (nIndex == 5)
	{
		if (m_nPaperLenth > 8000)
		{
			AfxMessageBox(_T("水平75分辨率，页长不能大于8000mm"));
			return;
		}
	}


	CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	pMainFrame->m_nPaperLenth = m_nPaperLenth;// -4

	// TODO:  在此添加控件通知处理程序代码
	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;
	pView->Invalidate(FALSE);

	SaveInkBoxParamIni();
	CDialog::OnOK();
}
void CPrintSetDlg::SaveInkBoxParamIni()
{
	CString strText;
	strText.Format("%d", m_nPaperLenth);
	::WritePrivateProfileString("PrintSet", "PAPER_LENTH", strText, m_strParamPath);
	strText.Format("%d",int( m_dHeibiao * 10));
	::WritePrivateProfileString("PrintSet", "Heibiao_LENTH", strText, m_strParamPath);
	int nCheck = m_SimeyeTrig.GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("PrintSet", "SIM_EYETRIG", strText, m_strParamPath);
	strText.Format("%d", m_nSimlEyeLenth);
	::WritePrivateProfileString("PrintSet", "EYETRIG_LENTH", strText, m_strParamPath);
	strText.Format("%d", m_nTimesTrig);
	::WritePrivateProfileString("PrintSet", "TIMES_TRIG", strText, m_strParamPath);
	CButton* pBtn = (CButton*)GetDlgItem(IDC_CHECK_INK_LIMI);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("PrintSet", "INK_LIMI", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("PrintSet", "MODE_NORMAL", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_HIGHSPEED);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("PrintSet", "MODE_HIGHSPEED", strText, m_strParamPath);
	int nIndex = m_nNorMal_PPIX.GetCurSel();
	strText.Format("%d", nIndex);
	::WritePrivateProfileString("PrintSet", "NORMAL_PPIX", strText, m_strParamPath);
	nIndex = m_nNorMal_PPIY.GetCurSel();
	strText.Format("%d", nIndex);
	::WritePrivateProfileString("PrintSet", "NORMAL_PPIY", strText, m_strParamPath);
	nIndex = m_nHeigh_PPIX.GetCurSel();
	strText.Format("%d", nIndex);
	::WritePrivateProfileString("PrintSet", "HIGHSPEED_PPIX", strText, m_strParamPath);
	nIndex = m_nHeigh_PPIY.GetCurSel();
	strText.Format("%d", nIndex);
	::WritePrivateProfileString("PrintSet", "HIGHSPEED_PPIY", strText, m_strParamPath); 
	strText.Format("%d", m_nSimuPrintSpeed);
	::WritePrivateProfileString("PrintSet", "PRINT_SPEED", strText, m_strParamPath);
}
void CPrintSetDlg::ReadInkBoxParamIni()
{
	CString sText;
	m_nPaperLenth = ::GetPrivateProfileInt(_T("PrintSet"), _T("PAPER_LENTH"), 297, m_strParamPath);
	m_dHeibiao = ::GetPrivateProfileInt(_T("PrintSet"), _T("Heibiao_LENTH"), 41, m_strParamPath);
	m_dHeibiao = (int(m_dHeibiao + 0.5)) / 10.;
	int nCheck = ::GetPrivateProfileInt(_T("PrintSet"), _T("SIM_EYETRIG"), 0, m_strParamPath);
	m_SimeyeTrig.SetCheck(nCheck);
	OnCheckSimEyetrig();
	m_nSimlEyeLenth = ::GetPrivateProfileInt(_T("PrintSet"), _T("EYETRIG_LENTH"), 355, m_strParamPath);
	m_nTimesTrig = ::GetPrivateProfileInt(_T("PrintSet"), _T("TIMES_TRIG"), 0, m_strParamPath);
	nCheck = ::GetPrivateProfileInt(_T("PrintSet"), _T("INK_LIMI"), 0, m_strParamPath);
	CButton* pBtn = (CButton*)GetDlgItem(IDC_CHECK_INK_LIMI);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("PrintSet"), _T("MODE_NORMAL"), 1, m_strParamPath);
	if (nCheck)
	{
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_NORMAL);
		pBtn->SetCheck(nCheck);
		//OnRadioPpiModeNormal();
	}
	nCheck = ::GetPrivateProfileInt(_T("PrintSet"), _T("MODE_HIGHSPEED"), 0, m_strParamPath);
	if (nCheck)
	{
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_PPI_MODE_HIGHSPEED);
		pBtn->SetCheck(nCheck);
		//OnRadioPpiModeHighspeed();
	}
	int nIndex = ::GetPrivateProfileInt(_T("PrintSet"), _T("NORMAL_PPIX"), 0, m_strParamPath);
	m_nNorMal_PPIX.SetCurSel(nIndex);
	nIndex = ::GetPrivateProfileInt(_T("PrintSet"), _T("NORMAL_PPIY"), 0, m_strParamPath);
	m_nNorMal_PPIY.SetCurSel(nIndex);
	nIndex = ::GetPrivateProfileInt(_T("PrintSet"), _T("HIGHSPEED_PPIX"), 0, m_strParamPath);
	m_nHeigh_PPIX.SetCurSel(nIndex);
	nIndex = ::GetPrivateProfileInt(_T("PrintSet"), _T("HIGHSPEED_PPIY"), 0, m_strParamPath);
	m_nHeigh_PPIY.SetCurSel(nIndex);
	m_nSimuPrintSpeed = ::GetPrivateProfileInt(_T("PrintSet"), _T("PRINT_SPEED"), 0, m_strParamPath);
}


HBRUSH CPrintSetDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
