#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LineProperties.h : header file
//
#include "Resource.h"
/////////////////////////////////////////////////////////////////////////////
// CLineProperties dialog

class CLineProperties : public CDialog
{
// Construction
public:
	COLORREF m_LineColor;
	CLineProperties(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLineProperties)
	enum { IDD = IDD_DIALOG_LINE };
	CButton	m_BtnColor;
	CString	m_LineWidth;
	CString	m_LineStyle;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLineProperties)
	protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CLineProperties)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonColor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
