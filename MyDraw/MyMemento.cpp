#include "StdAfx.h"
#include "MyMemento.h"
#include "Graph.h"

MyMemento::MyMemento(void)
{
}

MyMemento::~MyMemento(void)
{
}

CutMemento::CutMemento(CArray<CGraph*,CGraph*>& _graphs)
{
	this->graphs.Copy(_graphs);
}

 void CutMemento::GetGraph(CArray<CGraph*,CGraph*>& _graphs)
{
	CGraph* gp;
	for(int i=0;i<graphs.GetSize();i++)
	{
		gp=graphs.GetAt(i);
		_graphs.Add(gp);
	}
}
//DeleteMemento
 DeleteMemento::DeleteMemento()
 {
 }

 DeleteMemento::DeleteMemento(CArray<CGraph*,CGraph*> &_graphs)
 {
	 this->graphs.Copy(_graphs);
 }

 void DeleteMemento::GetGraph(CArray<CGraph*,CGraph*> &_graphs)
 {
	 CGraph* gp;
	 for(int i=0;i<graphs.GetSize();i++)
	 {
		 gp=graphs.GetAt(i);
		 _graphs.Add(gp);
	 }
 }

 //new
 NewMemento::NewMemento()
 {
	 this->graphs=NULL;
 }

 NewMemento::NewMemento(CGraph* _graphs)
 {
	this->graphs=_graphs;
 }

 CGraph* NewMemento::GetGraph()
 {
	 return graphs;
 }

 //paste
 PasteMemento::PasteMemento()
 {

 }

 PasteMemento::PasteMemento(CArray<CGraph*, CGraph*> &_graphs)
 {
	 this->graphs.Copy(_graphs);
 }

 void PasteMemento::GetGraph(CArray<CGraph*, CGraph*> &_graphs)
 {
	 CGraph* gp;
	 for (int i = 0; i<graphs.GetSize(); i++)
	 {
		 gp = graphs.GetAt(i);
		 _graphs.Add(gp);
	 }
 }
 //move

 MoveMemento::MoveMemento(CGraph *_graphs,CRect _OriRect)
 {
	 this->graphs = _graphs;
	 this->OriRect = _OriRect;
 }

 CGraph* MoveMemento::GetGraph()
 {
	 return graphs;
 }

 CRect MoveMemento::GetRect()
 {
	 return this->OriRect;
 }
 void MoveMemento::SetRect(CRect _OriRect)
 {
	 this->OriRect=_OriRect;
 }

