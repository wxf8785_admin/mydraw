/////////////////////////////////////////////////////////////////////////////////////////////////////////
////**************************************************************************************************
/*           版权所有(2004):  武汉豪迈电力自动化有限责任公司                   
			 开发时间：       2004年10月
////***********************************************************************************************///
#if !defined(AFX_SUBITEMEDIT_H__07604F3D_0EB0_4658_AEE3_7A2D0A434DA3__INCLUDED_)
#define AFX_SUBITEMEDIT_H__07604F3D_0EB0_4658_AEE3_7A2D0A434DA3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// subitemedit.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// CSubItemEdit window

class CSubItemEdit : public CEdit
{
// Construction
public:
	CSubItemEdit();
	CSubItemEdit(int iItem, int iSubItem, CString sInitText);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSubItemEdit)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetCheckStandard(float MaxValue, float MinValue, int DecimalDigits);
	virtual ~CSubItemEdit();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSubItemEdit)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnNcDestroy();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

private:
	int m_iItem;
	int m_iSubItem;
	CString m_sInitText;
	BOOL    m_bEsc;	 	// to indicate whether esc key was pressed

	float				m_fMax;//最大值
	float				m_fMin;//最小值
	int					m_nDecimal;//小数位数
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SUBITEMEDIT_H__07604F3D_0EB0_4658_AEE3_7A2D0A434DA3__INCLUDED_)
