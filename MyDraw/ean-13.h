#ifndef _ean_13__
#define _ean_13__


#define LONG_HEIGHT			12	 //ԭΪ8
#define LEFT_MARGE			9    //ԭ5
#define EAN13_FONT_SIZE		12	 //ԭΪ12	

enum EAN13CODE_ERROR
{
	EANCode_Error_Success = 0,
	EANCode_Error_InputData = 1,
};

#define		EANCODE_LENGTH			96		// ����EANCode��char�������鳤��
#define		EANCODE_SIDE_SIGN		"101"	// ���ߵľ����־
#define		EANCODE_MIDDLE_SIGN		"01010"	// �м�ľ����־

int ComputCheckCode(const int * inputNumber);
int TextToNumbers(int * numbers, const char * enCodeStr);
int PrintEAN13Code(char * codeBits, const int * numbers);


void EAN13DrawOneCode(HDC hdc, int l, int t, int w, int h, HPEN hPen, HBRUSH hBrush);

HRESULT draw_image(HDC hdc, const char * enCodeStr, int &left, int &top, int code_x, int code_y);

HRESULT draw_image_90(HDC hdc, const char * enCodeStr, int &left, int &top, int code_x, int code_y);

#endif // _ean_13__
