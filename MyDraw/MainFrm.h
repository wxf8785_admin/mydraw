// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////
#include "Afxtempl.h"
#include "ruler.h"
#include "PrintDlg.h"
#include "PrintSetDlg.h"
#include "LinkBoxOperDlg.h"
#include "HardWareSetDlg.h"
#include "FirmWareOperDlg.h"
#include "DBSettingDlg.h"
#include <vector>  
#include <map>

using namespace std;
#include <winsock.h>  
#define socklen_t   int  
#pragma comment(lib, "wsock32.lib")  

#pragma once
#include "FileView.h"
#include "ClassView.h"
#include "OutputWnd.h"
#include "PropertiesWnd.h"

#define WM_USER_MSG    2000

struct SocketRecvParam
{
	int nLenth;
	char sIP[50];
	char buff[1024];
};

struct SPRINTDATAITEM
{
	int nRow;
	int nCol;
	CString sData;
};
//////PC->ARM参数包(公共参数)///
struct STARTSTRUCT{
	//	short nCode;				//标识吗
	byte nCode;
	byte nLen;
	byte  bPPIX;				//X分辨率
	byte  bPPIY;				//Y分辨率
	byte  bInkBoxSelect;		//墨盒选择
	byte  bReserve;				//保留
	byte  bSimulateEysTrig;		//模拟电眼
	byte  bInkBoxDirect;		//墨盒方向
	byte  bInkBoxRow[4];			//左列，右列，双列打印
	short nPageLen;				//页长
	short  LenthSimulateEysTrig;	//模拟电眼距离
	short nDistancePenToEys[4];	//墨盒到电眼距离
	int  bEncodeerDistance;	//编码器单个脉冲对应距离
};

struct PRINTDATA{
	//    short nCode;
	byte nCode;
	byte bNeedAnswer;//是否需要回应
	short nPrinterIndex;
	int   nPaperIndex;
	short nPaperPackageNo;
	short  bCurPackageIndex;//
	short  nCurDataLen;//一个数据包点阵数
	short  nCurFill;   //填充
	int nPaperTotalData;//一页纸对应点阵数
	byte  bData[1000];
};

struct INKBOXPARAM{
	byte nCode;
	byte nLen;
	byte m_nPulseTime[4];//脉冲时间
	short m_nInkBoxTemp[4];//温度
	short m_nInkBoxVol[4];//电压
	byte m_nIsWarm[4];//是否加热
	byte m_nIsWash[4];//清洗
	byte m_nRenew[4];//换墨盒
	byte m_nInkBoxRow[4];//单列还是双列
};

struct FirmData{
	byte code; //本包为固件包6
	byte packtatal;//固件总包数
	byte pack_no;//本包编号
	byte reverse;//预留
	int total_len;//固件文件总大小（以字节为单位）
	int  total_byte;//1000个bin数据字节之和。
	byte  bData[1000];//1000个bin数据
};


typedef struct _info_head{

	int this_pack_index;    //本数据包编号
	int printer_index;      //打印头编号
	int page_index;         //打印页编号

	bool operator<(const struct _info_head & other) const
	{
		//Must be overloaded as public if this struct is being used as the KEY in map.
		if (this->this_pack_index < other.this_pack_index) return true;
		else if (this->this_pack_index > other.this_pack_index) return false;

		if (this->printer_index < other.printer_index) return true;
		else if (this->printer_index > other.printer_index) return false;

		if (this->page_index < other.page_index) return true;
			return false;
	}
}info_head;




// File Type.
typedef enum FileType
{
	ANSI = 0,
	UNICODE,
	UTF8,
}FILETYPE;



class CMainFrame : public CFrameWndEx
{

protected: // 仅从序列化创建
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)
	int m_nVersion;
	// 特性
public:
	SOCKET sockClient[6];
	SOCKADDR_IN addrSrv[6];

	static void *ListenThread(void *data);
	SOCKET ListenSocket;    // 等待接收数据的socket  
	sockaddr_in srv;        // 绑定地址 



public:
	void CMainFrame::OnInitUdpSock(CString sIP, int nPort, int SMJETIndex);
	void CMainFrame::OnUdpSendData(char* pBuff, int nLen, int nSMJETIndex);
	void CMainFrame::OnUdpClose(int nIndex);


	LRESULT OnDealRecvUdpMsg(WPARAM wParam, LPARAM lParam);

	void CMainFrame::OnEndTest();
	void CMainFrame::SendStartData(int nSMJETIndex);
	void CMainFrame::SendPrintData(int nPrinterIndex, int nPaperIndex, int bPackageIndex, int nPaperLenth, int nDataPackageSize, int nSMJETIndex, int nSendCount);
	void CMainFrame::SendKeepAlive(int nSMJETIndex);
	void CMainFrame::SendReadInkBoxParam(int nSMJETIndex);//读取墨盒参数
	void CMainFrame::SendWriteInkBoxParam(int nSMJETIndex);//写墨盒参数
	void CMainFrame::SendWashBoxParam(int nSMJETIndex);   //清洗墨盒

	void CMainFrame::OnDealRecvKeepAlive(char* pBuff, int nLen, int nIndex);

	void CMainFrame::OnDealRecvVersion(char* pBuff, int nLen, int nIndex);

	void CMainFrame::OnDealRecvDataFeedBack(char* pBuff, int nLen, int nSMJETIndex);

	void CMainFrame::OnDealRecvFirmWareFeedBack(char* pBuff, int nLen, int nSMJETIndex);

	void CMainFrame::OnDealRecvInkBoxParam(char* pBuff, int nLen, int nSMJETIndex);

	void CMainFrame::CancelPrint();//取消打印


	void CMainFrame::RequstInkBoxParam(int nSMJETIndex);//请求墨盒参数
	void CMainFrame::WriteInboxParam(int nSMJETIndex);	//写墨盒参数

	CString sDBPathName;
	void CMainFrame::ReadPrintData(CString sPathName);
	void CMainFrame::SendSetSMJETIndex(int OrgSMJETIndex, int SetSMJETIndex);


public:

	map<info_head, PRINTDATA*>m_PRINTDATA_info_head[6];
	CArray<SPRINTDATAITEM*, SPRINTDATAITEM*> m_PrintDataArray;

	byte m_nAckNum;		//1=数据验证成功，2=数据失败重发,3数据失败等待	
	short m_nPrintIndex[6];		//打印头编号
	int   m_nPaperIndex[6];		//打印页编号
	short m_nPackageIndex[6];	//本数据包编号
	int m_nPaperLenth;			//纸长
	short m_nPackageDataLenth;	//数据包中点阵数
	bool isPrint;              //打印中

	int nReSendCnt[6];         //一帧数据重发次数，超过10次认为网络异常；
	short m_nPrinterConut[6]; //选择的第几个喷头
	DWORD SpeedTime[6];       //用于计算每10页的时间
	bool hasData[6];          //对应6个arm是否有数据

	//int _nUpSMJET;//当前升级的打印头
public:
	void CMainFrame::SetFirmDataArray(FirmData tempArray);
	void CMainFrame::ClearFirmDataArray();
	void CMainFrame::SetSMJET(int nSmjet);
public:
	int CMainFrame::GetBitValue(int nValue, int index);
	int CMainFrame::bytesToInt(byte* src, int offset, int nLen);

	HACCEL m_hAccel;

	// Operations
public:
	CPrintDlg*			pPrintDlg;
	CPrintSetDlg*		pPrintSetDlg;
	CLinkBoxOperDlg*	pLinkBoxOperDlg;
	CHardWareSetDlg*    pHardWareSetDlg;
	CFirmWareOperDlg*   pFirmWareOperDlg;
	CDBSettingDlg*      pDBSettingDlg;
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL LoadFrame(UINT nIDResource, DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, CWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);
protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

	// Implementation

public:
	CString m_strPath;
	CString CMainFrame::GetWorkDir();
	void ToolBarComboBoxButtonSetText();
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 控件条嵌入成员
	CMFCMenuBar       m_wndMenuBar;
	CMFCToolBarImages m_UserImages;
	CFileView         m_wndFileView;
	CClassView        m_wndClassView;
	COutputWnd        m_wndOutput;

public:
	CPropertiesWnd    m_wndProperties;
	CMFCStatusBar     m_wndStatusBar;
	CMFCToolBar       m_wndToolBar;
	CMFCToolBar       m_myToolBar; //预览 
	CMFCToolBar       m_myToolBar2; //对齐 
	CComboBox         m_comboList; //下拉框 
	CEdit             m_barText;
	CRulerSplitterWnd m_Rulers;
	CMFCToolBarComboBoxButton  *m_comboZoom;
	// 生成的消息映射函数
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnViewCustomize();
	afx_msg LRESULT OnToolbarCreateNew(WPARAM wp, LPARAM lp);
	afx_msg void OnMenuitemPrint();
	afx_msg void OnMenuitemLine();
	afx_msg void OnMenuitemEllipse();
	afx_msg void OnMenuitemBar();
	afx_msg void OnMenuitemArc();
	afx_msg void OnMenuitemRect();
	afx_msg void OnMenuitemRect2();
	afx_msg void OnMenuitemText();
	afx_msg void OnMenuitemPrintset();
	afx_msg void OnMenuitemFirmwareset();
	afx_msg void OnMenuitemHardwareset();
	afx_msg void OnMenuitemLinkboxset();
	afx_msg void OnMenuitemPrentcodeerrorset();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnMenuitemSettingDb();
	afx_msg void OnMenuitemImportDb();
	afx_msg void OnUpdateMenuitemImportDb(CCmdUI* pCmdUI);
	afx_msg void OnMenuitemZoom(UINT id);
	afx_msg void OnUpdateMenuitemZoom(CCmdUI* pCmdUI);
	afx_msg void OnMenuitemPic();
	afx_msg void OnApplicationLook(UINT id);
	afx_msg void OnUpdateApplicationLook(CCmdUI* pCmdUI);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
	afx_msg void OnSelChangeClick();
	afx_msg void OnClickComboBox();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	BOOL CreateDockingWindows();
	void SetDockingWindowIcons(BOOL bHiColorIcons);

	// 刻度尺
public:
	void ShowRulers(BOOL bShow);
	void UpdateRulersInfo(stRULER_INFO stRulerInfo);

};


