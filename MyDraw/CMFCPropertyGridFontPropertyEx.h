#include "stdafx.h"

//派生一个子类
class CMFCPropertyGridFontPropertyEx :public CMFCPropertyGridFontProperty
{
public:
	CMFCPropertyGridFontPropertyEx(const CString& strName, LOGFONT& lf, DWORD dwFontDialogFlags = CF_EFFECTS | CF_SCREENFONTS,
		LPCTSTR lpszDescr = NULL, DWORD_PTR dwData = 0, COLORREF color = (COLORREF)-1) :CMFCPropertyGridFontProperty(strName, lf, dwFontDialogFlags, lpszDescr, dwData, color) {};

	// 自定义修改字体颜色方法
	void SetColor(COLORREF  clrColor)
	{
		m_Color = clrColor;
	}

	//自定义修改字体名称、高度、粗细和倾斜的方法
	void SetFont(LOGFONT lf)
	{

		CString strFontType = lf.lfFaceName;
		LONG ftHeight = lf.lfHeight;
		int ftWeight = lf.lfWeight;
		BYTE ftItalic = lf.lfItalic;


		_tcscpy(m_lf.lfFaceName, strFontType);

		//if (ftHeight < 0)
		//{
		//	// Map against logical height
		//	m_lf.lfHeight = -MulDiv(ftHeight, GetDeviceCaps(GetDC(NULL), LOGPIXELSY), 72);;
		//}
		//else
		//{
		//	// Map against cell height
		//	m_lf.lfHeight = MulDiv(ftHeight, GetDeviceCaps(GetDC(NULL), LOGPIXELSY), 72);;
		//}

		m_lf.lfHeight = ftHeight;
		m_lf.lfWeight = ftWeight;
		m_lf.lfItalic = ftItalic;
	}
};