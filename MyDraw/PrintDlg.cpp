// PrintDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MyDraw.h"
#include "MyDrawDoc.h"
#include "MyDrawView.h"
#include "MainFrm.h"
#include "PrintDlg.h"

#include "License.h"
#include "Logger.h"

using namespace LOGGER;
CLogger logger(LogLevel_Info, CLogger::GetAppPathA().append("log\\"));

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrintDlg dialog


CPrintDlg::CPrintDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrintDlg::IDD, pParent)
	, m_sCurentPage(_T(""))
	, m_sStartPage(_T(""))
	, m_sEndPage(_T(""))
{

	for(int i=0;i<6;i++)
	{
		nConnectStatus[i]=0;//连接状态
		nInkBoxStatus[i][0]=0;//墨盒状态
		nInkBoxStatus[i][1]=0;
		nInkBoxStatus[i][2]=0;
		nInkBoxStatus[i][3]=0;
		nPrintStatus[i]=0;//打印状态
		nTransSpeed[i] = 0.0f;//传输速度

		nPrintSpeed[i]=0;//打印速速
		nLeftTime[i]=0;//剩余时间
		PrintedNo[i]=0;//已打印业数
	}

	m_nRecordListCol = 1;
	isPrint = false;
	PPIX = 0;
}


void CPrintDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintDlg)
	DDX_Control(pDX, IDC_SUPPLY_LIST, m_SupplyList);
	DDX_Control(pDX, IDC_PRINT_LIST, m_PrintList);
	DDX_Control(pDX, IDC_RECORD_NUM_LIST, m_RecordNumList);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_STATIC_PRINT_NUM, PRINT_NUM);
	DDX_Control(pDX, IDC_STATIC_LEFT_TIME, LEFT_TIME);
	DDX_Control(pDX, IDC_STATIC_STATE, my_STATE);
	DDX_Control(pDX, IDC_STATIC_PRINTED_NEM, PRINTED_NUM);
	DDX_Control(pDX, IDC_STATIC_RESOLUTION, RESOLUTION);
	DDX_Text(pDX, IDC_EDIT1, m_sCurentPage);
	DDX_Text(pDX, IDC_EDIT2, m_sStartPage);
	DDX_Text(pDX, IDC_EDIT3, m_sEndPage);
	DDX_Control(pDX, IDC_CHECK1, bPrintCurrentPage);
}


BEGIN_MESSAGE_MAP(CPrintDlg, CDialog)
	//{{AFX_MSG_MAP(CPrintDlg)
	ON_BN_CLICKED(IDC_BUTTON_CANCEL_PRINT, OnButtonCancelPrint)
	ON_BN_CLICKED(IDC_BUTTON_START_PRINT, OnButtonStartPrint)
	ON_BN_CLICKED(IDC_BUTTON_FIND, OnButtonFind)
	ON_BN_CLICKED(IDC_BUTTON_NUM_ADD, OnButtonNumAdd)
	ON_BN_CLICKED(IDC_BUTTON_PRINT_NUM, OnButtonPrintNum)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_NUM, OnButtonClearNum)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_COL_NUM, OnButtonClearColNum)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_NOTIFY(LVN_GETDISPINFO, IDC_RECORD_NUM_LIST, OnGetdispinfoLSTAnalyse)
	ON_BN_CLICKED(IDC_CHECK1, &CPrintDlg::OnBnClickedCheck1)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPrintDlg message handlers
void CPrintDlg::SetSize()
{
	SetWindowPos(NULL,0,0,1000,600,SWP_NOMOVE); 
}

void CPrintDlg::InitDialog()
{

	m_SupplyList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,LVS_REPORT | LVS_SHOWSELALWAYS);
	ListView_SetExtendedListViewStyle(m_SupplyList.m_hWnd,
		LVS_EX_FULLROWSELECT |LVS_EX_GRIDLINES |/*LVS_EX_CHECKBOXES |*/LVS_EX_INFOTIP); 
	m_SupplyList.InsertColumn(0, _T("序号"),LVCFMT_CENTER, 60);
	m_SupplyList.InsertColumn(1, _T("名称"),LVCFMT_CENTER, 60);
	m_SupplyList.InsertColumn(2, _T("状态"),LVCFMT_CENTER, 60);	
	m_SupplyList.InsertColumn(3, _T("1号墨盒状态"),LVCFMT_CENTER, 100);
	m_SupplyList.InsertColumn(4, _T("2号墨盒状态"),LVCFMT_CENTER, 100);
	m_SupplyList.InsertColumn(5, _T("3号墨盒状态"),LVCFMT_CENTER, 100);
	m_SupplyList.InsertColumn(6, _T("4号墨盒状态"),LVCFMT_CENTER, 100);
	m_SupplyList.InsertColumn(7, _T("当前页"),LVCFMT_CENTER, 100);


	//int readCol1[] = {0,1,2,3,4,5,6,7}; // 装载值列项可以编辑
	//m_SupplyList.SetOnlyReadCol(readCol1,8);

	for(int i=0;i<6;i++)
	{
		nConnectStatus[i] =0;
		for(int j=0;j<4;j++)
			nInkBoxStatus[i][j] =0;	
	}

	CString sText = "";
	for (int i = 0; i<6; i++)
	{
		sText.Format("%d", i + 1);
		m_SupplyList.InsertItem(i, sText);

		sText = "SMJet" + sText;
		m_SupplyList.SetItemText(i, 1, sText);
		sText = "未连接";

		m_SupplyList.SetItemText(i, 2, sText);
		for (int j = 0; j<4; j++)
		{
			sText = "无效";
			m_SupplyList.SetItemText(i, 3 + j, sText);
		}
	}


	m_RecordNumList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON, LVS_REPORT | LVS_SHOWSELALWAYS);
	ListView_SetExtendedListViewStyle(m_RecordNumList.m_hWnd,
		LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES |/*LVS_EX_CHECKBOXES |*/LVS_EX_INFOTIP);
	m_RecordNumList.InsertColumn(0, _T("记录号"), LVCFMT_CENTER, 100);
	//	m_nRecordListCol=1;

	m_PrintList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON, LVS_REPORT | LVS_SHOWSELALWAYS);
	ListView_SetExtendedListViewStyle(m_PrintList.m_hWnd,
		LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES |/*LVS_EX_CHECKBOXES |*/LVS_EX_INFOTIP);
	m_PrintList.InsertColumn(0, _T("记录号"), LVCFMT_CENTER, 100);

	//UpdataCtrlData(0);

}

BOOL CPrintDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	InitDialog();

	my_STATE.SetBackColor(RGB(254, 0, 0));
	my_STATE.SetTextColor(RGB(254, 254, 254));	//tc:设置文本颜色

	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子  
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



/**
* 字符串
*/
bool CPrintDlg::findMostFrequentString(int &Rows, int &nCol)
{
	map<CString, int> m;

	if (nMaxColNum < 1)
		return false;

	for (int j = 1; j < 10000000 / nMaxColNum -1; j++)
	for (int i = 0; i < nMaxColNum; i++)
	{
		//CString sPrint = pFramePrepare->pPrintDlg->m_RecordNumList.GetItemText(nPageIndex - 1, i + 1);
		CString sPrint = sPrintData[(j - 1)*nMaxColNum + i + 1];

		if (sPrint.GetLength() > 0)
		{
			m[sPrint]++;
			if (m[sPrint] > 1)
			{
				Rows = j;
				nCol = i + 1;
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	return false;
}

bool LicenseCheck()
{
	bool license_check = false;
#ifdef DEBUG //不做校对
	license_check = false;
#else
	license_check = true;
#endif
	bool bRet = !license_check;
	if (!bRet)
	{
		CLicense sn1;
		if (sn1.Serialize("sn.txt", false))
		{
			logger.TraceInfo("TraceInfo");
			CLicense sn;	// used netcard serial first
			if (sn.Create() && sn.encrypt())
				bRet = (sn1 == sn);
			if (!bRet)
			{
				sn.SetMacAddrType(1); // used HD serial second
				if (sn.Create() && sn.encrypt())
					bRet = (sn1 == sn);
			}
		}
	}

	return bRet;
};


void CPrintDlg::OnButtonStartPrint() 
{
	// TODO: Add your control notification handler code here
	CLicense sn;
	sn.SetMacAddrType(1);
	//获取
	if (!sn.Create())
	{
		logger.TraceInfo("CLicense Create fail!\n");
		MessageBox(_T("CLicense Create fail!\n"));
		return;
	}
	//打印输出
	std::string strSn = sn.ToStringS();
	printf("strSn:%s\n", strSn.c_str());
	logger.TraceInfo("strSn:%s\n", strSn.c_str());
	//打印输出
	//向文件写入硬件信息
	sn.SerializeSource("sc.txt", true);

	//if (!LicenseCheck())
	//{
	//	MessageBox(_T("CLicense Check fail!\n"));
	//	return;
	//}

	UpdateData(true);

	GetDlgItem(IDC_BUTTON_START_PRINT)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_CANCEL_PRINT)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_FIND)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_NUM_ADD)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_PRINT_NUM)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_CLEAR_NUM)->EnableWindow(false);

	GetDlgItem(IDC_BUTTON_CLEAR_COL_NUM)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_PRINT_NUM)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_CLEAR_NUM)->EnableWindow(false);


	m_RecordNumList.SetItemState(-1, 0, LVIS_SELECTED);

	////打印之前查询是否有重复的字段
	//int Rows = 0;
	//int nCol = 0;
	//if (findMostFrequentString(Rows, nCol))
	//{
	//	CString tmp;
	//	tmp.Format(_T("第%d行，第%d列 重复出现！是否继续？"), Rows, nCol);
	//	if (AfxMessageBox(tmp, MB_YESNO) != IDYES)
	//	{
	//		GetDlgItem(IDC_BUTTON_START_PRINT)->EnableWindow(true);
	//		GetDlgItem(IDC_BUTTON_CANCEL_PRINT)->EnableWindow(true);
	//		GetDlgItem(IDC_BUTTON_FIND)->EnableWindow(true);
	//		GetDlgItem(IDC_BUTTON_NUM_ADD)->EnableWindow(true);
	//		GetDlgItem(IDC_BUTTON_PRINT_NUM)->EnableWindow(true);
	//		GetDlgItem(IDC_BUTTON_CLEAR_NUM)->EnableWindow(true);

	//		GetDlgItem(IDC_BUTTON_CLEAR_COL_NUM)->EnableWindow(true);
	//		GetDlgItem(IDC_BUTTON_PRINT_NUM)->EnableWindow(true);
	//		GetDlgItem(IDC_BUTTON_CLEAR_NUM)->EnableWindow(true);
	//		m_RecordNumList.SetFocus();
	//		return;
	//	}
	//}


	//for (int iCurSelectIndex = 0; iCurSelectIndex < m_RecordNumList.GetItemCount(); iCurSelectIndex++)
	//{
	//	if (m_RecordNumList.GetItemState(iCurSelectIndex, LVIS_SELECTED) == LVIS_SELECTED)
	//		m_RecordNumList.SetItemState(iCurSelectIndex, 0, -1);
	//}

	m_RecordNumList.SetFocus();

	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	int i = pFrame->pPrintSetDlg->GetPPIX();
	int j = pFrame->pPrintSetDlg->GetPPIY();

	int y[2] = { 600, 300 };
	int x[6] = { 600, 300, 200, 150, 100, 75 };

	CString strDPI;
	strDPI.Format(_T("DPI = %d * %d"), y[i], x[j]);
	RESOLUTION.SetWindowText(strDPI);
	PPIX = x[j];

	CMyDrawView *pView = (CMyDrawView *)pFrame->GetActiveView();
	if (pView->m_bIsEnablePrview)
	{
		pView->OnButtonPreview();
	}
	pView->ClearSelection();
	pView->m_selectLsit.RemoveAll();
	pView->Invalidate(FALSE);

	//int AllPaperNo = pView->nAllPaperNo;

	if (pFrame->pHardWareSetDlg->getSelectSMJET()>0)
	{	
		int nTmpIndex = pFrame->pHardWareSetDlg->getSMJETRealIndex(1);
		if (nTmpIndex<=0)
		{
			return;
		}
		int m_nPrintCnt = pFrame->pHardWareSetDlg->GetPrinterNo(nTmpIndex);
		if (m_nPrintCnt <= 0)
		{
			AfxMessageBox(_T("没有选择喷头,请选择后重新开始"));
			return;
		}

		if (pView->m_NextPageParam.size() > 0)
		{
			pView->m_NextPageParam.clear();
		}

		//开始打印前，刷新pview

		
		for (int i = 0; i < 6; i++)
		{
			pFrame->m_nPrintIndex[i] = 1;		//打印头编号
			pFrame->m_nPackageIndex[i] = 0;	//本数据包编号
			pFrame->m_nPrinterConut[i] = 1;

			pFrame->SpeedTime[i] = 0;
			pFrame->hasData[i] = false;
		}
		pView->isGetData = true;


		for (int i = 0; i < pView->m_nSMJET; i++)
		{
			if (PrintedNo[i] > 1)
				pFrame->m_nPaperIndex[i] = PrintedNo[i];

			//如果设置了打印起始页码
			if (!m_sStartPage.IsEmpty() && m_sStartPage.SpanIncluding(_T("0123456789")) == m_sStartPage)
			{
				if (atoi(m_sStartPage) > 0)
					pFrame->m_nPaperIndex[i] = atoi(m_sStartPage);
			}

			if (!m_sCurentPage.IsEmpty() && m_sCurentPage.SpanIncluding(_T("0123456789")) == m_sCurentPage)
			{
				if (bPrintCurrentPage.GetCheck() == 1)
				{
					if (atoi(m_sCurentPage) > 0)
						pFrame->m_nPaperIndex[i] = atoi(m_sCurentPage);
				}
			}
			

			for (int j = 0; j<4; j++)
				pView->SetRepPoint(i,j, pFrame->pHardWareSetDlg->GetBadPointNum(i,j));


			pView->Preparedata(pFrame->m_nPaperIndex[i],i,true);
			pView->NextPage(pFrame->m_nPaperIndex[i] + 1, i);
			//显示正在打印第几页
			RefreshCtrlPageData(pFrame->m_nPaperIndex[i]);
		}

		for (int i = 0; i < 6; i++)
		{
			if (pFrame->m_PRINTDATA_info_head[i].size() > 0)
			{
				map<info_head, PRINTDATA*>::iterator iter = pFrame->m_PRINTDATA_info_head[i].begin();
				while (iter != pFrame->m_PRINTDATA_info_head[i].end())
				{
					PRINTDATA*  pPRINTDATA = iter->second;
					if (pPRINTDATA != NULL)
					{
						delete pPRINTDATA;
						pPRINTDATA = NULL;
					}
					pFrame->m_PRINTDATA_info_head[i].erase(iter++);
				}
			}
		}
	
		
		for (int i = 1; i <= pFrame->pHardWareSetDlg->getSelectSMJET(); i++)
		{
			int nSMJETIndex = pFrame->pHardWareSetDlg->getSMJETRealIndex(i) - 1;
			//发数据
			pFrame->SendStartData(nSMJETIndex);

		}

	}
	else
	{
		AfxMessageBox("未选择打印设备");
	}

	m_RecordNumList.SetFocus();

	isPrint = true;
	pFrame->EnableWindow(FALSE);
	
}
void CPrintDlg::OnButtonCancelPrint() 
{
	// TODO: Add your control notification handler code here
	GetDlgItem(IDC_BUTTON_START_PRINT)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_CANCEL_PRINT)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_FIND)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_NUM_ADD)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_PRINT_NUM)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_CLEAR_NUM)->EnableWindow(true);

	GetDlgItem(IDC_BUTTON_CLEAR_COL_NUM)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_PRINT_NUM)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_CLEAR_NUM)->EnableWindow(true);


	//CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMainFrame* pFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	pFrame->isPrint = false;
	isPrint = false;
	//修改起始页
	if (!isPrint)
	{
		m_sStartPage.Format("%d", PrintedNo[0] + 1);
		GetDlgItem(IDC_EDIT2)->SetWindowText(m_sStartPage);
	}
	
	CMyDrawView *pView = (CMyDrawView *)pFrame->GetActiveView();
	if (pView != NULL)
	{
		pView->isGetData = false;
	}
	pFrame->OnEndTest();

	pFrame->EnableWindow(TRUE);

	//_CrtDumpMemoryLeaks();
}
int nRow =1;
int nCol = 1;
void CPrintDlg::OnButtonFind() 
{
	// TODO: Add your control notification handler code here

}

void CPrintDlg::OnButtonNumAdd() 
{
	// TODO: Add your control notification handler code here

	UpdateData(true);

	GetDlgItem(IDC_BUTTON_START_PRINT)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_CANCEL_PRINT)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_FIND)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_NUM_ADD)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_PRINT_NUM)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_CLEAR_NUM)->EnableWindow(false);

	GetDlgItem(IDC_BUTTON_CLEAR_COL_NUM)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_PRINT_NUM)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_CLEAR_NUM)->EnableWindow(false);


	m_RecordNumList.SetItemState(-1, 0, LVIS_SELECTED);
	m_RecordNumList.SetFocus();

	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	int i = pFrame->pPrintSetDlg->GetPPIX();
	int j = pFrame->pPrintSetDlg->GetPPIY();

	int y[2] = { 600, 300 };
	int x[6] = { 600, 300, 200, 150, 100, 75 };

	CString strDPI;
	strDPI.Format(_T("DPI = %d * %d"), y[i], x[j]);
	RESOLUTION.SetWindowText(strDPI);
	PPIX = x[j];

	CMyDrawView *pView = (CMyDrawView *)pFrame->GetActiveView();
	if (pView->m_bIsEnablePrview)
	{
		pView->OnButtonPreview();
	}
	pView->ClearSelection();
	pView->m_selectLsit.RemoveAll();
	pView->Invalidate(FALSE);

	//int AllPaperNo = pView->nAllPaperNo;

	if (pFrame->pHardWareSetDlg->getSelectSMJET()>0)
	{
		int nTmpIndex = pFrame->pHardWareSetDlg->getSMJETRealIndex(1);
		if (nTmpIndex <= 0)
		{
			return;
		}
		int m_nPrintCnt = pFrame->pHardWareSetDlg->GetPrinterNo(nTmpIndex);
		if (m_nPrintCnt <= 0)
		{
			AfxMessageBox(_T("没有选择喷头,请选择后重新开始"));
			return;
		}

		//开始打印前，刷新pview
		for (int i = 0; i < 6; i++)
		{
			pFrame->m_nPrintIndex[i] = 1;		//打印头编号
			pFrame->m_nPackageIndex[i] = 0;	//本数据包编号
			pFrame->m_nPrinterConut[i] = 1;

			pFrame->SpeedTime[i] = 0;
			pFrame->hasData[i] = false;
		}
		pView->isGetData = true;

		for (int i = 0; i < pView->m_nSMJET; i++)
		{
			for (int j = 0; j<4; j++)
				pView->SetRepPoint(i, j, pFrame->pHardWareSetDlg->GetBadPointNum(i, j));
			pView->Preparedata(1, i, true);
			//显示正在打印第几页
			RefreshCtrlPageData(pFrame->m_nPaperIndex[i]);
		}
	}
	else
	{
		AfxMessageBox("未选择打印设备");
	}

	m_RecordNumList.SetFocus();

	isPrint = true;
	pFrame->EnableWindow(FALSE);

	for (int j = 1; j < 100000000000; j++)
	{
		pView->NextPage(j + 1, 1);
		Sleep(20);
		//显示正在打印第几页
		RefreshCtrlPageData(j);

	}
	
}

void CPrintDlg::OnButtonPrintNum() 
{
	// TODO: Add your control notification handler code here
	
}

void CPrintDlg::OnButtonClearNum() 
{
	// TODO: Add your control notification handler code here
	
}

void CPrintDlg::OnButtonClearColNum() 
{
	// TODO: Add your control notification handler code here
	
}

void CPrintDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	
}




////默认控件数据
void CPrintDlg::DefaultCtrlData()
{
	CString str = "";
	str = "未连接";

	my_STATE.SetBackColor(RGB(254, 0, 0));
	my_STATE.SetCaption(str);
	my_STATE.Invalidate(FALSE);

	//m_SupplyList.DeleteAllItems();

	CString sText = "";
	for (int i = 0; i<6; i++)
	{
		sText.Format("%d", i + 1);
		//m_SupplyList.InsertItem(i, sText);

		sText = "SMJet" + sText;
		m_SupplyList.SetItemText(i, 1, sText);
		sText = "未连接";

		m_SupplyList.SetItemText(i, 2, sText);
		for (int j = 0; j<4; j++)
		{
			sText = "无效";
			m_SupplyList.SetItemText(i, 3 + j, sText);
		}
	}


}

////刷新控件数据
void CPrintDlg::UpdataCtrlData(int SMJET)
{
	CString str = "";
	int CMJETINDEX = SMJET;
	if(nConnectStatus[CMJETINDEX]==1)
	    str = "已连接";
	else 
		str = "未连接";

	//if(nPrintStatus[CMJETINDEX]==1)
	//{
	//	str +="--正在打印,待机";
	//}
	//else if(nPrintStatus[CMJETINDEX]==2)
	//{
	//	str +="--正在打印";
	//}
	//else if(nPrintStatus[CMJETINDEX]==3)
	//{
	//	str +="--待机";
	//}
	//else if(nPrintStatus[CMJETINDEX]==4)
	//{
	//	str +="--故障";
	//}
	//else if(nPrintStatus[CMJETINDEX]==5)
	//{
	//	str +="--电机过快";
	//}

	if (nConnectStatus[CMJETINDEX] == 1)
	{
		if (!isPrint)
		{
			str = "空闲";
		}
		else
		{
			str = "正在缓冲";

			//如果正在打印
			if (nTransSpeed[CMJETINDEX] > 0)
			{
				str = "正在打印";

				if (PPIX == 600 && nTransSpeed[CMJETINDEX] > 38)
				{
					str = "已超速";
				}
				if (PPIX == 300 && nTransSpeed[CMJETINDEX] > 76)
				{
					str = "已超速";
				}
				if (PPIX == 200 && nTransSpeed[CMJETINDEX] > 114)
				{
					str = "已超速";
				}
			}

		}
	}


	my_STATE.SetBackColor(RGB(0, 254, 0));
	my_STATE.SetCaption(str);
	my_STATE.Invalidate(FALSE);

	CStatic* pStatic = NULL;
	str.Format("%.1f",nTransSpeed[CMJETINDEX]);
	pStatic = (CStatic*)GetDlgItem(IDC_STATIC_TRANS_SPEED);//传输速度
	pStatic->SetWindowText(str);

	str.Format("%d", PrintedNo[CMJETINDEX]);//已打印页数
	PRINTED_NUM.SetWindowText(str);

	////修改起始页
	//if (!isPrint)
	//{
	//	m_sStartPage.Format("%d", PrintedNo[CMJETINDEX] + 1);
	//	GetDlgItem(IDC_EDIT2)->SetWindowText(m_sStartPage);

	//}


	str.Format("%d",nPrintSpeed[CMJETINDEX]);
	pStatic = (CStatic*)GetDlgItem(IDC_STATIC_PRINT_SPEED);//打印速度
	pStatic->SetWindowText(str);

	//m_SupplyList.DeleteAllItems();

	CString sText="";
	for(int i=0;i<6;i++)
	{
		sText.Format("%d",i+1);
		//m_SupplyList.InsertItem(i,sText);
		
		sText = "SMJet"+sText;
		if (m_SupplyList.GetItemText(i, 1) != sText)
			m_SupplyList.SetItemText(i, 1, sText);
		sText = "";
		if(nConnectStatus[i]==1)
			sText= "已连接";
		else
		{
			sText="未连接";
		}
		if (m_SupplyList.GetItemText(i, 2) != sText)
			m_SupplyList.SetItemText(i, 2, sText);
		for(int j=0;j<4;j++)
		{	

			if(nInkBoxStatus[i][j] ==0)
				sText="无效";
			else if(nInkBoxStatus[i][j] ==1)
			{
				sText="已连接";
			}
			if (m_SupplyList.GetItemText(i, 3 + j) != sText)
				m_SupplyList.SetItemText(i, 3 + j, sText);

		}

		if (i == CMJETINDEX)
		{
			str.Format("%d", PrintedNo[CMJETINDEX]);//已打印页数
			m_SupplyList.SetItemText(i, 7, str);
		}
	}


}

void CPrintDlg::CreateRecordList(int nCol)
{
	m_RecordNumList.DeleteAllItems();
	CHeaderCtrl* pHeaderCtrl = m_RecordNumList.GetHeaderCtrl();
	if (pHeaderCtrl != NULL)
	{
		int  nColumnCount = pHeaderCtrl->GetItemCount();
		for (int i=0; i<nColumnCount; i++)
		{
			m_RecordNumList.DeleteColumn(0);
		}
	}
	for(int i=0;i<nCol;i++)
	{
		if(i==0)
		{
			m_RecordNumList.InsertColumn(i, _T("记录号"),LVCFMT_CENTER, 100);
		}
//		else		
		{
			CString Tmp = "";
			Tmp.Format("%d",i+1);
			m_RecordNumList.InsertColumn(i+1, "字段"+Tmp,LVCFMT_CENTER, 100);	
		}
	}

}






////刷新当前打印页数
void CPrintDlg::RefreshCtrlPageData(int  n)
{
	CString str = "";
	str.Format("%d",n);

	//CStatic* pStatic = NULL;
	//pStatic = (CStatic*)GetDlgItem(IDC_STATIC_PRINT_NUM); //打印状态
	PRINT_NUM.SetWindowText(str);

	if (m_RecordNumList.GetItemCount() > 1)
	{

		m_RecordNumList.SetItemState(n - 1, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED | LVIS_DROPHILITED);//选择
		m_RecordNumList.SetItemState(n - 1, ~LVIS_SELECTED, LVIS_DROPHILITED);
		m_RecordNumList.SetSelectionMark(n - 1);
		m_RecordNumList.EnsureVisible(n - 1, TRUE);
		//m_RecordNumList.SetFocus();
	}
}


////刷新当前打印速度
void CPrintDlg::RefreshCtrlSpeedData(int  n)
{
	CString str = "";
	str.Format("%d ms/页", n / 10);

	//CStatic* pStatic = NULL;
	//pStatic = (CStatic*)GetDlgItem(IDC_STATIC_LEFT_TIME); //刷新当前打印速度
	LEFT_TIME.SetWindowText(str);

}



////刷新当前总页数
void CPrintDlg::RefreshCtrlAllPage(int  n)
{
	m_sEndPage.Format("%d", n);
	GetDlgItem(IDC_EDIT3)->SetWindowText(m_sEndPage);
}


void CPrintDlg::RefreshCtrlStartPage(int  n)
{
	m_sStartPage.Format("%d", n);
	GetDlgItem(IDC_EDIT2)->SetWindowText(m_sStartPage);
}

////刷新当前分辨率
void CPrintDlg::RefreshCtrlDPI(CString  strDPI)
{
	RESOLUTION.SetWindowText(strDPI);
}


void CPrintDlg::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	
	//取消打印
	OnButtonCancelPrint();

	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	if (!pFrame->IsWindowVisible())
		pFrame->ShowWindow(SW_SHOW);
	CDialog::OnClose();
}




void CPrintDlg::AddRows(const DWORD &dwRows)
{
	if (dwRows <= 0)
		return;

	m_RecordNumList.SetItemCount(dwRows);
	m_RecordNumList.RedrawItems(dwRows, dwRows);


}


void CPrintDlg::OnGetdispinfoLSTAnalyse(NMHDR* pNMHDR, LRESULT* pResult)
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	LV_ITEM* pItem = &(pDispInfo)->item;


	if (pItem == NULL)
		return;

	CString strTmp = "";
	int iItemIndx = pItem->iItem;
	if (pItem->mask & LVIF_TEXT) //字符串缓冲区有效
	{

		if(pItem->iSubItem == 0)
		{

			strTmp.Format("%d", iItemIndx+1);
			lstrcpy(pItem->pszText, strTmp);
			strTmp = "";
		}

		else if (pItem->iSubItem <= nMaxColNum)
		{
			strTmp.Format("%s", sPrintData[iItemIndx*nMaxColNum + pItem->iSubItem]);
			lstrcpy(pItem->pszText, strTmp);
			strTmp = "";
		}

	}

	*pResult = 0;
}


void CPrintDlg::OnBnClickedCheck1()
{
	// TODO:  在此添加控件通知处理程序代码

	if (bPrintCurrentPage.GetCheck())
	{
		GetDlgItem(IDC_EDIT1)->EnableWindow(true);
	}
	else
	{
		GetDlgItem(IDC_EDIT1)->EnableWindow(false);
	}
}


HBRUSH CPrintDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
