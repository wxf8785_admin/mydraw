#if !defined(AFX_PRINTDLG_H__E1CA3256_D5F3_4991_BE3C_35147C024F75__INCLUDED_)
#define AFX_PRINTDLG_H__E1CA3256_D5F3_4991_BE3C_35147C024F75__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintDlg.h : header file
//
#include "resource.h"
#include "stdafx.h"
#include "PrintDlg.h"
#include "Label.h"

#include "CommFiles/ListCtrlExt.h"
#include "afxwin.h"
/////////////////////////////////////////////////////////////////////////////
// CPrintDlg dialog

class CPrintDlg : public CDialog
{
// Construction
public:
	CPrintDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPrintDlg)
	enum { IDD = IDD_PRINT_DLG };
	CListCtrl	m_SupplyList;
	CListCtrl	m_PrintList;
	CListCtrl	    m_RecordNumList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintDlg)
	protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

public:


public:
	int m_nRecordListCol;

public:
	int nConnectStatus[6];		//连接状态
	int nInkBoxStatus[6][4];	//墨盒状态
	int nPrintStatus[6];		//打印状态
	float nTransSpeed[6];		//传输速度
	int nPrintSpeed[6];		//打印速速
	int nLeftTime[6];			//剩余时间
	int PrintedNo[6];			//已打印业数


	bool isPrint;                //打印状态
	int  PPIX;                   //分辨率


	int nMaxColNum;              //列数
	CString  sPrintData[15000000];  //存放数据库数据   sPrintData[(m_pSPRINTDATAITEM->nRow - 1) * MyView->nMaxColNum + m_pSPRINTDATAITEM->nCol] = m_pSPRINTDATAITEM->sData;

	bool findMostFrequentString(int &Rows, int &nCol);   //查重复
public:
	void SetSize();
	void InitDialog();
	////默认控件数据
	void CPrintDlg::DefaultCtrlData();
	void CPrintDlg::UpdataCtrlData(int nSMJET);
	////刷新当前发送页数
	void CPrintDlg::RefreshCtrlPageData(int  n);
	////刷新当前打印速度(每10页多少秒)
	void CPrintDlg::RefreshCtrlSpeedData(int  n);
	////刷新当前总页数
	void CPrintDlg::RefreshCtrlAllPage(int  n);
	////刷新当前开始页数
	void CPrintDlg::RefreshCtrlStartPage(int  n);
	////刷新当前分辨率
	void CPrintDlg::RefreshCtrlDPI(CString  strDPI);

	afx_msg void OnButtonCancelPrint();

	void CPrintDlg::CreateRecordList(int nCol);

	void AddRows(const DWORD &dwRows = 0);

protected:

	// Generated message map functions
	//{{AFX_MSG(CPrintDlg)
	virtual BOOL OnInitDialog();
	
	afx_msg void OnButtonStartPrint();
	afx_msg void OnButtonFind();
	afx_msg void OnButtonNumAdd();
	afx_msg void OnButtonPrintNum();
	afx_msg void OnButtonClearNum();
	afx_msg void OnButtonClearColNum();
	afx_msg void OnDestroy();

	afx_msg void OnGetdispinfoLSTAnalyse(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CStatic PRINT_NUM;
	CStatic LEFT_TIME;
	afx_msg void OnClose();
	CColorCStatic  my_STATE;
	//CStatic my_STATE;
	CStatic PRINTED_NUM;
	CStatic RESOLUTION;
	CString m_sCurentPage;
	CString m_sStartPage;
	CString m_sEndPage;
	CButton bPrintCurrentPage;
	afx_msg void OnBnClickedCheck1();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTDLG_H__E1CA3256_D5F3_4991_BE3C_35147C024F75__INCLUDED_)
