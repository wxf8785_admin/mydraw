﻿#if !defined(AFX_LABEL_H__A4EABEC5_2E8C_11D1_B79F_00805F9ECE10__INCLUDED_)
#define AFX_LABEL_H__A4EABEC5_2E8C_11D1_B79F_00805F9ECE10__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// Label.h : header file
//

#define	NM_LINKCLICK	WM_APP + 0x200

/////////////////////////////////////////////////////////////////////////////
// CLabel window

class CColorCStatic : public CStatic
{
	DECLARE_DYNAMIC(CColorCStatic)

public:
	CColorCStatic();
	virtual ~CColorCStatic();
protected:
	afx_msg void OnPaint();

protected:
	DECLARE_MESSAGE_MAP()
protected:
	CString  m_strCaption;
	COLORREF m_BackColor;
	COLORREF m_TextColor;
	// Operations
public:
	void SetTextColor(COLORREF TextColor);
	void SetBackColor(COLORREF BackColor );
	void SetCaption(CString strCaption );


public:
	//    afx_msg void OnRButtonDown(UINT nFlags, CPoint point);

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LABEL_H__A4EABEC5_2E8C_11D1_B79F_00805F9ECE10__INCLUDED_)
