// Arc.h: interface for the CArc class.
//
//////////////////////////////////////////////////////////////////////

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <math.h>

#pragma warning(disable:4996)


class CTBarCode11 :public CGraph
{
	DECLARE_SERIAL(CTBarCode11)
public:
	//int startX;
	//int startY;
	//int endX;
	//int endY;
	int Direction;

	CTBarCode11();
	virtual ~CTBarCode11();

	void Serialize(CArchive & ar);
	virtual CGraph* Clone();
	virtual void  InitPropList();


	t_BarCode* m_pBarCode;      // Pointer to general barcode settings
	CString m_sData;            // Buffer for barcode data string
	BOOL m_bAspectRatio;        // Flag: retain aspect ratio
	BOOL m_bResRatio;           // Flag: same horizontal and vertical resolution
	float m_dAspectRatio;       // Aspect ratio
	UINT m_nHeight;             // Height of barcode
	UINT m_nWidth;              // Width of barcode 
	UINT m_nHorzRes;            // Horizontal resolution
	UINT m_nVertRes;            // Vertical resolution
	int m_nUnit;                // Units (millimeters or pixels)
	COLORREF  m_colBackcolor;   // Backgound color


	int nCodeType;
	int   nBitMapLenth;//条码宽度
	int	  nBitMapHeight;//条码高度
	int	  nRotateAngle;//旋转角度
	int   nFrameType;//边框类型
	float   nPenWidth;//笔宽 
	LOGPEN LinePen;

	int m_nFieldIndex;//字段序号

	LOGFONT		TextFont;
	COLORREF	TextColor;
	int			nTextAlineType;//文本对齐方式
	int			m_nFrameType;//边框

	int         m_QietZoonUnit;//QietZoonUnit的单位
	int         m_nTop;        //QietZoonUnit上面的距离
	int         m_nLeft;       //QietZoonUnit左边面的距离
	int         m_nBottom;     //QietZoonUnit下面的距离
	int         m_nRight;      //QietZoonUnit右面的距离
	int			m_nRotateTyp;//旋转角度
	int			nTextGroupType;//分组类型
	int         nTextGroupDis;//字符分组间距
	int			nTextBarDis;//文本间距(文本和条形码间距)
	bool        isDefaultTextBarDis;


	CString     sInsetParam;
	CString     sFieldTypeString;
	CString     sFieldTypeDis;

	CString m_strFieldDes;
	CString m_strRecordVal;
	CString m_strInitVal;

	int m_nBarPropertyType;//固定、数据库、日期


	/////计数器
	int nCounterStart;
	int nCounterEnd;
	int nCounterStep;

	/////数据库
	int m_nSqlTypeFieldIndex;
	CString sSqlData;

	//////日期时间
	CString sBarDateTypeString;

	//二维码设置参数
	int nQRLevel;
	int nQRVersion;
	BOOL bQRAutoExtent;
	int nQRMaskingNo;
	float fQRRotateAngle;

	int nStartPos;
	int nCounts;
	int m_nTimeFormatIndex;


	//修边
	bool bBarWidthReduction;
	int nBarWidthReduction;

	//条码的文本是否显示，是否显示在上面
	BOOL bReadable; 
	BOOL bAbove;
	BOOL m_bMirror;

public:
	t_BarCode* GetBarCode() { return m_pBarCode; }
	CString GetData() { return m_sData; }
	BOOL GetbAspectRatio() { return m_bAspectRatio; }
	BOOL GetResRatio() { return m_bResRatio; }
	float GetdAspectRatio() { return m_dAspectRatio; }
	UINT GetHeight() { return m_nHeight; }
	UINT GetWidth() { return m_nWidth; }
	UINT GetHorzRes() { return m_nHorzRes; }
	UINT GetVertRes() { return m_nVertRes; }
	int GetUnit() { return m_nUnit; }
	int GetCodeType()  const{ return nCodeType; }

	void SetData(CString sData) { m_sData = sData; }
	void SetbAspectRatio(BOOL bAspectRatio) { m_bAspectRatio = bAspectRatio; }
	void SetResRatio(BOOL bResRatio) { m_bResRatio = bResRatio; }
	void SetdAspectRatio(float dAspectRatio) { m_dAspectRatio = dAspectRatio; }
	void SetHeight(UINT nHeight) { m_nHeight = nHeight; }
	void SetWidth(UINT nWidth) { m_nWidth = nWidth; }
	void SetHorzRes(UINT nHorzRes) { m_nHorzRes = nHorzRes; }
	void SetVertRes(UINT nVertRes) { m_nVertRes = nVertRes; }
	void SetUnit(int nUnit) { m_nUnit = nUnit; }
	void SetCodeType(int nType){ nCodeType = nType; }
};




