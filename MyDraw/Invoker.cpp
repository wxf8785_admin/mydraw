#include "StdAfx.h"
#include "Invoker.h"

CInvoker* CInvoker::_invoker=0;
CInvoker::CInvoker(void)
{
}

CInvoker::~CInvoker(void)
{
}
CInvoker* CInvoker::Invoker()
{
	if(_invoker==0)
	{
		_invoker=new CInvoker();
	}
	return _invoker;
}

void CInvoker::SetCommand(MyCommand *com)
{
	my_com=com;
}

void CInvoker::Execute()
{
	my_com->Execute();
	Command.Add(my_com);
}

void CInvoker::UnExecute()
{
	if(Command.GetSize()<=0)
		return;
	MyCommand* mycom;
	mycom=Command.GetAt((Command.GetSize()-1));
	mycom->UnExecute();
	UnCommand.Add(mycom);
	Command.RemoveAt((Command.GetSize()-1));
}

void CInvoker::ReExecute()
{
	if(UnCommand.GetSize()<=0)
		return;
	MyCommand* mycom;
	mycom=UnCommand.GetAt(UnCommand.GetSize()-1);
	mycom->ReExecute();
	Command.Add(mycom);
	UnCommand.RemoveAt(UnCommand.GetSize()-1);
}

