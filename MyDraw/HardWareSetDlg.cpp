// HardWareSetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MyDraw.h"
#include "MainFrm.h"
#include "MyDrawView.h"
#include "HardWareSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHardWareSetDlg dialog


CHardWareSetDlg::CHardWareSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CHardWareSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CHardWareSetDlg)
	m_EncoderSetting = 0.0846f;
	//}}AFX_DATA_INIT
}


void CHardWareSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHardWareSetDlg)
	DDX_Control(pDX, IDC_STATIC_RESERVE, m_PicReserve);
	DDX_Control(pDX, IDC_STATIC_FORWARD, m_PicForward);
	DDX_Control(pDX, IDC_LIST_PRINTER_POS, m_nPrinterPosList);
	DDX_Control(pDX, IDC_TAB_HARDWARE_SET, m_HardWareSetTab);
	DDX_Text(pDX, IDC_EDIT_ENCODER_SETTING, m_EncoderSetting);
	DDV_MinMaxFloat(pDX, m_EncoderSetting, 0.f, 9999999.f);

	//}}AFX_DATA_MAP
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHardWareSetDlg, CDialog)
	//{{AFX_MSG_MAP(CHardWareSetDlg)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_HARDWARE_SET, OnSelchangeTabHardwareSet)
	ON_BN_CLICKED(IDC_RADIO_RESERVE, OnRadioReserve)
	ON_MESSAGE(USER_KILL_FOCUS,onListEditKillFocus)
	ON_BN_CLICKED(IDC_RADIO_FORWARD, OnRadioForward)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST_PRINTER_POS, OnLvnEndlabeleditListPrinterPos)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CHardWareSetDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CHECK_SMJET1, &CHardWareSetDlg::OnBnClickedCheckSmjet1)
	ON_BN_CLICKED(IDC_CHECK_SMJET2, &CHardWareSetDlg::OnBnClickedCheckSmjet2)
	ON_BN_CLICKED(IDC_CHECK_SMJET3, &CHardWareSetDlg::OnBnClickedCheckSmjet3)
	ON_BN_CLICKED(IDC_CHECK_SMJET4, &CHardWareSetDlg::OnBnClickedCheckSmjet4)
	ON_BN_CLICKED(IDC_CHECK_SMJET5, &CHardWareSetDlg::OnBnClickedCheckSmjet5)
	ON_BN_CLICKED(IDC_CHECK_SMJET6, &CHardWareSetDlg::OnBnClickedCheckSmjet6)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHardWareSetDlg message handlers

BOOL CHardWareSetDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CMainFrame* pMain = (CMainFrame*)AfxGetMainWnd();
	m_strParamPath = pMain->GetWorkDir();
	// TODO: Add extra initialization here

	m_HardWareSetTab.InsertItem(0,"喷头选择",0);
	m_HardWareSetTab.InsertItem(1,"喷头位置",1);
	m_HardWareSetTab.InsertItem(2,"走纸方向",2);
	m_HardWareSetTab.InsertItem(3,"编码器",3);




	m_nPrinterPosList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,LVS_REPORT | LVS_SHOWSELALWAYS);
	ListView_SetExtendedListViewStyle(m_nPrinterPosList.m_hWnd,
		LVS_EX_FULLROWSELECT |LVS_EX_GRIDLINES |/*LVS_EX_CHECKBOXES |*/LVS_EX_INFOTIP); 
	m_nPrinterPosList.InsertColumn(0, _T("A-JET"),LVCFMT_CENTER, 100);

	m_nPrinterPosList.InsertColumn(1, _T("喷头编号"),LVCFMT_CENTER, 80);
	m_nPrinterPosList.InsertColumn(2, _T("到电眼距离(mm)"),LVCFMT_CENTER, 150);
	m_nPrinterPosList.InsertColumn(3, _T("相对偏移量(mm)"),LVCFMT_CENTER, 150);
	m_nPrinterPosList.InsertColumn(4, _T("重合点数"),LVCFMT_CENTER, 80);
	m_nPrinterPosList.InsertColumn(5, _T("墨盒方向"), LVCFMT_CENTER, 80);

	int nReadOnlyCol[2] ={0,1};
	m_nPrinterPosList.SetOnlyReadCol(nReadOnlyCol,2);

	int nSwRows[300] = { 0 }, nSwCols[300] = { 0 };
	CString text = "正|反|";
	
	for (int i = 0; i<4; i++)
	{
		CString sTmp = "", sText = "";
		sTmp.Format("A-JET%d", i + 1);
		sText.Format("喷头%d", i + 1);
		if (i == 0)
		{
			m_nPrinterPosList.InsertItem(i, sTmp);
		}
		else
		{
			m_nPrinterPosList.InsertItem(i, "");
		}
		m_nPrinterPosList.SetItemText(i, 1, sText);
		m_nPrinterPosList.SetItemText(i, 2, "10.00");
		m_nPrinterPosList.SetItemText(i, 3, "0.00");
		m_nPrinterPosList.SetItemText(i, 4, "0");
		m_nPrinterPosList.SetItemText(i, 5, "正");

	}

	nSwRows[5] = 5;
	m_nPrinterPosList.m_CmbItemsStr[5] = text;
	m_nPrinterPosList.SetCmbCol(nSwRows, 300);
	  
	/*CButton* pBtn = NULL;


	pBtn= (CButton*)GetDlgItem(IDC_CHECK_SMJET1);
	pBtn->SetCheck(1);
	pBtn= (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER1);
	pBtn->SetCheck(1);
	pBtn= (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER2);
	pBtn->SetCheck(1);
	pBtn= (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER3);
	pBtn->SetCheck(1);
	pBtn= (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER4);
	pBtn->SetCheck(1);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);
	pBtn->SetCheck(1);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_RESERVE);
	pBtn->SetCheck(0);


	pBtn = (CButton*)GetDlgItem(IDC_RADIO_CLOCKWISE);
	pBtn->SetCheck(1);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_COUNTERCLOCKWISE);
	pBtn->SetCheck(0);*/
	ReadInkBoxParamIni();

	SetCtrlVisible(0);
	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子  
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
void CHardWareSetDlg::SetCtrlVisible(int nFlag)
{

	if(nFlag==0)
	{
		GetDlgItem(IDC_STATIC_PRINTER_GROUP)->ShowWindow(true);
		for(int i = 0;i<6;i++)
		{
			GetDlgItem(IDC_CHECK_SMJET1+i)->ShowWindow(true);
		}

		for(int i=0;i<4;i++)
		{
			GetDlgItem(IDC_CHECK_SMJET1_PRINTER1+i)->ShowWindow(true);
			GetDlgItem(IDC_CHECK_SMJET2_PRINTER1+i)->ShowWindow(true);
			GetDlgItem(IDC_CHECK_SMJET3_PRINTER1+i)->ShowWindow(true);
			GetDlgItem(IDC_CHECK_SMJET4_PRINTER1+i)->ShowWindow(true);
			GetDlgItem(IDC_CHECK_SMJET5_PRINTER1+i)->ShowWindow(true);
			GetDlgItem(IDC_CHECK_SMJET6_PRINTER1+i)->ShowWindow(true);
		}
		
		m_nPrinterPosList.ShowWindow(false);


		CButton* pBtn = NULL;
		CStatic* pStatic = NULL;
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);
		pBtn->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_FORWARD);
		pStatic->ShowWindow(false);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_RESERVE);
		pBtn->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_RESERVE);
		pStatic->ShowWindow(false);


		pBtn = (CButton*)GetDlgItem(IDC_RADIO_CLOCKWISE);
		pBtn->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_CLOCKWISE);
		pStatic->ShowWindow(false);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_COUNTERCLOCKWISE);
		pBtn->ShowWindow(false);
		
		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_COUNTERCLOCKWISE);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_ENCODER);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_ENCODER_UNIT);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_zhengxiang);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_fanxiang);
		pStatic->ShowWindow(false);

		GetDlgItem(IDC_EDIT_ENCODER_SETTING)->ShowWindow(false);
		

	}
	else if(nFlag==1)
	{
		GetDlgItem(IDC_STATIC_PRINTER_GROUP)->ShowWindow(false);
		for(int i = 0;i<6;i++)
		{
			GetDlgItem(IDC_CHECK_SMJET1+i)->ShowWindow(false);
		}

		for(int i=0;i<4;i++)
		{
			GetDlgItem(IDC_CHECK_SMJET1_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET2_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET3_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET4_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET5_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET6_PRINTER1+i)->ShowWindow(false);
		}
		
		m_nPrinterPosList.ShowWindow(true);

		CButton* pBtn = NULL;
		CStatic* pStatic = NULL;
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);
		pBtn->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_FORWARD);
		pStatic->ShowWindow(false);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_RESERVE);
		pBtn->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_RESERVE);
		pStatic->ShowWindow(false);


		pBtn = (CButton*)GetDlgItem(IDC_RADIO_CLOCKWISE);
		pBtn->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_CLOCKWISE);
		pStatic->ShowWindow(false);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_COUNTERCLOCKWISE);
		pBtn->ShowWindow(false);
		
		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_COUNTERCLOCKWISE);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_ENCODER);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_ENCODER_UNIT);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_zhengxiang);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_fanxiang);
		pStatic->ShowWindow(false);

		GetDlgItem(IDC_EDIT_ENCODER_SETTING)->ShowWindow(false);
	}
	else if(nFlag==2)
	{
		GetDlgItem(IDC_STATIC_PRINTER_GROUP)->ShowWindow(false);
		for(int i = 0;i<6;i++)
		{
			GetDlgItem(IDC_CHECK_SMJET1+i)->ShowWindow(false);
		}

		for(int i=0;i<4;i++)
		{
			GetDlgItem(IDC_CHECK_SMJET1_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET2_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET3_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET4_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET5_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET6_PRINTER1+i)->ShowWindow(false);
		}
		
		m_nPrinterPosList.ShowWindow(false);

		CButton* pBtn = NULL;
		CStatic* pStatic = NULL;
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);
		pBtn->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_FORWARD);
		pStatic->ShowWindow(true);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_RESERVE);
		pBtn->ShowWindow(false);
		
		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_RESERVE);
		pStatic->ShowWindow(true);


		pBtn = (CButton*)GetDlgItem(IDC_RADIO_CLOCKWISE);
		pBtn->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_CLOCKWISE);
		pStatic->ShowWindow(false);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_COUNTERCLOCKWISE);
		pBtn->ShowWindow(false);
		
		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_COUNTERCLOCKWISE);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_ENCODER);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_ENCODER_UNIT);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_zhengxiang);
		pStatic->ShowWindow(true);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_fanxiang);
		pStatic->ShowWindow(true);

		GetDlgItem(IDC_EDIT_ENCODER_SETTING)->ShowWindow(false);
	}
	else if(nFlag==3)
	{
		GetDlgItem(IDC_STATIC_PRINTER_GROUP)->ShowWindow(false);
		for(int i = 0;i<6;i++)
		{
			GetDlgItem(IDC_CHECK_SMJET1+i)->ShowWindow(false);
		}

		for(int i=0;i<4;i++)
		{
			GetDlgItem(IDC_CHECK_SMJET1_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET2_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET3_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET4_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET5_PRINTER1+i)->ShowWindow(false);
			GetDlgItem(IDC_CHECK_SMJET6_PRINTER1+i)->ShowWindow(false);
		}
		
		m_nPrinterPosList.ShowWindow(false);

		CButton* pBtn = NULL;
		CStatic* pStatic = NULL;
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);
		pBtn->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_FORWARD);
		pStatic->ShowWindow(false);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_RESERVE);
		pBtn->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_RESERVE);
		pStatic->ShowWindow(false);


		pBtn = (CButton*)GetDlgItem(IDC_RADIO_CLOCKWISE);
		pBtn->ShowWindow(true);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_CLOCKWISE);
		pStatic->ShowWindow(true);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_COUNTERCLOCKWISE);
		pBtn->ShowWindow(true);
		
		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_COUNTERCLOCKWISE);
		pStatic->ShowWindow(true);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_ENCODER);
		pStatic->ShowWindow(true);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_ENCODER_UNIT);
		pStatic->ShowWindow(true);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_zhengxiang);
		pStatic->ShowWindow(false);

		pStatic = (CStatic*)GetDlgItem(IDC_STATIC_fanxiang);
		pStatic->ShowWindow(false);

		GetDlgItem(IDC_EDIT_ENCODER_SETTING)->ShowWindow(true);
	}

	Invalidate(FALSE);

}


void CHardWareSetDlg::OnSelchangeTabHardwareSet(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	SetCtrlVisible(m_HardWareSetTab.GetCurSel());
	*pResult = 0;
}


LRESULT CHardWareSetDlg::onListEditKillFocus(WPARAM wParam, LPARAM lParam)
{
	int nRow = m_nPrinterPosList.GetItemCount();
	int nCol = 5;
	int Item = wParam;
	int subItem = lParam;
	float fData = 0.0f;
	float fData2 = 0.0f;
	int   m_nData = 0;
	CString sText = "";
	if((subItem == 2 || subItem == 3 || subItem == 4) &&(Item<nRow))
	{
		switch(subItem)
		{
			case 2:
			if(Item == 0)
			{
				fData = (float)_tcstod(m_nPrinterPosList.GetItemText(Item,subItem),NULL);
				if(fData>600)
					fData = 600;
				sText.Format("%.2f",fData);
				m_nPrinterPosList.SetItemText(Item,subItem,sText);
				m_nPrinterPosList.SetItemText(Item,subItem+1,sText);

				fData = (float)_tcstod(m_nPrinterPosList.GetItemText(Item,subItem),NULL);//保留两位小数后
				fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(Item+1,subItem),NULL);
				sText.Format("%.2f",fData2-fData);
				m_nPrinterPosList.SetItemText(Item+1,subItem+1,sText);
			}
			else if(Item==(nRow-1) && (nRow >1))
			{
				fData = (float)_tcstod(m_nPrinterPosList.GetItemText(Item,subItem),NULL);
				if(fData>600)
					fData = 600;
				sText.Format("%.2f",fData);
				m_nPrinterPosList.SetItemText(Item,subItem,sText);

				fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(Item-1,subItem),NULL);
				sText.Format("%.2f",fData-fData2);
				m_nPrinterPosList.SetItemText(Item,subItem+1,sText);

			}
			else
			{
				fData = (float)_tcstod(m_nPrinterPosList.GetItemText(Item,subItem),NULL);
				if(fData>600)
					fData = 600;
				sText.Format("%.2f",fData);
				m_nPrinterPosList.SetItemText(Item,subItem,sText);

				fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(Item-1,subItem),NULL);
				sText.Format("%.2f",fData-fData2);
				m_nPrinterPosList.SetItemText(Item,subItem+1,sText);


				fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(Item+1,subItem),NULL);
				sText.Format("%.2f",fData2-fData);
				m_nPrinterPosList.SetItemText(Item+1,subItem+1,sText);

			}
			break;
			case 3:
				if(Item == 0)
				{
					fData = (float)_tcstod(m_nPrinterPosList.GetItemText(Item,subItem),NULL);
					if(fData>600)
						fData = 600;
					sText.Format("%.2f",fData);
					m_nPrinterPosList.SetItemText(Item,subItem,sText);
					m_nPrinterPosList.SetItemText(Item,subItem-1,sText);

									
					for(int i=Item;i<nRow-1;i++)
					{
						fData = (float)_tcstod(m_nPrinterPosList.GetItemText(i,subItem-1),NULL);//保留两位小数后

						fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(i+1,subItem),NULL);
						sText.Format("%.2f",fData2+fData);
						m_nPrinterPosList.SetItemText(i+1,subItem-1,sText);
					}
				
				}
				else if(Item==(nRow-1) && (nRow >1))
				{
					fData = (float)_tcstod(m_nPrinterPosList.GetItemText(Item,subItem),NULL);
					if(fData>600)
						fData = 600;
					sText.Format("%.2f",fData);
					m_nPrinterPosList.SetItemText(Item,subItem,sText);
					

					fData = (float)_tcstod(m_nPrinterPosList.GetItemText(Item,subItem),NULL);//保留两位小数后

					fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(Item-1,subItem-1),NULL);
					sText.Format("%.2f",fData2+fData);
					m_nPrinterPosList.SetItemText(Item+1,subItem-1,sText);
			
	

				}
				else
				{

					fData = (float)_tcstod(m_nPrinterPosList.GetItemText(Item,subItem),NULL);
					if(fData>600)
						fData = 600;
					sText.Format("%.2f",fData);
					m_nPrinterPosList.SetItemText(Item,subItem,sText);
					
					for(int i=Item;i<nRow-1;i++)
					{
						fData = (float)_tcstod(m_nPrinterPosList.GetItemText(i,subItem-1),NULL);//保留两位小数后

						fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(i+1,subItem),NULL);
						sText.Format("%.2f",fData2+fData);
						m_nPrinterPosList.SetItemText(i+1,subItem-1,sText);
					}

				}
				break;
			case 4:
					fData = (float)_tcstod(m_nPrinterPosList.GetItemText(Item,subItem),NULL);
					m_nData = (int)(fData);
					sText.Format("%d",m_nData);
					m_nPrinterPosList.SetItemText(Item,subItem,sText);
				break;
			default:
				break;
		}
	}
	return 1;
}

void CHardWareSetDlg::OnRadioReserve() 
{
	// TODO: Add your control notification handler code here
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);
	pBtn->SetCheck(0);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_RESERVE);
	pBtn->SetCheck(1);
}

void CHardWareSetDlg::OnRadioForward() 
{
	// TODO: Add your control notification handler code here
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);
	pBtn->SetCheck(1);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_RESERVE);
	pBtn->SetCheck(0);
}


//选择的第nIndex个喷头，是喷头几
int CHardWareSetDlg::GetPrinterIndex(int SMJETIndex,int nIndex)
{
	int nValue = 0;
	int nID = IDC_CHECK_SMJET1_PRINTER1;
	switch(SMJETIndex)
	{
	case 1:
		nID = IDC_CHECK_SMJET1_PRINTER1;
		break;
	case 2:
		nID = IDC_CHECK_SMJET2_PRINTER1;
		break;
	case 3:
		nID = IDC_CHECK_SMJET3_PRINTER1;
		break;
	case 4:
		nID = IDC_CHECK_SMJET4_PRINTER1;
		break;
	case 5:
		nID = IDC_CHECK_SMJET5_PRINTER1;
		break;
	case 6:
		nID = IDC_CHECK_SMJET6_PRINTER1;
		break;
	default:
		nID = IDC_CHECK_SMJET1_PRINTER1;
		break;

	}
	
	for(int i=0;i<4;i++)
	{
		if(((CButton*)GetDlgItem(nID+i))->GetCheck() ==1)
		{
			nValue++;
		}
		if(nIndex==nValue)
			return i+1;
	}
	return 0;
}

int CHardWareSetDlg::GetPrinterNo(int SMJETIndex)
{
	int nValue = 0;
	int nID = IDC_CHECK_SMJET1_PRINTER1;
	switch(SMJETIndex)
	{
	case 1:
		nID = IDC_CHECK_SMJET1_PRINTER1;
		break;
	case 2:
		nID = IDC_CHECK_SMJET2_PRINTER1;
		break;
	case 3:
		nID = IDC_CHECK_SMJET3_PRINTER1;
		break;
	case 4:
		nID = IDC_CHECK_SMJET4_PRINTER1;
		break;
	case 5:
		nID = IDC_CHECK_SMJET5_PRINTER1;
		break;
	case 6:
		nID = IDC_CHECK_SMJET6_PRINTER1;
		break;
	default:
		nID = IDC_CHECK_SMJET1_PRINTER1;
		break;

	}
	
	for(int i=0;i<4;i++)
	{
		if(((CButton*)GetDlgItem(nID+i))->GetCheck() ==1)
		{
			nValue++;
		}
	}

	return nValue;
}


int CHardWareSetDlg::GetSelectPrinter(int SMJETIndex)
{
	int nValue = 0;
	int nID = IDC_CHECK_SMJET1_PRINTER1;
	switch(SMJETIndex)
	{
	case 1:
		nID = IDC_CHECK_SMJET1_PRINTER1;
		break;
	case 2:
		nID = IDC_CHECK_SMJET2_PRINTER1;
		break;
	case 3:
		nID = IDC_CHECK_SMJET3_PRINTER1;
		break;
	case 4:
		nID = IDC_CHECK_SMJET4_PRINTER1;
		break;
	case 5:
		nID = IDC_CHECK_SMJET5_PRINTER1;
		break;
	case 6:
		nID = IDC_CHECK_SMJET6_PRINTER1;
		break;
	default:
		nID = IDC_CHECK_SMJET1_PRINTER1;
		break;

	}
	
	for(int i=0;i<4;i++)
	{
		int nData = 0;
		if(((CButton*)GetDlgItem(nID+i))->GetCheck() ==1)
		{
			nData =1;
			nData = (nData<<i);	
		}

		nValue = (nValue | nData);
	}

	return nValue;

}

int CHardWareSetDlg::getSelectSMJET()
{
	int nValue = 0;
	if(((CButton*)GetDlgItem(IDC_CHECK_SMJET1))->GetCheck() ==1)
		nValue++;

	if(((CButton*)GetDlgItem(IDC_CHECK_SMJET2))->GetCheck() ==1)
		nValue++;

	if(((CButton*)GetDlgItem(IDC_CHECK_SMJET3))->GetCheck() ==1)
		nValue++;

	if(((CButton*)GetDlgItem(IDC_CHECK_SMJET4))->GetCheck() ==1)
		nValue++;

	if(((CButton*)GetDlgItem(IDC_CHECK_SMJET5))->GetCheck() ==1)
		nValue++;

	if(((CButton*)GetDlgItem(IDC_CHECK_SMJET6))->GetCheck() ==1)
		nValue++;

	return nValue;

}
int CHardWareSetDlg::getSMJETRealIndex(int SMJETCntIndex)
{
	int nValue = 0;
	int nID = IDC_CHECK_SMJET1;
	for(int i=0;i<6;i++)
	{
		if(((CButton*)GetDlgItem(nID+i))->GetCheck() ==1)
		{
			nValue++;
		}
		if(SMJETCntIndex==nValue)
			return i+1;
	}

	return 0;

}


int CHardWareSetDlg::getSMJETRealCount(int SMJETCnt)
{
	int nValue = 0;
	int nID = IDC_CHECK_SMJET1;
	for (int i = 0; i <= SMJETCnt; i++)
	{
		if (((CButton*)GetDlgItem(nID + i))->GetCheck() == 1)
		{
			nValue++;
		}

	}

	return nValue;

}

float CHardWareSetDlg::GetInkBoxToEyeDis(int nSMJETIndex, int nIndex)
{
	float fData = 0;
	CString sText = "";
	sText.Format("A-JET%d", nSMJETIndex + 1);

	for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
	{
		CString sTmp = "";
		sTmp = m_nPrinterPosList.GetItemText(i, 0);
		if (sTmp == sText)
		{
			fData = (float)_tcstod(m_nPrinterPosList.GetItemText(i + nIndex, 2), NULL);
			break;
		}
	}

	return fData;

}

int CHardWareSetDlg::GetInkBoxDirect()
{
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);

	if(pBtn->GetCheck() ==1)
		return 1;
	else
		return 0;
	return 0;
}



int CHardWareSetDlg::GetInkBoxDirect(int nSMJETIndex)
{

	int nValue = 0;
	CString sText = "";
	sText.Format("A-JET%d", nSMJETIndex + 1);

	for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
	{
		CString sTmp = "";
		sTmp = m_nPrinterPosList.GetItemText(i, 0);
		if (sTmp == sText)
		{
			for (int j = 0; j < 4; j++)
			{
				int nData = 0;							
				if (m_nPrinterPosList.GetItemText(i + j, 5) == "正")
				{
					nData = 1;
					nData = (nData << j);
				}
				nValue = (nValue | nData);
			}		
			break;
		}
	}

	return nValue;
}





float CHardWareSetDlg::GetEncoderSpeed()
{
	UpdateData(true);
	return m_EncoderSetting;
}

///坏点数
int CHardWareSetDlg::GetBadPointNum(int nSMJET, int Index)
{
	CString  sText = "";
	sText.Format("A-JET%d", nSMJET + 1);
	for (int i = 0; i<m_nPrinterPosList.GetItemCount(); i++)
	{
		CString sTmp = "";	
		sTmp = m_nPrinterPosList.GetItemText(i, 0);
		if (sTmp == sText)
		{
			return  (int)_tcstod(m_nPrinterPosList.GetItemText(i + Index, 4), NULL);
		}
	}

	return  0;
}



void CHardWareSetDlg::OnBnClickedOk()
{
	// TODO:  在此添加控件通知处理程序代码


	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;
	pView->Invalidate(FALSE);

	SaveInkBoxParamIni();
	CDialog::OnOK();
}


void CHardWareSetDlg::OnBnClickedCheckSmjet1()
{
	// TODO:  在此添加控件通知处理程序代码
	int nID = IDC_CHECK_SMJET1_PRINTER1;
	if (((CButton*)GetDlgItem(IDC_CHECK_SMJET1))->GetCheck() == 1)
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(1);
		}

		//int n = m_nPrinterPosList.GetItemCount();
		int n = 0;

		for (int i = 0; i<4; i++)
		{
			CString sTmp = "", sText = "";
			sTmp.Format("A-JET%d", i + 1);
			sText.Format("喷头%d", i + 1);
			if (i == 0)
			{
				n = m_nPrinterPosList.InsertItem(i + n, sTmp);
			}
			else
			{
				m_nPrinterPosList.InsertItem(i + n, "");
			}

			m_nPrinterPosList.SetItemText(i + n, 1, sText);
			sTmp.Format("SMJET1_PRINTER%d_DDYJL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 2, sText);
			sTmp.Format("SMJET1_PRINTER%d_XDPYL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 3, sText);
			sTmp.Format("SMJET1_PRINTER%d_CHDS", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 4, sText);
			sTmp.Format("SMJET1_PRINTER%d_MHFX", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 5, sText);

		}
		float fPY = 0.0f;
		for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		{

			float fData = 0.0f;
			fData = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 3), NULL);
			fPY += fData;
			CString sText;
			sText.Format("%.2f", fPY);

			m_nPrinterPosList.SetItemText(i, 2, sText);
		}
	}
	else
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(0);
		}

		for (int i = 0; i<m_nPrinterPosList.GetItemCount(); i++)
		{
			CString sTmp = "";
			sTmp = m_nPrinterPosList.GetItemText(i, 0);
			if (sTmp == "A-JET1")
			{
				for (int j = i + 3; j >= i; j--)
				{
					m_nPrinterPosList.DeleteItem(j);
				}
			}
		}

		//float fPY = 0.0f;
		//for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		//{

		//	float fData1 = 0.0f;
		//	if (i > 0)
		//		fData1 = (float)_tcstod(m_nPrinterPosList.GetItemText(i - 1, 2), NULL);

		//	float fData2 = 0.0f;
		//	fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 2), NULL);
		//	fPY = fData2 - fData1;
		//	CString sText;
		//	sText.Format("%.2f", fPY);

		//	m_nPrinterPosList.SetItemText(i, 3, sText);
		//}
	}
}


void CHardWareSetDlg::OnBnClickedCheckSmjet2()
{
	// TODO:  在此添加控件通知处理程序代码
	int nID = IDC_CHECK_SMJET2_PRINTER1;
	if (((CButton*)GetDlgItem(IDC_CHECK_SMJET2))->GetCheck() == 1)
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(1);
		}

		//int n = m_nPrinterPosList.GetItemCount();
		int n = 4;

		for (int i = 0; i<4; i++)
		{
			CString sTmp = "", sText = "";
			sTmp.Format("A-JET%d", i + 2);
			sText.Format("喷头%d", i + 1);
			if (i == 0)
			{
				n = m_nPrinterPosList.InsertItem(i + n, sTmp);
			}
			else
			{
				m_nPrinterPosList.InsertItem(i + n, "");
			}

			m_nPrinterPosList.SetItemText(i + n, 1, sText);
			sTmp.Format("SMJET2_PRINTER%d_DDYJL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 2, sText);
			sTmp.Format("SMJET2_PRINTER%d_XDPYL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 3, sText);
			sTmp.Format("SMJET2_PRINTER%d_CHDS", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 4, sText);
			sTmp.Format("SMJET2_PRINTER%d_MHFX", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 5, sText);

		}
		float fPY = 0.0f;
		for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		{

			float fData = 0.0f;
			fData = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 3), NULL);
			fPY += fData;
			CString sText;
			sText.Format("%.2f", fPY);

			m_nPrinterPosList.SetItemText(i, 2, sText);
		}
	}
	else
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(0);
		}

		for (int i = 0; i<m_nPrinterPosList.GetItemCount(); i++)
		{
			CString sTmp = "";
			sTmp = m_nPrinterPosList.GetItemText(i, 0);
			if (sTmp == "A-JET2")
			{
				for (int j =i+3; j>=i; j--)
				{
					m_nPrinterPosList.DeleteItem(j);
				}
			}
		}

		//float fPY = 0.0f;
		//for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		//{

		//	float fData1 = 0.0f;
		//	if (i > 0)
		//		fData1 = (float)_tcstod(m_nPrinterPosList.GetItemText(i - 1, 2), NULL);

		//	float fData2 = 0.0f;
		//	fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 2), NULL);
		//	fPY = fData2 - fData1;
		//	CString sText;
		//	sText.Format("%.2f", fPY);

		//	m_nPrinterPosList.SetItemText(i, 3, sText);
		//}
	}
}


void CHardWareSetDlg::OnBnClickedCheckSmjet3()
{
	// TODO:  在此添加控件通知处理程序代码
	int nID = IDC_CHECK_SMJET3_PRINTER1;
	if (((CButton*)GetDlgItem(IDC_CHECK_SMJET3))->GetCheck() == 1)
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(1);
		}

		//int n = m_nPrinterPosList.GetItemCount();
		int n = 8;

		for (int i = 0; i<4; i++)
		{
			CString sTmp = "", sText = "";
			sTmp.Format("A-JET%d", i + 3);
			sText.Format("喷头%d", i + 1);
			if (i == 0)
			{
				n = m_nPrinterPosList.InsertItem(i + n, sTmp);
			}
			else
			{
				m_nPrinterPosList.InsertItem(i + n, "");
			}

			m_nPrinterPosList.SetItemText(i + n, 1, sText);
			sTmp.Format("SMJET3_PRINTER%d_DDYJL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 2, sText);
			sTmp.Format("SMJET3_PRINTER%d_XDPYL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 3, sText);
			sTmp.Format("SMJET3_PRINTER%d_CHDS", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 4, sText);
			sTmp.Format("SMJET3_PRINTER%d_MHFX", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 5, sText);

		}
		float fPY = 0.0f;
		for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		{

			float fData = 0.0f;
			fData = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 3), NULL);
			fPY += fData;
			CString sText;
			sText.Format("%.2f", fPY);

			m_nPrinterPosList.SetItemText(i, 2, sText);
		}
	}
	else
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(0);
		}
		for (int i = 0; i<m_nPrinterPosList.GetItemCount(); i++)
		{
			CString sTmp = "";
			sTmp = m_nPrinterPosList.GetItemText(i, 0);
			if (sTmp == "A-JET3")
			{
				for (int j = i + 3; j >= i; j--)
				{
					m_nPrinterPosList.DeleteItem(j);
				}
			}
		}
		//float fPY = 0.0f;
		//for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		//{

		//	float fData1 = 0.0f;
		//	if (i > 0)
		//		fData1 = (float)_tcstod(m_nPrinterPosList.GetItemText(i - 1, 2), NULL);

		//	float fData2 = 0.0f;
		//	fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 2), NULL);
		//	fPY = fData2 - fData1;
		//	CString sText;
		//	sText.Format("%.2f", fPY);

		//	m_nPrinterPosList.SetItemText(i, 3, sText);
		//}
	}
}


void CHardWareSetDlg::OnBnClickedCheckSmjet4()
{
	// TODO:  在此添加控件通知处理程序代码
	int nID = IDC_CHECK_SMJET4_PRINTER1;
	if (((CButton*)GetDlgItem(IDC_CHECK_SMJET4))->GetCheck() == 1)
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(1);
		}

		//int n = m_nPrinterPosList.GetItemCount();
		int n = 12;

		for (int i = 0; i<4; i++)
		{
			CString sTmp = "", sText = "";
			sTmp.Format("A-JET%d", i + 4);
			sText.Format("喷头%d", i + 1);
			if (i == 0)
			{
				n = m_nPrinterPosList.InsertItem(i + n, sTmp);
			}
			else
			{
				m_nPrinterPosList.InsertItem(i + n, "");
			}

			m_nPrinterPosList.SetItemText(i + n, 1, sText);
			sTmp.Format("SMJET4_PRINTER%d_DDYJL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 2, sText);
			sTmp.Format("SMJET4_PRINTER%d_XDPYL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 3, sText);
			sTmp.Format("SMJET4_PRINTER%d_CHDS", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 4, sText);
			sTmp.Format("SMJET4_PRINTER%d_MHFX", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 5, sText);

		}
		float fPY = 0.0f;
		for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		{

			float fData = 0.0f;
			fData = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 3), NULL);
			fPY += fData;
			CString sText;
			sText.Format("%.2f", fPY);

			m_nPrinterPosList.SetItemText(i, 2, sText);
		}
	}
	else
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(0);
		}

		for (int i = 0; i<m_nPrinterPosList.GetItemCount(); i++)
		{
			CString sTmp = "";
			sTmp = m_nPrinterPosList.GetItemText(i, 0);
			if (sTmp == "A-JET4")
			{
				for (int j = i + 3; j >= i; j--)
				{
					m_nPrinterPosList.DeleteItem(j);
				}
			}
		}
		//float fPY = 0.0f;
		//for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		//{

		//	float fData1 = 0.0f;
		//	if (i > 0)
		//		fData1 = (float)_tcstod(m_nPrinterPosList.GetItemText(i - 1, 2), NULL);

		//	float fData2 = 0.0f;
		//	fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 2), NULL);
		//	fPY = fData2 - fData1;
		//	CString sText;
		//	sText.Format("%.2f", fPY);

		//	m_nPrinterPosList.SetItemText(i, 3, sText);
		//}
	}
}


void CHardWareSetDlg::OnBnClickedCheckSmjet5()
{
	// TODO:  在此添加控件通知处理程序代码
	int nID = IDC_CHECK_SMJET5_PRINTER1;
	if (((CButton*)GetDlgItem(IDC_CHECK_SMJET5))->GetCheck() == 1)
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(1);
		}

		//int n = m_nPrinterPosList.GetItemCount();
		int n = 16;

		for (int i = 0; i<4; i++)
		{
			CString sTmp = "", sText = "";
			sTmp.Format("A-JET%d", i + 5);
			sText.Format("喷头%d", i + 1);
			if (i == 0)
			{
				n = m_nPrinterPosList.InsertItem(i + n, sTmp);
			}
			else
			{
				m_nPrinterPosList.InsertItem(i + n, "");
			}

			m_nPrinterPosList.SetItemText(i + n, 1, sText);
			sTmp.Format("SMJET5_PRINTER%d_DDYJL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 2, sText);
			sTmp.Format("SMJET5_PRINTER%d_XDPYL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 3, sText);
			sTmp.Format("SMJET5_PRINTER%d_CHDS", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 4, sText);
			sTmp.Format("SMJET5_PRINTER%d_MHFX", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 5, sText);

		}
		float fPY = 0.0f;
		for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		{

			float fData = 0.0f;
			fData = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 3), NULL);
			fPY += fData;
			CString sText;
			sText.Format("%.2f", fPY);

			m_nPrinterPosList.SetItemText(i, 2, sText);
		}
	}
	else
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(0);
		}

		for (int i = 0; i<m_nPrinterPosList.GetItemCount(); i++)
		{
			CString sTmp = "";
			sTmp = m_nPrinterPosList.GetItemText(i, 0);
			if (sTmp == "A-JET5")
			{
				for (int j = i + 3; j >= i; j--)
				{
					m_nPrinterPosList.DeleteItem(j);
				}
			}
		}
		//float fPY = 0.0f;
		//for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		//{

		//	float fData1 = 0.0f;
		//	if (i > 0)
		//		fData1 = (float)_tcstod(m_nPrinterPosList.GetItemText(i - 1, 2), NULL);

		//	float fData2 = 0.0f;
		//	fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 2), NULL);
		//	fPY = fData2 - fData1;
		//	CString sText;
		//	sText.Format("%.2f", fPY);

		//	m_nPrinterPosList.SetItemText(i, 3, sText);
		//}
	}
}


void CHardWareSetDlg::OnBnClickedCheckSmjet6()
{
	// TODO:  在此添加控件通知处理程序代码
	int nID = IDC_CHECK_SMJET6_PRINTER1;
	if (((CButton*)GetDlgItem(IDC_CHECK_SMJET6))->GetCheck() == 1)
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(1);
		}

		//int n = m_nPrinterPosList.GetItemCount();
		int n = 20;

		for (int i = 0; i<4; i++)
		{
			CString sTmp = "", sText = "";
			sTmp.Format("A-JET%d", i + 6);
			sText.Format("喷头%d", i + 1);
			if (i == 0)
			{
				n = m_nPrinterPosList.InsertItem(i + n, sTmp);
			}
			else
			{
				m_nPrinterPosList.InsertItem(i + n, "");
			}

			m_nPrinterPosList.SetItemText(i + n, 1, sText);
			sTmp.Format("SMJET6_PRINTER%d_DDYJL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 2, sText);
			sTmp.Format("SMJET6_PRINTER%d_XDPYL", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 3, sText);
			sTmp.Format("SMJET6_PRINTER%d_CHDS", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 4, sText);
			sTmp.Format("SMJET6_PRINTER%d_MHFX", i + 1);
			::GetPrivateProfileString(_T("HardWarePos"), sTmp, _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
			m_nPrinterPosList.SetItemText(i + n, 5, sText);

		}

		float fPY = 0.0f;
		for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		{

			float fData = 0.0f;
			fData = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 3), NULL);
			fPY += fData;
			CString sText;
			sText.Format("%.2f", fPY);

			m_nPrinterPosList.SetItemText(i, 2, sText);
		}
	}
	else
	{
		for (int i = 0; i<4; i++)
		{
			((CButton*)GetDlgItem(nID + i))->SetCheck(0);
		}

		for (int i = 0; i<m_nPrinterPosList.GetItemCount(); i++)
		{
			CString sTmp = "";
			sTmp = m_nPrinterPosList.GetItemText(i, 0);
			if (sTmp == "A-JET6")
			{
				for (int j = i + 3; j >= i; j--)
				{
					m_nPrinterPosList.DeleteItem(j);
				}
			}
		}

		//float fPY = 0.0f;
		//for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		//{

		//	float fData1 = 0.0f;
		//	if (i > 0)
		//		fData1 = (float)_tcstod(m_nPrinterPosList.GetItemText(i - 1, 2), NULL);

		//	float fData2 = 0.0f;
		//	fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 2), NULL);
		//	fPY = fData2 - fData1;
		//	CString sText;
		//	sText.Format("%.2f", fPY);

		//	m_nPrinterPosList.SetItemText(i, 3, sText);
		//}
	}
}
void CHardWareSetDlg::SaveInkBoxParamIni()
{
	int nIndex = 0;
	CString strText;
	CButton* pBtn = NULL;
	int nCheck;
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET1);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET1", strText, m_strParamPath); 
	if (nCheck == 1)
	{
		strText = m_nPrinterPosList.GetItemText(0, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER1_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(0, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER1_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(0, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER1_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(0, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER1_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(1, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER2_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(1, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER2_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(1, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER2_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(1, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER2_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(2, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER3_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(2, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER3_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(2, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER3_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(2, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER3_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(3, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER4_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(3, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER4_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(3, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER4_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(3, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET1_PRINTER4_MHFX", strText, m_strParamPath);
		nIndex = 3;
	}
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER1);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET1_PRINTER1", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER2);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET1_PRINTER2", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER3);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET1_PRINTER3", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER4);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET1_PRINTER4", strText, m_strParamPath);

	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET2);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET2", strText, m_strParamPath);
	if (nCheck == 1)
	{
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER1_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER1_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER1_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER1_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER2_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER2_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER2_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER2_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER3_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER3_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER3_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER3_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER4_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER4_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER4_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET2_PRINTER4_MHFX", strText, m_strParamPath);
		nIndex = nIndex + 4;
	}
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET2_PRINTER1);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET2_PRINTER1", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET2_PRINTER2);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET2_PRINTER2", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET2_PRINTER3);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET2_PRINTER3", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET2_PRINTER4);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET2_PRINTER4", strText, m_strParamPath);

	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET3);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET3", strText, m_strParamPath);
	if (nCheck == 1)
	{
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER1_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER1_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER1_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER1_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER2_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER2_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER2_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER2_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER3_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER3_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER3_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER3_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER4_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER4_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER4_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET3_PRINTER4_MHFX", strText, m_strParamPath);
		nIndex = nIndex + 4;
	}
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET3_PRINTER1);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET3_PRINTER1", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET3_PRINTER2);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET3_PRINTER2", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET3_PRINTER3);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET3_PRINTER3", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET3_PRINTER4);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET3_PRINTER4", strText, m_strParamPath);

	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET4);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET4", strText, m_strParamPath);
	if (nCheck == 1)
	{
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER1_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER1_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER1_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER1_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER2_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER2_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER2_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER2_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER3_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER3_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER3_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER3_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER4_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER4_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER4_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET4_PRINTER4_MHFX", strText, m_strParamPath);
		nIndex = nIndex + 4;
	}
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET4_PRINTER1);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET4_PRINTER1", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET4_PRINTER2);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET4_PRINTER2", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET4_PRINTER3);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET4_PRINTER3", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET4_PRINTER4);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET4_PRINTER4", strText, m_strParamPath);

	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET5);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET5", strText, m_strParamPath);
	if (nCheck == 1)
	{
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER1_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER1_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER1_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER1_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER2_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER2_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER2_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER2_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER3_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER3_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER3_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER3_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER4_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER4_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER4_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET5_PRINTER4_MHFX", strText, m_strParamPath);
		nIndex = nIndex + 4;
	}
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET5_PRINTER1);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET5_PRINTER1", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET5_PRINTER2);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET5_PRINTER2", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET5_PRINTER3);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET5_PRINTER3", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET5_PRINTER4);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET5_PRINTER4", strText, m_strParamPath);

	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET6);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET6", strText, m_strParamPath);
	if (nCheck == 1)
	{
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER1_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER1_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER1_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 1, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER1_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER2_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER2_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER2_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 2, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER2_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER3_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER3_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER3_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 3, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER3_MHFX", strText, m_strParamPath);

		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 2);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER4_DDYJL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 3);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER4_XDPYL", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 4);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER4_CHDS", strText, m_strParamPath);
		strText = m_nPrinterPosList.GetItemText(nIndex + 4, 5);
		::WritePrivateProfileString("HardWarePos", "SMJET6_PRINTER4_MHFX", strText, m_strParamPath);
		nIndex = nIndex + 4;
	}
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET6_PRINTER1);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET6_PRINTER1", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET6_PRINTER2);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET6_PRINTER2", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET6_PRINTER3);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET6_PRINTER3", strText, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET6_PRINTER4);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("HardWareSet", "SMJET6_PRINTER4", strText, m_strParamPath);
	//喷头位置
	
	//走纸方向
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("PaperDirection", "FORWARD", strText, m_strParamPath);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_RESERVE);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("PaperDirection", "RESERVE", strText, m_strParamPath);

	//编码器
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_CLOCKWISE);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("Encoder", "CLOCKWISE", strText, m_strParamPath);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_COUNTERCLOCKWISE);
	nCheck = pBtn->GetCheck();
	strText.Format("%d", nCheck);
	::WritePrivateProfileString("Encoder", "COUNTERCLOCKWISE", strText, m_strParamPath);

	strText.Format("%f", m_EncoderSetting);
	::WritePrivateProfileString("Encoder", "EncoderSetting", strText, m_strParamPath);


	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;
	for (int i = 0; i < pView->m_nSMJET; i++)
	{
		for (int j = 0; j<4; j++)
			pView->SetRepPoint(i, j, GetBadPointNum(i, j));
	}

}
void CHardWareSetDlg::ReadInkBoxParamIni()
{
	int nIndex = 0;
	CButton* pBtn = NULL;
	CString sText;
	int nSwRows[300], nSwCols[300];
	CString text = "正|反|";
	int nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET1"), 1, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET1);
	pBtn->SetCheck(nCheck);
	if (nCheck == 1)
	{
		m_nPrinterPosList.SetItemText(0, 0, "A-JET1");
		m_nPrinterPosList.SetItemText(0, 1, "喷头1");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER1_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(0, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER1_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(0, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER1_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(0, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER1_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(0, 5, sText);

		m_nPrinterPosList.SetItemText(1, 1, "喷头2");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER2_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(1, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER2_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(1, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER2_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(1, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER2_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(1, 5, sText);

		m_nPrinterPosList.SetItemText(2, 1, "喷头3");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER3_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(2, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER3_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(2, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER3_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(2, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER3_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(2, 5, sText);

		m_nPrinterPosList.SetItemText(3, 1, "喷头4");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER4_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(3, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER4_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(3, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER4_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(3, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET1_PRINTER4_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(3, 5, sText);

		nIndex = 3;
	}
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET1_PRINTER1"), 1, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER1);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET1_PRINTER2"), 1, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER2);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET1_PRINTER3"), 1, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER3);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET1_PRINTER4"), 1, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET1_PRINTER4);
	pBtn->SetCheck(nCheck);

	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET2"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET2);
	pBtn->SetCheck(nCheck);
	if (nCheck == 1)
	{
		OnBnClickedCheckSmjet2();
		m_nPrinterPosList.SetItemText(nIndex + 1, 0, "A-JET2");
		m_nPrinterPosList.SetItemText(nIndex + 1, 1, "喷头1");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER1_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER1_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER1_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER1_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 2, 1, "喷头2");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER2_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER2_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER2_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER2_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 3, 1, "喷头3");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER3_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER3_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER3_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER3_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 4, 1, "喷头4");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER4_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER4_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER4_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET2_PRINTER4_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 5, sText);
		nIndex = nIndex + 4;
	}
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET2_PRINTER1"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET2_PRINTER1);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET2_PRINTER2"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET2_PRINTER2);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET2_PRINTER3"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET2_PRINTER3);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET2_PRINTER4"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET2_PRINTER4);
	pBtn->SetCheck(nCheck);

	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET3"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET3);
	pBtn->SetCheck(nCheck);
	if (nCheck == 1)
	{
		OnBnClickedCheckSmjet3();
		m_nPrinterPosList.SetItemText(nIndex + 1, 0, "A-JET3");
		m_nPrinterPosList.SetItemText(nIndex + 1, 1, "喷头1");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER1_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER1_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER1_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER1_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 2, 1, "喷头2");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER2_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER2_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER2_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER2_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 3, 1, "喷头3");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER3_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER3_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER3_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER3_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 4, 1, "喷头4");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER4_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER4_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER4_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET3_PRINTER4_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 5, sText);
		nIndex = nIndex + 4;
	}
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET3_PRINTER1"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET3_PRINTER1);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET3_PRINTER2"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET3_PRINTER2);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET3_PRINTER3"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET3_PRINTER3);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET3_PRINTER4"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET3_PRINTER4);
	pBtn->SetCheck(nCheck);

	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET4"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET4);
	pBtn->SetCheck(nCheck);
	if (nCheck == 1)
	{
		OnBnClickedCheckSmjet4();
		m_nPrinterPosList.SetItemText(nIndex + 1, 0, "A-JET4");
		m_nPrinterPosList.SetItemText(nIndex + 1, 1, "喷头1");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER1_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER1_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER1_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER1_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 2, 1, "喷头2");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER2_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER2_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER2_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER2_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 3, 1, "喷头3");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER3_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER3_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER3_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER3_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 4, 1, "喷头4");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER4_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER4_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER4_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET4_PRINTER4_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 5, sText);
		nIndex = nIndex + 4;
	}
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET4_PRINTER1"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET4_PRINTER1);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET4_PRINTER2"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET4_PRINTER2);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET4_PRINTER3"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET4_PRINTER3);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET4_PRINTER4"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET4_PRINTER4);
	pBtn->SetCheck(nCheck);

	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET5"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET5);
	pBtn->SetCheck(nCheck);
	if (nCheck == 1)
	{
		OnBnClickedCheckSmjet5();
		m_nPrinterPosList.SetItemText(nIndex + 1, 0, "A-JET5");
		m_nPrinterPosList.SetItemText(nIndex + 1, 1, "喷头1");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER1_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER1_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER1_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER1_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 2, 1, "喷头2");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER2_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER2_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER2_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER2_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 3, 1, "喷头3");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER3_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER3_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER3_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER3_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 4, 1, "喷头4");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER4_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER4_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER4_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET5_PRINTER4_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 5, sText);
		nIndex = nIndex + 4;
	}
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET5_PRINTER1"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET5_PRINTER1);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET5_PRINTER2"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET5_PRINTER2);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET5_PRINTER3"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET5_PRINTER3);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET5_PRINTER4"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET5_PRINTER4);
	pBtn->SetCheck(nCheck);

	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET6"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET6);
	pBtn->SetCheck(nCheck);
	if (nCheck == 1)
	{
		OnBnClickedCheckSmjet6();
		m_nPrinterPosList.SetItemText(nIndex + 1, 0, "A-JET6");
		m_nPrinterPosList.SetItemText(nIndex + 1, 1, "喷头1");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER1_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER1_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER1_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER1_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 1, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 2, 1, "喷头2");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER2_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER2_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER2_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER2_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 2, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 3, 1, "喷头3");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER3_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER3_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER3_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER3_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 3, 5, sText);

		m_nPrinterPosList.SetItemText(nIndex + 4, 1, "喷头4");
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER4_DDYJL"), _T("10.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 2, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER4_XDPYL"), _T("0.00"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 3, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER4_CHDS"), _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 4, sText);
		::GetPrivateProfileString(_T("HardWarePos"), _T("SMJET6_PRINTER4_MHFX"), _T("正"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_nPrinterPosList.SetItemText(nIndex + 4, 5, sText);
		nIndex = nIndex + 4;
	}
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET6_PRINTER1"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET6_PRINTER1);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET6_PRINTER2"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET6_PRINTER2);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET6_PRINTER3"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET6_PRINTER3);
	pBtn->SetCheck(nCheck);
	nCheck = ::GetPrivateProfileInt(_T("HardWareSet"), _T("SMJET6_PRINTER4"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_CHECK_SMJET6_PRINTER4);
	pBtn->SetCheck(nCheck);
	//喷头位置

	//走纸方向"PaperDirection", "FORWARD"
	nCheck = ::GetPrivateProfileInt(_T("PaperDirection"), _T("FORWARD"), 1, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_FORWARD);
	pBtn->SetCheck(nCheck);

	nCheck = ::GetPrivateProfileInt(_T("PaperDirection"), _T("RESERVE"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_RESERVE);
	pBtn->SetCheck(nCheck);

	//编码器"Encoder", "CLOCKWISE"
	nCheck = ::GetPrivateProfileInt(_T("Encoder"), _T("CLOCKWISE"), 1, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_CLOCKWISE);
	pBtn->SetCheck(nCheck);

	nCheck = ::GetPrivateProfileInt(_T("Encoder"), _T("COUNTERCLOCKWISE"), 0, m_strParamPath);
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_COUNTERCLOCKWISE);
	pBtn->SetCheck(nCheck);

	::GetPrivateProfileString(_T("Encoder"), _T("EncoderSetting"), _T("0.0846"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
	float fData = (float)_tcstod(sText, NULL);
	m_EncoderSetting = fData;
	UpdateData(FALSE);
}

HBRUSH CHardWareSetDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}


void CHardWareSetDlg::OnLvnEndlabeleditListPrinterPos(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
	// TODO:  在此添加控件通知处理程序代码
	*pResult = 0;

	LV_ITEM	* plvitem = &(pDispInfo->item);

	if (plvitem->iSubItem == 3)
	{
		float fPY = 0.0f;
		for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		{

			float fData = 0.0f;	
			fData = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 3), NULL);
			fPY += fData;
			CString sText;
			sText.Format("%.2f", fPY);

			m_nPrinterPosList.SetItemText(i, 2, sText);
		}
	}

	else if (plvitem->iSubItem == 2)
	{

		float fPY = 0.0f;


		for (int i = 0; i < m_nPrinterPosList.GetItemCount(); i++)
		{

			float fData1 = 0.0f;
			if (i > 0)
				fData1 = (float)_tcstod(m_nPrinterPosList.GetItemText(i - 1, 2), NULL);

			float fData2 = 0.0f;
			fData2 = (float)_tcstod(m_nPrinterPosList.GetItemText(i, 2), NULL);
			fPY = fData2 - fData1;
			CString sText;
			sText.Format("%.2f", fPY);

			m_nPrinterPosList.SetItemText(i, 3, sText);


		}
		

	}

}
