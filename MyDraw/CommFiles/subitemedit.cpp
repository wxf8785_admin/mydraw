/////////////////////////////////////////////////////////////////////////////////////////////////////////
// subitemedit.cpp : implementation file
//

#include "../stdafx.h"
#include "../MyDraw.h"
#include "../MainFrm.h"
#include "subitemedit.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSubItemEdit

CSubItemEdit::CSubItemEdit()
{
}

CSubItemEdit::CSubItemEdit(int iItem, int iSubItem, CString sInitText)
:m_sInitText( sInitText )
{
	m_iItem = iItem;
	m_iSubItem = iSubItem;
	m_bEsc = FALSE;
}


CSubItemEdit::~CSubItemEdit()
{
}


BEGIN_MESSAGE_MAP(CSubItemEdit, CEdit)
	//{{AFX_MSG_MAP(CSubItemEdit)
	ON_WM_KILLFOCUS()
	ON_WM_CREATE()
	ON_WM_NCDESTROY()
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSubItemEdit message handlers

void CSubItemEdit::OnKillFocus(CWnd* pNewWnd) 
{
	CEdit::OnKillFocus(pNewWnd);

	CString str;
	GetWindowText(str);

	// send notification to parent of listview ctrl
	LV_DISPINFO dispinfo;
	dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
	dispinfo.hdr.idFrom = GetDlgCtrlID();
	dispinfo.hdr.code = LVN_ENDLABELEDIT;

	dispinfo.item.mask = LVIF_TEXT;
	dispinfo.item.iItem = m_iItem;
	dispinfo.item.iSubItem = m_iSubItem;
	dispinfo.item.pszText = m_bEsc ? NULL : LPTSTR((LPCTSTR)str);
	dispinfo.item.cchTextMax = str.GetLength();

	GetParent()->GetParent()->SendMessage( WM_NOTIFY, GetParent()->GetDlgCtrlID(), 
					(LPARAM)&dispinfo );

// 	CAcTestView* pView=(CAcTestView*)((CMainFrame*)AfxGetApp()->m_pMainWnd)->GetActiveView();
// 	pView->SetParaList();
// 		
// 	if(1052 == GetParent()->GetDlgCtrlID())
// 		pView->SendDCWeight();
	
	GetParent()->GetParent()->SendMessage(USER_KILL_FOCUS,(WPARAM)m_iItem,(LPARAM)m_iSubItem);

	DestroyWindow();
		
}

int CSubItemEdit::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CEdit::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CFont* font = GetParent()->GetFont();
	SetFont(font);

	SetWindowText( m_sInitText );
	SetFocus();
	SetSel( 0, -1 );
	
	return 0;
}

void CSubItemEdit::OnNcDestroy() 
{
	CEdit::OnNcDestroy();
	
	// TODO: Add your message handler code here
	delete this;
}

BOOL CSubItemEdit::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if(pMsg->wParam == VK_RETURN
				|| pMsg->wParam == VK_DELETE
				|| pMsg->wParam == VK_ESCAPE
				|| GetKeyState( VK_CONTROL)
				)
		{
			::TranslateMessage(pMsg);
			::DispatchMessage(pMsg);
			

// 			CAcTestView* pView=(CAcTestView*)((CMainFrame*)AfxGetApp()->m_pMainWnd)->GetActiveView();
// 			pView->SetParaList();
			
			return TRUE;		    	// do not process further
		}
	}
	
	return CEdit::PreTranslateMessage(pMsg);
}

void CSubItemEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if( nChar == VK_ESCAPE || nChar == VK_RETURN)
	{
		if( nChar == VK_ESCAPE )
			m_bEsc = true;
		GetParent()->SetFocus();
		return;
	}

	CEdit::OnChar(nChar, nRepCnt, nFlags);
	// resize edit control if needed

	// get text extent
	CString str;

	GetWindowText( str );
	CWindowDC dc(this);
	CFont *pfont = GetParent()->GetFont();
	CFont *pfontdc = dc.SelectObject( pfont );
	CSize size = dc.GetTextExtent( str );
	dc.SelectObject( pfontdc );
	size.cx += 5;			   	// add some extra buffer

	// get client rect
	CRect rect, parentrect;
	GetClientRect( &rect );
	GetParent()->GetClientRect( &parentrect );

	// transform rect to parent coordinates
	ClientToScreen( &rect );
	GetParent()->ScreenToClient( &rect );

	// check whether control needs to be resized
	// and whether there is space to grow
	if( size.cx > rect.Width() )
	{
		if( size.cx + rect.left < parentrect.right )
			rect.right = rect.left + size.cx;
		else
			rect.right = parentrect.right;
		MoveWindow( &rect );
	}
}
/******************************************************************************
*	功能说明：	设置数据检验标准
*	输入参数：	MaxValue 最大值
*				MinVaue 最小值
*				DecimalDigits 小数位数
******************************************************************************/
void CSubItemEdit::SetCheckStandard(float MaxValue, float MinValue, int DecimalDigits)
{
	m_fMax = MaxValue;
	m_fMin = MinValue;
	m_nDecimal = DecimalDigits;
}
/************************************************************************/
/*                                                                      */
/************************************************************************/