#pragma once
#include "MyCommand.h"
#include "stdafx.h"
class CInvoker
{
public:
	~CInvoker(void);
	static CInvoker* Invoker();
	void SetCommand(MyCommand* com);
	void Execute();
	void UnExecute();
	void ReExecute();
public:
	CArray<MyCommand*,MyCommand*> Command;
	CArray<MyCommand*,MyCommand*> UnCommand;
protected:
	CInvoker(void);
private:
	static CInvoker* _invoker;
	MyCommand* my_com;
};
