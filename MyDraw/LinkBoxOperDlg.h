#if !defined(AFX_LINKBOXOPERDLG_H__72BB4F03_3066_490A_81DD_BFDAE9D0305D__INCLUDED_)
#define AFX_LINKBOXOPERDLG_H__72BB4F03_3066_490A_81DD_BFDAE9D0305D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LinkBoxOperDlg.h : header file
//
#include "CommFiles/ListCtrlExt.h"
/////////////////////////////////////////////////////////////////////////////
// CLinkBoxOperDlg dialog

class CLinkBoxOperDlg : public CDialog
{
// Construction
public:
	CLinkBoxOperDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CLinkBoxOperDlg)
	enum { IDD = IDD_DIALOG_LINKBOXOPER };
	CListCtrlExt	m_InkBoxParamList;
	CListCtrlExt	m_InkBoxIndentifyList;
	CComboBox		m_nSMJETIndex_H;
	CComboBox		m_nSMJETIndex_B;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLinkBoxOperDlg)
	protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation

public:
	void CLinkBoxOperDlg::OnInitListData();
	float CLinkBoxOperDlg::GetInkBoxParam(int iRow,int iCol);//ī����ز���	
	CString m_strParamPath;
	int m_nSelIndex;
	void CLinkBoxOperDlg::SaveInkBoxParamIni(int nSelIndex);
	void CLinkBoxOperDlg::ReadInkBoxParamIni(int nSelIndex);

	afx_msg LRESULT onListEditKillFocus(WPARAM wParam, LPARAM lParam);

protected:

	// Generated message map functions
	//{{AFX_MSG(CLinkBoxOperDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonIndentify();
	afx_msg void OnButtonSettingInkboxType();
	afx_msg void OnButtonRead();
	afx_msg void OnButtonWrite();
	afx_msg void OnButtonWash();
	afx_msg void OnButtonReplace();
	afx_msg void OnSelchangeComboSMJETHIndex();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINKBOXOPERDLG_H__72BB4F03_3066_490A_81DD_BFDAE9D0305D__INCLUDED_)
