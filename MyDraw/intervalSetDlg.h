#pragma once
#include "afxwin.h"


// intervalSetDlg 对话框

class intervalSetDlg : public CDialogEx
{
	DECLARE_DYNAMIC(intervalSetDlg)

public:
	intervalSetDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~intervalSetDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_INTERVALSET };

protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	double m_dIntervalSet;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CComboBox comboFX;
	int m_combo_fx;
};
