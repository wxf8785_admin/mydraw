// RightView.h : CQRcodeProperties 
// Date 2006/05/17	Ver. 1.22	Psytec Inc.

#if !defined(AFX_RIGHTVIEW_H__F346E2CF_81BB_46DF_9F23_8351B7F80F49__INCLUDED_)
#define AFX_RIGHTVIEW_H__F346E2CF_81BB_46DF_9F23_8351B7F80F49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

/////////////////////////////////////////////////////////////////////////////
// CQRcodeProperties 

class CQRcodeProperties : public CDialog
{
public:
	CQRcodeProperties(CWnd* pParent = NULL);

// フォーム データ
public:

	enum { IDD = IDD_QRcodeProperties };
	CComboBox m_comboLevel;
	CComboBox m_comboVersion;
	CStatic m_staticVersion;
	CButton m_checkAutoExtent;
	CComboBox m_comboMaskingNo;
	CStatic m_staticMaskingNo;
	CEdit m_editModuleSize;
	CSpinButtonCtrl m_spinModuleSize;
	CStatic m_staticBmpSize;





private:
	BOOL m_bInitControl;

protected:
	virtual void DoDataExchange(CDataExchange* pDX); // DDX/DDV サポート
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL


protected:
	
	//{{AFX_MSG(CQRcodeProperties)

	afx_msg void OnSelChangeLevel();
	afx_msg void OnSelChangeVersion();
	afx_msg void OnAutoExtent();
	afx_msg void OnSelChangeMaskNo();
	afx_msg void OnChangeDrawSize();

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	int nLevel;
	int nVersion;
	BOOL bAutoExtent;
	int nMaskingNo;
	float m_fQRRotateAngle;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}

#endif // !defined(AFX_RIGHTVIEW_H__F346E2CF_81BB_46DF_9F23_8351B7F80F49__INCLUDED_)
