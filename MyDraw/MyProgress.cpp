// MyProgress.cpp: implementation of the CMyProgress class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MyProgress.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMyProgress::CMyProgress()
{

}

CMyProgress::~CMyProgress()
{
}
//去掉进度条
void CMyProgress::DestroyWindow()
{
	m_ProgressDlg.DestroyWindow();
}
BOOL CMyProgress::CreateProgressDlg(long nLength)
{
	m_ProgressLen = nLength;
	m_ProgressNum = 0;
	m_ProgressTmp = 0;	
	return m_ProgressDlg.Create( NULL );  // By default Create will disable the application's main window
}
void CMyProgress::SetCaption(CString strCaption)
{
	m_ProgressDlg.SetWindowText(strCaption);
}
void CMyProgress::HideCancel()
{
	CWnd *pWnd = NULL;
	pWnd = m_ProgressDlg.GetDlgItem(IDCANCEL);
	if(pWnd)
	{
		pWnd->ShowWindow(SW_HIDE);
	}
}
void CMyProgress::SetStatus(CString strMsg)
{
	m_ProgressDlg.SetStatus(strMsg);
}
//如果用户取消了，则返回FALSE
BOOL CMyProgress::DoProgress(long offset)
{
	offset += m_ProgressTmp;
	double len = m_ProgressLen / 100.0;
	int num = (int)(offset / len - m_ProgressNum);
	
	if(m_ProgressDlg.CheckCancelButton())
	{
		//CString strCancel = "";
		//strCancel.LoadString(IDS_STRING_CANCEL);
		//if(AfxMessageBox(strCancel,MB_YESNO)==IDYES){
		//	CString strStatus = "";
		//	strStatus.LoadString(IDS_CANCELING);//正在取消，请稍等
		//	SetStatus(strStatus);
		//	return FALSE;
		//}
	}
	
	if(num > 1)
	{
		for(int i = 0; i < num; i++)
		{
		    m_ProgressDlg.StepIt();
			m_ProgressNum++;
		}
	}
	m_ProgressDlg.UpdateWindowText(offset);
	return TRUE;
}
void CMyProgress::StepPart(long nPartLength)
{
	m_ProgressTmp += nPartLength;
}


int CMyProgress::GetPos()
{
	return m_ProgressDlg.GetPos();
}
