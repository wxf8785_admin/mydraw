// Line.h: interface for the CLine class.
//
//////////////////////////////////////////////////////////////////////

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CLine :public CGraph
{
//    DECLARE_DYNAMIC(CLine)
    DECLARE_SERIAL(CLine)
public:
	//int startX;
	//int startY;
	//int endX;
	//int endY;

	LOGPEN LinePen;
	CLine();
	virtual ~CLine();
	void Serialize(CArchive & ar);
	virtual CGraph* Clone();
	virtual void  InitPropList();
};
