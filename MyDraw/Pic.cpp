// Pic.cpp: implementation of the CPic class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MyDraw.h"
#include "MyDrawView.h"
#include "MainFrm.h"
#include "Pic.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CPic,CObject,1)


CPic::CPic()
{

	tracker.m_nStyle=CRectTracker::resizeInside|
		CRectTracker::dottedLine; 
	tracker.m_rect.SetRect(0,0,0,0); 
	tracker.m_nHandleSize=8; 

	bIsSelected=false;
	startX=41;
	startY=41;
	nWidth=50;
	nHeight=88;

	isStaticPic = true;
	m_PicStaticPathName = "TEST.bmp";
	m_PicDirectory = "";
	m_PicFieldNum = 0;
	nPicRotateAngle = 0;

	nGraphics = 7;
	m_isFixScale = false;
}

CPic::~CPic()
{

}


void CPic::Serialize(CArchive & ar)
{
	CObject::Serialize(ar);
    if(ar.IsLoading())
    {
		ar >> startX >> startY >> nWidth >> nHeight >>
			m_PicStaticName >> m_PicStaticPathName >> m_PicDirectory >> 
			isStaticPic >> m_PicFieldNum >> nPicRotateAngle >> m_isFixScale;


    }
	else
	{
		ar << startX << startY << nWidth << nHeight <<
			m_PicStaticName << m_PicStaticPathName << m_PicDirectory <<
			isStaticPic << m_PicFieldNum << nPicRotateAngle << m_isFixScale;
    }
}




CGraph* CPic::Clone()
{
	CPic* p = new CPic();

	p->startX = this->startX + 10;
	p->startY = this->startY + 10;

	p->m_PicStaticPathName = this->m_PicStaticPathName;
	p->m_PicStaticName = this->m_PicStaticName;
	p->m_PicDirectory = this->m_PicDirectory;
	p->m_PicFieldNum = this->m_PicFieldNum;
	p->m_isFixScale = this->m_isFixScale;

	p->isStaticPic = this->isStaticPic;
	p->nPicRotateAngle = this->nPicRotateAngle;

	return p;
}



void  CPic::InitPropList()
{

	CMFCPropertyGridProperty * pProp;
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMyDrawView *pView = (CMyDrawView *)pFrame->GetActiveView();

	pFrame->m_wndProperties.graphs.Add(this);
	//pFrame->m_wndProperties.m_wndObjectCombo.SetCurSel(nGraphics);
	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(0);
	//读取数据
	CString itemname;
	itemname = pProp->GetName(); //获取名称
	COleVariant itemvalue;
	itemvalue = pProp->GetValue();//获取值
	//写入数据
	CString m_startX;
	m_startX.Format(_T("%.2f"), startX / pView->ScaleX);
	pProp->SetValue((_variant_t)(m_startX));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(1);
	CString m_startY;
	m_startY.Format(_T("%.2f"), startY / pView->ScaleY);
	pProp->SetValue((_variant_t)(m_startY));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(0);
	CString m_BarWidth;
	m_BarWidth.Format(_T("%.2f"), (endX - startX) / pView->ScaleX * pView->Zoom);
	pProp->SetValue((_variant_t)(m_BarWidth));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(1);
	CString m_BarHeight;
	m_BarHeight.Format(_T("%.2f"), (endY - startY) / pView->ScaleY * pView->Zoom);
	pProp->SetValue((_variant_t)(m_BarHeight));


}