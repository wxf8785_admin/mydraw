#pragma once
#include "afxwin.h"

class CGraph : public CObject//用于保存，重绘
{
	//DECLARE_SERIAL(CGraph)//序列化机制
public:
	CGraph(void);
	virtual ~CGraph(void);
	/**/
	virtual void Serialize(CArchive& ar)=0;

	UINT GetType();
	virtual CRect GetRect();
	virtual void SetRect(CRect re);
	CRect _OriRect; //移动前的位置
	//

	CGraph* operator =(CGraph& p);
	virtual CGraph* Clone()=0;
	virtual void Add(CGraph*){}
	virtual void Remove(){}
	//
public:
	UINT nGraphics;
	int startX;
	int startY;
	int endX;
	int endY;


public:
	CRectTracker tracker;
	bool bIsSelected;
	bool bIsLocked;



	virtual void  InitPropList() = 0;


};
