/////////////////////////////////////////////////////////////////////////////
// Rulers. Written By Stefan Ungureanu (stefanu@usa.net)
//

#include "stdafx.h"
#include "Ruler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRulerSplitterWnd

CRulerSplitterWnd::CRulerSplitterWnd()
{
	m_cxSplitter = 0;
	m_cySplitter = 0;
	m_cxBorderShare = 0;
	m_cyBorderShare = 0;
	m_cxSplitterGap = 1;
	m_cySplitterGap = 1;
	m_bRulersVisible = FALSE;
}

CRulerSplitterWnd::~CRulerSplitterWnd()
{
}


BEGIN_MESSAGE_MAP(CRulerSplitterWnd, CSplitterWnd)
	//{{AFX_MSG_MAP(CRulerSplitterWnd)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CRulerSplitterWnd::CreateRulers(CFrameWnd* pParent, CCreateContext* pContext)
{
	if (!CreateStatic(pParent, 2, 2))
		return FALSE;

	if (!CreateView(0, 0, RUNTIME_CLASS(CRulerCornerView), CSize(0, 0), pContext))
		return FALSE;
	if (!CreateView(0, 1, RUNTIME_CLASS(CRulerView), CSize(0, 0), pContext))
		return FALSE;
	if (!CreateView(1, 0, RUNTIME_CLASS(CRulerView), CSize(0, 0), pContext))
		return FALSE;
	if (!CreateView(1, 1, pContext->m_pNewViewClass, CSize(0, 0), pContext))
		return FALSE;

	SetColumnInfo(0, 0, 0);
	SetRowInfo(0, 0, 0);

	((CRulerView*)GetPane(0, 1))->SetRulerType(RT_HORIZONTAL);
	((CRulerView*)GetPane(1, 0))->SetRulerType(RT_VERTICAL);

	SetActivePane(1, 1);

	return TRUE;
}

void CRulerSplitterWnd::ShowRulers(BOOL bShow, BOOL bSave)
{
	int nSize = (bShow) ? RULER_SIZE : 0;
	int nSizeBorder = (bShow) ? 3 : 1;

	SetRowInfo(0, nSize, 0);
	SetColumnInfo(0, nSize, 0);
	m_cxSplitterGap = nSizeBorder;
	m_cySplitterGap = nSizeBorder;
	m_bRulersVisible = (bSave) ? bShow : m_bRulersVisible;
	RecalcLayout();
}

void CRulerSplitterWnd::UpdateRulersInfo(stRULER_INFO stRulerInfo)
{
	((CRulerView*)GetPane(0, 1))->UpdateRulersInfo(stRulerInfo);
	((CRulerView*)GetPane(1, 0))->UpdateRulersInfo(stRulerInfo);


	if (m_bRulersVisible)
		ShowRulers(TRUE, FALSE);
}

void CRulerSplitterWnd::OnLButtonDown(UINT nFlags, CPoint point)
{
	//Lock Splitter
	//CSplitterWnd::OnLButtonDown(nFlags, point);
}

void CRulerSplitterWnd::OnMouseMove(UINT nFlags, CPoint point)
{
	//Lock Splitter
	//CSplitterWnd::OnMouseMove(nFlags, point);
}
/////////////////////////////////////////////////////////////////////////////
// CRulerSplitterWnd message handlers

/////////////////////////////////////////////////////////////////////////////
// CRulerView

IMPLEMENT_DYNCREATE(CRulerView, CView)

CRulerView::CRulerView()
{
	m_rulerType = RT_HORIZONTAL;
	m_scrollPos = 0;
	m_lastPos = 0;
	m_fZoomFactor = 1;

	font.CreatePointFont(90, _T("宋体"));
}

CRulerView::~CRulerView()
{
}


BEGIN_MESSAGE_MAP(CRulerView, CView)
	//{{AFX_MSG_MAP(CRulerView)
	ON_WM_SETFOCUS()
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRulerView drawing
void CRulerView::UpdateRulersInfo(stRULER_INFO stRulerInfo)
{
	m_DocSize = stRulerInfo.DocSize;
	m_fZoomFactor = stRulerInfo.fZoomFactor;
	m_scrollPos = stRulerInfo.ScrollPos;

	if (stRulerInfo.uMessage == RW_POSITION) {
		DrawCursorPos(stRulerInfo.Pos);
	}
	else if (stRulerInfo.uMessage == RW_SELECTION) {
		DrawSelection(stRulerInfo.Rect);
	}
	else if ((m_rulerType == RT_HORIZONTAL) && (stRulerInfo.uMessage == RW_HSCROLL) ||
		(m_rulerType == RT_VERTICAL) && (stRulerInfo.uMessage == RW_VSCROLL)) {

		CDC* pDC = GetDC();
		OnDraw(pDC);
		ReleaseDC(pDC);
	}
	//else
	//	Invalidate(FALSE);
}

void CRulerView::OnDraw(CDC* pDC)
{
	int iLOGPIXELSX = pDC->GetDeviceCaps(LOGPIXELSX);// 得到当前显示设备的水平单位英寸像素数
	int iLOGPIXELSY = pDC->GetDeviceCaps(LOGPIXELSY);// 得到当前显示设备的垂直单位英寸像素数
	//X每毫米像素数
	ScaleX = iLOGPIXELSX / 25.4f *m_fZoomFactor;
	//Y每毫米像素数
	ScaleY = iLOGPIXELSY / 25.4f *m_fZoomFactor;

	m_lastPos = 0;

	// set the map mode right
	int oldMapMode = pDC->SetMapMode(MM_TEXT);
	int oldBkMode = pDC->SetBkMode(TRANSPARENT);
	CFont *pOldFt = pDC->SelectObject(&font);

	CRect rulerRect;
	GetClientRect(&rulerRect);

	if (m_rulerType == RT_HORIZONTAL)
	{
		rulerRect.right = (int)(m_DocSize.cx);
	}
	else //(m_rulerType==RT_VERTICAL)
	{
		rulerRect.bottom = (int)(m_DocSize.cy);
	}

	pDC->FillSolidRect(rulerRect, RGB(255, 255, 255));

	if (m_rulerType == RT_HORIZONTAL)
	{
		Draw_H(pDC, rulerRect);
	}
	else //(m_rulerType==RT_VERTICAL)
	{
		Draw_V(pDC, rulerRect);
	}

	// restore DC settings
	pDC->SetMapMode(oldMapMode);
	pDC->SelectObject(pOldFt);
	pDC->SetBkMode(oldBkMode);
}

void CRulerView::Draw_H(CDC* pDC, CRect rulerRect)
{
	if (m_fZoomFactor > 0.3)
	{
		for (int i = 0; i < rulerRect.right - rulerRect.left; i++)
		{
			int Num = (int)(i / ScaleX);
			int nScaleLenth = 3;
			CString txt = "";
			if (Num % 10 == 0)
			{
				txt.Format("%d", Num / 10);
				nScaleLenth = 12;
			}
			else if (Num % 5 == 0)
			{
				nScaleLenth = 8;
			}

			if (i > 0)
			{
				int Mult = Num - (int)((i - 1) / ScaleX);
				if (Mult > 0.5)
				{
					pDC->PatBlt(i - m_scrollPos.x, rulerRect.bottom - nScaleLenth, 1, rulerRect.bottom, BLACKNESS);
					if (txt != "")
					{
						pDC->TextOut(i - m_scrollPos.x - 2, rulerRect.top, txt);
					}
				}
			}

		}
	}
	else if (m_fZoomFactor > 0.15)
	{
		DrawTicker(pDC, rulerRect, 100 * ScaleX, 13);
	}
	else
	{
		DrawTicker(pDC, rulerRect, 500 * ScaleX, 13);
	}


}

void CRulerView::Draw_V(CDC* pDC, CRect rulerRect)
{
	if (m_fZoomFactor > 0.3)
	{
		for (int i = 0; i < rulerRect.bottom - rulerRect.top; i++)
		{
			int Num = (int)(i / ScaleY);
			int nScaleLenth = 3;
			CString txt = "";
			if (Num % 10 == 0)
			{
				txt.Format("%d", Num / 10);
				nScaleLenth = 12;
			}
			else if (Num % 5 == 0)
			{
				nScaleLenth = 8;
			}

			if (i > 0)
			{
				int Mult = Num - (int)((i - 1) / ScaleY);
				if (Mult > 0.5)
				{
					pDC->PatBlt(rulerRect.right - nScaleLenth, i - m_scrollPos.y, rulerRect.right, 1, BLACKNESS);
					if (txt != "")
					{
						pDC->TextOut(rulerRect.left + 2, i - m_scrollPos.y - 5, txt);
					}
				}
			}

		}
	}
	else if (m_fZoomFactor > 0.15)
	{
		DrawTicker(pDC, rulerRect, 100 * ScaleY, 12);
	}
	else if (m_fZoomFactor > 0)
	{
		DrawTicker(pDC, rulerRect, 500 * ScaleY, 12);
	}

}

void CRulerView::DrawTicker(CDC* pDC, CRect rulerRect, float nFactor, int nBegin, BOOL bShowPos)
{
	int nSize = (m_rulerType == RT_HORIZONTAL) ? rulerRect.right : rulerRect.bottom;
	float nTick = nFactor*m_fZoomFactor;

	for (int i = 1; i <= nSize / nTick; i++)
	{
		char buffer[100];
		if (m_fZoomFactor > 0.3)
		{
			sprintf(buffer, "%d", i);
		}
		else if (m_fZoomFactor > 0.15)
		{
			sprintf(buffer, "%d", i * 2);
		}
		else if (m_fZoomFactor > 0)
		{
			sprintf(buffer, "%d", i * 5);
		}


		if (m_rulerType == RT_HORIZONTAL)
		{
			pDC->PatBlt(nTick*i - m_scrollPos.x, rulerRect.top + nBegin, 1, rulerRect.bottom, BLACKNESS);
			if (bShowPos)
				pDC->TextOut(nTick*i - m_scrollPos.x - 2, rulerRect.top, CString(buffer));
		}
		else //(m_rulerType==RT_VERTICAL)
		{
			pDC->PatBlt(rulerRect.left + nBegin, nTick*i - m_scrollPos.y, rulerRect.right, 1, BLACKNESS);
			if (bShowPos)
				pDC->TextOut(rulerRect.left + 2, nTick*i - m_scrollPos.y - 5, CString(buffer));
		}
	}
}

void CRulerView::DrawCursorPos(CPoint NewPos)
{
	if (((m_rulerType == RT_HORIZONTAL) && (NewPos.x > m_DocSize.cx)) ||
		((m_rulerType == RT_VERTICAL) && ((NewPos.y) > m_DocSize.cy)))
		return;

	CDC* pDC = GetDC();
	// set the map mode right
	int oldMapMode = pDC->SetMapMode(MM_TEXT);

	CRect clientRect;
	GetClientRect(&clientRect);
	if (m_rulerType == RT_HORIZONTAL)
	{
		// erase the previous position
		pDC->PatBlt(m_lastPos.x, clientRect.top, 1, clientRect.bottom, DSTINVERT);
		// draw the new position
		m_lastPos.x = NewPos.x;
		pDC->PatBlt(m_lastPos.x, clientRect.top, 1, clientRect.bottom, DSTINVERT);
	}
	else if (m_rulerType==RT_VERTICAL)
	{
		// erase the previous position
		pDC->PatBlt(clientRect.left, m_lastPos.y, clientRect.right, 1, DSTINVERT);
		// draw the new position
		m_lastPos.y = NewPos.y;
		pDC->PatBlt(clientRect.left, m_lastPos.y, clientRect.right, 1, DSTINVERT);
	}

	pDC->SetMapMode(oldMapMode);
	ReleaseDC(pDC);
}

void CRulerView::DrawSelection(CRect Rect)
{
	if (((m_rulerType == RT_HORIZONTAL) && (Rect.left > m_DocSize.cx)) ||
		((m_rulerType == RT_VERTICAL) && ((Rect.top) > m_DocSize.cy)))
		return;

	CDC* pDC = GetDC();
	// set the map mode right
	int oldMapMode = pDC->SetMapMode(MM_TEXT);

	if (m_rulerType == RT_HORIZONTAL)
	{
		pDC->FillSolidRect(Rect.left - m_scrollPos.x, 10, Rect.right - Rect.left, 12, RGB(255, 150, 130));
	}
	else if (m_rulerType==RT_VERTICAL)
	{
		pDC->FillSolidRect(10, Rect.top - m_scrollPos.y, 12, Rect.bottom - Rect.top, RGB(255, 150, 130));
	}

	pDC->SetMapMode(oldMapMode);
	ReleaseDC(pDC);
}

/////////////////////////////////////////////////////////////////////////////
// CRulerView diagnostics

#ifdef _DEBUG
void CRulerView::AssertValid() const
{
	CView::AssertValid();
}

void CRulerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRulerView message handlers

BOOL CRulerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// create a brush using the same color as the background
	if (cs.lpszClass == NULL)
	{
		HBRUSH hBr = CreateSolidBrush(GetSysColor(COLOR_MENU));
		cs.lpszClass = AfxRegisterWndClass(CS_DBLCLKS | CS_BYTEALIGNWINDOW, NULL, hBr);
	}

	return CView::PreCreateWindow(cs);
}

void CRulerView::OnSetFocus(CWnd* pOldWnd)
{
	CView::OnSetFocus(pOldWnd);

	((CSplitterWnd*)GetParent())->SetActivePane(1, 1);
}

void CRulerView::OnInitialUpdate()
{
	CView::OnInitialUpdate();
}

/////////////////////////////////////////////////////////////////////////////
// CRulerCornerView

IMPLEMENT_DYNCREATE(CRulerCornerView, CView)

CRulerCornerView::CRulerCornerView()
{
}

CRulerCornerView::~CRulerCornerView()
{
}


BEGIN_MESSAGE_MAP(CRulerCornerView, CView)
	//{{AFX_MSG_MAP(CRulerCornerView)
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRulerCornerView drawing

void CRulerCornerView::OnDraw(CDC* pDC)
{
}

/////////////////////////////////////////////////////////////////////////////
// CRulerCornerView diagnostics

#ifdef _DEBUG
void CRulerCornerView::AssertValid() const
{
	CView::AssertValid();
}

void CRulerCornerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRulerCornerView message handlers

BOOL CRulerCornerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// create a brush using the same color as the background
	if (cs.lpszClass == NULL)
	{
		HBRUSH hBr = CreateSolidBrush(GetSysColor(COLOR_MENU));
		cs.lpszClass = AfxRegisterWndClass(CS_DBLCLKS | CS_BYTEALIGNWINDOW, NULL, hBr);
	}

	return CView::PreCreateWindow(cs);
}

void CRulerCornerView::OnSetFocus(CWnd* pOldWnd)
{
	CView::OnSetFocus(pOldWnd);

	((CSplitterWnd*)GetParent())->SetActivePane(1, 1);
}


void CRulerSplitterWnd::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	CSplitterWnd::OnLButtonDblClk(nFlags, point);
}

void CRulerView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	CView::OnLButtonDblClk(nFlags, point);
}
