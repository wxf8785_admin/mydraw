
#include "stdafx.h"
#include "Text.h"
#include "Ellipse.h"
#include "Line.h"
#include "Rectangle.h"
#include "BarCode.h"
#include "Pic.h"
#include "TBarCode11.h"

#include "PropertiesWnd.h"
#include "Resource.h"
#include "MainFrm.h"
#include "MyDrawView.h"
#include "MyDraw.h"
#include "CMFCPropertyGridFontPropertyEx.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/////////////////////////////////////////////////////////////////////////////
// CResourceViewBar

CPropertiesWnd::CPropertiesWnd()
{
	m_nComboHeight = 0;
	//graph = NULL;
}

CPropertiesWnd::~CPropertiesWnd()
{
}

BEGIN_MESSAGE_MAP(CPropertiesWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND(ID_EXPAND_ALL, OnExpandAllProperties)
	ON_UPDATE_COMMAND_UI(ID_EXPAND_ALL, OnUpdateExpandAllProperties)
	ON_COMMAND(ID_SORTPROPERTIES, OnSortProperties)
	ON_UPDATE_COMMAND_UI(ID_SORTPROPERTIES, OnUpdateSortProperties)
	ON_COMMAND(ID_PROPERTIES1, OnProperties1)
	ON_UPDATE_COMMAND_UI(ID_PROPERTIES1, OnUpdateProperties1)
	ON_COMMAND(ID_PROPERTIES2, OnProperties2)
	ON_UPDATE_COMMAND_UI(ID_PROPERTIES2, OnUpdateProperties2)
	ON_WM_SETFOCUS()
	ON_WM_SETTINGCHANGE()
	ON_REGISTERED_MESSAGE(AFX_WM_PROPERTY_CHANGED, OnPropertyChanged)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CResourceViewBar 消息处理程序

void CPropertiesWnd::AdjustLayout()
{
	if (GetSafeHwnd () == NULL || (AfxGetMainWnd() != NULL && AfxGetMainWnd()->IsIconic()))
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

	m_wndObjectCombo.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), m_nComboHeight, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top + m_nComboHeight, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndPropList.SetWindowPos(NULL, rectClient.left, rectClient.top + m_nComboHeight + cyTlb, rectClient.Width(), rectClient.Height() -(m_nComboHeight+cyTlb), SWP_NOACTIVATE | SWP_NOZORDER);
}




int CPropertiesWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 创建组合: 
	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST | WS_BORDER | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	if (!m_wndObjectCombo.Create(dwViewStyle, rectDummy, this, 1))
	{
		TRACE0("未能创建属性组合 \n");
		return -1;      // 未能创建
	}


	m_wndObjectCombo.AddString(_T("未选择"));
	m_wndObjectCombo.SetCurSel(0);

	CRect rectCombo;
	m_wndObjectCombo.GetClientRect (&rectCombo);

	m_nComboHeight = rectCombo.Height();

	if (!m_wndPropList.Create(WS_VISIBLE | WS_CHILD, rectDummy, this, 2))
	{
		TRACE0("未能创建属性网格\n");
		return -1;      // 未能创建
	}

	InitPropList();

	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_PROPERTIES);
	m_wndToolBar.LoadToolBar(IDR_PROPERTIES, 0, 0, TRUE /* 已锁定*/);
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(theApp.m_bHiColorIcons ? IDB_PROPERTIES_HC : IDR_PROPERTIES, 0, 0, TRUE /* 锁定*/);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

	// 所有命令将通过此控件路由，而不是通过主框架路由: 
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	AdjustLayout();
	return 0;
}

void CPropertiesWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

void CPropertiesWnd::OnExpandAllProperties()
{
	m_wndPropList.ExpandAll();
}

void CPropertiesWnd::OnUpdateExpandAllProperties(CCmdUI* /* pCmdUI */)
{
}

void CPropertiesWnd::OnSortProperties()
{
	m_wndPropList.SetAlphabeticMode(!m_wndPropList.IsAlphabeticMode());
}

void CPropertiesWnd::OnUpdateSortProperties(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_wndPropList.IsAlphabeticMode());
}

void CPropertiesWnd::OnProperties1()
{
	// TODO:  在此处添加命令处理程序代码
}

void CPropertiesWnd::OnUpdateProperties1(CCmdUI* /*pCmdUI*/)
{
	// TODO:  在此处添加命令更新 UI 处理程序代码
}

void CPropertiesWnd::OnProperties2()
{
	// TODO:  在此处添加命令处理程序代码
}

void CPropertiesWnd::OnUpdateProperties2(CCmdUI* /*pCmdUI*/)
{
	// TODO:  在此处添加命令更新 UI 处理程序代码
}

void CPropertiesWnd::InitPropList()
{
	SetPropListFont();

	m_wndPropList.EnableHeaderCtrl(FALSE);
	m_wndPropList.EnableDescriptionArea();
	m_wndPropList.SetVSDotNetLook();
	m_wndPropList.MarkModifiedProperties();


	CMFCPropertyGridProperty* pLocation = new CMFCPropertyGridProperty(_T("对象位置"));

	CMFCPropertyGridProperty* pProp1 = new CMFCPropertyGridProperty(_T("左"), _T("0.0"), _T("指定对象的左"), 0, NULL, NULL, _T("1234567890."));


	//CMFCPropertyGridProperty(const CString& strName, const COleVariant& varValue, LPCTSTR lpszDescr = NULL, DWORD_PTR dwData = 0,
	//	LPCTSTR lpszEditMask = NULL, LPCTSTR lpszEditTemplate = NULL, LPCTSTR lpszValidChars = NULL);

	pLocation->AddSubItem(pProp1);

	pProp1 = new CMFCPropertyGridProperty(_T("顶"), _T("0.0"), _T("指定对象的顶"), 0, NULL, NULL, _T("1234567890."));
	pLocation->AddSubItem(pProp1);

	pLocation->Expand(TRUE);
	m_wndPropList.AddProperty(pLocation);


	

	CMFCPropertyGridProperty* pSize = new CMFCPropertyGridProperty(_T("对象大小"));

	CMFCPropertyGridProperty* pProp = new CMFCPropertyGridProperty(_T("宽度"), _T("0.0"), _T("指定对象的宽度"), 0, NULL, NULL, _T("1234567890."));
	pSize->AddSubItem(pProp);

	pProp = new CMFCPropertyGridProperty(_T("高度"), _T("0.0"), _T("指定对象的高度"), 0, NULL, NULL, _T("1234567890."));
	pSize->AddSubItem(pProp);

	pSize->Expand(TRUE);
	m_wndPropList.AddProperty(pSize);

	CMFCPropertyGridProperty* pGroup2 = new CMFCPropertyGridProperty(_T("字体"));

	LOGFONT lf;
	CFont* font = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	font->GetLogFont(&lf);

	lstrcpy(lf.lfFaceName, _T("宋体, Arial"));

	pGroup2->AddSubItem(new CMFCPropertyGridFontPropertyEx(_T("字体"), lf, CF_EFFECTS | CF_SCREENFONTS, _T("指定对象的字体")));
	//pGroup2->AddSubItem(new CMFCPropertyGridProperty(_T("使用系统字体"), (_variant_t) true, _T("指定窗口使用“MS Shell Dlg”字体")));

	m_wndPropList.AddProperty(pGroup2);





	CMFCPropertyGridProperty* pGroup1 = new CMFCPropertyGridProperty(_T("数据库"));

	//pGroup1->AddSubItem(new CMFCPropertyGridProperty(_T("字段"), (_variant_t) false, _T("绑定字段")));

	CMFCPropertyGridProperty* pProp11 = new CMFCPropertyGridProperty(_T("字段"), _T(" "), _T("其中之一: “字段1”、“字段2”、“字段3”...."));
	pProp11->AddOption(_T(" "));
	//pProp11->AddOption(_T("字段1"));
	//pProp11->AddOption(_T("字段2"));
	pProp11->AllowEdit(FALSE);

	pGroup1->AddSubItem(pProp11);

	m_wndPropList.AddProperty(pGroup1);




	//CMFCPropertyGridProperty* pGroup3 = new CMFCPropertyGridProperty(_T("杂项"));
	//pProp = new CMFCPropertyGridProperty(_T("(名称)"), _T("应用程序"));
	//pProp->Enable(FALSE);
	//pGroup3->AddSubItem(pProp);

	//CMFCPropertyGridColorProperty* pColorProp = new CMFCPropertyGridColorProperty(_T("窗口颜色"), RGB(210, 192, 254), NULL, _T("指定默认的窗口颜色"));
	//pColorProp->EnableOtherButton(_T("其他..."));
	//pColorProp->EnableAutomaticButton(_T("默认"), ::GetSysColor(COLOR_3DFACE));
	//pGroup3->AddSubItem(pColorProp);

	//static const TCHAR szFilter[] = _T("图标文件(*.ico)|*.ico|所有文件(*.*)|*.*||");
	//pGroup3->AddSubItem(new CMFCPropertyGridFileProperty(_T("图标"), TRUE, _T(""), _T("ico"), 0, szFilter, _T("指定窗口图标")));

	//pGroup3->AddSubItem(new CMFCPropertyGridFileProperty(_T("文件夹"), _T("c:\\")));

	//m_wndPropList.AddProperty(pGroup3);

}

void CPropertiesWnd::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
	m_wndPropList.SetFocus();
}

void CPropertiesWnd::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CDockablePane::OnSettingChange(uFlags, lpszSection);
	SetPropListFont();
}

void CPropertiesWnd::SetPropListFont()
{
	::DeleteObject(m_fntPropList.Detach());

	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);

	NONCLIENTMETRICS info;
	info.cbSize = sizeof(info);

	afxGlobalData.GetNonClientMetrics(info);

	lf.lfHeight = info.lfMenuFont.lfHeight;
	lf.lfWeight = info.lfMenuFont.lfWeight;
	lf.lfItalic = info.lfMenuFont.lfItalic;

	m_fntPropList.CreateFontIndirect(&lf);

	m_wndPropList.SetFont(&m_fntPropList);
	m_wndObjectCombo.SetFont(&m_fntPropList);
}




LRESULT CPropertiesWnd::OnPropertyChanged(WPARAM wParam, LPARAM lParam)
{

	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMyDrawView *pView = (CMyDrawView *)pFrame->GetActiveView();

	CMFCPropertyGridProperty* pProp = (CMFCPropertyGridProperty*)lParam;
	CString sname = pProp->GetName();  //被改变的参数名
	COleVariant t = pProp->GetValue(); //改变之后的值
	//t = pProp->GetOriginalValue();  //改变之前的值
	

	//修改时先关闭预览，完之后再打开
	bool bIsEnablePrview = pView->m_bIsEnablePrview;
	if (bIsEnablePrview)
		pView->OnButtonPreview();

	if (sname == "字体")
	{
		//字体
		CMFCPropertyGridFontPropertyEx * pProp2 = (CMFCPropertyGridFontPropertyEx*)pProp;

		LOGFONT *lf = pProp2->GetLogFont();
		CGraph* graph;
		for (int i = 0; i < graphs.GetSize(); i++)
		{
			graph = graphs.GetAt(i);
			if (graph != NULL)
			{
				if (!graph->bIsSelected)
					continue;

				if (graph->IsKindOf(RUNTIME_CLASS(CText)))
				{
					CText* pText = (CText*)graph;
					if (pText != NULL)
					{

						if (sname == "字体")
							pText->MyFont = *lf;

						pText->bIsSelected = false;
						pView->Invalidate(FALSE);
						//pText->bIsSelected = true;
					}
				}

				if (graph->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
				{
					CTBarCode11* pTBarCode11 = (CTBarCode11*)graph;
					if (pTBarCode11 != NULL)
					{
						if (sname == "字体")
							pTBarCode11->TextFont = *lf;


						pTBarCode11->bIsSelected = false;
						pView->Invalidate(FALSE);
						//pTBarCode11->bIsSelected = true;

					}
				}

			}
		}
	}

	else if (sname == "字段")
	{
		CString Str = _com_util::ConvertBSTRToString(t.bstrVal);
		//字段
		CGraph* graph;
		for (int i = 0; i < graphs.GetSize(); i++)
		{
			graph = graphs.GetAt(i);
			if (graph != NULL)
			{
				if (!graph->bIsSelected)
					continue;

				if (graph->IsKindOf(RUNTIME_CLASS(CText)))
				{
					CText* pText = (CText*)graph;
					if (pText != NULL)
					{
						if (sname == "字段")
						{
							if (Str.Find("字段") >= 0)
							{
								int nCount = atoi(Str.Right(1));
								if (nCount > 0)
								{
									if (pText->nConstValueType == 0 && pText->sInsetSqlParam.GetLength() > 0)
									{
										pText->m_nFieldIndex = nCount;
										pText->sInsetSqlParam = pView->ResetFieldNum(pText->sInsetSqlParam, nCount);
									}
									else
									{
										pText->nConstValueType = 2;
										pText->m_nFieldIndex = nCount;
									}
								}
							}
						
						}
						pText->bIsSelected = false;
						pView->Invalidate(FALSE);
						//pText->bIsSelected = true;
					}
				}

				if (graph->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
				{
					CTBarCode11* pTBarCode11 = (CTBarCode11*)graph;
					if (pTBarCode11 != NULL)
					{
						if (sname == "字段")
						{
							if (Str.Find("字段") >= 0)
							{
								int nCount = atoi(Str.Right(1));
								if (nCount > 0)
								{
									if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->sInsetParam.GetLength() > 0)
									{
										pTBarCode11->m_nFieldIndex = nCount;
										pTBarCode11->m_nSqlTypeFieldIndex = nCount;
										pTBarCode11->sInsetParam = pView->ResetFieldNum(pTBarCode11->sInsetParam, nCount);
									}
									else
									{
										pTBarCode11->m_nBarPropertyType = 2;
										pTBarCode11->m_nFieldIndex = nCount;
										pTBarCode11->m_nSqlTypeFieldIndex = nCount;
									}
								}
							}
						}
						pTBarCode11->bIsSelected = false;
						pView->Invalidate(FALSE);
						//pTBarCode11->bIsSelected = true;
					}
				}

			}
		}
	}


	else
	{
		CGraph* graph;
		for (int i = 0; i < graphs.GetSize(); i++)
		{
			graph = graphs.GetAt(i);
			if (graph != NULL)
			{
				if (!graph->bIsSelected)
					continue;

				if (graph->IsKindOf(RUNTIME_CLASS(CRectangle)))
				{
					CRectangle* pRectangle = (CRectangle*)graph;
					if (pRectangle != NULL)
					{
						CString Str = _com_util::ConvertBSTRToString(t.bstrVal);
						double v = atof(Str);
						if (v < 0)
							return 0;

						if (sname == "左")
							pRectangle->startX = v * pView->ScaleX;
						if (sname == "顶")
							pRectangle->startY = v * pView->ScaleY;
						if (sname == "宽度")
							pRectangle->endX = pRectangle->startX + v * pView->ScaleX;
						if (sname == "高度")
							pRectangle->endY = pRectangle->startY + v * pView->ScaleY;

						pRectangle->bIsSelected = false;
						pView->Invalidate(FALSE);
						//pText->bIsSelected = true;
					}
				}

				if (graph->IsKindOf(RUNTIME_CLASS(CLine)))
				{
					CLine* pLine = (CLine*)graph;
					if (pLine != NULL)
					{
						CString Str = _com_util::ConvertBSTRToString(t.bstrVal);
						double v = atof(Str);
						if (v < 0)
							return 0;

						if (sname == "左")
							pLine->startX = v * pView->ScaleX;
						if (sname == "顶")
							pLine->startY = v * pView->ScaleY;
						if (sname == "宽度")
							pLine->endX = pLine->startX + v * pView->ScaleX;
						if (sname == "高度")
							pLine->endY = pLine->startY + v * pView->ScaleY;

						pLine->bIsSelected = false;
						pView->Invalidate(FALSE);
						//pText->bIsSelected = true;
					}
				}

				if (graph->IsKindOf(RUNTIME_CLASS(CText)))
				{
					CText* pText = (CText*)graph;
					if (pText != NULL)
					{
						CString Str = _com_util::ConvertBSTRToString(t.bstrVal);
						double v = atof(Str);
						if (v < 0)
							return 0;

						if (sname == "左")
							pText->startX = v * pView->ScaleX;
						if (sname == "顶")
							pText->startY = v * pView->ScaleY;
						if (sname == "宽度")
							pText->endX = pText->startX + v * pView->ScaleX;
						if (sname == "高度")
							pText->endY = pText->startY + v * pView->ScaleY;

						pText->tracker.m_rect.SetRect(pView->CoordZoomOut(pText->startX, 0), pView->CoordZoomOut(pText->startY, 1), pView->CoordZoomOut(pText->endX, 0), pView->CoordZoomOut(pText->endY, 1));

						pText->bIsSelected = false;
						pView->Invalidate(FALSE);
						//pText->bIsSelected = true;
					}
				}

				if (graph->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
				{
					CTBarCode11* pTBarCode11 = (CTBarCode11*)graph;
					if (pTBarCode11 != NULL)
					{
						CString Str = _com_util::ConvertBSTRToString(t.bstrVal);
						double v = atof(Str);
						if (v < 0)
							return 0;

						//调整选择框
						int nLen = pTBarCode11->endX - pTBarCode11->startX;
						int nHeight = pTBarCode11->endY - pTBarCode11->startY;

						if (sname == "左")
						{
							pTBarCode11->startX = v * pView->ScaleX;
							pTBarCode11->endX = pTBarCode11->startX + nLen;
						}
						if (sname == "顶")
						{
							pTBarCode11->startY = v * pView->ScaleY;
							pTBarCode11->endY = pTBarCode11->startY + nHeight;
						}
						if (sname == "宽度")
						{
							pTBarCode11->endX = pTBarCode11->startX + v * pView->ScaleX;
							if (pTBarCode11->GetCodeType() == 8)//eBC_QRCode
							{
								pTBarCode11->endY = pTBarCode11->startY + v * pView->ScaleY;
							}
						}
						if (sname == "高度")
						{
							if (pTBarCode11->GetCodeType() == 8)//eBC_QRCode
							{
								pTBarCode11->endX = pTBarCode11->startX + v * pView->ScaleX;
							}
							pTBarCode11->endY = pTBarCode11->startY + v * pView->ScaleY;
						}

						pTBarCode11->bIsSelected = false;
						pView->Invalidate(FALSE);
						//pTBarCode11->bIsSelected = true;

					}
				}

			}
		}

	}



	//修改时先关闭预览，完之后再打开
	if (bIsEnablePrview)
		pView->OnButtonPreview();

	return 0;
}
