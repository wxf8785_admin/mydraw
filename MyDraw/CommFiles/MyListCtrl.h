#pragma once
#include "afxcmn.h"

#define LINEMAX 2048 

class CMyListCtrl :
	public CListCtrl
{
public:
	int m_nRowHeight;
	int nDrawLine[LINEMAX];
public:
	void SetRowHeigt(int nHeight);
public:
	void OnSetDrawLineFlag(int nDrawFlag[],int nSize);
	BOOL isDrawLine(int nRow);

public:
	CMyListCtrl(void);
	~CMyListCtrl(void);
	void DrawItem(LPDRAWITEMSTRUCT lpDIS);
	void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	DECLARE_MESSAGE_MAP()
	
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnMove(int x, int y);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnNMClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
};
