// MyDrawDoc.cpp : implementation of the CMyDrawDoc class
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "MyDraw.h"
#endif

#include  <io.h>
#include  <stdio.h>
#include  <stdlib.h>

#include "MyDrawDoc.h"
#include "MyDrawView.h"
#include "LineProperties.h"
#include <propkey.h>
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyDrawDoc

IMPLEMENT_DYNCREATE(CMyDrawDoc, CDocument)

BEGIN_MESSAGE_MAP(CMyDrawDoc, CDocument)
//{{AFX_MSG_MAP(CMyDrawDoc)
// NOTE - the ClassWizard will add and remove mapping macros here.
//    DO NOT EDIT what you see in these blocks of generated code!
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyDrawDoc construction/destruction

CMyDrawDoc::CMyDrawDoc()
{
	// TODO: add one-time construction code here
	EnableAutomation();
	AfxOleLockApp();
}

CMyDrawDoc::~CMyDrawDoc()
{
	AfxOleUnlockApp();
}

BOOL CMyDrawDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)
	//CFrameWndEx *pMain = (CFrameWndEx *)AfxGetMainWnd();
	//// View
	//CMyDrawView *MyView = (CMyDrawView *)pMain->GetActiveView();
	//if (MyView != NULL)
	//{
	//	if (!(MyView->m_ObjectList).IsEmpty())
	//	{
	//		(MyView->m_ObjectList).RemoveAll();
	//	}
	//	MyView->OnButtonSelect();
	//}


	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMyDrawDoc serialization

void CMyDrawDoc::Serialize(CArchive& ar)
{
	POSITION pos;
	if (ar.IsStoring())
	{
		// TODO: add storing code here
		CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd();
		ar << pMain->m_nVersion;
		ar << pMain->sDBPathName;
		ar << pMain->pPrintDlg->m_sStartPage;



		///记录号是否使用分隔符
		bool isUseSep = pMain->pDBSettingDlg->isUseFieldSymbol();
		//////记录号分隔符assic
		int nRecordAssic = pMain->pDBSettingDlg->GetRecordFieldSymbol();
		////记录号固定字段数
		int nSepNum = pMain->pDBSettingDlg->GetSeprateNum();
		/////字段分段符assic//
		int nSepAssic = pMain->pDBSettingDlg->GetSeprateFieldSymbol();

		ar << isUseSep << nRecordAssic << nSepNum << nSepAssic;
		
		// View
		CMyDrawView *MyView = (CMyDrawView *)pMain->GetActiveView();
		if (MyView != NULL)
		{
			pos = MyView->m_ObjectList.GetHeadPosition();
			while (pos != NULL)
			{
				ar << (CGraph*)(MyView->m_ObjectList.GetNext(pos));
			}
		}

	}
	else
	{
		// TODO: add loading code here
		CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd();
		ar >> pMain->m_nVersion;
		ar >> pMain->sDBPathName;
		ar >> pMain->pPrintDlg->m_sStartPage;
		if (atoi(pMain->pPrintDlg->m_sStartPage) > 0)
			pMain->pPrintDlg->RefreshCtrlStartPage(atoi(pMain->pPrintDlg->m_sStartPage));


		///记录号是否使用分隔符
		bool isUseSep ;
		//////记录号分隔符assic
		int nRecordAssic ;
		////记录号固定字段数
		int nSepNum ;
		/////字段分段符assic//
		int nSepAssic ;

		ar >> isUseSep >> nRecordAssic >> nSepNum >> nSepAssic;

		///记录号是否使用分隔符
		pMain->pDBSettingDlg->SetUseFieldSymbol(isUseSep);
		//////记录号分隔符assic
		pMain->pDBSettingDlg->SetRecordFieldSymbol(nRecordAssic);
		////记录号固定字段数
		pMain->pDBSettingDlg->SetSeprateNum(nSepNum);
		/////字段分段符assic//
		pMain->pDBSettingDlg->SetSeprateFieldSymbol(nSepAssic);

		

		// View
		CMyDrawView *MyView = (CMyDrawView *)pMain->GetActiveView();
		if (MyView != NULL)
		{
			if (!(MyView->m_ObjectList).IsEmpty())
			{
				(MyView->m_ObjectList).RemoveAll();
			}
			CObject * pObject;
			do{
				try
				{
					ar>>pObject;
					MyView->m_ObjectList.AddTail(pObject);
				}
				catch (CException * e)
				{
					e->Delete();
					break;
				}
			} while (pObject != NULL);
		}

		if ((_access(pMain->sDBPathName, 0)) != -1)
		{
			pMain->ReadPrintData(pMain->sDBPathName);
		}
		else
		{
			if (!pMain->sDBPathName.IsEmpty() && pMain->sDBPathName != _T("未加载数据库"))
				AfxMessageBox(_T("数据库文件不存在，请核实...."));
		}

	}
}


#ifdef SHARED_HANDLERS

// 缩略图的支持
void CMyDrawDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// 修改此代码以绘制文档数据
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
	fontDraw.DeleteObject();
}

// 搜索处理程序的支持
void CMyDrawDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// 从文档数据设置搜索内容。
	// 内容部分应由“;”分隔

	// 例如:     strSearchContent = _T("point;rectangle;circle;ole object;")；
	SetSearchContent(strSearchContent);
}

void CMyDrawDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// CMyDrawDoc 诊断

/////////////////////////////////////////////////////////////////////////////
// CMyDrawDoc diagnostics

#ifdef _DEBUG
void CMyDrawDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMyDrawDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMyDrawDoc commands

BOOL CMyDrawDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	OnNewDocument();

	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;

	// TODO: Add your specialized creation code here
	//OFSTRUCT of;
	//HANDLE hFile = NULL;;
	//hFile = (HANDLE)OpenFile(lpszPathName, &of, OF_READ | OF_SHARE_COMPAT);
	//if (hFile != (HANDLE)HFILE_ERROR)
	//{
	//	DWORD dwHighWord = NULL;
	//	DWORD dwSizeLow = GetFileSize(hFile, &dwHighWord);
	//	DWORD dwFileSize = dwSizeLow;
	//	if (0 == dwFileSize)
	//	{
	//		CFrameWndEx *pMain = (CFrameWndEx *)AfxGetMainWnd();
	//		// View
	//		CMyDrawView *MyView = (CMyDrawView *)pMain->GetActiveView();
	//		if (MyView != NULL)
	//		{
	//			if (!(MyView->m_ObjectList).IsEmpty())
	//			{
	//				(MyView->m_ObjectList).RemoveAll();
	//			}
	//		}
	//	}
	//}


	CFrameWndEx *pMain = (CFrameWndEx *)AfxGetMainWnd();
	// View
	CMyDrawView *MyView = (CMyDrawView *)pMain->GetActiveView();
	if (MyView != NULL)
	{
		MyView->OnButtonSelect();
	}


	return TRUE;
}
