// QuietZoon.cpp : 实现文件
//

#include "stdafx.h"
#include "MyDraw.h"
#include "QuietZoon.h"
#include "afxdialogex.h"


// CQuietZoon 对话框

IMPLEMENT_DYNAMIC(CQuietZoon, CDialogEx)

CQuietZoon::CQuietZoon(CWnd* pParent /*=NULL*/)
	: CDialogEx(CQuietZoon::IDD, pParent)
	, m_nTop(1)
	, m_nBottom(1)
	, m_nRight(1)
	, m_nLeft(1)
	, m_QietZoonUnit(0)
{

}

CQuietZoon::~CQuietZoon()
{
}

void CQuietZoon::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_comboxUnit);
	DDX_Text(pDX, IDC_EDIT1, m_nTop);
	DDV_MinMaxInt(pDX, m_nTop, 0, 100);
	DDX_Text(pDX, IDC_EDIT2, m_nBottom);
	DDV_MinMaxInt(pDX, m_nBottom, 0, 100);
	DDX_Text(pDX, IDC_EDIT3, m_nRight);
	DDX_Text(pDX, IDC_EDIT4, m_nLeft);
	DDV_MinMaxInt(pDX, m_nLeft, 0, 100);
	DDV_MinMaxInt(pDX, m_nRight, 0, 100);
	DDX_CBIndex(pDX, IDC_COMBO1, m_QietZoonUnit);
}


BEGIN_MESSAGE_MAP(CQuietZoon, CDialogEx)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CQuietZoon 消息处理程序


BOOL CQuietZoon::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_comboxUnit.AddString("No Quiet Zoon");
	m_comboxUnit.AddString("Pixel");
	m_comboxUnit.AddString("mm");
	m_comboxUnit.AddString("Mils");
	m_comboxUnit.AddString("Inch");
	m_comboxUnit.AddString("Modules");
	m_comboxUnit.AddString("Percent");
	m_comboxUnit.SetCurSel(m_QietZoonUnit);
	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子  
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


HBRUSH CQuietZoon::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
