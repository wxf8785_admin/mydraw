#include "stdafx.h"
#include <time.h>  
#include <iostream> 
#include "Timer.h"   
using namespace std;   
CTimer::CTimer():m_Elapse(0), m_hThread(NULL)  
{   
}   
CTimer::~CTimer()   
{   
}   
void CTimer::StartTimer(unsigned int nElapse)    
{    
	m_Elapse = nElapse;   
	m_hThread = CreateThread(NULL, 0, ThreadFunc, (LPVOID)(&m_Elapse), 0, NULL);  
}   
void CTimer::EndTimer()  
{    
	CloseHandle(m_hThread); 
}   


DWORD WINAPI CTimer::ThreadFunc(LPVOID pParam)   
{    
	time_t t1, t2; 
	double  Diff = 0;   
	int elapse = *((int *)pParam);  
	/*获取系统当前时间*/    
	t1 = time(NULL);  
	while(1)   
	{    
		/*以秒为单位获取系统当前时间*/   
		t2 = time(NULL);     
		/*比较第二次获取的时间与第一次的时间是不是间隔了两秒*/   
		Diff = difftime(t2,t1);    
		/*间隔两秒打印Diff和i*/      
		if((int)Diff == elapse)    
		{      
			cout<<"Time out!"<<endl;    
			t1 = t2;     
		}        
	}   
	return 0;  
}

