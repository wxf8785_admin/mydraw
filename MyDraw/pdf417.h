
#include "pdf417-imp.h"

#define PDF417_USE_ASPECT_RATIO			0	//- use aspectRatio to set the y/x dimension. Also uses yHeight
#define PDF417_FIXED_RECTANGLE			1	//- make the barcode dimensions at least codeColumns by codeRows
#define PDF417_FIXED_COLUMNS			2	//- make the barcode dimensions at least codeColumns
#define PDF417_FIXED_ROWS				4	//- make the barcode dimensions at least codeRows
#define PDF417_AUTO_ERROR_LEVEL			0	//- automatic error level depending on text size
#define PDF417_USE_ERROR_LEVEL			16	// - the error level is errorLevel. The used errorLevel may be different
#define PDF417_USE_RAW_CODEWORDS		64	//- use codewords instead of text
#define PDF417_INVERT_BITMAP			128	//- invert the resulting bitmap	
// pdf417错误信息
enum PDF417_ERROR{
	PDF417_ERROR_SUCCESS = 0,
	PDF417_ERROR_TEXT_TOO_BIG = 1,
	PDF417_ERROR_INVALID_PARAMS = 2,
};

typedef struct _PDF417PARAM
{
	char *outBits;      // 图像数据
	int lenBits;        // outBits的长度
	int bitColumns;     // 图像的每1行的bit位数
	int codeRows;       // 图像的行数
	int codeColumns;    // 编码的列数
	int codewords[928]; // the code words may be input. Is always output
	int lenCodewords;   // 码字长度
	int errorLevel;     // 纠错等级0-8
	char *text;         // 编码文本
	int lenText;        // 编码文本的长度。传入-1,自动获取长度
	int options;        // 编码设置
	float aspectRatio;  // y/x 的比值
	float yHeight;      // the y/x dot ratio
	int error;          // 返回错误信息
} PDF417PARAM, *PPDF417PARAM;

void paintCode	(PPDF417PARAM param);
void pdf417init	(PPDF417PARAM param);
void pdf417free	(PPDF417PARAM param);


