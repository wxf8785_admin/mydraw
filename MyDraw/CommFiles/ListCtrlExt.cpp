/////////////////////////////////////////////////////////////////////////////////////////////////////////
////**************************************************************************************************
/*           版权所有(2004):  武汉豪迈电力自动化有限责任公司                   
             程序开发人：     石旭刚
			 开发时间：       2004年10月
////***********************************************************************************************///
// shilistctrl.cpp : implementation file
//

#include "../stdafx.h"
#include "../MainFrm.h"
#include "ListCtrlExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define IDC_PROPEDIT     888
#define IDC_PROPCMBBOX   712

/////////////////////////////////////////////////////////////////////////////
// CShiListCtrl

CListCtrlExt::CListCtrlExt()
{
	//初试化列属性
	for (int i=0; i<POS_MAX; i++)
	{
		m_ColProperty[i].fMax = 0;
		m_ColProperty[i].fMin = 0;
		m_ColProperty[i].nDecimals = 0;
	}
	m_Song9Font.CreatePointFont(120,_T("宋体"));//FONT 9, "MS Sans Serif"
	m_bUnit = TRUE;
	m_bSelectAll = FALSE;
	 nOnlyReadCols = 0;
	 nColCount = 0;
	 nOnlyReadPoses = 0;
	 nFixedCount = 0;
	 nBtnCount = 0;
	 nCmbBoxCount = 0;
	 nRedCount = 0;
	 nCmbCols = 0;

	 nCompareDifferCount = 0;
	 nMaxRow = POS_MAX*3;
	 nMaxCol = 10;
	 nCmbListRow = -1;
	 nCmbListCol = -1;

	 for (int i=0;i<nMaxRow;i++)
	 {
		 //for(int j=0;j<nMaxCol;j++)
		 {
			 nRedItemValue[i]/*[j]*/ = 0;
			 nRedItemValue2[i]/*[j]*/ = 0;
		 }
	 }
}

CListCtrlExt::~CListCtrlExt()
{
	m_Song9Font.DeleteObject();
	m_fntText.DeleteObject();
}


BEGIN_MESSAGE_MAP(CListCtrlExt, CListCtrl)
	//{{AFX_MSG_MAP(CShiListCtrl)
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT_EX(LVN_ENDLABELEDIT, OnEndlabeledit)
	//}}AFX_MSG_MAP
	ON_NOTIFY_REFLECT_EX(NM_CUSTOMDRAW, OnNMCustomDraw)
	ON_CBN_KILLFOCUS(IDC_PROPCMBBOX, OnKillfocusCmbBox)
	ON_CBN_SELCHANGE(IDC_PROPCMBBOX, OnSelchangeCmbBox)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CShiListCtrl message handlers

void CListCtrlExt::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CListCtrl::OnLButtonDown(nFlags, point);
	
	int index;
	int colnum; 
	if( ( index = HitTestex( point, &colnum )) != -1 )
	{
		m_curSel = index;
		m_CurCol = colnum;
		

		if (IsFixedCol(colnum)) return;
		if (IsOnlyReadCol(colnum)) return;
		if (IsOnlyReadPos(index,colnum)) return;
		if (IsButtonPos(index,colnum)) return;
		if (IsFixedPos(index,colnum)) return;
		if (IsCmbBoxPos(index,colnum))
		{	
			CmbBoxLable(index, colnum);
			nCmbListRow = index;
			nCmbListCol =colnum; 
			return;
		}
		else if (IsCmbCol(colnum))
		{
			CmbBoxLable(index,colnum,-1);
			nCmbListRow = index;
			nCmbListCol =colnum; 
			return;
		}
		EditSubLabel( index, colnum );
		/*
		UINT flag = LVIS_FOCUSED;
		if( (GetItemState( index, flag ) & flag) == flag && colnum > 0)
		{
			// add check for lvs_editlabels
			if( GetWindowLong(m_hWnd, GWL_STYLE)  )//& LVS_EDITLABELS
				EditSubLabel( index, colnum );
		}
		else
		{
			
			SetItemState( index, LVIS_SELECTED | LVIS_FOCUSED ,
				    	LVIS_SELECTED | LVIS_FOCUSED);
			
//			this->EditLabel(index);
		}
		*/
	}
}
/************************************************************************/
/* 功能说明： 产生下拉列表框
/************************************************************************/
void CListCtrlExt::CmbBoxLable( int nItem, int nCol,int nType)
{

	CRect rect;
	//GetItemRect( nItem, &rect, LVIR_BOUNDS);
	GetSubItemRect(nItem,nCol,LVIR_BOUNDS,rect);
	CString SelText;
	
	//CPropertyItem* pItem = (CPropertyItem*)m_cmbBox.GetItemDataPtr(nItem);
	if (m_cmbBox)
		m_cmbBox.MoveWindow(rect);
	else
	{	
		rect.bottom += 540;//改变可修改combobox下拉菜单高度
		/*m_cmbBox.Create(CBS_DROPDOWNLIST | CBS_NOINTEGRALHEIGHT | WS_VISIBLE | WS_CHILD | WS_BORDER,
			rect,this,IDC_PROPCMBBOX);*/
		m_cmbBox.Create(CBS_DROPDOWN | WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_HASSTRINGS,rect,this,IDC_PROPCMBBOX);
		m_cmbBox.SetFont(&m_Song9Font);
	}
	
	//add the choices for this particular property
	//CString cmbItems = pItem->m_cmbItems;
	SelText = GetItemText( nItem, nCol );
	
	m_cmbBox.ResetContent();
	//m_cmbBox.AddString("");		
	int i=0,i2=0;
	int nstr = nItem;
	if(nItem==0&&nCol==4)nstr=0;
	if(nItem==3&&nCol==4)nstr=1;
	if(nItem==4&&nCol==4)nstr=2;
	if(nItem==5&&nCol==4)nstr=3;
	if (nType == -1) nstr = nCol;
	int nCount =0;
//	m_cmbBox.ClearColor();
	while ((i2=m_CmbItemsStr[nstr].Find('|',i)) != -1)
	{
		CString tmp =m_CmbItemsStr[nstr].Mid(i,i2-i);
		if (tmp.Find('&') >-1)
		{
			//AfxExtractSubString(name,(LPCTSTR)tmp,m,'&');
 			CString name = "";
 			AfxExtractSubString(name,(LPCTSTR)tmp,0,'&');
 			m_cmbBox.AddString(name);
 			CString sIndex = "0";
 			AfxExtractSubString(sIndex,(LPCTSTR)tmp,1,'&');
 			m_cmbBox.SetColor(nCount,atoi(sIndex));
 			m_cmbBox.SetCurSel(1);
		}
		else
		{
			CString tmpName = m_CmbItemsStr[nstr].Mid(i,i2-i);
			m_cmbBox.AddString(tmpName);
		}
//		m_cmbBox.AddString(m_CmbItemsStr[nstr].Mid(i,i2-i));
		i=i2+1;
		nCount++;
	}
	m_cmbBox.ShowWindow(SW_SHOW);
	m_cmbBox.SetFocus();
	
	//jump to the property's current value in the combo box
	int j=0;
	CString text;
	i=0;i2=0;
	j = m_CmbItemsStr[nstr].Find(SelText);///4
	text = m_CmbItemsStr[nstr].Mid(0,j);
	j=0;
	while ((i2=text.Find('|',i)) != -1)
	{
		j++;
		i=i2+1;
	}
	
	if (j != CB_ERR)
		m_cmbBox.SetCurSel(j);
	else
		m_cmbBox.SetCurSel(0);
}


void CListCtrlExt::onsetCmbCursel(int nRow)
{
	m_cmbBox.SetCurSel(nRow);
}


// void CListCtrlExt::onSetComBoxColor(int nRow,int nColor)
// {
// 	m_cmbBox.SetColor(nRow,nColor);
// }

/************************************************************************/
/* 功能说明： 下拉列表框选择处理
/************************************************************************/
void CListCtrlExt::OnSelchangeCmbBox()
{
	CString selStr;
	if (m_cmbBox)
	{
		m_cmbBox.GetLBText(m_cmbBox.GetCurSel(),selStr);
		SetItemText(m_curSel,m_CurCol,selStr);
		GetParent()->SendMessage(USER_CMB_MSG,(WPARAM)nCmbListRow,(LPARAM)nCmbListCol);
	}
//	GetMain()->SetListUnits();
	Invalidate();
}

void CListCtrlExt::OnKillfocusCmbBox() 
{
	m_cmbBox.ShowWindow(SW_HIDE);
//	GetMain()->SetListUnits();
	Invalidate();
}
/******************************************************************************
*	功能说明：	回车处理
******************************************************************************/
BOOL CListCtrlExt::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN )
	{
		if(pMsg->wParam == VK_RETURN)
		{
			if (m_cmbBox)
			{
				m_cmbBox.ShowWindow(SW_HIDE);
			}
			::TranslateMessage(pMsg);
			::DispatchMessage(pMsg);
				
			return TRUE;		    	// do not process further
		}
		else if (pMsg->wParam == VK_F9)
		{
			GetParent()->PostMessage(pMsg->message,pMsg->wParam,0);
			return TRUE;
		}
// 		else if  (pMsg->wParam >= 0x31 &&  pMsg->wParam <= 0x38 && GetMain()->GetCurTestIndex()==7)
// 		{
// 			GetMain()->PostMessage(pMsg->message,pMsg->wParam,0);
// 			return TRUE;
// 		}
	}
	
	return CListCtrl::PreTranslateMessage(pMsg);
}
/************************************************************************/
/* 功能说明： 编辑框处理
/************************************************************************/
int CListCtrlExt::HitTestex(CPoint &point, int *col) const
{
	int colnum = 0;
	int row = HitTest( point, NULL );
	
	if( col ) *col = 0;
	// make sure that the listview is in lvs_report
	if( (GetWindowLong(m_hWnd, GWL_STYLE) & LVS_TYPEMASK) != LVS_REPORT )
		return row;

	// get the top and bottom row visible
	row = GetTopIndex();
	int bottom = row + GetCountPerPage();
	if( bottom > GetItemCount() )
		bottom = GetItemCount();
	
	// get the number of columns
	CHeaderCtrl * pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColumnCount = pHeader->GetItemCount();

	// loop through the visible rows
	for( ;row <=bottom;row++)
	{
		// get bounding rect of item and check whether point falls in it.
		CRect rect; 
		GetItemRect( row, &rect, LVIR_BOUNDS );
		if( rect.PtInRect(point) )
		{ // now find the column
			for( colnum=0; colnum < nColumnCount; colnum++ )
			{
				int colwidth = GetColumnWidth(colnum); 
					if( point.x>= rect.left && point.x <= (rect.left + colwidth ) )
					{
						if( col )*col=colnum; 
							return row;
					}
					rect.left += colwidth;
			}
		}
	}
	return 1;
}

CSubItemEdit* CListCtrlExt::EditSubLabel(int nItem, int nCol)
{
	if( !EnsureVisible( nItem, true ) )
		return NULL;

	// make sure that nCol is valid
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColumncount = pHeader->GetItemCount();
	if( nCol >= nColumncount || GetColumnWidth(nCol) < 5 )
		return NULL;

	// get the column offset
	int offset = 0;
	for( int i = 0; i < nCol; i++ )
		offset += GetColumnWidth( i );

	CRect rect;
	GetItemRect( nItem, &rect, LVIR_BOUNDS );
	//GetItemRect( nItem, &rect, LVIR_LABEL);

	// now scroll if we need to expose the column
	CRect rcclient;
	GetClientRect( &rcclient );
	if( offset + rect.left < 0 || offset + rect.left > rcclient.right )
	{
		CSize size;

		size.cx = offset + rect.left;
		size.cy = 0;
		Scroll( size );
		rect.left -= size.cx;
	}

	// get column alignment
	LV_COLUMN lvcol;
	lvcol.mask = LVCF_FMT;
	GetColumn( nCol, &lvcol );
	DWORD dwstyle ;

	if((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)
		dwstyle = ES_LEFT;
	else if((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT)
		dwstyle = ES_RIGHT;
	else
		dwstyle = ES_CENTER;

	//rect.left += offset+4;
	rect.top += -2;
	rect.left += offset;
	//rect.right = rect.left + GetColumnWidth( nCol ) - 3 ;
	rect.right = rect.left + GetColumnWidth( nCol );
	if( rect.right > rcclient.right)
		rect.right = rcclient.right;

	dwstyle |= WS_BORDER|WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|ES_MULTILINE;
	CSubItemEdit *pEdit = new CSubItemEdit(nItem, nCol, GetItemText( nItem, nCol ));
	pEdit->Create( dwstyle, rect, this, IDC_PROPEDIT );

	return pEdit;

}

BOOL CListCtrlExt::OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult)
{
	
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	// TODO: Add your control notification handler code here
	LV_ITEM	* plvitem = &( pDispInfo->item );

	CString csSubItemTextOld;
	CString csFormat;
	float fSubItemText;
	CString csSubItemText;
	if (plvitem->pszText != NULL)
	{
		if (m_ColProperty[plvitem->iSubItem].fMax == m_ColProperty[plvitem->iSubItem].fMin)
		{
			csSubItemText = plvitem->pszText;
		}
		else
		{
			csSubItemTextOld = GetItemText(plvitem->iItem, plvitem->iSubItem);
			fSubItemText= (float) atof(plvitem->pszText);
			if (fSubItemText > m_ColProperty[plvitem->iSubItem].fMax)
				fSubItemText = m_ColProperty[plvitem->iSubItem].fMax;
			if (fSubItemText < m_ColProperty[plvitem->iSubItem].fMin)
				fSubItemText = m_ColProperty[plvitem->iSubItem].fMin;
			csFormat.Format(".%d", m_ColProperty[plvitem->iSubItem].nDecimals);
			csFormat = "%" + csFormat;
			csFormat += "f";
			csSubItemText.Format(csFormat, fSubItemText);
		}
		SetItemText( plvitem->iItem, plvitem->iSubItem,csSubItemText);
	};
	*pResult = 0;
	return FALSE;//返回false让父类可以继续处理这个消息;
}
/************************************************************************/
/* 功能说明： 设置列表中的只读位置
/************************************************************************/
void CListCtrlExt::SetOnlyReadPos(int arrRow[],int arrCol[], int nCount)
{
	memset(nItem, 0, sizeof(nItem)/sizeof(nItem[0]));
	memset(nSubItem, 0, sizeof(nSubItem)/sizeof(nSubItem[0]));
	for(int i=0;i<nCount;i++)
	{
		nItem[i]    = arrRow[i];
		nSubItem[i] = arrCol[i];	
	}
	nOnlyReadPoses = nCount;
	return;
}
/************************************************************************/
/* 功能说明： 判断是否只读位置
/************************************************************************/
BOOL CListCtrlExt::IsOnlyReadPos(int nRow,int nCol)
{
	BOOL bFound = FALSE;
	for (int i=0; i<nOnlyReadPoses; i++)
	{
		if (nItem[i] == nRow && nSubItem[i] == nCol) 
		{
			bFound = TRUE;
			break;
		}
	}
	return bFound;	
}
/************************************************************************/
/* 功能说明： 设置列表中的按钮位置
/************************************************************************/
void CListCtrlExt::SetButtonPos(int arrRow[],int arrCol[], int nCount)
{
	memset(BtnItem, 0, sizeof(BtnItem)/sizeof(BtnItem[0]));
	memset(BtnSubItem, 0, sizeof(BtnSubItem)/sizeof(BtnSubItem[0]));
	for(int i=0;i<nCount;i++)
	{
		BtnItem[i]    = arrRow[i];
		BtnSubItem[i] = arrCol[i];	
	}
	nBtnCount = nCount;
	return;
}
/************************************************************************/
/* 功能说明： 判断是否按钮位置
/************************************************************************/
BOOL CListCtrlExt::IsButtonPos(int nRow,int nCol)
{
	BOOL bFound = FALSE;
	for (int i=0; i<nBtnCount; i++)
	{
		if (BtnItem[i] == nRow && BtnSubItem[i] == nCol) 
		{
			bFound = TRUE;
			break;
		}
	}
	return bFound;	
}
/************************************************************************/
/* 功能说明： 设置列表中的固定列
/************************************************************************/
void CListCtrlExt::SetFixedCol(int arrColIndex[], int nSize)
{
	memset(arrCols, 0, sizeof(arrCols)/sizeof(arrCols[0]));
	for (int i=0; i<nSize; i++)
	{
		arrCols[i] = arrColIndex[i];
	}
	nColCount = nSize;
	return;
}
/************************************************************************/
/* 功能说明： 判断给定的列是否固定列
/************************************************************************/
BOOL CListCtrlExt::IsFixedCol(int nCol)
{
	BOOL bFound = FALSE;
	for (int i=0; i<nColCount; i++)
	{
		if (arrCols[i] == nCol) 
		{
			bFound = TRUE;
			break;
		}
	}
	return bFound;
}

void CListCtrlExt::SetCmbCol(int arrColIndex[], int nSize)
{
	memset(arrCmbCol, 0, sizeof(arrCmbCol)/sizeof(arrCmbCol[0]));
	for (int i=0; i<nSize; i++)
	{
		arrCmbCol[i] = arrColIndex[i];
	}
	nCmbCols = nSize;
	return;
}
/************************************************************************/
/* 功能说明： 判断给定的列是否固定列
/************************************************************************/
BOOL CListCtrlExt::IsCmbCol(int nCol)
{
	BOOL bFound = FALSE;
	for (int i=0; i<nCmbCols; i++)
	{
		if (arrCmbCol[i] == nCol) 
		{
			bFound = TRUE;
			break;
		}
	}
	return bFound;
}
/************************************************************************/
/* 功能说明： 设置只读列
/************************************************************************/
void CListCtrlExt::SetOnlyReadCol(int arrCol[], int nCount)
{
	memset(arrOnlyReadCol, 0, sizeof(arrOnlyReadCol)/sizeof(arrOnlyReadCol[0]));
	for (int i=0; i<nCount; i++)
	{
		arrOnlyReadCol[i] = arrCol[i];
	}
	nOnlyReadCols = nCount;
	return;
}
/************************************************************************/
/* 功能说明： 判断是否只读列
/************************************************************************/
BOOL CListCtrlExt::IsOnlyReadCol(int nCol)
{
	BOOL bFound = FALSE;
	for (int i=0; i<nOnlyReadCols; i++)
	{
		if (arrOnlyReadCol[i] == nCol) 
		{
			bFound = TRUE;
			break;
		}
	}
	return bFound;	
}
/************************************************************************/
/* 功能说明： 设置下拉列表位置
/************************************************************************/
void CListCtrlExt::SetCmbBoxPos(int arrRow[],int arrCol[], int nCount)
{
	memset(CmbBoxItem, 0, sizeof(CmbBoxItem)/sizeof(CmbBoxItem[0]));
	memset(CmbBoxSubItem, 0, sizeof(CmbBoxSubItem)/sizeof(CmbBoxSubItem[0]));
	for(int i=0;i<nCount;i++)
	{
		CmbBoxItem[i]    = arrRow[i];
		CmbBoxSubItem[i] = arrCol[i];	
	}
	nCmbBoxCount = nCount;
	return;
}
/************************************************************************/
/* 功能说明： 判断是否下拉列表位置
/************************************************************************/
BOOL CListCtrlExt::IsCmbBoxPos(int nRow,int nCol)
{
	BOOL bFound = FALSE;
	for (int i=0; i<nCmbBoxCount; i++)
	{
		if (CmbBoxItem[i] == nRow && CmbBoxSubItem[i] == nCol) 
		{
			bFound = TRUE;
			break;
		}
	}
	return bFound;
}

void CListCtrlExt::SetCompareDifferRow(int nRow[],int nCount)//设置着色行
{
	memset(nCompareDifferRow,-1,sizeof(nCompareDifferRow)/sizeof(nCompareDifferRow[0]));

	for(int i=0;i<nCount;i++)
	{
		nCompareDifferRow[i] = nRow[i];
	}
	nCompareDifferCount = nCount;
	return;
}
BOOL CListCtrlExt::IsCompareDifferRow(int nRow)			//判断着色行
{
	BOOL bFlag = FALSE;

	for(int i=0;i<nCompareDifferCount;i++)
	{
		if(nRow == nCompareDifferRow[i])
		{
			bFlag = TRUE;
			break;
		}
	}
	return bFlag;
/*	return false;*/
}
/************************************************************************/
/* 功能说明： 设置固定列表位置
/************************************************************************/
void CListCtrlExt::SetFixedPos(int arrRow[],int arrCol[], int nCount)
{
	memset(FixedItem, 0, sizeof(FixedItem)/sizeof(FixedItem[0]));
	memset(FixedSubItem, 0, sizeof(FixedSubItem)/sizeof(FixedSubItem[0]));
	for(int i=0;i<nCount;i++)
	{
		FixedItem[i]    = arrRow[i];
		FixedSubItem[i] = arrCol[i];	
	}
	nFixedCount = nCount;
	return;
}
/************************************************************************/
/* 功能说明： 判断是否固定列表位置
/************************************************************************/
BOOL CListCtrlExt::IsFixedPos(int nRow,int nCol)
{
	BOOL bFound = FALSE;
	for (int i=0; i<nFixedCount; i++)
	{
		if (FixedItem[i] == nRow && FixedSubItem[i] == nCol) 
		{
			bFound = TRUE;
			break;
		}
	}
	return bFound;
}



////////////////////////////////////////////////////////
int CListCtrlExt::IsRedOrangePos(int nRow/*,int nCol*/)
{


	if (nRow > nMaxRow /*|| nCol > nMaxCol*/)
	{
		return 0;
	}
	if (nRedItemValue[nRow]/*[nCol]*/ == 1)//red
	{
		return 1;
	}
	if (nRedItemValue2[nRow]/*[nCol]*/ == 2)//orange
	{
		return 2;
	}
	return 0;

}

void CListCtrlExt::SetRedOrangePos(int nRow/*,int nCol*/,int nItem,int nItem2)
{
	if (nRow>nMaxRow)
	{
		nRow = nMaxRow;
	}
	/*if (nCol>nMaxCol)
	{
		nCol = nMaxCol;
	}*/
	nRedItemValue[nRow]/*[nCol]*/ = nItem;


	nRedItemValue2[nRow]/*[nCol]*/ = nItem2;
}





bool CListCtrlExt::SetTextFont(LONG nHeight,bool bBold,bool bItalic,const CString& sFaceName)
{
	LOGFONT lgfnt;
	
	lgfnt.lfHeight			= -MulDiv(nHeight, GetDeviceCaps(GetDC()->m_hDC, LOGPIXELSY),72);    
	lgfnt.lfWidth			= 0; 
	lgfnt.lfEscapement		= 0;    
	lgfnt.lfOrientation		= 0;    
	lgfnt.lfWeight			= bBold?FW_BOLD:FW_DONTCARE; 
	lgfnt.lfItalic			= bItalic?TRUE:FALSE;    
	lgfnt.lfUnderline		= FALSE;    
	lgfnt.lfStrikeOut		= FALSE;    
	lgfnt.lfCharSet			= DEFAULT_CHARSET; 
	lgfnt.lfOutPrecision	= OUT_DEFAULT_PRECIS;    
	lgfnt.lfClipPrecision	= CLIP_DEFAULT_PRECIS;    
	lgfnt.lfQuality			= DEFAULT_QUALITY; 
	lgfnt.lfPitchAndFamily	= DEFAULT_PITCH | FF_DONTCARE;    
	strcpy(lgfnt.lfFaceName, sFaceName);
	
	return SetTextFont(lgfnt);
}

bool CListCtrlExt::SetTextFont(const LOGFONT& lgfnt)
{
	m_fntText.DeleteObject();
	m_fntText.CreateFontIndirect(&lgfnt);
	SetFont(&m_fntText, TRUE);
	
	return true;
}



/************************************************************************/
/* 功能说明： 列表颜色绘制处理
/************************************************************************/
BOOL CListCtrlExt::OnNMCustomDraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	 //for this notification, the structure is actually a
	 // NMLVCUSTOMDRAW that tells you what's going on with the custom
	 // draw action. So, we'll need to cast the generic pNMHDR pointer.
	 LPNMLVCUSTOMDRAW  lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	 switch(lplvcd->nmcd.dwDrawStage)
	 {
		  case CDDS_PREPAINT:
		   *pResult = CDRF_NOTIFYSUBITEMDRAW;          // ask for subitem notifications.
		  break;

		  case CDDS_PREPAINT | CDDS_ITEM:
			lplvcd->nmcd.uItemState &= ~(LVIS_FOCUSED);
			*pResult = CDRF_NOTIFYSUBITEMDRAW;
			break;
			

		  case CDDS_PREPAINT | CDDS_ITEM | CDDS_SUBITEM: // recd when CDRF_NOTIFYSUBITEMDRAW is returned in
			  {                                    // response to CDDS_ITEMPREPAINT.
				  *pResult = CDRF_DODEFAULT;

				  int iCol = lplvcd->iSubItem;
				  int iRow = lplvcd->nmcd.dwItemSpec;

				  lplvcd->clrText = RGB(0, 0, 0);

				 
				  //else 
				  if(IsCompareDifferRow(iRow))//着色行 不一致
				  {
					//lplvcd->clrTextBk = RGB(237, 253, 207);
					//  lplvcd->clrText = RGB(255,0,0);
					//  lplvcd->clrTextBk =RGB(241,255,237);
					  //lplvcd->clrTextBk =RGB(255,255,220);//浅黄，整行着色，
					  lplvcd->clrTextBk =RGB(192,192,255);
				  }
				  else 
					  if (IsOnlyReadCol(iCol))
				  {			
					//  lplvcd->clrTextBk =RGB(241,255,237);	
				  }	
				  else if(IsOnlyReadPos(iRow,iCol))
				  {					  
					lplvcd->clrTextBk = RGB(255, 255, 255);					  
				  }				  
				  else if (IsFixedCol(iCol)||IsFixedPos(iRow,iCol))
				  {
					  lplvcd->clrTextBk = RGB(207, 253, 207);
				  } 
				  else if (IsCmbCol(iCol) || IsCmbBoxPos(iRow,iCol))
				  {
					  lplvcd->clrTextBk = RGB(255, 255, 255);
				  }
				  else if(IsButtonPos(iRow,iCol))
				  {
					 // lplvcd->clrTextBk = RGB(224,223,227);
					lplvcd->clrTextBk = RGB(255, 255, 255);
				  }
				  else 
				  {
					  lplvcd->clrTextBk = RGB(255, 255, 255);//白色
				  }

				  if (IsRedOrangePos(iRow/*,iCol*/)==1)
				  {
				//	  if(iCol == 0)
				//	  {
				//		  int a = 0;
				//	  }
				//	  lplvcd->clrTextBk = RGB(255,255,200);
					  lplvcd->clrText = RGB(255, 0, 0);//智能告警
				  }	
				  else if (IsRedOrangePos(iRow/*,iCol*/)==2)
				  {
						//lplvcd->clrText = RGB(0, 128, 255);//变位 orange
					  lplvcd->clrText = RGB(150,60,190);
				  }

				  if (GetItemState(iRow, CDIS_SELECTED))
				  {
					  lplvcd->clrTextBk =RGB(0, 0, 255); 
					  lplvcd->clrText = RGB(255, 255, 255); 
				  }

				  if(m_bSelectAll && iRow==GetSelectionMark())
				  {
					  lplvcd->clrTextBk = RGB(210,222,255);
					  lplvcd->clrText = RGB(255, 0, 0);
				  }
				  *pResult = CDRF_NEWFONT;
				  break;
			  }

		  default:// it wasn't a notification that was interesting to us.
		   *pResult = CDRF_DODEFAULT;
	  }

	return TRUE;	

}
/******************************************************************************
*	功能说明：	设置列属性 最大值，最小值，小数位数
*	输入参数：	nIndex 列索引、fMin 最小值 fMax 最大值 nDecimal 小数位数
******************************************************************************/
void CListCtrlExt::SetColProperty(int nIndex, float fMin, float fMax, int nDecimal)
{
	ASSERT(nIndex >=0 && nIndex < 10);
	m_ColProperty[nIndex].fMax = fMax;
	m_ColProperty[nIndex].fMin = fMin;
	m_ColProperty[nIndex].nDecimals = nDecimal;
}



/************************************************************************/
/*                                                                      
/************************************************************************/

void CListCtrlExt::PreSubclassWindow()
{
	CListCtrl::PreSubclassWindow();
	m_ctrlHeader.SubclassWindow(::GetDlgItem(m_hWnd,0));

}
void CListCtrlExt::SetColHeaderRed(int nColIndex)
{
	//设置表头自绘属性
	HDITEM  hdItem;   
	hdItem.mask = HDI_FORMAT; 

	for(int i=0;i<m_ctrlHeader.GetItemCount();i++)   //m_ctrlList为CMyCtrlList 变量       
	{         
		m_ctrlHeader.GetItem(i,&hdItem);       
		if(i==nColIndex)
			hdItem.fmt |= HDF_OWNERDRAW;         
		else 
			hdItem.fmt &= ~(HDF_OWNERDRAW);   
		m_ctrlHeader.SetItem(i,&hdItem);     
	}

}
void CListCtrlExt::SetHeaderTextColor(int nItem[], int nCount,COLORREF color) //设置头部字体颜色
{
	memset(m_ctrlHeader.m_colorItem, 0, sizeof(m_ctrlHeader.m_colorItem)/sizeof(m_ctrlHeader.m_colorItem[0]));
	for (int i=0;i<nCount;i++)
	{
		m_ctrlHeader.m_colorItem[i] = nItem[i];
	}

	m_ctrlHeader.m_colorCnt = nCount;
	//m_ctrlHeader.m_colorItem = nItem;
	m_ctrlHeader.m_color = color;
	m_ctrlHeader.Invalidate(TRUE);
}

int CListCtrlExt::InsertColumn(int nCol, LPCTSTR lpszColumnHeading,int nFormat /* = LVCFMT_LEFT */,int nWidth /* = -1 */,int nSubItem /* = -1 */)
{
	m_ctrlHeader.m_HChar.Add(lpszColumnHeading);
	if (nFormat==LVCFMT_LEFT)
	{
		m_ctrlHeader.m_Format = m_ctrlHeader.m_Format + "0";
	}
	else if (nFormat==LVCFMT_CENTER)
	{
		m_ctrlHeader.m_Format = m_ctrlHeader.m_Format + "1";
	}
	else if (nFormat==LVCFMT_RIGHT)
	{
		m_ctrlHeader.m_Format = m_ctrlHeader.m_Format + "2";
	}
	else
	{
		m_ctrlHeader.m_Format = m_ctrlHeader.m_Format + "1";
	}

	return CListCtrl::InsertColumn(nCol,lpszColumnHeading,nFormat,nWidth,nSubItem);
}