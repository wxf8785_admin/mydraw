#include "StdAfx.h"
#include "MyListCtrl.h"

CMyListCtrl::CMyListCtrl(void)
{
	m_nRowHeight = 20;
	for (int i=0;i<LINEMAX;i++)
	{
		nDrawLine[i] = 0;
	}
}

CMyListCtrl::~CMyListCtrl(void)
{
}

BEGIN_MESSAGE_MAP(CMyListCtrl, CListCtrl)
//	ON_WM_PAINT()
	ON_WM_VSCROLL()
	ON_WM_MOVE()
	ON_WM_MOUSEWHEEL()
	ON_WM_MEASUREITEM_REFLECT()
	ON_WM_MEASUREITEM()
	ON_NOTIFY_REFLECT(NM_CLICK, &CMyListCtrl::OnNMClick)
END_MESSAGE_MAP()

void CMyListCtrl::OnSetDrawLineFlag(int nDrawFlag[],int nSize)
{
	memset(nDrawLine,0,sizeof(nDrawLine)/sizeof(nDrawLine[0]));
	if (nSize>LINEMAX)
	{
		nSize = LINEMAX;
	}
	if (nSize<0)
	{
		nSize = 0;
	}
	for (int i=0;i<nSize;i++)
	{
		nDrawLine[i] = nDrawFlag[i];
	}

}
BOOL CMyListCtrl::isDrawLine(int nRow)
{
	if (nRow>LINEMAX)
	{
		return FALSE;
	}
	if (nRow<0)
	{
		return FALSE;
	}
	if (nDrawLine[nRow] == 1)
	{
		return TRUE;
	}
	return FALSE;
}


void CMyListCtrl::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	CDC *pDC = CDC::FromHandle(lpDIS->hDC);
 	LVITEM lvi = {0};
 	lvi.mask = LVIF_STATE;
 	lvi.stateMask = LVIS_FOCUSED|LVIS_SELECTED;
 	lvi.iItem = lpDIS->itemID;
 	BOOL bGet = GetItem(&lvi);
 	BOOL bHighlight = ((lvi.state & LVIS_DROPHILITED) || (lvi.state & LVIS_SELECTED) &&
 		((GetFocus() == this) || (GetStyle() & LVS_SHOWSELALWAYS)));
	int border = GetColumnWidth(0);
 	CRect rcBack = lpDIS->rcItem;
	int width = rcBack.Width();
	int height = rcBack.Height();

 	pDC->SetBkMode(TRANSPARENT);
 	if (bHighlight)
 	{
 		pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
 		pDC->SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));
 		pDC->FillRect(rcBack,&CBrush(::GetSysColor(COLOR_HIGHLIGHT)));
 	}
 	else
 	{
  //		if ((lpDIS->itemID+1)%3 == 0)
		if (isDrawLine(lpDIS->itemID))
  		{
 			pDC->SetTextColor(RGB(0,0,0));
			pDC->MoveTo(0,rcBack.bottom);
			pDC->LineTo(width,rcBack.bottom);
  		}
  		else 
  		{
  			pDC->SetTextColor(RGB(0,0,0));
  		}
	
 	}
// 	pDC->MoveTo(border,0);
// 	pDC->LineTo(border,rcBack.bottom);
// 	LVITEM lvi = {0}; 
// 	lvi.mask = LVIF_STATE;//|LVIF_IMAGE; 
// 	lvi.stateMask = LVIS_FOCUSED | LVIS_SELECTED ; 
// 	lvi.iItem = lpDIS->itemID; 
// 	BOOL bGet = GetItem(&lvi);
// 	BOOL bHightlight = ((lvi.state & LVIS_DROPHILITED) || (lvi.state & LVIS_SELECTED) &&
// 		((GetFocus() == this) || (GetStyle() &LVS_SHOWSELALWAYS)));
// 	
// 	CRect rcBack = lpDIS->rcItem;
// 	pDC->SetBkMode(TRANSPARENT);
// 	if (bHightlight)
// 	{
// 		pDC->SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
// 		pDC->SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));
// 		pDC->FillRect(rcBack,&CBrush(::GetSysColor(COLOR_HIGHLIGHT)));
// 	}
// 	else 
// 	{
// 		if (lpDIS->itemID%2 == 0)
// 		{
// 			pDC->SetTextColor(0x0c0f80);
// 			pDC->FillRect(rcBack,&CBrush(0xeef123));
// 		}
// 		else
// 		{
// 			pDC->SetTextColor(RGB(0,128,255));
// 			pDC->FillRect(rcBack,&CBrush(0xe3e3e3));
// 		}
// 	}
	CString str;
 	CRect rcFocus = lpDIS->rcItem;
 	rcFocus.DeflateRect(1,1,1,1);
 	if (lpDIS->itemAction & ODA_DRAWENTIRE)
 	{
		
		CString szText;
		int nCollumn = GetHeaderCtrl()->GetItemCount();
		for (int i=0;i<GetHeaderCtrl()->GetItemCount();i++)
		{
			CRect rcitem;
			if (!GetSubItemRect(lpDIS->itemID,i,LVIR_LABEL,rcitem))
				continue;
			szText = GetItemText(lpDIS->itemID,i);
			pDC->DrawText(szText,lstrlen(szText),&rcitem,DT_LEFT|DT_VCENTER|DT_NOPREFIX|DT_SINGLELINE);
		}
// 			if (!GetSubItemRect(lpDIS->itemID,i,LVIR_LABEL,rcitem))
// 				continue;
	//		szText = GetItemText(lpDIS->itemID,i);
	//		rcitem.left +=5;rcitem.right-=1;
	//		pDC->DrawText(szText,lstrlen(szText),&rcitem,DT_LEFT|DT_VCENTER|DT_NOPREFIX|DT_SINGLELINE);			    
}
}

void CMyListCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	Invalidate();
	CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CMyListCtrl::OnMove(int x, int y)
{
	CListCtrl::OnMove(x, y);

	// TODO: 在此处添加消息处理程序代码
}

void CMyListCtrl::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    if (m_nRowHeight>0)
    {
        lpMeasureItemStruct->itemHeight = m_nRowHeight;
    }
}

void CMyListCtrl::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
    CListCtrl::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}


BOOL CMyListCtrl::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	Invalidate();
	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}



void CMyListCtrl::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPDRAWITEMSTRUCT pNMLV;
	pNMLV = (LPDRAWITEMSTRUCT)pNMHDR;

	Invalidate();
	
	*pResult = 0;
}

void CMyListCtrl::SetRowHeigt(int nHeight)
{
    m_nRowHeight = nHeight;
    CRect rcWin;
    GetWindowRect(&rcWin);
    WINDOWPOS wp;
    wp.hwnd = m_hWnd;
    wp.cx = rcWin.Width();
    wp.cy = rcWin.Height();
    wp.flags = SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOZORDER;
    SendMessage(WM_WINDOWPOSCHANGED, 0, (LPARAM)&wp);
}
