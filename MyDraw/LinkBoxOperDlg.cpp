// LinkBoxOperDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MyDraw.h"
#include "MyDrawDoc.h"
#include "MyDrawView.h"
#include "MainFrm.h"
#include "LinkBoxOperDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLinkBoxOperDlg dialog


CLinkBoxOperDlg::CLinkBoxOperDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLinkBoxOperDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLinkBoxOperDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CLinkBoxOperDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLinkBoxOperDlg)
	DDX_Control(pDX, IDC_LIST_INKBOX_PARAM, m_InkBoxParamList);
	DDX_Control(pDX, IDC_LIST_INKBOX_INDENTIFY, m_InkBoxIndentifyList);
	DDX_Control(pDX, IDC_COMBO_SMJETINDEX_H, m_nSMJETIndex_H);
	DDX_Control(pDX, IDC_COMBO_SMJETINDEX_B, m_nSMJETIndex_B);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLinkBoxOperDlg, CDialog)
	//{{AFX_MSG_MAP(CLinkBoxOperDlg)
	ON_BN_CLICKED(IDC_BUTTON_INDENTIFY, OnButtonIndentify)
	ON_BN_CLICKED(IDC_BUTTON_SETTING_INKBOX_TYPE, OnButtonSettingInkboxType)
	ON_BN_CLICKED(IDC_BUTTON_READ, OnButtonRead)
	ON_BN_CLICKED(IDC_BUTTON_WRITE, OnButtonWrite)
	ON_BN_CLICKED(IDC_BUTTON_WASH, OnButtonWash)
	ON_BN_CLICKED(IDC_BUTTON_REPLACE, OnButtonReplace)
	ON_MESSAGE(USER_KILL_FOCUS,onListEditKillFocus)
	ON_CBN_SELCHANGE(IDC_COMBO_SMJETINDEX_H, OnSelchangeComboSMJETHIndex)
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLinkBoxOperDlg message handlers

BOOL CLinkBoxOperDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CMainFrame* pMain = (CMainFrame*)AfxGetMainWnd();
	m_strParamPath = pMain->GetWorkDir();
	// TODO: Add extra initialization here
	
	m_InkBoxParamList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,LVS_REPORT);
	ListView_SetExtendedListViewStyle(m_InkBoxParamList.m_hWnd,
		LVS_EX_FULLROWSELECT |LVS_EX_GRIDLINES |LVS_EX_INFOTIP); 

	m_InkBoxParamList.InsertColumn(0,               _T("墨盒号"),LVCFMT_CENTER, 70);
	m_InkBoxParamList.InsertColumn(1,     _T("脉冲时间(us)"),LVCFMT_LEFT, 100);
	m_InkBoxParamList.InsertColumn(2,_T("	墨量(ml)"),LVCFMT_LEFT, 80);
	m_InkBoxParamList.InsertColumn(3,    _T("温度(℃)"),LVCFMT_LEFT, 80);
	m_InkBoxParamList.InsertColumn(4,    _T("电压(V)"),LVCFMT_LEFT, 80);
	m_InkBoxParamList.InsertColumn(5,    _T("是否加热"),LVCFMT_LEFT, 80);
	m_InkBoxParamList.InsertColumn(6,    _T("清洗程度"),LVCFMT_LEFT, 80);
	m_InkBoxParamList.InsertColumn(7,    _T("是否换墨盒"),LVCFMT_LEFT, 100);
	m_InkBoxParamList.InsertColumn(8,    _T("A/B列"),LVCFMT_LEFT, 80);

	int readCol1[] = {0}; // 装载值列项可以编辑
	m_InkBoxParamList.SetOnlyReadCol(readCol1,1);




	m_InkBoxIndentifyList.ModifyStyle(LVS_ICON | LVS_LIST | LVS_REPORT | LVS_SMALLICON ,LVS_REPORT);
	ListView_SetExtendedListViewStyle(m_InkBoxIndentifyList.m_hWnd,
		LVS_EX_FULLROWSELECT |LVS_EX_GRIDLINES |LVS_EX_INFOTIP); 

	m_InkBoxIndentifyList.InsertColumn(0,               _T("墨盒号"),LVCFMT_CENTER, 80);
	m_InkBoxIndentifyList.InsertColumn(1,     _T("原装墨盒序列号"),LVCFMT_LEFT, 200);
	m_InkBoxIndentifyList.InsertColumn(2,_T("	墨盒类型"),LVCFMT_LEFT, 150);

	int readCol[] = {0}; // 装载值列项可以编辑
	m_InkBoxIndentifyList.SetOnlyReadCol(readCol,1);


	m_nSMJETIndex_B.AddString(_T("1"));
	m_nSMJETIndex_B.AddString(_T("2"));
	m_nSMJETIndex_B.AddString(_T("3"));
	m_nSMJETIndex_B.AddString(_T("4"));
	m_nSMJETIndex_B.AddString(_T("5"));
	m_nSMJETIndex_B.AddString(_T("6"));
	m_nSMJETIndex_B.SetCurSel(0);


	m_nSMJETIndex_H.AddString(_T("1"));
	m_nSMJETIndex_H.AddString(_T("2"));
	m_nSMJETIndex_H.AddString(_T("3"));
	m_nSMJETIndex_H.AddString(_T("4"));
	m_nSMJETIndex_H.AddString(_T("5"));
	m_nSMJETIndex_H.AddString(_T("6"));
	
	m_nSelIndex = ::GetPrivateProfileInt(_T("LinkBoxSMJETH"), _T("SMJETNum"), 0, m_strParamPath);
	if (m_nSelIndex!=0)
	{
		m_nSelIndex--;
	}
	m_nSMJETIndex_H.SetCurSel(m_nSelIndex);

	OnInitListData();
	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子  
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLinkBoxOperDlg::OnInitListData()
{
	int nCnt =0;
	CString sText = "";
	int nSwRows[300] = { 0 }, nSwCols[300] = {0};
	CString text ="不设置墨盒类型|45A|VB|";
	int i=0;
	for(i=0;i<4;i++)
	{
		nCnt = i;
		sText.Format("%d",nCnt+1);
		m_InkBoxIndentifyList.InsertItem(nCnt,sText);
		sText = "请填入认证信息";
		m_InkBoxIndentifyList.SetItemText(nCnt,1, sText);
		sText = "不设置墨盒类型";
		m_InkBoxIndentifyList.SetItemText(nCnt,2, sText);//

		nSwRows[i] = i;
		nSwCols[i] = 2;
		m_InkBoxIndentifyList.m_CmbItemsStr[i]=text;
		m_InkBoxIndentifyList.SetCmbBoxPos(nSwRows,nSwCols,100);
	}


	nCnt = 0;
	int nSwRows111[300] = { 0 };
	for(i=0;i<4;i++)
	{
		nCnt = i;
		sText.Format("%d",nCnt+1);
		m_InkBoxParamList.InsertItem(nCnt,sText);
		sText = "1.9";
		m_InkBoxParamList.SetItemText(nCnt,1, sText);
		sText = "0";
		m_InkBoxParamList.SetItemText(nCnt,2, sText);
		sText = "0";
		m_InkBoxParamList.SetItemText(nCnt,3, sText);
		sText = "11.2";
		m_InkBoxParamList.SetItemText(nCnt,4, sText);


		sText = "否";
		m_InkBoxParamList.SetItemText(nCnt,5, sText);

		sText = "不清洗";
		m_InkBoxParamList.SetItemText(nCnt,6, sText);


		sText = "否";
		m_InkBoxParamList.SetItemText(nCnt,7, sText);

		sText = "A";
		m_InkBoxParamList.SetItemText(nCnt,8, sText);

	}

	text ="否|是|";
	nSwRows111[5] = 5;
	m_InkBoxParamList.m_CmbItemsStr[5]=text;
	m_InkBoxParamList.SetCmbCol(nSwRows111,300);	

	text ="不清洗|轻度|中度|重度|";
	nSwRows111[6] = 6;
	m_InkBoxParamList.m_CmbItemsStr[6]=text;
	m_InkBoxParamList.SetCmbCol(nSwRows111,300);	

	text ="否|是|";
	nSwRows111[7] = 7;
	m_InkBoxParamList.m_CmbItemsStr[7]=text;
	m_InkBoxParamList.SetCmbCol(nSwRows111,300);	


	text ="A|B|";
	nSwRows111[8] = 8;
	m_InkBoxParamList.m_CmbItemsStr[8]=text;
	m_InkBoxParamList.SetCmbCol(nSwRows111,300);	

	ReadInkBoxParamIni(m_nSelIndex);

}



void CLinkBoxOperDlg::OnButtonIndentify() 
{
	// TODO: Add your control notification handler code here
	
}

void CLinkBoxOperDlg::OnButtonSettingInkboxType() 
{
	// TODO: Add your control notification handler code here
	
}

void CLinkBoxOperDlg::OnButtonRead() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	pFrame->SendReadInkBoxParam(0);
}

void CLinkBoxOperDlg::OnButtonWrite() 
{
	// TODO: Add your control notification handler code here
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	int nSelIndex = m_nSMJETIndex_H.GetCurSel();
	pFrame->SendWriteInkBoxParam(nSelIndex);
}

void CLinkBoxOperDlg::OnButtonWash() 
{
	// TODO: Add your control notification handler code here


	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	int nSelIndex = m_nSMJETIndex_H.GetCurSel();
	pFrame->SendWashBoxParam(nSelIndex);


	
}

void CLinkBoxOperDlg::OnButtonReplace() 
{
	// TODO: Add your control notification handler code here
	
}
float CLinkBoxOperDlg::GetInkBoxParam(int iRow,int iCol)
{
	int nRow = m_InkBoxParamList.GetItemCount();
	if(iRow>nRow-1)
		return 0;
//	fData = (float)_tcstod(m_nPrinterPosList.GetItemText(Item,subItem),NULL);
	CString sText = "";
	float fData = 0.0f;
	if(iCol ==1)
	{
		fData = (float)_tcstod(m_InkBoxParamList.GetItemText(iRow,iCol),NULL);
		return fData;
	}
	else if(iCol ==2)
	{
		fData = (float)_tcstod(m_InkBoxParamList.GetItemText(iRow,iCol),NULL);
		return fData;
	}
	else if(iCol ==3)
	{
		fData = (float)_tcstod(m_InkBoxParamList.GetItemText(iRow,iCol),NULL);
		return fData;
	}
	else if(iCol ==4)
	{
		fData = (float)_tcstod(m_InkBoxParamList.GetItemText(iRow,iCol),NULL);
		return fData;
	}
	else if(iCol ==5)
	{
		sText = m_InkBoxParamList.GetItemText(iRow,iCol);
		if(sText =="是")
			return 1;
		else
			return 0;
	}
	else if(iCol ==6)
	{
		sText = m_InkBoxParamList.GetItemText(iRow,iCol);
		if(sText =="不清洗")
			return 0;
		else if(sText =="轻度")
			return 1;
		else if(sText =="中度")
			return 2;
		else if(sText =="重度")
			return 3;
		else
			return 0;
	}
	else if(iCol ==7)
	{
		sText = m_InkBoxParamList.GetItemText(iRow,iCol);
		if(sText =="是")
			return 1;
		else
			return 0;
	}
	else if(iCol ==8)
	{
		sText = m_InkBoxParamList.GetItemText(iRow,iCol);
		if(sText =="A")
			return 0;
		else if(sText =="B")
			return 1;
//		else if(sText =="AB")
//			return 2;
		else
			return 0;
	}

	return 0;
}
void CLinkBoxOperDlg::OnSelchangeComboSMJETHIndex()
{
	SaveInkBoxParamIni(m_nSelIndex);
	m_nSelIndex = m_nSMJETIndex_H.GetCurSel();
	ReadInkBoxParamIni(m_nSelIndex);
}
void CLinkBoxOperDlg::SaveInkBoxParamIni(int nSelIndex)
{
	
	CString strIndex;
	strIndex.Format("%d", nSelIndex+1);
	::WritePrivateProfileString("LinkBoxSMJETH", "SMJETNum", strIndex, m_strParamPath);
	CString sText;
	for (int i = 0; i<4; i++)
	{
		CString strCnt;
		strCnt.Format("%d", i + 1);
		::WritePrivateProfileString("LinkBoxSMJETH" + strIndex, "mhh" + strCnt, strCnt, m_strParamPath);
		sText = m_InkBoxParamList.GetItemText(i, 1);
		::WritePrivateProfileString("LinkBoxSMJETH" + strIndex, "hds" + strCnt, sText, m_strParamPath);
		sText = m_InkBoxParamList.GetItemText(i, 2);
		::WritePrivateProfileString("LinkBoxSMJETH" + strIndex, "ml" + strCnt, sText, m_strParamPath);
		sText = m_InkBoxParamList.GetItemText(i, 3);
		::WritePrivateProfileString("LinkBoxSMJETH" + strIndex, "wd" + strCnt, sText, m_strParamPath);
		sText = m_InkBoxParamList.GetItemText(i, 4);
		::WritePrivateProfileString("LinkBoxSMJETH" + strIndex, "dy" + strCnt, sText, m_strParamPath);
		sText = m_InkBoxParamList.GetItemText(i, 5);
		::WritePrivateProfileString("LinkBoxSMJETH" + strIndex, "sfjr" + strCnt, sText, m_strParamPath);
		sText = m_InkBoxParamList.GetItemText(i, 6);
		::WritePrivateProfileString("LinkBoxSMJETH" + strIndex, "qxcd" + strCnt, sText, m_strParamPath);
		sText = m_InkBoxParamList.GetItemText(i, 7);
		::WritePrivateProfileString("LinkBoxSMJETH" + strIndex, "sfhmh" + strCnt, sText, m_strParamPath);
		sText = m_InkBoxParamList.GetItemText(i, 8);
		::WritePrivateProfileString("LinkBoxSMJETH" + strIndex, "a/b列" + strCnt, sText, m_strParamPath);

	}
}
void CLinkBoxOperDlg::ReadInkBoxParamIni(int nSelIndex)
{
	CString strIndex;
	strIndex.Format("%d", nSelIndex + 1);
	CString sText;
	for (int i = 0; i < 4; i++)
	{
		CString strCnt;
		strCnt.Format("%d", i + 1);
		::GetPrivateProfileString(_T("LinkBoxSMJETH") + strIndex, _T("hds") + strCnt, _T("1.9"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_InkBoxParamList.SetItemText(i, 1, sText);
		::GetPrivateProfileString(_T("LinkBoxSMJETH") + strIndex, _T("ml") + strCnt, _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_InkBoxParamList.SetItemText(i, 2, sText);
		::GetPrivateProfileString(_T("LinkBoxSMJETH") + strIndex, _T("wd") + strCnt, _T("0"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_InkBoxParamList.SetItemText(i, 3, sText);
		::GetPrivateProfileString(_T("LinkBoxSMJETH") + strIndex, _T("dy") + strCnt, _T("11.2"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_InkBoxParamList.SetItemText(i, 4, sText);
		::GetPrivateProfileString(_T("LinkBoxSMJETH") + strIndex, _T("sfjr") + strCnt, _T("否"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_InkBoxParamList.SetItemText(i, 5, sText);
		::GetPrivateProfileString(_T("LinkBoxSMJETH") + strIndex, _T("qxcd") + strCnt, _T("不清洗"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_InkBoxParamList.SetItemText(i, 6, sText);
		::GetPrivateProfileString(_T("LinkBoxSMJETH") + strIndex, _T("sfhmh") + strCnt, _T("否"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_InkBoxParamList.SetItemText(i, 7, sText);
		::GetPrivateProfileString(_T("LinkBoxSMJETH") + strIndex, _T("a/b列") + strCnt, _T("A"), sText.GetBuffer(MAX_PATH), MAX_PATH, m_strParamPath);
		m_InkBoxParamList.SetItemText(i, 8, sText);
	}
}
void CLinkBoxOperDlg::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	SaveInkBoxParamIni(m_nSelIndex);
	CDialog::OnClose();
}
void CLinkBoxOperDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here

}
LRESULT CLinkBoxOperDlg::onListEditKillFocus(WPARAM wParam,LPARAM lParam)
{
	int nRow = m_InkBoxParamList.GetItemCount();
	int nCol = 5;
	int Item = wParam;
	int subItem = lParam;
	float fData = 0.0f;
	float fData2 = 0.0f;
	int   m_nData = 0;
	CString sText = "";
	if((subItem == 2 || subItem == 3 || subItem == 4))
	{
			fData = (float)_tcstod(m_InkBoxParamList.GetItemText(Item,subItem),NULL);
			if(fData>100)
				fData = 100;
			sText.Format("%.1f",fData);
			m_InkBoxParamList.SetItemText(Item,subItem,sText);
	}
	else if(subItem == 1)
	{
			fData = (float)_tcstod(m_InkBoxParamList.GetItemText(Item,subItem),NULL);
			if(fData>100)
				fData = 100;
			sText.Format("%d",(int)fData);
			m_InkBoxParamList.SetItemText(Item,subItem,sText);
	}
	return 1;

}

HBRUSH CLinkBoxOperDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
