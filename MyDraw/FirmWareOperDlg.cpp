// FirmWareOperDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MyDraw.h"
#include "MyDrawDoc.h"
#include "MyDrawView.h"
#include "MainFrm.h"
#include "FirmWareOperDlg.h"
#include <iostream>                                                                                                                                
#include <fstream>
#include <ios> 

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFirmWareOperDlg dialog


CFirmWareOperDlg::CFirmWareOperDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFirmWareOperDlg::IDD, pParent)
{
	m_strFilePath = _T("");
	//{{AFX_DATA_INIT(CFirmWareOperDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CFirmWareOperDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFirmWareOperDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_COMBO_FIRMWARE_SMJET_INDEX, m_nSMJETSelectCmb);
	DDX_Control(pDX, IDC_COMBO_FIRMWARE_SMJET_SET_INDEX, m_nSMJETSetIndexCmb);
	DDX_Control(pDX, IDC_TAB_FIX_SET, m_FixSetTab);
	DDX_Text(pDX, IDC_EDIT_FILEPATH, m_strFilePath);
	DDX_Control(pDX, IDC_COMBO_FIRMWARE_SMJET_INDEX2, m_nSMJETSelectCmb2);
	DDX_Control(pDX, IDC_STATIC_VERSION_NUM, VERSION_NUM);
}


BEGIN_MESSAGE_MAP(CFirmWareOperDlg, CDialog)
	//{{AFX_MSG_MAP(CFirmWareOperDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_SET_SMJET, &CFirmWareOperDlg::OnBnClickedButtonSetSmjet)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_FIX_SET, OnSelchangeTabFixSet)
	ON_BN_CLICKED(IDC_BUTTON_SELFILE, &CFirmWareOperDlg::OnBnClickedButtonSelfile)
	ON_BN_CLICKED(IDC_BUTTON_STRUCT, &CFirmWareOperDlg::OnBnClickedButtonStruct)
	ON_BN_CLICKED(IDC_BUTTON_version, &CFirmWareOperDlg::OnBnClickedButtonversion)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFirmWareOperDlg message handlers



BOOL CFirmWareOperDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_FixSetTab.InsertItem(0, "序号设定", 0);
	m_FixSetTab.InsertItem(1, "固定升级", 1);

	// TODO:  在此添加额外的初始化
	m_nSMJETSelectCmb.AddString("1");
	m_nSMJETSelectCmb.AddString("2");
	m_nSMJETSelectCmb.AddString("3");
	m_nSMJETSelectCmb.AddString("4");
	m_nSMJETSelectCmb.AddString("5");
	m_nSMJETSelectCmb.AddString("6");
	m_nSMJETSelectCmb.SetCurSel(0);


	m_nSMJETSetIndexCmb.AddString("1");
	m_nSMJETSetIndexCmb.AddString("2");
	m_nSMJETSetIndexCmb.AddString("3");
	m_nSMJETSetIndexCmb.AddString("4");
	m_nSMJETSetIndexCmb.AddString("5");
	m_nSMJETSetIndexCmb.AddString("6");
	m_nSMJETSetIndexCmb.SetCurSel(1);

	m_nSMJETSelectCmb2.AddString("1");
	m_nSMJETSelectCmb2.AddString("2");
	m_nSMJETSelectCmb2.AddString("3");
	m_nSMJETSelectCmb2.AddString("4");
	m_nSMJETSelectCmb2.AddString("5");
	m_nSMJETSelectCmb2.AddString("6");
	m_nSMJETSelectCmb2.SetCurSel(0);

	SetCtrlVisible(0);
	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子  
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CFirmWareOperDlg::OnBnClickedButtonSetSmjet()
{
	// TODO: 在此添加控件通知处理程序代码
	CString strName = "";
	int SMJETSelectIndex = 0;
	int SMJETSelectSetIndex = 0;


	int nIndex = m_nSMJETSelectCmb.GetCurSel();
	if (nIndex == CB_ERR)
	{
		SMJETSelectIndex = 0;
		return;
	}
	SMJETSelectIndex = nIndex;


	nIndex = m_nSMJETSetIndexCmb.GetCurSel();
	if (nIndex == CB_ERR)
	{
		SMJETSelectSetIndex = 0;
		return;
	}
	SMJETSelectSetIndex = nIndex;

	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

	if (SMJETSelectIndex == SMJETSelectSetIndex)
	{
		AfxMessageBox("原始值和设定值相同，请重新选择");
	}
	pFrame->SendSetSMJETIndex(SMJETSelectIndex,SMJETSelectSetIndex);




}
void CFirmWareOperDlg::OnSelchangeTabFixSet(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here
	SetCtrlVisible(m_FixSetTab.GetCurSel());
	*pResult = 0;
}
void CFirmWareOperDlg::SetCtrlVisible(int nFlag)
{
	if (nFlag == 0)
	{
		GetDlgItem(IDC_STATIC_FIRMWARE_SMJET_SELECT)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_FIRMWARE_SMJET_INDEX)->ShowWindow(true);
		GetDlgItem(IDC_GROUP_SMJET_SET)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_SMJET_SET)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_FIRMWARE_SMJET_SET_INDEX)->ShowWindow(true);
		GetDlgItem(IDC_BUTTON_SET_SMJET)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_PROFILE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FILEPATH)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_SELFILE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FIRMWARE_SMJET_SELECT2)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_FIRMWARE_SMJET_INDEX2)->ShowWindow(false); 
		GetDlgItem(GROUP_FIRMSTRUCT)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_STRUCT)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_version)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_VERSION_NUM)->ShowWindow(false);
	}
	else if (nFlag == 1)
	{
		GetDlgItem(IDC_STATIC_FIRMWARE_SMJET_SELECT)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_FIRMWARE_SMJET_INDEX)->ShowWindow(false);
		GetDlgItem(IDC_GROUP_SMJET_SET)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_SMJET_SET)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_FIRMWARE_SMJET_SET_INDEX)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_SET_SMJET)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_PROFILE)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_FILEPATH)->ShowWindow(true);
		GetDlgItem(IDC_BUTTON_SELFILE)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_FIRMWARE_SMJET_SELECT2)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_FIRMWARE_SMJET_INDEX2)->ShowWindow(true);
		GetDlgItem(GROUP_FIRMSTRUCT)->ShowWindow(true);
		GetDlgItem(IDC_BUTTON_STRUCT)->ShowWindow(true);
		GetDlgItem(IDC_BUTTON_version)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_VERSION_NUM)->ShowWindow(true);
	}
}

void CFirmWareOperDlg::OnBnClickedButtonSelfile()
{
	// TODO:  在此添加控件通知处理程序代码
	CFileDialog Filedlg(TRUE, NULL, NULL,
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |
		OFN_ALLOWMULTISELECT | OFN_EXPLORER,
		"Data Files (*.bin)|*.bin",
		NULL);

	char pBuf[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, pBuf);
	CString sPath = "";
	sPath.Format("%s", pBuf);

	Filedlg.m_ofn.lpstrTitle = "导入升级文件";//改变标题

	Filedlg.m_ofn.lpstrInitialDir = sPath;//设定打开初始目录

	//	Filedlg.m_ofn.lStructSize = structsize;

	Filedlg.m_ofn.nMaxFile = MAX_PATH;

	CString strFilePath = "";
	if (Filedlg.DoModal() == IDOK)
	{
		strFilePath = Filedlg.GetPathName();
		//ReadPrintData(strFilePath);

	}
	m_strFilePath = strFilePath;

	UpdateData(FALSE);

	
}


void CFirmWareOperDlg::OnBnClickedButtonStruct()
{
	if (m_strFilePath.IsEmpty())
	{
		AfxMessageBox("请选择升级文件");
		return;
	}
	std::ifstream fin;
	fin.open(m_strFilePath, std::ios::binary);
	if (!fin){

		return;
	}
	std::streampos pos = fin.tellg();
	fin.seekg(0, fin.end);
	size_t tSize = fin.tellg();
	fin.seekg(pos);
	int code = 6;
	int packtatal = tSize/1000;
	if (tSize%1000 != 0)
	{
		packtatal++;
	}
	int pack_no = 0;
	int total_len = tSize;
	int total_byte = 0;
	byte  byteData[1000] = { 0 };
	BYTE bData = 0;
	int nData = 0;
	int nCount = 0;

	CMainFrame* pMain = (CMainFrame*)AfxGetMainWnd();
	CMyDrawView *pView = (CMyDrawView *)pMain->GetActiveView();

	pMain->ClearFirmDataArray();

	FirmData* pFirmData = new FirmData();

	while (fin.read((char*)&bData, sizeof(bData)))
	{
		if (pFirmData == NULL)
		{
			pFirmData = new FirmData();
		}
		nData = bData;
		total_byte += nData;
		byteData[nCount - pack_no * 1000] = bData;
		nCount++;
		if (nCount%1000 == 0||nCount == tSize)
		{
			pack_no++;	
			if (pack_no == 1)
			{
				pFirmData->code = 6;
				pFirmData->pack_no = pack_no;
				pFirmData->packtatal = packtatal;
				pFirmData->reverse = 0;
				pFirmData->total_len = total_len;
				pFirmData->total_byte = total_byte;
				memcpy(pFirmData->bData, byteData, 1000);
			}
						
			FirmData pTempFirmData;
			pTempFirmData.code = 6;
			pTempFirmData.pack_no = pack_no;
			pTempFirmData.packtatal = packtatal;
			pTempFirmData.reverse = 0;
			pTempFirmData.total_len = total_len;
			pTempFirmData.total_byte = total_byte;
			memcpy(pTempFirmData.bData, byteData, 1000);
			pMain->SetFirmDataArray(pTempFirmData);
			total_byte = 0;
		}
	}
	int nSmget = m_nSMJETSelectCmb2.GetCurSel();
	pMain->SetSMJET(nSmget);
	pMain->OnUdpSendData((char*)pFirmData, 1012, nSmget);

	delete pFirmData;
	pFirmData = NULL;
	fin.close();
}


void CFirmWareOperDlg::OnBnClickedButtonversion()
{
	// TODO:  在此添加控件通知处理程序代码
	CMainFrame* pMain = (CMainFrame*)AfxGetMainWnd();
	byte m_Version[4] = { 0 };
	m_Version[0] = 7;
	m_Version[1] = 0;
	m_Version[2] = 0;
	m_Version[3] = 0;

	int nSmget = m_nSMJETSelectCmb2.GetCurSel();
	pMain->OnUdpSendData((char*)m_Version, 4, nSmget);
}






////刷新控件数据
void CFirmWareOperDlg::UpdataCtrlData(int SMJET, CString str)
{

	VERSION_NUM.SetWindowText(str);

}

HBRUSH CFirmWareOperDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
