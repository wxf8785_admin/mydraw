// MyProgress.h: interface for the CMyProgress class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MYPROGRESS_H__A2994539_1EE4_4411_B607_F9EE5F9F54D7__INCLUDED_)
#define AFX_MYPROGRESS_H__A2994539_1EE4_4411_B607_F9EE5F9F54D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ProgDlg.h"


class CMyProgress  
{
public:
	CMyProgress();
	virtual ~CMyProgress();
private:
	CProgressDlg m_ProgressDlg;
	long m_ProgressTmp;
	long m_ProgressLen;
	int m_ProgressNum;
public:
	int GetPos();
	//创建进度条，nLength为进度条的总长度。
	BOOL CreateProgressDlg(long nLength);
	//设置一些提示文字
	void SetCaption(CString strCaption);
	void SetStatus(CString strMsg);
	//根据当前偏离量显示进度条
	BOOL DoProgress(long offset);
	//进度条可以分多个过程（段），每完成一个段后，传入其长度。
	void StepPart(long nPartLength);
	void HideCancel();//Hide 取消按钮
	void DestroyWindow();//去掉进度条

};

#endif // !defined(AFX_MYPROGRESS_H__A2994539_1EE4_4411_B607_F9EE5F9F54D7__INCLUDED_)
