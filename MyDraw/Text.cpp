// Text.cpp: implementation of the CText class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MyDraw.h"
#include "MyDrawView.h"
#include "MainFrm.h"
#include "Text.h"
#include "CMFCPropertyGridFontPropertyEx.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CText,CObject,1)

CText::CText()
{
	tracker.m_nStyle=CRectTracker::resizeInside|
		CRectTracker::dottedLine; 
	tracker.m_rect.SetRect(0,0,0,0); 
	tracker.m_nHandleSize=8; 

	bIsSelected=false;
	startX=41;
	startY=41;
	endX=150;
	endY=72;

	BkMode=TRANSPARENT;
	MyText="D123456";
	m_strInitVal = MyText;
	MyColor=RGB(0,0,0);

	MyFont.lfCharSet=DEFAULT_CHARSET;
	MyFont.lfClipPrecision=CLIP_DEFAULT_PRECIS;
	MyFont.lfEscapement=0;
	strcpy(MyFont.lfFaceName,"Arial");
	MyFont.lfHeight=-27;
	MyFont.lfItalic=false;
	MyFont.lfOrientation=0;
	MyFont.lfOutPrecision=OUT_DEFAULT_PRECIS;
	MyFont.lfPitchAndFamily=FF_SWISS;
	MyFont.lfQuality=DEFAULT_QUALITY;
	MyFont.lfStrikeOut=false;
	MyFont.lfUnderline=false;
	MyFont.lfWeight=400;
	MyFont.lfWidth=0; 

	m_nFieldIndex = 1;
	m_iTxtFieldDis = 1;

	nConstValueType = 0;
	nTxtRotateType = 0;
	nTxtRotateAngle = 0;
	isMirrorImage = false;
	nTxtInlineType = 1;
	isTxtUseStyleCtrl = false;

	isUseAnyStyle = false;
	isAntiDistrot = false;
	isUseDeburring = false;
	nDeburring_x = 0;;
	nDeburring_y = 0;;

	isDelete0 = false;

	m_sTxtFieldType = "1,2,3,4,5";

	nGraphics = 5;


	nStartPos = 0;
	nCounts = 0;

	m_lTxtCounterStartNum = 1;
	m_lTxtCounterEndNum = 10000;
	m_lTxtCounterStep = 1;

}

CText::~CText()
{
	
}

void CText::Serialize(CArchive & ar)
{
	CObject::Serialize(ar);
    if(ar.IsLoading())
    {
		CString lfFaceName;

		ar>>startX>>startY>>endX>>endY>>MyText>>BkMode>>MyColor>>
			MyFont.lfCharSet>>
			MyFont.lfClipPrecision>>
			MyFont.lfEscapement>>
			lfFaceName >>
			MyFont.lfHeight>>
			MyFont.lfItalic>>
			MyFont.lfOrientation>>
			MyFont.lfOutPrecision>>
			MyFont.lfPitchAndFamily>>
			MyFont.lfQuality>>
			MyFont.lfStrikeOut>>
			MyFont.lfUnderline>>
			MyFont.lfWeight>>
			MyFont.lfWidth>>
			m_nFieldIndex>>m_iTxtFieldDis>>m_nTimeFormatIndex>>
			nConstValueType>>nTxtRotateType>>nTxtRotateAngle>>nTxtInlineType>>
			isTxtUseStyleCtrl>>isMirrorImage>>isUseAnyStyle>>isAntiDistrot>>isUseDeburring>>
			nDeburring_x>>nDeburring_y>>nStartPos>>nCounts>>
			m_lTxtCounterStartNum>>m_lTxtCounterEndNum>>m_lTxtCounterStep>>
			m_strFieldDes>>m_strRecordVal>>m_strInitVal>>sInsetSqlParam>>m_sTxtFieldType;

		strcpy(MyFont.lfFaceName, lfFaceName);
    }
	else
	{
		ar<<startX<<startY<<endX<<endY<<MyText<<BkMode<<MyColor<<
			MyFont.lfCharSet<<
			MyFont.lfClipPrecision<<
			MyFont.lfEscapement<<
			(CString)MyFont.lfFaceName<<
			MyFont.lfHeight<<
			MyFont.lfItalic<<
			MyFont.lfOrientation<<
			MyFont.lfOutPrecision<<
			MyFont.lfPitchAndFamily<<
			MyFont.lfQuality<<
			MyFont.lfStrikeOut<<
			MyFont.lfUnderline<<
			MyFont.lfWeight<<
			MyFont.lfWidth<<
			m_nFieldIndex<<m_iTxtFieldDis<<m_nTimeFormatIndex<<
			nConstValueType<<nTxtRotateType<<nTxtRotateAngle<<nTxtInlineType<<
			isTxtUseStyleCtrl<<isMirrorImage<<isUseAnyStyle<<isAntiDistrot<<isUseDeburring<<
			nDeburring_x<<nDeburring_y<<nStartPos<<nCounts<<
			m_lTxtCounterStartNum<<m_lTxtCounterEndNum<<m_lTxtCounterStep<<
			m_strFieldDes<<m_strRecordVal<<m_strInitVal<<sInsetSqlParam<<m_sTxtFieldType;
    }
}





CGraph* CText::Clone()
{
	CText* p = new CText();

	p->startX = this->startX + 10;
	p->startY = this->startY + 10;
	p->endX = this->endX + 10;
	p->endY = this->endY + 10;
	p->MyText = this->MyText;


	p->BkMode = this->BkMode;
	p->MyFont = this->MyFont;
	p->MyColor = this->MyColor;
	p->m_nFieldIndex = this->m_nFieldIndex;
	p->m_iTxtFieldDis = this->m_iTxtFieldDis;
	p->m_nTimeFormatIndex = this->m_nTimeFormatIndex;
	p->m_strFieldDes = this->m_strFieldDes;
	p->m_strRecordVal = this->m_strRecordVal;
	p->m_strInitVal = this->m_strInitVal;
	p->sInsetSqlParam = this->sInsetSqlParam;
	p->m_sTxtFieldType = this->m_sTxtFieldType;
	p->nConstValueType = this->nConstValueType;
	p->nTxtRotateType = this->nTxtRotateType;
	p->nTxtRotateAngle = this->nTxtRotateAngle;
	p->isMirrorImage = this->isMirrorImage;
	p->nTxtInlineType = this->nTxtInlineType;
	p->isTxtUseStyleCtrl = this->isTxtUseStyleCtrl;
	p->isUseAnyStyle = this->isUseAnyStyle;
	p->isAntiDistrot = this->isAntiDistrot;
	p->isUseDeburring = this->isUseDeburring;
	p->nDeburring_x = this->nDeburring_x;
	p->nDeburring_y = this->nDeburring_y;
	p->nStartPos = this->nStartPos;
	p->nCounts = this->nCounts;
	p->isDelete0 = this->isDelete0;
	p->m_lTxtCounterStartNum = this->m_lTxtCounterStartNum;
	p->m_lTxtCounterEndNum = this->m_lTxtCounterEndNum;
	p->m_lTxtCounterStep = this->m_lTxtCounterStep;


	p->_OriRect = this->_OriRect;
	p->tracker = this->tracker;
	p->bIsSelected = this->bIsSelected;
	p->bIsLocked = this->bIsLocked;

	return p;
}



void  CText::InitPropList()
{

	CMFCPropertyGridProperty * pProp;
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMyDrawView *pView = (CMyDrawView *)pFrame->GetActiveView();

	pFrame->m_wndProperties.graphs.Add(this);
	//pFrame->m_wndProperties.m_wndObjectCombo.SetCurSel(nGraphics);
	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(0);
	//读取数据
	CString itemname;
	itemname = pProp->GetName(); //获取名称
	COleVariant itemvalue;
	itemvalue = pProp->GetValue();//获取值
	//写入数据
	CString m_startX;
	m_startX.Format(_T("%.2f"), startX / pView->ScaleX);
	pProp->SetValue((_variant_t)(m_startX));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(1);
	CString m_startY;
	m_startY.Format(_T("%.2f"), startY / pView->ScaleY);
	pProp->SetValue((_variant_t)(m_startY));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(0);
	CString m_BarWidth;
	m_BarWidth.Format(_T("%.2f"), (endX - startX) / pView->ScaleX * pView->Zoom);
	pProp->SetValue((_variant_t)(m_BarWidth));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(1);
	CString m_BarHeight;
	m_BarHeight.Format(_T("%.2f"), (endY - startY) / pView->ScaleY * pView->Zoom);
	pProp->SetValue((_variant_t)(m_BarHeight));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(2)->GetSubItem(0);
	CMFCPropertyGridFontPropertyEx * pProp2 = (CMFCPropertyGridFontPropertyEx*)pProp;

	if (pProp2 != NULL)
	{
		pProp2->SetFont(this->MyFont);
		pProp2->SetColor(this->MyColor);
		pProp2->SetName("字体");
	}

	//字段
	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(3)->GetSubItem(0);
	int nCount = 0;

	if (this->nConstValueType == 0 && this->sInsetSqlParam.GetLength() > 0)
	{
		for (int i = 0; i <= pView->nMaxColNum; i++)
		{
			CString tmp;
			tmp.Format(_T("字段%d"), i);
			if (this->sInsetSqlParam.Find(tmp) >= 0)
				nCount = i;
		}
	}
	else if (this->nConstValueType == 2)
	{
		nCount = this->m_nFieldIndex;
	}
	if (nCount > 0)
	{
		CString tmp;
		tmp.Format(_T("字段%d"), nCount);
		pProp->SetValue((_variant_t)(tmp));
	}
	else
		pProp->SetValue((_variant_t)(_T(" ")));


}