// Rectangle.h: interface for the CRectangle class.
//
//////////////////////////////////////////////////////////////////////

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRectangle :public CGraph
{
    DECLARE_SERIAL(CRectangle)
public:
	CRectangle();
	virtual ~CRectangle();

public:
	//int startX;
	//int startY;
	//int endX;
	//int endY;
	LOGPEN LinePen;
	LOGBRUSH MyBrush;

	bool bRound; //�Ƿ�Բ�Ǿ���

	void Serialize(CArchive & ar);
	virtual CGraph* Clone();
	virtual void  InitPropList();

};