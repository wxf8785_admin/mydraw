#if !defined(AFX_HARDWARESETDLG_H__10AC8AE5_0C2E_4B7F_AC8B_5B8107B14047__INCLUDED_)
#define AFX_HARDWARESETDLG_H__10AC8AE5_0C2E_4B7F_AC8B_5B8107B14047__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HardWareSetDlg.h : header file
//
#include "CommFiles/ListCtrlExt.h"

/////////////////////////////////////////////////////////////////////////////
// CHardWareSetDlg dialog

class CHardWareSetDlg : public CDialog
{
// Construction
public:
	CHardWareSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CHardWareSetDlg)
	enum { IDD = IDD_DIALOG_HARDWARESET };
	CStatic	m_PicReserve;
	CStatic	m_PicForward;
	CListCtrlExt	m_nPrinterPosList;
	CTabCtrl	m_HardWareSetTab;
	float	m_EncoderSetting;
	//}}AFX_DATA
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHardWareSetDlg)
	protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


public:
	void CHardWareSetDlg::SetCtrlVisible(int nFlag);
	int CHardWareSetDlg::GetPrinterNo(int SMJETIndex);
	//选择的第nIndex个喷头，是喷头几
	int CHardWareSetDlg::GetPrinterIndex(int SMJETIndex,int nIndex);
	int CHardWareSetDlg::GetSelectPrinter(int SMJETIndex);//喷头选择
	float CHardWareSetDlg::GetInkBoxToEyeDis(int nSMJETIndex, int nIndex);//第几个喷头
	int CHardWareSetDlg::GetInkBoxDirect();
	int CHardWareSetDlg::GetInkBoxDirect(int nSMJETIndex);//新的墨盒方向的方法，每个墨盒都可以设置方向，原来不能
	afx_msg LRESULT onListEditKillFocus(WPARAM wParam, LPARAM lParam);
	float CHardWareSetDlg::GetEncoderSpeed();
	int CHardWareSetDlg::GetBadPointNum(int nSMJET, int Index);////重合点数

	int CHardWareSetDlg::getSelectSMJET();//SMJET选择数目
	int CHardWareSetDlg::getSMJETRealIndex(int SMJETCntIndex);//第SMJETCntIndex个选择的SMJET实际是第几个   譬如：选择集里面的 第3个arm ，实际是arm4，第3个arm没勾上
	int CHardWareSetDlg::getSMJETRealCount(int SMJETCnt);//与上面方法相反， 譬如：arm4 ，实际第3个选中的arm

	CString m_strParamPath;
	void CHardWareSetDlg::SaveInkBoxParamIni();
	void CHardWareSetDlg::ReadInkBoxParamIni();
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CHardWareSetDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeTabHardwareSet(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRadioReserve();
	afx_msg void OnRadioForward();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCheckSmjet1();
	afx_msg void OnBnClickedCheckSmjet2();
	afx_msg void OnBnClickedCheckSmjet3();
	afx_msg void OnBnClickedCheckSmjet4();
	afx_msg void OnBnClickedCheckSmjet5();
	afx_msg void OnBnClickedCheckSmjet6();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnLvnEndlabeleditListPrinterPos(NMHDR *pNMHDR, LRESULT *pResult);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HARDWARESETDLG_H__10AC8AE5_0C2E_4B7F_AC8B_5B8107B14047__INCLUDED_)
