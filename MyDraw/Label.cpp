﻿// Label.cpp : implementation file
//

#include "stdafx.h"
#include "Label.h"


// CColorCStatic

IMPLEMENT_DYNAMIC(CColorCStatic, CStatic)

CColorCStatic::CColorCStatic()
{

}

CColorCStatic::~CColorCStatic()
{
}


BEGIN_MESSAGE_MAP(CColorCStatic, CStatic)
	ON_WM_PAINT()
	//    ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()



// CColorCStatic 消息处理程序



void CColorCStatic::SetTextColor(COLORREF TextColor )
{
	m_TextColor  = TextColor; //设置文字颜色
}

//为CMultiColorStatic类添加“设置背景颜色”接口

void CColorCStatic::SetBackColor(COLORREF BackColor )
{
	m_BackColor  = BackColor; //设置背景颜色
}

//为CMultiColorStatic类添加“设置标题”接口

void CColorCStatic::SetCaption(CString strCaption )
{
	m_strCaption  = strCaption;
}
void CColorCStatic::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CRect rect;
	GetClientRect(&rect );
	CBrush BGBrush;
	BGBrush.CreateSolidBrush(m_BackColor);
	CBrush *pOldBrush = dc.SelectObject(&BGBrush);
	dc.Rectangle(&rect);
	dc.SetBkMode(TRANSPARENT );

	CFont font;
	font.CreatePointFont(400, "宋体");
	//CFont *pFont = GetParent()->GetFont();//得到父窗体的字体
	CFont *pOldFont;
	pOldFont = dc.SelectObject(&font);//选用父窗体的字体
	dc.SetTextColor(m_TextColor);//设置文本颜色
	dc.DrawText(m_strCaption, &rect, DT_CENTER);//将文本画在Static的中央
	dc.SelectObject(pOldFont);
	dc.SelectObject(pOldBrush);
	font.DeleteObject();
	BGBrush.DeleteObject();
	ReleaseDC(&dc);
}
