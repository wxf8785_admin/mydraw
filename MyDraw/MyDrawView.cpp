﻿// MyDrawView.cpp : implementation of the CMyDrawView class
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "MyDraw.h"
#endif

#include "MyDrawDoc.h"
#include "MyDrawView.h"
#include "PrintDlg.h"
#include "MainFrm.h"

#include "Ellipse.h"
#include "Line.h"

#include "LineProperties.h"
#include "EllipseProperties.h"

#include "Rectangle.h"
#include "Text.h"
#include "TextProperties.h"

#include "BarCode.h"
#include "BarCodeProperties.h"
#include "BarCodeProperties11.h"

#include "Pic.h"
#include "PicProperties.h"

#include "QR_Encode.h"
#include "pdf417.h"
#include "ean-13.h"
#include "ElementArray.h"
#include "CMFCPropertyGridFontPropertyEx.h"
#include "intervalSetDlg.h"

#include "TBarCode11.h"

#include  <fstream>
#include <math.h>
#include <vector>
#include "Graph.h"
#include "Invoker.h"
#include "thread_lock.h"


#include <windows.h>
#include <tlhelp32.h>
#include <psapi.h>
#pragma comment (lib,"psapi.lib")

CInvoker* invoker;
MyCommand* com;
CThreadLock  thread;     //声明CThreadLock类型的全局变量
#define KEY_DOWN(VK_NONAME) ((GetAsyncKeyState(VK_NONAME) & 0x8000) ? 1:0) 

#define PI      3.14159265358979323846
#define PI_2    1.57079632679489661923

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyDrawView

IMPLEMENT_DYNCREATE(CMyDrawView, CScrollView)

BEGIN_MESSAGE_MAP(CMyDrawView, CScrollView)
	//{{AFX_MSG_MAP(CMyDrawView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, OnPrint/*&CScrollView::OnFilePrint*/)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CMyDrawView::OnFilePrintPreview)
	ON_WM_LBUTTONDOWN()
	ON_WM_SETCURSOR()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_WM_KEYUP()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
	ON_COMMAND(ID_BUTTON_LINE, OnButtonLine)
	ON_COMMAND(ID_BUTTON_ELLIPSE, OnButtonEllipse)
	ON_COMMAND(ID_BUTTON_RECTANGLE, OnButtonRectangle)
	ON_COMMAND(ID_BUTTON_RECTANGLE2, OnButtonRectangle2)
	ON_COMMAND(ID_BUTTON_ARC, OnButtonArc)
	ON_COMMAND(ID_BUTTON_TEXT, OnButtonText)
	ON_COMMAND(ID_BUTTON_BAR, OnButtonBar)
	ON_COMMAND(ID_BUTTON_TBAR, OnButtonTBar)
	ON_COMMAND(ID_BUTTON_PIC, OnButtonPic)
	ON_COMMAND(ID_ARRAY_DISTRIBUTE, OnArrayDistribute)
	ON_COMMAND(ID_BUTTON_UP, OnButtonUP)
	ON_COMMAND(ID_BUTTON_LEFT, OnButtonLEFT)
	ON_COMMAND(ID_BUTTON_DOWN, OnButtonDOWN)
	ON_COMMAND(ID_BUTTON_RIGHT, OnButtonRIGHT)
	ON_COMMAND(ID_BUTTON_Horizontal, OnButtonHorizontal)
	ON_COMMAND(ID_Button_Vertical, OnButtonVertical)
	ON_COMMAND(ID_button_interval, OnButtonInterval)
	ON_COMMAND(ID_BUTTON_SELECT, OnButtonSelect)

	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, OnUpdateEditRedo)

	ON_COMMAND(ID_BUTTON_Lock, OnPropertiesLock)
	ON_UPDATE_COMMAND_UI(ID_BUTTON_Lock, OnUpdatePropertiesLock)
	//ON_WM_PAINT()
	//}}AFX_MSG_MAP
	ON_WM_RBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_ERASEBKGND()
	ON_WM_KEYDOWN()

	ON_COMMAND(ID_BUTTON_PREVIEW, OnButtonPreview)
	ON_UPDATE_COMMAND_UI(ID_BUTTON_PREVIEW, OnUpdatePreview)
	ON_COMMAND(ID_BUTTON_HOMEPAGE, OnButtonHomePage)
	ON_COMMAND(ID_BUTTON_NEXTPAGEBEFORE, OnButtonNextPageBefore)
	ON_COMMAND(ID_BUTTON_NEXTPAGE, OnButtonNextPage)
	ON_COMMAND(ID_BUTTON_TAILPAGE, OnButtonTailPage)
	ON_EN_CHANGE(ID_BARTEXT, OnPageNumChange)

	ON_COMMAND(ID_EDIT_DELETE, OnDeleteItem)
	ON_COMMAND(ID_EDIT_ATT, OnViewAtt)
	ON_COMMAND(ID_EDIT_LEFTMOVE, OnLeftMove)
	ON_COMMAND(ID_EDIT_RIGHTMOVE, OnRightMove)
	ON_COMMAND(ID_EDIT_UPMOVE, OnUpMove)
	ON_COMMAND(ID_EDIT_DOWNMOVE, OnDownMove)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyDrawView construction/destruction

CMyDrawView::CMyDrawView()
{
	// TODO: add construction code here
	m_pParent = NULL;

	m_tracker.m_nStyle = CRectTracker::resizeInside |
		CRectTracker::dottedLine;
	m_tracker.m_rect.SetRect(0, 0, 0, 0);
	m_tracker.m_nHandleSize = 8;

	nAllPaperNo = 100000000;//如果不导入数据库，一直打印

	nMaxColNum = 0;

	isGetData = false;

	Zoom = 1.0;
	ZoomInX = 1.0f;
	ZoomInY = 1.0f;
	ZoomInX_Pic = 1.0f;
	ZoomInY_Pic = 1.0f;

	//m_nWidth = 100; // Client width
	//m_nHeight = 100;// client height

	m_bLButtonDown = FALSE;
	m_enumCurTool = ToolNone;
	m_nSelectedCount = 0;

	m_bChangeSize = false;
	m_nRetCode = -1;
	invoker = CInvoker::Invoker();
	m_bIsLocked = false;
	m_bIsEnablePrview = FALSE;
	DPIX = 0;
	DPIY = 0;

	PaperLenth = 297;         //页长
	n_PageIndex = 1;          //当前页

	for (int i = 0; i < 6; i++)
	{
		nRepPoint[i][0] = 0;
		nRepPoint[i][1] = 0;
		nRepPoint[i][2] = 0;
		nRepPoint[i][3] = 0;
	}


	CMainFrame* pMain = (CMainFrame*)AfxGetMainWnd();
	CString m_strParamPath = pMain->GetWorkDir();

	int nCheck = ::GetPrivateProfileInt(_T("PrintSet"), _T("MODE_NORMAL"), 1, m_strParamPath);
	if (nCheck)
	{
		int nIndex = ::GetPrivateProfileInt(_T("PrintSet"), _T("NORMAL_PPIX"), 0, m_strParamPath);
		DPIX = nIndex;//当前选中的行。
	}
	else
	{
		int nIndex = ::GetPrivateProfileInt(_T("PrintSet"), _T("HIGHSPEED_PPIX"), 0, m_strParamPath);
		DPIX = nIndex + 1;
	}

}

CMyDrawView::~CMyDrawView()
{
	for (int i = 0; i < 6; i++)
	{	
		if (BitmapNext[i].m_hObject)
			BitmapNext[i].DeleteObject();

		if (MemDCnext[i].m_hDC)
			MemDCnext[i].DeleteDC();
	}
}

BOOL CMyDrawView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO:  在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式
	SetScrollSizes(MM_TEXT, CSize(0, 0));
	return CScrollView::PreCreateWindow(cs);
}

// CMyDrawView 绘制

void CMyDrawView::OnDraw(CDC* pDC)
{
	CMyDrawDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	if (isGetData)
		return;


	// TODO:  在此处为本机数据添加绘制代码

	CDC	MemDC;
	//GetClientRect(&mRect);
	//mRect.Width();   
	//mRect.Height();

	m_nWidth = 6000;
	m_nHeight = 1500;

	{
		DrawSelection();
		//GetDeviceCap获得设备每英寸的像素数
		// 1英寸     =     25.4     毫米 
		//A4纸为 297 X 210
		//	int nLenth = 297;//毫米
		//	int nHeigh = 100;//毫米

		//nLenth = 620;//310mm
		//nHeight 150//75mm

		iLOGPIXELSX = pDC->GetDeviceCaps(LOGPIXELSX);// 得到当前显示设备的水平单位英寸像素数
		iLOGPIXELSY = pDC->GetDeviceCaps(LOGPIXELSY);// 得到当前显示设备的垂直单位英寸像素数
	}

	//X每毫米像素数
	ScaleX = iLOGPIXELSX / 25.4f*Zoom;
	//Y每毫米像素数
	ScaleY = iLOGPIXELSY / 25.4f*Zoom;

	//默认是600*600
	float ScaleX_Print = 300 / 12.7f;
	float ScaleY_Print = 300 / 12.7f;

	//如果是300dpi
	if (DPIX > 0)
	{
		ScaleX_Print = 150 / 12.7f;
		ScaleY_Print = 150 / 12.7f;
	}

	ZoomInX = ScaleX_Print / (iLOGPIXELSX / 25.4f);
	ZoomInY = ScaleY_Print / (iLOGPIXELSY / 25.4f);

	ZoomInX_Pic = ZoomInX;
	ZoomInY_Pic = ZoomInY;

	ZoomInX_Pic /= Zoom;
	ZoomInY_Pic /= Zoom;


	{
		ZoomInX = (float)1.0*Zoom;
		ZoomInY = (float)1.0*Zoom;
	}


	if (!MemDC.CreateCompatibleDC(pDC))
	{
		MessageBox(_T("创建内存设备场景失败!"), NULL, MB_ICONWARNING);//创建一个特殊的内存设备环境
		return;
	}

	CBitmap bitmap ;
	if (Zoom > 2.0)
	{
		bitmap.CreateCompatibleBitmap(pDC, m_nWidth * 2, m_nHeight * 2);
		CBitmap *pBitmap = MemDC.SelectObject(&bitmap);//创建兼容的位图
		//将MemDC的作图区刷新成背景色(白色)
		MemDC.FillSolidRect(0, 0, m_nWidth * 2, m_nHeight * 2, RGB(110, 143, 175));
		DrawUserGraph(&MemDC);
		{
			pDC->BitBlt(0, 0, m_nWidth * 2, m_nHeight * 2, &MemDC, 0, 0, SRCCOPY);

		}
		MemDC.SelectObject(pBitmap);
	}
	else if(Zoom >= 1.0)
	{
		bitmap.CreateCompatibleBitmap(pDC, m_nWidth*ZoomInX, m_nHeight*ZoomInY);
		CBitmap *pBitmap = MemDC.SelectObject(&bitmap);//创建兼容的位图
		//将MemDC的作图区刷新成背景色(白色)
		MemDC.FillSolidRect(0, 0, m_nWidth*ZoomInX, m_nHeight*ZoomInY, RGB(110, 143, 175));
		DrawUserGraph(&MemDC);
		{
			pDC->BitBlt(0, 0, m_nWidth*ZoomInX, m_nHeight*ZoomInY, &MemDC, 0, 0, SRCCOPY);

		}
		MemDC.SelectObject(pBitmap);
	}
	else
	{
		bitmap.CreateCompatibleBitmap(pDC, m_nWidth, m_nHeight);
		CBitmap *pBitmap = MemDC.SelectObject(&bitmap);//创建兼容的位图
		//将MemDC的作图区刷新成背景色(白色)
		MemDC.FillSolidRect(0, 0, m_nWidth, m_nHeight, RGB(110, 143, 175));
		DrawUserGraph(&MemDC);
		{
			pDC->BitBlt(0, 0, m_nWidth, m_nHeight, &MemDC, 0, 0, SRCCOPY);

		}
		MemDC.SelectObject(pBitmap);
	}


	if (m_enumCurTool == ToolSelect && m_bLButtonDown == TRUE && m_nSelectedCount <= 0)
	{
		m_bIsDrawRect = true;
		CRect rect(m_startPoint, m_endPoint);
		rect.NormalizeRect();
		CBrush brush(RGB(0, 0, 0));
		pDC->FrameRect(&rect, &brush);
	}
	else
	{
		m_bIsDrawRect = false;
	}
	bitmap.DeleteObject();
	MemDC.DeleteDC();
	
}


void CMyDrawView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	// TODO: calculate the total size of this view
	m_pParent = ((CMainFrame*)GetParentFrame());

	m_pParent->ShowRulers(TRUE);
	SetScrollSizes(MM_TEXT, CSize(6000 * Zoom, 1500 * Zoom));
	UpdateRulersInfo(RW_POSITION, GetScrollPosition());


	CMFCStatusBar*pStatus = &m_pParent->m_wndStatusBar;//获得主窗口的状态栏指针
	if (pStatus)
	{
		CString str;
		str.Format("view: %d%%", int(Zoom * 100));//格式化文本
		pStatus->SetPaneText(2, str);
	}

	//选择工具
	m_enumCurTool = ToolSelect;

	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_ARROW));//光标离开该区域恢复默认箭头形状
}


void CMyDrawView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	UpdateRulersInfo(RW_HSCROLL, GetScrollPosition());

	CScrollView::OnHScroll(nSBCode, nPos, pScrollBar);
}



void CMyDrawView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: Add your message handler code here and/or call default
	UpdateRulersInfo(RW_VSCROLL, GetScrollPosition());

	CScrollView::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CMyDrawView::UpdateRulersInfo(int nMessage, CPoint ScrollPos, CRect Rect)
{
	if (!m_pParent)
		return;

	stRULER_INFO pRulerInfo;
	pRulerInfo.uMessage = nMessage;
	pRulerInfo.ScrollPos = ScrollPos;
	pRulerInfo.Rect = Rect;

	if (this->Zoom > 2.0)
		pRulerInfo.DocSize = CSize(6000 * 2, 1500 * 2);
	else if (Zoom >= 1.0)
		pRulerInfo.DocSize = CSize(6000 * Zoom, 1500 * Zoom);
	else
		pRulerInfo.DocSize = CSize(6000, 1500);

	pRulerInfo.fZoomFactor = Zoom;

	m_pParent->UpdateRulersInfo(pRulerInfo);
}


void CMyDrawView::UpdateRulersInfo(int nMessage, CPoint ScrollPos, CPoint Pos)
{
	if (!m_pParent)
		return;

	stRULER_INFO pRulerInfo;
	pRulerInfo.uMessage = nMessage;
	pRulerInfo.ScrollPos = ScrollPos;
	pRulerInfo.Pos = Pos;
	
	if (this->Zoom > 2.0)
		pRulerInfo.DocSize = CSize(6000 * 2, 1500 * 2);
	else if (Zoom >= 1.0)
		pRulerInfo.DocSize = CSize(6000 * Zoom, 1500 * Zoom);
	else
		pRulerInfo.DocSize = CSize(6000, 1500);

	pRulerInfo.fZoomFactor = Zoom;

	m_pParent->UpdateRulersInfo(pRulerInfo);
}
// CMyDrawView 打印


void CMyDrawView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CMyDrawView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CMyDrawView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO:  添加额外的打印前进行的初始化过程
}

void CMyDrawView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO:  添加打印后进行的清理过程
}


int CMyDrawView::CoordZoomOrgi(int nPos, int nType)
{
	if (nType == 0)//X轴
	{
		return (int)(nPos / ZoomInX);
	}
	else if (nType == 1)//Y轴
	{
		return (int)(nPos / ZoomInY);
	}
	return 0;
}

int CMyDrawView::CoordZoomOut(int nPos, int nType)
{
	if (nType == 0)//X轴
	{
		return (int)(ZoomInX*nPos);
	}
	else if (nType == 1)//Y轴
	{
		return (int)(ZoomInY*nPos);
	}
	return 0;
}

CMainFrame*pFramePrepare = NULL;
UINT _stdcall NextPageThread(LPVOID lpParam)
{

	NextPageParam *pNextPageParam = (NextPageParam*)lpParam;

	CMyDrawView *pView = (CMyDrawView *)pFramePrepare->GetActiveView();

	//double time = 0;
	//LARGE_INTEGER nFreq;
	//LARGE_INTEGER nBeginTime;
	//LARGE_INTEGER nEndTime;
	//QueryPerformanceFrequency(&nFreq);
	//QueryPerformanceCounter(&nBeginTime);//开始计时  


	pView->PreparedataNext(pNextPageParam->nPageIndex, pNextPageParam->nSMJET);

	//QueryPerformanceCounter(&nEndTime);//停止计时  
	//time = (double)(nEndTime.QuadPart - nBeginTime.QuadPart) / (double)nFreq.QuadPart;//计算程序执行时间单位为s  

	//CString str = "";
	//str.Format(_T("%d,%d,%d"), pNextPageParam->nPageIndex, pNextPageParam->nSMJET, int(time * 1000));
	//AfxMessageBox(str);


	NextPageParam it;
	it.nPageIndex = pNextPageParam->nPageIndex;         //打印头编号
	it.nSMJET = pNextPageParam->nSMJET;              //打印页编号

	{
		CThreadLockHandle lockHandle(&thread);        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁
		//添加状态，如果已经处理，加入map
		map<NextPageParam, int>::iterator iter = pView->m_NextPageParam.find(it);
		if (iter == pView->m_NextPageParam.end())
		{
			pView->m_NextPageParam.insert(make_pair(it, 1));
		}

		if (it.nPageIndex %2000 ==0)  //内存回收
			EmptyWorkingSet(GetCurrentProcess());
	}


	delete pNextPageParam;
	pNextPageParam = NULL;
	return 0;
}



//准备数据
void CMyDrawView::Preparedata(int nPageIndex, int  nSMJET, bool first)
{
	//CClientDC 				dc(this); // device context for painting
	//OnPrepareDC(&dc);


	if (first)
	{
		pFramePrepare = (CMainFrame*)AfxGetApp()->m_pMainWnd;//获得主窗口指标
	}

	//X每毫米像素数
	ScaleX = iLOGPIXELSX / 25.4f;
	//Y每毫米像素数
	ScaleY = iLOGPIXELSY / 25.4f;

	m_nWidth = (int)(PaperLenth* ScaleX + 1);
	m_nHeight = (int)(12.7*ScaleY * 4 * m_nSMJET + 1);

	//默认是600*600
	float ScaleX_Print = 300 / 12.7f;
	float ScaleY_Print = 300 / 12.7f;

	//如果是300dpi
	if (DPIX > 0)
	{
		ScaleX_Print = 150 / 12.7f;
		ScaleY_Print = 150 / 12.7f;
	}

	ZoomInX = ScaleX_Print / (iLOGPIXELSX / 25.4f);
	ZoomInY = ScaleY_Print / (iLOGPIXELSY / 25.4f);

	//if (isGetData)
	{
		ZoomInX = 1.0*ZoomInX;
		ZoomInY = 1.0*ZoomInY;
		ZoomInX_Pic = 1.0;
		ZoomInY_Pic = 1.0;
	}

	if (first)
	{
		CPaintDC 				dc(this); // device context for painting
		OnPrepareDC(&dc);

		//////////////准备下一页////////////////
		if (MemDCnext[nSMJET].m_hDC)
			MemDCnext[nSMJET].DeleteDC();

		if (!MemDCnext[nSMJET].CreateCompatibleDC(&dc))
		{
			MessageBox(_T("创建内存设备场景失败!"), NULL, MB_ICONWARNING);//创建一个特殊的内存设备环境
			return;
		}
		if (BitmapNext[nSMJET].m_hObject)
			BitmapNext[nSMJET].DeleteObject();
		//BitmapNext[nSMJET].CreateCompatibleBitmap(&dc, m_nWidth*ZoomInX, m_nHeight*ZoomInY);//32位 彩色
		BitmapNext[nSMJET].CreateCompatibleBitmap(&MemDCnext[nSMJET], m_nWidth*ZoomInX, m_nHeight*ZoomInY);//单色位图 1位
		CBitmap *pBitmap = MemDCnext[nSMJET].SelectObject(&BitmapNext[nSMJET]);//创建兼容的位图	
		ReleaseDC(&dc);
	}


	if (isGetData)
	{
		//水平方向的dpi，0:600,1:300,2:200,3:150,4:100,5:75
		float Y = 600, X = 600;
		if (DPIY > 0)
		{
			int y[2] = { 600, 300 };
			Y = y[DPIX];

			int x[6] = { 600, 300, 200, 150, 100, 75 };
			X = x[DPIY];
		}
		int xDPI =  Y/X ;
		{
			//CClientDC 				dc(this); // device context for painting
			//OnPrepareDC(&dc);

			CDC MemDC;
			if (!MemDC.CreateCompatibleDC(NULL))
			{
				MessageBox(_T("创建内存设备场景失败!"), NULL, MB_ICONWARNING);//创建一个特殊的内存设备环境
				return;
			}

			CBitmap bitmap ;
			bitmap.CreateCompatibleBitmap(&CClientDC(NULL), m_nWidth*ZoomInX, m_nHeight*ZoomInY);
			CBitmap *pBitmap = MemDC.SelectObject(&bitmap);//创建兼容的位图
			//将MemDC的作图区刷新成背景色(白色)
			MemDC.FillSolidRect(0, 0, m_nWidth*ZoomInX, m_nHeight*ZoomInY, RGB(255, 255, 255));
			{
				CThreadLockHandle lockHandle(&thread);                        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁
				n_PageIndex = nPageIndex;//当前是第几页
				for (int i = 0; i < nMaxColNum; i++)
				{
					//CString sPrint = pFramePrepare->pPrintDlg->m_RecordNumList.GetItemText(nPageIndex - 1, i + 1);
					CString sPrint = pFramePrepare->pPrintDlg->sPrintData[(nPageIndex - 1)*nMaxColNum + i + 1];
					if (sPrint.GetLength() > 0)
					{
						this->UpdateSQLPrintData(sPrint, i + 1);
					}
				}
				DrawUserGraph(&MemDC, first);
			}

			if (first)
			{
				pFramePrepare->hasData[nSMJET] = write_to_binary_file_bitmap(&bitmap, nSMJET, xDPI);
				//保存第一张
				if (nSMJET == 1)
				{		
					CString szbmpName = "";
					szbmpName.Format("%ld-%d", GetTickCount(), nSMJET+1);
					szbmpName += ".bmp";
					SaveBitmapToFile(&bitmap, szbmpName.GetBuffer(szbmpName.GetLength()), nSMJET + 1);
				}
			}
			else
			{
				write_to_binary_file_bitmap(&bitmap, nSMJET, xDPI);
			}
			MemDC.SelectObject(pBitmap);
			bitmap.DeleteObject();
			MemDC.DeleteDC();
			//ReleaseDC(&dc);
		}
	}

	m_bIsDrawRect = false;
}




void CMyDrawView::NextPage(int nPageIndex, int nSMJET)
{


	NextPageParam* pNextPageParam = new NextPageParam;
	pNextPageParam->nPageIndex = nPageIndex;
	pNextPageParam->nSMJET = nSMJET;

	HANDLE hNextPageThread = (HANDLE)_beginthreadex(NULL, 0, NextPageThread, pNextPageParam, 0, NULL);

}




void CMyDrawView::PreparedataNext(int nPageIndex, int  nSMJET)
{

	if (isGetData)
	{
		//水平方向的dpi，0:600,1:300,2:200,3:150,4:100,5:75
		float Y = 600, X = 600;
		if (DPIY > 0)
		{
			int y[2] = { 600, 300 };
			Y = y[DPIX];

			int x[6] = { 600, 300, 200, 150, 100, 75 };
			X = x[DPIY];
		}
		int xDPI = Y/X ;
		//内存中重新把600*600的拷贝下
		{
			//CBitmap *pBitmap = MemDCnext[nSMJET].SelectObject(&BitmapNext[nSMJET]);//创建兼容的位图		
			//将MemDC的作图区刷新成背景色(白色)
			MemDCnext[nSMJET].FillSolidRect(0, 0, m_nWidth*ZoomInX, m_nHeight*ZoomInY, RGB(255, 255, 255));
			{
				CThreadLockHandle lockHandle(&thread);                        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁
				n_PageIndex = nPageIndex;//当前是第几页
				for (int i = 0; i < nMaxColNum; i++)
				{
					//CString sPrint = pFramePrepare->pPrintDlg->m_RecordNumList.GetItemText(nPageIndex - 1, i + 1);
					CString sPrint = pFramePrepare->pPrintDlg->sPrintData[(nPageIndex - 1)*nMaxColNum + i + 1];
					if (sPrint.GetLength() > 0)
					{
						this->UpdateSQLPrintData(sPrint, i + 1);
					}
				}
				DrawUserGraph(&MemDCnext[nSMJET], false);
			}

			{
				write_to_binary_file_bitmap_Next(&BitmapNext[nSMJET], nSMJET, xDPI);
			}
			//MemDCnext[nSMJET].SelectObject(pBitmap);
			//pBitmap = NULL;
		}
	}

	m_bIsDrawRect = false;
}



//void CMyDrawView::OnPaint()
//{
//	CPaintDC				dc(this); // device context for painting
//	OnPrepareDC(&dc);
//	CDC						MemDC;
//
//	GetClientRect(&mRect);
//
//	m_nWidth = mRect.Width();
//	m_nHeight = mRect.Height();
//
//
//	//GetDeviceCap获得设备每英寸的像素数
//	// 1英寸     =     25.4     毫米 
//	//A4纸为 297 X 210
//	//	int nLenth = 297;//毫米
//	//	int nHeigh = 100;//毫米
//
//	//nLenth = 620;//310mm
//	//nHeight 150//75mm
//
//
//	iLOGPIXELSX = dc.GetDeviceCaps(LOGPIXELSX);// 得到当前显示设备的水平单位英寸像素数
//
//	iLOGPIXELSY = dc.GetDeviceCaps(LOGPIXELSY);// 得到当前显示设备的垂直单位英寸像素数
//
//	//X每毫米像素数
//	ScaleX = iLOGPIXELSX / 25.4f*Zoom;
//	//Y每毫米像素数
//	ScaleY = iLOGPIXELSY / 25.4f*Zoom;
//
//	//默认是600*600
//	float ScaleX_Print = 300 / 12.7f;
//	float ScaleY_Print = 300 / 12.7f;
//
//	//CMainFrame*pFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;//获得主窗口指标
//	//如果是300dpi
//	if (DPIX > 0)
//	{
//		ScaleX_Print = 150 / 12.7f;
//		ScaleY_Print = 150 / 12.7f;
//	}
//
//	ZoomInX = ScaleX_Print / (iLOGPIXELSX / 25.4f);
//	ZoomInY = ScaleY_Print / (iLOGPIXELSY / 25.4f);
//
//	ZoomInX_Pic = ZoomInX;
//	ZoomInY_Pic = ZoomInY;
//
//	ZoomInX_Pic /= Zoom;
//	ZoomInY_Pic /= Zoom;
//
//	if (isGetData)
//	{
//		ZoomInX = 1.0*ZoomInX;
//		ZoomInY = 1.0*ZoomInY;
//		ZoomInX_Pic = 1.0;
//		ZoomInY_Pic = 1.0;
//	}
//	else
//	{
//		//OnDrawScale(&dc, 620, 150);//310mm,75mm
//		ZoomInX = (float)1.0*Zoom;
//		ZoomInY = (float)1.0*Zoom;
//	}
//
//	if (!MemDC.CreateCompatibleDC(&dc))
//	{
//		MessageBox(_T("创建内存设备场景失败!"), NULL, MB_ICONWARNING);//创建一个特殊的内存设备环境
//		return;
//	}
//
//	CBitmap *bitmap = new CBitmap;
//	bitmap->CreateCompatibleBitmap(&dc, m_nWidth*ZoomInX, m_nHeight*ZoomInY);
//	CBitmap *pBitmap = MemDC.SelectObject(bitmap);//创建兼容的位图
//	//将MemDC的作图区刷新成背景色(白色)
//	MemDC.FillSolidRect(0, 0, m_nWidth*ZoomInX, m_nHeight*ZoomInY, RGB(255, 255, 255));//GrayColor//GetSysColor(COLOR_3DFACE) 
//
//	DrawUserGraph(&MemDC);
//
//	if (isGetData)
//	{
//		//水平方向的dpi，0:600,1:300,2:200,3:150,4:100,5:75
//		float Y = 600, X = 600;
//		if (DPIY > 0)
//		{
//			int y[2] = { 600, 300 };
//			Y = y[DPIX];
//
//			int x[6] = { 600, 300, 200, 150, 100, 75 };
//			X = x[DPIY];
//		}
//		float xDPI = X / Y;
//		//内存中重新把600*600的拷贝下
//
//		if (fabs(xDPI - 1) > 1e-6)
//		{
//			CDC	MemDC2;
//			if (!MemDC2.CreateCompatibleDC(&dc))
//			{
//				MessageBox(_T("创建内存设备场景失败!"), NULL, MB_ICONWARNING);//创建一个特殊的内存设备环境
//				return;
//			}
//
//			CBitmap *bitmap2 = new CBitmap;
//			bitmap2->CreateCompatibleBitmap(&dc, m_nWidth*ZoomInX*xDPI, m_nHeight*ZoomInY);
//
//			CBitmap *pBitmap2 = MemDC2.SelectObject(bitmap2);//创建兼容的位图
//			//将MemDC的作图区刷新成背景色(白色)
//			MemDC2.FillSolidRect(0, 0, m_nWidth*ZoomInX*xDPI, m_nHeight*ZoomInY, RGB(255, 255, 255));//GrayColor//GetSysColor(COLOR_3DFACE) 
//			MemDC2.StretchBlt(0, 0, m_nWidth*ZoomInX*xDPI, m_nHeight*ZoomInY, &MemDC, 0, 0, m_nWidth*ZoomInX, m_nHeight*ZoomInY, SRCCOPY);
//			//保存
//			write_to_binary_file_bitmap(bitmap2);
//			MemDC2.SelectObject(pBitmap2);
//			delete bitmap2;
//			MemDC2.DeleteDC();
//		}
//		else
//			write_to_binary_file_bitmap(bitmap);
//
//		//保存第一张
//		CString szbmpName = "";
//		szbmpName.Format("%ld", GetTickCount());
//		szbmpName += ".bmp";
//		SaveBitmapToFile(bitmap, szbmpName.GetBuffer(szbmpName.GetLength()));
//
//		//isGetData = false;
//		//Invalidate();		
//	}
//	else
//	{
//		dc.StretchBlt(0, 0, m_nWidth*ZoomInX, m_nHeight*ZoomInY, &MemDC, 0, 0, m_nWidth*ZoomInX, m_nHeight*ZoomInY, SRCCOPY);
//	}
//
//	MemDC.SelectObject(pBitmap);
//	delete bitmap;
//
//	if (m_enumCurTool == ToolSelect && m_bLButtonDown == TRUE && m_nSelectedCount <= 0)
//	{
//		m_bIsDrawRect = true;
//		CRect rect(m_startPoint, m_endPoint);
//		CBrush brush(RGB(0, 0, 0));
//		dc.FrameRect(&rect, &brush);
//	}
//	else
//	{
//		m_bIsDrawRect = false;
//	}
//
//	MemDC.DeleteDC();
//
//	ReleaseDC(&dc);
//
//}
//

void CMyDrawView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	CClientDC dc(this);
	OnPrepareDC(&dc);
	dc.DPtoLP(&point);

	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CMyDrawView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	//theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


bool CMyDrawView::SaveBitmapToFile(CBitmap* bitmap, LPSTR lpFileName, int nSMJET)
{

	HBITMAP hBitmap;    // 为刚才的屏幕位图句柄  
	HDC hDC; //设备描述表    
	int iBits; //当前显示分辨率下每个像素所占字节数    
	WORD wBitCount; //位图中每个像素所占字节数    
	DWORD dwPaletteSize = 0, //定义调色板大小  
		dwBmBitsSize,  //位图中像素字节大小     
		dwDIBSize,   //位图文件大小  
		dwWritten;  //写入文件字节数  
	BITMAP Bitmap; //位图属性结构    
	BITMAPFILEHEADER bmfHdr; //位图文件头结构    
	BITMAPINFOHEADER bi; //位图信息头结构      
	LPBITMAPINFOHEADER lpbi; //指向位图信息头结构    
	HANDLE fh,   //定义文件  
		hDib,    //分配内存句柄  
		hPal,   //调色板句柄  
		hOldPal = NULL;

	//计算位图文件每个像素所占字节数    
	hBitmap = (HBITMAP)*bitmap;
	hDC = CreateDC("DISPLAY", NULL, NULL, NULL);
	iBits = GetDeviceCaps(hDC, BITSPIXEL) * GetDeviceCaps(hDC, PLANES);
	DeleteDC(hDC);

	if (iBits <= 1)
		wBitCount = 1;
	else if (iBits <= 4)
		wBitCount = 4;
	else if (iBits <= 8)
		wBitCount = 8;
	else if (iBits <= 24)
		wBitCount = 24;
	else if (iBits <= 32)
		wBitCount = 32;

	//计算调色板大小    
	if (wBitCount <= 8)
		dwPaletteSize = (1 << wBitCount) * sizeof(RGBQUAD);

	//设置位图信息头结构    
	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&Bitmap);
	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biWidth = Bitmap.bmWidth;
	bi.biHeight = 300 * 4 * nSMJET;
	bi.biPlanes = 1;
	bi.biBitCount = wBitCount;
	bi.biCompression = BI_RGB;
	bi.biSizeImage = 0;
	bi.biXPelsPerMeter = 0;
	bi.biYPelsPerMeter = 0;
	bi.biClrUsed = 0;
	bi.biClrImportant = 0;

	//   dwBmBitsSize = ((Bitmap.bmWidth * wBitCount+31) / 32) * 4 * Bitmap.bmHeight;  
	dwBmBitsSize = ((Bitmap.bmWidth * wBitCount + 31) / 32) * 4 * bi.biHeight;

	//为位图内容分配内存    
	hDib = GlobalAlloc(GHND, dwBmBitsSize + dwPaletteSize + sizeof(BITMAPINFOHEADER));
	lpbi = (LPBITMAPINFOHEADER)GlobalLock(hDib);
	*lpbi = bi;

	// 处理调色板         
	hPal = GetStockObject(DEFAULT_PALETTE);
	if (hPal)
	{
		hDC = ::GetDC(NULL);
		hOldPal = ::SelectPalette(hDC, (HPALETTE)hPal, FALSE);
		RealizePalette(hDC);
	}

	//   获取该调色板下新的像素值    
	//   GetDIBits(hDC, hBitmap, 0, (UINT) Bitmap.bmHeight,    
	//       (LPSTR)lpbi + sizeof (BITMAPINFOHEADER) + dwPaletteSize,  
	//       (LPBITMAPINFO)lpbi, DIB_RGB_COLORS);    
	GetDIBits(hDC, hBitmap, 0, (UINT)300 * 4 * nSMJET,
		(LPSTR)lpbi + sizeof(BITMAPINFOHEADER) + dwPaletteSize,
		(LPBITMAPINFO)lpbi, DIB_RGB_COLORS);


	//恢复调色板       
	if (hOldPal)
	{
		SelectPalette(hDC, (HPALETTE)hOldPal, TRUE);
		RealizePalette(hDC);
		::ReleaseDC(NULL, hDC);
	}

	//创建位图文件              
	fh = CreateFile(lpFileName, GENERIC_WRITE,
		0, NULL, CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

	if (fh == INVALID_HANDLE_VALUE)
		return FALSE;

	//   设置位图文件头    
	bmfHdr.bfType = 0x4D42;     //   "BM"    
	dwDIBSize = sizeof(BITMAPFILEHEADER)
		+ sizeof(BITMAPINFOHEADER)
		+ dwPaletteSize + dwBmBitsSize;
	bmfHdr.bfSize = dwDIBSize;
	bmfHdr.bfReserved1 = 0;
	bmfHdr.bfReserved2 = 0;
	bmfHdr.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER)
		+ (DWORD)sizeof(BITMAPINFOHEADER)
		+ dwPaletteSize;

	//   写入位图文件头    
	WriteFile(fh, (LPSTR)&bmfHdr, sizeof(BITMAPFILEHEADER), &dwWritten, NULL);

	//   写入位图文件其余内容    
	WriteFile(fh, (LPSTR)lpbi, dwDIBSize,
		&dwWritten, NULL);

	//清除            
	GlobalUnlock(hDib);
	GlobalFree(hDib);
	CloseHandle(fh);

	return TRUE;
}



//保存到BMP文件
BOOL SaveBmp(HBITMAP hBitmap, CString FileName)
{
	HDC hDC;
	//当前分辨率下每象素所占字节数 
	int iBits;
	//位图中每象素所占字节数 
	WORD wBitCount;
	//定义调色板大小， 位图中像素字节大小 ，位图文件大小 ， 写入文件字节数 
	DWORD dwPaletteSize = 0, dwBmBitsSize = 0, dwDIBSize = 0, dwWritten = 0;
	//位图属性结构 
	BITMAP Bitmap;
	//位图文件头结构 
	BITMAPFILEHEADER bmfHdr;
	//位图信息头结构 
	BITMAPINFOHEADER bi;
	//指向位图信息头结构 
	LPBITMAPINFOHEADER lpbi;
	//定义文件，分配内存句柄，调色板句柄 
	HANDLE fh, hDib, hPal, hOldPal = NULL;

	//计算位图文件每个像素所占字节数 
	hDC = CreateDC("DISPLAY", NULL, NULL, NULL);
	iBits = GetDeviceCaps(hDC, BITSPIXEL) * GetDeviceCaps(hDC, PLANES);
	DeleteDC(hDC);
	if (iBits <= 1) wBitCount = 1;
	else if (iBits <= 4) wBitCount = 4;
	else if (iBits <= 8) wBitCount = 8;
	else wBitCount = 24;

	GetObject(hBitmap, sizeof(Bitmap), (LPSTR)&Bitmap);
	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biWidth = Bitmap.bmWidth;
	bi.biHeight = Bitmap.bmHeight;
	bi.biPlanes = 1;
	bi.biBitCount = wBitCount;
	bi.biCompression = BI_RGB;
	bi.biSizeImage = 0;
	bi.biXPelsPerMeter = 0;
	bi.biYPelsPerMeter = 0;
	bi.biClrImportant = 0;
	bi.biClrUsed = 0;

	dwBmBitsSize = ((Bitmap.bmWidth * wBitCount + 31) / 32) * 4 * Bitmap.bmHeight;

	//为位图内容分配内存 
	hDib = GlobalAlloc(GHND, dwBmBitsSize + dwPaletteSize + sizeof(BITMAPINFOHEADER));
	lpbi = (LPBITMAPINFOHEADER)GlobalLock(hDib);
	*lpbi = bi;

	// 处理调色板 
	hPal = GetStockObject(DEFAULT_PALETTE);
	if (hPal)
	{
		hDC = ::GetDC(NULL);
		//hDC = m_pDc->GetSafeHdc(); 
		hOldPal = ::SelectPalette(hDC, (HPALETTE)hPal, FALSE);
		RealizePalette(hDC);
	}
	// 获取该调色板下新的像素值 
	GetDIBits(hDC, hBitmap, 0, (UINT)Bitmap.bmHeight, (LPSTR)lpbi + sizeof(BITMAPINFOHEADER)
		+ dwPaletteSize, (BITMAPINFO *)lpbi, DIB_RGB_COLORS);

	//恢复调色板 
	if (hOldPal)
	{
		::SelectPalette(hDC, (HPALETTE)hOldPal, TRUE);
		RealizePalette(hDC);
		::ReleaseDC(NULL, hDC);
	}


	//创建位图文件 
	fh = CreateFile(FileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

	if (fh == INVALID_HANDLE_VALUE) return FALSE;

	// 设置位图文件头 
	bmfHdr.bfType = 0x4D42; // "BM" 
	dwDIBSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + dwPaletteSize + dwBmBitsSize;
	bmfHdr.bfSize = dwDIBSize;
	bmfHdr.bfReserved1 = 0;
	bmfHdr.bfReserved2 = 0;
	bmfHdr.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER) + (DWORD)sizeof(BITMAPINFOHEADER) + dwPaletteSize;
	// 写入位图文件头 
	WriteFile(fh, (LPSTR)&bmfHdr, sizeof(BITMAPFILEHEADER), &dwWritten, NULL);
	// 写入位图文件其余内容 
	WriteFile(fh, (LPSTR)lpbi, dwDIBSize, &dwWritten, NULL);
	//清除 
	GlobalUnlock(hDib);
	GlobalFree(hDib);
	CloseHandle(fh);

	return TRUE;
}



void CMyDrawView::DrawUserGraph(CDC* pDC, bool first)
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;

	//数据库字段使用次数统计    //黑色的是固定数据，绿色的是一个字段，蓝色的是出现两次相通字段，红色是出现三次以上都是红色，
	map<int, int>m_nCountsUse;
	CObject* pObject = NULL;
	if (!isGetData)
	{
		CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		if (pMainFrame != NULL)
		{
			PaperLenth = pMainFrame->m_nPaperLenth;
		}
		m_nSMJET = 1;
		if (pMainFrame != NULL)
			m_nSMJET = pMainFrame->pHardWareSetDlg->getSelectSMJET();

		for (int i = 0; i <m_nSMJET; i++)
		{
			for (int j = 0; j<4; j++)
				SetRepPoint(i, j, pMainFrame->pHardWareSetDlg->GetBadPointNum(i, j));
		}

		int nHeigh = 300;///每个画布高300个点，4个1200
		//如果是300dpi
		if (DPIX > 0)
		{
			nHeigh = 150;
		}

		int nRep = 0;       //已发生的重合点

		for (int i =0 ; i < 4 * m_nSMJET; i++)
		{
			
			if (i % 2 == 1)
			{
				pDC->FillSolidRect(0, (int)(12.7*ScaleY*i - nRep), PaperLenth* ScaleX, (int)(12.7*ScaleY) - (int)(nRepPoint[(int)(i / 4)][(i % 4)] * 12.7*ScaleY / nHeigh) - 1, RGB(255, 255, 255));
			}
			else
			{
				pDC->FillSolidRect(0, (int)(12.7*ScaleY*i - nRep), PaperLenth* ScaleX, (int)(12.7*ScaleY) - (int)(nRepPoint[(int)(i / 4)][(i % 4)] * 12.7*ScaleY / nHeigh) - 1, RGB(237, 237, 237));
			}

			nRep += (int)(nRepPoint[(int)(i / 4)][(i % 4)] * 12.7*ScaleY / nHeigh);//重合点数

		}

		//遍历数据库使用次数

		POSITION pos = m_ObjectList.GetHeadPosition();
		while (pos != NULL)
		{
			pObject = (CObject*)m_ObjectList.GetNext(pos);
			if (pObject->IsKindOf(RUNTIME_CLASS(CEllipse)))
			{

			}
			else if (pObject->IsKindOf(RUNTIME_CLASS(CPic)))
			{

			}
			else if (pObject->IsKindOf(RUNTIME_CLASS(CLine)))
			{

			}
			else if (pObject->IsKindOf(RUNTIME_CLASS(CArc)))
			{

			}
			else if (pObject->IsKindOf(RUNTIME_CLASS(CText)))
			{
				pText = (CText*)pObject;
				if (pText != NULL)
				{
					if (pText->nConstValueType == 0 && pText->sInsetSqlParam.GetLength() > 0)
					{
						for (int i = 0; i <= nMaxColNum; i++)
						{
							CString tmp;
							tmp.Format(_T("字段%d"), i);
							if (pText->sInsetSqlParam.Find(tmp) >= 0)
								m_nCountsUse[i]++;
						}
					}
					else if (pText->nConstValueType == 2)
					{
						int nCount = pText->m_nFieldIndex;
						m_nCountsUse[nCount]++;
					}
				}

			}
			else if (pObject->IsKindOf(RUNTIME_CLASS(CRectangle)))
			{

			}
			else if (pObject->IsKindOf(RUNTIME_CLASS(CBarCode)))
			{

			}
			else if (pObject->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
			{
				pTBarCode11 = (CTBarCode11*)pObject;
				if (pTBarCode11 != NULL)
				{
					if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->sInsetParam.GetLength() > 0)
					{
						for (int i = 0; i <= nMaxColNum; i++)
						{
							CString tmp;
							tmp.Format(_T("字段%d"), i);
							if (pTBarCode11->sInsetParam.Find(tmp) >= 0)
								m_nCountsUse[i]++;
						}

					}
					else if (pTBarCode11->m_nBarPropertyType == 2)
					{
						int nCount = pTBarCode11->m_nSqlTypeFieldIndex;
						m_nCountsUse[nCount]++;
					}
				}
			}
		}

	}

	int nOffsetx = m_endPoint.x - m_movePoint.x;
	int nOffsety = m_endPoint.y - m_movePoint.y;
	Offset_X = nOffsetx;
	Offset_Y = nOffsety;

	m_movePoint = m_endPoint;

	POSITION pos = m_ObjectList.GetHeadPosition();
	while (pos != NULL)
	{
		pObject = (CObject*)m_ObjectList.GetNext(pos);
		if (pObject->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
#pragma region CEllipse
			pEllipse = (CEllipse*)pObject;
			if (pEllipse != NULL)
			{
				if (pEllipse->bIsSelected && !pEllipse->bIsLocked &&!isGetData)
				{
					AdjustRectSize(pEllipse->tracker, m_movePoint, pEllipse->startX, pEllipse->startY, pEllipse->endX, pEllipse->endY, nOffsetx, nOffsety, pObject);

					pEllipse->startX = CoordZoomOrgi(pEllipse->tracker.m_rect.left, 0);
					pEllipse->startY = CoordZoomOrgi(pEllipse->tracker.m_rect.top, 1);
					pEllipse->endX = CoordZoomOrgi(pEllipse->tracker.m_rect.right, 0);
					pEllipse->endY = CoordZoomOrgi(pEllipse->tracker.m_rect.bottom, 1);
				}
				DrawEllipse(pDC, pEllipse);
				if (pEllipse->bIsSelected &&!isGetData)
				{
					pEllipse->tracker.Draw(pDC);
				}
			}
#pragma endregion
		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CPic)))
		{
#pragma region CPic
			pPic = (CPic*)pObject;
			if (pPic != NULL)
			{
				if (pPic->bIsSelected && !pPic->bIsLocked && !isGetData)
				{
					AdjustRectSize(pPic->tracker, m_movePoint, pPic->startX, pPic->startY, pPic->nWidth, pPic->nHeight, nOffsetx, nOffsety, pObject, pPic->m_isFixScale);

					pPic->startX = CoordZoomOrgi(pPic->tracker.m_rect.left, 0);
					pPic->startY = CoordZoomOrgi(pPic->tracker.m_rect.top, 1);
					int aax = CoordZoomOrgi(pPic->tracker.m_rect.right, 0);
					int aay = CoordZoomOrgi(pPic->tracker.m_rect.bottom, 1);
					pPic->nWidth = aax - pPic->startX;
					pPic->nHeight = aay - pPic->startY;
				}
				DrawPic(pDC, pPic);
				if (pPic->bIsSelected && !isGetData)
				{
					pPic->tracker.Draw(pDC);
				}
			}
#pragma endregion
		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CLine)))
		{
#pragma region CLine
			pLine = (CLine*)pObject;
			if (pLine != NULL)
			{
				if (pLine->bIsSelected && !pLine->bIsLocked && !isGetData)
				{
					AdjustRectSize(pLine->tracker, m_movePoint, pLine->startX, pLine->startY, pLine->endX, pLine->endY, nOffsetx, nOffsety, pObject);

					pLine->startX = CoordZoomOrgi(pLine->tracker.m_rect.left, 0);
					pLine->startY = CoordZoomOrgi(pLine->tracker.m_rect.top, 1);
					pLine->endX = CoordZoomOrgi(pLine->tracker.m_rect.right, 0);
					pLine->endY = CoordZoomOrgi(pLine->tracker.m_rect.bottom, 1);
				}
				DrawLine(pDC, pLine);
				if (pLine->bIsSelected && !isGetData)
				{
					pLine->tracker.Draw(pDC);
				}
			}
#pragma endregion
		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CArc)))
		{
#pragma region CArc
			pArc = (CArc*)pObject;
			if (pArc != NULL)
			{
				if (pArc->bIsSelected && !pArc->bIsLocked && !isGetData)
				{
					AdjustRectSize(pArc->tracker, m_movePoint, pArc->startX, pArc->startY, pArc->endX, pArc->endY, nOffsetx, nOffsety, pObject);

					pArc->startX = CoordZoomOrgi(pArc->tracker.m_rect.left, 0);
					pArc->startY = CoordZoomOrgi(pArc->tracker.m_rect.top, 1);
					pArc->endX = CoordZoomOrgi(pArc->tracker.m_rect.right, 1);
					pArc->endY = CoordZoomOrgi(pArc->tracker.m_rect.bottom, 1);
				}
				DrawArc(pDC, pArc);
				if (pArc->bIsSelected && !isGetData)
				{
					pArc->tracker.Draw(pDC);
				}
			}
#pragma endregion
		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CText)))
		{
#pragma region CText
			pText = (CText*)pObject;
			if (pText != NULL)
			{
				if (pText->bIsSelected && !pText->bIsLocked && !isGetData)
				{
					AdjustRectSize(pText->tracker, m_movePoint, pText->startX, pText->startY, pText->endX, pText->endY, nOffsetx, nOffsety, pObject);

					pText->startX = CoordZoomOrgi(pText->tracker.m_rect.left, 0);
					pText->startY = CoordZoomOrgi(pText->tracker.m_rect.top, 1);
					pText->endX = CoordZoomOrgi(pText->tracker.m_rect.right, 0);
					pText->endY = CoordZoomOrgi(pText->tracker.m_rect.bottom, 1);
				}


				if (!isGetData)
				{
					int nCount = 0;
					if (pText->nConstValueType == 0 && pText->sInsetSqlParam.GetLength() > 0)
					{
						nCount = pText->m_nFieldIndex;

						for (int i = 0; i <= nMaxColNum; i++)
						{
							CString tmp;
							tmp.Format(_T("字段%d"), i);
							if (pText->sInsetSqlParam.Find(tmp) >= 0)
								nCount = i;
						}

					}
					else if (pText->nConstValueType == 2)
					{
						nCount = pText->m_nFieldIndex;
					}
					//黑色的是固定数据，绿色的是一个字段，蓝色的是出现两次相通字段，红色是出现三次以上都是红色，
					COLORREF pColor = RGB(0, 0, 0);
					if (m_nCountsUse[nCount] == 1)
						pColor = RGB(0, 255, 0);
					if (m_nCountsUse[nCount] == 2)
						pColor = RGB(0, 0, 255);
					if (m_nCountsUse[nCount] >= 3)
						pColor = RGB(255, 0, 0);

					pText->MyColor = pColor;
				}

				DrawMyText(pDC, pText, first);
				if (pText->bIsSelected && !isGetData)
				{
					pText->tracker.Draw(pDC);
				}
			}
#pragma endregion
		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
#pragma region CRectangle
			pRectangle = (CRectangle*)pObject;
			if (pRectangle != NULL)
			{
				if (pRectangle->bIsSelected && !pRectangle->bIsLocked && !isGetData)
				{
					AdjustRectSize(pRectangle->tracker, m_movePoint, pRectangle->startX, pRectangle->startY, pRectangle->endX, pRectangle->endY, nOffsetx, nOffsety, pObject);

					pRectangle->startX = CoordZoomOrgi(pRectangle->tracker.m_rect.left, 0);
					pRectangle->startY = CoordZoomOrgi(pRectangle->tracker.m_rect.top, 1);
					pRectangle->endX = CoordZoomOrgi(pRectangle->tracker.m_rect.right, 0);
					pRectangle->endY = CoordZoomOrgi(pRectangle->tracker.m_rect.bottom, 1);
				}
				DrawRectangle(pDC, pRectangle);
				if (pRectangle->bIsSelected && !isGetData)
				{
					pRectangle->tracker.Draw(pDC);
				}
			}
#pragma endregion
		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
#pragma region pBarCode
			pBarCode = (CBarCode*)pObject;
			if (pBarCode != NULL)
			{
				int CodeType = pBarCode->GetCodeType();
				char* pCode = pBarCode->GetCodeString();
				int nLenth = pBarCode->GetEncodeLength();
				int nLenth1 = pBarCode->getBitMapLenth();
				int nBitmapHeight = pBarCode->nBitMapHeight;

				if (pBarCode->bIsSelected && !pBarCode->bIsLocked && !isGetData)
				{
					AdjustRectSize(pBarCode->tracker, m_movePoint, pBarCode->startX, pBarCode->startY, pBarCode->endX, pBarCode->endY, nOffsetx, nOffsety, pObject);

					pBarCode->startX = CoordZoomOrgi(pBarCode->tracker.m_rect.left, 0);
					pBarCode->startY = CoordZoomOrgi(pBarCode->tracker.m_rect.top, 1);
					pBarCode->endX = CoordZoomOrgi(pBarCode->tracker.m_rect.right, 0);
					pBarCode->endY = CoordZoomOrgi(pBarCode->tracker.m_rect.bottom, 1);

					int nTextDis = (pBarCode->TextFont.lfHeight + pBarCode->nTextBarDis);
					if (pBarCode->GetCodeType() == 8)//二维码
						pBarCode->nBitMapHeight = pBarCode->endY - pBarCode->startY;
					else if (pBarCode->GetCodeType() == 9)//二维码
						pBarCode->nBitMapHeight = pBarCode->endY - pBarCode->startY;
					else if (pBarCode->GetCodeType() == 10)//二维码
						pBarCode->nBitMapHeight = pBarCode->endY - pBarCode->startY;
					else if (pBarCode->m_nRotateTyp == 0)
						pBarCode->nBitMapHeight = pBarCode->endY - pBarCode->startY - nTextDis;
					else if (pBarCode->m_nRotateTyp == 1)
						pBarCode->nBitMapHeight = pBarCode->endX - pBarCode->startX - nTextDis;
					else if (pBarCode->m_nRotateTyp == 2)
						pBarCode->nBitMapHeight = pBarCode->endY - pBarCode->startY - nTextDis;
					else if (pBarCode->m_nRotateTyp == 3)
						pBarCode->nBitMapHeight = pBarCode->endX - pBarCode->startX - nTextDis;

				}
				if (isGetData)
				{
					pBarCode->startX;
					pBarCode->startY;
					pBarCode->endX;
					pBarCode->endY;
					CString TMP = "";
					TMP.Format("%d-%d-%d-%d", pBarCode->startX, pBarCode->startY, pBarCode->endX, pBarCode->endY);
				}

				CString sCodeString = "";
				sCodeString.Format("%s", pCode);

				if (isGetData)
				{
					if (pBarCode->m_nBarPropertyType == 0)//固定字符
					{
						CString sInsertData = pBarCode->sInsetParam;

						sCodeString = ProcessInsertData(sInsertData, sCodeString, !isGetData);
					}
					else if (pBarCode->m_nBarPropertyType == 1)//计数器
					{

					}
					else if (pBarCode->m_nBarPropertyType == 2)//数据库
					{
						if (pBarCode->sSqlData != "")
							sCodeString = pBarCode->sSqlData;
					}
					else if (pBarCode->m_nBarPropertyType == 3)//日期时间
					{
					}
				}
				pCode = sCodeString.GetBuffer(sCodeString.GetLength());
				COLORREF clrBar = RGB(0, 0, 0);
				COLORREF clrSpace = RGB(255, 255, 255);

				if (CodeType == 1)
				{
					Barcode39 code;
					code.Encode39(pCode);
					code.setBitMapLenth(pBarCode->getBitMapLenth());
					code.m_nTop = pBarCode->m_nTop;
					code.m_nBottom = pBarCode->m_nBottom;

					nLenth = P_DrawBarcode(pDC, CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						(int)(pBarCode->nBitMapHeight*ZoomInY), (int)(pBarCode->nBitMapHeight*ZoomInY), clrBar, clrSpace, pBarCode->nPenWidth*ZoomInX, &code, pBarCode->m_nFrameType, pBarCode->m_nRotateTyp);

					pBarCode->setBitMapLenth(nLenth);
					OnDrawBarCodeText(pDC, pBarCode, first);


				}
				else if (CodeType == 2)
				{
					Barcode93 code;
					code.Encode93(pCode);
					code.setBitMapLenth(pBarCode->getBitMapLenth());
					code.m_nTop = pBarCode->m_nTop;
					code.m_nBottom = pBarCode->m_nBottom;

					nLenth = P_DrawBarcode(pDC, CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						(int)(pBarCode->nBitMapHeight*ZoomInY), (int)(pBarCode->nBitMapHeight*ZoomInY), clrBar, clrSpace, pBarCode->nPenWidth*ZoomInX, &code, pBarCode->m_nFrameType, pBarCode->m_nRotateTyp);
					pBarCode->setBitMapLenth(nLenth);
					OnDrawBarCodeText(pDC, pBarCode, first);

				}
				else if (CodeType == 3)
				{
					Barcode128 code;
					code.Encode128A(pCode);
					code.setBitMapLenth(pBarCode->getBitMapLenth());
					code.m_nTop = pBarCode->m_nTop;
					code.m_nBottom = pBarCode->m_nBottom;

					nLenth = P_DrawBarcode(pDC, CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						(int)(pBarCode->nBitMapHeight*ZoomInY), (int)(pBarCode->nBitMapHeight*ZoomInY), clrBar, clrSpace, pBarCode->nPenWidth*ZoomInX, &code, pBarCode->m_nFrameType, pBarCode->m_nRotateTyp);
					pBarCode->setBitMapLenth(nLenth);
					OnDrawBarCodeText(pDC, pBarCode, first);
				}

				else if (CodeType == 4)
				{
					Barcode128 code;
					code.Encode128B(pCode);
					code.setBitMapLenth(pBarCode->getBitMapLenth());
					code.m_nTop = pBarCode->m_nTop;
					code.m_nBottom = pBarCode->m_nBottom;

					nLenth = P_DrawBarcode(pDC, CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						(int)(pBarCode->nBitMapHeight*ZoomInY), (int)(pBarCode->nBitMapHeight*ZoomInY), clrBar, clrSpace, pBarCode->nPenWidth*ZoomInX, &code, pBarCode->m_nFrameType, pBarCode->m_nRotateTyp);
					pBarCode->setBitMapLenth(nLenth);
					OnDrawBarCodeText(pDC, pBarCode, first);

				}
				else if (CodeType == 5)
				{
					Barcode128 code;
					code.Encode128C(pCode);
					code.setBitMapLenth(pBarCode->getBitMapLenth());
					code.m_nTop = pBarCode->m_nTop;
					code.m_nBottom = pBarCode->m_nBottom;

					nLenth = P_DrawBarcode(pDC, CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						(int)(pBarCode->nBitMapHeight*ZoomInY), (int)(pBarCode->nBitMapHeight*ZoomInY), clrBar, clrSpace, pBarCode->nPenWidth*ZoomInX, &code, pBarCode->m_nFrameType, pBarCode->m_nRotateTyp);
					pBarCode->setBitMapLenth(nLenth);
					OnDrawBarCodeText(pDC, pBarCode, first);

				}
				else if (CodeType == 6)
				{
					BarcodeI2of5 code;
					code.EncodeI2of5(pCode);
					code.setBitMapLenth(pBarCode->getBitMapLenth());
					code.m_nTop = pBarCode->m_nTop;
					code.m_nBottom = pBarCode->m_nBottom;

					nLenth = P_DrawBarcode(pDC, CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						(int)(pBarCode->nBitMapHeight*ZoomInY), (int)(pBarCode->nBitMapHeight*ZoomInY), clrBar, clrSpace, pBarCode->nPenWidth*ZoomInX, &code, pBarCode->m_nFrameType, pBarCode->m_nRotateTyp);
					pBarCode->setBitMapLenth(nLenth);
					OnDrawBarCodeText(pDC, pBarCode, first);

				}
				else if (CodeType == 7)
				{
					//BarcodeEan13 code;
					//code.EncodeEan13(pCode);
					//code.setBitMapLenth(pBarCode->getBitMapLenth());

					//nLenth = P_DrawBarcode(pDC,CoordZoomOut(pBarCode->startX,0),CoordZoomOut(pBarCode->startY,1),
					//	(int)(pBarCode->nBitMapHeight*ZoomInY),(int)(pBarCode->nBitMapHeight*ZoomInY)+10,clrBar,clrSpace,pBarCode->nPenWidth*ZoomInX,&code,pBarCode->m_nFrameType,pBarCode->m_nRotateTyp);
					//pBarCode->setBitMapLenth(nLenth);
					//OnDrawBarCodeText(pDC,pBarCode);


					nLenth = P_DrawEAN13code(pDC, CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						(int)(pBarCode->nBitMapHeight*ZoomInY), (int)(pBarCode->nBitMapHeight*ZoomInY), pBarCode->nPenWidth*ZoomInX, sCodeString, pBarCode->m_nFrameType, pBarCode->m_nRotateTyp,
						pBarCode->getBitMapLenth());
					pBarCode->setBitMapLenth(nLenth);

					OnDrawBarCodeText(pDC, pBarCode, first);
				}

				else if (CodeType == 8) //QR 二维码
				{
					nLenth = P_DrawQRcode(pDC, CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						(int)(pBarCode->nBitMapHeight*ZoomInY), (int)(pBarCode->nBitMapHeight*ZoomInY), sCodeString, pBarCode->m_nFrameType, pBarCode->m_nRotateTyp,
						pBarCode->nQRLevel, pBarCode->nQRVersion, pBarCode->bQRAutoExtent, pBarCode->nQRMaskingNo, pBarCode->fQRRotateAngle);
					pBarCode->setBitMapLenth(nLenth);
				}

				else if (CodeType == 9) //DataMatrix 二维码
				{
					nLenth = P_DrawDMcode(pDC, CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						(int)(pBarCode->nBitMapHeight*ZoomInY), (int)(pBarCode->nBitMapHeight*ZoomInY), sCodeString, pBarCode->m_nFrameType, pBarCode->m_nRotateTyp);
					pBarCode->setBitMapLenth(nLenth);
				}

				else if (CodeType == 10) //PDF417 二维码
				{
					nLenth = P_DrawPDF417code(pDC, CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						(int)(pBarCode->nBitMapHeight*ZoomInY), (int)(pBarCode->nBitMapHeight*ZoomInY), sCodeString, pBarCode->m_nFrameType, pBarCode->m_nRotateTyp);
					pBarCode->setBitMapLenth(nLenth);
				}

				if (pBarCode->bIsSelected && !isGetData)
				{
					pBarCode->tracker.Draw(pDC);
				}

			}
#pragma endregion
		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
#pragma region pTBarCode11
			pTBarCode11 = (CTBarCode11*)pObject;
			if (pTBarCode11 != NULL)
			{
				if (pTBarCode11->bIsSelected && !pTBarCode11->bIsLocked && !isGetData)
				{
					AdjustRectSize(pTBarCode11->tracker, m_movePoint, pTBarCode11->startX, pTBarCode11->startY, pTBarCode11->endX, pTBarCode11->endY, nOffsetx, nOffsety, pObject);
					pTBarCode11->startX = CoordZoomOrgi(pTBarCode11->tracker.m_rect.left, 0);
					pTBarCode11->startY = CoordZoomOrgi(pTBarCode11->tracker.m_rect.top, 1);
					pTBarCode11->endX = CoordZoomOrgi(pTBarCode11->tracker.m_rect.right, 0);
					pTBarCode11->endY = CoordZoomOrgi(pTBarCode11->tracker.m_rect.bottom, 1);
				}

				CString sCodeString = pTBarCode11->GetData();
				if (isGetData)
				{
					if (pTBarCode11->m_nBarPropertyType == 0)//固定字符
					{
						CString sInsertData = pTBarCode11->sInsetParam;
						sCodeString = ProcessInsertData(sInsertData, sCodeString, !isGetData);
					}
					else if (pTBarCode11->m_nBarPropertyType == 1)//计数器
					{
						CString strCounter = GetCounterText(pTBarCode11->nCounterStart, pTBarCode11->nCounterEnd, pTBarCode11->nCounterStep, n_PageIndex);
						sCodeString = strCounter;
						//if (strCounter.GetLength() > 0)
						//{
						//	pTBarCode11->nCounterStart = _ttol(_T(strCounter));
						//}
					}
					else if (pTBarCode11->m_nBarPropertyType == 2)//数据库
					{
						if (pTBarCode11->sSqlData != "")
							sCodeString = pTBarCode11->sSqlData;
					}
					else if (pTBarCode11->m_nBarPropertyType == 3)//日期时间
					{
						CString strTime = GetNowTime(pTBarCode11->m_nTimeFormatIndex);
						strTime.Replace(':', '-');
						sCodeString = strTime;
					}
				}

				else if(pTBarCode11->m_nBarPropertyType == 3)//日期时间
				{
					CString strTime = GetNowTime(pTBarCode11->m_nTimeFormatIndex);
					strTime.Replace(':', '-');
					sCodeString = strTime;
				}

				


				BCSetText(pTBarCode11->m_pBarCode, sCodeString, sCodeString.GetLength());
				ERRCODE eCode = S_OK;
				if (first)
				{

					//文本对齐方式
					if (pTBarCode11->nTextAlineType == 0)
					{
						BCSetTextAlignment(pTBarCode11->m_pBarCode, e_BCAlign::eAlDefault);
					}
					else if (pTBarCode11->nTextAlineType == 1)
					{
						BCSetTextAlignment(pTBarCode11->m_pBarCode, e_BCAlign::eAlLeft);
					}
					else if (pTBarCode11->nTextAlineType == 2)
					{
						BCSetTextAlignment(pTBarCode11->m_pBarCode, e_BCAlign::eAlRight);
					}
					else if (pTBarCode11->nTextAlineType == 3)
					{
						BCSetTextAlignment(pTBarCode11->m_pBarCode, e_BCAlign::eAlCenter);
					}
					//边框 					
					if (pTBarCode11->m_nFrameType == 1)//上边框
					{
						BCSetBearerBarType(pTBarCode11->m_pBarCode, e_BearerBar::eBearerBar_Top);
					}
					else if (pTBarCode11->m_nFrameType == 2)//下边框
					{
						BCSetBearerBarType(pTBarCode11->m_pBarCode, e_BearerBar::eBearerBar_Bottom);
					}
					else if (pTBarCode11->m_nFrameType == 3)//上下边框
					{
						BCSetBearerBarType(pTBarCode11->m_pBarCode, e_BearerBar::eBearerBar_TopAndBottom);
					}

					//条码的文本是否显示，是否显示在上面
					BCSetPrintText(pTBarCode11->m_pBarCode, pTBarCode11->bReadable, pTBarCode11->bAbove);

					BCSetMirror(pTBarCode11->m_pBarCode, pTBarCode11->m_bMirror);

					//文本与条码的间距
					//if (!pTBarCode11->isDefaultTextBarDis)
					{
						BCSetTextDist(pTBarCode11->m_pBarCode, pTBarCode11->nTextBarDis);
					}
					//Quiet Zoon


					//LPRECTD a =  BCGetQuietZone(pTBarCode11->m_pBarCode);
					//e_MUnit b =  BCGetQuietZoneUnit(pTBarCode11->m_pBarCode);

					if (pTBarCode11->m_QietZoonUnit)
					{
						RECTD lpRect;
						lpRect.left = pTBarCode11->m_nLeft;
						lpRect.right = pTBarCode11->m_nRight;
						lpRect.top = pTBarCode11->m_nTop;
						lpRect.bottom = pTBarCode11->m_nBottom;

						if (pTBarCode11->m_QietZoonUnit != 2 && pTBarCode11->m_QietZoonUnit != 4)//排查异常情况
							BCSetQuietZone(pTBarCode11->m_pBarCode, &lpRect, e_MUnit(pTBarCode11->m_QietZoonUnit));
					}
					else
					{
						RECTD lpRect;
						lpRect.left = 0;
						lpRect.right = 0;
						lpRect.top = 0;
						lpRect.bottom = 0;

						BCSetQuietZone(pTBarCode11->m_pBarCode, &lpRect, e_MUnit(pTBarCode11->m_QietZoonUnit));
					}


					int CodeType = pTBarCode11->GetCodeType();
					if (CodeType == 8) //QR 二维码
					{
						BCSet_QR_ECLevel(pTBarCode11->m_pBarCode, (e_QRECLevel)pTBarCode11->nQRLevel);
						BCSet_QR_Version(pTBarCode11->m_pBarCode, (e_QRVersion)pTBarCode11->nQRVersion);
						BCSet_QR_Mask(pTBarCode11->m_pBarCode, (e_QRMask)pTBarCode11->nQRMaskingNo);

						BCSetCodePage(pTBarCode11->m_pBarCode, e_CodePage::eCodePage_UTF8);

						//调整选择框
						int nLen = pTBarCode11->endX - pTBarCode11->startX;
						int nHeight = pTBarCode11->endY - pTBarCode11->startY;

						if (nLen > nHeight)
							pTBarCode11->endX = pTBarCode11->startX + nHeight;
						else
							pTBarCode11->endY = pTBarCode11->startY + nLen;

						pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(pTBarCode11->startX, 0), CoordZoomOut(pTBarCode11->startY, 1),
							CoordZoomOut(pTBarCode11->endX, 0), CoordZoomOut(pTBarCode11->endY, 1));

					}
					if (pTBarCode11->bBarWidthReduction)
					{
						BCSetBarWidthReduction(pTBarCode11->m_pBarCode, pTBarCode11->nBarWidthReduction * 15, eMUPercent);
					}
					else
					{
						BCSetBarWidthReduction(pTBarCode11->m_pBarCode, 0, eMUPercent);
					}
					LOGFONTA pFont = pTBarCode11->TextFont;
					long height = 0;
					// set 9 pt font (formula is used to get correct screen representation)
					if (pDC)
					{
						if (pTBarCode11->TextFont.lfHeight < 0)
						{
							pFont.lfHeight = -(int)(pTBarCode11->TextFont.lfHeight * iLOGPIXELSY / 96);
						}
						pFont.lfHeight = MulDiv(pFont.lfHeight, 72, pDC->GetDeviceCaps(LOGPIXELSY));
						pFont.lfHeight = (int)(pFont.lfHeight*ZoomInY); // 改变字体高度 
						pFont.lfWidth = (int)(pFont.lfWidth*ZoomInX);
						BCSetLogFont(pTBarCode11->m_pBarCode, &pFont);
					}


					if (!isGetData)
					{
						int nCount = 0;
						if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->sInsetParam.GetLength() > 0)
						{
							nCount = pTBarCode11->m_nFieldIndex;

							for (int i = 0; i <= nMaxColNum; i++)
							{
								CString tmp;
								tmp.Format(_T("字段%d"), i);
								if (pTBarCode11->sInsetParam.Find(tmp) >= 0)
									nCount = i;
							}
						}
						else if (pTBarCode11->m_nBarPropertyType == 2)
						{
							nCount = pTBarCode11->m_nSqlTypeFieldIndex;
						}
						//黑色的是固定数据，绿色的是一个字段，蓝色的是出现两次相通字段，红色是出现三次以上都是红色，
						COLORREF pColor = RGB(0, 0, 0);
						if (m_nCountsUse[nCount] == 1)
							pColor = RGB(0, 255, 0);
						if (m_nCountsUse[nCount] == 2)
							pColor = RGB(0, 0, 255);
						if (m_nCountsUse[nCount] >= 3)
							pColor = RGB(255, 0, 0);

						BCSetBarcodeColorGDI(pTBarCode11->m_pBarCode, pColor);
					}

					eCode = BCCheck(pTBarCode11->m_pBarCode);
					if (eCode != S_OK)
					{
						TCHAR lzBuffer[50] = { 0 };
						LPTSTR szCCError = _T("");
						LPTSTR szErrorMsg = lzBuffer;
						CString sWrongChar;
						// retrieve error information for error code
						BCGetErrorText(eCode, lzBuffer, (sizeof(lzBuffer) / sizeof(lzBuffer[0])) - 1);
						// do we have a composite component? (GS1 DataBar Composite Symbology only)
						if (BCGet2DCompositeComponent(pTBarCode11->m_pBarCode) != eCC_None)
						{
							// T: error occured in composite component (F: error was in standard linear symbol)?
							BOOL bIsErrorInCC = BCGet_CompSym_ErrorInCC(pTBarCode11->m_pBarCode);
							// set our error text accordingly
							szCCError = bIsErrorInCC ? _T(" in 2D Composite Component") : _T(" in linear component");
							// set info message: Composite Symbology needs GS1 DataBar or EAN-128 as base symbology!
							if (bIsErrorInCC && eCode == HRESULT_FROM_WIN32(ERROR_NOT_SUPPORTED))
								szErrorMsg = _T("Wrong linear symbology (only GS1 DataBar and EAN-128 allowed)");
						}
						// all symbologies: input character valid or not?
						if (eCode == HRESULT_FROM_WIN32(ERROR_INVALID_DATA))
						{
							INT nPos = BCGetInvalidDataPos(pTBarCode11->m_pBarCode); // get invalid character position
							CString sData = sCodeString;
							CString sHex;
							// display invalid char with position
							if (nPos >= 0 && nPos < sData.GetLength())
							{
								sHex.Format(" (ASCII Code 0x%X)", sData[nPos] & 0xff);
								sWrongChar.Format("'%c' at position %d%s", sData[nPos] & 0xff, nPos + 1, sHex);
							}
						}
						CString m_sErrorText;
						m_sErrorText.Format(_T("Error %s: %s%s!"), szCCError, (LPCSTR)szErrorMsg, sWrongChar); //FormatMessage
						pDC->TextOut(pTBarCode11->startX, pTBarCode11->startY, m_sErrorText);
					}
				}
				if (eCode == S_OK)
				{

					CString sInsertData = pTBarCode11->sInsetParam;
					//处理分段
					if ((pTBarCode11->nTextGroupType == 1))
					{
						sCodeString = OnProcessFieldedString(sCodeString, pTBarCode11->sFieldTypeString, true, pTBarCode11->nTextGroupDis);
					}
					else if (pTBarCode11->nTextGroupType == 0)
					{
						sCodeString = OnProcessFieldedString(sCodeString, atoi(pTBarCode11->sFieldTypeDis), true, pTBarCode11->nTextGroupDis);
					}

					BCSetDisplayTextA(pTBarCode11->m_pBarCode, sCodeString);

					eCode = BCCreate(pTBarCode11->m_pBarCode);
					if (eCode != S_OK)
					{
						TCHAR lzBuffer[50] = { 0 };
						LPTSTR szCCError = _T("");
						LPTSTR szErrorMsg = lzBuffer;
						CString sWrongChar;
						// retrieve error information for error code
						BCGetErrorText(eCode, lzBuffer, (sizeof(lzBuffer) / sizeof(lzBuffer[0])) - 1);
						// do we have a composite component? (GS1 DataBar Composite Symbology only)
						if (BCGet2DCompositeComponent(pTBarCode11->m_pBarCode) != eCC_None)
						{
							// T: error occured in composite component (F: error was in standard linear symbol)?
							BOOL bIsErrorInCC = BCGet_CompSym_ErrorInCC(pTBarCode11->m_pBarCode);
							// set our error text accordingly
							szCCError = bIsErrorInCC ? _T(" in 2D Composite Component") : _T(" in linear component");
							// set info message: Composite Symbology needs GS1 DataBar or EAN-128 as base symbology!
							if (bIsErrorInCC && eCode == HRESULT_FROM_WIN32(ERROR_NOT_SUPPORTED))
								szErrorMsg = _T("Wrong linear symbology (only GS1 DataBar and EAN-128 allowed)");
						}
						// all symbologies: input character valid or not?
						if (eCode == HRESULT_FROM_WIN32(ERROR_INVALID_DATA))
						{
							INT nPos = BCGetInvalidDataPos(pTBarCode11->m_pBarCode); // get invalid character position
							CString sData = pTBarCode11->GetData();
							CString sHex;
							// display invalid char with position
							if (nPos >= 0 && nPos < sData.GetLength())
							{
								sHex.Format(" (ASCII Code 0x%X)", sData[nPos] & 0xff);
								sWrongChar.Format("'%c' at position %d%s", sData[nPos] & 0xff, nPos + 1, sHex);
							}
						}
						// format error message incl. all information, which has been constructed above
						CString m_sErrorText;
						m_sErrorText.Format(_T("Error %s: %s%s!"), szCCError, (LPCSTR)szErrorMsg, sWrongChar); //FormatMessage
						pDC->TextOut(pTBarCode11->startX, pTBarCode11->startY, m_sErrorText);

						//创建m_pBarCode
						if (pTBarCode11->m_pBarCode)
						{
							BCFree(pTBarCode11->m_pBarCode);
							pTBarCode11->m_pBarCode = NULL;
						}

						ERRCODE eCode = S_OK;
						eCode = BCAlloc(&(pTBarCode11->m_pBarCode));
						if (eCode != S_OK)
						{
							return;
						}

						CClientDC dc(AfxGetApp()->GetMainWnd());
						// Dummy text
						BCSetMustFit(pTBarCode11->m_pBarCode, FALSE);              // no check for too low resolution
						if (pTBarCode11->GetCodeType() == 1)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_EAN8);  //
						}
						else if (pTBarCode11->GetCodeType() == 2)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_EAN13);
						}
						else if (pTBarCode11->GetCodeType() == 3)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_EAN128);
						}
						else if (pTBarCode11->GetCodeType() == 4)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_3OF9);
						}
						else if (pTBarCode11->GetCodeType() == 5)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_Code128);
						}
						else if (pTBarCode11->GetCodeType() == 6)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_CodaBar2);
						}
						else if (pTBarCode11->GetCodeType() == 7)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_PDF417);
						}
						else if (pTBarCode11->GetCodeType() == 8)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_QRCode);
						}
						else if (pTBarCode11->GetCodeType() == 9)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_MAXICODE);
						}
						else if (pTBarCode11->GetCodeType() == 10)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_DataMatrix);
						}
						else if (pTBarCode11->GetCodeType() == 11)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_9OF3);
						}
						else if (pTBarCode11->GetCodeType() == 12)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_UPC12);
						}
						else if (pTBarCode11->GetCodeType() == 13)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_Code128A);
						}
						else if (pTBarCode11->GetCodeType() == 14)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_Code128B);
						}
						else if (pTBarCode11->GetCodeType() == 15)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_Code128C);
						}
						else if (pTBarCode11->GetCodeType() == 16)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_UPCE);
						}
						else if (pTBarCode11->GetCodeType() == 17)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_ITF14);
						}
						else if (pTBarCode11->GetCodeType() == 18)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_2OF5IL);
						}
						BCSetRotation(pTBarCode11->GetBarCode(), (e_Degree)pTBarCode11->m_nRotateTyp);
						BCSetCDMethod(pTBarCode11->m_pBarCode, eCDStandard);       // check digit method

						LOGFONTA* pFont;
						pFont = BCGetLogFont(pTBarCode11->m_pBarCode);             // get pointer to font struct

						// set 8 pt font (formula is used to get correct screen representation)
						if (dc)
							pFont->lfHeight = -MulDiv(8, dc.GetDeviceCaps(LOGPIXELSY), 72);

						_tcscpy(pFont->lfFaceName, _T("Arial"));
						BCSetLogFont(pTBarCode11->m_pBarCode, pFont);
					}
					else
					{
						int nLen = pTBarCode11->endX - pTBarCode11->startX;
						int nHeight = pTBarCode11->endY - pTBarCode11->startY;

						RECT rect;
						rect.left = CoordZoomOut(pTBarCode11->startX, 0);
						rect.top = CoordZoomOut(pTBarCode11->startY, 1);
						rect.right = rect.left + nLen*ZoomInX;
						rect.bottom = rect.top + nHeight *ZoomInY;

						BCDraw(pTBarCode11->m_pBarCode, pDC->GetSafeHdc(), &rect);
					}

					if (pTBarCode11->bIsSelected &&!isGetData)
					{
						pTBarCode11->tracker.Draw(pDC);
					}
				}

			}
#pragma endregion
		}
	}


	pos = m_selectLsit.GetHeadPosition();
	while (pos != NULL)
	{
		CObject* pHitItem = (CObject*)m_selectLsit.GetNext(pos);

		CGraph* pGraph = (CGraph*)pHitItem;
		if (pGraph != NULL)
		{
			if (!pGraph->bIsSelected &&!isGetData)
			{

				pGraph->tracker.m_rect.SetRect(CoordZoomOut(pGraph->startX, 0), CoordZoomOut(pGraph->startY, 1),
					CoordZoomOut(pGraph->endX, 0), CoordZoomOut(pGraph->endY, 1));
				pGraph->InitPropList();
				pGraph->tracker.Draw(pDC);


				//if (pObject->IsKindOf(RUNTIME_CLASS(CText)))
				//{
				//	pText = (CText*)pObject;
				//	if (pText != NULL)
				//	{
				//		pText->tracker.m_rect.SetRect(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), 
				//			CoordZoomOut(pText->endX, 0), CoordZoomOut(pText->endY, 1));

				//		pText->tracker.Draw(pDC);
				//	}
				//}
				//else if (pGraph->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
				//{
				//	pTBarCode11 = (CTBarCode11*)pObject;
				//	if (pTBarCode11 != NULL)
				//	{
				//		pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(pTBarCode11->startX, 0), CoordZoomOut(pTBarCode11->startY, 1),
				//			CoordZoomOut(pTBarCode11->endX, 0), CoordZoomOut(pTBarCode11->endY, 1));
				//		pTBarCode11->tracker.Draw(pDC);
				//	}
				//}
			}
		}
	}


}



/////////////////////////////////////////////////////////////////////////////
// CMyDrawView diagnostics

#ifdef _DEBUG
void CMyDrawView::AssertValid() const
{
	//CScrollView::AssertValid();
}

void CMyDrawView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CMyDrawDoc* CMyDrawView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMyDrawDoc)));
	return (CMyDrawDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMyDrawView message handlers

void CMyDrawView::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_bLButtonDown)//防止上一次框选未结束
		return;

	CClientDC dc(this);
	OnPrepareDC(&dc);
	dc.DPtoLP(&point);

	CreateItem(point);
	m_bLButtonDown = TRUE;

	if (m_enumCurTool == ToolSelect)
	{
		m_startPoint = point;
		m_endPoint = point;
		m_movePoint = point;
		if (!(GetKeyState(VK_CONTROL) < 0) && !(GetKeyState(VK_SHIFT)<0))
			ClearSelection();
		CRect rectDraw(point, point);
		m_nSelectedCount = SelectElement(rectDraw, point);
	}
	Invalidate(FALSE);
	CScrollView::OnLButtonDown(nFlags, point);
}

void CMyDrawView::CreateItem(CPoint point)
{
	if (m_createList.GetCount() > 0)
	{
		CEllipse* pEllipse = NULL;
		CLine* pLine = NULL;
		CRectangle* pRectangle = NULL;
		CArc* pArc = NULL;
		CText* pText = NULL;
		CBarCode* pBarCode = NULL;
		CTBarCode11* pTBarCode11 = NULL;
		CPic* pPic = NULL;
		POSITION pos = m_createList.GetHeadPosition();
		CObject* pHitItem;
		if (pos != NULL)
		{
			pHitItem = (CObject*)m_createList.GetNext(pos);
			if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
			{
				pText = (CText*)pHitItem;
				if (pText != NULL)
				{
					pText->endX = CoordZoomOrgi(point.x + pText->endX - pText->startX, 0);
					pText->endY = CoordZoomOrgi(point.y + pText->endY - pText->startY, 1);

					pText->startX = CoordZoomOrgi(point.x, 0);
					pText->startY = CoordZoomOrgi(point.y, 1);
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
			{
				pEllipse = (CEllipse*)pHitItem;
				if (pEllipse != NULL)
				{
					pEllipse->endX = CoordZoomOrgi(point.x + pEllipse->endX - pEllipse->startX, 0);
					pEllipse->endY = CoordZoomOrgi(point.y + pEllipse->endY - pEllipse->startY, 1);
					pEllipse->startX = CoordZoomOrgi(point.x, 0);
					pEllipse->startY = CoordZoomOrgi(point.y, 1);
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
			{
				pLine = (CLine*)pHitItem;
				if (pLine != NULL)
				{
					pLine->endX = CoordZoomOrgi(point.x + pLine->endX - pLine->startX, 0);
					pLine->endY = CoordZoomOrgi(point.y + pLine->endY - pLine->startY, 1);
					pLine->startX = CoordZoomOrgi(point.x, 0);
					pLine->startY = CoordZoomOrgi(point.y, 1);
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
			{
				pRectangle = (CRectangle*)pHitItem;
				if (pRectangle != NULL)
				{
					pRectangle->endX = CoordZoomOrgi(point.x + pRectangle->endX - pRectangle->startX, 0);
					pRectangle->endY = CoordZoomOrgi(point.y + pRectangle->endY - pRectangle->startY, 1);
					pRectangle->startX = CoordZoomOrgi(point.x, 0);
					pRectangle->startY = CoordZoomOrgi(point.y, 1);
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
			{
				pArc = (CArc*)pHitItem;
				if (pArc != NULL)
				{
					pArc->endX = CoordZoomOrgi(point.x + pArc->endX - pArc->startX, 0);
					pArc->endY = CoordZoomOrgi(point.y + pArc->endY - pArc->startY, 1);
					pArc->startX = CoordZoomOrgi(point.x, 0);
					pArc->startY = CoordZoomOrgi(point.y, 1);
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
			{
				pBarCode = (CBarCode*)pHitItem;
				if (pBarCode != NULL)
				{
					pBarCode->startX = CoordZoomOrgi(point.x, 0);
					pBarCode->startY = CoordZoomOrgi(point.y, 1);
				}
			}

			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
			{
				pTBarCode11 = (CTBarCode11*)pHitItem;
				if (pTBarCode11 != NULL)
				{
					pTBarCode11->startX = CoordZoomOrgi(point.x, 0);
					pTBarCode11->startY = CoordZoomOrgi(point.y, 1);

					pTBarCode11->endX = CoordZoomOrgi(point.x, 0) + pTBarCode11->nBitMapLenth;
					pTBarCode11->endY = CoordZoomOrgi(point.y, 1) + pTBarCode11->nBitMapHeight;
				}
			}

			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
			{
				pPic = (CPic*)pHitItem;
				if (pPic != NULL)
				{
					pPic->startX = CoordZoomOrgi(point.x, 0);
					pPic->startY = CoordZoomOrgi(point.y, 1);
				}
			}
		}
		invoker->Execute();
		m_createList.RemoveAll();
	}

	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_ARROW));//光标离开该区域恢复默认箭头形状
}
BOOL CMyDrawView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;

	CPic* pPic = NULL;

	POSITION pos = m_ObjectList.GetHeadPosition();
	CObject* pHitItem;
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL)
			{
				if (pEllipse->tracker.SetCursor(pWnd, nHitTest))
				{
					return true;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL)
			{
				if (pLine->tracker.SetCursor(pWnd, nHitTest))
				{
					return true;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL)
			{
				if (pRectangle->tracker.SetCursor(pWnd, nHitTest))
				{
					return true;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL)
			{
				if (pArc->tracker.SetCursor(pWnd, nHitTest))
				{
					return true;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText != NULL)
			{
				if (pText->tracker.SetCursor(pWnd, nHitTest))
				{
					return true;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL)
			{
				if (pBarCode->tracker.SetCursor(pWnd, nHitTest))
				{
					return true;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL)
			{
				if (pTBarCode11->tracker.SetCursor(pWnd, nHitTest))
				{
					return true;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic != NULL)
			{
				if (pPic->tracker.SetCursor(pWnd, nHitTest))
				{
					return true;
				}
			}
		}
	}
	// TODO: Add your message handler code here and/or call default
	//鼠标移动到调节框范围内时，改变鼠标指针形状，指示可以拖动或改变绘图元素
	//if(m_tracker.SetCursor(pWnd,nHitTest)) 
	//return true;
	return CScrollView::OnSetCursor(pWnd, nHitTest, message);
}

void CMyDrawView::ClearSelection(bool refresh/* = false*/)
{
	if (!isGetData)
	{
		ZoomInX = (float)1.0*Zoom;
		ZoomInY = (float)1.0*Zoom;
	}

	if (!refresh && m_nSelectedCount <= 0)
		return;

	CObject* pHitItem = NULL;
	POSITION pos = m_ObjectList.GetHeadPosition();
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;

	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL)
			{
				pEllipse->bIsSelected = false;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL)
			{
				pLine->bIsSelected = false;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL)
			{
				pRectangle->bIsSelected = false;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL)
			{
				pArc->bIsSelected = false;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;

			pText->tracker.m_rect.SetRect(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), CoordZoomOut(pText->endX, 0), CoordZoomOut(pText->endY, 1));

			if (pText != NULL)
			{
				pText->bIsSelected = false;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic != NULL)
			{
				pPic->bIsSelected = false;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL)
			{
				pBarCode->bIsSelected = false;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL)
			{
				pTBarCode11->bIsSelected = false;
			}
		}
	}


	UpdateRulersInfo(RW_HSCROLL, GetScrollPosition());
	UpdateRulersInfo(RW_VSCROLL, GetScrollPosition());

	return;
}




void CMyDrawView::DrawSelection()
{
	if (m_nSelectedCount <= 0)
		return;

	CObject* pHitItem = NULL;
	POSITION pos = m_ObjectList.GetHeadPosition();
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;

	CRect SelectionRect;
	SelectionRect.SetRectEmpty();
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL && pEllipse->bIsSelected)
			{
				CRect rect = pEllipse->tracker.m_rect;
				if (SelectionRect.IsRectEmpty())
				{
					SelectionRect = rect;
				}
				else
				{
					SelectionRect.UnionRect(SelectionRect, rect);
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL&& pLine->bIsSelected)
			{
				CRect rect = pLine->tracker.m_rect;
				if (SelectionRect.IsRectEmpty())
				{
					SelectionRect = rect;
				}
				else
				{
					SelectionRect.UnionRect(SelectionRect, rect);
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL&& pRectangle->bIsSelected)
			{
				CRect rect = pRectangle->tracker.m_rect;
				if (SelectionRect.IsRectEmpty())
				{
					SelectionRect = rect;
				}
				else
				{
					SelectionRect.UnionRect(SelectionRect, rect);
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL&& pArc->bIsSelected)
			{
				CRect rect = pArc->tracker.m_rect;
				if (SelectionRect.IsRectEmpty())
				{
					SelectionRect = rect;
				}
				else
				{
					SelectionRect.UnionRect(SelectionRect, rect);
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText != NULL&& pText->bIsSelected)
			{
				CRect rect = pText->tracker.m_rect;
				if (SelectionRect.IsRectEmpty())
				{
					SelectionRect = rect;
				}
				else
				{
					SelectionRect.UnionRect(SelectionRect, rect);
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic != NULL&& pPic->bIsSelected)
			{
				CRect rect = pPic->tracker.m_rect;
				if (SelectionRect.IsRectEmpty())
				{
					SelectionRect = rect;
				}
				else
				{
					SelectionRect.UnionRect(SelectionRect, rect);
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL&& pBarCode->bIsSelected)
			{
				CRect rect = pBarCode->tracker.m_rect;
				if (SelectionRect.IsRectEmpty())
				{
					SelectionRect = rect;
				}
				else
				{
					SelectionRect.UnionRect(SelectionRect, rect);
				}

			}
		}

		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL&& pTBarCode11->bIsSelected)
			{
				CRect rect = pTBarCode11->tracker.m_rect;
				if (SelectionRect.IsRectEmpty())
				{
					SelectionRect = rect;
				}
				else
				{
					SelectionRect.UnionRect(SelectionRect, rect);
				}

			}
		}
	}
	if (!SelectionRect.IsRectEmpty())
	{
		UpdateRulersInfo(RW_SELECTION, GetScrollPosition(), SelectionRect);
	}

	return;
}



void CMyDrawView::DrawLine(CDC *pDC, CLine *pLine)
{

	LOGBRUSH lb;
	lb.lbStyle = BS_SOLID;
	lb.lbColor = pLine->LinePen.lopnColor;
	lb.lbHatch = 0;

	long lfWidth = pLine->LinePen.lopnWidth.x;


	HDC hdc = pDC->m_hDC;

	HGDIOBJ hOld;

	HPEN cMyPen = ExtCreatePen(PS_SOLID | PS_GEOMETRIC |
		PS_ENDCAP_FLAT | PS_JOIN_BEVEL, lfWidth*ZoomInX, &lb, 0, NULL);

	hOld = SelectObject(hdc, cMyPen);


	BeginPath(hdc);
	MoveToEx(hdc, CoordZoomOut(pLine->startX, 0), CoordZoomOut(pLine->startY, 1), NULL);
	LineTo(hdc, CoordZoomOut(pLine->endX, 0), CoordZoomOut(pLine->endY, 1));

	EndPath(hdc);
	StrokePath(hdc);
	DeleteObject(SelectObject(hdc, GetStockObject(BLACK_PEN)));
	SelectObject(hdc, hOld);

	::DeleteObject(cMyPen);




	//CPen cMyPen;
	//CPen* pOldPen;
	//HDC hdc = pDC->m_hDC;

	//cMyPen.CreatePenIndirect(&pLine->LinePen);
	//pOldPen = pDC->SelectObject(&cMyPen);

	//BeginPath(hdc);
	//pDC->MoveTo(CoordZoomOut(pLine->startX, 0), CoordZoomOut(pLine->startY, 1));
	//pDC->LineTo(CoordZoomOut(pLine->endX, 0), CoordZoomOut(pLine->endY, 1));

	//EndPath(hdc);
	//StrokePath(hdc);
	//DeleteObject(SelectObject(hdc, GetStockObject(BLACK_PEN)));
	//pDC->SelectObject(pOldPen);

	//cMyPen.DeleteObject();

}

void CMyDrawView::DrawArc(CDC *pDC, CArc *pArc)
{

	LOGBRUSH lb;
	lb.lbStyle = BS_SOLID;
	lb.lbColor = pArc->LinePen.lopnColor;
	lb.lbHatch = 0;

	long lfWidth = pArc->LinePen.lopnWidth.x;


	HDC hdc = pDC->m_hDC;

	HGDIOBJ hOld;

	HPEN cMyPen = ExtCreatePen(PS_SOLID | PS_GEOMETRIC |
		PS_ENDCAP_FLAT | PS_JOIN_BEVEL, lfWidth*ZoomInX, &lb, 0, NULL);

	hOld = SelectObject(hdc, cMyPen);

	BeginPath(hdc);
	if (pArc->Direction)
	{

		pDC->Arc(CoordZoomOut(pArc->startX, 0), CoordZoomOut(pArc->startY, 1), CoordZoomOut(pArc->endX, 0), CoordZoomOut(pArc->endY, 1),
			CoordZoomOut(pArc->startX, 1), CoordZoomOut(pArc->startY, 1), CoordZoomOut(pArc->endX, 0), CoordZoomOut(pArc->endY, 1));
	}
	else
	{
		pDC->Arc(CoordZoomOut(pArc->endX, 0), CoordZoomOut(pArc->endY, 1), CoordZoomOut(pArc->startX, 0), CoordZoomOut(pArc->startY, 1),
			CoordZoomOut(pArc->endX, 0), CoordZoomOut(pArc->endY, 1),
			CoordZoomOut(pArc->startX, 0), CoordZoomOut(pArc->startY, 1));

	}

	EndPath(hdc);
	StrokePath(hdc);
	DeleteObject(SelectObject(hdc, GetStockObject(BLACK_PEN)));
	SelectObject(hdc, hOld);

	::DeleteObject(cMyPen);

}




void CMyDrawView::DrawMyText(CDC *pDC, CText *pText, bool bIsFirst)
{

	CFont cMyFont;


	//默认是600*600
	float ScaleX_Print = 300 / 12.7f;
	float ScaleY_Print = 300 / 12.7f;

	//如果是300dpi
	if (DPIX > 0)
	{
		ScaleX_Print = 150 / 12.7f;
		ScaleY_Print = 150 / 12.7f;
	}

	float fZoomInX = ScaleX_Print / (iLOGPIXELSX / 25.4f);
	float fZoomInY = ScaleY_Print / (iLOGPIXELSY / 25.4f);



	LOGFONT MyFont = pText->MyFont;

	//if (!isGetData)
	//{
	//	MyFont.lfHeight = pText->MyFont.lfHeight; // 改变字体高度 
	//	MyFont.lfWidth = pText->MyFont.lfWidth;
	//}
	//else
	{
		MyFont.lfHeight = pText->MyFont.lfHeight*fZoomInY; // 改变字体高度 
		MyFont.lfWidth = pText->MyFont.lfWidth*fZoomInX;
	}



	MyFont.lfEscapement = 3600 - (int)pText->nTxtRotateAngle * 10;
	cMyFont.CreateFontIndirect(&MyFont);


	if (pText->nConstValueType == 1 && isGetData)
	{
		pText->MyText = GetCounterText(pText->m_lTxtCounterStartNum, pText->m_lTxtCounterEndNum, pText->m_lTxtCounterStep, n_PageIndex);
		//if (pText->MyText.GetLength() > 0)
		//{
		//	pText->m_lTxtCounterStartNum = _ttol(_T(pText->MyText));
		//}
	}
	else if (pText->nConstValueType == 3)
	{
		pText->MyText = GetNowTime(pText->m_nTimeFormatIndex);
	}

	CString sCodeString = "";
	sCodeString = pText->MyText;
	if (!m_bIsEnablePrview)
	{
		sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, !isGetData);
	}

	//去掉左侧的0并居中
	if (pText->isDelete0)
	{
		int nLength = sCodeString.GetLength();
		sCodeString.TrimLeft('0');
		int nLength2 = sCodeString.GetLength();

		CString sSpace = "";
		for (int m = 0; m < nLength - nLength2; m++)
		{
			sSpace += " ";
		}

		sCodeString = sSpace + sCodeString;
	}

	CStringArray strArray;
	//处理分段
	if (pText->isTxtUseStyleCtrl)
	{
		//if ((pText->sInsetSqlParam.GetLength() == 0) || (isGetData))
		{
			sCodeString = OnProcessFieldedString(sCodeString, pText->m_sTxtFieldType, true, 1);
			SplitString(sCodeString, ' ', strArray);
		}
	}

	
	{
		BOOL bRes = FALSE;
		//创建内存DC  
		CDC memDC;
		bRes = memDC.CreateCompatibleDC(NULL);
		ASSERT(bRes);
		CFont* pOldFont = memDC.SelectObject(&cMyFont);

		CSize size;
		{
			size = memDC.GetTextExtent(sCodeString);

			pText->endX = pText->startX + size.cx / fZoomInX + pText->m_iTxtFieldDis;
			pText->endY = pText->startY + size.cy / fZoomInY;

		}

		float cosine = (float)cos(pText->nTxtRotateAngle * PI / 180.);
		float sine = (float)sin(pText->nTxtRotateAngle * PI / 180.);

		// Compute dimensions of the resulting bitmap  
		// First get the coordinates of the 3 corners other than origin  
		int x1 = (int)(size.cy * sine);
		int y1 = (int)(size.cy * cosine);
		int x2 = (int)(size.cx * cosine + size.cy * sine);
		int y2 = (int)(size.cy * cosine - size.cx * sine);
		int x3 = (int)(size.cx * cosine);
		int y3 = (int)(-size.cx * sine);

		int minx = min(0, min(x1, min(x2, x3)));
		int miny = min(0, min(y1, min(y2, y3)));
		int maxx = max(0, max(x1, max(x2, x3)));
		int maxy = max(0, max(y1, max(y2, y3)));

		int w = maxx - minx;
		int h = maxy - miny;


		CBitmap m_pSymbleBitmap ;
		bRes = m_pSymbleBitmap.CreateCompatibleBitmap(&CClientDC(NULL), w, h);
		ASSERT(bRes);
		CBitmap* m_pOldBitmap = memDC.SelectObject(&m_pSymbleBitmap);
		CRect rect(0, 0, w, h);
		////填充白色背景色	
		memDC.FillSolidRect(rect, RGB(255, 255, 255));
		int oldMode = memDC.SetBkMode(pText->BkMode);
		COLORREF oldTextColor = memDC.SetTextColor(pText->MyColor);

		if (pText->nTxtRotateAngle == 0)
		{
			if (pText->isTxtUseStyleCtrl)
			{
				int startX = 0;
				for (int m = 0; m < strArray.GetCount(); m++)
				{
					memDC.TextOut(0 + startX, 0, strArray[m]);
					size = memDC.GetTextExtent(strArray[m]);
					
					{
						startX += size.cx + pText->m_iTxtFieldDis;
					}
				}
			}
			else
				memDC.TextOut(0, 0, sCodeString);
		}
		else if (pText->nTxtRotateAngle == 90)
		{
			if (pText->isTxtUseStyleCtrl)
			{
				int startY = 0;
				for (int m = 0; m < strArray.GetCount(); m++)
				{
					memDC.TextOut(w, 0 + startY, strArray[m]);
					size = memDC.GetTextExtent(strArray[m]);

					{
						startY += size.cx + pText->m_iTxtFieldDis;
					}
				}
			}
			else
				memDC.TextOut(w, 0, sCodeString);
		}
		else if (pText->nTxtRotateAngle == 180)
		{
			if (pText->isTxtUseStyleCtrl)
			{
				int startX = 0;
				for (int m = 0; m < strArray.GetCount(); m++)
				{
					memDC.TextOut(w - startX, h, strArray[m]);
					size = memDC.GetTextExtent(strArray[m]);
					
					{
						startX += size.cx + pText->m_iTxtFieldDis;
					}
				}
			}
			else
				memDC.TextOut(w, h, sCodeString);
		}
		else if (pText->nTxtRotateAngle == 270)
		{
			if (pText->isTxtUseStyleCtrl)
			{
				int startY = 0;
				for (int m = 0; m < strArray.GetCount(); m++)
				{
					memDC.TextOut(0, h - startY, strArray[m]);
					size = memDC.GetTextExtent(strArray[m]);
					
					{
						startY += size.cx + pText->m_iTxtFieldDis;
					}
				}
			}
			else
				memDC.TextOut(0, h, sCodeString);
		}
		else //文本旋转其他角度
		{
			SetTextAlign(memDC.GetSafeHdc(), TA_CENTER | TA_BASELINE);
			memDC.TextOut(w / 2, h / 2, sCodeString);
			SetTextAlign(memDC.GetSafeHdc(), TA_LEFT | TA_TOP);
		}
		

		//如果是镜像
		if (pText->isMirrorImage)
		{
			bool Mirror_X = true;
			bool Mirror_Y = false;

			memDC.SetMapMode(MM_ISOTROPIC);
			CSize saveWindowExt = memDC.SetWindowExt(1, 1);
			int vp_cx = 1, vp_cy = 1;
			if (Mirror_X) vp_cx = -1;
			if (Mirror_Y) vp_cy = -1;
			CSize saveViewport = memDC.SetViewportExt(vp_cx, vp_cy);
			int vporg_x = 0, vporg_y = 0;
			if (Mirror_X)vporg_x = rect.Width() - 1;
			if (Mirror_Y)vporg_y = rect.Height() - 1;
			CPoint saveViewportOrg = memDC.SetViewportOrg(vporg_x, vporg_y);
		}

		if (pText->isUseDeburring)//修边处理
		{

			BITMAP bmp;
			ZeroMemory(&bmp, sizeof(bmp));

			m_pSymbleBitmap.GetBitmap(&bmp);  //得到加载位图信息
			unsigned char *px = new unsigned char[bmp.bmHeight*bmp.bmWidthBytes];
			m_pSymbleBitmap.GetBitmapBits(bmp.bmHeight*bmp.bmWidthBytes, px);//读取位图数据
			int PixelBytes = bmp.bmBitsPixel / 8;//每一个像素占字节数


			COLORREF white = RGB(255, 255, 255);
			COLORREF lastColor;
			if (pText->nDeburring_x >0)
			for (int j = 0; j < h; j++)
			{
				lastColor = white;
				for (int i = 0; i < w; i++)
				{
					//COLORREF clr = memDC.GetPixel(i, j);
					long rgb_b = j*bmp.bmWidthBytes + i*PixelBytes;
					int R = px[rgb_b + 0];
					int G = px[rgb_b + 1];
					int B = px[rgb_b + 2];//以上三个值就分别是BGR三个色彩的值。
					COLORREF clr = RGB(R, G, B);
					if (clr != lastColor && clr != white)
					{
						lastColor = clr;
						i++;
						for (int x = 0; x < pText->nDeburring_x; x++)
						{
							//lastColor = memDC.GetPixel(i, j);
							rgb_b = j*bmp.bmWidthBytes + i*PixelBytes;
							R = px[rgb_b + 0];
							G = px[rgb_b + 1];
							B = px[rgb_b + 2];//以上三个值就分别是BGR三个色彩的值。
							lastColor = RGB(R, G, B);
							if (lastColor != white)
							{
								//memDC.SetPixel(i - 1, j, white);
								rgb_b = j*bmp.bmWidthBytes + (i - 1)*PixelBytes;
								px[rgb_b + 0] = 255;
								px[rgb_b + 1] = 255;
								px[rgb_b + 2] = 255;//以上三个值就分别是BGR三个色彩的值。
							}
							else
								break;

							i++;
						}
					}
					else
						lastColor = clr;
				}
			}

			if (pText->nDeburring_y >0)
			for (int i = 0; i < w; i++)
			{
				lastColor = white;
				for (int j = 0; j < h; j++)
				{
					//COLORREF clr = memDC.GetPixel(i, j);
					long rgb_b = j*bmp.bmWidthBytes + i*PixelBytes;
					int R = px[rgb_b + 0];
					int G = px[rgb_b + 1];
					int B = px[rgb_b + 2];//以上三个值就分别是BGR三个色彩的值。
					COLORREF clr = RGB(R, G, B);
					if (clr != lastColor && clr != white)
					{
						lastColor = clr;
						j++;
						for (int y = 0; y < pText->nDeburring_y; y++)
						{
							//lastColor = memDC.GetPixel(i, j);
							rgb_b = j*bmp.bmWidthBytes + i*PixelBytes;
							R = px[rgb_b + 0];
							G = px[rgb_b + 1];
							B = px[rgb_b + 2];//以上三个值就分别是BGR三个色彩的值。
							lastColor = RGB(R, G, B);
							if (lastColor != white)
							{
								//memDC.SetPixel(i, j - 1, white);
								rgb_b = (j - 1)*bmp.bmWidthBytes + i*PixelBytes;
								px[rgb_b + 0] = 255;
								px[rgb_b + 1] = 255;
								px[rgb_b + 2] = 255;//以上三个值就分别是BGR三个色彩的值。
							}
							else
								break;

							j++;
						}
					}
					else
						lastColor = clr;
				}
			}


			m_pSymbleBitmap.SetBitmapBits(bmp.bmHeight*bmp.bmWidthBytes, px);//读取位图数据
			

			delete(px);
			px = NULL;
		}


		pDC->SetStretchBltMode(COLORONCOLOR);

		//如果是任意尺寸
		if (!pText->isUseAnyStyle)
		{
			if (!isGetData)
			{
				pDC->StretchBlt(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), w / fZoomInX * Zoom, h / fZoomInY * Zoom, &memDC, 0, 0, w, h, SRCAND);
				pText->endX = pText->startX + w / fZoomInX;
				pText->endY = pText->startY + h / fZoomInY;
			}
			else
			{
				pDC->StretchBlt(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), w, h, &memDC, 0, 0, w, h, SRCAND);
			}
			
		}
		else
		{
			//拷贝DC到窗口  	
			if (!isGetData)
			{
				pDC->StretchBlt(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), pText->tracker.m_rect.Width(), pText->tracker.m_rect.Height(), &memDC, 0, 0, w, h, SRCAND);
				pText->endX = pText->startX + pText->tracker.m_rect.Width()/Zoom;
				pText->endY = pText->startY + pText->tracker.m_rect.Height()/Zoom;
			}
			else
			{
				pDC->StretchBlt(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), w, h, &memDC, 0, 0, w, h, SRCAND);
			}
		}
		//清理内存  
		memDC.SetTextColor(oldTextColor);//恢复字体颜色
		memDC.SetBkMode(oldMode);
		memDC.SelectObject(m_pOldBitmap);
		memDC.SelectObject(pOldFont);
		bRes = cMyFont.DeleteObject();
		ASSERT(bRes);
		bRes = m_pSymbleBitmap.DeleteObject();
		ASSERT(bRes);
		bRes = memDC.DeleteDC();
		ASSERT(bRes);//通过 
		m_pOldBitmap = NULL;

		if (memDC.m_hDC)
			memDC.DeleteDC();

		if (m_pSymbleBitmap.m_hObject)
			m_pSymbleBitmap.DeleteObject();
		
	}

	
}

void CMyDrawView::DrawEllipse(CDC *pDC, CEllipse *pEllipse)
{

	//LOGBRUSH lb;
	//lb.lbStyle = BS_SOLID;
	//lb.lbColor = pEllipse->LinePen.lopnColor;
	//lb.lbHatch = 0;

	//long lfWidth = pEllipse->LinePen.lopnWidth.x;


	//HDC hdc = pDC->m_hDC;

	//HGDIOBJ hOld;

	//HPEN cMyPen = ExtCreatePen(PS_SOLID | PS_GEOMETRIC |
	//	PS_ENDCAP_FLAT | PS_JOIN_BEVEL, lfWidth*ZoomInX, &lb, 0, NULL);

	//hOld = SelectObject(hdc, cMyPen);

	//BeginPath(hdc);

	//pDC->Ellipse(CoordZoomOut(pEllipse->startX, 0), CoordZoomOut(pEllipse->startY, 1), CoordZoomOut(pEllipse->endX, 0), CoordZoomOut(pEllipse->endY, 1));

	//EndPath(hdc);
	//StrokeAndFillPath(hdc);
	//DeleteObject(SelectObject(hdc, GetStockObject(BLACK_PEN)));
	//SelectObject(hdc, hOld);

	//::DeleteObject(cMyPen);


	CPen cMyPen;
	CPen* pOldPen;
	HDC hdc = pDC->GetSafeHdc();

	cMyPen.CreatePenIndirect(&pEllipse->LinePen);
	pOldPen = pDC->SelectObject(&cMyPen);

	CBrush cMyBrush;
	CBrush* pOldBrush;

	cMyBrush.CreateBrushIndirect(&pEllipse->MyBrush);
	pOldBrush = pDC->SelectObject(&cMyBrush);
	BeginPath(hdc);

	pDC->Ellipse(CoordZoomOut(pEllipse->startX, 0), CoordZoomOut(pEllipse->startY, 1), CoordZoomOut(pEllipse->endX, 0), CoordZoomOut(pEllipse->endY, 1));

	EndPath(hdc);
	StrokeAndFillPath(hdc);
	DeleteObject(SelectObject(hdc, GetStockObject(BLACK_PEN)));

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
	
	cMyBrush.DeleteObject();
	cMyPen.DeleteObject();
	
}

void CMyDrawView::DrawRectangle(CDC *pDC, CRectangle *pRectangle)
{

	//LOGBRUSH lb;
	//lb.lbStyle = BS_SOLID;
	//lb.lbColor = pRectangle->LinePen.lopnColor;
	//lb.lbHatch = 0;

	//long lfWidth = pRectangle->LinePen.lopnWidth.x;


	//HDC hdc = pDC->m_hDC;

	//HGDIOBJ hOld;

	//HPEN cMyPen = ExtCreatePen(PS_SOLID | PS_GEOMETRIC |
	//	PS_ENDCAP_FLAT | PS_JOIN_BEVEL, lfWidth*ZoomInX, &lb, 0, NULL);

	//hOld = SelectObject(hdc, cMyPen);


	//BeginPath(hdc);

	//pDC->Rectangle(CoordZoomOut(pRectangle->startX, 0), CoordZoomOut(pRectangle->startY, 1),
	//	CoordZoomOut(pRectangle->endX, 0), CoordZoomOut(pRectangle->endY, 1));

	//EndPath(hdc);
	//StrokeAndFillPath(hdc);
	//DeleteObject(SelectObject(hdc, GetStockObject(BLACK_PEN)));
	//SelectObject(hdc, hOld);


	//::DeleteObject(cMyPen);




	CPen cMyPen;
	CPen* pOldPen;

	HDC hdc = pDC->GetSafeHdc();

	cMyPen.CreatePenIndirect(&pRectangle->LinePen);
	pOldPen = pDC->SelectObject(&cMyPen);

	CBrush cMyBrush;
	CBrush* pOldBrush;

	cMyBrush.CreateBrushIndirect(&pRectangle->MyBrush);
	pOldBrush = pDC->SelectObject(&cMyBrush);

	BeginPath(hdc);
	if (pRectangle->bRound)
	{
		CRect rtRect = CRect(CoordZoomOut(pRectangle->startX, 0), CoordZoomOut(pRectangle->startY, 1),
			CoordZoomOut(pRectangle->endX, 0), CoordZoomOut(pRectangle->endY, 1));

		pDC->RoundRect(rtRect, CPoint(10, 10));
	}
	else
	{
		pDC->Rectangle(CoordZoomOut(pRectangle->startX, 0), CoordZoomOut(pRectangle->startY, 1),
			CoordZoomOut(pRectangle->endX, 0), CoordZoomOut(pRectangle->endY, 1));
	}
	EndPath(hdc);
	StrokeAndFillPath(hdc);
	DeleteObject(SelectObject(hdc, GetStockObject(BLACK_PEN)));

	
	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);

	
	cMyBrush.DeleteObject();
	cMyPen.DeleteObject();

}

void CMyDrawView::DrawPic(CDC *pDC, CPic *pPic)
{

	CString sFilePathName = "";
	if (pPic->isStaticPic)//静态图片
	{
		sFilePathName = pPic->m_PicStaticPathName;
	}
	else//动态图片
	{
		sFilePathName = pPic->m_PicDirectory + "\\" + pPic->m_PicStaticName;

	}

	if (sFilePathName.GetLength() < 1)
	{
		return;
	}
	//加载图片  
	CBitmap m_BkGndBmp;

	sFilePathName.Replace("\\", "\\\\");
	HBITMAP hBitmap = (HBITMAP)::LoadImage(AfxGetInstanceHandle(), sFilePathName, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE);
	if (!hBitmap)
	{
		return;
	}

	int RotateAngle = pPic->nPicRotateAngle;//旋转角度
	hBitmap = GetRotatedBitmapNT(hBitmap, RotateAngle *PI / 180, RGB(255, 255, 255));

	m_BkGndBmp.Attach(hBitmap);

	//获取图片大小  
	BITMAP bm;
	m_BkGndBmp.GetBitmap(&bm);

	//创建内存DC  
	CDC memDC;
	memDC.CreateCompatibleDC(pDC);

	//将位图选入DC  
	CBitmap *pOldBmp = memDC.SelectObject(&m_BkGndBmp);

	{
		//拷贝DC到窗口  
		if (pPic->nWidth == 50 && pPic->nHeight == 88)//图片第一次加载
		{
			pDC->StretchBlt(pPic->startX, pPic->startY, int(bm.bmWidth / ZoomInX_Pic), int(bm.bmHeight / ZoomInY_Pic), &memDC, 0, 0, (int)(bm.bmWidth), (int)(bm.bmHeight), SRCCOPY);
			pPic->nWidth = int(bm.bmWidth / ZoomInX_Pic);
			pPic->nHeight = int(bm.bmHeight / ZoomInY_Pic);
		}
		else
			pDC->StretchBlt(CoordZoomOut(pPic->startX, 0), CoordZoomOut(pPic->startY, 1), pPic->nWidth * ZoomInX, pPic->nHeight * ZoomInY, &memDC, 0, 0, (int)(bm.bmWidth), (int)(bm.bmHeight), SRCCOPY);
	}

	//清理内存  
	memDC.SelectObject(pOldBmp);
	m_BkGndBmp.DeleteObject();
	memDC.DeleteDC();
	
}



HBITMAP CMyDrawView::GetRotatedBitmapNT(HBITMAP hBitmap, float radians, COLORREF clrBack)
{
	// Create a memory DC compatible with the display  
	CDC sourceDC, destDC;
	sourceDC.CreateCompatibleDC(NULL);
	destDC.CreateCompatibleDC(NULL);

	// Get logical coordinates  
	BITMAP bm;
	::GetObject(hBitmap, sizeof(bm), &bm);

	float cosine = (float)cos(radians);
	float sine = (float)sin(radians);

	// Compute dimensions of the resulting bitmap  
	// First get the coordinates of the 3 corners other than origin  
	int x1 = (int)(bm.bmHeight * sine);
	int y1 = (int)(bm.bmHeight * cosine);
	int x2 = (int)(bm.bmWidth * cosine + bm.bmHeight * sine);
	int y2 = (int)(bm.bmHeight * cosine - bm.bmWidth * sine);
	int x3 = (int)(bm.bmWidth * cosine);
	int y3 = (int)(-bm.bmWidth * sine);

	int minx = min(0, min(x1, min(x2, x3)));
	int miny = min(0, min(y1, min(y2, y3)));
	int maxx = max(0, max(x1, max(x2, x3)));
	int maxy = max(0, max(y1, max(y2, y3)));

	int w = maxx - minx;
	int h = maxy - miny;

	// Create a bitmap to hold the result  

	HBITMAP hbmResult = ::CreateCompatibleBitmap(CClientDC(NULL), w, h);

	HBITMAP hbmOldSource = (HBITMAP)::SelectObject(sourceDC.m_hDC, hBitmap);
	HBITMAP hbmOldDest = (HBITMAP)::SelectObject(destDC.m_hDC, hbmResult);

	// Draw the background color before we change mapping mode  
	HBRUSH hbrBack = CreateSolidBrush(clrBack);
	HBRUSH hbrOld = (HBRUSH)::SelectObject(destDC.m_hDC, hbrBack);
	destDC.PatBlt(0, 0, w, h, PATCOPY);
	::DeleteObject(::SelectObject(destDC.m_hDC, hbrOld));

	// We will use world transform to rotate the bitmap  
	SetGraphicsMode(destDC.m_hDC, GM_ADVANCED);
	XFORM xform;
	xform.eM11 = cosine;
	xform.eM12 = -sine;
	xform.eM21 = sine;
	xform.eM22 = cosine;
	xform.eDx = (float)-minx;
	xform.eDy = (float)-miny;

	SetWorldTransform(destDC.m_hDC, &xform);

	// Now do the actual rotating - a pixel at a time  
	destDC.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &sourceDC, 0, 0, SRCCOPY);

	// Restore DCs  
	::SelectObject(sourceDC.m_hDC, hbmOldSource);
	::SelectObject(destDC.m_hDC, hbmOldDest);


	DeleteObject(hBitmap);//最后还要使用这个删除HBITMAP句柄

	return hbmResult;
}


// 将DC旋转一定的角度
int RotateDC(HDC hDc, float iAngle, POINT centerPt)
{
	int nGraphicsMode = SetGraphicsMode(hDc, GM_ADVANCED);
	XFORM xform;
	if (iAngle != 0)
	{
		double fangle = iAngle / 180. * PI;
		xform.eM11 = (float)cos(fangle);
		xform.eM12 = (float)sin(fangle);
		xform.eM21 = (float)-sin(fangle);
		xform.eM22 = (float)cos(fangle);
		xform.eDx = (float)(centerPt.x - cos(fangle)*centerPt.x + sin(fangle)*centerPt.y);
		xform.eDy = (float)(centerPt.y - cos(fangle)*centerPt.y - sin(fangle)*centerPt.x);
		SetWorldTransform(hDc, &xform);
	}
	return nGraphicsMode;
}

// 恢复旋转过的DC
void RestoreRotatedDC(HDC hDc, int nGraphicsMode)
{
	XFORM xform;
	xform.eM11 = (float)1.0;
	xform.eM12 = (float)0;
	xform.eM21 = (float)0;
	xform.eM22 = (float)1.0;
	xform.eDx = (float)0;
	xform.eDy = (float)0;

	SetWorldTransform(hDc, &xform);
	SetGraphicsMode(hDc, nGraphicsMode);
}


int  CMyDrawView::P_DrawBarcode(CDC*pDC, int iX, int iY0, int iY10, int iY11, COLORREF clrBar, COLORREF clrSpace, float iPenW, CBarCode* pBc, int m_nFrameType, int m_nRotateTyp)
{

	int w = 0;
	int nPenW = 0;
	CBitmap m_pSymbleBitmap ;

	if (m_nRotateTyp == 1 || m_nRotateTyp == 3)
	{
		w = pBc->getWidth_90(iX, iY0, iY10, iY11, clrBar, clrSpace, iPenW, m_nFrameType);

		////如果未设置画笔宽度，
		float BitMapLenth = pBc->getBitMapLenth();
		if (fabs(iPenW - ZoomInX) < 1e-6 &&fabs(BitMapLenth - 100) > 1e-6)
			iPenW = BitMapLenth / w;
		nPenW = (int)iPenW;
		if (nPenW == 0)nPenW = 1;
		m_pSymbleBitmap.CreateBitmap(iY11, w*nPenW, 1, 1, NULL);
	}

	else
	{
		w = pBc->getWidth(iX, iY0, iY10, iY11, clrBar, clrSpace, iPenW, m_nFrameType);

		////如果未设置画笔宽度，
		float BitMapLenth = pBc->getBitMapLenth();
		if (fabs(iPenW - ZoomInX) < 1e-6 &&fabs(BitMapLenth - 100) > 1e-6)
			iPenW = BitMapLenth / w;
		nPenW = (int)iPenW;
		if (nPenW == 0)nPenW = 1;
		m_pSymbleBitmap.CreateBitmap(w*nPenW, iY11, 1, 1, NULL);
	}


	//创建内存DC  
	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* m_pOldBitmap = memDC.SelectObject(&m_pSymbleBitmap);

	if (m_nRotateTyp == 1 || m_nRotateTyp == 3)
	{
		memDC.PatBlt(0, 0, w*nPenW, iY11, WHITENESS);
	}
	else
	{
		memDC.PatBlt(0, 0, iY11, w*nPenW, WHITENESS);
	}


	int nlen = w;
	if (m_nRotateTyp == 0)
		nlen = pBc->DrawBarcode(memDC.m_hDC, iX, iY0, iY10, iY11, clrBar, clrSpace, nPenW, m_nFrameType);
	else if (m_nRotateTyp == 1)
		nlen = pBc->DrawBarcode_90(memDC.m_hDC, iX, iY0, iY10, iY11, clrBar, clrSpace, nPenW, m_nFrameType);
	else if (m_nRotateTyp == 2)
		nlen = pBc->DrawBarcode(memDC.m_hDC, iX, iY0, iY10, iY11, clrBar, clrSpace, nPenW, m_nFrameType);
	else if (m_nRotateTyp == 3)
		nlen = pBc->DrawBarcode_90(memDC.m_hDC, iX, iY0, iY10, iY11, clrBar, clrSpace, nPenW, m_nFrameType);
	else//0°
		nlen = pBc->DrawBarcode(memDC.m_hDC, iX, iY0, iY10, iY11, clrBar, clrSpace, nPenW, m_nFrameType);

	if (m_nRotateTyp == 1 || m_nRotateTyp == 3)
	{
		//拷贝DC到窗口  
		pDC->StretchBlt(iX, iY0, iY10, w*iPenW * ZoomInX, &memDC, 0, 0, iY11, w*nPenW, SRCAND);
	}
	else
	{
		//拷贝DC到窗口  
		pDC->StretchBlt(iX, iY0, w*iPenW * ZoomInX, iY10, &memDC, 0, 0, w*nPenW, iY11, SRCAND);
	}

	
	//清理内存  
	memDC.SelectObject(m_pOldBitmap);
	m_pSymbleBitmap.DeleteObject();
	memDC.DeleteDC();

	return w*iPenW;

}


int  CMyDrawView::P_DrawQRcode(CDC*pDC, int iX, int iY0, int iY10, int iY11, CString sCodeString, int m_nFrameType, int m_nRotateTyp, int nQRLevel, int nQRVersion, BOOL bQRAutoExtent, int nQRMaskingNo, float angle)
{

	int nLevel = nQRLevel;//m_pwndRightView->m_comboLevel.GetCurSel();
	int nVersion = nQRVersion;//m_pwndRightView->m_comboVersion.GetCurSel();
	BOOL bAutoExtent = bQRAutoExtent;//(m_pwndRightView->m_checkAutoExtent.GetCheck() != 0);
	int nMaskingNo = nQRMaskingNo - 1;//m_pwndRightView->m_comboMaskingNo.GetCurSel() - 1;
	float fQRRotateAngle = angle;

	CString strEncodeData = sCodeString;
	int i, j;

	CQR_Encode* pQR_Encode = new CQR_Encode;

	BOOL m_bDataEncoded = pQR_Encode->EncodeData(nLevel, nVersion, bAutoExtent, nMaskingNo, strEncodeData);

	int w = 0;
	if (m_bDataEncoded)
	{

		int m_nSymbleSize = pQR_Encode->m_nSymbleSize + (QR_MARGIN * 2);

		CBitmap m_pSymbleBitmap;
		m_pSymbleBitmap.CreateBitmap(m_nSymbleSize, m_nSymbleSize, 1, 1, NULL);

		//创建内存DC  
		CDC memDC;
		memDC.CreateCompatibleDC(pDC);
		CBitmap* m_pOldBitmap = memDC.SelectObject(&m_pSymbleBitmap);

		//memDC.PatBlt(0, 0, m_nSymbleSize, m_nSymbleSize, WHITENESS);
		CRect rect(0, 0, m_nSymbleSize, m_nSymbleSize);
		//填充白色背景色
		memDC.FillSolidRect(rect, RGB(255, 255, 255));
		memDC.SetBkMode(TRANSPARENT);

		if (m_nRotateTyp == 0)//0°
		{
			for (i = 0; i < pQR_Encode->m_nSymbleSize; ++i)
			{
				for (j = 0; j < pQR_Encode->m_nSymbleSize; ++j)
				{
					if (pQR_Encode->m_byModuleData[i][j])
					{
						memDC.SetPixel(i + QR_MARGIN, j + QR_MARGIN, RGB(0, 0, 0));
					}
				}
			}
		}
		else if (m_nRotateTyp == 1)//90°
		{
			for (i = 0; i < pQR_Encode->m_nSymbleSize; ++i)
			{
				for (j = 0; j < pQR_Encode->m_nSymbleSize; ++j)
				{
					if (pQR_Encode->m_byModuleData[i][j])
					{
						memDC.SetPixel(m_nSymbleSize - (j + QR_MARGIN) - 1, i + QR_MARGIN, RGB(0, 0, 0));
					}
				}
			}
		}
		else if (m_nRotateTyp == 2)//180°
		{
			for (i = 0; i < pQR_Encode->m_nSymbleSize; ++i)
			{
				for (j = 0; j < pQR_Encode->m_nSymbleSize; ++j)
				{
					if (pQR_Encode->m_byModuleData[i][j])
					{
						memDC.SetPixel(m_nSymbleSize - (i + QR_MARGIN) - 1, m_nSymbleSize - (j + QR_MARGIN) - 1, RGB(0, 0, 0));
					}
				}
			}
		}
		else if (m_nRotateTyp == 3)//270°
		{
			for (i = 0; i < pQR_Encode->m_nSymbleSize; ++i)
			{
				for (j = 0; j < pQR_Encode->m_nSymbleSize; ++j)
				{
					if (pQR_Encode->m_byModuleData[i][j])
					{
						memDC.SetPixel((j + QR_MARGIN), m_nSymbleSize - (i + QR_MARGIN) - 1, RGB(0, 0, 0));
					}
				}
			}
		}

		w = pQR_Encode->m_nSymbleSize + (QR_MARGIN * 2);
		delete pQR_Encode;


		//先放大后旋转
		CBitmap m_pSymbleBitmap0 ;
		m_pSymbleBitmap0.CreateBitmap(iY10, iY10, 1, 1, NULL);

		//创建内存DC  
		CDC memDC1;
		memDC1.CreateCompatibleDC(pDC);
		CBitmap* m_pOldBitmap0 = memDC1.SelectObject(&m_pSymbleBitmap0);
		//memDC0.PatBlt(0, 0, iY10, iY10, WHITENESS);
		CRect rect0(0, 0, iY10, iY10);
		//填充白色背景色
		memDC1.FillSolidRect(rect0, RGB(255, 255, 255));
		memDC1.SetBkMode(TRANSPARENT);
		//拷贝DC到窗口  
		memDC1.StretchBlt(0, 0, iY10, iY10, &memDC, 0, 0, w, w, SRCAND);
		//清理内存  
		memDC.SelectObject(m_pOldBitmap);
		m_pSymbleBitmap.DeleteObject();
		memDC.DeleteDC(); 	

		HBITMAP hBitmap = (HBITMAP)GetCurrentObject(memDC1.m_hDC, OBJ_BITMAP);
		//清理内存  
		memDC1.SelectObject(m_pOldBitmap0);
		HBITMAP hBitmap3 = GetRotatedBitmapNT(hBitmap, fQRRotateAngle * PI / 180, RGB(255, 255, 255));
		m_pSymbleBitmap0.DeleteObject(); 
		memDC1.DeleteDC();
		

		CBitmap m_BkGndBmp;
		m_BkGndBmp.Attach(hBitmap3);
		//将位图选入DC  
		//创建内存DC  
		CDC memDC2;
		memDC2.CreateCompatibleDC(pDC);

		//将位图选入DC  
		CBitmap *pOldBmp = memDC2.SelectObject(&m_BkGndBmp);

		//获取图片大小  
		BITMAP bm;
		m_BkGndBmp.GetBitmap(&bm);
		{
			//拷贝DC到窗口  
			pDC->StretchBlt(iX, iY0, iY10, iY10, &memDC2, 0, 0, (int)(bm.bmWidth), (int)(bm.bmHeight), SRCAND);
		}

		//清理内存  
		memDC2.SelectObject(pOldBmp);
		m_BkGndBmp.DeleteObject();
		memDC2.DeleteDC();
		
	}

	return iY10;
}



int  CMyDrawView::P_DrawDMcode(CDC*pDC, int iX, int iY0, int iY10, int iY11, CString sCodeString, int m_nFrameType, int m_nRotateTyp)
{

	//CString strEncodeData = sCodeString;
	//int i, j;

	//DmtxEncode* enc = dmtxEncodeCreate();
	//enc->pixelPacking = DmtxPack24bppRGB;

	//enc->method = 0; //
	//enc->scheme = 0; //是否二进制
	//enc->moduleSize = 5;//符号大小
	//enc->marginSize = 0;//边缘

	//enc->imageFlip = DmtxFlipNone;
	//BOOL m_bDataEncoded = dmtxEncodeDataMatrix(enc, strlen(strEncodeData.GetBuffer()), (unsigned char*)strEncodeData.GetBuffer());

	//int w = 0;
	//if (m_bDataEncoded)
	//{
	//	DmtxImage *img = enc->image;
	//	const int w = img->width, h = img->height;

	//	CBitmap* m_pSymbleBitmap = new CBitmap;
	//	m_pSymbleBitmap->CreateBitmap(w, h, 1, 1, NULL);

	//	//创建内存DC  
	//	CDC memDC;
	//	memDC.CreateCompatibleDC(pDC);
	//	CBitmap* m_pOldBitmap = memDC.SelectObject(m_pSymbleBitmap);

	//	//memDC.PatBlt(0, 0, w, h, WHITENESS);
	//	CRect rect(0, 0, w, h);
	//	//填充白色背景色
	//	CBrush brush(RGB(255, 255, 255));
	//	memDC.FillRect(rect, &brush);
	//	memDC.SetBkMode(TRANSPARENT);

	//	if (m_nRotateTyp == 0)//0°
	//	{
	//		for (i = 0; i < h; ++i)
	//		{
	//			for (j = 0; j < w; ++j)
	//			{
	//				if (!img->pxl[i * 3 * w + j * 3])
	//				{
	//					memDC.SetPixel(j, i, RGB(0, 0, 0));
	//				}
	//			}
	//		}
	//	}
	//	else if (m_nRotateTyp == 1)//90°
	//	{
	//		for (i = 0; i < h; ++i)
	//		{
	//			for (j = 0; j < w; ++j)
	//			{
	//				if (!img->pxl[i * 3 * w + j * 3])
	//				{
	//					memDC.SetPixel(h - i, j, RGB(0, 0, 0));
	//				}
	//			}
	//		}
	//	}
	//	else if (m_nRotateTyp == 2)//180°
	//	{
	//		for (i = 0; i < h; ++i)
	//		{
	//			for (j = 0; j < w; ++j)
	//			{
	//				if (!img->pxl[i * 3 * w + j * 3])
	//				{
	//					memDC.SetPixel(w - j, h - i, RGB(0, 0, 0));
	//				}
	//			}
	//		}
	//	}
	//	else if (m_nRotateTyp == 3)//270°
	//	{
	//		for (i = 0; i < h; ++i)
	//		{
	//			for (j = 0; j < w; ++j)
	//			{
	//				if (!img->pxl[i * 3 * w + j * 3])
	//				{
	//					memDC.SetPixel(i, w - j, RGB(0, 0, 0));
	//				}
	//			}
	//		}
	//	}


	//	delete enc;


	//	//拷贝DC到窗口  
	//	pDC->StretchBlt(iX, iY0, iY10, iY10, &memDC, 0, 0, w, h, SRCAND);

	//	delete m_pSymbleBitmap;
	//	//清理内存  
	//	memDC.SelectObject(m_pOldBitmap);
	//	memDC.DeleteDC();
	//}

	return iY10;

}




int  CMyDrawView::P_DrawPDF417code(CDC*pDC, int iX, int iY0, int iY10, int iY11, CString sCodeString, int m_nFrameType, int m_nRotateTyp)
{

	int img_w, img_h;			//图片像素单位的高度和宽度
	int i, rowNumber;			//临时变量
	char bit = 0;

	PDF417PARAM a;
	PPDF417PARAM pdf417param = &a;
	//初始化pdf417param
	pdf417param->options = PDF417_INVERT_BITMAP | PDF417_FIXED_COLUMNS;
	pdf417param->codeColumns = 1;
	pdf417param->errorLevel = 2;
	pdf417param->outBits = NULL;
	pdf417param->lenBits = 0;
	pdf417param->error = 0;
	pdf417param->lenText = -1;
	pdf417param->text = sCodeString.GetBuffer();
	pdf417param->yHeight = 3;
	pdf417param->aspectRatio = 0.5;


	paintCode(pdf417param);
	if (PDF417_ERROR_SUCCESS != pdf417param->error)
	{
		return pdf417param->error;
	}

	int cols = (pdf417param->lenBits / pdf417param->codeRows) * 8;
	int rows = (pdf417param->codeRows);

	int code_x = 1; int code_y = 3;
	img_w = cols * code_x;
	img_h = rows * code_y;


	int t = 0; int l = 0;

	CBitmap m_pSymbleBitmap ;
	if (m_nRotateTyp == 0 || m_nRotateTyp == 2)
		m_pSymbleBitmap.CreateBitmap(img_w, img_h, 1, 1, NULL);
	else if (m_nRotateTyp == 1 || m_nRotateTyp == 3)
		m_pSymbleBitmap.CreateBitmap(img_h, img_w, 1, 1, NULL);

	//创建内存DC  
	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* m_pOldBitmap = memDC.SelectObject(&m_pSymbleBitmap);
	if (m_nRotateTyp == 0 || m_nRotateTyp == 2)
		memDC.PatBlt(0, 0, img_w, img_h, BLACKNESS);
	else if (m_nRotateTyp == 1 || m_nRotateTyp == 3)
		memDC.PatBlt(0, 0, img_h, img_w, BLACKNESS);

	memDC.SelectStockObject(WHITE_BRUSH);
	memDC.SelectStockObject(WHITE_PEN);

	if (m_nRotateTyp == 0)//0°
	{
		for (i = 0; i < pdf417param->lenBits; i++)
		{
			char j = 0;
			bit = pdf417param->outBits[i];
			t = ((i * 8) / cols) * code_y;
			while (j != 8)
			{
				if (bit & 0x80)//bit最高位为1
				{
					l = code_x*((i * 8) % cols + j);
					memDC.Rectangle(l, t, l + code_x, t + code_y);
				}
				j++;
				bit = bit << 1;
			}
		}
	}
	else if (m_nRotateTyp == 1)//90°
	{
		for (i = 0; i < pdf417param->lenBits; i++)
		{
			char j = 0;
			bit = pdf417param->outBits[i];
			t = ((i * 8) / cols) * code_y;
			while (j != 8)
			{
				if (bit & 0x80)//bit最高位为1
				{
					l = code_x*((i * 8) % cols + j);
					memDC.Rectangle(img_h - t - code_y, l, img_h - t, l + code_x);
				}
				j++;
				bit = bit << 1;
			}
		}
	}
	else if (m_nRotateTyp == 2)//180°
	{
		for (i = 0; i < pdf417param->lenBits; i++)
		{
			char j = 0;
			bit = pdf417param->outBits[i];
			t = ((i * 8) / cols) * code_y;
			while (j != 8)
			{
				if (bit & 0x80)//bit最高位为1
				{
					l = code_x*((i * 8) % cols + j);
					memDC.Rectangle(img_w - l - code_x, img_h - t - code_y, img_w - l, img_h - t);
				}
				j++;
				bit = bit << 1;
			}
		}
	}
	else if (m_nRotateTyp == 3)//270°
	{
		for (i = 0; i < pdf417param->lenBits; i++)
		{
			char j = 0;
			bit = pdf417param->outBits[i];
			t = ((i * 8) / cols) * code_y;
			while (j != 8)
			{
				if (bit & 0x80)//bit最高位为1
				{
					l = code_x*((i * 8) % cols + j);
					memDC.Rectangle(t, img_w - l - code_x, t + code_y, img_w - l);
				}
				j++;
				bit = bit << 1;
			}
		}
	}

	pdf417free(pdf417param);

	int  nLen = 0;
	//拷贝DC到窗口  
	if (m_nRotateTyp == 0 || m_nRotateTyp == 2)
	{
		pDC->StretchBlt(iX, iY0, iY10* img_w / img_h, iY10, &memDC, 0, 0, img_w, img_h, SRCCOPY);
		nLen = iY10* img_w / img_h;
	}
	else if (m_nRotateTyp == 1 || m_nRotateTyp == 3)
	{
		pDC->StretchBlt(iX, iY0, iY10* img_h / img_w, iY10, &memDC, 0, 0, img_h, img_w, SRCCOPY);
		nLen = iY10* img_h / img_w;
	}

	//清理内存  
	memDC.SelectObject(m_pOldBitmap);
	m_pSymbleBitmap.DeleteObject(); 
	memDC.DeleteDC();
	

	return nLen;

}




int  CMyDrawView::P_DrawEAN13code(CDC*pDC, int iX, int iY0, int iY10, int iY11, float iPenW, CString sCodeString, int m_nFrameType, int m_nRotateTyp, int nBitMapLenth)
{

	int img_w, img_h;			//图片像素单位的高度和宽度
	int code_x = 1; int code_y = iY10 - LONG_HEIGHT;
	img_w = 95 * code_x + LEFT_MARGE;
	img_h = code_y + LONG_HEIGHT;


	//float iPenW = 1;
	////如果未设置画笔宽度，
	if (fabs(iPenW - ZoomInX) < 1e-6 && abs(nBitMapLenth - 100) > 1e-6)
		iPenW = ((float)nBitMapLenth) / img_w;

	int t = 0; int l = 0;

	CBitmap m_pSymbleBitmap ;
	if (m_nRotateTyp == 0 || m_nRotateTyp == 2)
		m_pSymbleBitmap.CreateBitmap(img_w, img_h, 1, 1, NULL);
	else if (m_nRotateTyp == 1 || m_nRotateTyp == 3)
		m_pSymbleBitmap.CreateBitmap(img_h, img_w, 1, 1, NULL);

	//创建内存DC  
	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* m_pOldBitmap = memDC.SelectObject(&m_pSymbleBitmap);

	if (m_nRotateTyp == 0 || m_nRotateTyp == 2)
		memDC.PatBlt(0, 0, img_w, img_h, WHITENESS);
	else if (m_nRotateTyp == 1 || m_nRotateTyp == 3)
		memDC.PatBlt(0, 0, img_h, img_w, WHITENESS);

	int left = 0;
	int top = 0;
	if (m_nRotateTyp == 0)//0°
	{
		draw_image(memDC.m_hDC, sCodeString.GetBuffer(), left, top, code_x, code_y);
	}
	else if (m_nRotateTyp == 1)//90°
	{
		draw_image_90(memDC.m_hDC, sCodeString.GetBuffer(), left, top, code_x, code_y);
	}
	else if (m_nRotateTyp == 2)//180°
	{
		draw_image(memDC.m_hDC, sCodeString.GetBuffer(), left, top, code_x, code_y);
	}
	else if (m_nRotateTyp == 3)//270°
	{
		draw_image_90(memDC.m_hDC, sCodeString.GetBuffer(), left, top, code_x, code_y);
	}



	int  nLen = img_w * iPenW;
	//拷贝DC到窗口  
	if (m_nRotateTyp == 0 || m_nRotateTyp == 2)
	{
		pDC->StretchBlt(iX, iY0, nLen* ZoomInX, iY10, &memDC, 0, 0, img_w, img_h, SRCCOPY);
	}
	else if (m_nRotateTyp == 1 || m_nRotateTyp == 3)
	{
		pDC->StretchBlt(iX, iY0, iY10, nLen* ZoomInX, &memDC, 0, 0, img_h, img_w, SRCCOPY);
	}

	//清理内存  
	memDC.SelectObject(m_pOldBitmap);
	m_pSymbleBitmap.DeleteObject();
	memDC.DeleteDC();

	return nLen;

}



int CMyDrawView::OnDrawBarCodeText(CDC*pDC, CBarCode* pBarCode, bool bIsFirst)
{
	CFont cMyFont;
	CFont* pOldFont;

	if (pBarCode->m_nBarPropertyType == 1)
	{
		CString strCounter = GetCounterText(pBarCode->nCounterStart, pBarCode->nCounterEnd, pBarCode->nCounterStep, n_PageIndex);
		pBarCode->setCodeString(strCounter);
		//if (strCounter.GetLength() > 0)
		//{
		//	pBarCode->nCounterStart = _ttol(_T(strCounter));
		//}
	}
	else if (pBarCode->m_nBarPropertyType == 3)
	{
		CString strTime = GetNowTime(pBarCode->m_nTimeFormatIndex);
		strTime.Replace(':', '-');
		pBarCode->setCodeString(strTime);
	}


	//long lfWidth = pBarCode->TextFont.lfWidth;
	//long lfHeight = pBarCode->TextFont.lfHeight;

	int nLenth = pBarCode->getBitMapLenth();
	char* pCode = pBarCode->GetCodeString();
	int nDis = pBarCode->nTextBarDis;
	CString sInsertData = pBarCode->sInsetParam;
	CString sCodeString = "";
	sCodeString.Format("%s", pCode);

	if (pBarCode->m_nBarPropertyType == 0 && !m_bIsEnablePrview)//固定字符
	{
		CString sInsertData = pBarCode->sInsetParam;

		sCodeString = ProcessInsertData(sInsertData, sCodeString, !isGetData);
	}
	else if (pBarCode->m_nBarPropertyType == 1)//计数器
	{

	}
	else if (pBarCode->m_nBarPropertyType == 2)//数据库
	{
		if (pBarCode->sSqlData != "")
			sCodeString = pBarCode->sSqlData;
	}
	else if (pBarCode->m_nBarPropertyType == 3)//日期时间
	{
	}


	//处理分段
	if ((pBarCode->nTextGroupType == 1))
	{
		if (isGetData)
		{
			sCodeString = OnProcessFieldedString(sCodeString, pBarCode->sFieldTypeString, true, pBarCode->nTextGroupDis);
		}
		else
		{
			if (sInsertData.GetLength() == 0)
			{
				sCodeString = OnProcessFieldedString(sCodeString, pBarCode->sFieldTypeString, true, pBarCode->nTextGroupDis);
			}
		}
	}

	else if (pBarCode->nTextGroupType == 0)
	{

		if (isGetData)
		{
			sCodeString = OnProcessFieldedString(sCodeString, atoi(pBarCode->sFieldTypeDis), true, pBarCode->nTextGroupDis);
		}
		else
		{
			if (sInsertData.GetLength() == 0)
			{
				sCodeString = OnProcessFieldedString(sCodeString, atoi(pBarCode->sFieldTypeDis), true, pBarCode->nTextGroupDis);
			}
		}
	}
	pCode = sCodeString.GetBuffer(sCodeString.GetLength());


	if (!isGetData)
	{
		ZoomInX = (float)1.0*Zoom;
		ZoomInY = (float)1.0*Zoom;
	}

	LOGFONT TextFont = pBarCode->TextFont;

	TextFont.lfHeight = (int)(pBarCode->TextFont.lfHeight*ZoomInY); // 改变字体高度 
	TextFont.lfWidth = (int)(pBarCode->TextFont.lfWidth*ZoomInX);

	if (pBarCode->m_nRotateTyp == 0)
		TextFont.lfEscapement = 0;
	if (pBarCode->m_nRotateTyp == 1)
		TextFont.lfEscapement = 2700;
	if (pBarCode->m_nRotateTyp == 2)
		TextFont.lfEscapement = 1800;
	if (pBarCode->m_nRotateTyp == 3)
		TextFont.lfEscapement = 900;


	TextFont.lfHeight = MulDiv(TextFont.lfHeight, pDC->GetDeviceCaps(LOGPIXELSY), 72);

	cMyFont.CreateFontIndirect(&TextFont);
	pOldFont = pDC->SelectObject(&cMyFont);
	pDC->SetTextColor(pBarCode->TextColor);
	//pDC->SetTextCharacterExtra(pBarCode->nTextGroupDis);


	if (pBarCode->m_nRotateTyp == 0)
	{
		int nTxtLeft = CoordZoomOut(pBarCode->startX, 0);
		int nTop = CoordZoomOut(pBarCode->startY + pBarCode->nBitMapHeight + nDis, 1);
		int nRight = CoordZoomOut(pBarCode->startX + nLenth, 0);
		int nBotton = nTop + TextFont.lfHeight;
		CRect m_nRect(nTxtLeft, nTop, nRight, nBotton);

		if (pBarCode->nTextAlineType == 0)
		{
			pDC->DrawText(pCode, m_nRect, DT_CENTER);
		}
		else if (pBarCode->nTextAlineType == 1)
		{
			pDC->DrawText(pCode, m_nRect, DT_LEFT);
		}
		else if (pBarCode->nTextAlineType == 2)
		{
			pDC->DrawText(pCode, m_nRect, DT_RIGHT);
		}
		else if (pBarCode->nTextAlineType == 3)
		{
			pDC->DrawText(pCode, m_nRect, DT_CENTER);
		}

	}

	else if (pBarCode->m_nRotateTyp == 1)
	{
		CSize size = pDC->GetTextExtent(sCodeString);
		int nTxtLeft = CoordZoomOut(pBarCode->startX, 0) - (int)((nDis)*ZoomInY);
		int nTop = CoordZoomOut(pBarCode->startY, 1) + int(nLenth * ZoomInX / 2 - size.cx / 2);

		pDC->TextOut(nTxtLeft, nTop, sCodeString);
	}
	else if (pBarCode->m_nRotateTyp == 2)
	{
		CSize size = pDC->GetTextExtent(sCodeString);
		int nTxtLeft = CoordZoomOut(pBarCode->startX + nLenth, 0) - int(nLenth* ZoomInX / 2 - size.cx / 2);
		int nTop = CoordZoomOut(pBarCode->startY, 1);

		pDC->TextOut(nTxtLeft, nTop, sCodeString);
	}
	else if (pBarCode->m_nRotateTyp == 3)
	{
		CSize size = pDC->GetTextExtent(sCodeString);
		int nTxtLeft = CoordZoomOut(pBarCode->startX + pBarCode->nBitMapHeight + nDis, 0) + 1;
		int nTop = CoordZoomOut(pBarCode->startY + nLenth, 1) - int(nLenth* ZoomInX / 2 - size.cx / 2);

		pDC->TextOut(nTxtLeft, nTop, sCodeString);
	}


	pDC->SelectObject(pOldFont);
	cMyFont.DeleteObject();

	//pBarCode->TextFont.lfWidth = lfWidth;
	//pBarCode->TextFont.lfHeight = lfHeight;

	return (pBarCode->nBitMapHeight + nDis);
}
void CMyDrawView::OnViewAtt()
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;

	POSITION pos = m_ObjectList.GetHeadPosition();
	CObject* pHitItem;

	CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd;

	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL)
			{
				if (pLine->bIsSelected)
				{
					//CLineProperties LineDlg;
					//CString str_LineWidth;
					//str_LineWidth.Format("%d", pLine->LinePen.lopnWidth.x);
					//LineDlg.m_LineWidth = str_LineWidth;

					//if (pLine->LinePen.lopnStyle == PS_DASH)
					//	LineDlg.m_LineStyle = "虚线";
					//else if (pLine->LinePen.lopnStyle == PS_DOT)
					//	LineDlg.m_LineStyle = "点线";
					//else if (pLine->LinePen.lopnStyle == PS_DASHDOT)
					//	LineDlg.m_LineStyle = "点划线";
					//else
					//	LineDlg.m_LineStyle = "实线";

					//LineDlg.m_LineColor = pLine->LinePen.lopnColor;
					//if (LineDlg.DoModal() == IDOK)
					//{
					//	pLine->LinePen.lopnWidth.x = atoi(LineDlg.m_LineWidth);

					//	if (LineDlg.m_LineStyle == "虚线")
					//		pLine->LinePen.lopnStyle = PS_DASH;
					//	else if (LineDlg.m_LineStyle == "点线")
					//		pLine->LinePen.lopnStyle = PS_DOT;
					//	else if (LineDlg.m_LineStyle == "点划线")
					//		pLine->LinePen.lopnStyle = PS_DASHDOT;
					//	else
					//		pLine->LinePen.lopnStyle = PS_SOLID;

					//	pLine->LinePen.lopnColor = LineDlg.m_LineColor;
					//	Invalidate();
					//}





					CEllipseProperties EllipseDlg;
					CString str_LineWidth;
					str_LineWidth.Format("%d", pLine->LinePen.lopnWidth.x);
					EllipseDlg.m_LineWidth = str_LineWidth;

					if (pLine->LinePen.lopnStyle == PS_DASH)
						EllipseDlg.m_LineStyle = "虚线";
					else if (pLine->LinePen.lopnStyle == PS_DOT)
						EllipseDlg.m_LineStyle = "点线";
					else if (pLine->LinePen.lopnStyle == PS_DASHDOT)
						EllipseDlg.m_LineStyle = "点划线";
					else
						EllipseDlg.m_LineStyle = "实线";
					EllipseDlg.m_LineColor = pLine->LinePen.lopnColor;

					EllipseDlg.bOnlyLine = true;

					int nX = pLine->startX;
					int nY = pLine->startY;
					EllipseDlg.m_fTxtLeftPos = nX / ScaleX *Zoom;
					EllipseDlg.m_fTxtTopPos = nY / ScaleY *Zoom;
					EllipseDlg.m_fTxtWidth = (pLine->endX - pLine->startX) / ScaleX  *Zoom;
					EllipseDlg.m_fTxtHeight = (pLine->endY - pLine->startY) / ScaleY  *Zoom;

					if (EllipseDlg.DoModal() == IDOK)
					{
						pLine->LinePen.lopnWidth.x = atoi(EllipseDlg.m_LineWidth);

						if (EllipseDlg.m_LineStyle == "虚线")
							pLine->LinePen.lopnStyle = PS_DASH;
						else if (EllipseDlg.m_LineStyle == "点线")
							pLine->LinePen.lopnStyle = PS_DOT;
						else if (EllipseDlg.m_LineStyle == "点划线")
							pLine->LinePen.lopnStyle = PS_DASHDOT;
						else
							pLine->LinePen.lopnStyle = PS_SOLID;
						pLine->LinePen.lopnColor = EllipseDlg.m_LineColor;

						

						pLine->startX = EllipseDlg.m_fTxtLeftPos * ScaleX / Zoom;
						pLine->startY = EllipseDlg.m_fTxtTopPos * ScaleY / Zoom;

						pLine->endX = pLine->startX + EllipseDlg.m_fTxtWidth * ScaleX / Zoom;
						pLine->endY = pLine->startY + EllipseDlg.m_fTxtHeight * ScaleY / Zoom;

						pLine->bIsSelected = false;
						Invalidate(FALSE);
					}


				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL)
			{
				if (pEllipse->bIsSelected)
				{
					CEllipseProperties EllipseDlg;
					CString str_LineWidth;
					str_LineWidth.Format("%d", pEllipse->LinePen.lopnWidth.x);
					EllipseDlg.m_LineWidth = str_LineWidth;

					if (pEllipse->LinePen.lopnStyle == PS_DASH)
						EllipseDlg.m_LineStyle = "虚线";
					else if (pEllipse->LinePen.lopnStyle == PS_DOT)
						EllipseDlg.m_LineStyle = "点线";
					else if (pEllipse->LinePen.lopnStyle == PS_DASHDOT)
						EllipseDlg.m_LineStyle = "点划线";
					else
						EllipseDlg.m_LineStyle = "实线";
					EllipseDlg.m_LineColor = pEllipse->LinePen.lopnColor;

					EllipseDlg.m_BrushColor = pEllipse->MyBrush.lbColor;
					if (pEllipse->MyBrush.lbHatch == HS_BDIAGONAL)
						EllipseDlg.m_HatchStyle = "HS_BDIAGONAL";
					else if (pEllipse->MyBrush.lbHatch == HS_CROSS)
						EllipseDlg.m_HatchStyle = "HS_CROSS";
					else if (pEllipse->MyBrush.lbHatch == HS_DIAGCROSS)
						EllipseDlg.m_HatchStyle = "HS_DIAGCROSS";
					else if (pEllipse->MyBrush.lbHatch == HS_FDIAGONAL)
						EllipseDlg.m_HatchStyle = "HS_FDIAGONAL";
					else if (pEllipse->MyBrush.lbHatch == HS_HORIZONTAL)
						EllipseDlg.m_HatchStyle = "HS_HORIZONTAL";
					else
						EllipseDlg.m_HatchStyle = "HS_VERTICAL";

					if (pEllipse->MyBrush.lbStyle == BS_HATCHED)
						EllipseDlg.m_BrushStyle = "BS_HATCHED";
					else if (pEllipse->MyBrush.lbStyle == BS_NULL)
						EllipseDlg.m_BrushStyle = "BS_NULL";
					else
						EllipseDlg.m_BrushStyle = "BS_SOLID";

					int nX = pEllipse->startX;
					int nY = pEllipse->startY;
					EllipseDlg.m_fTxtLeftPos = nX / ScaleX * Zoom;
					EllipseDlg.m_fTxtTopPos = nY / ScaleY * Zoom;
					EllipseDlg.m_fTxtWidth = (pEllipse->endX - pEllipse->startX) / ScaleX* Zoom;
					EllipseDlg.m_fTxtHeight = (pEllipse->endY - pEllipse->startY) / ScaleY* Zoom;

					if (EllipseDlg.DoModal() == IDOK)
					{
						pEllipse->LinePen.lopnWidth.x = atoi(EllipseDlg.m_LineWidth);

						if (EllipseDlg.m_LineStyle == "虚线")
							pEllipse->LinePen.lopnStyle = PS_DASH;
						else if (EllipseDlg.m_LineStyle == "点线")
							pEllipse->LinePen.lopnStyle = PS_DOT;
						else if (EllipseDlg.m_LineStyle == "点划线")
							pEllipse->LinePen.lopnStyle = PS_DASHDOT;
						else
							pEllipse->LinePen.lopnStyle = PS_SOLID;
						pEllipse->LinePen.lopnColor = EllipseDlg.m_LineColor;

						pEllipse->MyBrush.lbColor = EllipseDlg.m_BrushColor;
						if (EllipseDlg.m_HatchStyle == "HS_BDIAGONAL")
							pEllipse->MyBrush.lbHatch = HS_BDIAGONAL;
						else if (EllipseDlg.m_HatchStyle == "HS_CROSS")
							pEllipse->MyBrush.lbHatch = HS_CROSS;
						else if (EllipseDlg.m_HatchStyle == "HS_DIAGCROSS")
							pEllipse->MyBrush.lbHatch = HS_DIAGCROSS;
						else if (EllipseDlg.m_HatchStyle == "HS_FDIAGONAL")
							pEllipse->MyBrush.lbHatch = HS_FDIAGONAL;
						else if (EllipseDlg.m_HatchStyle == "HS_HORIZONTAL")
							pEllipse->MyBrush.lbHatch = HS_HORIZONTAL;
						else
							pEllipse->MyBrush.lbHatch = HS_VERTICAL;

						if (EllipseDlg.m_BrushStyle == "BS_HATCHED")
							pEllipse->MyBrush.lbStyle = BS_HATCHED;
						else if (EllipseDlg.m_BrushStyle == "BS_NULL")
							pEllipse->MyBrush.lbStyle = BS_NULL;
						else
							pEllipse->MyBrush.lbStyle = BS_SOLID;

						pEllipse->startX = EllipseDlg.m_fTxtLeftPos * ScaleX / Zoom;
						pEllipse->startY = EllipseDlg.m_fTxtTopPos * ScaleY / Zoom;

						pEllipse->endX = pEllipse->startX + EllipseDlg.m_fTxtWidth * ScaleX / Zoom;
						pEllipse->endY = pEllipse->startY + EllipseDlg.m_fTxtHeight * ScaleY / Zoom;

						pEllipse->bIsSelected = false;
						Invalidate(FALSE);
					}
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL)
			{
				if (pArc->bIsSelected)
				{
					CLineProperties LineDlg;
					CString str_LineWidth;
					str_LineWidth.Format("%d", pArc->LinePen.lopnWidth.x);
					LineDlg.m_LineWidth = str_LineWidth;

					if (pArc->LinePen.lopnStyle == PS_DASH)
						LineDlg.m_LineStyle = "虚线";
					else if (pArc->LinePen.lopnStyle == PS_DOT)
						LineDlg.m_LineStyle = "点线";
					else if (pArc->LinePen.lopnStyle == PS_DASHDOT)
						LineDlg.m_LineStyle = "点划线";
					else
						LineDlg.m_LineStyle = "实线";

					LineDlg.m_LineColor = pArc->LinePen.lopnColor;
					if (LineDlg.DoModal() == IDOK)
					{
						pArc->LinePen.lopnWidth.x = atoi(LineDlg.m_LineWidth);

						if (LineDlg.m_LineStyle == "虚线")
							pArc->LinePen.lopnStyle = PS_DASH;
						else if (LineDlg.m_LineStyle == "点线")
							pArc->LinePen.lopnStyle = PS_DOT;
						else if (LineDlg.m_LineStyle == "点划线")
							pArc->LinePen.lopnStyle = PS_DASHDOT;
						else
							pArc->LinePen.lopnStyle = PS_SOLID;

						pArc->LinePen.lopnColor = LineDlg.m_LineColor;
						Invalidate(FALSE);
					}
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText != NULL)
			{
				if (pText->bIsSelected)
				{
					CTextProperties TextDlg;
					TextDlg.pText = pText;
					if (pText->BkMode == TRANSPARENT)
						TextDlg.m_BkMode = true;
					else
						TextDlg.m_BkMode = false;
					TextDlg.nConstValueType = pText->nConstValueType;
					TextDlg.nFieldIndex = pText->m_nFieldIndex;
					TextDlg.m_startPos = pText->nStartPos;
					TextDlg.m_counts = pText->nCounts;
					TextDlg.m_Font = pText->MyFont;
					TextDlg.m_TextColor = pText->MyColor;
					//					TextDlg.m_Text=pText->MyText; 
					TextDlg.sInsertParam = pText->sInsetSqlParam;
					TextDlg.m_Text = ProcessInsertData(pText->sInsetSqlParam, pText->m_strInitVal, true);
					TextDlg.m_lTxtCounterStartNum = pText->m_lTxtCounterStartNum;
					TextDlg.m_lTxtCounterEndNum = pText->m_lTxtCounterEndNum;
					TextDlg.m_lTxtCounterStep = pText->m_lTxtCounterStep;
					TextDlg.nTimeFormatIndex = pText->m_nTimeFormatIndex;

					int nX = pText->startX;
					int nY = pText->startY;
					TextDlg.m_fTxtLeftPos = nX / ScaleX * Zoom;
					TextDlg.m_fTxtTopPos = nY / ScaleY * Zoom;
					TextDlg.m_fTxtWidth = (pText->endX - pText->startX) / ScaleX * Zoom;
					TextDlg.m_fTxtHeight = (pText->endY - pText->startY) / ScaleY * Zoom;

					TextDlg.isTxtUseStyleCtrl = pText->isTxtUseStyleCtrl;//是否使用文本控制
					TextDlg.m_iTxtFieldDis = pText->m_iTxtFieldDis;//组间距
					TextDlg.m_sTxtFieldType = pText->m_sTxtFieldType;//组格式串

					TextDlg.nTxtRotateType = pText->nTxtRotateType;
					TextDlg.nTxtRotateAngle = pText->nTxtRotateAngle;
					TextDlg.SetSqlFieldNumParam(nMaxColNum, pText->m_nFieldIndex);

					TextDlg.isMirrorImage = pText->isMirrorImage;
					TextDlg.isUseAnyStyle = pText->isUseAnyStyle;
					TextDlg.isUseDeburring = pText->isUseDeburring;
					TextDlg.nDeburring_x = pText->nDeburring_x;
					TextDlg.nDeburring_y = pText->nDeburring_y;

					TextDlg.isDelete0 = pText->isDelete0;

					if (TextDlg.DoModal() == IDOK)
					{
						pText->MyFont = TextDlg.m_Font;
						pText->MyColor = TextDlg.m_TextColor;

						//删除字段后的处理
						CStringArray strArray;
						SplitString(TextDlg.sInsertParam, '*', strArray);
						CString sInsertParamTemp = "";
						for (int n = 0; n <= nMaxColNum; n++)
						{
							CString sTmp = "";
							sTmp.Format("[字段%d]", n);
							if (TextDlg.m_Text.Find(sTmp) != -1)
							{
								for (int m = 0; m < strArray.GetCount(); m++)
								{
									if (strArray[m].Find(sTmp) != -1)
									{
										sInsertParamTemp += strArray[m] + "*";
									}
								}
							}
						}
						TextDlg.sInsertParam = sInsertParamTemp;


						CString Tmp = TextDlg.m_Text;
						for (int n = 0; n <= nMaxColNum; n++)
						{
							CString sTmp = "";
							sTmp.Format("[字段%d]", n);
							Tmp.Replace(sTmp, "");
						}

						pText->MyText = Tmp;
						pText->m_strInitVal = Tmp;

						//						pText->MyText=TextDlg.m_Text;  
						pText->sInsetSqlParam = TextDlg.sInsertParam;
						pText->nConstValueType = TextDlg.nConstValueType;
						pText->m_nFieldIndex = TextDlg.nFieldIndex;
						pText->nStartPos = TextDlg.m_startPos;
						pText->nCounts = TextDlg.m_counts;
						pText->m_lTxtCounterStartNum = TextDlg.m_lTxtCounterStartNum;
						pText->m_lTxtCounterEndNum = TextDlg.m_lTxtCounterEndNum;
						pText->m_lTxtCounterStep = TextDlg.m_lTxtCounterStep;
						pText->m_nTimeFormatIndex = TextDlg.nTimeFormatIndex;
						pText->isMirrorImage = TextDlg.isMirrorImage;
						pText->isUseAnyStyle = TextDlg.isUseAnyStyle;
						pText->nTxtRotateAngle = TextDlg.nTxtRotateAngle;
						pText->nTxtRotateType = TextDlg.nTxtRotateType;
						if (pText->nTxtRotateType == 0)pText->nTxtRotateAngle = 0;
						if (pText->nTxtRotateType == 1)pText->nTxtRotateAngle = 90;
						if (pText->nTxtRotateType == 2)pText->nTxtRotateAngle = 180;
						if (pText->nTxtRotateType == 3)pText->nTxtRotateAngle = 270;


						if (TextDlg.m_BkMode)
							pText->BkMode = TRANSPARENT;
						else
							pText->BkMode = OPAQUE;

						pText->isTxtUseStyleCtrl = TextDlg.isTxtUseStyleCtrl;//是否使用文本控制
						pText->m_iTxtFieldDis = TextDlg.m_iTxtFieldDis;//组间距
						if (!pText->isTxtUseStyleCtrl)
						{
							pText->m_iTxtFieldDis = 0;
						}
						pText->m_sTxtFieldType = TextDlg.m_sTxtFieldType;//组格式串

						pText->isUseDeburring = TextDlg.isUseDeburring;
						pText->nDeburring_x = TextDlg.nDeburring_x;
						pText->nDeburring_y = TextDlg.nDeburring_y;

						pText->isDelete0 = TextDlg.isDelete0;

						pText->startX = TextDlg.m_fTxtLeftPos * ScaleX / Zoom;
						pText->startY = TextDlg.m_fTxtTopPos * ScaleY / Zoom;

						pText->endX = pText->startX + TextDlg.m_fTxtWidth * ScaleX / Zoom;
						pText->endY = pText->startY + TextDlg.m_fTxtHeight * ScaleY / Zoom;

						pText->tracker.m_rect.SetRect(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), CoordZoomOut(pText->endX, 0), CoordZoomOut(pText->endY, 1));
						if (pText->nConstValueType == 0 && pText->m_nFieldIndex >= 0 && m_bIsEnablePrview)
						{
							CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
							int nPageNum;
							CString strPageNum;
							int nRecordNums = pMainFrame->pPrintDlg->m_RecordNumList.GetItemCount();
							pMainFrame->m_barText.GetWindowTextA(strPageNum);
							if (!strPageNum.IsEmpty())
							{
								nPageNum = _ttoi(strPageNum);
								pText->m_strFieldDes = ProcessInsertData(pText->sInsetSqlParam, pText->m_strInitVal, true);
								pText->m_strRecordVal = pText->m_strFieldDes;
								for (int i = 0; i < nMaxColNum; i++)
								{
									CString fieldNum;
									fieldNum.Format(_T("%d"), i + 1);
									CString tmp = "[字段" + fieldNum + "]";
									CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
									//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
									CString sCodeString = pText->m_strRecordVal;
									//sCodeString = pText->MyText;
									sCodeString.Replace(tmp, sPrint);
									//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
									pText->MyText = sCodeString;
									pText->m_strRecordVal = sCodeString;
								}
							}

						}
						if (pText->nConstValueType == 1)
						{
							pText->MyText = GetCounterText(pText->m_lTxtCounterStartNum, pText->m_lTxtCounterEndNum, pText->m_lTxtCounterStep, 1);
							//if (pText->MyText.GetLength() > 0)
							//{
							//	pText->m_lTxtCounterStartNum = _ttol(_T(pText->MyText));
							//}
						}
						else if (pText->nConstValueType == 2)
						{
							CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
							CString strPageNum;
							int nPageNum;
							int nRecordNums = pMainFrame->pPrintDlg->m_RecordNumList.GetItemCount();
							pMainFrame->m_barText.GetWindowTextA(strPageNum);
							if (strPageNum.IsEmpty())
							{
								continue;
							}
							nPageNum = _ttoi(strPageNum);
							if (!m_bIsEnablePrview && (nPageNum <= 0 || nPageNum > nRecordNums))
							{
								continue;
							}
							CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pText->m_nFieldIndex);
							if (pText->nStartPos >= 0 && pText->nStartPos < sPrint.GetLength() && pText->nCounts>0 && pText->nCounts <= sPrint.GetLength())
							{
								sPrint = sPrint.Mid(pText->nStartPos, pText->nCounts);
							}
							pText->MyText = sPrint;
						}
						else if (pText->nConstValueType == 3)
						{

							pText->MyText = GetNowTime(pText->m_nTimeFormatIndex);
						}

						pText->bIsSelected = false;

						Invalidate(FALSE);
					}
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL)
			{
				if (pRectangle->bIsSelected)
				{
					CEllipseProperties EllipseDlg;
					CString str_LineWidth;
					str_LineWidth.Format("%d", pRectangle->LinePen.lopnWidth.x);
					EllipseDlg.m_LineWidth = str_LineWidth;

					if (pRectangle->LinePen.lopnStyle == PS_DASH)
						EllipseDlg.m_LineStyle = "虚线";
					else if (pRectangle->LinePen.lopnStyle == PS_DOT)
						EllipseDlg.m_LineStyle = "点线";
					else if (pRectangle->LinePen.lopnStyle == PS_DASHDOT)
						EllipseDlg.m_LineStyle = "点划线";
					else
						EllipseDlg.m_LineStyle = "实线";
					EllipseDlg.m_LineColor = pRectangle->LinePen.lopnColor;

					EllipseDlg.m_BrushColor = pRectangle->MyBrush.lbColor;
					if (pRectangle->MyBrush.lbHatch == HS_BDIAGONAL)
						EllipseDlg.m_HatchStyle = "HS_BDIAGONAL";
					else if (pRectangle->MyBrush.lbHatch == HS_CROSS)
						EllipseDlg.m_HatchStyle = "HS_CROSS";
					else if (pRectangle->MyBrush.lbHatch == HS_DIAGCROSS)
						EllipseDlg.m_HatchStyle = "HS_DIAGCROSS";
					else if (pRectangle->MyBrush.lbHatch == HS_FDIAGONAL)
						EllipseDlg.m_HatchStyle = "HS_FDIAGONAL";
					else if (pRectangle->MyBrush.lbHatch == HS_HORIZONTAL)
						EllipseDlg.m_HatchStyle = "HS_HORIZONTAL";
					else
						EllipseDlg.m_HatchStyle = "HS_VERTICAL";

					if (pRectangle->MyBrush.lbStyle == BS_HATCHED)
						EllipseDlg.m_BrushStyle = "BS_HATCHED";
					else if (pRectangle->MyBrush.lbStyle == BS_NULL)
						EllipseDlg.m_BrushStyle = "BS_NULL";
					else
						EllipseDlg.m_BrushStyle = "BS_SOLID";

					int nX = pRectangle->startX;
					int nY = pRectangle->startY;
					EllipseDlg.m_fTxtLeftPos = nX / ScaleX * Zoom;
					EllipseDlg.m_fTxtTopPos = nY / ScaleY * Zoom;
					EllipseDlg.m_fTxtWidth = (pRectangle->endX - pRectangle->startX) / ScaleX * Zoom;
					EllipseDlg.m_fTxtHeight = (pRectangle->endY - pRectangle->startY) / ScaleY * Zoom;

					if (EllipseDlg.DoModal() == IDOK)
					{
						pRectangle->LinePen.lopnWidth.x = atoi(EllipseDlg.m_LineWidth);

						if (EllipseDlg.m_LineStyle == "虚线")
							pRectangle->LinePen.lopnStyle = PS_DASH;
						else if (EllipseDlg.m_LineStyle == "点线")
							pRectangle->LinePen.lopnStyle = PS_DOT;
						else if (EllipseDlg.m_LineStyle == "点划线")
							pRectangle->LinePen.lopnStyle = PS_DASHDOT;
						else
							pRectangle->LinePen.lopnStyle = PS_SOLID;
						pRectangle->LinePen.lopnColor = EllipseDlg.m_LineColor;

						pRectangle->MyBrush.lbColor = EllipseDlg.m_BrushColor;
						if (EllipseDlg.m_HatchStyle == "HS_BDIAGONAL")
							pRectangle->MyBrush.lbHatch = HS_BDIAGONAL;
						else if (EllipseDlg.m_HatchStyle == "HS_CROSS")
							pRectangle->MyBrush.lbHatch = HS_CROSS;
						else if (EllipseDlg.m_HatchStyle == "HS_DIAGCROSS")
							pRectangle->MyBrush.lbHatch = HS_DIAGCROSS;
						else if (EllipseDlg.m_HatchStyle == "HS_FDIAGONAL")
							pRectangle->MyBrush.lbHatch = HS_FDIAGONAL;
						else if (EllipseDlg.m_HatchStyle == "HS_HORIZONTAL")
							pRectangle->MyBrush.lbHatch = HS_HORIZONTAL;
						else
							pRectangle->MyBrush.lbHatch = HS_VERTICAL;

						if (EllipseDlg.m_BrushStyle == "BS_HATCHED")
							pRectangle->MyBrush.lbStyle = BS_HATCHED;
						else if (EllipseDlg.m_BrushStyle == "BS_NULL")
							pRectangle->MyBrush.lbStyle = BS_NULL;
						else
							pRectangle->MyBrush.lbStyle = BS_SOLID;

						pRectangle->startX = EllipseDlg.m_fTxtLeftPos * ScaleX / Zoom;
						pRectangle->startY = EllipseDlg.m_fTxtTopPos * ScaleY / Zoom;

						pRectangle->endX = pRectangle->startX + EllipseDlg.m_fTxtWidth * ScaleX / Zoom;
						pRectangle->endY = pRectangle->startY + EllipseDlg.m_fTxtHeight * ScaleY / Zoom;

						pRectangle->bIsSelected = false;
						Invalidate(FALSE);
					}
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic != NULL)
			{
				if (pPic->bIsSelected)
				{
					CPicProperties mPicProperties;
					mPicProperties.pPic = pPic;
					mPicProperties.m_fPicLeft = pPic->startX / ScaleX * Zoom;
					mPicProperties.m_fPicTop = pPic->startY / ScaleY * Zoom;
					mPicProperties.m_fPicWidth = pPic->nWidth / ScaleX * Zoom;
					mPicProperties.m_fPicHeight = pPic->nHeight / ScaleY * Zoom;
					mPicProperties.isStaticPic = pPic->isStaticPic;
					mPicProperties.m_nPicRotateAngle = pPic->nPicRotateAngle;
					mPicProperties.m_bFixScale = pPic->m_isFixScale;
					int nStart = 0;
					int nLen = 0;
					if (pPic->m_PicStaticPathName.ReverseFind('\\') != -1)
					{
						nStart = pPic->m_PicStaticPathName.ReverseFind('\\') + 1;
					}
					nLen = pPic->m_PicStaticPathName.GetLength();
					CString TMP = pPic->m_PicStaticPathName;
					mPicProperties.m_PicStaticName = TMP.Mid(nStart, nLen - nStart);
					mPicProperties.m_PicStaticPathName = pPic->m_PicStaticPathName;
					mPicProperties.m_PicDirectory = pPic->m_PicDirectory;
					mPicProperties.m_PicFieldNum = pPic->m_PicFieldNum;
					mPicProperties.SetSqlFieldNumParam(nMaxColNum, pPic->m_PicFieldNum);

					if (mPicProperties.DoModal() == IDOK)
					{
						pPic->startX = ((int)(mPicProperties.m_fPicLeft*ScaleX /Zoom + 0.005) * 1000) / 1000.0;
						pPic->startY = ((int)(mPicProperties.m_fPicTop*ScaleY / Zoom + 0.005) * 1000) / 1000.0;
						//pPic->nWidth = mPicProperties.m_fPicWidth;
						//pPic->nHeight = mPicProperties.m_fPicHeight;
						pPic->isStaticPic = mPicProperties.isStaticPic;
						pPic->m_PicStaticName = mPicProperties.m_PicStaticName;
						pPic->m_PicStaticPathName = mPicProperties.m_PicStaticPathName;
						pPic->m_PicDirectory = mPicProperties.m_PicDirectory;
						pPic->m_PicFieldNum = mPicProperties.m_PicFieldNum;
						pPic->nPicRotateAngle = mPicProperties.m_nPicRotateAngle;

						pPic->nWidth = mPicProperties.m_fPicWidth * ScaleX / Zoom;
						pPic->nHeight = mPicProperties.m_fPicHeight * ScaleY / Zoom;

						pPic->m_isFixScale = mPicProperties.m_bFixScale;

						pPic->bIsSelected = false;

						Invalidate(FALSE);
					}

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL)
			{
				if (pBarCode->bIsSelected)
				{
					CBarCodeProperties mBarCodeProperties;
					mBarCodeProperties.pBarCode = pBarCode;
					mBarCodeProperties.nBarPropertyType = pBarCode->m_nBarPropertyType;//类型，固定，计数器，数据库，时间

					mBarCodeProperties.sInsertParam = pBarCode->sInsetParam;
					mBarCodeProperties.GetInitParam(nMaxColNum, pBarCode->m_nFieldIndex);
					//mBarCodeProperties.m_EditText = pBarCode->GetCodeString();
					//mBarCodeProperties.m_EditText = ProcessInsertData(pBarCode->sInsetParam,pBarCode->GetCodeString(),true);
					mBarCodeProperties.m_EditText = ProcessInsertData(pBarCode->sInsetParam, pBarCode->m_strInitVal, true);
					int nX = pBarCode->startX;
					int nY = pBarCode->startY;
					mBarCodeProperties.m_PosX = nX / ScaleX * Zoom;;
					mBarCodeProperties.m_PosY = nY / ScaleY * Zoom;;
					mBarCodeProperties.m_BarWidth = (pBarCode->getBitMapLenth()) / ScaleX * Zoom;
					mBarCodeProperties.m_BarHeight = (pBarCode->nBitMapHeight + pBarCode->TextFont.lfHeight + pBarCode->nTextBarDis) / ScaleY*Zoom;
					if (pBarCode->GetCodeType() == 8)//二维码
						mBarCodeProperties.m_BarHeight = (pBarCode->nBitMapHeight) / ScaleY*Zoom;
					else if (pBarCode->GetCodeType() == 9)//二维码
						mBarCodeProperties.m_BarHeight = (pBarCode->nBitMapHeight) / ScaleY*Zoom;
					else if (pBarCode->GetCodeType() == 10)//二维码
						mBarCodeProperties.m_BarHeight = (pBarCode->nBitMapHeight) / ScaleY*Zoom;

					mBarCodeProperties.m_BarPenSize = pBarCode->nPenWidth;
					mBarCodeProperties.m_nBarTypeIndex = pBarCode->GetCodeType() - 1;

					/////计数器
					mBarCodeProperties.m_nStartNum = pBarCode->nCounterStart;
					mBarCodeProperties.m_nEndNum = pBarCode->nCounterEnd;
					mBarCodeProperties.m_nStep = pBarCode->nCounterStep;

					///数据库
					mBarCodeProperties.m_nSqlTypeFieldIndex = pBarCode->m_nSqlTypeFieldIndex;

					mBarCodeProperties.m_nFrameTypeIndex = pBarCode->m_nFrameType;//边框
					mBarCodeProperties.m_nTop = pBarCode->m_nTop;  //上边框宽度值（pixel）
					mBarCodeProperties.m_nBottom = pBarCode->m_nBottom; ////下边框宽度值（pixel）
					mBarCodeProperties.m_nRotateTypeIndex = pBarCode->m_nRotateTyp;//旋转
					mBarCodeProperties.m_nAlineTypeIndex = pBarCode->nTextAlineType;//对齐方式
					mBarCodeProperties.isDefaultTextBarDis = pBarCode->isDefaultTextBarDis;//是否使用默认间距(文字和条码)

					mBarCodeProperties.m_nTextAndBarDis = pBarCode->nTextBarDis;//文本和条码间距

					mBarCodeProperties.m_sArbGroup = pBarCode->sFieldTypeString;//分组格式串
					mBarCodeProperties.m_FixGroup = pBarCode->sFieldTypeDis;//分组间距
					mBarCodeProperties.m_nGroupDis = pBarCode->nTextGroupDis;//组间距
					mBarCodeProperties.nTextSpreadType = pBarCode->nTextGroupType;//分组方式(固定还是任意)

					mBarCodeProperties.TextFont = pBarCode->TextFont;
					mBarCodeProperties.TextColor = pBarCode->TextColor;

					mBarCodeProperties.TextFont.lfHeight;
					mBarCodeProperties.TextFont.lfWidth;
					mBarCodeProperties.TextFont.lfWeight;


					mBarCodeProperties.nQRLevel = pBarCode->nQRLevel;
					mBarCodeProperties.nQRVersion = pBarCode->nQRVersion;
					mBarCodeProperties.bQRAutoExtent = pBarCode->bQRAutoExtent;
					mBarCodeProperties.nQRMaskingNo = pBarCode->nQRMaskingNo;
					mBarCodeProperties.fQRRotateAngle = pBarCode->fQRRotateAngle;

					mBarCodeProperties.m_startPos = pBarCode->nStartPos;
					mBarCodeProperties.m_counts = pBarCode->nCounts;
					mBarCodeProperties.nTimeFormatIndex = pBarCode->m_nTimeFormatIndex;

					if (mBarCodeProperties.DoModal() == IDOK)
					{
						//CString Tmp = ProcessInsertData(pBarCode->sInsetParam,mBarCodeProperties.m_EditText);

						//删除字段后的处理
						CStringArray strArray;
						SplitString(mBarCodeProperties.sInsertParam, '*', strArray);
						CString sInsertParamTemp = "";
						for (int n = 0; n <= nMaxColNum; n++)
						{
							CString sTmp = "";
							sTmp.Format("[字段%d]", n);
							if (mBarCodeProperties.m_EditText.Find(sTmp) != -1)
							{
								for (int m = 0; m < strArray.GetCount(); m++)
								{
									if (strArray[m].Find(sTmp) != -1)
									{
										sInsertParamTemp += strArray[m] + "*";
									}
								}
							}
						}
						mBarCodeProperties.sInsertParam = sInsertParamTemp;


						CString Tmp = mBarCodeProperties.m_EditText;
						for (int n = 0; n <= nMaxColNum; n++)
						{
							CString sTmp = "";
							sTmp.Format("[字段%d]", n);
							Tmp.Replace(sTmp, "");
						}

						pBarCode->m_strInitVal = Tmp;
						pBarCode->setCodeString(Tmp);

						pBarCode->m_nBarPropertyType = mBarCodeProperties.nBarPropertyType;//类型，固定，计数器，数据库，时间
						pBarCode->sInsetParam = mBarCodeProperties.sInsertParam;
						pBarCode->startX = ((int)(mBarCodeProperties.m_PosX*ScaleX / Zoom + 0.005) * 1000) / 1000.0;
						pBarCode->startY = ((int)(mBarCodeProperties.m_PosY*ScaleY / Zoom + 0.005) * 1000) / 1000.0;

						pBarCode->setBitMapLenth(int(mBarCodeProperties.m_BarWidth / Zoom*ScaleX));
						pBarCode->nBitMapHeight = mBarCodeProperties.m_BarHeight / Zoom*ScaleY - pBarCode->TextFont.lfHeight - pBarCode->nTextBarDis;
						if (pBarCode->GetCodeType() == 8)//二维码
							pBarCode->nBitMapHeight = mBarCodeProperties.m_BarHeight / Zoom*ScaleY;
						else if (pBarCode->GetCodeType() == 9)//二维码
							pBarCode->nBitMapHeight = mBarCodeProperties.m_BarHeight / Zoom*ScaleY;
						else if (pBarCode->GetCodeType() == 10)//二维码
							pBarCode->nBitMapHeight = mBarCodeProperties.m_BarHeight / Zoom*ScaleY;

						pBarCode->nPenWidth = mBarCodeProperties.m_BarPenSize;

						pBarCode->SetCodeType(mBarCodeProperties.m_nBarTypeIndex + 1);

						/////计数器
						pBarCode->nCounterStart = mBarCodeProperties.m_nStartNum;
						pBarCode->nCounterEnd = mBarCodeProperties.m_nEndNum;
						pBarCode->nCounterStep = mBarCodeProperties.m_nStep;

						///数据库
						pBarCode->m_nSqlTypeFieldIndex = mBarCodeProperties.m_nSqlTypeFieldIndex;

						pBarCode->m_nFrameType = mBarCodeProperties.m_nFrameTypeIndex;
						pBarCode->m_nTop = mBarCodeProperties.m_nTop;  //上边框宽度值（pixel）
						pBarCode->m_nBottom = mBarCodeProperties.m_nBottom; ////下边框宽度值（pixel）

						pBarCode->m_nRotateTyp = mBarCodeProperties.m_nRotateTypeIndex;
						pBarCode->nTextAlineType = mBarCodeProperties.m_nAlineTypeIndex;

						pBarCode->isDefaultTextBarDis = mBarCodeProperties.isDefaultTextBarDis;
						pBarCode->nTextBarDis = mBarCodeProperties.m_nTextAndBarDis;
						if (pBarCode->isDefaultTextBarDis)
						{
							pBarCode->nTextBarDis = 0;
						}
						pBarCode->nTextGroupDis = mBarCodeProperties.m_nGroupDis;

						pBarCode->TextFont = mBarCodeProperties.TextFont;
						pBarCode->TextColor = mBarCodeProperties.TextColor;

						pBarCode->sFieldTypeString = mBarCodeProperties.m_sArbGroup;
						pBarCode->sFieldTypeDis = mBarCodeProperties.m_FixGroup;//分组间距

						pBarCode->nTextGroupType = mBarCodeProperties.nTextSpreadType;

						pBarCode->nStartPos = mBarCodeProperties.m_startPos;
						pBarCode->nCounts = mBarCodeProperties.m_counts;

						if (pBarCode->TextFont.lfHeight < 0)
						{
							pBarCode->TextFont.lfHeight = -(int)(pBarCode->TextFont.lfHeight * iLOGPIXELSY / 96);
						}
						mBarCodeProperties.TextFont.lfHeight;
						mBarCodeProperties.TextFont.lfWidth;
						mBarCodeProperties.TextFont.lfWeight;

						pBarCode->nQRLevel = mBarCodeProperties.nQRLevel;
						pBarCode->nQRVersion = mBarCodeProperties.nQRVersion;
						pBarCode->bQRAutoExtent = mBarCodeProperties.bQRAutoExtent;
						pBarCode->nQRMaskingNo = mBarCodeProperties.nQRMaskingNo;
						pBarCode->m_nTimeFormatIndex = mBarCodeProperties.nTimeFormatIndex;
						if (pBarCode->m_nBarPropertyType == 0 && pBarCode->m_nFieldIndex >= 0 && m_bIsEnablePrview)
						{
							CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
							int nPageNum;
							CString strPageNum;
							int nRecordNums = pMainFrame->pPrintDlg->m_RecordNumList.GetItemCount();
							pMainFrame->m_barText.GetWindowTextA(strPageNum);
							if (!strPageNum.IsEmpty())
							{
								nPageNum = _ttoi(strPageNum);
								pBarCode->m_strFieldDes = ProcessInsertData(pBarCode->sInsetParam, pBarCode->m_strInitVal, true);
								pBarCode->m_strRecordVal = pBarCode->m_strFieldDes;
								for (int i = 0; i < nMaxColNum; i++)
								{
									CString fieldNum;
									fieldNum.Format(_T("%d"), i + 1);
									CString tmp = "[字段" + fieldNum + "]";
									CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
									//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
									CString sCodeString = pBarCode->m_strRecordVal;
									//sCodeString = pText->MyText;
									sCodeString.Replace(tmp, sPrint);
									//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
									pBarCode->setCodeString(sCodeString);
									pBarCode->m_strRecordVal = sCodeString;
								}
							}

						}
						if (pBarCode->m_nBarPropertyType == 1)
						{
							CString strCountTmp = GetCounterText(pBarCode->nCounterStart, pBarCode->nCounterEnd, pBarCode->nCounterStep, 1);
							pBarCode->setCodeString(strCountTmp);
							//if (strCountTmp.GetLength() > 0)
							//{
							//	pBarCode->nCounterStart = _ttol(_T(strCountTmp));
							//}
						}
						else if (pBarCode->m_nBarPropertyType == 2)
						{
							CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
							CString strPageNum;
							int nPageNum;
							int nRecordNums = pMainFrame->pPrintDlg->m_RecordNumList.GetItemCount();
							pMainFrame->m_barText.GetWindowTextA(strPageNum);
							if (strPageNum.IsEmpty())
							{
								continue;
							}
							nPageNum = _ttoi(strPageNum);
							if (!m_bIsEnablePrview && (nPageNum <= 0 || nPageNum > nRecordNums))
							{
								continue;
							}
							CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pBarCode->m_nSqlTypeFieldIndex);
							if (pBarCode->nStartPos >= 0 && pBarCode->nStartPos < sPrint.GetLength() && pBarCode->nCounts>0 && pBarCode->nCounts <= sPrint.GetLength())
							{
								sPrint = sPrint.Mid(pBarCode->nStartPos, pBarCode->nCounts);
							}
							pBarCode->setCodeString(sPrint);
						}
						else if (pBarCode->m_nBarPropertyType == 3)
						{
							CString strTime = GetNowTime(pBarCode->m_nTimeFormatIndex);
							strTime.Replace(':', '-');
							pBarCode->setCodeString(strTime);
						}
						pBarCode->bIsSelected = false;

						pBarCode->m_nFieldIndex = mBarCodeProperties.nFieldIndex;

						Invalidate(FALSE);
					}
				}

			}
		}


		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL)
			{
				if (pTBarCode11->bIsSelected)
				{
					CBarCodeProperties11 mBarCodeProperties;
					mBarCodeProperties.pTBarCode11 = pTBarCode11;
					mBarCodeProperties.nBarPropertyType = pTBarCode11->m_nBarPropertyType;//类型，固定，计数器，数据库，时间

					mBarCodeProperties.sInsertParam = pTBarCode11->sInsetParam;
					mBarCodeProperties.GetInitParam(nMaxColNum, pTBarCode11->m_nFieldIndex);
					mBarCodeProperties.m_EditText = ProcessInsertData(pTBarCode11->sInsetParam, pTBarCode11->m_strInitVal, true);
					int nX = pTBarCode11->startX;
					int nY = pTBarCode11->startY;
					mBarCodeProperties.m_PosX = nX / ScaleX * Zoom;
					mBarCodeProperties.m_PosY = nY / ScaleY * Zoom;
					mBarCodeProperties.m_BarWidth = (pTBarCode11->endX - pTBarCode11->startX) / ScaleX * Zoom;
					mBarCodeProperties.m_BarHeight = (pTBarCode11->endY - pTBarCode11->startY) / ScaleY*Zoom;
					//if (pTBarCode11->GetCodeType() == 8)//二维码
					//	mBarCodeProperties.m_BarHeight = (pTBarCode11->nBitMapHeight) / ScaleY*Zoom;
					//else if (pTBarCode11->GetCodeType() == 9)//二维码
					//	mBarCodeProperties.m_BarHeight = (pTBarCode11->nBitMapHeight) / ScaleY*Zoom;
					//else if (pTBarCode11->GetCodeType() == 10)//二维码
					//	mBarCodeProperties.m_BarHeight = (pTBarCode11->nBitMapHeight) / ScaleY*Zoom;

					//mBarCodeProperties.m_BarPenSize = pTBarCode11->nPenWidth;
					//mBarCodeProperties.m_nBarTypeIndex = pTBarCode11->GetCodeType() - 1;

					/////计数器
					mBarCodeProperties.m_nStartNum = pTBarCode11->nCounterStart;
					mBarCodeProperties.m_nEndNum = pTBarCode11->nCounterEnd;
					mBarCodeProperties.m_nStep = pTBarCode11->nCounterStep;

					///数据库
					mBarCodeProperties.m_nSqlTypeFieldIndex = pTBarCode11->m_nSqlTypeFieldIndex;

					mBarCodeProperties.m_nFrameTypeIndex = pTBarCode11->m_nFrameType;//边框
					mBarCodeProperties.m_QietZoonUnit = pTBarCode11->m_QietZoonUnit;
					mBarCodeProperties.m_nTop = pTBarCode11->m_nTop;  //上边框宽度值（pixel）
					mBarCodeProperties.m_nBottom = pTBarCode11->m_nBottom; ////下边框宽度值（pixel）
					mBarCodeProperties.m_nLeft = pTBarCode11->m_nLeft;  //上边框宽度值（pixel）
					mBarCodeProperties.m_nRight = pTBarCode11->m_nRight; ////下边框宽度值（pixel）
					mBarCodeProperties.m_nRotateTypeIndex = pTBarCode11->m_nRotateTyp;//旋转
					mBarCodeProperties.m_nAlineTypeIndex = pTBarCode11->nTextAlineType;//对齐方式
					mBarCodeProperties.isDefaultTextBarDis = pTBarCode11->isDefaultTextBarDis;//是否使用默认间距(文字和条码)

					mBarCodeProperties.m_nTextAndBarDis = pTBarCode11->nTextBarDis;//文本和条码间距

					mBarCodeProperties.m_sArbGroup = pTBarCode11->sFieldTypeString;//分组格式串
					mBarCodeProperties.m_FixGroup = pTBarCode11->sFieldTypeDis;//分组间距
					mBarCodeProperties.m_nGroupDis = pTBarCode11->nTextGroupDis;//组间距
					mBarCodeProperties.nTextSpreadType = pTBarCode11->nTextGroupType;//分组方式(固定还是任意)

					mBarCodeProperties.TextFont = pTBarCode11->TextFont;
					mBarCodeProperties.TextColor = pTBarCode11->TextColor;

					mBarCodeProperties.TextFont.lfHeight;
					mBarCodeProperties.TextFont.lfWidth;
					mBarCodeProperties.TextFont.lfWeight;


					mBarCodeProperties.nQRLevel = pTBarCode11->nQRLevel;
					mBarCodeProperties.nQRVersion = pTBarCode11->nQRVersion;
					mBarCodeProperties.bQRAutoExtent = pTBarCode11->bQRAutoExtent;
					mBarCodeProperties.nQRMaskingNo = pTBarCode11->nQRMaskingNo;
					mBarCodeProperties.fQRRotateAngle = pTBarCode11->m_nRotateTyp * 90;

					mBarCodeProperties.m_startPos = pTBarCode11->nStartPos;
					mBarCodeProperties.m_counts = pTBarCode11->nCounts;
					mBarCodeProperties.nTimeFormatIndex = pTBarCode11->m_nTimeFormatIndex;
					mBarCodeProperties.m_nBarTypeIndex = pTBarCode11->GetCodeType() - 1;


					mBarCodeProperties.isUseDeburring = pTBarCode11->bBarWidthReduction;
					mBarCodeProperties.nBarWidthReduction = pTBarCode11->nBarWidthReduction;


					mBarCodeProperties.bReadable = pTBarCode11->bReadable;
					mBarCodeProperties.bAbove = pTBarCode11->bAbove;
					mBarCodeProperties.m_bMirror = pTBarCode11->m_bMirror;

					if (mBarCodeProperties.DoModal() == IDOK)
					{

						//删除字段后的处理
						CStringArray strArray;
						SplitString(mBarCodeProperties.sInsertParam, '*', strArray);
						CString sInsertParamTemp = "";
						for (int n = 0; n <= nMaxColNum; n++)
						{
							CString sTmp = "";
							sTmp.Format("[字段%d]", n);
							if (mBarCodeProperties.m_EditText.Find(sTmp) != -1)
							{
								for (int m = 0; m < strArray.GetCount(); m++)
								{
									if (strArray[m].Find(sTmp) != -1)
									{
										sInsertParamTemp += strArray[m] + "*";
									}
								}
							}
						}
						mBarCodeProperties.sInsertParam = sInsertParamTemp;


						CString Tmp = mBarCodeProperties.m_EditText;
						for (int n = 0; n <= nMaxColNum; n++)
						{
							CString sTmp = "";
							sTmp.Format("[字段%d]", n);
							Tmp.Replace(sTmp, "");
						}

						pTBarCode11->m_strInitVal = Tmp;
						pTBarCode11->SetData(Tmp);

						pTBarCode11->m_nBarPropertyType = mBarCodeProperties.nBarPropertyType;//类型，固定，计数器，数据库，时间
						pTBarCode11->sInsetParam = mBarCodeProperties.sInsertParam;
						pTBarCode11->startX = ((int)(mBarCodeProperties.m_PosX*ScaleX / Zoom + 0.005) * 1000) / 1000.0;
						pTBarCode11->startY = ((int)(mBarCodeProperties.m_PosY*ScaleY / Zoom + 0.005) * 1000) / 1000.0;

						pTBarCode11->nBitMapLenth = (int(mBarCodeProperties.m_BarWidth / Zoom *ScaleX));
						pTBarCode11->nBitMapHeight = mBarCodeProperties.m_BarHeight / Zoom*ScaleY;

						pTBarCode11->endX = pTBarCode11->startX + pTBarCode11->nBitMapLenth;
						pTBarCode11->endY = pTBarCode11->startY + pTBarCode11->nBitMapHeight;
						//if (pTBarCode11->GetCodeType() == 8)//二维码
						//	pTBarCode11->nBitMapHeight = mBarCodeProperties.m_BarHeight / Zoom*ScaleY;
						//else if (pTBarCode11->GetCodeType() == 9)//二维码
						//	pTBarCode11->nBitMapHeight = mBarCodeProperties.m_BarHeight / Zoom*ScaleY;
						//else if (pTBarCode11->GetCodeType() == 10)//二维码
						//	pTBarCode11->nBitMapHeight = mBarCodeProperties.m_BarHeight / Zoom*ScaleY;

						//pTBarCode11->nPenWidth = mBarCodeProperties.m_BarPenSize;

						pTBarCode11->SetCodeType(mBarCodeProperties.m_nBarTypeIndex + 1);

						/////计数器
						pTBarCode11->nCounterStart = mBarCodeProperties.m_nStartNum;
						pTBarCode11->nCounterEnd = mBarCodeProperties.m_nEndNum;
						pTBarCode11->nCounterStep = mBarCodeProperties.m_nStep;

						///数据库
						pTBarCode11->m_nSqlTypeFieldIndex = mBarCodeProperties.m_nSqlTypeFieldIndex;

						pTBarCode11->m_nFrameType = mBarCodeProperties.m_nFrameTypeIndex;

						pTBarCode11->m_QietZoonUnit = mBarCodeProperties.m_QietZoonUnit;
						pTBarCode11->m_nTop = mBarCodeProperties.m_nTop;  //上边框宽度值（pixel）
						pTBarCode11->m_nBottom = mBarCodeProperties.m_nBottom; ////下边框宽度值（pixel）
						pTBarCode11->m_nLeft = mBarCodeProperties.m_nLeft;  //上边框宽度值（pixel）
						pTBarCode11->m_nRight = mBarCodeProperties.m_nRight; ////下边框宽度值（pixel）

						pTBarCode11->m_nRotateTyp = mBarCodeProperties.m_nRotateTypeIndex;
						pTBarCode11->nTextAlineType = mBarCodeProperties.m_nAlineTypeIndex;

						pTBarCode11->isDefaultTextBarDis = mBarCodeProperties.isDefaultTextBarDis;
						pTBarCode11->nTextBarDis = mBarCodeProperties.m_nTextAndBarDis;
						if (pTBarCode11->isDefaultTextBarDis)
						{
							pTBarCode11->nTextBarDis = 0;
						}
						pTBarCode11->nTextGroupDis = mBarCodeProperties.m_nGroupDis;

						pTBarCode11->TextFont = mBarCodeProperties.TextFont;
						pTBarCode11->TextColor = mBarCodeProperties.TextColor;

						pTBarCode11->sFieldTypeString = mBarCodeProperties.m_sArbGroup;
						pTBarCode11->sFieldTypeDis = mBarCodeProperties.m_FixGroup;//分组间距

						pTBarCode11->nTextGroupType = mBarCodeProperties.nTextSpreadType;

						pTBarCode11->nStartPos = mBarCodeProperties.m_startPos;
						pTBarCode11->nCounts = mBarCodeProperties.m_counts;

						if (pTBarCode11->TextFont.lfHeight < 0)
						{
							//pTBarCode11->TextFont.lfHeight = -(int)(pTBarCode11->TextFont.lfHeight * iLOGPIXELSY / 96);
						}
						mBarCodeProperties.TextFont.lfHeight;
						mBarCodeProperties.TextFont.lfWidth;
						mBarCodeProperties.TextFont.lfWeight;

						pTBarCode11->nQRLevel = mBarCodeProperties.nQRLevel;
						pTBarCode11->nQRVersion = mBarCodeProperties.nQRVersion;
						pTBarCode11->bQRAutoExtent = mBarCodeProperties.bQRAutoExtent;
						pTBarCode11->nQRMaskingNo = mBarCodeProperties.nQRMaskingNo;
						pTBarCode11->m_nTimeFormatIndex = mBarCodeProperties.nTimeFormatIndex;

						pTBarCode11->bReadable = mBarCodeProperties.bReadable;
						pTBarCode11->bAbove = mBarCodeProperties.bAbove; 
						pTBarCode11->m_bMirror = mBarCodeProperties.m_bMirror;

						if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->m_nFieldIndex >= 0 && m_bIsEnablePrview)
						{
							CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
							int nPageNum;
							CString strPageNum;
							int nRecordNums = pMainFrame->pPrintDlg->m_RecordNumList.GetItemCount();
							pMainFrame->m_barText.GetWindowTextA(strPageNum);
							if (!strPageNum.IsEmpty())
							{
								nPageNum = _ttoi(strPageNum);
								pTBarCode11->m_strFieldDes = ProcessInsertData(pTBarCode11->sInsetParam, pTBarCode11->m_strInitVal, true);
								pTBarCode11->m_strRecordVal = pTBarCode11->m_strFieldDes;
								for (int i = 0; i < nMaxColNum; i++)
								{
									CString fieldNum;
									fieldNum.Format(_T("%d"), i + 1);
									CString tmp = "[字段" + fieldNum + "]";
									CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
									CString sCodeString = pTBarCode11->m_strRecordVal;
									sCodeString.Replace(tmp, sPrint);
									pTBarCode11->SetData(sCodeString);
									pTBarCode11->m_strRecordVal = sCodeString;
								}
							}

						}
						if (pTBarCode11->m_nBarPropertyType == 1)
						{
							CString strCountTmp = GetCounterText(pTBarCode11->nCounterStart, pTBarCode11->nCounterEnd, pTBarCode11->nCounterStep, 1);
							pTBarCode11->SetData(strCountTmp);
							//if (strCountTmp.GetLength() > 0)
							//{
							//	pTBarCode11->nCounterStart = _ttol(_T(strCountTmp));
							//}
						}
						else if (pTBarCode11->m_nBarPropertyType == 2)
						{
							CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
							CString strPageNum;
							int nPageNum;
							int nRecordNums = pMainFrame->pPrintDlg->m_RecordNumList.GetItemCount();
							pMainFrame->m_barText.GetWindowTextA(strPageNum);
							if (strPageNum.IsEmpty())
							{
								continue;
							}
							nPageNum = _ttoi(strPageNum);
							if (!m_bIsEnablePrview && (nPageNum <= 0 || nPageNum > nRecordNums))
							{
								continue;
							}
							CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pTBarCode11->m_nSqlTypeFieldIndex);
							if (pTBarCode11->nStartPos >= 0 && pTBarCode11->nStartPos < sPrint.GetLength() && pTBarCode11->nCounts>0 && pTBarCode11->nCounts <= sPrint.GetLength())
							{
								sPrint = sPrint.Mid(pTBarCode11->nStartPos, pTBarCode11->nCounts);
							}
							pTBarCode11->SetData(sPrint);
						}
						else if (pTBarCode11->m_nBarPropertyType == 3)
						{
							CString strTime = GetNowTime(pTBarCode11->m_nTimeFormatIndex);
							strTime.Replace(':', '-');
							pTBarCode11->SetData(strTime);
						}
						pTBarCode11->bIsSelected = false;

						pTBarCode11->m_nFieldIndex = mBarCodeProperties.nFieldIndex;

						if (pTBarCode11->GetCodeType() == 1)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_3OF9);  //eBC_Code128       // symbology = Code 128 (bar code type)//eBC_9OF3
						}
						else if (pTBarCode11->GetCodeType() == 2)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_9OF3A);
						}
						else if (pTBarCode11->GetCodeType() == 3)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_Code128A);
						}
						else if (pTBarCode11->GetCodeType() == 4)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_Code128B);
						}
						else if (pTBarCode11->GetCodeType() == 5)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_Code128C);
						}
						else if (pTBarCode11->GetCodeType() == 6)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_2OF5);
						}
						else if (pTBarCode11->GetCodeType() == 7)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_EAN13);
						}
						else if (pTBarCode11->GetCodeType() == 8)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_QRCode);
						}
						else if (pTBarCode11->GetCodeType() == 9)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_DataMatrix);
						}
						else if (pTBarCode11->GetCodeType() == 10)
						{
							BCSetBCType(pTBarCode11->GetBarCode(), eBC_PDF417);
						}

						BCSetRotation(pTBarCode11->GetBarCode(), (e_Degree)mBarCodeProperties.m_nRotateTypeIndex);

						pTBarCode11->bBarWidthReduction = mBarCodeProperties.isUseDeburring;
						pTBarCode11->nBarWidthReduction = mBarCodeProperties.nBarWidthReduction;

						Invalidate(FALSE);
					}
				}

			}
		}
	}
}

void CMyDrawView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	CClientDC dc(this);
	OnPrepareDC(&dc);
	dc.DPtoLP(&point);

	OnViewAtt();
	// TODO: Add your message handler code here and/or call default

	CScrollView::OnLButtonDblClk(nFlags, point);
}

CString CMyDrawView::GetNowTime(int nIndex)
{
	CString strTime;
	// 获取系统当前时间，并保存到curTime   
	CTime curTime = CTime::GetCurrentTime();

	// 格式化curTime，将字符串保存到strTime   
	if (nIndex == 1)
	{
		strTime = curTime.Format(_T("%#H:%#M:%#S"));
	}
	else if (nIndex == 2)
	{
		strTime = curTime.Format(_T("%H:%M:%S"));
	}
	else if (nIndex == 3)
	{
		strTime = curTime.Format(_T("%y-%#m-%#d"));
	}
	else if (nIndex == 4)
	{
		strTime = curTime.Format(_T("%y-%m-%d"));
	}
	else if (nIndex == 5)
	{
		strTime = curTime.Format(_T("%Y-%#m-%#d"));
	}
	else if (nIndex == 6)
	{
		strTime = curTime.Format(_T("%Y-%m-%d"));
	}
	else if (nIndex == 7)
	{
		strTime = curTime.Format(_T("%d-%m-%y"));
	}
	else if (nIndex == 8)
	{
		strTime = curTime.Format(_T("%m-%d-%y"));
	}
	else if (nIndex == 9)
	{
		strTime = curTime.Format(_T("%m-%d-%Y"));
	}
	else if (nIndex == 10)
	{
		strTime = curTime.Format(_T("%Y.%m.%d"));
	}
	else if (nIndex == 11)
	{
		strTime = curTime.Format(_T("%d.%m.%Y"));
	}
	else if (nIndex == 12)
	{
		strTime = curTime.Format(_T("%m.%d.%Y"));
	}
	else if (nIndex == 13)
	{
		strTime = curTime.Format(_T("%Y%m%d%H%M%S"));
	}
	else if (nIndex == 14)
	{
		strTime = curTime.Format(_T("%Y-%m-%d %H:%M:%S"));
	}
	else
	{
		strTime = curTime.Format(_T("%Y-%m-%d %H:%M:%S"));
	}

	return strTime;
}

CString CMyDrawView::GetCounterText(long lStartNum, long lEndNum, long lStep, int pageNum)
{
	CString strCounterText = _T("");
	CString strLen;
	CString strEndNum;
	if (lStartNum > lEndNum)
	{
		return strCounterText;
	}
	strEndNum.Format(_T("%d"), lEndNum);
	if (strEndNum.GetLength() > 0)
	{
		strLen.Format(_T("%d"), strEndNum.GetLength());
		if ((lStartNum + lStep*(pageNum-1)) > lEndNum)
		{
			return strCounterText;
		}
		else
		{
			strCounterText.Format("%." + strLen + "d", lStartNum + lStep*(pageNum-1));
		}
	}
	return strCounterText;
}


void CMyDrawView::OnPrint()
{
	// TODO: Add your command handler code here
	CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	if (pMainFrame->pPrintDlg)
	{
		pMainFrame->pPrintDlg->ShowWindow(SW_SHOW);
		pMainFrame->pPrintDlg->SetFocus();
	}
}


void CMyDrawView::OnButtonLine()
{
	// TODO: Add your command handler code here

	m_createList.RemoveAll();
	CLine* pLine = new CLine;
	pLine->tracker.m_nStyle = CRectTracker::resizeInside |
		CRectTracker::dottedLine;
	pLine->tracker.m_rect.SetRect(0, 0, 0, 0);
	pLine->tracker.m_nHandleSize = 8;
	//Invalidate();

	Sing->pGraph = pLine;
	com = new NewCommand(this);
	invoker->SetCommand(com);
	//invoker->Execute();
	m_enumCurTool = ToolSelect;
	m_createList.AddTail(pLine);

	HCURSOR cursor = LoadCursor(NULL, IDC_CROSS);
	::SetCursor(cursor);//将光标设置成十字
	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_CROSS));//使光标在该区域不再闪烁
}

void CMyDrawView::OnButtonEllipse()
{
	// TODO: Add your command handler code here
	m_createList.RemoveAll();
	CEllipse* pEllipse = new CEllipse;

	pEllipse->tracker.m_nStyle = CRectTracker::resizeInside |
		CRectTracker::dottedLine;
	pEllipse->tracker.m_rect.SetRect(0, 0, 0, 0);
	pEllipse->tracker.m_nHandleSize = 8;
	//Invalidate();

	Sing->pGraph = pEllipse;
	com = new NewCommand(this);
	invoker->SetCommand(com);
	//invoker->Execute();
	m_enumCurTool = ToolSelect;
	m_createList.AddTail(pEllipse);

	HCURSOR cursor = LoadCursor(NULL, IDC_CROSS);
	::SetCursor(cursor);//将光标设置成十字
	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_CROSS));//使光标在该区域不再闪烁
}

void CMyDrawView::OnButtonRectangle()
{
	// TODO: Add your command handler code here
	m_createList.RemoveAll();
	CRectangle* pRectangle = new CRectangle;
	pRectangle->tracker.m_nStyle = CRectTracker::resizeInside |
		CRectTracker::dottedLine;
	pRectangle->tracker.m_rect.SetRect(0, 0, 0, 0);
	pRectangle->tracker.m_nHandleSize = 8;
	//Invalidate();

	Sing->pGraph = pRectangle;
	com = new NewCommand(this);
	invoker->SetCommand(com);
	//invoker->Execute();
	m_enumCurTool = ToolSelect;
	m_createList.AddTail(pRectangle);

	HCURSOR cursor = LoadCursor(NULL, IDC_CROSS);
	::SetCursor(cursor);//将光标设置成十字
	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_CROSS));//使光标在该区域不再闪烁
}
void CMyDrawView::OnButtonRectangle2()
{
	// TODO: Add your command handler code here
	m_createList.RemoveAll();
	CRectangle* pRectangle = new CRectangle;
	pRectangle->tracker.m_nStyle = CRectTracker::resizeInside |
		CRectTracker::dottedLine;
	pRectangle->tracker.m_rect.SetRect(0, 0, 0, 0);
	pRectangle->tracker.m_nHandleSize = 8;
	pRectangle->bRound = true; //圆角
	//Invalidate();

	Sing->pGraph = pRectangle;
	com = new NewCommand(this);
	invoker->SetCommand(com);
	//invoker->Execute();
	m_enumCurTool = ToolSelect;
	m_createList.AddTail(pRectangle);

	HCURSOR cursor = LoadCursor(NULL, IDC_CROSS);
	::SetCursor(cursor);//将光标设置成十字
	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_CROSS));//使光标在该区域不再闪烁
}
int nDirection = 0;
void CMyDrawView::OnButtonArc()
{
	// TODO: Add your command handler code here
	m_createList.RemoveAll();
	CArc* pArc = new CArc;
	srand(::GetTickCount());
	if (nDirection++ % 2 + 1 > 1)//偶数为0奇数为1
		pArc->Direction = 0;
	else
		pArc->Direction = 1;
	pArc->tracker.m_nStyle = CRectTracker::resizeInside |
		CRectTracker::dottedLine;
	pArc->tracker.m_rect.SetRect(0, 0, 0, 0);
	pArc->tracker.m_nHandleSize = 8;
	//Invalidate();

	Sing->pGraph = pArc;
	com = new NewCommand(this);
	invoker->SetCommand(com);
	//invoker->Execute();
	m_enumCurTool = ToolSelect;
	m_createList.AddTail(pArc);

	HCURSOR cursor = LoadCursor(NULL, IDC_CROSS);
	::SetCursor(cursor);//将光标设置成十字
	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_CROSS));//使光标在该区域不再闪烁

}

void CMyDrawView::OnButtonText()
{
	// TODO: Add your command handler code here
	m_createList.RemoveAll();
	CText* pText = new CText;
	pText->m_nFieldIndex = GetPrintTypeNum(DTEXT) + 1;
	pText->tracker.m_nStyle = CRectTracker::resizeInside |
		CRectTracker::dottedLine;
	pText->tracker.m_rect.SetRect(0, 0, 0, 0);
	pText->tracker.m_nHandleSize = 8;
	pText->nStartPos = 0;
	pText->nCounts = 0;
	pText->m_lTxtCounterStartNum = 1;
	pText->m_lTxtCounterEndNum = 10000;
	pText->m_lTxtCounterStep = 1;
	//Invalidate();

	Sing->pGraph = pText;
	com = new NewCommand(this);
	invoker->SetCommand(com);
	//invoker->Execute();
	m_enumCurTool = ToolSelect;
	m_createList.AddTail(pText);

	HCURSOR cursor = LoadCursor(NULL, IDC_CROSS);
	::SetCursor(cursor);//将光标设置成十字
	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_CROSS));//使光标在该区域不再闪烁
}

void CMyDrawView::OnButtonBar()
{
	m_createList.RemoveAll();
	CBarCode* pBarCode = new CBarCode;
	pBarCode->m_strInitVal = "123456789";
	pBarCode->setCodeString("123456789");
	pBarCode->SetCodeType(1);


	//CTBarCode11* pBarCode = new CTBarCode11;
	//pBarCode->m_strInitVal = "123456789";

	pBarCode->tracker.m_nStyle = CRectTracker::resizeInside |
		CRectTracker::dottedLine;
	pBarCode->tracker.m_rect.SetRect(0, 0, 0, 0);
	pBarCode->tracker.m_nHandleSize = 8;
	pBarCode->nCounterStart = 1;
	pBarCode->nStartPos = 0;
	pBarCode->nCounts = 0;
	//Invalidate();

	Sing->pGraph = pBarCode;
	com = new NewCommand(this);
	invoker->SetCommand(com);
	//invoker->Execute();
	m_enumCurTool = ToolSelect;
	m_createList.AddTail(pBarCode);

	HCURSOR cursor = LoadCursor(NULL, IDC_CROSS);
	::SetCursor(cursor);//将光标设置成十字
	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_CROSS));//使光标在该区域不再闪烁
}



void CMyDrawView::OnButtonTBar()
{
	m_createList.RemoveAll();
	//CBarCode* pBarCode = new CBarCode;
	//pBarCode->m_strInitVal = "123456789";
	//pBarCode->setCodeString("123456789");
	//pBarCode->SetCodeType(1);


	CTBarCode11* pBarCode = new CTBarCode11;
	pBarCode->m_strInitVal = "123456789";

	pBarCode->tracker.m_nStyle = CRectTracker::resizeInside |
		CRectTracker::dottedLine;
	pBarCode->tracker.m_rect.SetRect(0, 0, 0, 0);
	pBarCode->tracker.m_nHandleSize = 8;
	pBarCode->nCounterStart = 1;
	pBarCode->nStartPos = 0;
	pBarCode->nCounts = 0;
	//Invalidate();

	Sing->pGraph = pBarCode;
	com = new NewCommand(this);
	invoker->SetCommand(com);
	//invoker->Execute();
	m_enumCurTool = ToolSelect;
	m_createList.AddTail(pBarCode);

	HCURSOR cursor = LoadCursor(NULL, IDC_CROSS);
	::SetCursor(cursor);//将光标设置成十字
	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_CROSS));//使光标在该区域不再闪烁
}
void  CMyDrawView::OnButtonPic()
{
	m_createList.RemoveAll();
	CPic* pPic = new CPic;
	pPic->tracker.m_nStyle = CRectTracker::resizeInside |
		CRectTracker::dottedLine;
	pPic->tracker.m_rect.SetRect(0, 0, 0, 0);
	pPic->tracker.m_nHandleSize = 8;
	//Invalidate();

	Sing->pGraph = pPic;
	com = new NewCommand(this);
	invoker->SetCommand(com);
	//invoker->Execute();
	m_enumCurTool = ToolSelect;
	m_createList.AddTail(pPic);

	HCURSOR cursor = LoadCursor(NULL, IDC_CROSS);
	::SetCursor(cursor);//将光标设置成十字
	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_CROSS));//使光标在该区域不再闪烁
}

void  CMyDrawView::OnButtonUP()
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;

	CPic* pPic = NULL;
	POSITION pos = m_ObjectList.GetHeadPosition();
	int sy = 0;
	int nWidth = 0;
	int nHeight = 0;
	CObject* pHitItem;
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{

			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pEllipse->startY;
			}
			if (sy != 0 && sy >= pEllipse->startY)
			{
				sy = pEllipse->startY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pPic->startY;
			}
			if (sy != 0 && sy >= pPic->startY)
			{
				sy = pPic->startY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pLine->startY;
			}
			if (sy != 0 && sy >= pLine->startY)
			{
				sy = pLine->startY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pRectangle->startY;
			}
			if (sy != 0 && sy >= pRectangle->startY)
			{
				sy = pRectangle->startY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pArc->startY;
			}
			if (sy != 0 && sy >= pArc->startY)
			{
				sy = pArc->startY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pText->startY;
			}
			if (sy != 0 && sy >= pText->startY)
			{
				sy = pText->startY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pBarCode->startY;
			}
			if (sy != 0 && sy >= pBarCode->startY)
			{
				sy = pBarCode->startY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pTBarCode11->startY;
			}
			if (sy != 0 && sy >= pTBarCode11->startY)
			{
				sy = pTBarCode11->startY;
			}
		}
	}
	pos = m_ObjectList.GetHeadPosition();
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			nHeight = pEllipse->endY - pEllipse->startY;
			pEllipse->tracker.m_rect.SetRect(CoordZoomOut(pEllipse->startX, 0), CoordZoomOut(sy, 1),
				CoordZoomOut(pEllipse->endX, 0), CoordZoomOut(sy + nHeight, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			CRect rc;
			rc.left = CoordZoomOut(pPic->startX, 0);
			rc.top = CoordZoomOut(sy, 1);
			rc.right = rc.left + pPic->nWidth*ZoomInX;
			rc.bottom = rc.top + pPic->nHeight*ZoomInY;
			pPic->tracker.m_rect.SetRect(rc.left, rc.top,
				rc.right, rc.bottom);
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			nHeight = pLine->endY - pLine->startY;
			pLine->tracker.m_rect.SetRect(CoordZoomOut(pLine->startX, 0), CoordZoomOut(sy, 1),
				CoordZoomOut(pLine->endX, 0), CoordZoomOut(sy + nHeight, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			nHeight = pRectangle->endY - pRectangle->startY;
			pRectangle->tracker.m_rect.SetRect(CoordZoomOut(pRectangle->startX, 0), CoordZoomOut(sy, 1),
				CoordZoomOut(pRectangle->endX, 0), CoordZoomOut(sy + nHeight, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			nHeight = pArc->endY - pArc->startY;
			pArc->tracker.m_rect.SetRect(CoordZoomOut(pArc->startX, 0), CoordZoomOut(sy, 1),
				CoordZoomOut(pArc->endX, 0), CoordZoomOut(sy + nHeight, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			nHeight = pText->endY - pText->startY;
			pText->tracker.m_rect.SetRect(CoordZoomOut(pText->startX, 0), CoordZoomOut(sy, 1),
				CoordZoomOut(pText->endX, 0), CoordZoomOut(sy + nHeight, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			nHeight = pBarCode->endY - pBarCode->startY;
			pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(sy, 1),
				CoordZoomOut(pBarCode->endX, 0), CoordZoomOut(sy + nHeight, 1));
		}

		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			nHeight = pTBarCode11->endY - pTBarCode11->startY;
			pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(pTBarCode11->startX, 0), CoordZoomOut(sy, 1),
				CoordZoomOut(pTBarCode11->endX, 0), CoordZoomOut(sy + nHeight, 1));
		}

		CGraph* pGraph = (CGraph*)pHitItem;
		if (pGraph != NULL)
		{
			if (pGraph->bIsSelected && !pGraph->bIsLocked && !isGetData)
			{
				if (!pGraph->_OriRect.EqualRect(pGraph->tracker.m_rect))
				{
					Sing->pGraph = pGraph;
					com = new MoveCommand(this);
					invoker->SetCommand(com);
					invoker->Execute();
					//pGraph->SetRect(pGraph->tracker.m_rect);
				}
			}
		}
	}
	Invalidate(FALSE);
}
void  CMyDrawView::OnButtonLEFT()
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;

	CPic* pPic = NULL;
	POSITION pos = m_ObjectList.GetHeadPosition();
	int sy = 0;
	int nWidth = 0;
	int nHeight = 0;
	CObject* pHitItem;
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{

			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pEllipse->startX;
			}
			if (sy != 0 && sy >= pEllipse->startX)
			{
				sy = pEllipse->startX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pPic->startX;
			}
			if (sy != 0 && sy >= pPic->startX)
			{
				sy = pPic->startX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pLine->startX;
			}
			if (sy != 0 && sy >= pLine->startX)
			{
				sy = pLine->startX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pRectangle->startX;
			}
			if (sy != 0 && sy >= pRectangle->startX)
			{
				sy = pRectangle->startX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pArc->startX;
			}
			if (sy != 0 && sy >= pArc->startX)
			{
				sy = pArc->startX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pText->startX;
			}
			if (sy != 0 && sy >= pText->startX)
			{
				sy = pText->startX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pBarCode->startX;
			}
			if (sy != 0 && sy >= pBarCode->startX)
			{
				sy = pBarCode->startX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pTBarCode11->startX;
			}
			if (sy != 0 && sy >= pTBarCode11->startX)
			{
				sy = pTBarCode11->startX;
			}
		}
	}
	pos = m_ObjectList.GetHeadPosition();
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			nHeight = pEllipse->endX - pEllipse->startX;
			pEllipse->tracker.m_rect.SetRect(CoordZoomOut(sy, 0), CoordZoomOut(pEllipse->startY, 1),
				CoordZoomOut(sy + nHeight, 0), CoordZoomOut(pEllipse->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			CRect rc;
			rc.left = CoordZoomOut(sy, 0);
			rc.top = CoordZoomOut(pPic->startY, 1);
			rc.right = rc.left + pPic->nWidth*ZoomInX;
			rc.bottom = rc.top + pPic->nHeight*ZoomInY;
			pPic->tracker.m_rect.SetRect(rc.left, rc.top,
				rc.right, rc.bottom);
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			nHeight = pLine->endX - pLine->startX;
			pLine->tracker.m_rect.SetRect(CoordZoomOut(sy, 0), CoordZoomOut(pLine->startY, 1),
				CoordZoomOut(sy + nHeight, 0), CoordZoomOut(pLine->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			nHeight = pRectangle->endX - pRectangle->startX;
			pRectangle->tracker.m_rect.SetRect(CoordZoomOut(sy, 0), CoordZoomOut(pRectangle->startY, 1),
				CoordZoomOut(sy + nHeight, 0), CoordZoomOut(pRectangle->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			nHeight = pArc->endX - pArc->startX;
			pArc->tracker.m_rect.SetRect(CoordZoomOut(sy, 0), CoordZoomOut(pArc->startY, 1),
				CoordZoomOut(sy + nHeight, 0), CoordZoomOut(pArc->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			nHeight = pText->endX - pText->startX;
			pText->tracker.m_rect.SetRect(CoordZoomOut(sy, 0), CoordZoomOut(pText->startY, 1),
				CoordZoomOut(sy + nHeight, 0), CoordZoomOut(pText->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			nHeight = pBarCode->endX - pBarCode->startX;
			pBarCode->tracker.m_rect.SetRect(CoordZoomOut(sy, 0), CoordZoomOut(pBarCode->startY, 1),
				CoordZoomOut(sy + nHeight, 0), CoordZoomOut(pBarCode->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			nHeight = pTBarCode11->endX - pTBarCode11->startX;
			pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(sy, 0), CoordZoomOut(pTBarCode11->startY, 1),
				CoordZoomOut(sy + nHeight, 0), CoordZoomOut(pTBarCode11->endY, 1));
		}


		CGraph* pGraph = (CGraph*)pHitItem;
		if (pGraph != NULL)
		{
			if (pGraph->bIsSelected && !pGraph->bIsLocked && !isGetData)
			{
				if (!pGraph->_OriRect.EqualRect(pGraph->tracker.m_rect))
				{
					Sing->pGraph = pGraph;
					com = new MoveCommand(this);
					invoker->SetCommand(com);
					invoker->Execute();
					//pGraph->SetRect(pGraph->tracker.m_rect);
				}
			}
		}

	}
	Invalidate(FALSE);
}
void  CMyDrawView::OnButtonDOWN()
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;

	CPic* pPic = NULL;
	POSITION pos = m_ObjectList.GetHeadPosition();
	int sy = 0;
	int nWidth = 0;
	int nHeight = 0;
	CObject* pHitItem;
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{

			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pEllipse->endY;
			}
			if (sy != 0 && sy <= pEllipse->endY)
			{
				sy = pEllipse->endY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pPic->startY + pPic->nHeight;
			}
			if (sy != 0 && sy <= (pPic->startY + pPic->nHeight))
			{
				sy = pPic->startY + pPic->nHeight;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pLine->endY;
			}
			if (sy != 0 && sy <= pLine->endY)
			{
				sy = pLine->endY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pRectangle->endY;
			}
			if (sy != 0 && sy <= pRectangle->endY)
			{
				sy = pRectangle->endY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pArc->endY;
			}
			if (sy != 0 && sy <= pArc->endY)
			{
				sy = pArc->endY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pText->endY;
			}
			if (sy != 0 && sy <= pText->endY)
			{
				sy = pText->endY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pBarCode->endY;
			}
			if (sy != 0 && sy <= pBarCode->endY)
			{
				sy = pBarCode->endY;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pTBarCode11->endY;
			}
			if (sy != 0 && sy <= pTBarCode11->endY)
			{
				sy = pTBarCode11->endY;
			}
		}
	}
	pos = m_ObjectList.GetHeadPosition();
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			nHeight = pEllipse->endY - pEllipse->startY;
			pEllipse->tracker.m_rect.SetRect(CoordZoomOut(pEllipse->startX, 0), CoordZoomOut(sy - nHeight, 1),
				CoordZoomOut(pEllipse->endX, 0), CoordZoomOut(sy, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			CRect rc;
			rc.left = CoordZoomOut(pPic->startX, 0);
			rc.top = CoordZoomOut(sy - pPic->nHeight, 1);
			rc.right = rc.left + pPic->nWidth*ZoomInX;
			rc.bottom = rc.top + pPic->nHeight*ZoomInY;
			pPic->tracker.m_rect.SetRect(rc.left, rc.top,
				rc.right, rc.bottom);
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			nHeight = pLine->endY - pLine->startY;
			pLine->tracker.m_rect.SetRect(CoordZoomOut(pLine->startX, 0), CoordZoomOut(sy - nHeight, 1),
				CoordZoomOut(pLine->endX, 0), CoordZoomOut(sy, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			nHeight = pRectangle->endY - pRectangle->startY;
			pRectangle->tracker.m_rect.SetRect(CoordZoomOut(pRectangle->startX, 0), CoordZoomOut(sy - nHeight, 1),
				CoordZoomOut(pRectangle->endX, 0), CoordZoomOut(sy, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			nHeight = pArc->endY - pArc->startY;
			pArc->tracker.m_rect.SetRect(CoordZoomOut(pArc->startX, 0), CoordZoomOut(sy - nHeight, 1),
				CoordZoomOut(pArc->endX, 0), CoordZoomOut(sy, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			nHeight = pText->endY - pText->startY;
			pText->tracker.m_rect.SetRect(CoordZoomOut(pText->startX, 0), CoordZoomOut(sy - nHeight, 1),
				CoordZoomOut(pText->endX, 0), CoordZoomOut(sy, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			nHeight = pBarCode->endY - pBarCode->startY;
			pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(sy - nHeight, 1),
				CoordZoomOut(pBarCode->endX, 0), CoordZoomOut(sy, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			nHeight = pTBarCode11->endY - pTBarCode11->startY;
			pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(pTBarCode11->startX, 0), CoordZoomOut(sy - nHeight, 1),
				CoordZoomOut(pTBarCode11->endX, 0), CoordZoomOut(sy, 1));
		}


		CGraph* pGraph = (CGraph*)pHitItem;
		if (pGraph != NULL)
		{
			if (pGraph->bIsSelected && !pGraph->bIsLocked && !isGetData)
			{
				if (!pGraph->_OriRect.EqualRect(pGraph->tracker.m_rect))
				{
					Sing->pGraph = pGraph;
					com = new MoveCommand(this);
					invoker->SetCommand(com);
					invoker->Execute();
					//pGraph->SetRect(pGraph->tracker.m_rect);
				}
			}
		}
	}
	Invalidate(FALSE);
}
void  CMyDrawView::OnButtonRIGHT()
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;

	CPic* pPic = NULL;
	POSITION pos = m_ObjectList.GetHeadPosition();
	int sy = 0;
	int nWidth = 0;
	int nHeight = 0;
	CObject* pHitItem;
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{

			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pEllipse->endX;
			}
			if (sy != 0 && sy <= pEllipse->endX)
			{
				sy = pEllipse->endX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pPic->startX + pPic->nWidth;
			}
			if (sy != 0 && sy <= (pPic->startX + pPic->nWidth))
			{
				sy = pPic->startX + pPic->nWidth;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pLine->endX;
			}
			if (sy != 0 && sy <= pLine->endX)
			{
				sy = pLine->endX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pRectangle->endX;
			}
			if (sy != 0 && sy <= pRectangle->endX)
			{
				sy = pRectangle->endX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pArc->endX;
			}
			if (sy != 0 && sy <= pArc->endX)
			{
				sy = pArc->endX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pText->endX;
			}
			if (sy != 0 && sy <= pText->endX)
			{
				sy = pText->endX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pBarCode->endX;
			}
			if (sy != 0 && sy <= pBarCode->endX)
			{
				sy = pBarCode->endX;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = pTBarCode11->endX;
			}
			if (sy != 0 && sy <= pTBarCode11->endX)
			{
				sy = pTBarCode11->endX;
			}
		}
	}
	pos = m_ObjectList.GetHeadPosition();
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			nHeight = pEllipse->endX - pEllipse->startX;
			pEllipse->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight, 0), CoordZoomOut(pEllipse->startY, 1),
				CoordZoomOut(sy, 0), CoordZoomOut(pEllipse->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			CRect rc;
			rc.left = CoordZoomOut(sy - pPic->nWidth, 0);
			rc.top = CoordZoomOut(pPic->startY, 1);
			rc.right = rc.left + pPic->nWidth*ZoomInX;
			rc.bottom = rc.top + pPic->nHeight*ZoomInY;
			pPic->tracker.m_rect.SetRect(rc.left, rc.top,
				rc.right, rc.bottom);
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			nHeight = pLine->endX - pLine->startX;
			pLine->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight, 0), CoordZoomOut(pLine->startY, 1),
				CoordZoomOut(sy, 0), CoordZoomOut(pLine->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			nHeight = pRectangle->endX - pRectangle->startX;
			pRectangle->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight, 0), CoordZoomOut(pRectangle->startY, 1),
				CoordZoomOut(sy, 0), CoordZoomOut(pRectangle->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			nHeight = pArc->endX - pArc->startX;
			pArc->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight, 0), CoordZoomOut(pArc->startY, 1),
				CoordZoomOut(sy, 0), CoordZoomOut(pArc->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			nHeight = pText->endX - pText->startX;
			pText->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight, 0), CoordZoomOut(pText->startY, 1),
				CoordZoomOut(sy, 0), CoordZoomOut(pText->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			nHeight = pBarCode->endX - pBarCode->startX;
			pBarCode->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight, 0), CoordZoomOut(pBarCode->startY, 1),
				CoordZoomOut(sy, 0), CoordZoomOut(pBarCode->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			nHeight = pTBarCode11->endX - pTBarCode11->startX;
			pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight, 0), CoordZoomOut(pTBarCode11->startY, 1),
				CoordZoomOut(sy, 0), CoordZoomOut(pTBarCode11->endY, 1));
		}


		CGraph* pGraph = (CGraph*)pHitItem;
		if (pGraph != NULL)
		{
			if (pGraph->bIsSelected && !pGraph->bIsLocked && !isGetData)
			{
				if (!pGraph->_OriRect.EqualRect(pGraph->tracker.m_rect))
				{
					Sing->pGraph = pGraph;
					com = new MoveCommand(this);
					invoker->SetCommand(com);
					invoker->Execute();
					//pGraph->SetRect(pGraph->tracker.m_rect);
				}
			}
		}
	}
	Invalidate(FALSE);
}

void  CMyDrawView::OnButtonHorizontal()
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;

	CPic* pPic = NULL;
	POSITION pos = m_ObjectList.GetHeadPosition();
	int sy = 0;
	int nWidth = 0;
	int nHeight = 0;
	CObject* pHitItem;
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{

			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pEllipse->startY + pEllipse->endY) / 2;
			}
			if (sy != 0 && sy <= (pEllipse->startY + pEllipse->endY) / 2)
			{
				sy = (pEllipse->startY + pEllipse->endY) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pPic->startY + pPic->startY + pPic->nHeight) / 2;
			}
			if (sy != 0 && sy <= (pPic->startY + pPic->startY + pPic->nHeight) / 2)
			{
				sy = (pPic->startY + pPic->startY + pPic->nHeight) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pLine->startY + pLine->endY) / 2;
			}
			if (sy != 0 && sy <= (pLine->startY + pLine->endY) / 2)
			{
				sy = (pLine->startY + pLine->endY) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pRectangle->startY + pRectangle->endY) / 2;
			}
			if (sy != 0 && sy <= (pRectangle->startY + pRectangle->endY) / 2)
			{
				sy = (pRectangle->startY + pRectangle->endY) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pArc->startY + pArc->endY) / 2;
			}
			if (sy != 0 && sy <= (pArc->startY + pArc->endY) / 2)
			{
				sy = (pArc->startY + pArc->endY) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pText->startY + pText->endY) / 2;
			}
			if (sy != 0 && sy <= (pText->startY + pText->endY) / 2)
			{
				sy = (pText->startY + pText->endY) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pBarCode->startY + pBarCode->endY) / 2;
			}
			if (sy != 0 && sy <= (pBarCode->startY + pBarCode->endY) / 2)
			{
				sy = (pBarCode->startY + pBarCode->endY) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pTBarCode11->startY + pTBarCode11->endY) / 2;
			}
			if (sy != 0 && sy <= (pTBarCode11->startY + pTBarCode11->endY) / 2)
			{
				sy = (pTBarCode11->startY + pTBarCode11->endY) / 2;
			}
		}
	}
	pos = m_ObjectList.GetHeadPosition();
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			nHeight = pEllipse->endY - pEllipse->startY;
			pEllipse->tracker.m_rect.SetRect(CoordZoomOut(pEllipse->startX, 0), CoordZoomOut(sy - nHeight / 2, 1),
				CoordZoomOut(pEllipse->endX, 0), CoordZoomOut(sy + nHeight / 2, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			CRect rc;
			rc.left = CoordZoomOut(pPic->startX, 0);
			rc.top = CoordZoomOut(sy - pPic->nHeight / 2, 1);
			rc.right = rc.left + pPic->nWidth*ZoomInX;
			rc.bottom = CoordZoomOut(sy + pPic->nHeight / 2, 1);
			pPic->tracker.m_rect.SetRect(rc.left, rc.top,
				rc.right, rc.bottom);
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			nHeight = pLine->endY - pLine->startY;
			pLine->tracker.m_rect.SetRect(CoordZoomOut(pLine->startX, 0), CoordZoomOut(sy - nHeight / 2, 1),
				CoordZoomOut(pLine->endX, 0), CoordZoomOut(sy + nHeight / 2, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			nHeight = pRectangle->endY - pRectangle->startY;
			pRectangle->tracker.m_rect.SetRect(CoordZoomOut(pRectangle->startX, 0), CoordZoomOut(sy - nHeight / 2, 1),
				CoordZoomOut(pRectangle->endX, 0), CoordZoomOut(sy + nHeight / 2, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			nHeight = pArc->endY - pArc->startY;
			pArc->tracker.m_rect.SetRect(CoordZoomOut(pArc->startX, 0), CoordZoomOut(sy - nHeight / 2, 1),
				CoordZoomOut(pArc->endX, 0), CoordZoomOut(sy + nHeight / 2, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			nHeight = pText->endY - pText->startY;
			pText->tracker.m_rect.SetRect(CoordZoomOut(pText->startX, 0), CoordZoomOut(sy - nHeight / 2, 1),
				CoordZoomOut(pText->endX, 0), CoordZoomOut(sy + nHeight / 2, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			nHeight = pBarCode->endY - pBarCode->startY;
			pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(sy - nHeight / 2, 1),
				CoordZoomOut(pBarCode->endX, 0), CoordZoomOut(sy + nHeight / 2, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			nHeight = pTBarCode11->endY - pTBarCode11->startY;
			pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(pTBarCode11->startX, 0), CoordZoomOut(sy - nHeight / 2, 1),
				CoordZoomOut(pTBarCode11->endX, 0), CoordZoomOut(sy + nHeight / 2, 1));
		}


		CGraph* pGraph = (CGraph*)pHitItem;
		if (pGraph != NULL)
		{
			if (pGraph->bIsSelected && !pGraph->bIsLocked && !isGetData)
			{
				if (!pGraph->_OriRect.EqualRect(pGraph->tracker.m_rect))
				{
					Sing->pGraph = pGraph;
					com = new MoveCommand(this);
					invoker->SetCommand(com);
					invoker->Execute();
					//pGraph->SetRect(pGraph->tracker.m_rect);
				}
			}
		}
	}
	Invalidate(FALSE);
}


void  CMyDrawView::OnButtonVertical()
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;

	CPic* pPic = NULL;
	POSITION pos = m_ObjectList.GetHeadPosition();
	int sy = 0;
	int nWidth = 0;
	int nHeight = 0;
	CObject* pHitItem;
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{

			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pEllipse->startX + pEllipse->endX) / 2;
			}
			if (sy != 0 && sy <= (pEllipse->startX + pEllipse->endX) / 2)
			{
				sy = (pEllipse->startX + pEllipse->endX) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pPic->startX + pPic->startX + pPic->nWidth) / 2;
			}
			if (sy != 0 && sy <= (pPic->startX + pPic->startX + pPic->nWidth) / 2)
			{
				sy = (pPic->startX + pPic->startX + pPic->nWidth) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pLine->startX + pLine->endX) / 2;
			}
			if (sy != 0 && sy <= (pLine->startX + pLine->endX) / 2)
			{
				sy = (pLine->startX + pLine->endX) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pRectangle->startX + pRectangle->endX) / 2;
			}
			if (sy != 0 && sy <= (pRectangle->startX + pRectangle->endX) / 2)
			{
				sy = (pRectangle->startX + pRectangle->endX) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pArc->startX + pArc->endX) / 2;
			}
			if (sy != 0 && sy <= (pArc->startX + pArc->endX) / 2)
			{
				sy = (pArc->startX + pArc->endX) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pText->startX + pText->endX) / 2;
			}
			if (sy != 0 && sy <= (pText->startX + pText->endX) / 2)
			{
				sy = (pText->startX + pText->endX) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pBarCode->startX + pBarCode->endX) / 2;
			}
			if (sy != 0 && sy <= (pBarCode->startX + pBarCode->endX) / 2)
			{
				sy = (pBarCode->startX + pBarCode->endX) / 2;
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			if (sy == 0)
			{
				sy = (pTBarCode11->startX + pTBarCode11->endX) / 2;
			}
			if (sy != 0 && sy <= (pTBarCode11->startX + pTBarCode11->endX) / 2)
			{
				sy = (pTBarCode11->startX + pTBarCode11->endX) / 2;
			}
		}
	}
	pos = m_ObjectList.GetHeadPosition();
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse->bIsSelected == false)
			{
				continue;
			}
			nHeight = pEllipse->endX - pEllipse->startX;
			pEllipse->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight / 2, 0), CoordZoomOut(pEllipse->startY, 1),
				CoordZoomOut(sy + nHeight / 2, 0), CoordZoomOut(pEllipse->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic->bIsSelected == false)
			{
				continue;
			}
			CRect rc;
			rc.left = CoordZoomOut(sy - pPic->nWidth / 2, 0);
			rc.top = CoordZoomOut(pPic->startY, 1);
			rc.right = CoordZoomOut(sy + pPic->nWidth / 2, 0);
			rc.bottom = rc.top + pPic->nHeight*ZoomInY;
			pPic->tracker.m_rect.SetRect(rc.left, rc.top,
				rc.right, rc.bottom);
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine->bIsSelected == false)
			{
				continue;
			}
			nHeight = pLine->endX - pLine->startX;
			pLine->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight / 2, 0), CoordZoomOut(pLine->startY, 1),
				CoordZoomOut(sy + nHeight / 2, 0), CoordZoomOut(pLine->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle->bIsSelected == false)
			{
				continue;
			}
			nHeight = pRectangle->endX - pRectangle->startX;
			pRectangle->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight / 2, 0), CoordZoomOut(pRectangle->startY, 1),
				CoordZoomOut(sy + nHeight / 2, 0), CoordZoomOut(pRectangle->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc->bIsSelected == false)
			{
				continue;
			}
			nHeight = pArc->endX - pArc->startX;
			pArc->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight / 2, 0), CoordZoomOut(pArc->startY, 1),
				CoordZoomOut(sy + nHeight / 2, 0), CoordZoomOut(pArc->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText->bIsSelected == false)
			{
				continue;
			}
			nHeight = pText->endX - pText->startX;
			pText->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight / 2, 0), CoordZoomOut(pText->startY, 1),
				CoordZoomOut(sy + nHeight / 2, 0), CoordZoomOut(pText->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode->bIsSelected == false)
			{
				continue;
			}
			nHeight = pBarCode->endX - pBarCode->startX;
			pBarCode->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight / 2, 0), CoordZoomOut(pBarCode->startY, 1),
				CoordZoomOut(sy + nHeight / 2, 0), CoordZoomOut(pBarCode->endY, 1));
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11->bIsSelected == false)
			{
				continue;
			}
			nHeight = pTBarCode11->endX - pTBarCode11->startX;
			pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(sy - nHeight / 2, 0), CoordZoomOut(pTBarCode11->startY, 1),
				CoordZoomOut(sy + nHeight / 2, 0), CoordZoomOut(pTBarCode11->endY, 1));
		}

		CGraph* pGraph = (CGraph*)pHitItem;
		if (pGraph != NULL)
		{
			if (pGraph->bIsSelected && !pGraph->bIsLocked && !isGetData)
			{
				if (!pGraph->_OriRect.EqualRect(pGraph->tracker.m_rect))
				{
					Sing->pGraph = pGraph;
					com = new MoveCommand(this);
					invoker->SetCommand(com);
					invoker->Execute();
					//pGraph->SetRect(pGraph->tracker.m_rect);
				}
			}
		}
	}
	Invalidate(FALSE);
}



void  CMyDrawView::OnButtonInterval()
{
	intervalSetDlg dlg;
	int fx = 0;//0水平  1垂直
	double m_dIntervalSet = 0;

	//间隔设置参数
	if (dlg.DoModal() == IDOK)
	{
		fx = dlg.m_combo_fx;
		m_dIntervalSet = dlg.m_dIntervalSet * ScaleX / Zoom ;
	}
	else
		return;


	if (fx == 0)
	{
		map<int, CGraph*>m_pCGraphs;
		POSITION pos = m_ObjectList.GetHeadPosition();
		int sy = 0;
		while (pos != NULL)
		{

			CGraph* pHitItem = (CGraph*)m_ObjectList.GetNext(pos);
			if (pHitItem != NULL && pHitItem->bIsSelected)
			{
				sy = (pHitItem->startX + pHitItem->endX) / 2;
				m_pCGraphs.insert(make_pair(sy, pHitItem));
			}
		}
		int nCount = 0;
		map<int, CGraph*>::iterator iter;
		for (iter = m_pCGraphs.begin(); iter != m_pCGraphs.end(); iter++)//未返回的包号，重发
		{
			CGraph*  pHitItem = iter->second;


			if (nCount == 0)
			{
				sy = pHitItem->endX;
			}
			else
			{
				int nHeight = pHitItem->endX - pHitItem->startX;
				pHitItem->tracker.m_rect.SetRect(CoordZoomOut(sy + m_dIntervalSet, 0), CoordZoomOut(pHitItem->startY, 1),
					CoordZoomOut(sy + m_dIntervalSet +nHeight, 0), CoordZoomOut(pHitItem->endY, 1));

				sy = sy + m_dIntervalSet + nHeight;
			}

			nCount++;


			CGraph* pGraph = (CGraph*)pHitItem;
			if (pGraph != NULL)
			{
				if (pGraph->bIsSelected && !pGraph->bIsLocked && !isGetData)
				{
					if (!pGraph->_OriRect.EqualRect(pGraph->tracker.m_rect))
					{
						Sing->pGraph = pGraph;
						com = new MoveCommand(this);
						invoker->SetCommand(com);
						invoker->Execute();
						//pGraph->SetRect(pGraph->tracker.m_rect);
					}
				}
			}

		}
		
	}
	else
	{
		map<int, CGraph*>m_pCGraphs;
		POSITION pos = m_ObjectList.GetHeadPosition();
		int sy = 0;
		while (pos != NULL)
		{

			CGraph* pHitItem = (CGraph*)m_ObjectList.GetNext(pos);
			if (pHitItem != NULL && pHitItem->bIsSelected)
			{
				sy = (pHitItem->startY + pHitItem->endY) / 2;
				m_pCGraphs.insert(make_pair(sy, pHitItem));
			}
		}
		int nCount = 0;
		map<int, CGraph*>::iterator iter;
		for (iter = m_pCGraphs.begin(); iter != m_pCGraphs.end(); iter++)//未返回的包号，重发
		{
			CGraph*  pHitItem = iter->second;


			if (nCount == 0)
			{
				sy = pHitItem->endY;
			}
			else
			{
				int nHeight = pHitItem->endY - pHitItem->startY;
				pHitItem->tracker.m_rect.SetRect(CoordZoomOut(pHitItem->startX, 0), CoordZoomOut(sy + m_dIntervalSet, 1),
					CoordZoomOut(pHitItem->endX, 0), CoordZoomOut(sy + m_dIntervalSet + nHeight, 1));

				sy = sy + m_dIntervalSet + nHeight;
			}

			nCount++;


			CGraph* pGraph = (CGraph*)pHitItem;
			if (pGraph != NULL)
			{
				if (pGraph->bIsSelected && !pGraph->bIsLocked && !isGetData)
				{
					if (!pGraph->_OriRect.EqualRect(pGraph->tracker.m_rect))
					{
						Sing->pGraph = pGraph;
						com = new MoveCommand(this);
						invoker->SetCommand(com);
						invoker->Execute();
						//pGraph->SetRect(pGraph->tracker.m_rect);
					}
				}
			}

		}
		
		
	}
	Invalidate(FALSE);
}

void CMyDrawView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	//按下delete键进行删除
	if (46 == nChar)
	{

		com = new DeleteCommand(this);
		invoker->SetCommand(com);
		invoker->Execute();

		Invalidate(FALSE);
	}
	CScrollView::OnKeyUp(nChar, nRepCnt, nFlags);
}

void CMyDrawView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (37 == nChar)
	{
		OnLeftMove();
	}
	if (38 == nChar)
	{
		OnUpMove();
	}
	if (39 == nChar)
	{
		OnRightMove();
	}
	if (40 == nChar)
	{
		OnDownMove();
	}
	CScrollView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CMyDrawView::OnLeftMove()
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;

	POSITION pos = m_ObjectList.GetTailPosition();
	CObject* pHitItem;
	while (pos != NULL)
	{
		POSITION curpos = pos;
		pHitItem = (CObject*)m_ObjectList.GetPrev(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL)
			{
				if (pEllipse->bIsSelected)
				{
					pEllipse->startX -= 1;
					pEllipse->endX -= 1;
					pEllipse->tracker.m_rect.SetRect(CoordZoomOut(pEllipse->startX, 0), CoordZoomOut(pEllipse->startY, 1),
						CoordZoomOut(pEllipse->endX, 0), CoordZoomOut(pEllipse->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL)
			{
				if (pLine->bIsSelected)
				{
					pLine->startX -= 1;
					pLine->endX -= 1;
					pLine->tracker.m_rect.SetRect(CoordZoomOut(pLine->startX, 0), CoordZoomOut(pLine->startY, 1),
						CoordZoomOut(pLine->endX, 0), CoordZoomOut(pLine->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL)
			{
				if (pArc->bIsSelected)
				{
					pArc->startX -= 1;
					pArc->endX -= 1;
					pArc->tracker.m_rect.SetRect(CoordZoomOut(pArc->startX, 0), CoordZoomOut(pArc->startY, 1),
						CoordZoomOut(pArc->endX, 0), CoordZoomOut(pArc->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText != NULL)
			{
				if (pText->bIsSelected)
				{
					pText->startX -= 1;
					pText->endX -= 1;
					pText->tracker.m_rect.SetRect(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), CoordZoomOut(pText->endX, 0), CoordZoomOut(pText->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL)
			{
				if (pRectangle->bIsSelected)
				{
					pRectangle->startX -= 1;
					pRectangle->endX -= 1;
					pRectangle->tracker.m_rect.SetRect(CoordZoomOut(pRectangle->startX, 0), CoordZoomOut(pRectangle->startY, 1),
						CoordZoomOut(pRectangle->endX, 0), CoordZoomOut(pRectangle->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic != NULL)
			{
				if (pPic->bIsSelected)
				{
					CRect rc;
					pPic->startX -= 1;
					rc.left = CoordZoomOut(pPic->startX, 0);
					rc.top = CoordZoomOut(pPic->startY, 1);
					rc.right = rc.left + pPic->nWidth*ZoomInX;
					rc.bottom = rc.top + pPic->nHeight*ZoomInY;
					pPic->tracker.m_rect.SetRect(rc.left, rc.top,
						rc.right, rc.bottom);

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL)
			{
				if (pBarCode->bIsSelected)
				{
					pBarCode->startX -= 1;
					pBarCode->endX -= 1;
					pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						CoordZoomOut(pBarCode->endX, 0), CoordZoomOut(pBarCode->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL)
			{
				if (pTBarCode11->bIsSelected)
				{
					pTBarCode11->startX -= 1;
					pTBarCode11->endX -= 1;
					pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(pTBarCode11->startX, 0), CoordZoomOut(pTBarCode11->startY, 1),
						CoordZoomOut(pTBarCode11->endX, 0), CoordZoomOut(pTBarCode11->endY, 1));

				}
			}
		}
	}
	Invalidate(FALSE);
}
void CMyDrawView::OnRightMove()
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;

	POSITION pos = m_ObjectList.GetTailPosition();
	CObject* pHitItem;
	while (pos != NULL)
	{
		POSITION curpos = pos;
		pHitItem = (CObject*)m_ObjectList.GetPrev(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL)
			{
				if (pEllipse->bIsSelected)
				{
					pEllipse->startX += 1;
					pEllipse->endX += 1;
					pEllipse->tracker.m_rect.SetRect(CoordZoomOut(pEllipse->startX, 0), CoordZoomOut(pEllipse->startY, 1),
						CoordZoomOut(pEllipse->endX, 0), CoordZoomOut(pEllipse->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL)
			{
				if (pLine->bIsSelected)
				{
					pLine->startX += 1;
					pLine->endX += 1;
					pLine->tracker.m_rect.SetRect(CoordZoomOut(pLine->startX, 0), CoordZoomOut(pLine->startY, 1),
						CoordZoomOut(pLine->endX, 0), CoordZoomOut(pLine->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL)
			{
				if (pArc->bIsSelected)
				{
					pArc->startX += 1;
					pArc->endX += 1;
					pArc->tracker.m_rect.SetRect(CoordZoomOut(pArc->startX, 0), CoordZoomOut(pArc->startY, 1),
						CoordZoomOut(pArc->endX, 0), CoordZoomOut(pArc->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText != NULL)
			{
				if (pText->bIsSelected)
				{
					pText->startX += 1;
					pText->endX += 1;
					pText->tracker.m_rect.SetRect(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), CoordZoomOut(pText->endX, 0), CoordZoomOut(pText->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL)
			{
				if (pRectangle->bIsSelected)
				{
					pRectangle->startX += 1;
					pRectangle->endX += 1;
					pRectangle->tracker.m_rect.SetRect(CoordZoomOut(pRectangle->startX, 0), CoordZoomOut(pRectangle->startY, 1),
						CoordZoomOut(pRectangle->endX, 0), CoordZoomOut(pRectangle->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic != NULL)
			{
				if (pPic->bIsSelected)
				{
					CRect rc;
					pPic->startX += 1;
					rc.left = CoordZoomOut(pPic->startX, 0);
					rc.top = CoordZoomOut(pPic->startY, 1);
					//rc.right=rc.left+pPic->nWidth/ZoomInX_Pic;
					//rc.bottom=rc.top+pPic->nHeight/ZoomInY_Pic;
					rc.right = rc.left + pPic->nWidth*ZoomInX;
					rc.bottom = rc.top + pPic->nHeight*ZoomInY;
					pPic->tracker.m_rect.SetRect(rc.left, rc.top,
						rc.right, rc.bottom);

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL)
			{
				if (pBarCode->bIsSelected)
				{
					pBarCode->startX += 1;
					pBarCode->endX += 1;
					pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						CoordZoomOut(pBarCode->endX, 0), CoordZoomOut(pBarCode->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL)
			{
				if (pTBarCode11->bIsSelected)
				{
					pTBarCode11->startX += 1;
					pTBarCode11->endX += 1;
					pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(pTBarCode11->startX, 0), CoordZoomOut(pTBarCode11->startY, 1),
						CoordZoomOut(pTBarCode11->endX, 0), CoordZoomOut(pTBarCode11->endY, 1));

				}
			}
		}
	}
	Invalidate(FALSE);
}
void CMyDrawView::OnUpMove()
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;

	POSITION pos = m_ObjectList.GetTailPosition();
	CObject* pHitItem;
	while (pos != NULL)
	{
		POSITION curpos = pos;
		pHitItem = (CObject*)m_ObjectList.GetPrev(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL)
			{
				if (pEllipse->bIsSelected)
				{
					pEllipse->startY -= 1;
					pEllipse->endY -= 1;
					pEllipse->tracker.m_rect.SetRect(CoordZoomOut(pEllipse->startX, 0), CoordZoomOut(pEllipse->startY, 1),
						CoordZoomOut(pEllipse->endX, 0), CoordZoomOut(pEllipse->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL)
			{
				if (pLine->bIsSelected)
				{
					pLine->startY -= 1;
					pLine->endY -= 1;
					pLine->tracker.m_rect.SetRect(CoordZoomOut(pLine->startX, 0), CoordZoomOut(pLine->startY, 1),
						CoordZoomOut(pLine->endX, 0), CoordZoomOut(pLine->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL)
			{
				if (pArc->bIsSelected)
				{
					pArc->startY -= 1;
					pArc->endY -= 1;
					pArc->tracker.m_rect.SetRect(CoordZoomOut(pArc->startX, 0), CoordZoomOut(pArc->startY, 1),
						CoordZoomOut(pArc->endX, 0), CoordZoomOut(pArc->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText != NULL)
			{
				if (pText->bIsSelected)
				{
					pText->startY -= 1;
					pText->endY -= 1;
					pText->tracker.m_rect.SetRect(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), CoordZoomOut(pText->endX, 0), CoordZoomOut(pText->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL)
			{
				if (pRectangle->bIsSelected)
				{
					pRectangle->startY -= 1;
					pRectangle->endY -= 1;
					pRectangle->tracker.m_rect.SetRect(CoordZoomOut(pRectangle->startX, 0), CoordZoomOut(pRectangle->startY, 1),
						CoordZoomOut(pRectangle->endX, 0), CoordZoomOut(pRectangle->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic != NULL)
			{
				if (pPic->bIsSelected)
				{
					CRect rc;
					pPic->startY -= 1;
					rc.left = CoordZoomOut(pPic->startX, 0);
					rc.top = CoordZoomOut(pPic->startY, 1);
					//rc.right=rc.left+pPic->nWidth/ZoomInX_Pic;
					//rc.bottom=rc.top+pPic->nHeight/ZoomInY_Pic;
					rc.right = rc.left + pPic->nWidth*ZoomInX;
					rc.bottom = rc.top + pPic->nHeight*ZoomInY;
					pPic->tracker.m_rect.SetRect(rc.left, rc.top,
						rc.right, rc.bottom);

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL)
			{
				if (pBarCode->bIsSelected)
				{
					pBarCode->startY -= 1;
					pBarCode->endY -= 1;
					pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						CoordZoomOut(pBarCode->endX, 0), CoordZoomOut(pBarCode->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL)
			{
				if (pTBarCode11->bIsSelected)
				{
					pTBarCode11->startY -= 1;
					pTBarCode11->endY -= 1;
					pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(pTBarCode11->startX, 0), CoordZoomOut(pTBarCode11->startY, 1),
						CoordZoomOut(pTBarCode11->endX, 0), CoordZoomOut(pTBarCode11->endY, 1));

				}
			}
		}
	}
	Invalidate(FALSE);
}
void CMyDrawView::OnDownMove()
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;

	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;

	POSITION pos = m_ObjectList.GetTailPosition();
	CObject* pHitItem;
	while (pos != NULL)
	{
		POSITION curpos = pos;
		pHitItem = (CObject*)m_ObjectList.GetPrev(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL)
			{
				if (pEllipse->bIsSelected)
				{
					pEllipse->startY += 1;
					pEllipse->endY += 1;
					pEllipse->tracker.m_rect.SetRect(CoordZoomOut(pEllipse->startX, 0), CoordZoomOut(pEllipse->startY, 1),
						CoordZoomOut(pEllipse->endX, 0), CoordZoomOut(pEllipse->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL)
			{
				if (pLine->bIsSelected)
				{
					pLine->startY += 1;
					pLine->endY += 1;
					pLine->tracker.m_rect.SetRect(CoordZoomOut(pLine->startX, 0), CoordZoomOut(pLine->startY, 1),
						CoordZoomOut(pLine->endX, 0), CoordZoomOut(pLine->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL)
			{
				if (pArc->bIsSelected)
				{
					pArc->startY += 1;
					pArc->endY += 1;
					pArc->tracker.m_rect.SetRect(CoordZoomOut(pArc->startX, 0), CoordZoomOut(pArc->startY, 1),
						CoordZoomOut(pArc->endX, 0), CoordZoomOut(pArc->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText != NULL)
			{
				if (pText->bIsSelected)
				{
					pText->startY += 1;
					pText->endY += 1;
					pText->tracker.m_rect.SetRect(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), CoordZoomOut(pText->endX, 0), CoordZoomOut(pText->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL)
			{
				if (pRectangle->bIsSelected)
				{
					pRectangle->startY += 1;
					pRectangle->endY += 1;
					pRectangle->tracker.m_rect.SetRect(CoordZoomOut(pRectangle->startX, 0), CoordZoomOut(pRectangle->startY, 1),
						CoordZoomOut(pRectangle->endX, 0), CoordZoomOut(pRectangle->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic != NULL)
			{
				if (pPic->bIsSelected)
				{
					CRect rc;
					pPic->startY += 1;
					rc.left = CoordZoomOut(pPic->startX, 0);
					rc.top = CoordZoomOut(pPic->startY, 1);
					rc.right = rc.left + pPic->nWidth*ZoomInX;
					rc.bottom = rc.top + pPic->nHeight*ZoomInY;
					pPic->tracker.m_rect.SetRect(rc.left, rc.top,
						rc.right, rc.bottom);

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL)
			{
				if (pBarCode->bIsSelected)
				{
					pBarCode->startY += 1;
					pBarCode->endY += 1;
					pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0), CoordZoomOut(pBarCode->startY, 1),
						CoordZoomOut(pBarCode->endX, 0), CoordZoomOut(pBarCode->endY, 1));

				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL)
			{
				if (pTBarCode11->bIsSelected)
				{
					pTBarCode11->startY += 1;
					pTBarCode11->endY += 1;
					pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(pTBarCode11->startX, 0), CoordZoomOut(pTBarCode11->startY, 1),
						CoordZoomOut(pTBarCode11->endX, 0), CoordZoomOut(pTBarCode11->endY, 1));

				}
			}
		}
	}
	Invalidate(FALSE);
}
//////
//pos:位置，0-7
//nValue:值，0或1
BYTE CMyDrawView::getByteValue(BYTE orgValue, int pos, int nValue)
{
	//////////////高地位反20180119//////////
	if (nValue == 0)
		return orgValue;


	if (pos == 7)
	{
		return (orgValue | 0x01);
	}
	else if (pos == 6)
	{
		return (orgValue | 0x02);
	}
	else if (pos == 5)
	{
		return (orgValue | 0x04);
	}
	else if (pos == 4)
	{
		return (orgValue | 0x08);
	}
	else if (pos == 3)
	{
		return (orgValue | 0x10);
	}
	else if (pos == 2)
	{
		return (orgValue | 0x20);
	}
	else if (pos == 1)
	{
		return (orgValue | 0x40);
	}
	else if (pos == 0)
	{
		return (orgValue | 0x80);
	}
	return orgValue;
}



void CMyDrawView::read_to_binary_file()
{
	ifstream fin("printData.bin", ios::binary);

	//	int nNum;
	char szBuf[256] = { 0 };
	BYTE bData = 0;

	while (fin.read((char*)&bData, sizeof(bData)))
	{
		int nData = bData;
	}

	//	fin.read((char*)&nNum, sizeof(int));
	//	fin.read(szBuf, sizeof(char) * 256);

	//	std::cout << "int = " << nNum << std::endl;
	//	std::cout << "str = " << szBuf << std::endl;

	fin.close();
}



bool CMyDrawView::write_to_binary_file_bitmap(CBitmap* Bitmap, int nSMJET,int xDPI)
{
	//CThreadLockHandle lockHandle(&thread);                        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁

	bool hasData = false;
	BYTE bData = 0;
	BITMAP bmp;
	ZeroMemory(&bmp, sizeof(bmp));

	Bitmap->GetBitmap(&bmp);  //得到加载位图信息
	unsigned char *px = new unsigned char[bmp.bmHeight*bmp.bmWidthBytes];
	Bitmap->GetBitmapBits(bmp.bmHeight*bmp.bmWidthBytes, px);//读取位图数据
	int PixelBytes = bmp.bmBitsPixel / 8;//每一个像素占字节数
	int nHeigh = 300;///每个画布高300个点，4个1200
	//如果是300dpi
	if (DPIX > 0)
	{
		nHeigh = 150;
	}

	int nX, nY;
	memset(bPrintData[nSMJET], 0, sizeof(bPrintData[nSMJET]));

	//#pragma omp parallel for
	for (int x = 0; x < bmp.bmWidth/xDPI; x++)
	{
		nX = x * xDPI;
		bData = 0;

		for (int mm = 4 * nSMJET; mm < 4 * nSMJET + 4; mm++)
		{
			int m = (int)(mm / 4);
			int n = mm % 4;

			bData = 0;
			int nRep = 0;       //已发生的重合点
			for (int mmm = 0; mmm < nSMJET; mmm++)//解决跨arm之间的拼接 ，如第4和第5直接的拼接
			{
				nRep += nRepPoint[mmm][0];//重合点数
				nRep += nRepPoint[mmm][1];//重合点数
				nRep += nRepPoint[mmm][2];//重合点数
				nRep += nRepPoint[mmm][3];//重合点数
			}
			for (int nnn = 0; nnn < n; nnn++)
			{
				nRep += nRepPoint[nSMJET][nnn];//重合点数
			}

			for (int y = 0; y < nHeigh - nRepPoint[nSMJET][n]; y++)
			{
				nY = y - nRep + nHeigh*mm;
				long rgb_b = nY*bmp.bmWidthBytes + nX*PixelBytes;
				int R = px[rgb_b + 0];
				int G = px[rgb_b + 1];
				int B = px[rgb_b + 2];//以上三个值就分别是BGR三个色彩的值。
				//if (y >= nHeigh - nRep)
				//{
				//	R = 255;
				//	G = 255;
				//	B = 255;
				//}

				int yy = y % 8;
				if ((R == 255) && (G == 255) && (B == 255))
				{
					//bData = getByteValue(bData, yy, 0);
				}
				else
				{
					bData = getByteValue(bData, yy, 1);
					hasData = true;
				}

				if (DPIX > 0)
				{
					if ((y + 1) % 8 == 0)
					{
						if (bData != 0)
						{
							int nCol = (int)(y + 1) / 8;
							bPrintData[m][x][nCol + n * 20 - 1] = bData;
							bData = 0;
						}
					}
					//else if ((y + 1) % 150 == 0)
					//{
					//	bData = 0;
					//	bPrintData[m][x][18 + n * 20] = bData;
					//	bPrintData[m][x][19 + n * 20] = bData;
					//}
				}
				else
				{
					if ((y + 1) % 8 == 0)
					{
						if (bData != 0)
						{
							int nCol = (int)(y + 1) / 8;
							bPrintData[m][x][nCol + n * 40 - 1] = bData;
							bData = 0;
						}
					}
					//else if ((y + 1) % 300 == 0)
					//{
					//	bData = 0;
					//	bPrintData[m][x][37 + n * 40] = bData;
					//	bPrintData[m][x][38 + n * 40] = bData;
					//	bPrintData[m][x][39 + n * 40] = bData;
					//}
				}
			}
		}

	}

	delete(px);
	px = NULL;

	return hasData;

}






bool CMyDrawView::write_to_binary_file_bitmap_Next(CBitmap* Bitmap, int nSMJET, int xDPI)
{
	//CThreadLockHandle lockHandle(&thread);                        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁

	bool hasData = false;
	BYTE bData = 0;
	BITMAP bmp;
	//ZeroMemory(&bmp, sizeof(bmp));

	Bitmap->GetBitmap(&bmp);  //得到加载位图信息
	unsigned char *px = new unsigned char[bmp.bmHeight*bmp.bmWidthBytes];
	Bitmap->GetBitmapBits(bmp.bmHeight*bmp.bmWidthBytes, px);//读取位图数据
	int nHeigh = 300;///每个画布高300个点，4个1200
	//如果是300dpi
	if (DPIX > 0)
	{
		nHeigh = 150;
	}

	int nX, nY;
	memset(bPrintDataNext[nSMJET], 0, sizeof(bPrintDataNext[nSMJET]));

	//#pragma omp parallel for
	for (int x = 0; x < bmp.bmWidth/xDPI; x++)
	{
		nX = x * xDPI;
		bData = 0;
		
		for (int mm = 4 * nSMJET; mm < 4 * nSMJET + 4; mm++)
		{
			int m = (int)(mm / 4);
			int n = mm % 4;

			bData = 0;
			int nRep = 0;       //已发生的重合点
			for (int mmm = 0; mmm < nSMJET; mmm++)//解决跨arm之间的拼接 ，如第4和第5直接的拼接
			{
				nRep += nRepPoint[mmm][0];//重合点数
				nRep += nRepPoint[mmm][1];//重合点数
				nRep += nRepPoint[mmm][2];//重合点数
				nRep += nRepPoint[mmm][3];//重合点数
			}
			for (int nnn = 0; nnn < n;nnn++)
			{ 
				nRep += nRepPoint[nSMJET][nnn];//重合点数
			}
			
			for (int y = 0; y < nHeigh - nRepPoint[nSMJET][n]; y++)
			{
				nY = y - nRep + nHeigh*mm;
				long rgb_b = nY*bmp.bmWidthBytes + nX/8;
				int Rgb = px[rgb_b];

				Rgb = ((Rgb & (1 << (7 - nX%8))) ? 0 : 1);
				//if (y >= nHeigh - nRepNext)
				//{
				//	Rgb = 0;
				//}
				int yy = y % 8;
				if (Rgb == 0)
				{
					//bData = getByteValue(bData, yy, 0);
				}
				else
				{
					bData = getByteValue(bData, yy, 1);
					hasData = true;
				}

				if (DPIX > 0)
				{
					if ((y + 1) % 8 == 0)
					{
						if (bData != 0)
						{
							int nCol = (int)(y + 1) / 8;
							bPrintDataNext[m][x][nCol + n * 20 - 1] = bData;
							bData = 0;
						}
					}
					//else if ((y + 1) % 150 == 0)
					//{
					//	bData = 0;
					//	bPrintDataNext[m][x][18 + n * 20] = bData;
					//	bPrintDataNext[m][x][19 + n * 20] = bData;
					//}
				}
				else
				{
					if ((y + 1) % 8 == 0)
					{
						if (bData != 0)
						{
							int nCol = (int)(y + 1) / 8;
							bPrintDataNext[m][x][nCol + n * 40 - 1] = bData;
							bData = 0;
						}
					}
					//else if ((y + 1) % 300 == 0)
					//{
					//	bData = 0;
					//	bPrintDataNext[m][x][37 + n * 40] = bData;
					//	bPrintDataNext[m][x][38 + n * 40] = bData;
					//	bPrintDataNext[m][x][39 + n * 40] = bData;
					//}
				}
			}
		}

	}

	delete(px);
	px = NULL;

	return hasData;

}


//
//
//bool CMyDrawView::write_to_binary_file_bitmap_Next(CBitmap* Bitmap, int nSMJET)
//{
//	//CThreadLockHandle lockHandle(&thread);                        //需要使用临界区是，声明一个CThreadLockHandle类型的变量，其生存周期结束自动解锁
//
//	bool hasData = false;
//	BYTE bData = 0;
//	BITMAP bmp;
//	//ZeroMemory(&bmp, sizeof(bmp));
//
//	Bitmap->GetBitmap(&bmp);  //得到加载位图信息
//	unsigned char *px = new unsigned char[bmp.bmHeight*bmp.bmWidthBytes];
//	Bitmap->GetBitmapBits(bmp.bmHeight*bmp.bmWidthBytes, px);//读取位图数据
//	int PixelBytes = bmp.bmBitsPixel / 8;//每一个像素占字节数
//	int nHeigh = 300;///每个画布高300个点，4个1200
//	//如果是300dpi
//	if (DPIX > 0)
//	{
//		nHeigh = 150;
//	}
//
//	int nX, nY;
//	memset(bPrintDataNext[nSMJET], 0, sizeof(bPrintDataNext[nSMJET]));
//
//	//#pragma omp parallel for
//	for (int x = 0; x < bmp.bmWidth; x++)
//	{
//		nX = x + 1;
//		bData = 0;
//		int nRep = 0;
//		for (int mm = 4 * nSMJET; mm < 4 * nSMJET + 4; mm++)
//		{
//			int m = (int)(mm / 4);
//			int n = mm % 4;
//
//			bData = 0;
//			nRep += nRepPoint[n];//重合点数
//
//			for (int y = 0; y < nHeigh; y++)
//			{
//				nY = y + nHeigh*mm;
//				long rgb_b = nY*bmp.bmWidthBytes + nX*PixelBytes;
//				int R = px[rgb_b + 0];
//				int G = px[rgb_b + 1];
//				int B = px[rgb_b + 2];//以上三个值就分别是BGR三个色彩的值。
//				if (y >= nHeigh - nRep)
//				{
//					R = 255;
//					G = 255;
//					B = 255;
//				}
//				int yy = y % 8;
//				if ((R == 255) && (G == 255) && (B == 255))
//				{
//					//bData = getByteValue(bData, yy, 0);
//				}
//				else
//				{
//					bData = getByteValue(bData, yy, 1);
//					hasData = true;
//				}
//
//				if (DPIX > 0)
//				{
//					if ((y + 1) % 8 == 0)
//					{
//						int nCol = (int)(y + 1) / 8;
//						bPrintDataNext[m][x][nCol + n * 20 - 1] = bData;
//						bData = 0;
//					}
//					//else if ((y + 1) % 150 == 0)
//					//{
//					//	bData = 0;
//					//	bPrintDataNext[m][x][18 + n * 20] = bData;
//					//	bPrintDataNext[m][x][19 + n * 20] = bData;
//					//}
//				}
//				else
//				{
//					if ((y + 1) % 8 == 0)
//					{
//						int nCol = (int)(y + 1) / 8;
//						bPrintDataNext[m][x][nCol + n * 40 - 1] = bData;
//						bData = 0;
//					}
//					//else if ((y + 1) % 300 == 0)
//					//{
//					//	bData = 0;
//					//	bPrintDataNext[m][x][37 + n * 40] = bData;
//					//	bPrintDataNext[m][x][38 + n * 40] = bData;
//					//	bPrintDataNext[m][x][39 + n * 40] = bData;
//					//}
//				}
//			}
//		}
//
//	}
//
//	delete(px);
//	px = NULL;
//
//	return hasData;
//
//}


////获取同类型的数据个数
int  CMyDrawView::GetPrintTypeNum(int nType)
{
	int nNum = 0;
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;

	POSITION pos = m_ObjectList.GetHeadPosition();
	CObject* pHitItem;

	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL)
			{
				if (nType == DLINE)
				{
					nNum++;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL)
			{
				if (nType == DELPSE)
				{
					nNum++;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL)
			{
				if (nType == DARC)
				{
					nNum++;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText != NULL)
			{
				if (nType == DTEXT)
				{
					nNum++;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL)
			{
				if (nType == DRECT)
				{
					nNum++;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL)
			{
				if (nType == DBAR)
				{
					nNum++;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL)
			{
				if (nType == DBAR)
				{
					nNum++;
				}
			}
		}
	}

	return nNum;
}


///更新和数据库相关的数据
void CMyDrawView::UpdateSQLPrintData(CString sPrintData, int nIndex)
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;

	POSITION pos = m_ObjectList.GetHeadPosition();
	CObject* pHitItem;

	while (pos != NULL)
	{
		pHitItem = (CObject*)m_ObjectList.GetNext(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL)
			{

			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL)
			{

			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL)
			{

			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText != NULL)
			{
				//if (pText->m_nFieldIndex == nIndex)
				{
					if (pText->nConstValueType == 0)
					{
						pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, nIndex, sPrintData);
					}
					else if(pText->nConstValueType == 1)
					{
						pText->MyText = GetCounterText(pText->m_lTxtCounterStartNum, pText->m_lTxtCounterEndNum, pText->m_lTxtCounterStep, n_PageIndex);
						//if (pText->MyText.GetLength() > 0)
						//{
						//	pText->m_lTxtCounterStartNum = _ttol(_T(pText->MyText));
						//}

					}
					else if (pText->nConstValueType == 2 && pText->m_nFieldIndex == nIndex)
					{
						if (pText->nStartPos >= 0 && pText->nStartPos < sPrintData.GetLength() && pText->nCounts>0 && pText->nCounts <= sPrintData.GetLength())
						{
							sPrintData = sPrintData.Mid(pText->nStartPos, pText->nCounts);
						}
						pText->MyText = sPrintData;
					}
					else if (pText->nConstValueType == 3)
					{
						pText->MyText = GetNowTime(pText->m_nTimeFormatIndex);
					}
				}

			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic != NULL)
			{
				if (pPic->m_PicFieldNum == nIndex)
				{
					pPic->m_PicStaticName = sPrintData;
				}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL)
			{

			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL)
			{
				//if(pBarCode->m_nFieldIndex == nIndex)
				{
					if (pBarCode->m_nBarPropertyType == 0)
					{
						pBarCode->sInsetParam = ReplaceFieldNumToSring(pBarCode->sInsetParam, nIndex, sPrintData);
						//pBarCode->setCodeString(sPrintData);
					}
					else if (pBarCode->m_nBarPropertyType == 1)
					{
						CString strCountTmp = GetCounterText(pBarCode->nCounterStart, pBarCode->nCounterEnd, pBarCode->nCounterStep, n_PageIndex);
						pBarCode->setCodeString(strCountTmp);
						//if (strCountTmp.GetLength() > 0)
						//{
						//	pBarCode->nCounterStart = _ttol(_T(strCountTmp));
						//}
					}
					else if (pBarCode->m_nBarPropertyType == 2)
					{
						if (nIndex == pBarCode->m_nSqlTypeFieldIndex)
						{
							if (pBarCode->nStartPos >= 0 && pBarCode->nStartPos < sPrintData.GetLength() && pBarCode->nCounts>0 && pBarCode->nCounts <= sPrintData.GetLength())
							{
								sPrintData = sPrintData.Mid(pBarCode->nStartPos, pBarCode->nCounts);
							}
							pBarCode->sSqlData = sPrintData;
						}
					}

				}
			}
		}

		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL)
			{
				//if(pBarCode->m_nFieldIndex == nIndex)
				{
					if (pTBarCode11->m_nBarPropertyType == 0)
					{
						pTBarCode11->sInsetParam = ReplaceFieldNumToSring(pTBarCode11->sInsetParam, nIndex, sPrintData);
					}
					else if (pTBarCode11->m_nBarPropertyType == 1)
					{
						CString strCountTmp = GetCounterText(pTBarCode11->nCounterStart, pTBarCode11->nCounterEnd, pTBarCode11->nCounterStep, n_PageIndex);
						pTBarCode11->SetData(strCountTmp);
						//if (strCountTmp.GetLength() > 0)
						//{
						//	pTBarCode11->nCounterStart = _ttol(_T(strCountTmp));
						//}
					}
					else if (pTBarCode11->m_nBarPropertyType == 2)
					{
						if (nIndex == pTBarCode11->m_nSqlTypeFieldIndex)
						{
							if (pTBarCode11->nStartPos >= 0 && pTBarCode11->nStartPos < sPrintData.GetLength() && pTBarCode11->nCounts>0 && pTBarCode11->nCounts <= sPrintData.GetLength())
							{
								sPrintData = sPrintData.Mid(pTBarCode11->nStartPos, pTBarCode11->nCounts);
							}
							pTBarCode11->sSqlData = sPrintData;
						}
					}

				}
			}
		}

	}

}

void CMyDrawView::SetRepPoint(int nSMJET, int index, int nValue)
{
	if (index < 0 || index>3)
		return;

	if (nValue < 0)
		nValue = 0;
	if (nValue > 300)
		nValue = 300;
	nRepPoint[nSMJET][index] = nValue;
}
CString CMyDrawView::ReplaceFieldNumToSring(CString sInsert, int nFieldIndex, CString sData)
{
	if (sInsert.Find("*") > 0)
	{
		int i = 0, i2 = 0, i3 = 0;
		while ((i2 = sInsert.Find('*', i)) != -1)
		{
			CString tmp = sInsert.Mid(i, i2 - i);
			int nNumPos1 = -1;
			int nNumPos2 = -1;
			i3 = 0;
			if ((nNumPos1 = tmp.Find('_')) > -1)
			{
				nNumPos2 = tmp.ReverseFind('_');
				if (nNumPos1 >= nNumPos2)
				{
					nNumPos2 = i2;
				}
				CString sPos = "";
				CString sFieldContent = "";
				CString sFieldNum = "";
				CString sText = "";
				AfxExtractSubString(sPos, (LPCTSTR)tmp, 0, '_');
				AfxExtractSubString(sFieldContent, (LPCTSTR)tmp, 1, '_');
				AfxExtractSubString(sFieldNum, (LPCTSTR)tmp, 2, '_');
				AfxExtractSubString(sText, (LPCTSTR)tmp, 3, '_');
				int nPos = atoi(sPos);

				int nFieldNum = atoi(sFieldNum);

				if (nFieldNum == nFieldIndex)
				{
					sInsert.Replace(sText, sData);
					i3 = sData.GetLength() - sFieldContent.GetLength();
				}

			}
			i = i2 + 1 + i3;

		}

	}

	return sInsert;

}


CString CMyDrawView::ProcessInsertData(CString sInsert, CString sText, bool isField)
{
	if (sInsert.Find("*") > 0)
	{
		int i = 0, i2 = 0;
		while ((i2 = sInsert.Find('*', i)) != -1)
		{
			CString tmp = sInsert.Mid(i, i2 - i);
			if (tmp.Find('_') > -1)
			{
				CString sPos = "";
				CString sFieldContent = "";
				CString sFieldNum = "";
				CString sContent = "";
				AfxExtractSubString(sPos, (LPCTSTR)tmp, 0, '_');
				AfxExtractSubString(sFieldContent, (LPCTSTR)tmp, 1, '_');
				AfxExtractSubString(sFieldNum, (LPCTSTR)tmp, 2, '_');
				AfxExtractSubString(sContent, (LPCTSTR)tmp, 3, '_');
				int nPos = atoi(sPos);
				CString tmp = "";
				if (sFieldNum.GetLength() > 0)
				{
					tmp = "[字段" + sFieldNum + "]";
				}
				if (isField)
				{
					sText.Insert(nPos, sFieldContent);
				}
				else
				{
					if (sContent == "#")
					{
						sContent = "";
					}
					sText.Insert(nPos, sContent);
				}


			}
			i = i2 + 1;
		}
	}

	return sText;
}

CString CMyDrawView::OnProcessFieldedString(CString oriString, CString sStyle, bool isField, int nSpace)
{
	if (isField)
	{
		int nStart = 0;
		char sChar = 0;
		int iOrgSpace = 0;
		CString sSpace = "";
		for (int m = 0; m < nSpace; m++)
		{
			sSpace += " ";
		}
		for (int i = 0; i < sStyle.GetLength(); i++)
		{
			sChar = sStyle.GetAt(i);
			if (!(sChar >= '0' && sChar <= '9'))
			{
				CString sTmp = sStyle.Mid(nStart, i - nStart);
				int nFieldCnt = atoi(sTmp);
				nStart = i + 1;
				iOrgSpace += nFieldCnt;
				oriString.Insert(iOrgSpace, sSpace);
				iOrgSpace += sSpace.GetLength();
			}
		}

		CString sTmp = "";
		int nLent = sStyle.GetLength() - nStart;
		if (nStart < sStyle.GetLength())
		{
			sTmp = sStyle.Mid(nStart, nLent);
		}
	}
	return oriString;

}

CString CMyDrawView::OnProcessFieldedString(CString oriString, int nDis, bool isField, int nSpace)
{
	if (isField && nDis > 0)
	{
		int nStart = 0;
		char sChar = 0;
		int iOrgSpace = 0;
		CString sSpace = "";
		for (int m = 0; m < nSpace; m++)
		{
			sSpace += " ";
		}
		int n = int(oriString.GetLength() / nDis);
		for (int i = 0; i < n; i++)
		{
			int nFieldCnt = nDis;
			nStart = i + 1;
			iOrgSpace += nFieldCnt;
			oriString.Insert(iOrgSpace, sSpace);
			iOrgSpace += sSpace.GetLength();
		}
	}
	return oriString;

}


void CMyDrawView::OnMouseMove(UINT nFlags, CPoint point)
{
	UpdateRulersInfo(RW_POSITION, GetScrollPosition(), point);

	CClientDC dc(this);
	OnPrepareDC(&dc);
	dc.DPtoLP(&point);

	// TODO: Add your message handler code here and/or call default
	if (m_enumCurTool == ToolSelect && m_bLButtonDown == true)
	{
		m_endPoint = point;
		Invalidate(FALSE);
	}

	//显示坐标
	CMainFrame*pFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;//获得主窗口指标
	CMFCStatusBar*pStatus = &pFrame->m_wndStatusBar;//获得主窗口的状态栏指针
	if (pStatus)
	{
		CString str;
		str.Format("x:%.2f cm,  y:%.2f cm", point.x / ScaleX / 10, point.y / ScaleY / 10);//格式化文本
		//str.Format("%f",ZoomInX);
		pStatus->SetPaneText(3, str);

	}

	CScrollView::OnMouseMove(nFlags, point);
}



void CMyDrawView::OnRButtonDown(UINT nFlags, CPoint point)
{
	CClientDC dc(this);
	OnPrepareDC(&dc);
	dc.DPtoLP(&point);

	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;

	POSITION pos = m_ObjectList.GetHeadPosition();
	CObject* pHitItem;

	CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd;

	CMenu menu;
	menu.LoadMenu(IDR_MENU1);
	CMenu *pPopup = menu.GetSubMenu(0);//获取右键弹出菜单的子菜单,只有一个子菜单,必须是0  
	ClientToScreen(&point); //把客户区坐标转换为屏幕坐标  

	//pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y,this);  
	//最后一个参数用 this --- 就是把菜单的拥有者指定为CMenuView,那就只能是CMenuView可以响应菜单项的COMMAND消息  
	POSITION posSel = m_selectLsit.GetHeadPosition();
	int nSelCount = 0;
	while (posSel != NULL)
	{
		nSelCount++;
		pHitItem = (CObject*)m_selectLsit.GetNext(posSel);
	}
	if (nSelCount > 0)
	{
		pPopup->EnableMenuItem(ID_EDIT_CUT, MF_BYCOMMAND | MF_ENABLED);
		pPopup->EnableMenuItem(ID_EDIT_COPY, MF_BYCOMMAND | MF_ENABLED);

		pPopup->EnableMenuItem(ID_EDIT_DELETE, MF_BYCOMMAND | MF_ENABLED);
		if (nSelCount == 1)
		{
			pPopup->EnableMenuItem(ID_EDIT_ATT, MF_BYCOMMAND | MF_ENABLED);
		}
		else
		{
			pPopup->EnableMenuItem(ID_EDIT_ATT, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		}
		pPopup->EnableMenuItem(ID_EDIT_LEFTMOVE, MF_BYCOMMAND | MF_ENABLED);
		pPopup->EnableMenuItem(ID_EDIT_RIGHTMOVE, MF_BYCOMMAND | MF_ENABLED);
		pPopup->EnableMenuItem(ID_EDIT_UPMOVE, MF_BYCOMMAND | MF_ENABLED);
		pPopup->EnableMenuItem(ID_EDIT_DOWNMOVE, MF_BYCOMMAND | MF_ENABLED);
		if (nSelCount > 1)
		{
			pPopup->EnableMenuItem(ID_BUTTON_LEFT, MF_BYCOMMAND | MF_ENABLED);
			pPopup->EnableMenuItem(ID_BUTTON_RIGHT, MF_BYCOMMAND | MF_ENABLED);
			pPopup->EnableMenuItem(ID_BUTTON_UP, MF_BYCOMMAND | MF_ENABLED);
			pPopup->EnableMenuItem(ID_BUTTON_DOWN, MF_BYCOMMAND | MF_ENABLED);
			pPopup->EnableMenuItem(ID_BUTTON_Horizontal, MF_BYCOMMAND | MF_ENABLED);
			pPopup->EnableMenuItem(ID_Button_Vertical, MF_BYCOMMAND | MF_ENABLED);
			pPopup->EnableMenuItem(ID_button_interval, MF_BYCOMMAND | MF_ENABLED);
		}
		else
		{
			pPopup->EnableMenuItem(ID_BUTTON_LEFT, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			pPopup->EnableMenuItem(ID_BUTTON_RIGHT, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			pPopup->EnableMenuItem(ID_BUTTON_UP, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			pPopup->EnableMenuItem(ID_BUTTON_DOWN, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			pPopup->EnableMenuItem(ID_BUTTON_Horizontal, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			pPopup->EnableMenuItem(ID_Button_Vertical, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
			pPopup->EnableMenuItem(ID_button_interval, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		}
		pPopup->EnableMenuItem(ID_BUTTON_Lock, MF_BYCOMMAND | MF_ENABLED);
		pPopup->EnableMenuItem(ID_ARRAY_DISTRIBUTE, MF_BYCOMMAND | MF_ENABLED);
	}
	else if (nSelCount == 0)
	{
		pPopup->EnableMenuItem(ID_EDIT_CUT, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_EDIT_COPY, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		//pPopup->EnableMenuItem(ID_EDIT_PASTE, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_EDIT_DELETE, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_EDIT_ATT, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_EDIT_LEFTMOVE, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_EDIT_RIGHTMOVE, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_EDIT_UPMOVE, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_EDIT_DOWNMOVE, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_BUTTON_LEFT, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_BUTTON_RIGHT, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_BUTTON_UP, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_BUTTON_DOWN, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_BUTTON_Horizontal, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_Button_Vertical, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_BUTTON_Lock, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED); 
		pPopup->EnableMenuItem(ID_button_interval, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
		pPopup->EnableMenuItem(ID_ARRAY_DISTRIBUTE, MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
	}
	pPopup->EnableMenuItem(ID_EDIT_PASTE, MF_BYCOMMAND | MF_ENABLED);
	pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, GetParent());
	//最后一个参数用 GetParent()---拥有者为框架,那么框架CMainFrame就可以响应菜单的COMMAND消息  
	// 但是前提是CMenuView中没有设计该菜单项COMMAND消息,才会轮到CMainFrame响应  
	// 这就是 消息响应顺序: 所有的子类中都没有设计响应函数,才会轮到父类. 

	CScrollView::OnRButtonDown(nFlags, point);
}


void CMyDrawView::OnSetElementArray(int nHori, float fHDis, int nVercal, float fVDis, int nFieldStart, int nFieldEnd, int nFX)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;

	POSITION pos = m_selectLsit.GetHeadPosition();
	CObject* pHitItem;

	CMainFrame *pMain = (CMainFrame *)AfxGetMainWnd;
	CPtrList	m_ObjectList_New;
	//递增方向
	if (nFX == 0)
	{ 
		while (pos != NULL)
		{
			pHitItem = (CObject*)m_selectLsit.GetNext(pos);
			if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
			{
				pLine = (CLine*)pHitItem;
				if (pLine != NULL)
				{
					if (pLine->bIsSelected)
					{
						for (int i = 0; i < nVercal; i++)
						{
							for (int j = 0; j < nHori; j++)
							{
								if (i == 0 && j == 0)
								{
									continue;
								}
								CLine* pLine_new = (CLine*)pLine->Clone();
								pLine_new->startX = pLine->startX + fHDis / Zoom*ScaleX*j;
								pLine_new->startY = pLine->startY + fVDis / Zoom*ScaleY*i;
								pLine_new->endX = pLine->endX + fHDis / Zoom*ScaleX*j;
								pLine_new->endY = pLine->endY + fVDis / Zoom*ScaleY*i;
								m_ObjectList_New.AddTail(pLine_new);
							}

						}

					}
				}

			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
			{
				pEllipse = (CEllipse*)pHitItem;
				if (pEllipse != NULL)
				{
					if (pEllipse->bIsSelected)
					{
						for (int i = 0; i < nVercal; i++)
						{
							for (int j = 0; j < nHori; j++)
							{
								if (i == 0 && j == 0)
								{
									continue;
								}
								CEllipse* pEllipse_new = (CEllipse*)pEllipse->Clone();
								pEllipse_new->startX = pEllipse->startX + fHDis / Zoom*ScaleX*j;
								pEllipse_new->startY = pEllipse->startY + fVDis / Zoom*ScaleY*i;
								pEllipse_new->endX = pEllipse->endX + fHDis / Zoom*ScaleX*j;
								pEllipse_new->endY = pEllipse->endY + fVDis / Zoom*ScaleY*i;
								m_ObjectList_New.AddTail(pEllipse_new);
							}

						}

					}
				}

			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
			{
				pArc = (CArc*)pHitItem;
				if (pArc != NULL)
				{
					if (pArc->bIsSelected)
					{
						for (int i = 0; i < nVercal; i++)
						{
							for (int j = 0; j < nHori; j++)
							{
								if (i == 0 && j == 0)
								{
									continue;
								}
								CArc* pArc_new = (CArc*)pArc->Clone();
								pArc_new->startX = pArc->startX + fHDis / Zoom*ScaleX*j;
								pArc_new->startY = pArc->startY + fVDis / Zoom*ScaleY*i;
								pArc_new->endX = pArc->endX + fHDis / Zoom*ScaleX*j;
								pArc_new->endY = pArc->endY + fVDis / Zoom*ScaleY*i;
								m_ObjectList_New.AddTail(pArc_new);
							}

						}

					}
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
			{
				pText = (CText*)pHitItem;
				if (pText != NULL)
				{
					if (pText->bIsSelected)
					{
						int nCount = nFieldStart;
						for (int i = 0; i < nVercal; i++)
						{
							for (int j = 0; j < nHori; j++)
							{
								if (nCount > nFieldEnd)
								{
									nCount = nFieldEnd;
								}

								if (i == 0 && j == 0)
								{
									if (pText->nConstValueType == 0 && pText->sInsetSqlParam.GetLength() > 0)
									{
										pText->m_nFieldIndex = nCount;
										pText->sInsetSqlParam = ResetFieldNum(pText->sInsetSqlParam, nCount);
									}
									else
									{
										pText->nConstValueType = 2;
										pText->m_nFieldIndex = nCount;
									}

									nCount++;
									continue;
								}
								CText* pText_new = (CText*)pText->Clone();
								pText_new->startX = pText->startX + fHDis / Zoom*ScaleX*j;
								pText_new->startY = pText->startY + fVDis / Zoom*ScaleY*i;
								pText_new->endX = pText->endX + fHDis / Zoom*ScaleX*j;
								pText_new->endY = pText->endY + fVDis / Zoom*ScaleY*i;

								pText_new->tracker.m_rect.SetRect(CoordZoomOut(pText_new->startX, 0), CoordZoomOut(pText_new->startY, 1), CoordZoomOut(pText_new->endX, 0), CoordZoomOut(pText_new->endY, 1));


								if (pText_new->nConstValueType == 0 && pText_new->sInsetSqlParam.GetLength() > 0)
								{
									pText_new->m_nFieldIndex = nCount;
									pText_new->sInsetSqlParam = ResetFieldNum(pText_new->sInsetSqlParam, nCount);
								}
								else
								{
									pText_new->nConstValueType = 2;
									pText_new->m_nFieldIndex = nCount;
								}

								nCount++;
								m_ObjectList_New.AddTail(pText_new);
							}

						}

					}
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
			{
				pRectangle = (CRectangle*)pHitItem;
				if (pRectangle != NULL)
				{
					if (pRectangle->bIsSelected)
					{
						for (int i = 0; i < nVercal; i++)
						{
							for (int j = 0; j < nHori; j++)
							{
								if (i == 0 && j == 0)
								{
									continue;
								}
								CRectangle* pRectangle_new = (CRectangle*)pRectangle->Clone();
								pRectangle_new->startX = pRectangle->startX + fHDis / Zoom*ScaleX*j;
								pRectangle_new->startY = pRectangle->startY + fVDis / Zoom*ScaleY*i;
								pRectangle_new->endX = pRectangle->endX + fHDis / Zoom*ScaleX*j;
								pRectangle_new->endY = pRectangle->endY + fVDis / Zoom*ScaleY*i;
								m_ObjectList_New.AddTail(pRectangle_new);
							}

						}

					}
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
			{
				pPic = (CPic*)pHitItem;
				if (pPic != NULL)
				{
					if (pPic->bIsSelected)
					{
						for (int i = 0; i < nVercal; i++)
						{
							for (int j = 0; j < nHori; j++)
							{
								if (i == 0 && j == 0)
								{
									continue;
								}
								CPic* pPic_new = (CPic*)pPic->Clone();
								pPic_new->startX = pPic->startX + fHDis / Zoom*ScaleX*j;
								pPic_new->startY = pPic->startY + fVDis / Zoom*ScaleY*i;


								m_ObjectList_New.AddTail(pPic_new);
							}

						}

					}
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
			{
				pBarCode = (CBarCode*)pHitItem;
				if (pBarCode != NULL)
				{
					int nCount = nFieldStart;
					for (int i = 0; i < nVercal; i++)
					{
						for (int j = 0; j < nHori; j++)
						{
							if (nCount > nFieldEnd)
							{
								nCount = nFieldEnd;
							}
							if (i == 0 && j == 0)
							{
								if (nCount > 0)
								{
									if (pBarCode->m_nBarPropertyType == 0 && pBarCode->sInsetParam.GetLength() > 0)
									{
										pBarCode->m_nFieldIndex = nCount;
										pBarCode->m_nSqlTypeFieldIndex = nCount;
										pBarCode->sInsetParam = ResetFieldNum(pBarCode->sInsetParam, nCount);
									}
									else
									{
										pBarCode->m_nBarPropertyType = 2;
										pBarCode->m_nFieldIndex = nCount;
										pBarCode->m_nSqlTypeFieldIndex = nCount;
									}
								}
								else
								{
									pTBarCode11->sInsetParam = "";
								}

								nCount++;
								continue;
							}
							CBarCode* pBarCode_new = (CBarCode*)pBarCode->Clone();
							pBarCode_new->setCodeString(pBarCode->GetCodeString());
							pBarCode_new->startX = pBarCode->startX + fHDis / Zoom*ScaleX*j;
							pBarCode_new->startY = pBarCode->startY + fVDis / Zoom*ScaleY*i;
							pBarCode_new->endX = pBarCode_new->startX + pBarCode->getBitMapLenth();
							pBarCode_new->endY = pBarCode_new->startY + pBarCode->nBitMapHeight;

							if (nCount > 0)
							{
								if (pBarCode_new->m_nBarPropertyType == 0 && pBarCode_new->sInsetParam.GetLength() > 0)
								{
									pBarCode_new->m_nFieldIndex = nCount;
									pBarCode_new->m_nSqlTypeFieldIndex = nCount;
									pBarCode_new->sInsetParam = ResetFieldNum(pBarCode_new->sInsetParam, nCount);
								}
								else
								{
									pBarCode_new->m_nBarPropertyType = 2;
									pBarCode_new->m_nFieldIndex = nCount;
									pBarCode_new->m_nSqlTypeFieldIndex = nCount;
								}
							}
							else
							{
								pBarCode_new->sInsetParam = "";

							}

							nCount++;

							m_ObjectList_New.AddTail(pBarCode_new);
						}

					}
				}
			}

			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
			{
				pTBarCode11 = (CTBarCode11*)pHitItem;
				if (pTBarCode11 != NULL)
				{
					int nCount = nFieldStart;
					for (int i = 0; i < nVercal; i++)
					{
						for (int j = 0; j < nHori; j++)
						{
							if (nCount > nFieldEnd)
							{
								nCount = nFieldEnd;
							}
							if (i == 0 && j == 0)
							{
								if (nCount > 0)
								{
									if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->sInsetParam.GetLength() > 0)
									{
										pTBarCode11->m_nFieldIndex = nCount;
										pTBarCode11->m_nSqlTypeFieldIndex = nCount;
										pTBarCode11->sInsetParam = ResetFieldNum(pTBarCode11->sInsetParam, nCount);
									}
									else
									{
										pTBarCode11->m_nBarPropertyType = 2;
										pTBarCode11->m_nFieldIndex = nCount;
										pTBarCode11->m_nSqlTypeFieldIndex = nCount;
									}
								}
								else
								{
									pTBarCode11->sInsetParam = "";
								}

								nCount++;
								continue;
							}
							else
							{
								CTBarCode11* pTBarCode11_new = (CTBarCode11*)pTBarCode11->Clone();
								pTBarCode11_new->SetData(pTBarCode11->GetData());
								pTBarCode11_new->startX = pTBarCode11->startX + fHDis / Zoom*ScaleX*j;
								pTBarCode11_new->startY = pTBarCode11->startY + fVDis / Zoom*ScaleY*i;
								pTBarCode11_new->endX = pTBarCode11->endX + fHDis / Zoom*ScaleX*j;
								pTBarCode11_new->endY = pTBarCode11->endY + fVDis / Zoom*ScaleY*i;

								if (nCount > 0)
								{
									if (pTBarCode11_new->m_nBarPropertyType == 0 && pTBarCode11_new->sInsetParam.GetLength() > 0)
									{
										pTBarCode11_new->m_nFieldIndex = nCount;
										pTBarCode11_new->m_nSqlTypeFieldIndex = nCount;
										pTBarCode11_new->sInsetParam = ResetFieldNum(pTBarCode11_new->sInsetParam, nCount);
									}
									else
									{
										pTBarCode11_new->m_nBarPropertyType = 2;
										pTBarCode11_new->m_nFieldIndex = nCount;
										pTBarCode11_new->m_nSqlTypeFieldIndex = nCount;
									}
								}
								else
								{
									pTBarCode11_new->sInsetParam = "";

								}

								m_ObjectList_New.AddTail(pTBarCode11_new);

								nCount++;
							}

						}

					}
				}
			}
		}



	}

	else
	{
		while (pos != NULL)
		{
			pHitItem = (CObject*)m_selectLsit.GetNext(pos);
			if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
			{
				pLine = (CLine*)pHitItem;
				if (pLine != NULL)
				{
					if (pLine->bIsSelected)
					{
						for (int i = 0; i < nHori; i++)
						{
							for (int j = 0; j < nVercal; j++)
							{
								if (i == 0 && j == 0)
								{
									continue;
								}
								CLine* pLine_new = (CLine*)pLine->Clone();
								pLine_new->startX = pLine->startX + fHDis / Zoom*ScaleX*i;
								pLine_new->startY = pLine->startY + fVDis / Zoom*ScaleY*j;
								pLine_new->endX = pLine->endX + fHDis / Zoom*ScaleX*i;
								pLine_new->endY = pLine->endY + fVDis / Zoom*ScaleY*j;
								m_ObjectList_New.AddTail(pLine_new);
							}

						}

					}
				}

			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
			{
				pEllipse = (CEllipse*)pHitItem;
				if (pEllipse != NULL)
				{
					if (pEllipse->bIsSelected)
					{
						for (int i = 0; i < nHori; i++)
						{
							for (int j = 0; j < nVercal; j++)
							{
								if (i == 0 && j == 0)
								{
									continue;
								}
								CEllipse* pEllipse_new = (CEllipse*)pEllipse->Clone();
								pEllipse_new->startX = pEllipse->startX + fHDis / Zoom*ScaleX*i;
								pEllipse_new->startY = pEllipse->startY + fVDis / Zoom*ScaleY*j;
								pEllipse_new->endX = pEllipse->endX + fHDis / Zoom*ScaleX*i;
								pEllipse_new->endY = pEllipse->endY + fVDis / Zoom*ScaleY*j;
								m_ObjectList_New.AddTail(pEllipse_new);
							}

						}

					}
				}

			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
			{
				pArc = (CArc*)pHitItem;
				if (pArc != NULL)
				{
					if (pArc->bIsSelected)
					{
						for (int i = 0; i < nHori; i++)
						{
							for (int j = 0; j < nVercal; j++)
							{
								if (i == 0 && j == 0)
								{
									continue;
								}
								CArc* pArc_new = (CArc*)pArc->Clone();
								pArc_new->startX = pArc->startX + fHDis / Zoom*ScaleX*i;
								pArc_new->startY = pArc->startY + fVDis / Zoom*ScaleY*j;
								pArc_new->endX = pArc->endX + fHDis / Zoom*ScaleX*i;
								pArc_new->endY = pArc->endY + fVDis / Zoom*ScaleY*j;
								m_ObjectList_New.AddTail(pArc_new);
							}

						}

					}
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
			{
				pText = (CText*)pHitItem;
				if (pText != NULL)
				{
					if (pText->bIsSelected)
					{
						int nCount = nFieldStart;
						for (int i = 0; i < nHori; i++)
						{
							for (int j = 0; j < nVercal; j++)
							{
								if (nCount > nFieldEnd)
								{
									nCount = nFieldEnd;
								}

								if (i == 0 && j == 0)
								{
									if (pText->nConstValueType == 0 && pText->sInsetSqlParam.GetLength() > 0)
									{
										pText->m_nFieldIndex = nCount;
										pText->sInsetSqlParam = ResetFieldNum(pText->sInsetSqlParam, nCount);
									}
									else
									{
										pText->nConstValueType = 2;
										pText->m_nFieldIndex = nCount;
									}

									nCount++;
									continue;
								}
								CText* pText_new = (CText*)pText->Clone();
								pText_new->startX = pText->startX + fHDis / Zoom*ScaleX*i;
								pText_new->startY = pText->startY + fVDis / Zoom*ScaleY*j;
								pText_new->endX = pText->endX + fHDis / Zoom*ScaleX*i;
								pText_new->endY = pText->endY + fVDis / Zoom*ScaleY*j;

								if (pText_new->nConstValueType == 0 && pText_new->sInsetSqlParam.GetLength() > 0)
								{
									pText_new->m_nFieldIndex = nCount;
									pText_new->sInsetSqlParam = ResetFieldNum(pText_new->sInsetSqlParam, nCount);
								}
								else
								{
									pText_new->nConstValueType = 2;
									pText_new->m_nFieldIndex = nCount;
								}

								nCount++;
								m_ObjectList_New.AddTail(pText_new);
							}

						}

					}
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
			{
				pRectangle = (CRectangle*)pHitItem;
				if (pRectangle != NULL)
				{
					if (pRectangle->bIsSelected)
					{
						for (int i = 0; i < nHori; i++)
						{
							for (int j = 0; j < nVercal; j++)
							{
								if (i == 0 && j == 0)
								{
									continue;
								}
								CRectangle* pRectangle_new = (CRectangle*)pRectangle->Clone();
								pRectangle_new->startX = pRectangle->startX + fHDis / Zoom*ScaleX*i;
								pRectangle_new->startY = pRectangle->startY + fVDis / Zoom*ScaleY*j;
								pRectangle_new->endX = pRectangle->endX + fHDis / Zoom*ScaleX*i;
								pRectangle_new->endY = pRectangle->endY + fVDis / Zoom*ScaleY*j;
								m_ObjectList_New.AddTail(pRectangle_new);
							}

						}

					}
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
			{
				pPic = (CPic*)pHitItem;
				if (pPic != NULL)
				{
					if (pPic->bIsSelected)
					{
						for (int i = 0; i < nHori; i++)
						{
							for (int j = 0; j < nVercal; j++)
							{
								if (i == 0 && j == 0)
								{
									continue;
								}
								CPic* pPic_new = (CPic*)pPic->Clone();
								pPic_new->startX = pPic->startX + fHDis / Zoom*ScaleX*i;
								pPic_new->startY = pPic->startY + fVDis / Zoom*ScaleY*j;


								m_ObjectList_New.AddTail(pPic_new);
							}

						}

					}
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
			{
				pBarCode = (CBarCode*)pHitItem;
				if (pBarCode != NULL)
				{
					int nCount = nFieldStart;
					for (int i = 0; i < nHori; i++)
					{
						for (int j = 0; j < nVercal; j++)
						{
							if (nCount > nFieldEnd)
							{
								nCount = nFieldEnd;
							}
							if (i == 0 && j == 0)
							{
								if (nCount > 0)
								{
									if (pBarCode->m_nBarPropertyType == 0 && pBarCode->sInsetParam.GetLength() > 0)
									{
										pBarCode->m_nFieldIndex = nCount;
										pBarCode->m_nSqlTypeFieldIndex = nCount;
										pBarCode->sInsetParam = ResetFieldNum(pBarCode->sInsetParam, nCount);
									}
									else
									{
										pBarCode->m_nBarPropertyType = 2;
										pBarCode->m_nFieldIndex = nCount;
										pBarCode->m_nSqlTypeFieldIndex = nCount;
									}
								}
								else
								{
									pTBarCode11->sInsetParam = "";
								}

								nCount++;
								continue;
							}
							CBarCode* pBarCode_new = (CBarCode*)pBarCode->Clone();
							pBarCode_new->setCodeString(pBarCode->GetCodeString());
							pBarCode_new->startX = pBarCode->startX + fHDis / Zoom*ScaleX*i;
							pBarCode_new->startY = pBarCode->startY + fVDis / Zoom*ScaleY*j;
							pBarCode_new->endX = pBarCode_new->startX + pBarCode->getBitMapLenth();
							pBarCode_new->endY = pBarCode_new->startY + pBarCode->nBitMapHeight;

							if (nCount > 0)
							{
								if (pBarCode_new->m_nBarPropertyType == 0 && pBarCode_new->sInsetParam.GetLength() > 0)
								{
									pBarCode_new->m_nFieldIndex = nCount;
									pBarCode_new->m_nSqlTypeFieldIndex = nCount;
									pBarCode_new->sInsetParam = ResetFieldNum(pBarCode_new->sInsetParam, nCount);
								}
								else
								{
									pBarCode_new->m_nBarPropertyType = 2;
									pBarCode_new->m_nFieldIndex = nCount;
									pBarCode_new->m_nSqlTypeFieldIndex = nCount;
								}
							}
							else
							{
								pBarCode_new->sInsetParam = "";

							}

							nCount++;

							m_ObjectList_New.AddTail(pBarCode_new);
						}

					}
				}
			}

			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
			{
				pTBarCode11 = (CTBarCode11*)pHitItem;
				if (pTBarCode11 != NULL)
				{
					int nCount = nFieldStart;
					for (int i = 0; i < nHori; i++)
					{
						for (int j = 0; j < nVercal; j++)
						{
							if (nCount > nFieldEnd)
							{
								nCount = nFieldEnd;
							}
							if (i == 0 && j == 0)
							{
								if (nCount > 0)
								{
									if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->sInsetParam.GetLength() > 0)
									{
										pTBarCode11->m_nFieldIndex = nCount;
										pTBarCode11->m_nSqlTypeFieldIndex = nCount;
										pTBarCode11->sInsetParam = ResetFieldNum(pTBarCode11->sInsetParam, nCount);
									}
									else
									{
										pTBarCode11->m_nBarPropertyType = 2;
										pTBarCode11->m_nFieldIndex = nCount;
										pTBarCode11->m_nSqlTypeFieldIndex = nCount;
									}
								}
								else
								{
									pTBarCode11->sInsetParam = "";
								}

								nCount++;
								continue;
							}
							else
							{
								CTBarCode11* pTBarCode11_new = (CTBarCode11*)pTBarCode11->Clone();
								pTBarCode11_new->SetData(pTBarCode11->GetData());
								pTBarCode11_new->startX = pTBarCode11->startX + fHDis / Zoom*ScaleX*i;
								pTBarCode11_new->startY = pTBarCode11->startY + fVDis / Zoom*ScaleY*j;
								pTBarCode11_new->endX = pTBarCode11->endX + fHDis / Zoom*ScaleX*i;
								pTBarCode11_new->endY = pTBarCode11->endY + fVDis / Zoom*ScaleY*j;

								if (nCount > 0)
								{
									if (pTBarCode11_new->m_nBarPropertyType == 0 && pTBarCode11_new->sInsetParam.GetLength() > 0)
									{
										pTBarCode11_new->m_nFieldIndex = nCount;
										pTBarCode11_new->m_nSqlTypeFieldIndex = nCount;
										pTBarCode11_new->sInsetParam = ResetFieldNum(pTBarCode11_new->sInsetParam, nCount);
									}
									else
									{
										pTBarCode11_new->m_nBarPropertyType = 2;
										pTBarCode11_new->m_nFieldIndex = nCount;
										pTBarCode11_new->m_nSqlTypeFieldIndex = nCount;
									}
								}
								else
								{
									pTBarCode11_new->sInsetParam = "";

								}

								m_ObjectList_New.AddTail(pTBarCode11_new);

								nCount++;
							}

						}

					}
				}
			}
		}

	}






	POSITION pos1 = m_ObjectList_New.GetHeadPosition();
	while (pos1 != NULL)
	{
		pHitItem = (CObject*)m_ObjectList_New.GetNext(pos1);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL)
			{
				//m_ObjectList.AddTail(pLine);
				Sing->pGraph = pLine;
				com = new NewCommand(this);
				invoker->SetCommand(com);
				invoker->Execute();
			}

		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL)
			{
				//m_ObjectList.AddTail(pEllipse);
				Sing->pGraph = pEllipse;
				com = new NewCommand(this);
				invoker->SetCommand(com);
				invoker->Execute();
			}

		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL)
			{
				//m_ObjectList.AddTail(pArc);
				Sing->pGraph = pArc;
				com = new NewCommand(this);
				invoker->SetCommand(com);
				invoker->Execute();
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText != NULL)
			{
				//m_ObjectList.AddTail(pText);
				Sing->pGraph = pText;
				com = new NewCommand(this);
				invoker->SetCommand(com);
				invoker->Execute();
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL)
			{
				//m_ObjectList.AddTail(pRectangle);
				Sing->pGraph = pRectangle;
				com = new NewCommand(this);
				invoker->SetCommand(com);
				invoker->Execute();
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic != NULL)
			{
				//m_ObjectList.AddTail(pPic);
				Sing->pGraph = pPic;
				com = new NewCommand(this);
				invoker->SetCommand(com);
				invoker->Execute();
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL)
			{
				//m_ObjectList.AddTail(pBarCode);
				Sing->pGraph = pBarCode;
				com = new NewCommand(this);
				invoker->SetCommand(com);
				invoker->Execute();
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL)
			{
				//m_ObjectList.AddTail(pTBarCode11);
				Sing->pGraph = pTBarCode11;
				com = new NewCommand(this);
				invoker->SetCommand(com);
				invoker->Execute();
			}
		}
	}
	
	Invalidate(FALSE);


}

void CMyDrawView::OnArrayDistribute()
{
	CElementArray mElementArray;
	mElementArray.SetMaxFieldNum(nMaxColNum);

	if (mElementArray.DoModal() == IDOK)
	{
		//阵列时先关闭预览，阵列完之后再打开
		bool bIsEnablePrview = m_bIsEnablePrview;
		if (bIsEnablePrview)
			OnButtonPreview();

		int m_Horizontal = mElementArray.m_Horizontal;
		float m_fHorizontalDis = mElementArray.m_fHorizontalDis;
		int m_Verital = mElementArray.m_Verital;
		float m_fVerticalDis = mElementArray.m_fVerticalDis;

		int nFieldNumType = mElementArray.nType;

		int nConstIndex = mElementArray.ConstFieldIndex;
		int nChangeStart = mElementArray.m_ChangeFieldStart;
		int nChangeEnd = mElementArray.m_ChangeFieldEnd;

		int nFX = mElementArray.m_FX;

		if (nFieldNumType == 0)
		{
			nChangeStart = nChangeEnd = nConstIndex;
		}

		OnSetElementArray(m_Horizontal, m_fHorizontalDis, m_Verital, m_fVerticalDis, nChangeStart, nChangeEnd, nFX);

		if (bIsEnablePrview)
			OnButtonPreview();

	}

}

void CMyDrawView::OnDeleteItem()
{
	com = new DeleteCommand(this);
	invoker->SetCommand(com);
	invoker->Execute();

	Invalidate(FALSE);
}

CString CMyDrawView::ResetFieldNum(CString sInsert, int nFieldIndex)
{
	if (sInsert.Find("*") > 0)
	{
		int i = 0, i2 = 0, i3 = 0;
		while ((i2 = sInsert.Find('*', i)) != -1)
		{
			CString tmp = sInsert.Mid(i, i2 - i);
			CString TMP = tmp;
			int nNumPos1 = -1;
			int nNumPos2 = -1;
			if ((nNumPos1 = tmp.Find('_')) > -1)
			{
				CString sPos = "";
				CString sFieldContent = "";
				CString sFieldNum = "";
				CString sText = "";
				AfxExtractSubString(sPos, (LPCTSTR)tmp, 0, '_');
				AfxExtractSubString(sFieldContent, (LPCTSTR)tmp, 1, '_');
				AfxExtractSubString(sFieldNum, (LPCTSTR)tmp, 2, '_');
				AfxExtractSubString(sText, (LPCTSTR)tmp, 3, '_');

				//int nFieldNum=atoi(sFieldNum);
				CString sContentNew = "";
				sContentNew.Format("[字段%d]", nFieldIndex);
				CString sFieldNumNew = "";
				sFieldNumNew.Format("%d", nFieldIndex);

				//sInsert.Replace(sFieldContent,sContentNew);
				//sInsert.Replace(sFieldNum,sFieldNumNew);
				TMP = sPos + "_" + sContentNew + "_" + sFieldNumNew + "_" + sText;
			}
			sInsert.Replace(tmp, TMP);
			i = i2 + 1;

		}

	}

	return sInsert;

}

void CMyDrawView::OnButtonSelect()
{
	m_enumCurTool = ToolSelect;

	SetClassLongPtr(this->GetSafeHwnd(),
		GCLP_HCURSOR,
		(LONG)LoadCursor(NULL, IDC_ARROW));//光标离开该区域恢复默认箭头形状
}

void CMyDrawView::OnLButtonUp(UINT nFlags, CPoint point)
{
	CClientDC dc(this);
	OnPrepareDC(&dc);
	dc.DPtoLP(&point);

	m_bLButtonDown = FALSE;
	m_bChangeSize = false;
	m_nRetCode = -1;


	if (m_enumCurTool == ToolSelect)
	{
		Invalidate(FALSE);

		//支持移动后回退
		POSITION pos = m_ObjectList.GetHeadPosition();
		CObject* pHitItem;
		while (pos != NULL)
		{
			pHitItem = (CObject*)m_ObjectList.GetNext(pos);
			CGraph* pGraph = (CGraph*)pHitItem;
			if (pGraph != NULL)
			{
				if (pGraph->bIsSelected && !pGraph->bIsLocked && !isGetData)
				{
					if (!pGraph->_OriRect.EqualRect(pGraph->tracker.m_rect))
					{
						Sing->pGraph = pGraph;
						com = new MoveCommand(this);
						invoker->SetCommand(com);
						invoker->Execute();
						//pGraph->SetRect(pGraph->tracker.m_rect);
					}
				}
			}
		}


		CPoint pointLB;
		CPoint pointRT;

		if (m_endPoint.x - m_startPoint.x == 0)
		{
			if (!(GetKeyState(VK_CONTROL) < 0) && !(GetKeyState(VK_SHIFT)<0))
				m_selectLsit.RemoveAll();
		}
		if (m_bIsDrawRect == false)
		{
			m_startPoint = point;
			m_endPoint = point;
		}

		pointLB.x = m_startPoint.x < point.x ? m_startPoint.x : point.x;
		pointLB.y = m_startPoint.y < point.y ? m_startPoint.y : point.y;
		pointRT.x = m_startPoint.x > point.x ? m_startPoint.x : point.x;
		pointRT.y = m_startPoint.y > point.y ? m_startPoint.y : point.y;

		CRect rectDraw(pointLB, pointRT);
		if (!(GetKeyState(VK_CONTROL) < 0) && !(GetKeyState(VK_SHIFT)<0))
			ClearSelection();
		m_nSelectedCount = SelectElement(rectDraw, point);
	}

	CScrollView::OnLButtonUp(nFlags, point);
}

BOOL CMyDrawView::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;
}

BOOL CMyDrawView::IsRectOverlap(const CRect rcA, const CRect rcB)
{
	if (!(rcA.bottom < rcB.top || rcA.top > rcB.bottom) &&
		!(rcA.right < rcB.left || rcA.left > rcB.right))
	{
		return TRUE;
	}
	if (rcB.top > rcB.bottom && rcB.left > rcB.right)
	{
		if (!(rcA.bottom > rcB.top || rcA.top < rcB.bottom) &&
			!(rcA.right > rcB.left || rcA.left < rcB.right))
		{
			return TRUE;
		}
	}
	if (rcB.top > rcB.bottom)
	{
		if (!(rcA.bottom > rcB.top || rcA.top < rcB.bottom) &&
			!(rcA.right < rcB.left || rcA.left > rcB.right))
		{
			return TRUE;
		}
	}
	if (rcB.left > rcB.right)
	{
		if (!(rcA.bottom < rcB.top || rcA.top > rcB.bottom) &&
			!(rcA.right > rcB.left || rcA.left < rcB.right))
		{
			return TRUE;
		}
	}

	return FALSE;
}

int CMyDrawView::SelectElement(const CRect rectDraw, const CPoint point)
{
	CEllipse* pEllipse = NULL;
	CLine* pLine = NULL;
	CRectangle* pRectangle = NULL;
	CArc* pArc = NULL;
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;
	int	nSelectedCount = 0;
	BOOL bIsSelIn = false;//是否选中图元

	POSITION pos = m_ObjectList.GetTailPosition();
	CObject* pHitItem;
	while (pos != NULL)
	{
		POSITION temp = pos;
		pHitItem = (CObject*)m_ObjectList.GetPrev(pos);
		if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
		{
			pEllipse = (CEllipse*)pHitItem;
			if (pEllipse != NULL)
			{
				CRect rectElement;
				rectElement.left = CoordZoomOut(pEllipse->startX, 0);
				rectElement.top = CoordZoomOut(pEllipse->startY, 1);
				rectElement.right = CoordZoomOut(pEllipse->endX, 0);
				rectElement.bottom = CoordZoomOut(pEllipse->endY, 1);
				if (IsRectOverlap(rectDraw, rectElement))
				{
					bIsSelIn = true;
					nSelectedCount++;
					pEllipse->bIsSelected = true;
					pEllipse->tracker.m_rect.SetRect(CoordZoomOut(pEllipse->startX, 0), CoordZoomOut(pEllipse->startY, 1),
						CoordZoomOut(pEllipse->endX, 0), CoordZoomOut(pEllipse->endY, 1));
					if (m_bLButtonDown == true)
					{
						//pEllipse->tracker.Track(this, point);//显示调节框
						if (m_selectLsit.Find(pEllipse) == NULL)
						{
							if (!(GetKeyState(VK_CONTROL) < 0) && !(GetKeyState(VK_SHIFT)<0))
								m_selectLsit.RemoveAll();
							m_selectLsit.AddTail(pEllipse);
						}
					}
					else//down的时候不加，up的时候加
					{
						if (m_selectLsit.Find(pEllipse) == NULL)
							m_selectLsit.AddTail(pEllipse);
					}
					if (m_bIsDrawRect == false)
					{
						break;
					}
				}
				//else
				//{
				//	pEllipse->bIsSelected = false;
				//}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
		{
			pPic = (CPic*)pHitItem;
			if (pPic != NULL)
			{
				CRect rectElement;

				rectElement.left = CoordZoomOut(pPic->startX, 0);
				rectElement.top = CoordZoomOut(pPic->startY, 1);
				rectElement.right = CoordZoomOut(pPic->startX + pPic->nWidth, 0);
				rectElement.bottom = CoordZoomOut(pPic->startY + pPic->nHeight, 1);

				if (IsRectOverlap(rectDraw, rectElement))
				{
					bIsSelIn = true;
					pPic->bIsSelected = true;
					pPic->tracker.m_rect.SetRect(rectElement.left, rectElement.top,
						rectElement.right, rectElement.bottom);
					if (m_bLButtonDown == true)
					{
						//pPic->tracker.Track(this, point);//显示调节框
						if (m_selectLsit.Find(pPic) == NULL)
						{
							if (!(GetKeyState(VK_CONTROL) < 0) && !(GetKeyState(VK_SHIFT)<0))
								m_selectLsit.RemoveAll();
							m_selectLsit.AddTail(pPic);
						}
					}
					else//down的时候不加，up的时候加
					{
						if (m_selectLsit.Find(pPic) == NULL)
							m_selectLsit.AddTail(pPic);
					}
					nSelectedCount++;
					if (m_bIsDrawRect == false)
					{
						break;
					}
				}
				//else
				//{
				//	pPic->bIsSelected = false;
				//}
			}

		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
		{
			pLine = (CLine*)pHitItem;
			if (pLine != NULL)
			{
				CRect rectElement(CoordZoomOut(pLine->startX, 0), CoordZoomOut(pLine->startY, 1), CoordZoomOut(pLine->endX, 0), CoordZoomOut(pLine->endY, 1));
				if (IsRectOverlap(rectDraw, rectElement))
				{
					bIsSelIn = true;
					pLine->bIsSelected = true;
					pLine->tracker.m_rect.SetRect(CoordZoomOut(pLine->startX, 0), CoordZoomOut(pLine->startY, 1),
						CoordZoomOut(pLine->endX, 0), CoordZoomOut(pLine->endY, 1));
					if (m_bLButtonDown == true)
					{
						//pLine->tracker.Track(this, point);//显示调节框
						if (m_selectLsit.Find(pLine) == NULL)
						{
							if (!(GetKeyState(VK_CONTROL) < 0) && !(GetKeyState(VK_SHIFT)<0))
								m_selectLsit.RemoveAll();
							m_selectLsit.AddTail(pLine);
						}
					}
					else//down的时候不加，up的时候加
					{
						if (m_selectLsit.Find(pLine) == NULL)
							m_selectLsit.AddTail(pLine);
					}
					nSelectedCount++;
					if (m_bIsDrawRect == false)
					{
						break;
					}
				}
				//else
				//{
				//	pLine->bIsSelected = false;
				//}
			}
			//pLine->tracker.Track(this, point);//显示调节框
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
		{
			pArc = (CArc*)pHitItem;
			if (pArc != NULL)
			{
				CRect rectElement(CoordZoomOut(pArc->startX, 0), CoordZoomOut(pArc->startY, 1), CoordZoomOut(pArc->endX, 0), CoordZoomOut(pArc->endY, 1));
				if (IsRectOverlap(rectDraw, rectElement))
				{
					bIsSelIn = true;
					pArc->bIsSelected = true;
					pArc->tracker.m_rect.SetRect(CoordZoomOut(pArc->startX, 0), CoordZoomOut(pArc->startY, 1),
						CoordZoomOut(pArc->endX, 0), CoordZoomOut(pArc->endY, 1));
					if (m_bLButtonDown == true)
					{
						//pArc->tracker.Track(this, point);//显示调节框
						if (m_selectLsit.Find(pArc) == NULL)
						{
							if (!(GetKeyState(VK_CONTROL) < 0) && !(GetKeyState(VK_SHIFT)<0))
								m_selectLsit.RemoveAll();
							m_selectLsit.AddTail(pArc);
						}
					}
					else//down的时候不加，up的时候加
					{
						if (m_selectLsit.Find(pArc) == NULL)
							m_selectLsit.AddTail(pArc);
					}
					nSelectedCount++;
					if (m_bIsDrawRect == false)
					{
						break;
					}
				}
				//else
				//{
				//	pArc->bIsSelected = false;
				//}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pHitItem;
			if (pText != NULL)
			{

				CRect rectElement(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), CoordZoomOut(pText->endX, 0), CoordZoomOut(pText->endY, 1));
				if (IsRectOverlap(rectDraw, rectElement))
				{
					bIsSelIn = true;
					if (pText->nConstValueType == 3)
					{
						pText->MyText = GetNowTime(pText->m_nTimeFormatIndex);
					}
					pText->bIsSelected = true;
					pText->tracker.m_rect.SetRect(CoordZoomOut(pText->startX, 0), CoordZoomOut(pText->startY, 1), CoordZoomOut(pText->endX, 0), CoordZoomOut(pText->endY, 1));
					if (m_bLButtonDown == true)
					{
						//pText->tracker.Track(this, point);//显示调节框
						if (m_selectLsit.Find(pText) == NULL)
						{
							if (!(GetKeyState(VK_CONTROL) < 0) && !(GetKeyState(VK_SHIFT)<0))
								m_selectLsit.RemoveAll();
							m_selectLsit.AddTail(pText);
						}
					}
					else//down的时候不加，up的时候加
					{
						if (m_selectLsit.Find(pText) == NULL)
							m_selectLsit.AddTail(pText);
					}
					nSelectedCount++;
					if (m_bIsDrawRect == false)
					{
						break;
					}
				}
				//else
				//{
				//	pText->bIsSelected = false;
				//}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
		{
			pRectangle = (CRectangle*)pHitItem;
			if (pRectangle != NULL)
			{
				CRect rectElement;
				rectElement.left = CoordZoomOut(pRectangle->startX, 0);
				rectElement.top = CoordZoomOut(pRectangle->startY, 1);
				rectElement.right = CoordZoomOut(pRectangle->endX, 0);
				rectElement.bottom = CoordZoomOut(pRectangle->endY, 1);
				if (IsRectOverlap(rectDraw, rectElement))
				{
					bIsSelIn = true;
					pRectangle->bIsSelected = true;
					pRectangle->tracker.m_rect.SetRect(CoordZoomOut(pRectangle->startX, 0),
						CoordZoomOut(pRectangle->startY, 1), CoordZoomOut(pRectangle->endX, 0), CoordZoomOut(pRectangle->endY, 1));
					if (m_bLButtonDown == true)
					{
						//pRectangle->tracker.Track(this, point);//显示调节框
						if (m_selectLsit.Find(pRectangle) == NULL)
						{
							if (!(GetKeyState(VK_CONTROL) < 0) && !(GetKeyState(VK_SHIFT)<0))
								m_selectLsit.RemoveAll();
							m_selectLsit.AddTail(pRectangle);
						}
					}
					else//down的时候不加，up的时候加
					{
						if (m_selectLsit.Find(pRectangle) == NULL)
							m_selectLsit.AddTail(pRectangle);
					}
					nSelectedCount++;
					if (m_bIsDrawRect == false)
					{
						break;
					}
				}
				//else
				//{
				//	pRectangle->bIsSelected = false;
				//}
			}
		}

		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pHitItem;
			if (pTBarCode11 != NULL)
			{
				int nLen = 12;
				int nHeight = 8;


				int Len = pTBarCode11->endX - pTBarCode11->startX;
				if (Len < 12)
				{
					Len = nLen;
				}
				int Height = pTBarCode11->endY - pTBarCode11->startY;
				if (Height < 8)
				{
					Height = nHeight;
				}

				CRect rectElement;
				rectElement.left = CoordZoomOut(pTBarCode11->startX, 0);
				rectElement.top = CoordZoomOut(pTBarCode11->startY, 1);
				rectElement.right = CoordZoomOut(pTBarCode11->startX + Len, 0);
				rectElement.bottom = CoordZoomOut(pTBarCode11->startY + Height, 1);
				if (IsRectOverlap(rectDraw, rectElement))
				{
					bIsSelIn = true;
					pTBarCode11->bIsSelected = true;
					pTBarCode11->tracker.m_rect.SetRect(CoordZoomOut(pTBarCode11->startX, 0),
						CoordZoomOut(pTBarCode11->startY, 1), CoordZoomOut(pTBarCode11->startX + Len, 0), CoordZoomOut(pTBarCode11->startY + Height, 1));
					if (m_bLButtonDown == true)
					{
						//pTBarCode11->tracker.Track(this, point);//显示调节框
						if (m_selectLsit.Find(pTBarCode11) == NULL)
						{
							if (!(GetKeyState(VK_CONTROL) < 0) && !(GetKeyState(VK_SHIFT)<0))
								m_selectLsit.RemoveAll();
							m_selectLsit.AddTail(pTBarCode11);
						}
					}
					else//down的时候不加，up的时候加
					{
						if (m_selectLsit.Find(pTBarCode11) == NULL)
							m_selectLsit.AddTail(pTBarCode11);
					}
					nSelectedCount++;
					if (m_bIsDrawRect == false)
					{
						break;
					}
				}
				//else
				//{
				//	pTBarCode11->bIsSelected = false;
				//}
			}
		}
		else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{
			pBarCode = (CBarCode*)pHitItem;
			if (pBarCode != NULL)
			{
				int nLen = pBarCode->getBitMapLenth();
				CRect rectElement;

				int nHeight = pBarCode->nBitMapHeight + pBarCode->TextFont.lfHeight + pBarCode->nTextBarDis;

				if (nLen < 12)
					nLen = 12;

				if (nHeight < 10)
					nHeight = 10;

				rectElement.left = CoordZoomOut(pBarCode->startX, 0);
				rectElement.top = CoordZoomOut(pBarCode->startY, 1);
				rectElement.right = CoordZoomOut(pBarCode->startX + nLen, 0);
				rectElement.bottom = CoordZoomOut(pBarCode->startY + nHeight, 1);

				if (pBarCode->m_nRotateTyp == 1)
				{
					rectElement.left = CoordZoomOut(pBarCode->startX, 0);
					rectElement.top = CoordZoomOut(pBarCode->startY, 1);
					rectElement.right = CoordZoomOut(pBarCode->startX + nHeight, 0);
					rectElement.bottom = CoordZoomOut(pBarCode->startY + nLen, 1);
				}
				else if (pBarCode->m_nRotateTyp == 2)
				{
					rectElement.left = CoordZoomOut(pBarCode->startX, 0);
					rectElement.top = CoordZoomOut(pBarCode->startY, 1);
					rectElement.right = CoordZoomOut(pBarCode->startX + nLen, 0);
					rectElement.bottom = CoordZoomOut(pBarCode->startY + nHeight, 1);

				}
				else if (pBarCode->m_nRotateTyp == 3)
				{
					rectElement.left = CoordZoomOut(pBarCode->startX, 0);
					rectElement.top = CoordZoomOut(pBarCode->startY, 1);
					rectElement.right = CoordZoomOut(pBarCode->startX + nHeight, 0);
					rectElement.bottom = CoordZoomOut(pBarCode->startY + nLen, 1);
				}


				if (IsRectOverlap(rectDraw, rectElement))
				{
					bIsSelIn = true;
					pBarCode->bIsSelected = true;
					if (pBarCode->m_nRotateTyp == 0)
					{
						pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0),
							CoordZoomOut(pBarCode->startY, 1), CoordZoomOut(pBarCode->startX + nLen, 0), CoordZoomOut(pBarCode->startY + nHeight, 1));
					}
					else if (pBarCode->m_nRotateTyp == 1)
					{

						pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0),
							CoordZoomOut(pBarCode->startY, 1), CoordZoomOut(pBarCode->startX + nHeight, 0), CoordZoomOut(pBarCode->startY + nLen, 1));
					}
					else if (pBarCode->m_nRotateTyp == 2)
					{
						pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0),
							CoordZoomOut(pBarCode->startY, 1), CoordZoomOut(pBarCode->startX + nLen, 0), CoordZoomOut(pBarCode->startY + nHeight, 1));
					}
					else if (pBarCode->m_nRotateTyp == 3)
					{
						pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0),
							CoordZoomOut(pBarCode->startY, 1), CoordZoomOut(pBarCode->startX + nHeight, 0), CoordZoomOut(pBarCode->startY + nLen, 1));
					}

					if (pBarCode->GetCodeType() == 8)//二维码
					{
						int nHeight = pBarCode->nBitMapHeight;
						pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0),
							CoordZoomOut(pBarCode->startY, 1), CoordZoomOut(pBarCode->startX + nHeight, 0), CoordZoomOut(pBarCode->startY + nHeight, 1));
					}
					else if (pBarCode->GetCodeType() == 9)//二维码
					{
						int nHeight = pBarCode->nBitMapHeight;
						pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0),
							CoordZoomOut(pBarCode->startY, 1), CoordZoomOut(pBarCode->startX + nHeight, 0), CoordZoomOut(pBarCode->startY + nHeight, 1));
					}
					else if (pBarCode->GetCodeType() == 10)//二维码
					{
						int nHeight = pBarCode->nBitMapHeight;
						pBarCode->tracker.m_rect.SetRect(CoordZoomOut(pBarCode->startX, 0),
							CoordZoomOut(pBarCode->startY, 1), CoordZoomOut(pBarCode->startX + nLen, 0), CoordZoomOut(pBarCode->startY + nHeight, 1));

					}

					if (m_bLButtonDown == true)
					{
						//pBarCode->tracker.Track(this, point);//显示调节框
						if (m_selectLsit.Find(pBarCode) == NULL)
						{
							if (!(GetKeyState(VK_CONTROL) < 0) && !(GetKeyState(VK_SHIFT)<0))
								m_selectLsit.RemoveAll();
							m_selectLsit.AddTail(pBarCode);
						}
					}
					else//down的时候不加，up的时候加
					{
						if (m_selectLsit.Find(pBarCode) == NULL)
							m_selectLsit.AddTail(pBarCode);
					}
					nSelectedCount++;
					if (m_bIsDrawRect == false)
					{
						break;
					}
				}
				//else
				//{
				//	pBarCode->bIsSelected = false;
				//}
			}
		}
	}
	if (bIsSelIn)
	{
		pos = m_selectLsit.GetHeadPosition();

		map<CString, int> attributeNum;
		int num = 0;
		while (pos != NULL)
		{
			pHitItem = (CObject*)m_selectLsit.GetNext(pos);

			CGraph* pGraph = (CGraph*)pHitItem;
			if (pGraph != NULL)
			{
				if (!pGraph->tracker.m_rect.IsRectNull())
					pGraph->SetRect(pGraph->tracker.m_rect);

				pGraph->InitPropList();

				if (m_enumCurTool == ToolSelect && m_bLButtonDown == true)
					pGraph->_OriRect = pGraph->GetRect();//选中后的位置
			}

			if (pHitItem->IsKindOf(RUNTIME_CLASS(CEllipse)))
			{
				pEllipse = (CEllipse*)pHitItem;
				if (pEllipse != NULL)
				{
					pEllipse->bIsSelected = true;
				}
				attributeNum[_T("椭圆")]++;
				num++;
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CPic)))
			{
				pPic = (CPic*)pHitItem;
				if (pPic != NULL)
				{
					pPic->bIsSelected = true;
				}
				attributeNum[_T("图片")]++;
				num++;
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CLine)))
			{
				pLine = (CLine*)pHitItem;
				if (pLine != NULL)
				{
					pLine->bIsSelected = true;
				}
				attributeNum[_T("直线")]++;
				num++;
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CArc)))
			{
				pArc = (CArc*)pHitItem;
				if (pArc != NULL)
				{
					pArc->bIsSelected = true;
				}
				attributeNum[_T("弧段")]++;
				num++;
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
			{
				pText = (CText*)pHitItem;
				if (pText != NULL)
				{
					pText->bIsSelected = true;
				}
				attributeNum[_T("文本")]++;
				num++;
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CRectangle)))
			{
				pRectangle = (CRectangle*)pHitItem;
				if (pRectangle != NULL)
				{
					pRectangle->bIsSelected = true;
				}
				attributeNum[_T("矩形")]++;
				num++;
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
			{
				pTBarCode11 = (CTBarCode11*)pHitItem;
				if (pTBarCode11 != NULL)
				{
					pTBarCode11->bIsSelected = true;
				}
				attributeNum[_T("TBarCode")]++;
				num++;
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
			{
				pBarCode = (CBarCode*)pHitItem;
				if (pBarCode != NULL)
				{
					pBarCode->bIsSelected = true;
				}
				attributeNum[_T("BarCode")]++;
				num++;
			}

		}
		//更新属性窗口对象个数
		if (num > 0)
		{
			CMainFrame* pFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;//获得主窗口指标
			pFrame->m_wndProperties.m_wndObjectCombo.ResetContent();
			if (attributeNum.size() > 1)
			{
				CString tmp;
				tmp.Format(_T("ALL(%d)"), num);
				pFrame->m_wndProperties.m_wndObjectCombo.AddString(tmp);
			}
			if (attributeNum[_T("椭圆")] > 0)
			{
				CString tmp;
				tmp.Format(_T("椭圆(%d)"), attributeNum[_T("椭圆")]);
				pFrame->m_wndProperties.m_wndObjectCombo.AddString(tmp);
			}
			if (attributeNum[_T("图片")] > 0)
			{
				CString tmp;
				tmp.Format(_T("图片(%d)"), attributeNum[_T("图片")]);
				pFrame->m_wndProperties.m_wndObjectCombo.AddString(tmp);
			}
			if (attributeNum[_T("直线")] > 0)
			{
				CString tmp;
				tmp.Format(_T("直线(%d)"), attributeNum[_T("直线")]);
				pFrame->m_wndProperties.m_wndObjectCombo.AddString(tmp);
			}
			if (attributeNum[_T("弧段")] > 0)
			{
				CString tmp;
				tmp.Format(_T("弧段(%d)"), attributeNum[_T("弧段")]);
				pFrame->m_wndProperties.m_wndObjectCombo.AddString(tmp);
			}
			if (attributeNum[_T("文本")] > 0)
			{
				CString tmp;
				tmp.Format(_T("文本(%d)"), attributeNum[_T("文本")]);
				pFrame->m_wndProperties.m_wndObjectCombo.AddString(tmp);
			}
			if (attributeNum[_T("矩形")] > 0)
			{
				CString tmp;
				tmp.Format(_T("矩形(%d)"), attributeNum[_T("矩形")]);
				pFrame->m_wndProperties.m_wndObjectCombo.AddString(tmp);
			}
			if (attributeNum[_T("TBarCode")] > 0)
			{
				CString tmp;
				tmp.Format(_T("TBarCode(%d)"), attributeNum[_T("TBarCode")]);
				pFrame->m_wndProperties.m_wndObjectCombo.AddString(tmp);
			}
			if (attributeNum[_T("BarCode")] > 0)
			{
				CString tmp;
				tmp.Format(_T("BarCode(%d)"), attributeNum[_T("BarCode")]);
				pFrame->m_wndProperties.m_wndObjectCombo.AddString(tmp);
			}

			set_DropDownHight(pFrame->m_wndProperties.m_wndObjectCombo, 10);
			set_DropDownWidth(pFrame->m_wndProperties.m_wndObjectCombo, 100);

			pFrame->m_wndProperties.m_wndObjectCombo.SetCurSel(0);
		}
	}
	else
	{
		m_selectLsit.RemoveAll();

		UnInitPropList();

	}

	return nSelectedCount;
}

void CMyDrawView::set_DropDownHight(CComboBox& box, UINT LinesToDisplay)
{
	ASSERT(IsWindow(box));

	CRect cbSize;
	int Height;

	box.GetClientRect(cbSize);
	Height = box.GetItemHeight(-1);
	Height += box.GetItemHeight(0) * LinesToDisplay;

	Height += GetSystemMetrics(SM_CYEDGE) * 2;

	Height += GetSystemMetrics(SM_CYEDGE) * 2;

	box.SetWindowPos(NULL,
		0, 0,
		cbSize.right, Height,
		SWP_NOMOVE | SWP_NOZORDER
		);
}

void CMyDrawView::set_DropDownWidth(CComboBox& box, UINT nWidth)
{
	ASSERT(IsWindow(box));
	box.SetDroppedWidth(nWidth);
}


void  CMyDrawView::AdjustRectSize(CRectTracker &tracker, CPoint point, int sx, int sy, int ex, int ey, int nOffsetx, int nOffsety, CObject* pHitItem, BOOL isFixScale)
{

	CRect rect = tracker.m_rect;

	int nRetCode = tracker.HitTest(point);
	if (m_nRetCode == -1 && m_bLButtonDown == TRUE)
	{
		m_nRetCode = nRetCode;
	}

	double dRatio = 0.0f;
	dRatio = ex * 1.0 / ey;

	if (m_nRetCode == 0)
	{
		m_bChangeSize = true;
		if (isFixScale)
		{
			//按比例修正y
			nOffsety = round(nOffsetx / dRatio);
		}
		rect.left = rect.left + nOffsetx;
		rect.top = rect.top + nOffsety;
		if ((rect.right - rect.left) == 1)
		{
			rect.right = rect.left;
		}
		if ((rect.bottom - rect.top) == 1)
		{
			rect.bottom = rect.top;
		}
		//rect.left = rect.left + nOffsetx < rect.right ? rect.left + nOffsetx : rect.left;
		//rect.top = rect.top + nOffsety < rect.bottom ? rect.top + nOffsety : rect.top;
		tracker.m_rect = rect;

	}
	else if (m_nRetCode == 1)
	{
		m_bChangeSize = true;
		if (isFixScale)
		{
			nOffsety = round(-nOffsetx / dRatio);
		}
		rect.top = rect.top + nOffsety;
		rect.right = rect.right + nOffsetx;
		if ((rect.right - rect.left) == 1)
		{
			rect.right = rect.left;
		}
		if ((rect.bottom - rect.top) == 1)
		{
			rect.bottom = rect.top;
		}
		//rect.top = rect.top + nOffsety < rect.bottom ? rect.top + nOffsety : rect.top;
		//rect.right = rect.left < rect.right + nOffsetx ? rect.right + nOffsetx : rect.right;

		tracker.m_rect = rect;
	}
	else if (m_nRetCode == 2)
	{
		m_bChangeSize = true;
		if (isFixScale)
		{
			//按比例修正y
			nOffsety = round(nOffsetx / dRatio);
		}
		rect.right = rect.right + nOffsetx;
		rect.bottom = rect.bottom + nOffsety;
		if ((rect.right - rect.left) == 1)
		{
			rect.right = rect.left;
		}
		if ((rect.bottom - rect.top) == 1)
		{
			rect.bottom = rect.top;
		}
		//rect.right = rect.left < rect.right + nOffsetx ? rect.right + nOffsetx : rect.right;
		//rect.bottom = rect.top < rect.bottom + nOffsety ? rect.bottom + nOffsety : rect.bottom;
		tracker.m_rect = rect;
	}
	else if (m_nRetCode == 3)
	{
		m_bChangeSize = true;
		if (isFixScale)
		{
			nOffsety = round(-nOffsetx / dRatio);
		}
		rect.left = rect.left + nOffsetx;
		rect.bottom = rect.bottom + nOffsety;
		if ((rect.right - rect.left) == 1)
		{
			rect.right = rect.left;
		}
		if ((rect.bottom - rect.top) == 1)
		{
			rect.bottom = rect.top;
		}
		//rect.left = rect.left + nOffsetx < rect.right ? rect.left + nOffsetx : rect.left;
		//rect.bottom = rect.top < rect.bottom + nOffsety ? rect.bottom + nOffsety : rect.bottom;
		tracker.m_rect = rect;
	}
	else if (m_nRetCode == 4)
	{
		m_bChangeSize = true;
		if (isFixScale)
		{
			nOffsetx = nOffsety * dRatio;
		}
		rect.right = rect.right - nOffsetx;
		rect.top = rect.top + nOffsety;
		if ((rect.right - rect.left) == 1)
		{
			rect.right = rect.left;
		}
		if ((rect.bottom - rect.top) == 1)
		{
			rect.bottom = rect.top;
		}
		//rect.right = rect.left < rect.right - nOffsetx ? rect.right - nOffsetx : rect.right;
		//rect.top = rect.top + nOffsety < rect.bottom ? rect.top + nOffsety : rect.top;
		tracker.m_rect = rect;
	}
	else if (m_nRetCode == 5)
	{
		m_bChangeSize = true;
		if (isFixScale)
		{
			nOffsety = nOffsetx / dRatio;
		}
		rect.bottom = rect.bottom + nOffsety;
		rect.right = rect.right + nOffsetx;
		if ((rect.right - rect.left) == 1)
		{
			rect.right = rect.left;
		}
		if ((rect.bottom - rect.top) == 1)
		{
			rect.bottom = rect.top;
		}
		//rect.bottom = rect.top < rect.bottom + nOffsety ? rect.bottom + nOffsety : rect.bottom;
		//rect.right = rect.left < rect.right + nOffsetx ? rect.right + nOffsetx : rect.right;
		tracker.m_rect = rect;
	}
	else if (m_nRetCode == 6)
	{
		m_bChangeSize = true;
		if (isFixScale)
		{
			nOffsetx = nOffsety * dRatio;
		}
		rect.right = rect.right + nOffsetx;
		rect.bottom = rect.bottom + nOffsety;
		if ((rect.right - rect.left) == 1)
		{
			rect.right = rect.left;
		}
		if ((rect.bottom - rect.top) == 1)
		{
			rect.bottom = rect.top;
		}
		//rect.right = rect.left < rect.right + nOffsetx ? rect.right + nOffsetx : rect.right;
		//rect.bottom = rect.top < rect.bottom + nOffsety ? rect.bottom + nOffsety : rect.bottom;
		tracker.m_rect = rect;
	}
	else if (m_nRetCode == 7)
	{
		m_bChangeSize = true;
		if (isFixScale)
		{
			nOffsety = nOffsetx / dRatio;
		}
		rect.bottom = rect.bottom - nOffsety;
		rect.left = rect.left + nOffsetx;
		if ((rect.right - rect.left) == 1)
		{
			rect.right = rect.left;
		}
		if ((rect.bottom - rect.top) == 1)
		{
			rect.bottom = rect.top;
		}
		//rect.bottom = rect.top < rect.bottom - nOffsety ? rect.bottom - nOffsety : rect.bottom;
		//rect.left = rect.left + nOffsetx < rect.right ? rect.left + nOffsetx : rect.left;
		tracker.m_rect = rect;
	}
	else if (m_bChangeSize == false)
	{
		//CRect rect = tracker.m_rect;
		tracker.m_rect.SetRect(rect.left + nOffsetx, rect.top + nOffsety,
			rect.right + nOffsetx, rect.bottom + nOffsety);

	}
}



//复制
void CMyDrawView::OnEditCopy()
{
	// TODO: 在此添加命令处理程序代码
	Sing->pasteLsit.RemoveAll();

	POSITION pos = m_selectLsit.GetHeadPosition();
	CObject* pHitItem;
	while (pos != NULL)
	{
		pHitItem = (CObject*)m_selectLsit.GetNext(pos);
		Sing->pasteLsit.AddTail(pHitItem);
	}

}


//粘贴，将复制或剪切过的图形添加到存储数组中，然后发起重绘操作
void CMyDrawView::OnEditPaste()
{
	com = new PasteCommand(this);
	invoker->SetCommand(com);
	if (Sing->pasteLsit.GetCount() > 0)
	{
		ClearSelection();
		m_selectLsit.RemoveAll();
	}
	invoker->Execute();
	Invalidate(FALSE);//重画
}


//剪切，从存储容器中删除记录，并重绘
void CMyDrawView::OnEditCut()
{
	// TODO: 在此添加命令处理程序代码
	com = new CutCommand(this);
	invoker->SetCommand(com);
	invoker->Execute();
	Invalidate(FALSE);//重画
}


//撤销
void CMyDrawView::OnEditUndo()
{
	// TODO: 在此添加命令处理程序代码
	invoker->UnExecute();
	Invalidate();
}


void CMyDrawView::OnUpdateEditUndo(CCmdUI* pCmdUI)
{
	BOOL bOn = TRUE;
	if (invoker->Command.GetSize() <= 0)
		bOn = FALSE;
	pCmdUI->Enable(bOn);
}
//重做
void CMyDrawView::OnEditRedo()
{
	// TODO: 在此添加命令处理程序代码
	invoker->ReExecute();
	Invalidate(FALSE);
}

void CMyDrawView::OnUpdateEditRedo(CCmdUI* pCmdUI)
{
	BOOL bOn = TRUE;
	if (invoker->UnCommand.GetSize() <= 0)
		bOn = FALSE;
	pCmdUI->Enable(bOn);
}


BOOL CMyDrawView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	if (::GetKeyState(VK_CONTROL) >= 0)
	{
		return false;
	}

	float scale[] = { 0.1, 0.2, 0.5, 1, 2, 3, 5 };
	static int upLimit = 6;
	static int downLinit = 0;
	static int fUp = 1;
	static int fDown = -1;
	int n = 3;
	int i = 0;
	for (; i < 7; i++)
	{
		if (fabs(scale[i] - this->Zoom) < 1e-6)
		{
			n = i;
			break;
		}
	}
	if (i == 7)
		n = 3;

	if (zDelta > 0 && ++n <= upLimit)
	{
		this->Zoom = scale[n];
	}
	else if (zDelta < 0 && --n >= downLinit)
	{
		this->Zoom = scale[n];
	}

	ClearSelection(true);
	Invalidate();
	this->UpdateWindow();

	if (this->Zoom > 2.0)
		this->SetScrollSizes(MM_TEXT, CSize(6000 * 2, 1500 * 2));
	else
		this->SetScrollSizes(MM_TEXT, CSize(6000 * this->Zoom, 1500 * this->Zoom));

	switch (n)
	{
	case 0:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_10;
		break;

	case 1:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_20;
		break;

	case 2:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_50;
		break;

	case 3:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_100;
		break;

	case 4:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_200;
		break;

	case 5:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_300;
		break;

	case 6:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_500;
		break;

	default:
		theApp.m_nAppZOOM = ID_MENUITEM_ZOOM_10;
		break;
	}

	//显示百分比
	CMainFrame*pFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;//获得主窗口指标
	CMFCStatusBar*pStatus = &pFrame->m_wndStatusBar;//获得主窗口的状态栏指针
	if (pStatus)
	{
		CString str;
		str.Format("view: %d%%", int(Zoom * 100));//格式化文本
		pStatus->SetPaneText(2, str);
	}

	pFrame->ToolBarComboBoxButtonSetText();
	return CScrollView::OnMouseWheel(nFlags, zDelta, pt);
}


void CMyDrawView::OnPropertiesLock()
{
	m_bIsLocked = !m_bIsLocked;
	POSITION pos = m_ObjectList.GetHeadPosition();
	CGraph* pHitItem;
	while (pos != NULL)
	{
		pHitItem = (CGraph*)m_ObjectList.GetNext(pos);
		if (pHitItem->bIsSelected)
			pHitItem->bIsLocked = m_bIsLocked;
	}
}

void CMyDrawView::OnUpdatePropertiesLock(CCmdUI* pCmdUI)
{
	m_bIsLocked = false;
	POSITION pos = m_ObjectList.GetHeadPosition();
	CGraph* pHitItem;
	while (pos != NULL)
	{
		pHitItem = (CGraph*)m_ObjectList.GetNext(pos);
		if (pHitItem->bIsSelected)
		{
			if (pHitItem->bIsLocked)
			{
				m_bIsLocked = true;
				break;
			}
			else
				m_bIsLocked = false;
		}
	}


	CMainFrame *pMainFrame = (CMainFrame*)AfxGetApp()->GetMainWnd();
	int index = pMainFrame->m_myToolBar2.CommandToIndex(ID_BUTTON_Lock);
	if (m_bIsLocked)
		pMainFrame->m_myToolBar2.SetButtonInfo(index, ID_BUTTON_Lock, TBBS_BUTTON, 5);
	else
		pMainFrame->m_myToolBar2.SetButtonInfo(index, ID_BUTTON_Lock, TBBS_BUTTON, 4);

	pCmdUI->SetCheck(m_bIsLocked);

}

void CMyDrawView::OnButtonPreview()
{
	CMainFrame *pMainFrame = (CMainFrame*)AfxGetApp()->GetMainWnd();
	CMFCToolBarButton *bt = pMainFrame->m_myToolBar.GetButton(1);
	bt->EnableWindow(FALSE);
	bt->m_bUserButton = FALSE;
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	if (m_bIsEnablePrview == FALSE)
	{
		m_bIsEnablePrview = TRUE;
		pMainFrame->m_barText.EnableWindow(TRUE);
		int nPageNum;
		CString strPageNum;
		int nRecordNums = pMainFrame->pPrintDlg->m_RecordNumList.GetItemCount();
		pMainFrame->m_barText.GetWindowTextA(strPageNum);
		if (strPageNum.IsEmpty())
		{
			return;
		}
		nPageNum = _ttoi(strPageNum);
		if (nPageNum > nRecordNums || nPageNum <= 0)
		{
			return;
		}
		if (m_bIsEnablePrview)
		{
			POSITION pos = m_ObjectList.GetHeadPosition();
			while (pos != NULL)
			{
				CObject* pHitItem = (CObject*)m_ObjectList.GetNext(pos);
				if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
				{
					pText = (CText*)pHitItem;
					if (pText->nConstValueType == 0 && pText->m_nFieldIndex >= 0)
					{
						pText->m_strFieldDes = ProcessInsertData(pText->sInsetSqlParam, pText->m_strInitVal, true);
						pText->m_strRecordVal = pText->m_strFieldDes;
						for (int i = 0; i < nMaxColNum; i++)
						{
							CString fieldNum;
							fieldNum.Format(_T("%d"), i + 1);
							CString tmp = "[字段" + fieldNum + "]";
							CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
							//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
							CString sCodeString = pText->m_strRecordVal;
							//sCodeString = pText->MyText;
							sCodeString.Replace(tmp, sPrint);
							//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
							pText->MyText = sCodeString;
							pText->m_strRecordVal = sCodeString;
						}

					}
					if (pText->nConstValueType == 1)
					{
						pText->MyText = GetCounterText(pText->m_lTxtCounterStartNum, pText->m_lTxtCounterEndNum, pText->m_lTxtCounterStep, nPageNum);
						//if (pText->MyText.GetLength() > 0)
						//{
						//	pText->m_lTxtCounterStartNum = _ttol(_T(pText->MyText));
						//}
					}
					if (pText->nConstValueType == 2 && pText->m_nFieldIndex >= 0)
					{
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pText->m_nFieldIndex);
						if (pText->nStartPos >= 0 && pText->nStartPos < sPrint.GetLength() && pText->nCounts>0 && pText->nCounts <= sPrint.GetLength())
						{
							sPrint = sPrint.Mid(pText->nStartPos, pText->nCounts);
						}
						pText->MyText = sPrint;
					}
				}
				else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
				{
					pBarCode = (CBarCode*)pHitItem;
					if (pBarCode->m_nBarPropertyType == 0 && pBarCode->m_nFieldIndex >= 0)
					{
						pBarCode->m_strFieldDes = ProcessInsertData(pBarCode->sInsetParam, pBarCode->m_strInitVal, true);
						pBarCode->m_strRecordVal = pBarCode->m_strFieldDes;
						for (int i = 0; i < nMaxColNum; i++)
						{
							CString fieldNum;
							fieldNum.Format(_T("%d"), i + 1);
							CString tmp = "[字段" + fieldNum + "]";
							CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
							//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
							CString sCodeString = pBarCode->m_strRecordVal;
							//sCodeString = pText->MyText;
							sCodeString.Replace(tmp, sPrint);
							//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
							pBarCode->setCodeString(sCodeString);
							pBarCode->m_strRecordVal = sCodeString;
						}
					}
					if (pBarCode->m_nBarPropertyType == 1)
					{
						CString strCountTmp = GetCounterText(pBarCode->nCounterStart, pBarCode->nCounterEnd, pBarCode->nCounterStep, nPageNum);
						pBarCode->setCodeString(strCountTmp);
						//if (strCountTmp.GetLength() > 0)
						//{
						//	pBarCode->nCounterStart = _ttol(_T(strCountTmp));
						//}
					}
					if (pBarCode->m_nBarPropertyType == 2 && pBarCode->m_nFieldIndex >= 0)
					{
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(0, pBarCode->m_nSqlTypeFieldIndex);
						if (pBarCode->nStartPos >= 0 && pBarCode->nStartPos < sPrint.GetLength() && pBarCode->nCounts>0 && pBarCode->nCounts <= sPrint.GetLength())
						{
							sPrint = sPrint.Mid(pBarCode->nStartPos, pBarCode->nCounts);
						}
						pBarCode->setCodeString(sPrint);
					}
				}
				else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
				{
					pTBarCode11 = (CTBarCode11*)pHitItem;
					if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->m_nFieldIndex >= 0)
					{
						pTBarCode11->m_strFieldDes = ProcessInsertData(pTBarCode11->sInsetParam, pTBarCode11->m_strInitVal, true);
						pTBarCode11->m_strRecordVal = pTBarCode11->m_strFieldDes;
						for (int i = 0; i < nMaxColNum; i++)
						{
							CString fieldNum;
							fieldNum.Format(_T("%d"), i + 1);
							CString tmp = "[字段" + fieldNum + "]";
							CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
							//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
							CString sCodeString = pTBarCode11->m_strRecordVal;
							//sCodeString = pText->MyText;
							sCodeString.Replace(tmp, sPrint);
							//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
							pTBarCode11->SetData(sCodeString);
							pTBarCode11->m_strRecordVal = sCodeString;
						}
					}
					if (pTBarCode11->m_nBarPropertyType == 1)
					{
						CString strCountTmp = GetCounterText(pTBarCode11->nCounterStart, pTBarCode11->nCounterEnd, pTBarCode11->nCounterStep, nPageNum);
						pTBarCode11->SetData(strCountTmp);
						//if (strCountTmp.GetLength() > 0)
						//{
						//	pTBarCode11->nCounterStart = _ttol(_T(strCountTmp));
						//}
					}
					if (pTBarCode11->m_nBarPropertyType == 2 && pTBarCode11->m_nFieldIndex >= 0)
					{
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(0, pTBarCode11->m_nSqlTypeFieldIndex);
						if (pTBarCode11->nStartPos >= 0 && pTBarCode11->nStartPos < sPrint.GetLength() && pTBarCode11->nCounts>0 && pTBarCode11->nCounts <= sPrint.GetLength())
						{
							sPrint = sPrint.Mid(pTBarCode11->nStartPos, pTBarCode11->nCounts);
						}
						pTBarCode11->SetData(sPrint);
					}
				}
			}
			Invalidate(FALSE);
		}
	}
	else
	{
		m_bIsEnablePrview = FALSE;
		pMainFrame->m_barText.EnableWindow(FALSE);

		POSITION pos = m_ObjectList.GetHeadPosition();
		while (pos != NULL)
		{
			CObject* pHitItem = (CObject*)m_ObjectList.GetNext(pos);
			if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
			{
				pText = (CText*)pHitItem;
				pText->MyText = pText->m_strInitVal;
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
			{
				pBarCode = (CBarCode*)pHitItem;
				pBarCode->setCodeString(pBarCode->m_strInitVal);
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
			{
				pTBarCode11 = (CTBarCode11*)pHitItem;
				pTBarCode11->SetData(pTBarCode11->m_strInitVal);
			}
		}
		Invalidate(FALSE);
	}
}
void CMyDrawView::OnButtonHomePage()
{
	CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	if (m_bIsEnablePrview)
	{
		POSITION pos = m_ObjectList.GetHeadPosition();
		while (pos != NULL)
		{
			CObject* pHitItem = (CObject*)m_ObjectList.GetNext(pos);
			if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
			{
				pText = (CText*)pHitItem;
				pMainFrame->m_barText.SetWindowTextA("1");
				if (pText->nConstValueType == 0 && pText->m_nFieldIndex >= 0)
				{
					pText->m_strFieldDes = ProcessInsertData(pText->sInsetSqlParam, pText->m_strInitVal, true);
					pText->m_strRecordVal = pText->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(0, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pText->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pText->MyText = sCodeString;
						pText->m_strRecordVal = sCodeString;
					}
				}
				if (pText->nConstValueType == 1)
				{
					pText->MyText = GetCounterText(pText->m_lTxtCounterStartNum, pText->m_lTxtCounterEndNum, pText->m_lTxtCounterStep, 1);
					//if (pText->MyText.GetLength() > 0)
					//{
					//	pText->m_lTxtCounterStartNum = _ttol(_T(pText->MyText));
					//}
				}
				if (pText->nConstValueType == 2 && pText->m_nFieldIndex >= 0)
				{

					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(0, pText->m_nFieldIndex);
					if (pText->nStartPos >= 0 && pText->nStartPos < sPrint.GetLength() && pText->nCounts>0 && pText->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pText->nStartPos, pText->nCounts);
					}
					pText->MyText = sPrint;
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
			{
				pBarCode = (CBarCode*)pHitItem;
				if (pBarCode->m_nBarPropertyType == 0 && pBarCode->m_nFieldIndex >= 0)
				{
					pBarCode->m_strFieldDes = ProcessInsertData(pBarCode->sInsetParam, pBarCode->m_strInitVal, true);
					pBarCode->m_strRecordVal = pBarCode->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(0, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pBarCode->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pBarCode->setCodeString(sCodeString);
						pBarCode->m_strRecordVal = sCodeString;
					}
				}
				if (pBarCode->m_nBarPropertyType == 1)
				{
					CString strCountTmp = GetCounterText(pBarCode->nCounterStart, pBarCode->nCounterEnd, pBarCode->nCounterStep, 1);
					pBarCode->setCodeString(strCountTmp);
					//if (strCountTmp.GetLength() > 0)
					//{
					//	pBarCode->nCounterStart = _ttol(_T(strCountTmp));
					//}
				}
				if (pBarCode->m_nBarPropertyType == 2 && pBarCode->m_nFieldIndex >= 0)
				{
					pMainFrame->m_barText.SetWindowTextA("1");
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(0, pBarCode->m_nSqlTypeFieldIndex);
					if (pBarCode->nStartPos >= 0 && pBarCode->nStartPos < sPrint.GetLength() && pBarCode->nCounts>0 && pBarCode->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pBarCode->nStartPos, pBarCode->nCounts);
					}
					pBarCode->setCodeString(sPrint);
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
			{
				pTBarCode11 = (CTBarCode11*)pHitItem;
				if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->m_nFieldIndex >= 0)
				{
					pTBarCode11->m_strFieldDes = ProcessInsertData(pTBarCode11->sInsetParam, pTBarCode11->m_strInitVal, true);
					pTBarCode11->m_strRecordVal = pTBarCode11->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(0, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pTBarCode11->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pTBarCode11->SetData(sCodeString);
						pTBarCode11->m_strRecordVal = sCodeString;
					}
				}
				if (pTBarCode11->m_nBarPropertyType == 1)
				{
					CString strCountTmp = GetCounterText(pTBarCode11->nCounterStart, pTBarCode11->nCounterEnd, pTBarCode11->nCounterStep, 1);
					pTBarCode11->SetData(strCountTmp);
					//if (strCountTmp.GetLength() > 0)
					//{
					//	pTBarCode11->nCounterStart = _ttol(_T(strCountTmp));
					//}
				}
				if (pTBarCode11->m_nBarPropertyType == 2 && pTBarCode11->m_nFieldIndex >= 0)
				{
					pMainFrame->m_barText.SetWindowTextA("1");
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(0, pTBarCode11->m_nSqlTypeFieldIndex);
					if (pTBarCode11->nStartPos >= 0 && pTBarCode11->nStartPos < sPrint.GetLength() && pTBarCode11->nCounts>0 && pTBarCode11->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pTBarCode11->nStartPos, pTBarCode11->nCounts);
					}
					pTBarCode11->SetData(sPrint);
				}
			}
		}
		Invalidate(FALSE);
	}
}
void CMyDrawView::OnButtonNextPageBefore()
{
	CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	int nPageNum;
	CString strPageNum;
	int nRecordNums = pMainFrame->pPrintDlg->m_RecordNumList.GetItemCount();
	pMainFrame->m_barText.GetWindowTextA(strPageNum);
	if (strPageNum.IsEmpty())
	{
		return;
	}
	nPageNum = _ttoi(strPageNum);
	//strPageNum.Format(_T("%d"), nPageNum);
	if (nPageNum == 1)
	{
		return;
	}
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	if (m_bIsEnablePrview)
	{
		nPageNum--;
		strPageNum.Format(_T("%d"), nPageNum);
		pMainFrame->m_barText.SetWindowTextA(strPageNum);
		POSITION pos = m_ObjectList.GetHeadPosition();
		while (pos != NULL)
		{
			CObject* pHitItem = (CObject*)m_ObjectList.GetNext(pos);
			if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
			{
				pText = (CText*)pHitItem;
				if (pText->nConstValueType == 0 && pText->m_nFieldIndex >= 0)
				{
					pText->m_strFieldDes = ProcessInsertData(pText->sInsetSqlParam, pText->m_strInitVal, true);
					pText->m_strRecordVal = pText->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pText->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pText->MyText = sCodeString;
						pText->m_strRecordVal = sCodeString;
					}
				}
				if (pText->nConstValueType == 1)
				{
					pText->MyText = GetCounterText(pText->m_lTxtCounterStartNum, pText->m_lTxtCounterEndNum, pText->m_lTxtCounterStep, nPageNum);
					//if (pText->MyText.GetLength() > 0)
					//{
					//	pText->m_lTxtCounterStartNum = _ttol(_T(pText->MyText));
					//}
				}
				if (pText->nConstValueType == 2 && pText->m_nFieldIndex >= 0)
				{
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pText->m_nFieldIndex);
					if (pText->nStartPos >= 0 && pText->nStartPos < sPrint.GetLength() && pText->nCounts>0 && pText->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pText->nStartPos, pText->nCounts);
					}
					pText->MyText = sPrint;
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
			{
				pBarCode = (CBarCode*)pHitItem;
				if (pBarCode->m_nBarPropertyType == 0 && pBarCode->m_nFieldIndex >= 0)
				{
					pBarCode->m_strFieldDes = ProcessInsertData(pBarCode->sInsetParam, pBarCode->m_strInitVal, true);
					pBarCode->m_strRecordVal = pBarCode->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pBarCode->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pBarCode->setCodeString(sCodeString);
						pBarCode->m_strRecordVal = sCodeString;
					}
				}
				if (pBarCode->m_nBarPropertyType == 2 && pBarCode->m_nFieldIndex >= 0)
				{
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pBarCode->m_nSqlTypeFieldIndex);
					if (pBarCode->nStartPos >= 0 && pBarCode->nStartPos < sPrint.GetLength() && pBarCode->nCounts>0 && pBarCode->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pBarCode->nStartPos, pBarCode->nCounts);
					}
					pBarCode->setCodeString(sPrint);
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
			{
				pTBarCode11 = (CTBarCode11*)pHitItem;
				if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->m_nFieldIndex >= 0)
				{
					pTBarCode11->m_strFieldDes = ProcessInsertData(pTBarCode11->sInsetParam, pTBarCode11->m_strInitVal, true);
					pTBarCode11->m_strRecordVal = pTBarCode11->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pTBarCode11->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pTBarCode11->SetData(sCodeString);
						pTBarCode11->m_strRecordVal = sCodeString;
					}
				}
				if (pTBarCode11->m_nBarPropertyType == 1)
				{
					CString strCountTmp = GetCounterText(pTBarCode11->nCounterStart, pTBarCode11->nCounterEnd, pTBarCode11->nCounterStep, nPageNum);
					pTBarCode11->SetData(strCountTmp);
					//if (strCountTmp.GetLength() > 0)
					//{
					//	pTBarCode11->nCounterStart = _ttol(_T(strCountTmp));
					//}
				}
				if (pTBarCode11->m_nBarPropertyType == 2 && pTBarCode11->m_nFieldIndex >= 0)
				{
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pTBarCode11->m_nSqlTypeFieldIndex);
					if (pTBarCode11->nStartPos >= 0 && pTBarCode11->nStartPos < sPrint.GetLength() && pTBarCode11->nCounts>0 && pTBarCode11->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pTBarCode11->nStartPos, pTBarCode11->nCounts);
					}
					pTBarCode11->SetData(sPrint);
				}
			}
		}
		Invalidate(FALSE);
	}
}
void CMyDrawView::OnButtonNextPage()
{
	CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	int nPageNum;
	CString strPageNum;
	int nRecordNums = pMainFrame->pPrintDlg->m_RecordNumList.GetItemCount();
	pMainFrame->m_barText.GetWindowTextA(strPageNum);
	if (strPageNum.IsEmpty())
	{
		return;
	}
	nPageNum = _ttoi(strPageNum);
	//strPageNum.Format(_T("%d"), nPageNum);
	if (nPageNum == nRecordNums)
	{
		return;
	}
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	if (m_bIsEnablePrview)
	{
		nPageNum++;
		strPageNum.Format(_T("%d"), nPageNum);
		pMainFrame->m_barText.SetWindowTextA(strPageNum);
		POSITION pos = m_ObjectList.GetHeadPosition();
		while (pos != NULL)
		{
			CObject* pHitItem = (CObject*)m_ObjectList.GetNext(pos);
			if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
			{
				pText = (CText*)pHitItem;
				if (pText->nConstValueType == 0 && pText->m_nFieldIndex >= 0)
				{
					pText->m_strFieldDes = ProcessInsertData(pText->sInsetSqlParam, pText->m_strInitVal, true);
					pText->m_strRecordVal = pText->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pText->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pText->MyText = sCodeString;
						pText->m_strRecordVal = sCodeString;
					}
				}
				if (pText->nConstValueType == 1)
				{
					pText->MyText = GetCounterText(pText->m_lTxtCounterStartNum, pText->m_lTxtCounterEndNum, pText->m_lTxtCounterStep, nPageNum);
					//if (pText->MyText.GetLength() > 0)
					//{
					//	pText->m_lTxtCounterStartNum = _ttol(_T(pText->MyText));
					//}
				}
				if (pText->nConstValueType == 2 && pText->m_nFieldIndex >= 0)
				{

					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pText->m_nFieldIndex);
					if (pText->nStartPos >= 0 && pText->nStartPos < sPrint.GetLength() && pText->nCounts>0 && pText->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pText->nStartPos, pText->nCounts);
					}
					pText->MyText = sPrint;
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
			{
				pBarCode = (CBarCode*)pHitItem;
				if (pBarCode->m_nBarPropertyType == 0 && pBarCode->m_nFieldIndex >= 0)
				{
					pBarCode->m_strFieldDes = ProcessInsertData(pBarCode->sInsetParam, pBarCode->m_strInitVal, true);
					pBarCode->m_strRecordVal = pBarCode->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pBarCode->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pBarCode->setCodeString(sCodeString);
						pBarCode->m_strRecordVal = sCodeString;
					}
				}
				if (pBarCode->m_nBarPropertyType == 2 && pBarCode->m_nFieldIndex >= 0)
				{
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pBarCode->m_nSqlTypeFieldIndex);
					if (pBarCode->nStartPos >= 0 && pBarCode->nStartPos < sPrint.GetLength() && pBarCode->nCounts>0 && pBarCode->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pBarCode->nStartPos, pBarCode->nCounts);
					}
					pBarCode->setCodeString(sPrint);
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
			{
				pTBarCode11 = (CTBarCode11*)pHitItem;
				if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->m_nFieldIndex >= 0)
				{
					pTBarCode11->m_strFieldDes = ProcessInsertData(pTBarCode11->sInsetParam, pTBarCode11->m_strInitVal, true);
					pTBarCode11->m_strRecordVal = pTBarCode11->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pTBarCode11->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pTBarCode11->SetData(sCodeString);
						pTBarCode11->m_strRecordVal = sCodeString;
					}
				}
				if (pTBarCode11->m_nBarPropertyType == 1)
				{
					CString strCountTmp = GetCounterText(pTBarCode11->nCounterStart, pTBarCode11->nCounterEnd, pTBarCode11->nCounterStep, nPageNum);
					pTBarCode11->SetData(strCountTmp);
					//if (strCountTmp.GetLength() > 0)
					//{
					//	pTBarCode11->nCounterStart = _ttol(_T(strCountTmp));
					//}
				}
				if (pTBarCode11->m_nBarPropertyType == 2 && pTBarCode11->m_nFieldIndex >= 0)
				{
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pTBarCode11->m_nSqlTypeFieldIndex);
					if (pTBarCode11->nStartPos >= 0 && pTBarCode11->nStartPos < sPrint.GetLength() && pTBarCode11->nCounts>0 && pTBarCode11->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pTBarCode11->nStartPos, pTBarCode11->nCounts);
					}
					pTBarCode11->SetData(sPrint);
				}
			}
		}
		Invalidate(FALSE);
	}
}
void CMyDrawView::OnButtonTailPage()
{
	CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CString strPageNum;
	if (m_bIsEnablePrview)
	{
		POSITION pos = m_ObjectList.GetHeadPosition();
		int nDataCount = pMainFrame->pPrintDlg->m_RecordNumList.GetItemCount();
		if (nDataCount <= 0)
		{
			return;
		}
		strPageNum.Format(_T("%d"), nDataCount);
		pMainFrame->m_barText.SetWindowTextA(strPageNum);
		while (pos != NULL)
		{
			CObject* pHitItem = (CObject*)m_ObjectList.GetNext(pos);
			if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
			{
				pText = (CText*)pHitItem;
				if (pText->nConstValueType == 0 && pText->m_nFieldIndex >= 0)
				{
					pText->m_strFieldDes = ProcessInsertData(pText->sInsetSqlParam, pText->m_strInitVal, true);
					pText->m_strRecordVal = pText->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nDataCount - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pText->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pText->MyText = sCodeString;
						pText->m_strRecordVal = sCodeString;
					}
				}
				if (pText->nConstValueType == 1)
				{
					pText->MyText = GetCounterText(pText->m_lTxtCounterStartNum, pText->m_lTxtCounterEndNum, pText->m_lTxtCounterStep, nDataCount);
					//if (pText->MyText.GetLength() > 0)
					//{
					//	pText->m_lTxtCounterStartNum = _ttol(_T(pText->MyText));
					//}
				}
				if (pText->nConstValueType == 2 && pText->m_nFieldIndex >= 0)
				{
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nDataCount - 1, pText->m_nFieldIndex);
					if (pText->nStartPos >= 0 && pText->nStartPos < sPrint.GetLength() && pText->nCounts>0 && pText->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pText->nStartPos, pText->nCounts);
					}
					pText->MyText = sPrint;
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
			{
				pBarCode = (CBarCode*)pHitItem;
				if (pBarCode->m_nBarPropertyType == 0 && pBarCode->m_nFieldIndex >= 0)
				{
					pBarCode->m_strFieldDes = ProcessInsertData(pBarCode->sInsetParam, pBarCode->m_strInitVal, true);
					pBarCode->m_strRecordVal = pBarCode->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nDataCount - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pBarCode->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pBarCode->setCodeString(sCodeString);
						pBarCode->m_strRecordVal = sCodeString;
					}
				}
				if (pBarCode->m_nBarPropertyType == 2 && pBarCode->m_nFieldIndex >= 0)
				{
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nDataCount - 1, pBarCode->m_nSqlTypeFieldIndex);
					if (pBarCode->nStartPos >= 0 && pBarCode->nStartPos < sPrint.GetLength() && pBarCode->nCounts>0 && pBarCode->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pBarCode->nStartPos, pBarCode->nCounts);
					}
					pBarCode->setCodeString(sPrint);
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
			{
				pTBarCode11 = (CTBarCode11*)pHitItem;
				if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->m_nFieldIndex >= 0)
				{
					pTBarCode11->m_strFieldDes = ProcessInsertData(pTBarCode11->sInsetParam, pTBarCode11->m_strInitVal, true);
					pTBarCode11->m_strRecordVal = pTBarCode11->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nDataCount - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pTBarCode11->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pTBarCode11->SetData(sCodeString);
						pTBarCode11->m_strRecordVal = sCodeString;
					}
				}
				if (pTBarCode11->m_nBarPropertyType == 1)
				{
					CString strCountTmp = GetCounterText(pTBarCode11->nCounterStart, pTBarCode11->nCounterEnd, pTBarCode11->nCounterStep, nDataCount);
					pTBarCode11->SetData(strCountTmp);
					//if (strCountTmp.GetLength() > 0)
					//{
					//	pTBarCode11->nCounterStart = _ttol(_T(strCountTmp));
					//}
				}
				if (pTBarCode11->m_nBarPropertyType == 2 && pTBarCode11->m_nFieldIndex >= 0)
				{
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nDataCount - 1, pTBarCode11->m_nSqlTypeFieldIndex);
					if (pTBarCode11->nStartPos >= 0 && pTBarCode11->nStartPos < sPrint.GetLength() && pTBarCode11->nCounts>0 && pTBarCode11->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pTBarCode11->nStartPos, pTBarCode11->nCounts);
					}
					pTBarCode11->SetData(sPrint);
				}
			}
		}
		Invalidate(FALSE);
	}
}

void CMyDrawView::OnUpdatePreview(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bIsEnablePrview);
}
void CMyDrawView::OnPageNumChange()
{
	CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
	DWORD   dwStyle = pMainFrame->m_barText.GetStyle();

	dwStyle |= ES_NUMBER;
	SetWindowLong(pMainFrame->m_barText.m_hWnd, GWL_STYLE, dwStyle);

	int nPageNum;
	CString strPageNum;
	int nRecordNums = pMainFrame->pPrintDlg->m_RecordNumList.GetItemCount();
	pMainFrame->m_barText.GetWindowTextA(strPageNum);
	if (strPageNum.IsEmpty())
	{
		return;
	}
	nPageNum = _ttoi(strPageNum);
	if (nPageNum > nRecordNums || nPageNum <= 0)
	{
		return;
	}
	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode = NULL;
	if (m_bIsEnablePrview)
	{
		POSITION pos = m_ObjectList.GetHeadPosition();
		while (pos != NULL)
		{
			CObject* pHitItem = (CObject*)m_ObjectList.GetNext(pos);
			if (pHitItem->IsKindOf(RUNTIME_CLASS(CText)))
			{
				pText = (CText*)pHitItem;
				if (pText->nConstValueType == 0 && pText->m_nFieldIndex >= 0)
				{
					pText->m_strFieldDes = ProcessInsertData(pText->sInsetSqlParam, pText->m_strInitVal, true);
					pText->m_strRecordVal = pText->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pText->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pText->MyText = sCodeString;
						pText->m_strRecordVal = sCodeString;
					}
				}
				if (pText->nConstValueType == 1)
				{
					pText->MyText = GetCounterText(pText->m_lTxtCounterStartNum, pText->m_lTxtCounterEndNum, pText->m_lTxtCounterStep, nPageNum);
					//if (pText->MyText.GetLength() > 0)
					//{
					//	pText->m_lTxtCounterStartNum = _ttol(_T(pText->MyText));
					//}
				}
				if (pText->nConstValueType == 2 && pText->m_nFieldIndex >= 0)
				{
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pText->m_nFieldIndex);
					if (pText->nStartPos >= 0 && pText->nStartPos < sPrint.GetLength() && pText->nCounts>0 && pText->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pText->nStartPos, pText->nCounts);
					}
					pText->MyText = sPrint;
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CBarCode)))
			{
				pBarCode = (CBarCode*)pHitItem;
				if (pBarCode->m_nBarPropertyType == 0 && pBarCode->m_nFieldIndex >= 0)
				{
					pBarCode->m_strFieldDes = ProcessInsertData(pBarCode->sInsetParam, pBarCode->m_strInitVal, true);
					pBarCode->m_strRecordVal = pBarCode->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pBarCode->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pBarCode->setCodeString(sCodeString);
						pBarCode->m_strRecordVal = sCodeString;
					}
				}
				if (pBarCode->m_nBarPropertyType == 2 && pBarCode->m_nFieldIndex >= 0)
				{
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pBarCode->m_nSqlTypeFieldIndex);
					if (pBarCode->nStartPos >= 0 && pBarCode->nStartPos < sPrint.GetLength() && pBarCode->nCounts>0 && pBarCode->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pBarCode->nStartPos, pBarCode->nCounts);
					}
					pBarCode->setCodeString(sPrint);
				}
			}
			else if (pHitItem->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
			{
				pTBarCode = (CTBarCode11*)pHitItem;
				if (pTBarCode->m_nBarPropertyType == 0 && pTBarCode->m_nFieldIndex >= 0)
				{
					pTBarCode->m_strFieldDes = ProcessInsertData(pTBarCode->sInsetParam, pTBarCode->m_strInitVal, true);
					pTBarCode->m_strRecordVal = pTBarCode->m_strFieldDes;
					for (int i = 0; i < nMaxColNum; i++)
					{
						CString fieldNum;
						fieldNum.Format(_T("%d"), i + 1);
						CString tmp = "[字段" + fieldNum + "]";
						CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, i + 1);
						//pText->sInsetSqlParam = ReplaceFieldNumToSring(pText->sInsetSqlParam, pText->m_nFieldIndex, sPrint);
						CString sCodeString = pTBarCode->m_strRecordVal;
						//sCodeString = pText->MyText;
						sCodeString.Replace(tmp, sPrint);
						//sCodeString = ProcessInsertData(pText->sInsetSqlParam, sCodeString, FALSE);
						pTBarCode->SetData(sCodeString);
						pTBarCode->m_strRecordVal = sCodeString;
					}
				}
				if (pTBarCode->m_nBarPropertyType == 2 && pTBarCode->m_nFieldIndex >= 0)
				{
					CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(nPageNum - 1, pTBarCode->m_nSqlTypeFieldIndex);
					if (pTBarCode->nStartPos >= 0 && pTBarCode->nStartPos < sPrint.GetLength() && pTBarCode->nCounts>0 && pTBarCode->nCounts <= sPrint.GetLength())
					{
						sPrint = sPrint.Mid(pTBarCode->nStartPos, pTBarCode->nCounts);
					}
					pTBarCode->SetData(sPrint);
				}
			}
		}
		Invalidate(FALSE);
	}
}




void CMyDrawView::UnInitPropList()
{
	CMFCPropertyGridProperty * pProp;
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

	pFrame->m_wndProperties.graphs.RemoveAll();

	pFrame->m_wndProperties.m_wndObjectCombo.ResetContent();
	pFrame->m_wndProperties.m_wndObjectCombo.AddString(_T("未选择"));
	pFrame->m_wndProperties.m_wndObjectCombo.SetCurSel(0);

	
	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(0);
	//读取数据
	CString itemname;
	itemname = pProp->GetName(); //获取名称
	COleVariant itemvalue;
	itemvalue = pProp->GetValue();//获取值
	//写入数据
	pProp->SetValue((_variant_t)(_T("0.0")));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(1);
	pProp->SetValue((_variant_t)(_T("0.0")));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(0);
	pProp->SetValue((_variant_t)(_T("0.0")));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(1);
	pProp->SetValue((_variant_t)(_T("0.0")));



	LOGFONT lf;
	CFont* font = CFont::FromHandle((HFONT)GetStockObject(DEFAULT_GUI_FONT));
	font->GetLogFont(&lf);

	lstrcpy(lf.lfFaceName, _T("宋体, Arial"));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(2)->GetSubItem(0);
	CMFCPropertyGridFontPropertyEx * pProp2 = (CMFCPropertyGridFontPropertyEx*)pProp;
	if (pProp2 != NULL)
	{
		pProp2->SetFont(lf);
		pProp2->SetName("字体");
	}

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(3)->GetSubItem(0);
	pProp->SetValue((_variant_t)(_T(" ")));

}

