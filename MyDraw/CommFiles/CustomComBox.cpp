// CustomComBox.cpp : implementation file
//

#include "../stdafx.h"
#include "../MyDraw.h"
#include "CustomComBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCustomComBox

CCustomComBox::CCustomComBox()
{
	for (int i=0;i< CMBMAXITEM;i++)
	{
		nColorFalg[i] = 0;
	}
}

CCustomComBox::~CCustomComBox()
{
}


BEGIN_MESSAGE_MAP(CCustomComBox, CComboBox)
	//{{AFX_MSG_MAP(CCustomComBox)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCustomComBox message handlers

void CCustomComBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{

	if ( (long)lpDrawItemStruct->itemID == -1 )
		return;
	
	//	int n = lpDrawItemStruct->itemID;
	
	CDC* dc = CDC::FromHandle(lpDrawItemStruct->hDC);
	CRect rcItem(lpDrawItemStruct->rcItem);
	CString strItem;
	GetLBText(lpDrawItemStruct->itemID, strItem);
	
	dc->SetBkMode(TRANSPARENT);


	
	BOOL bSelected = (lpDrawItemStruct->itemState & ODS_SELECTED) ? TRUE : FALSE;
	if ( bSelected )
	{
		dc->FillSolidRect(rcItem, RGB(200, 200, 238));
		dc->SetTextColor(RGB(100, 100, 100));
	}
	else if (nColorFalg[lpDrawItemStruct->itemID] == 0)
	{
		dc->FillSolidRect(rcItem, RGB(150, 150, 150));
		dc->SetTextColor(RGB(255, 255, 255));
	}
	else if (nColorFalg[lpDrawItemStruct->itemID] == 1)
	{
		dc->FillSolidRect(rcItem, RGB(255, 0, 0));
		dc->SetTextColor(RGB(255, 255, 255));
	}
	else if (nColorFalg[lpDrawItemStruct->itemID] == 2)
	{
		dc->FillSolidRect(rcItem, RGB(0,255, 0));
		dc->SetTextColor(RGB(255, 255, 255));
	}
	else if (nColorFalg[lpDrawItemStruct->itemID] == 3)
	{
		dc->FillSolidRect(rcItem, RGB(0,255, 255));
		dc->SetTextColor(RGB(255, 255, 255));	
	}
	else
	{
		dc->FillSolidRect(rcItem, RGB(0,255, 255));
		dc->SetTextColor(RGB(255, 255, 255));
	}
	
	dc->DrawText(strItem, strItem.GetLength(), rcItem, DT_LEFT | DT_SINGLELINE);

}


BOOL CCustomComBox::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID) 
{
	// TODO: Add your specialized code here and/or call the base class
	dwStyle &= ~(CBS_SIMPLE | CBS_DROPDOWN); // 0xF;
    //dwStyle |= CBS_DROPDOWNLIST;
	dwStyle |= CBS_DROPDOWN;
	
    // Make sure to use the CBS_OWNERDRAWVARIABLE style
    dwStyle |= CBS_OWNERDRAWVARIABLE;

	
    return CComboBox::Create(dwStyle, rect, pParentWnd, nID);
	//	return CWnd::Create(dwStyle, rect, pParentWnd, nID;
}

void CCustomComBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
{
	// TODO: Add your code to determine the size of specified item
	if ( (long)lpMeasureItemStruct->itemID == -1 )
		return;
		
}


void CCustomComBox::ClearColor()
{
	for (int i=0;i< CMBMAXITEM;i++)
	{
		nColorFalg[i] = 0;
	}

}


void CCustomComBox::SetColor(int nRow,int nColor)
{
	if (nRow >= CMBMAXITEM)
	{
		return;
	}
	
	nColorFalg[nRow] = nColor;
}