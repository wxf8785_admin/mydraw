// Ellipse.h: interface for the CEllipse class.
//
//////////////////////////////////////////////////////////////////////
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEllipse :public CGraph
{
//    DECLARE_DYNAMIC(CEllipse)
    DECLARE_SERIAL(CEllipse)
public:
	//int startX;
	//int startY;
	//int endX;
	//int endY;
	LOGPEN LinePen;
	LOGBRUSH MyBrush;

	CEllipse();
	virtual ~CEllipse();
	void Serialize(CArchive & ar);
	virtual CGraph* Clone();
	virtual void  InitPropList();
};
