// Pic.h: interface for the CPic class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PIC_H__5689A71A_22DA_4F41_9FA2_A092B9E5FF5A__INCLUDED_)
#define AFX_PIC_H__5689A71A_22DA_4F41_9FA2_A092B9E5FF5A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CPic : public CGraph
{
	 DECLARE_SERIAL(CPic)
public:
	CPic();
	virtual ~CPic();

public:
	//int startX;
	//int startY;
	int nWidth;
	int nHeight;


	bool    isStaticPic;//�Ƿ�Ϊ��̬ͼƬ
	CString m_PicStaticName;//ͼƬ��
	CString m_PicStaticPathName;//·��
	CString m_PicDirectory;//��̬ʱĿ¼
	int m_PicFieldNum;//�ֶ�
	int nPicRotateAngle;//��ת�Ƕ�
	bool m_isFixScale; //�Ƿ���������

	void Serialize(CArchive & ar);
	virtual CGraph* Clone();
	virtual void  InitPropList();

};

#endif // !defined(AFX_PIC_H__5689A71A_22DA_4F41_9FA2_A092B9E5FF5A__INCLUDED_)
