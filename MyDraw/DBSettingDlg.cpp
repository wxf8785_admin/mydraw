// DBSettingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MyDraw.h"
#include "DBSettingDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDBSettingDlg dialog


CDBSettingDlg::CDBSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDBSettingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDBSettingDlg)
	m_OtherCharValueCor = 44;
	m_FieldNumConstCord = 1;
	m_FieldNumConstSep = 44;
	m_nCharcterStringCord = _T(",");
	m_nCharcterStringSep = _T(",");
	//}}AFX_DATA_INIT
}


void CDBSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDBSettingDlg)
	DDX_Text(pDX, IDC_EDIT_OTHER_CHAR_COR, m_OtherCharValueCor);
	DDV_MinMaxInt(pDX, m_OtherCharValueCor, 1, 255);
	DDX_Text(pDX, IDC_EDIT_FIELD_NUM_CONST, m_FieldNumConstCord);
	DDV_MinMaxInt(pDX, m_FieldNumConstCord, 1, 255);
	DDX_Text(pDX, IDC_EDIT_OTHER_CHAR_SEP, m_FieldNumConstSep);
	DDV_MinMaxInt(pDX, m_FieldNumConstSep, 1, 255);
	DDX_Text(pDX, IDC_EDIT_CHAR_C_CORD, m_nCharcterStringCord);
	DDX_Text(pDX, IDC_EDIT_CHAR_C_SEP, m_nCharcterStringSep);
	DDV_MaxChars(pDX, m_nCharcterStringSep, 255);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDBSettingDlg, CDialog)
	//{{AFX_MSG_MAP(CDBSettingDlg)
	ON_BN_CLICKED(IDC_RADIO_CONST_FIELD, OnRadioConstField)
	ON_BN_CLICKED(IDC_RADIO_ENTER_RECORD, OnRadioEnterRecord)
	ON_BN_CLICKED(IDC_RADIO_OTHER_RECORD, OnRadioOtherRecord)
	ON_BN_CLICKED(IDC_RADIO_ENTER_SEPRATE, OnRadioEnterSeprate)
	ON_BN_CLICKED(IDC_RADIO_OTHER_SEPRATE, OnRadioOtherSeprate)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_CHARTER_OTHER_SORD, OnDeltaposSpinCharterOtherSord)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_FIELD_NUM_SEP, OnDeltaposSpinFieldNumSep)
	ON_BN_CLICKED(IDC_RADIO_USE_SEPRATE, OnRadioUseSeprate)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_OTHER_CHAR_SEP, OnDeltaposSpinOtherCharSep)
	//}}AFX_MSG_MAP
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDBSettingDlg message handlers

BOOL CDBSettingDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_USE_SEPRATE);
	pBtn->SetCheck(1);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_RECORD);
	pBtn->SetCheck(1);
	pBtn =  (CButton*)GetDlgItem(IDC_RADIO_OTHER_RECORD);
	pBtn->SetCheck(0);

//	IDC_EDIT_OTHER_CHAR_COR
	GetDlgItem(IDC_EDIT_OTHER_CHAR_COR)->EnableWindow(true);
	GetDlgItem(IDC_SPIN_CHARTER_OTHER_SORD)->EnableWindow(true);



	pBtn =  (CButton*)GetDlgItem(IDC_RADIO_CONST_FIELD);
	pBtn->SetCheck(0);
	GetDlgItem(IDC_EDIT_FIELD_NUM_CONST)->EnableWindow(false);
	GetDlgItem(IDC_SPIN_FIELD_NUM_SEP)->EnableWindow(false);



	pBtn =  (CButton*)GetDlgItem(IDC_RADIO_ENTER_SEPRATE);
	pBtn->SetCheck(0);
	pBtn =  (CButton*)GetDlgItem(IDC_RADIO_OTHER_SEPRATE);
	pBtn->SetCheck(1);
	GetDlgItem(IDC_EDIT_OTHER_CHAR_SEP)->EnableWindow(true);
	GetDlgItem(IDC_SPIN_OTHER_CHAR_SEP)->EnableWindow(true);


	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子  
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

///使用分隔符//
void CDBSettingDlg::OnRadioUseSeprate() 
{
	// TODO: Add your control notification handler code here
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_USE_SEPRATE);
	pBtn->SetCheck(1);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_CONST_FIELD);
	pBtn->SetCheck(0);


	GetDlgItem(IDC_EDIT_FIELD_NUM_CONST)->EnableWindow(false);
	GetDlgItem(IDC_SPIN_FIELD_NUM_SEP)->EnableWindow(false);

	GetDlgItem(IDC_RADIO_ENTER_RECORD)->EnableWindow(true);
	GetDlgItem(IDC_RADIO_OTHER_RECORD)->EnableWindow(true);

	GetDlgItem(IDC_EDIT_OTHER_CHAR_COR)->EnableWindow(true);
	GetDlgItem(IDC_SPIN_CHARTER_OTHER_SORD)->EnableWindow(true);


}

///按照固定字段数
void CDBSettingDlg::OnRadioConstField() 
{
	// TODO: Add your control notification handler code here
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_USE_SEPRATE);
	pBtn->SetCheck(0);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_CONST_FIELD);
	pBtn->SetCheck(1);


	GetDlgItem(IDC_RADIO_ENTER_RECORD)->EnableWindow(false);
	GetDlgItem(IDC_RADIO_OTHER_RECORD)->EnableWindow(false);

	GetDlgItem(IDC_EDIT_OTHER_CHAR_COR)->EnableWindow(false);
	GetDlgItem(IDC_SPIN_CHARTER_OTHER_SORD)->EnableWindow(false);

	GetDlgItem(IDC_EDIT_FIELD_NUM_CONST)->EnableWindow(true);
	GetDlgItem(IDC_SPIN_FIELD_NUM_SEP)->EnableWindow(true);

}

////回车///
void CDBSettingDlg::OnRadioEnterRecord() 
{
	// TODO: Add your control notification handler code here
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_RECORD);
	pBtn->SetCheck(1);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_CONST_FIELD);
	pBtn->SetCheck(0);

	GetDlgItem(IDC_EDIT_FIELD_NUM_CONST)->EnableWindow(false);
	GetDlgItem(IDC_SPIN_FIELD_NUM_SEP)->EnableWindow(false);
}

///其它///
void CDBSettingDlg::OnRadioOtherRecord() 
{
	// TODO: Add your control notification handler code here
	CButton* pBtn = NULL;

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_OTHER_RECORD);
	pBtn->SetCheck(1);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_RECORD);
	pBtn->SetCheck(0);

	GetDlgItem(IDC_EDIT_OTHER_CHAR_COR)->EnableWindow(true);
	GetDlgItem(IDC_SPIN_CHARTER_OTHER_SORD)->EnableWindow(true);

}


//字段分隔符、回车/
void CDBSettingDlg::OnRadioEnterSeprate() 
{
	// TODO: Add your control notification handler code here
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_SEPRATE);
	pBtn->SetCheck(1);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_OTHER_SEPRATE);
	pBtn->SetCheck(0);

	GetDlgItem(IDC_EDIT_OTHER_CHAR_SEP)->EnableWindow(false);
	GetDlgItem(IDC_SPIN_OTHER_CHAR_SEP)->EnableWindow(false);
}


//字段分隔符、其它/
void CDBSettingDlg::OnRadioOtherSeprate() 
{
	// TODO: Add your control notification handler code here
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_SEPRATE);
	pBtn->SetCheck(0);

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_OTHER_SEPRATE);
	pBtn->SetCheck(1);

	GetDlgItem(IDC_EDIT_OTHER_CHAR_SEP)->EnableWindow(true);
	GetDlgItem(IDC_SPIN_OTHER_CHAR_SEP)->EnableWindow(true);
}



void CDBSettingDlg::OnDeltaposSpinCharterOtherSord(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here

    if(pNMUpDown->iDelta == 1)        // 如果此值为1 , 说明点击了Spin的往下箭头

    {
		m_OtherCharValueCor--;
		if(m_OtherCharValueCor<1)
			m_OtherCharValueCor =1;
		m_nCharcterStringCord.Format("%c",m_OtherCharValueCor);
		 UpdateData(false);
    }

    else if(pNMUpDown->iDelta == -1) // 如果此值为-1 , 说明点击了Spin的往上箭头

    {
		m_OtherCharValueCor++;
		if(m_OtherCharValueCor>255)
			m_OtherCharValueCor = 255;	
         m_nCharcterStringCord.Format("%c",m_OtherCharValueCor);
		 UpdateData(false);
    }


	*pResult = 0;
}

void CDBSettingDlg::OnDeltaposSpinFieldNumSep(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	if(pNMUpDown->iDelta == 1)        // 如果此值为1 , 说明点击了Spin的往下箭头
    {
		m_FieldNumConstCord--;
		if(m_FieldNumConstCord<1)
			m_FieldNumConstCord = 1;
		UpdateData(false);
    }

    else if(pNMUpDown->iDelta == -1) // 如果此值为-1 , 说明点击了Spin的往上箭头
    {
		m_FieldNumConstCord++;
		if(m_FieldNumConstCord>255)
			m_FieldNumConstCord= 255;
		UpdateData(false);
    }

	*pResult = 0;
}

void CDBSettingDlg::OnDeltaposSpinOtherCharSep(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
		if(pNMUpDown->iDelta == 1)        // 如果此值为1 , 说明点击了Spin的往下箭头
    {
		m_FieldNumConstSep--;
		if(m_FieldNumConstSep<1)
			m_FieldNumConstSep = 1;
		m_nCharcterStringSep.Format("%c",m_FieldNumConstSep);
		UpdateData(false);
    }

    else if(pNMUpDown->iDelta == -1) // 如果此值为-1 , 说明点击了Spin的往上箭头
    {
		m_FieldNumConstSep++;
		if(m_FieldNumConstSep>255)
			m_FieldNumConstSep = 255;
		m_nCharcterStringSep.Format("%c",m_FieldNumConstSep);
		UpdateData(false);
    }
	*pResult = 0;
}



////记录号是否使用分隔符////
bool CDBSettingDlg::isUseFieldSymbol()
{
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_USE_SEPRATE);
	int state = pBtn->GetCheck();
	if(state == 1)
		return true;
	else
		return false;
}


void CDBSettingDlg::SetUseFieldSymbol(bool isUseFieldSymbol)
{
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_USE_SEPRATE);
	pBtn->SetCheck(isUseFieldSymbol);


	pBtn = (CButton*)GetDlgItem(IDC_RADIO_CONST_FIELD);
	pBtn->SetCheck(!isUseFieldSymbol);

}

////获取记录号的分隔符assic码
int CDBSettingDlg::GetRecordFieldSymbol()
{
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_RECORD);
	int state = pBtn->GetCheck();
	if(state == 1)
		return 10;

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_OTHER_RECORD);
	state = pBtn->GetCheck();
	if(state == 1)
		return m_OtherCharValueCor;
	return 0;
}

void CDBSettingDlg::SetRecordFieldSymbol(int n)
{

	if (n == 0)
	{
		CButton* pBtn = NULL;
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_CONST_FIELD);
		pBtn->SetCheck(1);

	}
	else
	{
		CButton* pBtn = NULL;
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_USE_SEPRATE);
		pBtn->SetCheck(1);
	}


	if (n == 10)
	{
		CButton* pBtn = NULL;
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_RECORD);
		pBtn->SetCheck(true);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_OTHER_RECORD);
		pBtn->SetCheck(false);
	}
	else
	{
			m_OtherCharValueCor = n;
			CButton* pBtn = NULL;
			pBtn = (CButton*)GetDlgItem(IDC_RADIO_OTHER_RECORD);
			pBtn->SetCheck(true);

			pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_RECORD);
			pBtn->SetCheck(false);
	}
}

///////记录号固定段/////////
int CDBSettingDlg::GetSeprateNum()
{
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_CONST_FIELD);
	int state = pBtn->GetCheck();
	if(state == 1)
		return m_FieldNumConstCord;

	return 0;

}


void CDBSettingDlg::SetSeprateNum(int n)
{
	if (n != 0)
	{
		m_FieldNumConstCord = n;
		CButton* pBtn = NULL;
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_CONST_FIELD);
		pBtn->SetCheck(1);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_USE_SEPRATE);
		pBtn->SetCheck(0);
	}
	else
	{
		CButton* pBtn = NULL;
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_CONST_FIELD);
		pBtn->SetCheck(0);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_USE_SEPRATE);
		pBtn->SetCheck(1);
	}
}

//////字段分隔符////
int CDBSettingDlg::GetSeprateFieldSymbol()
{
	CButton* pBtn = NULL;
	pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_SEPRATE);
	int state = pBtn->GetCheck();
	if(state == 1)
		return 10;

	pBtn = (CButton*)GetDlgItem(IDC_RADIO_OTHER_SEPRATE);
	state = pBtn->GetCheck();
	if(state == 1)
		return m_FieldNumConstSep;
	return -1;
}


void CDBSettingDlg::SetSeprateFieldSymbol(int n)
{
	if (n == 10)
	{
		CButton* pBtn = NULL;
		pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_SEPRATE);
		pBtn->SetCheck(1);

		pBtn = (CButton*)GetDlgItem(IDC_RADIO_OTHER_SEPRATE);
		pBtn->SetCheck(0);
	}
	else
	{
		if (n != -1)
		{
			CButton* pBtn = NULL;
			pBtn = (CButton*)GetDlgItem(IDC_RADIO_OTHER_SEPRATE);
			pBtn->SetCheck(1);
			m_FieldNumConstSep = n;

			pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_SEPRATE);
			pBtn->SetCheck(0);
		}
		else
		{
			CButton* pBtn = NULL;
			pBtn = (CButton*)GetDlgItem(IDC_RADIO_OTHER_SEPRATE);
			pBtn->SetCheck(0);

			pBtn = (CButton*)GetDlgItem(IDC_RADIO_ENTER_SEPRATE);
			pBtn->SetCheck(0);
		}
	}

}

HBRUSH CDBSettingDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
