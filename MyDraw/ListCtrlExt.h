
#if !defined(AFX_LISTEXTCTRL_H__28DE2D51_9580_4DCF_9FF8_B5759E73AB83__INCLUDED_)
#define AFX_LISTEXTCTRL_H__28DE2D51_9580_4DCF_9FF8_B5759E73AB83__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ListCtrlExt.h : header file
//

#include "subitemedit.h"
#include "CustomComBox.h"
#include "HeaderCtrlCl.h"


#define USER_CMB_MSG 5000

struct DataUnit
{
	int     bIsChecked;
	CString	Value;
};

class CPropertyItem
{
// Attributes
public:
	CString m_propName;
	CString m_curValue;
	int		m_nItemType;
	CString m_cmbItems;

public:
	CPropertyItem(CString propName, CString curValue,
				  int nItemType, CString cmbItems)
	{
		m_propName = propName;
		m_curValue = curValue;
		m_nItemType = nItemType;
		m_cmbItems = cmbItems;
	}
};

struct ColProperty
{
	float		fMax;
	float		fMin;
	int			nDecimals;
};

#define  POS_MAX 100
/////////////////////////////////////////////////////////////////////////////
// CShiListCtrl window

class CListCtrlExt : public CListCtrl
{
// Construction
public:
	CListCtrlExt();

	CHeaderCtrlCl m_ctrlHeader;
// Attributes
public:
	ColProperty			m_ColProperty[POS_MAX];//列属性

private:
	BOOL IsOnlyReadCol(int nCol);           //判断是否是可读列
	BOOL IsOnlyReadPos(int nRow,int nCol);  //判断是否是可读位置
	BOOL IsFixedCol(int nCol);              //判断是否是固定列
	BOOL IsButtonPos(int nRow,int nCol);    //判断是否是按钮位置 
	BOOL IsButtonCol(int nCol);
	BOOL IsCmbBoxPos(int nRow,int nCol);    //判断是否是下拉框位置
	BOOL IsFixedPos(int nRow,int nCol);		//判断是否是固定位置
	BOOL IsCmbCol(int nCol);
	int IsRedOrangePos(int nRow/*,int nCol*/);//0无  1红色  2orange


	int arrCols[POS_MAX]; 
	int nColCount;
	int arrOnlyReadCol[POS_MAX];  //存储只读列(灰显)
	int nOnlyReadCols;	//只读列数目
	int arrCmbCol[POS_MAX];
	int nCmbCols;
	
	//OnlyRead
	int	nItem[POS_MAX*3];
	int nSubItem[POS_MAX*3];
	int nOnlyReadPoses;
	//Fix
	int FixedItem[POS_MAX];
	int FixedSubItem[POS_MAX];
	int nFixedCount;
    //按钮
	int BtnItem[POS_MAX*3];
	int BtnSubItem[POS_MAX*3];
	int nBtnCount;
	//CmbBox
	int CmbBoxItem[POS_MAX*3];
	int CmbBoxSubItem[POS_MAX*3];
	int nCmbBoxCount;

	//红色
	int nRedItem[POS_MAX*3];
	int nRedSubItem[POS_MAX*3];
	int nRedCount;

	int nRedItemValue[POS_MAX*3]/*[10]*/;
	int nRedItemValue2[POS_MAX*3]/*[10]*/;
	int nMaxRow;
	int nMaxCol;

	CCustomComBox m_cmbBox;
	//CComboBox	m_cmbBox;
	int		  m_curSel;
	int       m_CurCol;

	int nCmbListRow;
	int nCmbListCol;

public:
	//着色行
	int nCompareDifferRow[POS_MAX*3];
	int nCompareDifferCount;
	void SetCompareDifferRow(int nRow[],int nCount);//设置着色行
	BOOL IsCompareDifferRow(int nRow);			//判断着色行


	void SetHeaderTextColor(int nItem[], int nCount,COLORREF color=RGB(255,0,0)); // add by hudx
	void SetColHeaderRed(int nColIndex);// add by hudx
	int InsertColumn(int nCol, LPCTSTR lpszColumnHeading,
		int nFormat = LVCFMT_LEFT, int nWidth = -1, int nSubItem = -1);//add by hudx

	void CListCtrlExt::onsetCmbCursel(int nRow);
	//void CListCtrlExt::onSetComBoxColor(int nRow,int nColor);

// Operations
public:
	CString   m_CmbItemsStr[POS_MAX*3]; 
	CFont     m_Song9Font;
	CFont	  m_fntText;
	BOOL	  m_bUnit;
	BOOL      m_bSelectAll;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShiListCtrl)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetColProperty(int nIndex, float fMin, float fMax, int nDecimal);
	void SetOnlyReadCol(int arrCol[], int nCount);
	void SetFixedCol(int arrColIndex[], int nSize);
	void SetCmbCol(int arrColIndex[],int nSize);
	void SetOnlyReadPos(int arrRow[],int arrCol[], int nCount);
	void SetButtonPos(int arrRow[],int arrCol[], int nCount);
	void SetCmbBoxPos(int arrRow[],int arrCol[], int nCount);
	void SetFixedPos(int arrRow[],int arrCol[], int nCount);
	CSubItemEdit* EditSubLabel( int nItem, int nCol);
	void CmbBoxLable( int nItem, int nCol,int nType = 0);
	int HitTestex(CPoint &point, int *col)const; 
	virtual ~CListCtrlExt();

	bool SetTextFont(LONG nHeight, bool bBold, bool bItalic, const CString& sFaceName);
	bool SetTextFont(const LOGFONT& lgfnt);

	void SetRedOrangePos(int nRow/*,int nCol*/,int nItem,int nItem2);



	// Generated message map functions
protected:
	//{{AFX_MSG(CShiListCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnEndlabeledit(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	afx_msg BOOL OnNMCustomDraw(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnKillfocusCmbBox();
	afx_msg void OnSelchangeCmbBox();
	DECLARE_MESSAGE_MAP()
	virtual void PreSubclassWindow();
public:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTEXTCTRL_H__28DE2D51_9580_4DCF_9FF8_B5759E73AB83__INCLUDED_)
