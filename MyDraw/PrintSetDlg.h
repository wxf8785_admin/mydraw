#if !defined(AFX_PRINTSETDLG_H__C18789B9_E562_4717_9A2E_ED815F04BF85__INCLUDED_)
#define AFX_PRINTSETDLG_H__C18789B9_E562_4717_9A2E_ED815F04BF85__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrintSetDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPrintSetDlg dialog

class CPrintSetDlg : public CDialog
{
// Construction
public:
	CPrintSetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPrintSetDlg)
	enum { IDD = IDD_DIALOG_PRINTSET };
	CButton	m_SimeyeTrig;
	CComboBox	m_nHeigh_PPIY;
	CComboBox	m_nHeigh_PPIX;
	CComboBox	m_nNorMal_PPIX;
	CComboBox	m_nNorMal_PPIY;
	int		m_nPaperLenth;
	float   m_dHeibiao;
	int		m_nSimuPrintSpeed;
	int		m_nSimlEyeLenth;
	int		m_nTimesTrig;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPrintSetDlg)
	protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	int nPaperLenth;
	bool isEyeTrig;

public:
	int CPrintSetDlg::GetPPIX();
	int CPrintSetDlg::GetPPIY();
	bool CPrintSetDlg::isSimEye();//�Ƿ�ģ�����
	int CPrintSetDlg::getSimeyeDistance();
	int CPrintSetDlg::GetPaperLenth();
	CString m_strParamPath;
	void CPrintSetDlg::SaveInkBoxParamIni();
	void CPrintSetDlg::ReadInkBoxParamIni();
protected:

	// Generated message map functions
	//{{AFX_MSG(CPrintSetDlg)
	afx_msg void OnCheckInkLimi();
	afx_msg void OnRadioPpiModeNormal();
	afx_msg void OnRadioPpiModeHighspeed();
	virtual BOOL OnInitDialog();
	afx_msg void OnCheckSimEyetrig();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeComboNormalPpix();
	afx_msg void OnCbnSelchangeComboHighspeedPpix();
	afx_msg void OnCbnSelchangeComboNormalPpiy();
	afx_msg void OnCbnSelchangeComboHighspeedPpiy();
	afx_msg void OnBnClickedOk();

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTSETDLG_H__C18789B9_E562_4717_9A2E_ED815F04BF85__INCLUDED_)
