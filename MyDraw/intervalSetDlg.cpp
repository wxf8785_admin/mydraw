// intervalSetDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MyDraw.h"
#include "intervalSetDlg.h"
#include "afxdialogex.h"


// intervalSetDlg 对话框

IMPLEMENT_DYNAMIC(intervalSetDlg, CDialogEx)

intervalSetDlg::intervalSetDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(intervalSetDlg::IDD, pParent)
	, m_dIntervalSet(0)
	, m_combo_fx(0)
{

}

intervalSetDlg::~intervalSetDlg()
{
}

void intervalSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_INTERVAL, m_dIntervalSet);
	DDX_Control(pDX, IDC_COMBO_FX, comboFX);
	DDX_CBIndex(pDX, IDC_COMBO_FX, m_combo_fx);
}


BEGIN_MESSAGE_MAP(intervalSetDlg, CDialogEx)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// intervalSetDlg 消息处理程序

BOOL intervalSetDlg::OnInitDialog()
{


	CDialog::OnInitDialog();

	comboFX.AddString(_T("水平"));
	comboFX.AddString(_T("垂直"));
	comboFX.SetCurSel(m_combo_fx);

	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子   
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

HBRUSH intervalSetDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
