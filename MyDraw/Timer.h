#pragma once
class CTimer
{

public:
	CTimer();
	~CTimer();
	void StartTimer(unsigned int nElapse);
	void EndTimer();
	static DWORD WINAPI ThreadFunc(LPVOID pParam);
private:
	unsigned int m_Elapse;
	HANDLE m_hThread;

};

