// ElementArray.cpp : 实现文件
//

#include "stdafx.h"
#include "MyDraw.h"
#include "ElementArray.h"


// CElementArray 对话框

IMPLEMENT_DYNAMIC(CElementArray, CDialog)

CElementArray::CElementArray(CWnd* pParent /*=NULL*/)
	: CDialog(CElementArray::IDD, pParent)

	, m_Horizontal(2)
	, m_fHorizontalDis(50)
	, m_Verital(2)
	, m_fVerticalDis(12.7)
	, m_FX(0)
{
	nMaxFieldNum = 0;

	nType = 0;
	ConstFieldIndex = 0;
	m_ChangeFieldStart = 0;
	m_ChangeFieldEnd = 0;
}

CElementArray::~CElementArray()
{
}

void CElementArray::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_HORIZONTAL, m_Horizontal);
	DDX_Text(pDX, IDC_EDIT_HORIZONTAL_DIS, m_fHorizontalDis);
	DDX_Text(pDX, IDC_EDIT_VERTICAL, m_Verital);
	DDX_Text(pDX, IDC_EDITVERTICAL_DIS, m_fVerticalDis);
	DDX_Control(pDX, IDC_COMBO_CONST_FIELDNUM, m_ConstFieldNumCmb);
	DDX_Control(pDX, IDC_COMBO_CHANGE_FIELD_START, m_ChangeFieldNumStartCmb);
	DDX_Control(pDX, IDC_COMBO_CHANGE_FIELD_END, m_ChangeFieldNumEndCmb);
	DDX_Control(pDX, IDC_RADIO_CONST_FIELDNUM, m_RadioConstFieldNum);
	DDX_Control(pDX, IDC_RADIO_CHANGE_FIELDNUM, m_RadioChangeFieldNum);
	DDX_Radio(pDX, IDC_RADIO_Z, m_FX);
}


BEGIN_MESSAGE_MAP(CElementArray, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO_CONST_FIELDNUM, &CElementArray::OnCbnSelchangeComboConstFieldnum)
	ON_CBN_SELCHANGE(IDC_COMBO_CHANGE_FIELD_START, &CElementArray::OnCbnSelchangeComboChangeFieldStart)
	ON_CBN_SELCHANGE(IDC_COMBO_CHANGE_FIELD_END, &CElementArray::OnCbnSelchangeComboChangeFieldEnd)
	ON_BN_CLICKED(IDC_RADIO_CONST_FIELDNUM, &CElementArray::OnBnClickedRadioConstFieldnum)
	ON_BN_CLICKED(IDC_RADIO_CHANGE_FIELDNUM, &CElementArray::OnBnClickedRadioChangeFieldnum)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CElementArray 消息处理程序

BOOL CElementArray::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_RadioConstFieldNum.SetCheck(true);
	m_RadioChangeFieldNum.SetCheck(false);

	m_ConstFieldNumCmb.EnableWindow(true);
	m_ChangeFieldNumStartCmb.EnableWindow(false);
	m_ChangeFieldNumEndCmb.EnableWindow(false);

	SetCmbString();


	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子  
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
void CElementArray::SetCmbString()
{
	if (nMaxFieldNum>0)
	{
		CString sTmp = "";
		m_ConstFieldNumCmb.AddString(sTmp);
		m_ChangeFieldNumStartCmb.AddString(sTmp);
		m_ChangeFieldNumEndCmb.AddString(sTmp);
		for (int i=0;i<nMaxFieldNum;i++)
		{
			sTmp.Format("字段%d",i+1);
			m_ConstFieldNumCmb.AddString(sTmp);
			m_ChangeFieldNumStartCmb.AddString(sTmp);
			m_ChangeFieldNumEndCmb.AddString(sTmp);

		}
		m_ConstFieldNumCmb.SetCurSel(1);
		m_ChangeFieldNumStartCmb.SetCurSel(1);
		m_ChangeFieldNumEndCmb.SetCurSel(1);

		ConstFieldIndex = 1;
		m_ChangeFieldStart = 1;
		m_ChangeFieldEnd = 1;
	}
	else
	{
		m_ConstFieldNumCmb.SetCurSel(0);
		m_ChangeFieldNumStartCmb.SetCurSel(0);
		m_ChangeFieldNumEndCmb.SetCurSel(0);
	}


}

void CElementArray::SetMaxFieldNum(int nMax)
{
	nMaxFieldNum = nMax;
}
void CElementArray::OnCbnSelchangeComboConstFieldnum()
{
	// TODO: 在此添加控件通知处理程序代码
	ConstFieldIndex  = m_ConstFieldNumCmb.GetCurSel();
}

void CElementArray::OnCbnSelchangeComboChangeFieldStart()
{
	// TODO: 在此添加控件通知处理程序代码
	m_ChangeFieldStart = m_ChangeFieldNumStartCmb.GetCurSel();
}

void CElementArray::OnCbnSelchangeComboChangeFieldEnd()
{
	// TODO: 在此添加控件通知处理程序代码
	m_ChangeFieldEnd = m_ChangeFieldNumEndCmb.GetCurSel();
}

void CElementArray::OnBnClickedRadioConstFieldnum()
{
	// TODO: 在此添加控件通知处理程序代码
	nType = 0;
	m_ConstFieldNumCmb.EnableWindow(true);
	m_ChangeFieldNumStartCmb.EnableWindow(false);
	m_ChangeFieldNumEndCmb.EnableWindow(false); 
	GetDlgItem(IDC_STATIC)->EnableWindow(false);
	GetDlgItem(IDC_RADIO_Z)->EnableWindow(false);
	GetDlgItem(IDC_RADIO_N)->EnableWindow(false);
}

void CElementArray::OnBnClickedRadioChangeFieldnum()
{
	// TODO: 在此添加控件通知处理程序代码
	nType = 1;
	m_ConstFieldNumCmb.EnableWindow(false);
	m_ChangeFieldNumStartCmb.EnableWindow(true);
	m_ChangeFieldNumEndCmb.EnableWindow(true);
	GetDlgItem(IDC_STATIC)->EnableWindow(true);
	GetDlgItem(IDC_RADIO_Z)->EnableWindow(true);
	GetDlgItem(IDC_RADIO_N)->EnableWindow(true);
}


HBRUSH CElementArray::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
