#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EllipseProperties.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEllipseProperties dialog

class CEllipseProperties : public CDialog
{
// Construction
public:
	COLORREF m_LineColor;
	COLORREF m_BrushColor;
	CEllipseProperties(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEllipseProperties)
	enum { IDD = IDD_DIALOG_ELLIPSE };
	CButton	m_BtnBrushColor;
	CButton	m_BtnColor;
	CString	m_LineWidth;
	CString	m_LineStyle;
	CString	m_HatchStyle;
	CString	m_BrushStyle;

	bool  bOnlyLine;

	//}}AFX_DATA
	CTabCtrl	m_tab;
	float m_fTxtWidth;
	float m_fTxtHeight;
	float m_fTxtLeftPos;
	float m_fTxtTopPos;

public:
	void CEllipseProperties::SetCtrlVisible(int nView);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEllipseProperties)
protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEllipseProperties)
	afx_msg void OnButtonColor();
	afx_msg void OnButtonBrushcolor();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnTcnSelchangeTabText(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
