#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CPicProperties �Ի���

class CPicProperties : public CDialog
{
	DECLARE_DYNAMIC(CPicProperties)

public:
	CPicProperties(CWnd* pParent = NULL);   // ��׼���캯��
	virtual ~CPicProperties();

// �Ի�������
	enum { IDD = IDD_DIALOG_PIC };

protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV ֧��

	DECLARE_MESSAGE_MAP()


public:
	CPic* pPic ;
	CTabCtrl m_PicPropertyTab;
	float m_fPicWidth;
	float m_fPicHeight;
	float m_fPicLeft;
	float m_fPicTop;
	CString m_PicStaticName;//��̬ͼƬ��
	CString m_PicStaticPathName;//��̬ͼƬ·��
	CString m_PicDirectory;//��̬ͼƬĿ¼
	int m_PicFieldNum;//�ֶ�
	bool    isStaticPic;//�Ƿ�Ϊ��̬ͼƬ
	CComboBox m_PicFieldNumCmb;//�ֶ�
	CButton m_PicStaticRadio;//��̬ͼƬ
	CButton m_PicDynamicRadio;//��̬ͼƬ
	int m_nPicRotateAngle;	//��ת�Ƕ�
	BOOL m_bFixScale;		//�Ƿ���������


public:
	int nMaxFieldNum;
//	int nFieldIndex;

public:
	void CPicProperties::SetSqlFieldNumParam(int nMxFdNum,int nFieldCmbIndex);
	void CPicProperties::OnInitSqlFieldNumCmb(int nNum,int nIndex);
public:

	afx_msg void OnBnClickedRadioStaticPic();
	afx_msg void OnBnClickedRadioDynamicPic();
	afx_msg void OnBnClickedButtonSelectPicPath();
	afx_msg void OnBnClickedButtonSelectBynamicPic();
	afx_msg void OnBnClickedRadioPicRorate0();
	afx_msg void OnBnClickedRadioPicRorate90();
	afx_msg void OnBnClickedRadioPicRorate180();
	afx_msg void OnBnClickedRadioPicRorate270();
	afx_msg void OnBnClickedRadioidcRadioPicMirror();

	afx_msg void OnCbnSelchangeComboPicFieldnum();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedYes();
	void SetCtrlVisible(int nView);
	afx_msg void OnTcnSelchangeTabPic(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedCheckFixScale();
	CButton m_checkFixScale;
	afx_msg void OnBnClickedOk();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
