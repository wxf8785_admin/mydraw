// Rectangle.cpp: implementation of the CRectangle class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MyDraw.h"
#include "MyDrawView.h"
#include "MainFrm.h"
#include "Rectangle.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
IMPLEMENT_SERIAL(CRectangle,CObject,1)

CRectangle::CRectangle()
{
	tracker.m_nStyle=CRectTracker::resizeInside|
		CRectTracker::dottedLine; 
	tracker.m_rect.SetRect(0,0,0,0); 
	tracker.m_nHandleSize=8; 

	bIsSelected=0;
	startX=41;
	startY=41;
	endX=140;
	endY=140;

	LinePen.lopnWidth.x=1;
	LinePen.lopnColor=RGB(0,0,0);
	LinePen.lopnStyle=PS_SOLID;

	MyBrush.lbColor=RGB(0,0,0);
	MyBrush.lbHatch=HS_CROSS;
	MyBrush.lbStyle=BS_NULL;

	bRound = false;

	nGraphics = 4;
}

CRectangle::~CRectangle()
{

}

void CRectangle::Serialize(CArchive & ar)
{
	CObject::Serialize(ar);
    if(ar.IsLoading())
    {
		ar>>startX>>startY>>endX>>endY>>
			LinePen.lopnWidth.x>>LinePen.lopnColor>>LinePen.lopnStyle>>
			MyBrush.lbColor >> MyBrush.lbHatch >> MyBrush.lbStyle >> bRound;
    }
	else
	{
        ar<<startX<<startY<<endX<<endY<<LinePen.lopnWidth.x<<
			LinePen.lopnColor<<LinePen.lopnStyle<<
			MyBrush.lbColor << MyBrush.lbHatch << MyBrush.lbStyle << bRound;
    }
}




CGraph* CRectangle::Clone()
{
	CRectangle* p = new CRectangle();

	p->startX = this->startX + 10;
	p->startY = this->startY + 10;
	p->endX = this->endX + 10;
	p->endY = this->endY + 10;
	p->LinePen = this->LinePen;
	p->MyBrush = this->MyBrush;
	p->bRound = this->bRound;
	return p;
}



void  CRectangle::InitPropList()
{

	CMFCPropertyGridProperty * pProp;
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();
	CMyDrawView *pView = (CMyDrawView *)pFrame->GetActiveView();

	pFrame->m_wndProperties.graphs.Add(this);
	//pFrame->m_wndProperties.m_wndObjectCombo.SetCurSel(nGraphics);
	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(0);
	//读取数据
	CString itemname;
	itemname = pProp->GetName(); //获取名称
	COleVariant itemvalue;
	itemvalue = pProp->GetValue();//获取值
	//写入数据
	CString m_startX;
	m_startX.Format(_T("%.2f"), startX / pView->ScaleX);
	pProp->SetValue((_variant_t)(m_startX));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(0)->GetSubItem(1);
	CString m_startY;
	m_startY.Format(_T("%.2f"), startY / pView->ScaleY);
	pProp->SetValue((_variant_t)(m_startY));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(0);
	CString m_BarWidth;
	m_BarWidth.Format(_T("%.2f"), (endX - startX) / pView->ScaleX * pView->Zoom);
	pProp->SetValue((_variant_t)(m_BarWidth));

	pProp = pFrame->m_wndProperties.m_wndPropList.GetProperty(1)->GetSubItem(1);
	CString m_BarHeight;
	m_BarHeight.Format(_T("%.2f"), (endY - startY) / pView->ScaleY * pView->Zoom);
	pProp->SetValue((_variant_t)(m_BarHeight));


}