
// stdafx.cpp : 只包括标准包含文件的源文件
// MyDraw.pch 将作为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include <math.h>

/////////////////////////////////////////////////////////////////////////////
// Convert units (pixels -> millimeters)
/////////////////////////////////////////////////////////////////////////////
int ConvPix2MM(int pixel, int dpi)
{
	return int(pixel  * 25400.0 / dpi + 0.5);
}


/////////////////////////////////////////////////////////////////////////////
// Convert units (millimeters -> pixels)
/////////////////////////////////////////////////////////////////////////////
int ConvMM2Pix(int mm, int dpi)
{
	return int(dpi * mm / 25400.0 + 0.5);
}



int SplitString(const CString str, char split, CStringArray &strArray)
{
	strArray.RemoveAll();
	CString strTemp = str;
	int iIndex = 0;
	while (1)
	{
		iIndex = strTemp.Find(split);
		if (iIndex >= 0)
		{
			strArray.Add(strTemp.Left(iIndex));
			strTemp = strTemp.Right(strTemp.GetLength() - iIndex - 1);
		}
		else
		{
			break;
		}
	}
	strArray.Add(strTemp);

	return strArray.GetSize();
}


double round(double r)
{
	return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
}

