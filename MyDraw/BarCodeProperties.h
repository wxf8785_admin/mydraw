#include "afxwin.h"
#if !defined(AFX_BARCODEPROPERTIES_H__0D6BA0B7_2F32_464A_BEC1_ABA352F21125__INCLUDED_)
#define AFX_BARCODEPROPERTIES_H__0D6BA0B7_2F32_464A_BEC1_ABA352F21125__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BarCodeProperties.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBarCodeProperties dialog

class CBarCodeProperties : public CDialog
{
	// Construction
public:
	CBarCodeProperties(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
	//{{AFX_DATA(CBarCodeProperties)
	enum { IDD = IDD_DIALOG_BARCODE };
	CBarCode *pBarCode;
	CButton	m_DefaultTxtDis;
	CComboBox	m_AlineType;
	CCustomComBox	m_DBFieldCmb;
	CComboBox	m_FrameCmb;
	CComboBox	m_RotateAngle;
	CComboBox	m_BarTypeCmb;
	CTabCtrl	m_BarProptyTab;
	CCustomComBox	m_nBarFieldNumCmb;
	CButton	m_RadioDate;
	CButton	m_RadioCounter;
	CButton	m_RadoiText;
	CButton m_RadoiDataBase;
	CButton m_FixGroupRadio;
	CButton m_ArbGroupRadio;
	CString	m_EditText;
	long	m_nStartNum;
	long	m_nStep;
	long	m_nEndNum;
	float	m_PosX;
	float	m_PosY;
	float	m_BarPenSize;
	int		m_nFieldIndex;//是哪个字段
	int		m_nFieldDistance;
	float	m_BarWidth;
	float	m_BarHeight;
	CString	m_sArbGroup;
	CString	m_FixGroup;
	//	CString	m_GroupDis;
	int	m_nGroupDis;
	int m_nTextAndBarDis;//文本和条形码的距离
	//}}AFX_DATA
	CString m_timeFormat;
	int m_startPos;
	int m_counts;
	CComboBox m_timeFormatCmb;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBarCodeProperties)
protected:
	CBrush   m_brush;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

public:
	LOGFONT		TextFont;
	COLORREF	TextColor;

	int nMaxFieldNum;
	int nFieldIndex;
	int nTimeFormatIndex;

	int m_nBarTypeIndex;
	int m_nFrameTypeIndex;
	int m_nTop;    //上边框宽度值（pixel）
	int m_nBottom;  ////下边框宽度值（pixel）
	int m_nRotateTypeIndex;
	int m_nAlineTypeIndex;
	BOOL isDefaultTextBarDis;

	CString sInsertParam;

	int nTextSpreadType;//文字分组方式，0固定分组，1任意分组

	int nBarPropertyType;
	int m_nSqlTypeFieldIndex;


	//二维码设置参数
	int nQRLevel;
	int nQRVersion;
	BOOL bQRAutoExtent;
	int nQRMaskingNo;
	float fQRRotateAngle;
public:
	CListBox m_listFontType;
	CListBox m_listFontStyle;
	CListBox m_listFontSize;
	CStatic m_staticFontTmp;
	CString m_strFontType;
	CString m_strFontStyle;
	CString m_strFontSize;
	CMap<CString, LPCSTR, int, int> m_mapFont;
	CMap<CString, LPCSTR, int, int> m_mapFontStyle;

	CButton	m_underLine;
	CButton m_strikeOut;
	CFont newFont;
	void CBarCodeProperties::OnInitFontCtrl();
	CString m_font; //定义的全局变量
	void CBarCodeProperties::OnInitFontType();
	void CBarCodeProperties::OnInitFontStyle();
	void CBarCodeProperties::OnInitFontSize();

	void CBarCodeProperties::OnIninFieldNumCom(int nNum, int nIndex, int SqlFieldIndex);
	void CBarCodeProperties::GetInitParam(int nMxFdNum, int nFieldCmbIndex);
	void CBarCodeProperties::SetCtrlVisible(int nView);

	int CBarCodeProperties::getInsetParam(CString sData);

	void CBarCodeProperties::OnInitTimeFormat(int nIndex);
	// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBarCodeProperties)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioContent();
	afx_msg void OnRadioConter();
	afx_msg void OnRadioDate();
	afx_msg void OnSelchangeComboFieldIndex();
	afx_msg void OnButtoBarFont();
	afx_msg void OnSelchangeTabBar(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRadioDatabase();
	afx_msg void OnRadioArbGroup();

	afx_msg void OnSelchangeComboBarType();

	afx_msg void OnSelchangeComboFrameType();
	afx_msg void OnSelchangeComboRotateType();
	afx_msg void OnSelchangeComboAlineType();

	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnBnClickedCheckDefaultTextdis();
	afx_msg void OnBnClickedButtonInsert();
	afx_msg void OnBnClickedRadioFixGroup();

	afx_msg void OnCbnSelchangeComboFieldDatabase();
	afx_msg void OnBnClickedButtonQrcodeproperties();
	afx_msg void OnBnClickedOk();
	CComboBox m_PENSIZEcmb;
	afx_msg void OnCbnSelchangeComboBarPensize();
	afx_msg void OnCbnSelchangeComboTimeFormat();
	afx_msg void OnBnClickedYes();
	afx_msg void OnLbnSelChangeFontType();
	afx_msg void OnLbnSelChangeFontStyle();
	afx_msg void OnLbnSelChangeFontSize();
	afx_msg void OnBnClickedCheckUnderLine();
	afx_msg void OnBnClickedCheckStikeOut();
	afx_msg void OnBnClickedButtonquietzoon();
	BOOL bReadable;
	BOOL bAbove;
	BOOL m_bMirror;

	afx_msg void OnEnChangeEditTxtFontSize();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BARCODEPROPERTIES_H__0D6BA0B7_2F32_464A_BEC1_ABA352F21125__INCLUDED_)
