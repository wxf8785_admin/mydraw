// MyDrawView.h : interface of the CMyDrawView class
//
/////////////////////////////////////////////////////////////////////////////
#include "Afxtempl.h"


#pragma once

#include "Line.h"
#include "LineProperties.h"

#include "Ellipse.h"

#include "Rectangle.h"
#include "Arc.h"
#include "Text.h"

#include "Barcode.h"

#include "PrintDlg.h"
#include "Pic.h"
#include "dmtx.h"

#include "ToolType.h"
#include "MainFrm.h"
#include "MyDrawDoc.h"



#ifdef _DEBUG
#pragma comment(lib, "libdmtx[rel_x86_vc10].lib")
#else
#pragma comment(lib, "libdmtx[rel_x86_vc10].lib")
#endif



typedef struct _NextPageParam
{
	int nPageIndex;
	int  nSMJET;

	bool operator<(const struct _NextPageParam & other) const
	{
		//Must be overloaded as public if this struct is being used as the KEY in map.
		if (this->nPageIndex < other.nPageIndex) return true;
		else if (this->nPageIndex > other.nPageIndex) return false;

		if (this->nSMJET < other.nSMJET) return true;

		return false;
	}
}NextPageParam;

class CMyDrawView : public CScrollView
{
protected: // 仅从序列化创建
	CMyDrawView();
	DECLARE_DYNCREATE(CMyDrawView)

	// 特性
public:
	CMainFrame*  m_pParent;

	CMyDrawDoc* GetDocument() const;
	CPtrList	m_ObjectList;		// 绘图对象的链表
	CRectTracker m_tracker;
	CPtrList m_selectLsit;//选中对象列表
	CPtrList m_createList;//创建图元list
	virtual  ~CMyDrawView();
	// Operations
public:

	// 操作
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyDrawView)
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
public:
	void     UpdateRulersInfo(int nMessage, CPoint ScrollPos, CPoint Pos = CPoint(0, 0));
	void     UpdateRulersInfo(int nMessage, CPoint ScrollPos, CRect Rect);
	//}}AFX_VIRTUAL
	
	//准备下一页
	CBitmap BitmapNext[6];    //600DPI
	CDC		MemDCnext[6];
	int		n_PageIndex;    //当前是第几页

	map<NextPageParam, int>m_NextPageParam;

public:
	int iLOGPIXELSX;
	int iLOGPIXELSY;

	int	m_nWidth; // Client width
	int	m_nHeight;// client height

public:
	float ScaleX;//每毫米像素数
	float ScaleY;//每毫米像素数

	int m_nSMJET;//总的arm数量
	BYTE bPrintData[6][12500][160];//当前页
	BYTE bPrintDataNext[6][12500][160];//下一页
	int nAllPaperNo;

	float ZoomInX;////放大缩小比例，为了处理画布问题
	float ZoomInY;
	float Zoom;//整体缩放比例
	bool  isGetData;//是否为获取数据，获取数据时不需要界面刷新，且需要放大

	float ZoomInX_Pic;
	float ZoomInY_Pic;

	int nRepPoint[6][4];//重合点数

	int Offset_X;//拉框x方向上偏移量
	int Offset_Y;//拉框y方向上偏移量

	int DPIX;    //垂直方向的dpi，0:600,1:300
	int DPIY;    //水平方向的dpi，0:600,1:300,2:200,3:150,4:100,5:75

	bool m_bIsEnablePrview;//预览是否禁用

public:

	int CoordZoomOut(int nPos, int nType);
	int CoordZoomOrgi(int nPos, int nType);
	void SetRepPoint(int nSMJET, int index, int nValue);

	CString ProcessInsertData(CString sInsert, CString sText, bool isField);
	CString ReplaceFieldNumToSring(CString sInsert, int nFieldIndex, CString sData);
	CString OnProcessFieldedString(CString oriString, CString sStyle, bool isField, int nSpace);
	CString OnProcessFieldedString(CString oriString, int nDis, bool isField, int nSpace);//固定字符分组
	CString GetNowTime(int nIndex);
	CString GetCounterText(long lStartNum, long lEndNum, long lStep, int pageNum);
	void CreateItem(CPoint point);

public:
	void read_to_binary_file();
	BYTE getByteValue(BYTE orgValue, int pos, int nValue);
	bool write_to_binary_file_bitmap(CBitmap* Bitmap, int nSMJET, int xDPI);//具体对应一个arm,xDPI表示横向缩放比例，600*300横向比例是2
	bool write_to_binary_file_bitmap_Next(CBitmap* Bitmap, int nSMJET, int xDPI);// 提前准备下一页数据
	bool SaveBitmapToFile(CBitmap* bitmap, LPSTR lpFileName, int nSMJET);
	HBITMAP GetRotatedBitmapNT(HBITMAP hBitmap, float radians, COLORREF clrBack);//旋转

	void OnArrayDistribute();
	void OnSetElementArray(int nHori, float fHDis, int nVercal, float fVDis, int nFieldStart, int nFieldEnd, int nFX);
	CString ResetFieldNum(CString sInsert, int nFieldIndex);

	void OnDeleteItem();
	void OnViewAtt();
	void OnLeftMove();
	void OnRightMove();
	void OnUpMove();
	void OnDownMove();
public:
	afx_msg void OnPrint();
	afx_msg void OnButtonLine();
	afx_msg void OnButtonEllipse();
	afx_msg void OnButtonRectangle();
	afx_msg void OnButtonRectangle2();
	afx_msg void OnButtonArc();
	afx_msg void OnButtonText();
	afx_msg void OnButtonBar();
	afx_msg void OnButtonTBar();
	afx_msg void OnButtonPic();
	afx_msg void OnButtonUP();
	afx_msg void OnButtonLEFT();
	afx_msg void OnButtonDOWN();
	afx_msg void OnButtonRIGHT();
	afx_msg void OnButtonHorizontal();
	afx_msg void OnButtonVertical();
	afx_msg void OnButtonInterval();

	//选择元素工具
	afx_msg void OnButtonSelect();

	afx_msg void OnEditCopy();
	afx_msg void OnEditPaste();
	afx_msg void OnEditCut();
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
	afx_msg void OnEditRedo();
	afx_msg void OnUpdateEditRedo(CCmdUI* pCmdUI);

	afx_msg void OnPropertiesLock();
	afx_msg void OnUpdatePropertiesLock(CCmdUI* pCmdUI);
	////获取同类型的数据个数
	int  GetPrintTypeNum(int nType);

	//预览
	afx_msg void OnButtonPreview();
	afx_msg void OnButtonHomePage();
	afx_msg void OnButtonNextPageBefore();
	afx_msg void OnButtonNextPage();
	afx_msg void OnButtonTailPage();
	afx_msg void OnUpdatePreview(CCmdUI* pCmdUI);
	afx_msg void OnPageNumChange();

	// Implementation
public:
	void DrawLine(CDC* pDC, CLine* pLine);
	void DrawArc(CDC* pDC, CArc* pArc);
	void DrawEllipse(CDC *pDC, CEllipse *pEllipse);
	void DrawRectangle(CDC *pDC, CRectangle *pRectangle);
	void DrawMyText(CDC *pDC, CText *pText, bool bIsFirst);
	void DrawPic(CDC *pDC, CPic *pPic);

	int P_DrawBarcode(CDC*pDC, int iX, int iY0, int iY10, int iY11, COLORREF clrBar, COLORREF clrSpace, float iPenW, CBarCode* pBc, int nFrameBord, int m_nRotateTyp);
	int P_DrawQRcode(CDC*pDC, int iX, int iY0, int iY10, int iY11, CString sCodeString, int nFrameBord, int m_nRotateTyp, int nQRLevel, int nQRVersion, BOOL bQRAutoExtent, int nQRMaskingNo, float angle);
	int P_DrawDMcode(CDC*pDC, int iX, int iY0, int iY10, int iY11, CString sCodeString, int nFrameBord, int m_nRotateTyp);
	int P_DrawPDF417code(CDC*pDC, int iX, int iY0, int iY10, int iY11, CString sCodeString, int nFrameBord, int m_nRotateTyp);
	int P_DrawEAN13code(CDC*pDC, int iX, int iY0, int iY10, int iY11, float iPenW, CString sCodeString, int m_nFrameType, int m_nRotateTyp, int nBitMapLenth);

	int OnDrawBarCodeText(CDC*pDC, CBarCode* pBarCode, bool bIsFirst);//返回高度
	void DrawSelection();  //选中状态下显示在刻度线上
	void ClearSelection(bool refresh = false);
	void UnInitPropList();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:
	void DrawUserGraph(CDC* pDC, bool first = true);
	void UpdateSQLPrintData(CString sPrintData, int nIndex);

	BOOL m_bChangeSize;
	int  m_nRetCode;
	BOOL IsRectOverlap(const CRect rcA, const CRect rcB);
	int  SelectElement(const CRect rectDraw, CPoint point);
	void AdjustRectSize(CRectTracker &tracker, CPoint point, int sx, int sy, int ex, int ey, int nOffsetx, int nOffsety, CObject* pHitItem, BOOL isFixScale = false);

public:
	int nMaxColNum;
	bool m_bIsLocked;
	CPoint		m_startPoint;		//绘制起点
	CPoint		m_endPoint;			//绘制终点
	CPoint      m_movePoint;
	BOOL		m_bLButtonDown;		//是否处于选择模式
	BOOL		m_nSelectedCount;	//有当前选中元素
	BOOL        m_bIsDrawRect;		//是否绘制拉框
	ToolsEnum	m_enumCurTool;		//当前工具

public:
	CRect   mRect;			    //全局画图区域
	int     PaperLenth;         //页长
protected:

	// Generated message map functions
protected:
	//{{AFX_MSG(CMyDrawView)
	afx_msg void OnFilePrintPreview();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//afx_msg void OnPaint();

	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);

	void set_DropDownHight(CComboBox& box, UINT LinesToDisplay);
	void set_DropDownWidth(CComboBox& box, UINT nWidth);
	////准备打印数据
	void Preparedata(int nPageIndex, int nSMJET, bool first = false);//具体对应哪一个arm    //first  第一次准备数据

	void PreparedataNext(int nPageIndex, int  nSMJET);

	void NextPage(int nPageIndex, int nSMJET);
};

#ifndef _DEBUG  // MyDrawView.cpp 中的调试版本
inline CMyDrawDoc* CMyDrawView::GetDocument() const
{
	return reinterpret_cast<CMyDrawDoc*>(m_pDocument);
}
#endif

