// TextProperties.cpp : implementation file
//

#include "stdafx.h"
#include "MyDraw.h"
#include "MainFrm.h"
#include "MyDrawView.h"
#include "TextProperties.h"

#include "Text.h"
#include "BarCode.h"
#include "Pic.h"
#include "TBarCode11.h"
#include <math.h>
#include <vector>
#include "Graph.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTextProperties dialog

extern CMap<int, int, int, int> m_mapFontSize;
extern CMap<int, int, CString, CString> m_mapFontHeight;
CTextProperties::CTextProperties(CWnd* pParent /*=NULL*/)
	: CDialog(CTextProperties::IDD, pParent)
	, pText(NULL)
	, m_fTxtWidth(0)
	, m_fTxtHeight(0)
	, m_fTxtLeftPos(0)
	, m_fTxtTopPos(0)
	, m_TxtLine(0)
	, m_lTxtCounterStartNum(1)
	, m_lTxtCounterEndNum(10000)
	, m_lTxtCounterStep(1)
	, m_fTxtRorateAngle(0)
	, m_iTxtFieldDis(2)
	, m_sTxtFieldType(_T("1,2,3,4"))
	, nDeburring_x(0)
	, nDeburring_y(0)
	, m_startPos(0)
	, m_counts(0)
	, nTxtRotateAngle(0)
	, isDelete0(false)
{
	//{{AFX_DATA_INIT(CTextProperties)
	m_Text = _T("");
	m_BkMode = FALSE;
	sInsertParam = "";

	nConstValueType=0;
	nFieldIndex = 0;
	nTxtRotateType=0;//旋转类型
	isMirrorImage=false;//是否为镜像；
	nTxtInlineType=1;//对齐方式
	isTxtUseStyleCtrl=false;//是否使用文本格式控制

	isUseAnyStyle=false;//任意模式
	isAntiDistrot=false;//防失真
	isUseDeburring=false;//是否修边
	nDeburring_x = 0;;
	nDeburring_y = 0;;
	//}}AFX_DATA_INIT
	nTimeFormatIndex = 0;
}


void CTextProperties::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTextProperties)
	DDX_Text(pDX, IDC_EDIT_TEXT_CONTENT, m_Text);
	DDX_Check(pDX, IDC_CHECK_BKMODE, m_BkMode);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_TAB_TEXT, m_TxtTab);
	DDX_Text(pDX, IDC_EDIT_TXT_WIDTH, m_fTxtWidth);
	DDX_Text(pDX, IDC_EDIT_TXT_HEIGHT, m_fTxtHeight);
	DDX_Text(pDX, IDC_EDIT_TXT_LEFT, m_fTxtLeftPos);
	DDX_Text(pDX, IDC_EDIT_TXT_TOP, m_fTxtTopPos);
	DDX_Control(pDX, IDC_COMBO_SQL_FIELD, m_InsertSqlFieldCmb);
	DDX_Control(pDX, IDC_COMBO_TXT_SQL, m_TxtContentSqlCmb);
	DDX_Control(pDX, IDC_RADIO_TXT_CONST, m_TxtConstRadio);
	DDX_Control(pDX, IDC_RADIO_TXT_COUNTER, m_TxtCounterRadio);
	DDX_Control(pDX, IDC_RADIO_TXT_SQL_FIELD, m_TxtSQLFieldRadio);
	DDX_Control(pDX, IDC_RADIO_TXT_DATE, m_TxtDateRadio);

	DDX_Control(pDX, IDC_COMBO_TXT_INLINE_TYPE, m_TxtInlineTypeCmb);
	DDX_Control(pDX, IDC_CHECK_TXT_ANY_STYLE, m_TxtEstendStyleAnyChk);
	DDX_Control(pDX, IDC_CHECK_TXT_ANTIDISTROT, m_TxtAntiDistrotChk);
	DDX_Control(pDX, IDC_CHECK_DEBURRING, m_TxtDeburringChk);
	DDX_Control(pDX, IDC_CHECK_TXT_IMAGE, m_TxtImageChk);
	DDX_Control(pDX, IDC_CHECK_TXT_FORM, m_isUseTxtFormChk);
	DDX_Text(pDX, IDC_EDIT_TXT_FIELD_DIS, m_iTxtFieldDis);
	DDV_MinMaxInt(pDX, m_iTxtFieldDis, -999, 999999);
	DDX_Text(pDX, IDC_EDIT_TXT_FOTM, m_sTxtFieldType);
	DDX_Text(pDX, IDC_EDIT_TXT_DEBURRING_X, nDeburring_x);
	DDX_Text(pDX, IDC_EDIT_TXT_DEBURRING_Y, nDeburring_y);
	DDV_MinMaxInt(pDX, nDeburring_y, 0, 20);
	DDV_MinMaxInt(pDX, nDeburring_x, 0, 20);

	DDX_Text(pDX, IDC_EDIT_STARTPOS, m_startPos);
	DDX_Text(pDX, IDC_EDIT_COUNTS, m_counts);
	DDX_Control(pDX, IDC_COMBO_TIME_FORMAT, m_timeFormatCmb);

	DDX_Text(pDX, IDC_EDIT_TXT_COUNTER_START, m_lTxtCounterStartNum);
	DDV_MinMaxLong(pDX, m_lTxtCounterStartNum, 1, 999999999);
	DDX_Text(pDX, IDC_EDIT_TXT_COUNTER_STEP, m_lTxtCounterStep);
	DDV_MinMaxLong(pDX, m_lTxtCounterStep, 1, 2147483647);
	DDX_Text(pDX, IDC_EDIT_TXT_COUNTER_END, m_lTxtCounterEndNum);
	DDV_MinMaxLong(pDX, m_lTxtCounterEndNum, 1, 999999999);
	DDX_Control(pDX, IDC_LIST_FONT_TYPE, m_listFontType);
	DDX_Control(pDX, IDC_LIST_FONT_STYLE, m_listFontStyle);
	DDX_Control(pDX, IDC_LIST_FONT_SIZE, m_listFontSize);
	DDX_Control(pDX, IDC_STATIC_FONT_TMP, m_staticFontTmp);
	DDX_Text(pDX, IDC_EDIT_FONT_TYPE, m_strFontType);
	DDX_Text(pDX, IDC_EDIT_FONT_STYLE, m_strFontStyle);
	DDX_Text(pDX, IDC_EDIT_FONT_SIZE, m_strFontSize);
	DDX_Control(pDX, IDC_CHECK_FONT_UNDERLINE, m_underLine);
	DDX_Control(pDX, IDC_CHECK_FONT_STRIKEOUT, m_strikeOut);
	DDX_Text(pDX, IDC_EDIT_TXT_RORATE_ANY, nTxtRotateAngle);
	DDV_MinMaxInt(pDX, nTxtRotateAngle, 0, 360);
	DDX_Control(pDX, IDC_CHECK_isDelete0, m_isDelete0);
}


BEGIN_MESSAGE_MAP(CTextProperties, CDialog)
	//{{AFX_MSG_MAP(CTextProperties)
	ON_BN_CLICKED(IDC_BUTTON_FONT, OnButtonFont)
	//}}AFX_MSG_MAP
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_TEXT, &CTextProperties::OnTcnSelchangeTabText)
	ON_BN_CLICKED(IDC_RADIO_TXT_CONST, &CTextProperties::OnBnClickedRadioTxtConst)
	ON_BN_CLICKED(IDC_RADIO_TXT_COUNTER, &CTextProperties::OnBnClickedRadioTxtCounter)
	ON_BN_CLICKED(IDC_RADIO_TXT_SQL_FIELD, &CTextProperties::OnBnClickedRadioTxtSqlField)
	ON_BN_CLICKED(IDC_RADIO_TXT_DATE, &CTextProperties::OnBnClickedRadioTxtDate)
	ON_BN_CLICKED(IDC_RADIO_RORATE_0, &CTextProperties::OnBnClickedRadioRorate0)
	ON_BN_CLICKED(IDC_RADIO_RORATE_90, &CTextProperties::OnBnClickedRadioRorate90)
	ON_BN_CLICKED(IDC_RADIO_RORATE_180, &CTextProperties::OnBnClickedRadioRorate180)
	ON_BN_CLICKED(IDC_RADIO_RORATE_270, &CTextProperties::OnBnClickedRadioRorate270)
	ON_BN_CLICKED(IDC_RADIO_RORATE_ANY, &CTextProperties::OnBnClickedRadioRorateAny)
	ON_BN_CLICKED(IDC_CHECK_TXT_IMAGE, &CTextProperties::OnBnClickedCheckTxtImage)
	ON_CBN_SELCHANGE(IDC_COMBO_TXT_INLINE_TYPE, &CTextProperties::OnCbnSelchangeComboTxtInlineType)
	ON_CBN_SELCHANGE(IDC_COMBO_TXT_SQL, &CTextProperties::OnCbnSelchangeComboTxtField)
	ON_BN_CLICKED(IDC_CHECK_TXT_FORM, &CTextProperties::OnBnClickedCheckTxtForm)
	ON_BN_CLICKED(IDC_CHECK_TXT_ANY_STYLE, &CTextProperties::OnBnClickedCheckTxtAnyStyle)
	ON_BN_CLICKED(IDC_CHECK_TXT_ANTIDISTROT, &CTextProperties::OnBnClickedCheckTxtAntidistrot)
	ON_BN_CLICKED(IDC_CHECK_DEBURRING, &CTextProperties::OnBnClickedCheckDeburring)
	ON_BN_CLICKED(IDC_BUTTON_INSERT_SQL_FIELD, &CTextProperties::OnBnClickedButtonInsertSqlField)
	ON_EN_CHANGE(IDC_EDIT_TXT_RORATE_ANY, &CTextProperties::OnEnChangeEditTxtRorateAny)
	ON_BN_CLICKED(IDOK, &CTextProperties::OnBnClickedOk)
	ON_CBN_SELCHANGE(IDC_COMBO_TIME_FORMAT, &CTextProperties::OnCbnSelchangeComboTimeFormat)
	ON_BN_CLICKED(IDYES, &CTextProperties::OnBnClickedYes)
	ON_LBN_SELCHANGE(IDC_LIST_FONT_TYPE, &CTextProperties::OnLbnSelChangeFontType)
	ON_LBN_SELCHANGE(IDC_LIST_FONT_STYLE, &CTextProperties::OnLbnSelChangeFontStyle)
	ON_LBN_SELCHANGE(IDC_LIST_FONT_SIZE, &CTextProperties::OnLbnSelChangeFontSize)
	ON_BN_CLICKED(IDC_CHECK_FONT_UNDERLINE, &CTextProperties::OnBnClickedCheckUnderLine)
	ON_BN_CLICKED(IDC_CHECK_FONT_STRIKEOUT, &CTextProperties::OnBnClickedCheckStikeOut)
	ON_EN_CHANGE(IDC_EDIT_FONT_SIZE, &CTextProperties::OnEnChangeEditTxtFontSize)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_CHECK_isDelete0, &CTextProperties::OnBnClickedCheckisdelete0)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTextProperties message handlers

void CTextProperties::OnButtonFont() 
{
	// TODO: Add your control notification handler code here
	CFontDialog dlg(&m_Font);
	if(dlg.DoModal()==IDOK)
	{
		dlg.GetCurrentFont(&m_Font);
		m_TextColor=dlg.m_cf.rgbColors;
	}
}

BOOL CTextProperties::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_TxtTab.InsertItem(0,"位置",0);
	m_TxtTab.InsertItem(1,"内容",1);
	m_TxtTab.InsertItem(2,"外形",2);
	m_TxtTab.InsertItem(3,"字体",3);

	m_TxtTab.SetCurSel(1);

	m_TxtInlineTypeCmb.AddString("左对齐");
	m_TxtInlineTypeCmb.AddString("居中对齐");
	m_TxtInlineTypeCmb.AddString("右对齐");
	m_TxtInlineTypeCmb.SetCurSel(nTxtInlineType);


	OnInitCtrlData();
	OnInitSqlFieldNumCmb(nMaxFieldNum,nFieldIndex);
	OnInitTimeFormat(nTimeFormatIndex);
	SetCtrlVisible(1);
	OnInitFontCtrl();
	
	m_brush.CreateSolidBrush(RGB(255, 255, 255));   //   生成一白色刷子      
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
void CTextProperties::OnInitCtrlData()
{
	if (nConstValueType ==0)
	{
		OnBnClickedRadioTxtConst();
	}
	else if (nConstValueType ==1)
	{
		OnBnClickedRadioTxtCounter();
	}
	else if (nConstValueType ==2)
	{
		OnBnClickedRadioTxtSqlField();
	}
	else if (nConstValueType ==3)
	{
		OnBnClickedRadioTxtDate();
	}


	/////
	CButton*	m_TxtRorateRadio1[5];
	m_TxtRorateRadio1[0] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_0);
	m_TxtRorateRadio1[1] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_90);
	m_TxtRorateRadio1[2] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_180);
	m_TxtRorateRadio1[3] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_270);
	m_TxtRorateRadio1[4] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_ANY);
	GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->EnableWindow(FALSE);

	for (int i=0;i<5;i++)
	{
		if (i==nTxtRotateType)
		{
			m_TxtRorateRadio1[i]->SetCheck(1);
			if (nTxtRotateType != 4)
			{
				m_fTxtRorateAngle = 0;
			}
			else
			{
				GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->EnableWindow(TRUE);
			}
		}
		else
			m_TxtRorateRadio1[i]->SetCheck(0);

		m_TxtRorateRadio1[i]->EnableWindow(TRUE);
	}
	
	////镜像
	if (isMirrorImage)
	{
		m_TxtImageChk.SetCheck(1);
	}
	else 
		m_TxtImageChk.SetCheck(0);

	//m_TxtImageChk.EnableWindow(false);


	//文本格式
	if (isTxtUseStyleCtrl)
	{
		m_isUseTxtFormChk.SetCheck(1);
		GetDlgItem(IDC_EDIT_TXT_FIELD_DIS)->EnableWindow(true);
		GetDlgItem(IDC_EDIT_TXT_FOTM)->EnableWindow(true);
	}
	else
	{	
		m_isUseTxtFormChk.SetCheck(0);
		GetDlgItem(IDC_EDIT_TXT_FIELD_DIS)->EnableWindow(false);
		GetDlgItem(IDC_EDIT_TXT_FOTM)->EnableWindow(false);	
	}

	//扩展
	if (isUseAnyStyle)
	{
		m_TxtEstendStyleAnyChk.SetCheck(1);
	}
	else
		m_TxtEstendStyleAnyChk.SetCheck(0);

	//m_TxtEstendStyleAnyChk.EnableWindow(false);

	if (isAntiDistrot)
	{
		m_TxtAntiDistrotChk.SetCheck(1);
	}
	else
		m_TxtAntiDistrotChk.SetCheck(0);
	m_TxtAntiDistrotChk.EnableWindow(false);



	if (isUseDeburring)
	{
		m_TxtDeburringChk.SetCheck(1);
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_X)->EnableWindow(true);
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_Y)->EnableWindow(true);
	}
	else
	{
		m_TxtDeburringChk.SetCheck(0);
		
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_X)->EnableWindow(false);
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_Y)->EnableWindow(false);

	}


	if (isDelete0)
	{
		m_isDelete0.SetCheck(1);
	}
	else
	{
		m_isDelete0.SetCheck(0);
	}

	//m_TxtDeburringChk.EnableWindow(FALSE);
		

}


void CTextProperties::SetRadioContentPropty()
{
	m_TxtConstRadio.SetCheck(1);
	m_TxtCounterRadio.SetCheck(0);
	m_TxtSQLFieldRadio.SetCheck(0);
	m_TxtDateRadio.SetCheck(0);


	GetDlgItem(IDC_EDIT_TEXT_CONTENT)->EnableWindow(true);
	GetDlgItem(IDC_COMBO_SQL_FIELD)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_TXT_LINE)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_INSERT_SQL_FIELD)->EnableWindow(true);

	GetDlgItem(IDC_EDIT_TXT_COUNTER_START)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TXT_COUNTER_END)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TXT_COUNTER_STEP)->EnableWindow(false);

	GetDlgItem(IDC_COMBO_TXT_SQL)->EnableWindow(false);


}


void CTextProperties::SetCtrlVisible(int nView)
{
	if(nView==0)
	{
		////位置与大小
		GetDlgItem(IDC_GROUP_TEXT_CONTENTPOS)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_TXT_WIDTH)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TXT_WIDTH)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_TXT_HEIGHT)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TXT_HEIGHT)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_TXT_LEFT)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TXT_LEFT)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_TXT_TOP)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TXT_TOP)->ShowWindow(true);

		//////内容
		GetDlgItem(IDC_STATIC_TXT_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_TXT_CONST)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TEXT_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_SQL_FIELD)->ShowWindow(false);

		GetDlgItem(IDC_BUTTON_INSERT_SQL_FIELD)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_LINE)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_TXT_LINE)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_TXT_COUNTER)->ShowWindow(false);
		
		GetDlgItem(IDC_STATIC_TXT_COUNTER_START)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_START)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_COUNTER_END)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_END)->ShowWindow(false);


		GetDlgItem(IDC_STATIC_TXT_COUNTER_STEP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_STEP)->ShowWindow(false);

		GetDlgItem(IDC_RADIO_TXT_SQL_FIELD)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_TXT_SQL)->ShowWindow(false);

		GetDlgItem(IDC_RADIO_TXT_DATE)->ShowWindow(false);

		////外形
		GetDlgItem(IDC_GROUP_TXT_RORATE)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_0)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_90)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_180)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_270)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_ANY)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_IMAGE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_IMAGE)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_INLINE_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_INLINE_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_TXT_INLINE_TYPE)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_FORM)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_FORM)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_FIELD_DIS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_FIELD_DIS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_FORM)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_FOTM)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_EXTEND_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_ANY_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_isDelete0)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_ANTIDISTROT)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_DEBURRING)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_DEBURRING)->ShowWindow(false);
	
		GetDlgItem(IDC_STATIC_TXT_DEBURRING_X)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_X)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_DEBURRING_Y)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_Y)->ShowWindow(false);


		////字体和颜色
		GetDlgItem(IDC_STATIC_FONT_COLOR)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_FONT)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_BKMODE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT_TMP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_UNDERLINE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_STRIKEOUT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_EFFECT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_TEMP)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_Text_Field)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_COUNTS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_COUNTS)->ShowWindow(false);


		GetDlgItem(IDC_COMBO_TIME_FORMAT)->ShowWindow(false);

	}
	else if (nView==1)
	{
		////位置与大小
		GetDlgItem(IDC_GROUP_TEXT_CONTENTPOS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_WIDTH)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_HEIGHT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_LEFT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_LEFT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_TOP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_TOP)->ShowWindow(false);

	
		//////内容
		GetDlgItem(IDC_STATIC_TXT_CONTENT)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_TXT_CONST)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TEXT_CONTENT)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_SQL_FIELD)->ShowWindow(true);

		GetDlgItem(IDC_BUTTON_INSERT_SQL_FIELD)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_TXT_LINE)->ShowWindow(true);

		GetDlgItem(IDC_EDIT_TXT_LINE)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_TXT_COUNTER)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_TXT_COUNTER_START)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_START)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_TXT_COUNTER_END)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_END)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_TXT_COUNTER_STEP)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_STEP)->ShowWindow(true);

		GetDlgItem(IDC_RADIO_TXT_SQL_FIELD)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_TXT_SQL)->ShowWindow(true);

		GetDlgItem(IDC_RADIO_TXT_DATE)->ShowWindow(true);


		////外形
		GetDlgItem(IDC_GROUP_TXT_RORATE)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_0)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_90)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_180)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_270)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_ANY)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_IMAGE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_IMAGE)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_INLINE_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_INLINE_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_TXT_INLINE_TYPE)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_FORM)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_FORM)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_FIELD_DIS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_FIELD_DIS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_FORM)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_FOTM)->ShowWindow(false);


		GetDlgItem(IDC_GROUP_EXTEND_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_ANY_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_isDelete0)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_ANTIDISTROT)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_DEBURRING)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_DEBURRING)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_DEBURRING_X)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_TXT_DEBURRING_X)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_DEBURRING_Y)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_Y)->ShowWindow(false);

		////字体和颜色
		GetDlgItem(IDC_STATIC_FONT_COLOR)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_FONT)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_BKMODE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT_TMP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_UNDERLINE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_STRIKEOUT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_EFFECT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_TEMP)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_Text_Field)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_STARTPOS)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_STARTPOS)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_COUNTS)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_COUNTS)->ShowWindow(true);


		GetDlgItem(IDC_COMBO_TIME_FORMAT)->ShowWindow(true);

	}
	else if (nView==2)
	{
		////位置与大小
		GetDlgItem(IDC_GROUP_TEXT_CONTENTPOS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_WIDTH)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_HEIGHT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_LEFT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_LEFT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_TOP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_TOP)->ShowWindow(false);

		//////内容
		GetDlgItem(IDC_STATIC_TXT_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_TXT_CONST)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TEXT_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_SQL_FIELD)->ShowWindow(false);

		GetDlgItem(IDC_BUTTON_INSERT_SQL_FIELD)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_LINE)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_TXT_LINE)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_TXT_COUNTER)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_COUNTER_START)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_START)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_COUNTER_END)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_END)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_COUNTER_STEP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_STEP)->ShowWindow(false);

		GetDlgItem(IDC_RADIO_TXT_SQL_FIELD)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_TXT_SQL)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_TXT_DATE)->ShowWindow(false);

		////外形
		GetDlgItem(IDC_GROUP_TXT_RORATE)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_RORATE_0)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_RORATE_90)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_RORATE_180)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_RORATE_270)->ShowWindow(true);
		GetDlgItem(IDC_RADIO_RORATE_ANY)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->ShowWindow(true);

		GetDlgItem(IDC_GROUP_TXT_IMAGE)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_TXT_IMAGE)->ShowWindow(true);

		GetDlgItem(IDC_GROUP_TXT_INLINE_TYPE)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_TXT_INLINE_TYPE)->ShowWindow(true);
		GetDlgItem(IDC_COMBO_TXT_INLINE_TYPE)->ShowWindow(true);

		GetDlgItem(IDC_GROUP_TXT_FORM)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_TXT_FORM)->ShowWindow(true);

		GetDlgItem(IDC_STATIC_TXT_FIELD_DIS)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TXT_FIELD_DIS)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_TXT_FORM)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TXT_FOTM)->ShowWindow(true);

		GetDlgItem(IDC_GROUP_EXTEND_STYLE)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_TXT_ANY_STYLE)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_isDelete0)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_TXT_ANTIDISTROT)->ShowWindow(true);

		GetDlgItem(IDC_GROUP_TXT_DEBURRING)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_DEBURRING)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_TXT_DEBURRING_X)->ShowWindow(true);

		GetDlgItem(IDC_EDIT_TXT_DEBURRING_X)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_TXT_DEBURRING_Y)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_Y)->ShowWindow(true);

		////字体和颜色
		GetDlgItem(IDC_STATIC_FONT_COLOR)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_FONT)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_BKMODE)->ShowWindow(false);

		GetDlgItem(IDC_LIST_FONT_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_LIST_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_FONT_TMP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_FONT_SIZE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_UNDERLINE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_FONT_STRIKEOUT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_EFFECT)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_GROUP_TEMP)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_Text_Field)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_COUNTS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_COUNTS)->ShowWindow(false);


		GetDlgItem(IDC_COMBO_TIME_FORMAT)->ShowWindow(false);

	}
	else if (nView==3)
	{
		////位置与大小
		GetDlgItem(IDC_GROUP_TEXT_CONTENTPOS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_WIDTH)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_WIDTH)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_HEIGHT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_HEIGHT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_LEFT)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_LEFT)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_TOP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_TOP)->ShowWindow(false);

		//////内容
		GetDlgItem(IDC_STATIC_TXT_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_TXT_CONST)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TEXT_CONTENT)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_SQL_FIELD)->ShowWindow(false);

		GetDlgItem(IDC_BUTTON_INSERT_SQL_FIELD)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_LINE)->ShowWindow(false);

		GetDlgItem(IDC_EDIT_TXT_LINE)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_TXT_COUNTER)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_COUNTER_START)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_START)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_COUNTER_END)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_END)->ShowWindow(false);


		GetDlgItem(IDC_STATIC_TXT_COUNTER_STEP)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_COUNTER_STEP)->ShowWindow(false);

		GetDlgItem(IDC_RADIO_TXT_SQL_FIELD)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_TXT_SQL)->ShowWindow(false);

		GetDlgItem(IDC_RADIO_TXT_DATE)->ShowWindow(false);

		////外形
		GetDlgItem(IDC_GROUP_TXT_RORATE)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_0)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_90)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_180)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_270)->ShowWindow(false);
		GetDlgItem(IDC_RADIO_RORATE_ANY)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_IMAGE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_IMAGE)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_INLINE_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_INLINE_TYPE)->ShowWindow(false);
		GetDlgItem(IDC_COMBO_TXT_INLINE_TYPE)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_FORM)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_FORM)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_FIELD_DIS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_FIELD_DIS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_FORM)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_FOTM)->ShowWindow(false);
		

		GetDlgItem(IDC_GROUP_EXTEND_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_ANY_STYLE)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_isDelete0)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_TXT_ANTIDISTROT)->ShowWindow(false);

		GetDlgItem(IDC_GROUP_TXT_DEBURRING)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_DEBURRING)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_TXT_DEBURRING_X)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_X)->ShowWindow(false);

		GetDlgItem(IDC_STATIC_TXT_DEBURRING_Y)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_Y)->ShowWindow(false);

		////字体和颜色
		GetDlgItem(IDC_STATIC_FONT_COLOR)->ShowWindow(false);
		GetDlgItem(IDC_BUTTON_FONT)->ShowWindow(false);
		GetDlgItem(IDC_CHECK_BKMODE)->ShowWindow(true);
		GetDlgItem(IDC_LIST_FONT_TYPE)->ShowWindow(true);
		GetDlgItem(IDC_LIST_FONT_STYLE)->ShowWindow(true);
		GetDlgItem(IDC_LIST_FONT_SIZE)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_FONT_TMP)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_FONT_TYPE)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_FONT_STYLE)->ShowWindow(true);
		GetDlgItem(IDC_EDIT_FONT_SIZE)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_FONT_UNDERLINE)->ShowWindow(true);
		GetDlgItem(IDC_CHECK_FONT_STRIKEOUT)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_GROUP_EFFECT)->ShowWindow(true);
		GetDlgItem(IDC_STATIC_GROUP_TEMP)->ShowWindow(true);


		GetDlgItem(IDC_GROUP_Text_Field)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_STARTPOS)->ShowWindow(false);
		GetDlgItem(IDC_STATIC_COUNTS)->ShowWindow(false);
		GetDlgItem(IDC_EDIT_COUNTS)->ShowWindow(false);


		GetDlgItem(IDC_COMBO_TIME_FORMAT)->ShowWindow(false);

	}

}

void CTextProperties::OnTcnSelchangeTabText(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	int nIndex = m_TxtTab.GetCurSel();
	SetCtrlVisible(nIndex);

	*pResult = 0;
}



void CTextProperties::OnBnClickedRadioTxtConst()
{
	// TODO: 在此添加控件通知处理程序代码
	nConstValueType  =0;
	m_TxtConstRadio.SetCheck(1);
	m_TxtCounterRadio.SetCheck(0);
	m_TxtSQLFieldRadio.SetCheck(0);
	m_TxtDateRadio.SetCheck(0);


	GetDlgItem(IDC_EDIT_TEXT_CONTENT)->EnableWindow(true);
	GetDlgItem(IDC_COMBO_SQL_FIELD)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_TXT_LINE)->EnableWindow(true);
	GetDlgItem(IDC_BUTTON_INSERT_SQL_FIELD)->EnableWindow(true);

	GetDlgItem(IDC_EDIT_TXT_COUNTER_START)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TXT_COUNTER_END)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TXT_COUNTER_STEP)->EnableWindow(false);

	GetDlgItem(IDC_COMBO_TXT_SQL)->EnableWindow(false);

	GetDlgItem(IDC_GROUP_Text_Field)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_COUNTS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_COUNTS)->EnableWindow(false);

	GetDlgItem(IDC_COMBO_TIME_FORMAT)->EnableWindow(false);
}

void CTextProperties::OnBnClickedRadioTxtCounter()
{
	// TODO: 在此添加控件通知处理程序代码
	nConstValueType = 1;
	m_TxtConstRadio.SetCheck(0);
	m_TxtCounterRadio.SetCheck(1);
	m_TxtSQLFieldRadio.SetCheck(0);
	m_TxtDateRadio.SetCheck(0);


	GetDlgItem(IDC_EDIT_TEXT_CONTENT)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_SQL_FIELD)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TXT_LINE)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_INSERT_SQL_FIELD)->EnableWindow(false);

	GetDlgItem(IDC_EDIT_TXT_COUNTER_START)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_TXT_COUNTER_END)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_TXT_COUNTER_STEP)->EnableWindow(true);

	GetDlgItem(IDC_COMBO_TXT_SQL)->EnableWindow(false);

	GetDlgItem(IDC_GROUP_Text_Field)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_COUNTS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_COUNTS)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_TIME_FORMAT)->EnableWindow(false);
}

void CTextProperties::OnBnClickedRadioTxtSqlField()
{
	// TODO: 在此添加控件通知处理程序代码
	nConstValueType = 2;
	m_TxtConstRadio.SetCheck(0);
	m_TxtCounterRadio.SetCheck(0);
	m_TxtSQLFieldRadio.SetCheck(1);
	m_TxtDateRadio.SetCheck(0);

	GetDlgItem(IDC_EDIT_TEXT_CONTENT)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_SQL_FIELD)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TXT_LINE)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_INSERT_SQL_FIELD)->EnableWindow(false);

	GetDlgItem(IDC_EDIT_TXT_COUNTER_START)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TXT_COUNTER_END)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TXT_COUNTER_STEP)->EnableWindow(false);

	GetDlgItem(IDC_COMBO_TXT_SQL)->EnableWindow(true);

	GetDlgItem(IDC_GROUP_Text_Field)->EnableWindow(true);
	GetDlgItem(IDC_STATIC_STARTPOS)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_STARTPOS)->EnableWindow(true);
	GetDlgItem(IDC_STATIC_COUNTS)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_COUNTS)->EnableWindow(true);
	GetDlgItem(IDC_COMBO_TIME_FORMAT)->EnableWindow(false);
}

void CTextProperties::OnBnClickedRadioTxtDate()
{
	// TODO: 在此添加控件通知处理程序代码
	nConstValueType = 3;
	m_TxtConstRadio.SetCheck(0);
	m_TxtCounterRadio.SetCheck(0);
	m_TxtSQLFieldRadio.SetCheck(0);
	m_TxtDateRadio.SetCheck(1);

	GetDlgItem(IDC_EDIT_TEXT_CONTENT)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_SQL_FIELD)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TXT_LINE)->EnableWindow(false);
	GetDlgItem(IDC_BUTTON_INSERT_SQL_FIELD)->EnableWindow(false);

	GetDlgItem(IDC_EDIT_TXT_COUNTER_START)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TXT_COUNTER_END)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_TXT_COUNTER_STEP)->EnableWindow(false);

	GetDlgItem(IDC_COMBO_TXT_SQL)->EnableWindow(false);

	GetDlgItem(IDC_GROUP_Text_Field)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_STARTPOS)->EnableWindow(false);
	GetDlgItem(IDC_STATIC_COUNTS)->EnableWindow(false);
	GetDlgItem(IDC_EDIT_COUNTS)->EnableWindow(false);
	GetDlgItem(IDC_COMBO_TIME_FORMAT)->EnableWindow(true);
}

void CTextProperties::OnBnClickedRadioRorate0()
{
	// TODO: 在此添加控件通知处理程序代码
	nTxtRotateType =0;
	GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->EnableWindow(FALSE);

	/////
	CButton	*m_TxtRorateRadio1[5];
	m_TxtRorateRadio1[0] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_0);
	m_TxtRorateRadio1[1] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_90);
	m_TxtRorateRadio1[2] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_180);
	m_TxtRorateRadio1[3] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_270);
	m_TxtRorateRadio1[4] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_ANY);

	for (int i=0;i<5;i++)
	{
		if (i==nTxtRotateType)
		{
			m_TxtRorateRadio1[i]->SetCheck(1);
			if (nTxtRotateType != 4)
			{
				nTxtRotateAngle = 0;
			}
		}
		else
			m_TxtRorateRadio1[i]->SetCheck(0);
	}

}

void CTextProperties::OnBnClickedRadioRorate90()
{
	// TODO: 在此添加控件通知处理程序代码
	nTxtRotateType =1;
	GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->EnableWindow(FALSE);

	/////
	CButton*	m_TxtRorateRadio1[5];
	m_TxtRorateRadio1[0] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_0);
	m_TxtRorateRadio1[1] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_90);
	m_TxtRorateRadio1[2] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_180);
	m_TxtRorateRadio1[3] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_270);
	m_TxtRorateRadio1[4] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_ANY);

	for (int i=0;i<5;i++)
	{
		if (i==nTxtRotateType)
		{
			m_TxtRorateRadio1[i]->SetCheck(1);
			if (nTxtRotateType != 4)
			{
				nTxtRotateAngle = 90;
			}
		}
		else
			m_TxtRorateRadio1[i]->SetCheck(0);
	}
}

void CTextProperties::OnBnClickedRadioRorate180()
{
	// TODO: 在此添加控件通知处理程序代码
	nTxtRotateType =2;
	GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->EnableWindow(FALSE);
	/////
	CButton*	m_TxtRorateRadio1[5];
	m_TxtRorateRadio1[0] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_0);
	m_TxtRorateRadio1[1] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_90);
	m_TxtRorateRadio1[2] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_180);
	m_TxtRorateRadio1[3] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_270);
	m_TxtRorateRadio1[4] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_ANY);

	for (int i=0;i<5;i++)
	{
		if (i==nTxtRotateType)
		{
			m_TxtRorateRadio1[i]->SetCheck(1);
			if (nTxtRotateType != 4)
			{
				nTxtRotateAngle = 180;
			}
		}
		else
			m_TxtRorateRadio1[i]->SetCheck(0);
	}
}

void CTextProperties::OnBnClickedRadioRorate270()
{
	// TODO: 在此添加控件通知处理程序代码
	nTxtRotateType =3;
	GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->EnableWindow(FALSE);
	/////
	CButton*	m_TxtRorateRadio1[5];
	m_TxtRorateRadio1[0] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_0);
	m_TxtRorateRadio1[1] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_90);
	m_TxtRorateRadio1[2] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_180);
	m_TxtRorateRadio1[3] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_270);
	m_TxtRorateRadio1[4] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_ANY);

	for (int i=0;i<5;i++)
	{
		if (i==nTxtRotateType)
		{
			m_TxtRorateRadio1[i]->SetCheck(1);
			if (nTxtRotateType != 4)
			{
				nTxtRotateAngle = 270;
			}
		}
		else
			m_TxtRorateRadio1[i]->SetCheck(0);
	}
}

void CTextProperties::OnBnClickedRadioRorateAny()
{
	// TODO: 在此添加控件通知处理程序代码
	nTxtRotateType =4;
	GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->EnableWindow(TRUE);
	/////
	CButton*	m_TxtRorateRadio1[5];
	m_TxtRorateRadio1[0] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_0);
	m_TxtRorateRadio1[1] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_90);
	m_TxtRorateRadio1[2] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_180);
	m_TxtRorateRadio1[3] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_270);
	m_TxtRorateRadio1[4] = (CButton*)GetDlgItem(IDC_RADIO_RORATE_ANY);

	for (int i=0;i<5;i++)
	{
		if (i==nTxtRotateType)
		{
			m_TxtRorateRadio1[i]->SetCheck(1);
			if (nTxtRotateType == 4)
			{
				CString str;
				GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->GetWindowTextA(str);
				nTxtRotateAngle = atoi(str);
			}
		}
		else
			m_TxtRorateRadio1[i]->SetCheck(0);
	}
}

void CTextProperties::OnBnClickedCheckTxtImage()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_TxtImageChk.GetCheck() == 1)
	{
		isMirrorImage = true;
	}
	else
		isMirrorImage = false;
}

void CTextProperties::OnCbnSelchangeComboTxtInlineType()
{
	// TODO: 在此添加控件通知处理程序代码
	int nIndex = m_TxtInlineTypeCmb.GetCurSel();

	if (nIndex == CB_ERR)
	{
		nTxtInlineType = 0;
		return;
	}
	nTxtInlineType = nIndex;

}

void CTextProperties::OnCbnSelchangeComboTxtField()
{
	CString strName = "";
	int nIndex = m_TxtContentSqlCmb.GetCurSel();

	if (nIndex == CB_ERR)
	{
		nFieldIndex = 0;
		return;
	}
	nFieldIndex = nIndex+1;
}
void CTextProperties::OnCbnSelchangeComboTimeFormat()
{
	int nIndex = m_timeFormatCmb.GetCurSel();
	if (nIndex == CB_ERR)
	{
		nTimeFormatIndex = 0;
		return;
	}
	nTimeFormatIndex = nIndex + 1;
}

void CTextProperties::OnBnClickedCheckTxtForm()
{
	// TODO: 在此添加控件通知处理程序代码
	if(m_isUseTxtFormChk.GetCheck())
	{
		isTxtUseStyleCtrl = true;
		GetDlgItem(IDC_EDIT_TXT_FIELD_DIS)->EnableWindow(true);
		GetDlgItem(IDC_EDIT_TXT_FOTM)->EnableWindow(true);

	}
	else
	{
		isTxtUseStyleCtrl = false;
		GetDlgItem(IDC_EDIT_TXT_FIELD_DIS)->EnableWindow(false);
		GetDlgItem(IDC_EDIT_TXT_FOTM)->EnableWindow(false);
	}
}

void CTextProperties::OnBnClickedCheckTxtAnyStyle()
{
	// TODO: 在此添加控件通知处理程序代码
	isUseAnyStyle = !isUseAnyStyle;

}

void CTextProperties::OnBnClickedCheckTxtAntidistrot()
{
	// TODO: 在此添加控件通知处理程序代码
}

void CTextProperties::OnBnClickedCheckDeburring()
{
	// TODO: 在此添加控件通知处理程序代码

	
	if (m_TxtDeburringChk.GetCheck())
	{

		isUseDeburring = true;
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_X)->EnableWindow(true);
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_Y)->EnableWindow(true);
	}
	else
	{
		isUseDeburring = false;
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_X)->EnableWindow(false);
		GetDlgItem(IDC_EDIT_TXT_DEBURRING_Y)->EnableWindow(false);

	}
}

void CTextProperties::SetSqlFieldNumParam(int nMxFdNum,int nFieldCmbIndex)
{
	nMaxFieldNum=nMxFdNum;
	nFieldIndex=nFieldCmbIndex;

}
void CTextProperties::OnInitSqlFieldNumCmb(int nNum,int nIndex)
{
	if(nNum<1)
		return;
	if(nIndex>nNum)
		nIndex =nNum;


	//数据库字段使用次数统计    //黑色的是固定数据，绿色的是一个字段，蓝色的是出现两次相通字段，红色是出现三次以上都是红色，
	map<int, int>m_nCountsUse;

	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;


	CText* pText = NULL;
	CBarCode* pBarCode = NULL;
	CTBarCode11* pTBarCode11 = NULL;
	CPic* pPic = NULL;
	CObject* pObject = NULL;
	POSITION pos = pView->m_ObjectList.GetHeadPosition();
	while (pos != NULL)
	{
		pObject = (CObject*)pView->m_ObjectList.GetNext(pos);

		if (pObject->IsKindOf(RUNTIME_CLASS(CPic)))
		{

		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CText)))
		{
			pText = (CText*)pObject;
			if (pText != NULL)
			{
				if (pText->nConstValueType == 0 && pText->sInsetSqlParam.GetLength() > 0)
				{
					for (int i = 0; i <= pView->nMaxColNum; i++)
					{
						CString tmp;
						tmp.Format(_T("字段%d"), i);
						if (pText->sInsetSqlParam.Find(tmp) >= 0)
							m_nCountsUse[i]++;
					}
				}
				else if (pText->nConstValueType == 2)
				{
					int nCount = pText->m_nFieldIndex;
					m_nCountsUse[nCount]++;
				}
			}

		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CBarCode)))
		{

		}
		else if (pObject->IsKindOf(RUNTIME_CLASS(CTBarCode11)))
		{
			pTBarCode11 = (CTBarCode11*)pObject;
			if (pTBarCode11 != NULL)
			{
				if (pTBarCode11->m_nBarPropertyType == 0 && pTBarCode11->sInsetParam.GetLength() > 0)
				{
					for (int i = 0; i <= pView->nMaxColNum; i++)
					{
						CString tmp;
						tmp.Format(_T("字段%d"), i);
						if (pTBarCode11->sInsetParam.Find(tmp) >= 0)
							m_nCountsUse[i]++;
					}

				}
				else if (pTBarCode11->m_nBarPropertyType == 2)
				{
					int nCount = pTBarCode11->m_nSqlTypeFieldIndex;
					m_nCountsUse[nCount]++;
				}
			}
		}
	}


	CString sText = "";
	for(int i=0;i<nNum;i++)
	{
		sText.Format("字段%d",i+1);
		int nIndex = m_InsertSqlFieldCmb.AddString(sText);

		COLORREF pColor = 0;
		if (m_nCountsUse[i + 1] == 1)
			pColor = 1;
		if (m_nCountsUse[i + 1] == 2)
			pColor = 2;
		if (m_nCountsUse[i + 1] >= 3)
			pColor = 3;

		if (pColor > 0)
			m_InsertSqlFieldCmb.SetColor(nIndex, pColor);

		nIndex = m_TxtContentSqlCmb.AddString(sText);
		if (pColor > 0)
			m_TxtContentSqlCmb.SetColor(nIndex, pColor);
	}

	if(nIndex<1)
		nIndex = 1;
	nFieldIndex = nIndex;
	m_InsertSqlFieldCmb.SetCurSel(nIndex-1);
	m_TxtContentSqlCmb.SetCurSel(nIndex-1);

}
void CTextProperties::OnInitTimeFormat(int nIndex)
{
	if (m_timeFormatCmb.GetCount() == 0)
	{
		m_timeFormatCmb.AddString("H:M:S");
		m_timeFormatCmb.AddString("HH:MM:SS");
		m_timeFormatCmb.AddString("YY-M-D");
		m_timeFormatCmb.AddString("YY-MM-DD");
		m_timeFormatCmb.AddString("YYYY-M-D");
		m_timeFormatCmb.AddString("YYYY-MM-DD");
		m_timeFormatCmb.AddString("DD-MM-YY");
		m_timeFormatCmb.AddString("MM-DD-YY");
		m_timeFormatCmb.AddString("MM-DD-YYYY");
		m_timeFormatCmb.AddString("YYYY.MM.DD");
		m_timeFormatCmb.AddString("DD.MM.YYYY");
		m_timeFormatCmb.AddString("MM.DD.YYYY");
		m_timeFormatCmb.AddString("YYYYMMDDHHMMSS");
		m_timeFormatCmb.AddString("YYYY-MM-DD HH:MM:SS");
	}
	if (nIndex<1)
	{
		nIndex = 13;
	}
	nTimeFormatIndex = nIndex;
	m_timeFormatCmb.SetCurSel(nIndex - 1);
}
void CTextProperties::OnBnClickedButtonInsertSqlField()
{
	// TODO: 在此添加控件通知处理程序代码
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_TEXT_CONTENT);
	ASSERT(pEdit);
	CString str;
	pEdit->GetWindowText(str);
	int nPos;

	pEdit->GetSel(nPos, nPos); 

	pEdit->SetSel(-1,0);//选中光标的当前位置

	int nIndex = m_InsertSqlFieldCmb.GetCurSel()+1;
	if (nIndex>0)
	{
		CString sData = "";
		sData.Format("[字段%d]",nIndex);
		pEdit->ReplaceSel(sData);//
		CString sTmp="";
		sTmp.Format("%d_[字段%d]_%d_#*",nPos,nIndex,nIndex);
		if (str.Find("[字段") >= 0) //如果插入多个字段
			sInsertParam += (sTmp);
		else
			sInsertParam = (sTmp);
	}
	
}


void CTextProperties::OnEnChangeEditTxtRorateAny()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialog::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码

	CString str;  
	GetDlgItem(IDC_EDIT_TXT_RORATE_ANY)->GetWindowTextA(str);


	nTxtRotateAngle = atoi(str);  

}

void CTextProperties::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	OnOK();
}

void CTextProperties::OnInitFontCtrl()
{
	OnInitFontType();
	OnInitFontStyle();
	OnInitFontSize();
	int nIdex;
	CString strSelText;
	if (m_mapFont.Lookup(_T(m_Font.lfFaceName), nIdex))
	{
		m_listFontType.SetCurSel(nIdex);
		m_strFontType = m_Font.lfFaceName;
		CEdit* pFontType;
		pFontType = (CEdit*)GetDlgItem(IDC_EDIT_FONT_TYPE);
		//赋值
		pFontType->SetWindowText(_T(m_strFontType));
	}
	if (m_Font.lfWeight == 700 && m_Font.lfItalic == TRUE)
	{
		strSelText = "粗体 斜体";
		if (m_mapFontStyle.Lookup(_T(strSelText), nIdex))
		{
			m_listFontStyle.SetCurSel(nIdex);
		}
	}
	else if (m_Font.lfWeight == 700 && m_Font.lfItalic == FALSE)
	{
		strSelText = "粗体";
		if (m_mapFontStyle.Lookup(_T(strSelText), nIdex))
		{
			m_listFontStyle.SetCurSel(nIdex);			
		}
	}
	else if (m_Font.lfWeight == 400 && m_Font.lfItalic == TRUE)
	{
		strSelText = "斜体";
		if (m_mapFontStyle.Lookup(_T(strSelText), nIdex))
		{
			m_listFontStyle.SetCurSel(nIdex);
		}
	}
	else if (m_Font.lfWeight == 400 && m_Font.lfItalic == FALSE)
	{
		strSelText = "常规";
		if (m_mapFontStyle.Lookup(_T(strSelText), nIdex))
		{
			m_listFontStyle.SetCurSel(nIdex);
		}
	}
	m_strFontStyle = strSelText;
	CEdit* pFontStyle;
	pFontStyle = (CEdit*)GetDlgItem(IDC_EDIT_FONT_STYLE);
	//赋值
	pFontStyle->SetWindowText(_T(m_strFontStyle));

	if (m_mapFontSize.Lookup(m_Font.lfHeight, nIdex))
	{
		m_listFontSize.SetCurSel(nIdex);
		CString strTemp;
		if (m_mapFontHeight.Lookup(m_Font.lfHeight, strTemp))
		{
			m_strFontSize = strTemp;
		}
		CEdit* pFontSize;
		pFontSize = (CEdit*)GetDlgItem(IDC_EDIT_FONT_SIZE);
		//赋值
		pFontSize->SetWindowText(_T(m_strFontSize));
	}
	else
	{
		CString strTemp;
		if (m_mapFontHeight.Lookup(m_Font.lfHeight, strTemp))
		{
			m_strFontSize = strTemp;
		}
		else
		{
			float FontSize = fabs(m_Font.lfHeight*72.0/ GetDeviceCaps(::GetDC(NULL), LOGPIXELSY));
			strTemp.Format(_T("%.1f"), FontSize);

			m_strFontSize = strTemp;
		}
		CEdit* pFontSize;
		pFontSize = (CEdit*)GetDlgItem(IDC_EDIT_FONT_SIZE);
		//赋值
		pFontSize->SetWindowText(_T(m_strFontSize));
	}
	if (m_Font.lfUnderline == TRUE)
	{
		m_underLine.SetCheck(1);
	}
	else
	{
		m_underLine.SetCheck(0);
	}
	if (m_Font.lfStrikeOut == TRUE)
	{
		m_strikeOut.SetCheck(1);
	}
	else
	{
		m_strikeOut.SetCheck(0);
	}
	//CFont newFont;
	newFont.DeleteObject();
	newFont.CreateFontIndirect(&m_Font);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
}



int CALLBACK EnumFontFamProc(LPENUMLOGFONT lpelf, LPNEWTEXTMETRIC lpntm, DWORD nFontType, long lparam)
{
	CTextProperties *pTextProperties = (CTextProperties*)lparam;//窗口句柄
	if (pTextProperties->m_font != (lpelf->elfLogFont.lfFaceName))
	{
		pTextProperties->m_font = (lpelf->elfLogFont.lfFaceName);
		int nIndex = pTextProperties->m_listFontType.AddString(pTextProperties->m_font);
		pTextProperties->m_mapFont.SetAt(pTextProperties->m_font, nIndex);
	}
	return 1;
}


void CTextProperties::OnInitFontType()
{
	
	LOGFONT lf;
	lf.lfCharSet = DEFAULT_CHARSET;
	strcpy(lf.lfFaceName, "");
	CClientDC dc(this);
	m_font = ""; //定义的全局变量
	::EnumFontFamiliesEx((HDC)dc, &lf,
		(FONTENUMPROC)EnumFontFamProc, (LPARAM)this, 0);

}

void CTextProperties::OnInitFontStyle()
{
	int nIndex;
	nIndex = m_listFontStyle.AddString("常规");
	m_mapFontStyle.SetAt("常规", nIndex);
	nIndex = m_listFontStyle.AddString("斜体");
	m_mapFontStyle.SetAt("斜体", nIndex);
	nIndex = m_listFontStyle.AddString("粗体");
	m_mapFontStyle.SetAt("粗体", nIndex);
	nIndex = m_listFontStyle.AddString("粗体 斜体");
	m_mapFontStyle.SetAt("粗体 斜体", nIndex);
}

void CTextProperties::OnInitFontSize()
{
	int nIndex = 0;


	nIndex = m_listFontSize.InsertString(nIndex, "5");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "6");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "7");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "8");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "9");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "10");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "11");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "12");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "14");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "16");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "18");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "20");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "22");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "24");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "26");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "28");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "36");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "48");
	nIndex = m_listFontSize.InsertString(nIndex + 1, "72");

	
}

void CTextProperties::OnLbnSelChangeFontType()
{
	//CFont newFont;
	newFont.DeleteObject();
	//newFont.CreatePointFont(160, "楷体");
	//m_staticFontTmp.SetFont(&newFont);

	int nSel = m_listFontType.GetCurSel();
	CString strSelText;
	m_listFontType.GetText(nSel, strSelText);
	strcpy(m_Font.lfFaceName, strSelText);

	//lf.lfHeight = -26;    //修改字体大小
	//lf.lfItalic = TRUE;        //倾斜
	//lf.lfWeight = 400;   //修改字体的粗细
	newFont.CreateFontIndirect(&m_Font);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
	m_strFontType = strSelText;
	CEdit* pBoxOne;
	pBoxOne = (CEdit*)GetDlgItem(IDC_EDIT_FONT_TYPE);
	//赋值
	pBoxOne->SetWindowText(_T(strSelText));
}
void CTextProperties::OnLbnSelChangeFontStyle()
{
	//CFont newFont;
	newFont.DeleteObject();
	int nSel = m_listFontStyle.GetCurSel();
	CString strSelText;
	m_listFontStyle.GetText(nSel, strSelText);
	if (strSelText == "粗体 斜体")
	{
		m_Font.lfWeight = 700;
		m_Font.lfItalic = TRUE;
	}
	else if (strSelText == "粗体")
	{
		m_Font.lfWeight = 700;
		m_Font.lfItalic = FALSE;
	}
	else if (strSelText == "斜体")
	{
		m_Font.lfWeight = 400;
		m_Font.lfItalic = TRUE;
	}
	else
	{
		m_Font.lfWeight = 400;
		m_Font.lfItalic = FALSE;
	}
	newFont.CreateFontIndirect(&m_Font);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
	m_strFontStyle = strSelText;
	CEdit* pBoxOne;
	pBoxOne = (CEdit*)GetDlgItem(IDC_EDIT_FONT_STYLE);
	//赋值
	pBoxOne->SetWindowText(_T(strSelText));
}
void CTextProperties::OnLbnSelChangeFontSize()
{
	//CFont newFont;
	newFont.DeleteObject();
	int nSel = m_listFontSize.GetCurSel();
	CString strSelText;
	m_listFontSize.GetText(nSel, strSelText);
	{
		int nFontSize = atof(strSelText) * 10;
		m_Font.lfHeight = -(int)(fabs(nFontSize*GetDeviceCaps(::GetDC(NULL), LOGPIXELSY) / 72.) / 10 + 0.5);
		newFont.CreateFontIndirect(&m_Font);//创建一个新的字体
		m_staticFontTmp.SetFont(&newFont);
		m_strFontSize = strSelText;


		m_mapFontSize.SetAt(m_Font.lfHeight, nSel);
	}

	//{
	//	newFont.CreateFontIndirect(&m_Font);//创建一个新的字体
	//	m_staticFontTmp.SetFont(&newFont);
	//	m_strFontSize = strSelText;
	//}
	CEdit* pBoxOne;
	pBoxOne = (CEdit*)GetDlgItem(IDC_EDIT_FONT_SIZE);
	//赋值
	pBoxOne->SetWindowText(_T(strSelText));
}

void CTextProperties::OnBnClickedCheckStikeOut()
{
	//CFont newFont;
	newFont.DeleteObject();
	if (m_strikeOut.GetCheck())
	{
		m_Font.lfStrikeOut = TRUE;
	}
	else
	{
		m_Font.lfStrikeOut = FALSE;
	}
	newFont.CreateFontIndirect(&m_Font);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
}
void CTextProperties::OnBnClickedCheckUnderLine()
{
	//CFont newFont;
	newFont.DeleteObject();
	if (m_underLine.GetCheck())
	{
		m_Font.lfUnderline = TRUE;
	}
	else
	{
		m_Font.lfUnderline = FALSE;
	}
	newFont.CreateFontIndirect(&m_Font);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
}
void CTextProperties::OnEnChangeEditTxtFontSize()
{
	//CFont newFont;
	newFont.DeleteObject();
	HDC hDC;
	hDC = ::GetDC(NULL);
	CString str;
	GetDlgItem(IDC_EDIT_FONT_SIZE)->GetWindowTextA(str);

	int nIndex = m_listFontSize.FindStringExact(0, str);
	if (nIndex>0)
	{
		int nFontSize = atof(str) * 10;
		m_Font.lfHeight = -(int)(fabs(nFontSize*GetDeviceCaps(hDC, LOGPIXELSY)/72.) / 10 + 0.5);
		newFont.CreateFontIndirect(&m_Font);//创建一个新的字体
		m_staticFontTmp.SetFont(&newFont);
		m_strFontSize = str;
		m_mapFontHeight.SetAt(m_Font.lfHeight, m_strFontSize);
		return;
	}

	int nFontSize = atof(str) * 10;
	m_Font.lfHeight = -(int)(fabs(nFontSize*GetDeviceCaps(hDC, LOGPIXELSY) / 72.) / 10 + 0.5);
	newFont.CreateFontIndirect(&m_Font);//创建一个新的字体
	m_staticFontTmp.SetFont(&newFont);
	m_strFontSize = str;


	m_mapFontHeight.SetAt(m_Font.lfHeight, m_strFontSize);
	::ReleaseDC(NULL, hDC);
}
void CTextProperties::OnBnClickedCheckisdelete0()
{
	// TODO:  在此添加控件通知处理程序代码

	if (m_isDelete0.GetCheck())
	{
		isDelete0 = TRUE;
	}
	else
	{
		isDelete0 = FALSE;
	}
}



void CTextProperties::OnBnClickedYes()
{
	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);


	if (pText == NULL)
		return;

	CView *pView1 = ((CFrameWndEx *)AfxGetMainWnd())->GetActiveView();
	CMyDrawView *pView = (CMyDrawView *)pView1;


	pText->MyFont = m_Font;
	pText->MyColor = m_TextColor;

	//删除字段后的处理
	CStringArray strArray;
	SplitString(sInsertParam, '*', strArray);
	CString sInsertParamTemp = "";
	for (int n = 0; n <= nMaxFieldNum; n++)
	{
		CString sTmp = "";
		sTmp.Format("[字段%d]", n);
		if (m_Text.Find(sTmp) != -1)
		{
			for (int m = 0; m < strArray.GetCount(); m++)
			{
				if (strArray[m].Find(sTmp) != -1)
				{
					sInsertParamTemp += strArray[m]+"*";
				}
			}
		}
	}
	sInsertParam = sInsertParamTemp;
	

	CString Tmp = m_Text;
	for (int n = 0; n <= nMaxFieldNum; n++)
	{
		CString sTmp = "";
		sTmp.Format("[字段%d]", n);
		Tmp.Replace(sTmp, "");
	}
	pText->MyText = Tmp;

	//						pText->MyText=m_Text;  
	pText->sInsetSqlParam = sInsertParam;
	pText->nConstValueType = nConstValueType;
	pText->m_nFieldIndex = nFieldIndex;
	pText->nStartPos = m_startPos;
	pText->nCounts = m_counts;
	pText->m_lTxtCounterStartNum = m_lTxtCounterStartNum;
	pText->m_lTxtCounterEndNum = m_lTxtCounterEndNum;
	pText->m_lTxtCounterStep = m_lTxtCounterStep;
	pText->m_nTimeFormatIndex = nTimeFormatIndex;

	pText->nTxtRotateAngle = nTxtRotateAngle;
	pText->nTxtRotateType = nTxtRotateType;
	if (pText->nTxtRotateType == 0)pText->nTxtRotateAngle = 0;
	if (pText->nTxtRotateType == 1)pText->nTxtRotateAngle = 90;
	if (pText->nTxtRotateType == 2)pText->nTxtRotateAngle = 180;
	if (pText->nTxtRotateType == 3)pText->nTxtRotateAngle = 270;



	if (m_BkMode)
		pText->BkMode = TRANSPARENT;
	else
		pText->BkMode = OPAQUE;

	pText->isTxtUseStyleCtrl = isTxtUseStyleCtrl;//是否使用文本控制
	pText->m_iTxtFieldDis = m_iTxtFieldDis;//组间距
	if (!pText->isTxtUseStyleCtrl)
	{
		pText->m_iTxtFieldDis = 0;
	}
	pText->m_sTxtFieldType = m_sTxtFieldType;//组格式串

	pText->isUseDeburring = isUseDeburring;
	pText->nDeburring_x = nDeburring_x;
	pText->nDeburring_y = nDeburring_y;

	pText->isDelete0 = isDelete0;

	pText->startX = m_fTxtLeftPos * pView->ScaleX / pView->Zoom;
	pText->startY = m_fTxtTopPos * pView->ScaleY / pView->Zoom;

	pText->endX = pText->startX + m_fTxtWidth * pView->ScaleX / pView->Zoom;
	pText->endY = pText->startY + m_fTxtHeight * pView->ScaleY / pView->Zoom;

	pText->tracker.m_rect.SetRect(pView->CoordZoomOut(pText->startX, 0), pView->CoordZoomOut(pText->startY, 1), pView->CoordZoomOut(pText->endX, 0), pView->CoordZoomOut(pText->endY, 1));
	if (pText->nConstValueType == 1)
	{
		pText->MyText = pView->GetCounterText(pText->m_lTxtCounterStartNum, pText->m_lTxtCounterEndNum, pText->m_lTxtCounterStep, 1);
		//if (pText->MyText.GetLength() > 0)
		//{
		//	pText->m_lTxtCounterStartNum = _ttol(_T(pText->MyText));
		//}
	}
	else if (pText->nConstValueType == 2)
	{
		CMainFrame* pMainFrame = (CMainFrame*)AfxGetApp()->m_pMainWnd;
		CString sPrint = pMainFrame->pPrintDlg->m_RecordNumList.GetItemText(0, pText->m_nFieldIndex);
		if (pText->nStartPos >= 0 && pText->nStartPos<sPrint.GetLength() && pText->nCounts>0 && pText->nCounts <= sPrint.GetLength())
		{
			sPrint = sPrint.Mid(pText->nStartPos, pText->nCounts);
		}
		pText->MyText = sPrint;
	}
	else if (pText->nConstValueType == 3)
	{

		pText->MyText = pView->GetNowTime(pText->m_nTimeFormatIndex);
	}

	pText->bIsSelected = false;

	pView->Invalidate(FALSE);
}


HBRUSH CTextProperties::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	//HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	//return hbr;
	return   m_brush;       //返加白色刷子 
}
